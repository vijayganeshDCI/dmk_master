package com.dci.dmkitwings.model;

public class ShareCount {
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getMainid() {
        return mainid;
    }

    public void setMainid(int mainid) {
        this.mainid = mainid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    private int type;
    private int mainid;
    private int userid;
}
