package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.utils.LanguageHelper;


import butterknife.BindView;
import butterknife.ButterKnife;

public class DigitalLibraryDetailsActivity extends BaseActivity {
    String contentLink,chatwithProPic;
    @BindView(R.id.webview)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_pdf_library, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        frameLayoutNotification.setVisibility(View.GONE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        imageBaseProPic.setVisibility(View.VISIBLE);
        Intent bundle = getIntent();
        if (getIntent() != null) {
            contentLink = bundle.getStringExtra("content");
            textTitle.setText(bundle.getStringExtra("bookName"));
            chatwithProPic = bundle.getStringExtra("proPic");
            universalImageLoader(imageBaseProPic, getString(R.string.s3_bucket_Library_path),
                    chatwithProPic, R.mipmap.icon_pro_image_loading_256,
                    R.mipmap.icon_pro_image_loading_256);
        }

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                webView.loadUrl("javascript:(function() { " +
                        "document.querySelector('[role=\"toolbar\"]').remove();})()");

                hideProgress();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                hideProgress();

            }
        });
        if (contentLink != null) {
            showProgress();
            webView.loadUrl("http://docs.google.com/gview?embedded=true&url="+
                    getString(R.string.s3_image_url_download)+getString(R.string.s3_bucket_Library_path)+contentLink);
        }


    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }


    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("MyData", "newslink");
        setResult(0, intent);
        finish();
    }

}
