package com.dci.dmkitwings.model;

import java.util.List;

public class ViewEventResults {

    public String getVideothumb() {
        return Videothumb;
    }

    public void setVideothumb(String videothumb) {
        Videothumb = videothumb;
    }

    private String Videothumb;

    public int getMunicipalityid() {
        return municipalityid;
    }

    public void setMunicipalityid(int municipalityid) {
        this.municipalityid = municipalityid;
    }

    public int getEvent_createdby() {
        return event_createdby;
    }

    public void setEvent_createdby(int event_createdby) {
        this.event_createdby = event_createdby;
    }

    public int getPartid() {
        return partid;
    }

    public void setPartid(int partid) {
        this.partid = partid;
    }

    public int getVillagepanchayat() {
        return villagepanchayat;
    }

    public void setVillagepanchayat(int villagepanchayat) {
        this.villagepanchayat = villagepanchayat;
    }

    public String getEvent_end_date() {
        return event_end_date;
    }

    public void setEvent_end_date(String event_end_date) {
        this.event_end_date = event_end_date;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }

    public int getEvent_updatedby() {
        return event_updatedby;
    }

    public void setEvent_updatedby(int event_updatedby) {
        this.event_updatedby = event_updatedby;
    }

    public int getWardid() {
        return wardid;
    }

    public void setWardid(int wardid) {
        this.wardid = wardid;
    }

    public int getTownpanchayat() {
        return townpanchayat;
    }

    public void setTownpanchayat(int townpanchayat) {
        this.townpanchayat = townpanchayat;
    }

    public String getEvent_desc() {
        return event_desc;
    }

    public void setEvent_desc(String event_desc) {
        this.event_desc = event_desc;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public int getMedia_type() {
        return media_type;
    }

    public void setMedia_type(int media_type) {
        this.media_type = media_type;
    }

    public int getConstituencyid() {
        return constituencyid;
    }

    public void setConstituencyid(int constituencyid) {
        this.constituencyid = constituencyid;
    }

    public String getEvent_titleimage() {
        return event_titleimage;
    }

    public void setEvent_titleimage(String event_titleimage) {
        this.event_titleimage = event_titleimage;
    }

    public int getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(int district_id) {
        this.district_id = district_id;
    }

    public int getDivisionid() {
        return divisionid;
    }

    public void setDivisionid(int divisionid) {
        this.divisionid = divisionid;
    }

    public int getPanchayatunion() {
        return panchayatunion;
    }

    public void setPanchayatunion(int panchayatunion) {
        this.panchayatunion = panchayatunion;
    }

    public int getVattamid() {
        return vattamid;
    }

    public void setVattamid(int vattamid) {
        this.vattamid = vattamid;
    }

    public int getPartydistrictid() {
        return partydistrictid;
    }

    public void setPartydistrictid(int partydistrictid) {
        this.partydistrictid = partydistrictid;
    }

    public String getEvent_start_date() {
        return event_start_date;
    }

    public void setEvent_start_date(String event_start_date) {
        this.event_start_date = event_start_date;
    }

    private int municipalityid;
    private int event_createdby;
    private int partid;
    private int villagepanchayat;
    private String event_end_date;
    private String event_title;
    private int event_updatedby;
    private int wardid;
    private int townpanchayat;
    private String event_desc;
    private int event_id;
    private int media_type;
    private int constituencyid;
    private String event_titleimage;
    private int district_id;
    private int divisionid;
    private int panchayatunion;
    private int vattamid;

    public int getSharecount() {
        return Sharecount;
    }

    public void setSharecount(int sharecount) {
        Sharecount = sharecount;
    }

    private int Sharecount;
    private String venue;

    public int getViewcount() {
        return Viewcount;
    }

    public void setViewcount(int viewcount) {
        Viewcount = viewcount;
    }

    private int Viewcount;

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public int getBoothID() {
        return BoothID;
    }

    public void setBoothID(int boothID) {
        BoothID = boothID;
    }

    private int BoothID;
    private int partydistrictid;
    private String event_start_date;

    public EventListUser getUser() {
        return user;
    }

    public void setUser(EventListUser user) {
        this.user = user;
    }

    private EventListUser user;


    public List<String> getMediafiles() {
        return mediafiles;
    }

    public void setMediafiles(List<String> mediafiles) {
        this.mediafiles = mediafiles;
    }

    private List<String> mediafiles;

    public int getMediatype() {
        return mediatype;
    }

    public void setMediatype(int mediatype) {
        this.mediatype = mediatype;
    }

    private int mediatype;
}
