package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ElectionPollActivity;
import com.dci.dmkitwings.model.Election;
import com.dci.dmkitwings.model.ElectionUserlistItem;

import java.util.ArrayList;
import java.util.List;

import static com.dci.dmkitwings.app.DmkApplication.getProxy;

/**
 * Created by vijayaganesh on 4/2/2018.
 */

public class ElectionListAdpater extends RecyclerView.Adapter<ElectionListAdpater.TimeLineViewHolder> {


    ArrayList<ElectionUserlistItem> electionuserListArray;
    Context context;
    HttpProxyCacheServer proxy;
    boolean clickablecheck=false;
    int userid;

    public ElectionListAdpater(   ArrayList<ElectionUserlistItem> electionuserListArray, Context context,Boolean clickablecheck) {
        this.electionuserListArray = electionuserListArray;
        this.context = context;
        this.clickablecheck=clickablecheck;
        this.userid=userid;

    }


    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_election, parent, false);
        proxy = getProxy(context);
        return new TimeLineViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, final int position) {
        ElectionUserlistItem election = electionuserListArray.get(position);
        holder.textElectionid.setText(" "+election.getVoterid());
        //holder.textElectionlink.setText(election.getLastname());
        holder.textElectionidname.setText(election.getFirstname());

       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if (clickablecheck)
               {
                   Intent intent = new Intent(context, ElectionPollActivity.class);
                   intent.putExtra("userid",electionuserListArray.get(position).getUserid());
                   intent.putExtra("surveyid",electionuserListArray.get(position).getSurveyid());
                   intent.putExtra("address",electionuserListArray.get(position).getAddress());
                   intent.putExtra("username",electionuserListArray.get(position).getFirstname());
                   intent.putExtra("position",position);
                   context.startActivity(intent);
               }

           }
       });

    }

    public Bitmap createThumbnailFromPath(String filePath, int type) {
        return ThumbnailUtils.createVideoThumbnail(filePath, type);
    }

    @Override
    public int getItemCount() {
        return electionuserListArray.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class TimeLineViewHolder extends RecyclerView.ViewHolder {

        CardView cardElection;
        ConstraintLayout consItemElection;
        TextView textElectionid;
        TextView textElectionlink;
        TextView textElectionidname;
        TextView textconstituencyname;

        public TimeLineViewHolder(View itemView) {
            super(itemView);
//            cardElection = (CardView) itemView.findViewById(R.id.card_election);
            consItemElection = (ConstraintLayout) itemView.findViewById(R.id.cons_item_election);
            textElectionid = (TextView) itemView.findViewById(R.id.text_voterid);
            textElectionlink = (TextView) itemView.findViewById(R.id.text_voteridlink);
            textElectionidname = (TextView) itemView.findViewById(R.id.text_votername);


        }
    }
}
