package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.dci.dmkitwings.BuildConfig;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.UserDetailsResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.view.CustomTextView;
import com.firebase.client.utilities.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.droidsonroids.gif.GifImageView;

import static com.dci.dmkitwings.app.DmkApplication.getContext;

/**
 * Created by vijayaganesh on 3/15/2018.
 */

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.cons_splash)
    ConstraintLayout consSplash;
    @BindView(R.id.image_gif_splash)
    GifImageView imageGifSplash;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String language = "ta";
    Uri deepLinkData;
    UserDetailsResponse userDetailsResponse;
    @Inject
    DmkAPI dmkAPI;
    @BindView(R.id.image_splash)
    ImageView imageSplash;
    @BindView(R.id.text_version)
    CustomTextView textVersion;
    private SharedPreferences fcmSharedPrefrences;
    private TelephonyManager telephonyManager;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getSharedPreferences(DmkConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        deepLinkData = getIntent().getData();

        if (sharedPreferences.getInt(DmkConstants.SELECTEDLANGUAGES, 1) == 0) {
            language = "en";
            LanguageHelper.setLanguage(getContext(), language);
        } else {
            language = "ta";
            LanguageHelper.setLanguage(getContext(), language);

        }
        editor.putString(DmkConstants.DEVICEID, Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID)).commit();
        editor.putString(DmkConstants.APPID, getApplication().getPackageName()).commit();
        editor.putString(DmkConstants.APPVERSION_CODE, String.valueOf(BuildConfig.VERSION_NAME)).commit();
        editor.putString(DmkConstants.APPVERSION, String.valueOf(BuildConfig.VERSION_NAME)).commit();
        editor.putInt(DmkConstants.OS_VERSION, Build.VERSION.SDK_INT).commit();
        fcmSharedPrefrences.getString(DmkConstants.FCMTOKEN, "");
        textVersion.setText("Version-" + sharedPreferences.getString(DmkConstants.APPVERSION, ""));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for sInstance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

//        switch (sharedPreferences.getInt(DmkConstants.LOGINSTATUS, 0)) {
//            case 0:
//                //New User
//                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
//                if (deepLinkData != null) {
//                    if (deepLinkData.getPath().equalsIgnoreCase("/timeline"))
//                        intent.putExtra("timeLineID", deepLinkData.getQueryParameter("id"));
//                    else if (deepLinkData.getPath().equalsIgnoreCase("/news"))
//                        intent.putExtra("newsID", deepLinkData.getQueryParameter("id"));
//                    else if (deepLinkData.getPath().equalsIgnoreCase("/event"))
//                        intent.putExtra("eventID", deepLinkData.getQueryParameter("id"));
//
//                }
//                startActivity(intent);
//                finish();
//                break;
//            case 1:
//                // Existing User
//                getUserDetails();
//                break;
//            default:
//                Intent intent1 = new Intent(SplashActivity.this, LoginActivity.class);
//                if (deepLinkData != null) {
//                    if (deepLinkData.getPath().equalsIgnoreCase("/timeline"))
//                        intent1.putExtra("timeLineID", deepLinkData.getQueryParameter("id"));
//                    else if (deepLinkData.getPath().equalsIgnoreCase("/news"))
//                        intent1.putExtra("newsID", deepLinkData.getQueryParameter("id"));
//                    else if (deepLinkData.getPath().equalsIgnoreCase("/event"))
//                        intent1.putExtra("eventID", deepLinkData.getQueryParameter("id"));
//                }
//                startActivity(intent1);
//                finish();
//                break;
//        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    if (deepLinkData != null) {
                        String id = deepLinkData.getPathSegments().get(1);
                        String ps2 = id;
                        byte[] tmp2 = Base64.decode(ps2);
                        tmp2 = Base64.decode(tmp2);
                        id = new String(tmp2, "UTF-8");
                        id = String.valueOf(Integer.parseInt(id.replaceAll("[\\D]", "")));
                        if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("timeline"))
                            intent.putExtra("timeLineID", id);
                        else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("news"))
                            intent.putExtra("newsID", id);
                        else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("events"))
                            intent.putExtra("eventID", id);
                        else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("articles"))
                            intent.putExtra("articleID", id);
                    }
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

//    private void Validate() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(1000);
////                    switch (sharedPreferences.getInt(DmkConstants.LOGINSTATUS, 0)) {
////                        case 0:
////                            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
////                            if (deepLinkData != null) {
////                                String id = deepLinkData.getPathSegments().get(1);
////                                String ps2 = id;
////                                byte[] tmp2 = Base64.decode(ps2);
////                                tmp2 = Base64.decode(tmp2);
////                                id = new String(tmp2, "UTF-8");
////                                id = String.valueOf(Integer.parseInt(id.replaceAll("[\\D]", "")));
////                                if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("timeline"))
////                                    intent.putExtra("timeLineID", id);
////                                else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("news"))
////                                    intent.putExtra("newsID", id);
////                                else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("events"))
////                                    intent.putExtra("eventID", id);
////                                else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("articles"))
////                                    intent.putExtra("articleID", id);
////
////                            }
////                            startActivity(intent);
////                            finish();
////                            break;
////                        case 1:
////                            Intent intent2 = new Intent(SplashActivity.this, HomeActivity.class);
////                            if (deepLinkData != null) {
////                                String id = deepLinkData.getPathSegments().get(1);
////                                String ps2 = id;
////                                byte[] tmp2 = Base64.decode(ps2);
////                                tmp2 = Base64.decode(tmp2);
////                                id = new String(tmp2, "UTF-8");
////                                id = String.valueOf(Integer.parseInt(id.replaceAll("[\\D]", "")));
////                                if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("timeline"))
////                                    intent2.putExtra("timeLineID", id);
////                                else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("news"))
////                                    intent2.putExtra("newsID", id);
////                                else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("events"))
////                                    intent2.putExtra("eventID", id);
////                                else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("articles"))
////                                    intent2.putExtra("articleID", id);
////                            }
////                            startActivity(intent2);
////                            finish();
////                            break;
////                        default:
////                            Intent intent1 = new Intent(SplashActivity.this, LoginActivity.class);
////                            if (deepLinkData != null) {
////                                String id = deepLinkData.getPathSegments().get(1);
////                                String ps2 = id;
////                                byte[] tmp2 = Base64.decode(ps2);
////                                tmp2 = Base64.decode(tmp2);
////                                id = new String(tmp2, "UTF-8");
////                                id = String.valueOf(Integer.parseInt(id.replaceAll("[\\D]", "")));
////                                if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("timeline"))
////                                    intent1.putExtra("timeLineID", id);
////                                else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("news"))
////                                    intent1.putExtra("newsID", id);
////                                else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("events"))
////                                    intent1.putExtra("eventID", id);
////                                else if (deepLinkData.getPathSegments().get(0).equalsIgnoreCase("articles"))
////                                    intent1.putExtra("articleID", id);
////                            }
////                            startActivity(intent1);
////                            finish();
////                            break;
////                    }
//
//                    finish();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }).start();
//    }

//    private void getUserDetails() {
//        if (Util.isNetworkAvailable()) {
////            showProgress();
//            UserStatusParam userStatusParam = new UserStatusParam();
//            userStatusParam.setPhoneNumber(sharedPreferences.getString(DmkConstants.MOBILENUMBER, null));
//            userStatusParam.setDeviceToken(fcmSharedPrefrences.getString(DmkConstants.FCMTOKEN, ""));
//            dmkAPI.getUserDetails(userStatusParam).enqueue(new Callback<UserDetailsResponse>() {
//                @Override
//                public void onResponse(Call<UserDetailsResponse> call, Response<UserDetailsResponse> response) {
//                    userDetailsResponse = response.body();
//                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
//                        if (userDetailsResponse.getResults() != null) {
//                            editor.putString(DmkConstants.LASTNAME, userDetailsResponse.getResults().getLastName());
//                            editor.putString(DmkConstants.MOBILENUMBER, userDetailsResponse.getResults().getPhoneNumber());
//                            editor.putInt(DmkConstants.USERID, userDetailsResponse.getResults().getId());
//                            editor.putString(DmkConstants.USERDISTRICTNAME, userDetailsResponse.getResults().getDistrictName());
//                            editor.putString(DmkConstants.USERWINGNAME, userDetailsResponse.getResults().getWings().getWingName());
//                            editor.putString(DmkConstants.DESGINATION, userDetailsResponse.getResults().getRoles().getRolename());
//                            editor.putString(DmkConstants.UNIQUEDMKID, userDetailsResponse.getResults().getUniquieID());
//                            editor.putInt(DmkConstants.USERAGE, userDetailsResponse.getResults().getAge());
//                            editor.putInt(DmkConstants.SUPERIORID, userDetailsResponse.getResults().getSuperiorUserID());
//                            editor.putInt(DmkConstants.USER_DISTRICT_ID, userDetailsResponse.getResults().getDistrictID());
//                            editor.putInt(DmkConstants.USER_PARTY_DISTRICT_ID, userDetailsResponse.getResults().getPartydistrict());
//                            editor.putInt(DmkConstants.USER_CONSTITUENCY_ID, userDetailsResponse.getResults().getConstituencyID());
//                            editor.putInt(DmkConstants.USER_DIVISION_ID, userDetailsResponse.getResults().getTypeid());
//                            editor.putInt(DmkConstants.USER_PART_ID, userDetailsResponse.getResults().getPartid());
//                            //editor.putInt(DmkConstants.USER_VATTAM_ID, userDetailsResponse.getResults().getVattamid());
//                            editor.putInt(DmkConstants.USER_WARD_ID, userDetailsResponse.getResults().getWardID());
//                            editor.putInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, userDetailsResponse.getResults().getVillagepanchayat());
//                            editor.putInt(DmkConstants.USER_PANCHAYAT_UNION_ID, userDetailsResponse.getResults().getPanchayatunion());
//                            editor.putInt(DmkConstants.USER_TOWNSHIP_ID, userDetailsResponse.getResults().getTownshipid());
//                            editor.putInt(DmkConstants.USER_BOOTH_ID, userDetailsResponse.getResults().getBoothID());
//                            editor.putInt(DmkConstants.DESIGNATIONID, userDetailsResponse.getResults().getRoles().getLevel());
//                            editor.putInt(DmkConstants.USERTTYPE, userDetailsResponse.getResults().getUserType());
//                            editor.putString(DmkConstants.PROFILE_PICTURE, userDetailsResponse.getResults().getAvatar());
//                            editor.putString(DmkConstants.USERPERMISSION_COMMON, userDetailsResponse.getResults().getUserPermissionset().getCommon());
//                            editor.putString(DmkConstants.USERPERMISSION_ARTICLE, userDetailsResponse.getResults().getUserPermissionset().getArticle());
//                            editor.putString(DmkConstants.USERPERMISSION_EVENTS, userDetailsResponse.getResults().getUserPermissionset().getEvents());
//                            editor.putString(DmkConstants.USERPERMISSION_NEWS, userDetailsResponse.getResults().getUserPermissionset().getNews());
//                            editor.putString(DmkConstants.USERPERMISSION_ELECTION, userDetailsResponse.getResults().getUserPermissionset().getElection());
//                            editor.putString(DmkConstants.USER_VOTER_ID, userDetailsResponse.getResults().getVoterID());
//                            editor.putString(DmkConstants.USER_ADDRESS, userDetailsResponse.getResults().getAddress());
//                            editor.putInt(DmkConstants.USER_UNION_TYPE_ID, userDetailsResponse.getResults().getUniontype());
//                            editor.commit();
//                            Validate();
//                        } else {
//                            Toast.makeText(SplashActivity.this, userDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//
//                    } else {
//                        Toast.makeText(SplashActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//
//                @Override
//                public void onFailure(Call<UserDetailsResponse> call, Throwable t) {
//
//                    Toast.makeText(SplashActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
//
//                }
//            });
//        } else {
//            Toast.makeText(SplashActivity.this, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
//        }
//    }
}
