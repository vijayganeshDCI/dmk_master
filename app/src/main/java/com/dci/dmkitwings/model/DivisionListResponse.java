package com.dci.dmkitwings.model;

import java.util.List;

public class DivisionListResponse {
	private String Status;
	private List<DivisionListResultsItem> Results;

	public String getStatus() {
		return Status;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<DivisionListResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<DivisionListResultsItem> results) {
		Results = results;
	}

	private String Message;

}