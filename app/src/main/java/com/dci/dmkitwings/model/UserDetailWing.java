package com.dci.dmkitwings.model;

public class UserDetailWing {
	private int Status;
	private String updated_at;

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getWingName() {
		return WingName;
	}

	public void setWingName(String wingName) {
		WingName = wingName;
	}

	public String getWingDescription() {
		return WingDescription;
	}

	public void setWingDescription(String wingDescription) {
		WingDescription = wingDescription;
	}

	private String created_at;
	private int id;
	private String WingName;
	private String WingDescription;
}
