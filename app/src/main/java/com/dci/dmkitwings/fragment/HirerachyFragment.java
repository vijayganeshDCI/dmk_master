package com.dci.dmkitwings.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.adapter.HirerachyAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.HirerachyResponse;
import com.dci.dmkitwings.model.HirerachyResultsItem;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 4/13/2018.
 */

public class HirerachyFragment extends BaseFragment {


    @BindView(R.id.list_hirerachy)
    ListView listHirerachy;
    @BindView(R.id.cons_hirerachy)
    ConstraintLayout consHirerachy;
    List<HirerachyResultsItem> memberLists;
    HirerachyAdapter hirerachyAdapter;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    HirerachyResponse hirerachyResponse;
    BaseActivity baseActivity;
    @BindView(R.id.image_no_network)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_network)
    CustomTextView textNoNetwork;
    @BindView(R.id.swipe_hirerachy)
    SwipeRefreshLayout swipeHirerachy;
    private SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hirerachy, container, false);
        ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        memberLists = new ArrayList<HirerachyResultsItem>();
        editor = sharedPreferences.edit();
        baseActivity = (BaseActivity) getActivity();
        swipeHirerachy.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeHirerachy.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getParentHirerachyList();
                        swipeHirerachy.setRefreshing(false);
                    }
                }, 000);
            }
        });

        getParentHirerachyList();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void getParentHirerachyList() {
        TimeLineDelete timeLineDelete = new TimeLineDelete();
        timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            showProgress();
            memberLists.clear();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getParentHirerachyList(headerMap, timeLineDelete).enqueue(new Callback<HirerachyResponse>() {
                @Override
                public void onResponse(Call<HirerachyResponse> call, Response<HirerachyResponse> response) {
                    hideProgress();
                    hirerachyResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (hirerachyResponse.getStatuscode() == 0) {
                            if (isAdded()) {
                                imageNoNetwork.setVisibility(View.GONE);
                                textNoNetwork.setVisibility(View.GONE);
                                listHirerachy.setVisibility(View.VISIBLE);
                            }
                            if (hirerachyResponse.getResults().size() > 0) {
                                for (int i = 0; i < hirerachyResponse.getResults().size(); i++) {
                                    memberLists.add(new HirerachyResultsItem(
                                            hirerachyResponse.getResults().get(i).getDesignation() != null ?
                                                    hirerachyResponse.getResults().get(i).getDesignation() : "",
                                            hirerachyResponse.getResults().get(i).getFirstName() != null ?
                                                    hirerachyResponse.getResults().get(i).getFirstName() : "",
                                            hirerachyResponse.getResults().get(i).getLevel(),
                                            hirerachyResponse.getResults().get(i).getId(),
                                            hirerachyResponse.getResults().get(i).getLastName() != null ?
                                                    hirerachyResponse.getResults().get(i).getLastName() : "",
                                            hirerachyResponse.getResults().get(i).getAvatar() != null ?
                                                    hirerachyResponse.getResults().get(i).getAvatar() : "",
                                            hirerachyResponse.getResults().get(i).getUserType(), false, true));
                                }
                            }
                            memberLists.add((memberLists.size() == 0 ? 0 : memberLists.size()), new HirerachyResultsItem(
                                    sharedPreferences.getString(DmkConstants.DESGINATION, ""),
                                    sharedPreferences.getString(DmkConstants.FIRSTNAME, ""),
                                    sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0),
                                    sharedPreferences.getInt(DmkConstants.USERID, 0),
                                    sharedPreferences.getString(DmkConstants.LASTNAME, ""),
                                    sharedPreferences.getString(DmkConstants.PROFILE_PICTURE, ""),
                                    sharedPreferences.getInt(DmkConstants.USERTTYPE, 0), true, true));

                            hirerachyAdapter = new HirerachyAdapter(getActivity(), memberLists, baseActivity,
                                    HirerachyFragment.this);
                            listHirerachy.setAdapter(hirerachyAdapter);
                            listHirerachy.requestFocus();
                            listHirerachy.setItemsCanFocus(true);
                            listHirerachy.setSelection(memberLists.size() - 1);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, hirerachyResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, hirerachyResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(hirerachyResponse.getSource(),
                                    hirerachyResponse.getSourcedata()));
                            editor.commit();
                            getParentHirerachyList();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<HirerachyResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            imageNoNetwork.setVisibility(View.VISIBLE);
            textNoNetwork.setVisibility(View.VISIBLE);
            imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
            textNoNetwork.setText(R.string.no_network);
            listHirerachy.setVisibility(View.GONE);
        }
    }

    public void subMemberListView(String designation, String firstName,
                                  int level, int id, String lastName, String avatar, int userType, int parentID) {

        boolean newUser = true;

        int position = -1;//check parent position
        for (int i = 0; i < memberLists.size(); i++) {
            if (memberLists.get(i).getId() == id) {
                newUser = false;
                break;
            }
        }
        if (newUser) {
            for (int i = 0; i < memberLists.size(); i++)//memberlist
            {
                for (int j = 0; j < memberLists.get(i).getSubMember().size(); j++)//get submember from memberlist
                {

                    for (int l = 0; l < memberLists.size(); l++) {//check all memberlist with submemberlist
                        if (memberLists.get(i).getSubMember().get(j).getId() == memberLists.get(l).getId()) {//submember act as parent

                            for (HirerachyResultsItem items : memberLists.get(i).getSubMember()) {//check hierarchy of submember
                                if (id == items.getId()) {//new user is same hierarchy which already open
                                    position = l - 1;//get hierarchy position
                                    break;
                                }
                            }
                        }
                    }
                }
                if (position >= 0)
                    break;
            }
        }
        Log.e("pos", "" + position);
        if (newUser && position >= 0) {
            for (int k = 0; k < memberLists.size(); k++) {
                if (k > position) {
                    memberLists.remove(k);//remove memberlist based on new hierarchy
                    k--;
                }
            }
        }
        if (newUser)
            memberLists.add(new HirerachyResultsItem(
                    designation, firstName, level, id, lastName, avatar, userType, true, false));
        hirerachyAdapter.notifyDataSetChanged();


    }
}
