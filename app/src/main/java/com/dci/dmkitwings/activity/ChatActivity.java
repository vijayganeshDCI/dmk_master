package com.dci.dmkitwings.activity;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.SelectedImageAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.SelectedImage;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;


public class ChatActivity extends BaseActivity {
    LinearLayout layout;
    RelativeLayout layout_2;
    ImageView sendButton, attachMedia;
    EditText messageArea;
    ScrollView scrollView;
    Firebase firebaseReference1, firebaseReference2;
    String chatWith;
    String senderUserName;
    String fireBaseChatDataBaseURL;
    int chatWithUserID, senderUserID;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    DmkAPI dmkAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_chat, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        frameLayoutNotification.setVisibility(View.GONE);
        //Navigation Control
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        layout = (LinearLayout) findViewById(R.id.layout1);
        layout_2 = (RelativeLayout) findViewById(R.id.layout2);
        sendButton = (ImageView) findViewById(R.id.image_comment_send);
        attachMedia = (ImageView) findViewById(R.id.image_attach_file);
        messageArea = (EditText) findViewById(R.id.edit_write_comment);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        Intent intent = getIntent();
        if (getIntent() != null) {
            chatWith = intent.getStringExtra("chatWith");
            chatWithUserID = intent.getIntExtra("chatWithUserID", 0);
        }
        textTitle.setText(chatWith);
        senderUserName = sharedPreferences.getString(DmkConstants.FIRSTNAME, "") + "" +
                sharedPreferences.getString(DmkConstants.LASTNAME, "");

        senderUserID = sharedPreferences.getInt(DmkConstants.USERID, 0);
        fireBaseChatDataBaseURL = "https://dmkitwings-66ae4.firebaseio.com/chatMessages/";
        Firebase.setAndroidContext(this);

        firebaseReference1 = new Firebase(fireBaseChatDataBaseURL +
                senderUserName + senderUserID + "to" + chatWith + chatWithUserID);
        firebaseReference2 = new Firebase(fireBaseChatDataBaseURL +
                chatWith + chatWithUserID + "to" + senderUserName + senderUserID);

        scrollView.fullScroll(View.FOCUS_DOWN);
        attachMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageArea.getText().toString();
                if (!messageText.equals("")) {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("message", messageText);
                    map.put("user", senderUserName + senderUserID);
                    firebaseReference1.push().setValue(map);
                    firebaseReference2.push().setValue(map);
                    messageArea.setText("");
                }
            }
        });

        firebaseReference1.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map map = dataSnapshot.getValue(Map.class);
                String message = map.get("message").toString();
                String userName = map.get("user").toString();
                scrollView.fullScroll(View.FOCUS_DOWN);
                if (userName.equalsIgnoreCase(senderUserName + senderUserID)) {
                    addMessageBox("You: " + message, 1);
                } else {
                    addMessageBox(chatWith + ": " + message, 2);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    public void addMessageBox(String message, int type) {
        TextView textView = new TextView(ChatActivity.this);
        textView.setText(message);

        LinearLayout.LayoutParams lp2 = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        lp2.weight = 1.0f;

        if (type == 1) {
            lp2.gravity = Gravity.LEFT;
            textView.setTextColor(getResources().getColor(R.color.white));
//            textView.setBackgroundResource(R.drawable.bubble_in);
        } else {
            lp2.gravity = Gravity.RIGHT;
            textView.setTextColor(getResources().getColor(R.color.black));
//            textView.setBackgroundResource(R.drawable.bubble_out);
        }
        textView.setLayoutParams(lp2);
        layout.addView(textView);
        scrollView.fullScroll(View.FOCUS_DOWN);
    }


    private void imageFromGalleryResult(Intent data) {
        if (data.getData() != null) {
            Uri mImageUri = data.getData();
            if (mImageUri != null) {
            }
        }
//        else if (data.getClipData() != null) {
//            ClipData mClipData = data.getClipData();
//            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
//            for (int i = 0; i < mClipData.getItemCount(); i++) {
//                ClipData.Item item = mClipData.getItemAt(i);
//                Uri uri = item.getUri();
//                mArrayUri.add(uri);
//                if (uri != null) {
//                    selectedImageList.add(new SelectedImage(null, 1,
//                            compressInputImage(uri, this), false, System.currentTimeMillis() + ".jpg"));
//                }
//            }
//
//        }
    }


}