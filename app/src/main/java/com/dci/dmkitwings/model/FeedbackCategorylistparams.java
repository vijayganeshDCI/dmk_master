package com.dci.dmkitwings.model;

public class FeedbackCategorylistparams
{
    private int userid;
    private int type;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
