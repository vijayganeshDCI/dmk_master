package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.adapter.CommentAdapter;
import com.dci.dmkitwings.adapter.CommentListAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.CommentList;
import com.dci.dmkitwings.model.CommentListInput;
import com.dci.dmkitwings.model.CommentListResponse;
import com.dci.dmkitwings.model.CommentPost;
import com.dci.dmkitwings.model.CommentPostResponse;
import com.dci.dmkitwings.model.UserError;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomEditText;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentFragment extends BaseFragment {

    CommentAdapter commentListAdapter;
    List<CommentList> commentList;
    CommentListResponse commentListResponse;
    UserError userError;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    DmkAPI dmkAPI;
    @BindView(R.id.list_comment_list)
    RecyclerView listCommentList;
    @BindView(R.id.cons_comment)
    ConstraintLayout consComment;
    Unbinder unbinder;
    @BindView(R.id.swipe_comment)
    SwipeRefreshLayout swipeComment;
    int timeLineID;
    @BindView(R.id.edit_write_comment)
    CustomEditText editWriteComment;
    @BindView(R.id.image_comment_send)
    ImageView imageCommentSend;
    CommentPostResponse commentPostResponse;
    @BindView(R.id.image_no_comments)
    ImageView imageNoComments;
    @BindView(R.id.text_no_comm)
    CustomTextView textNoComm;
    BaseActivity baseActivity;
    LinearLayoutManager linearLayoutManager;
    private boolean isLastPage = false;
    private int currentPage = 0;
    boolean lastEnd;
    private boolean isLoading = false;
    private SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comment, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        commentList = new ArrayList<CommentList>();
        baseActivity = (BaseActivity) getActivity();
        editor = sharedPreferences.edit();
        Intent bundle = getActivity().getIntent();
        if (bundle != null)
            timeLineID = bundle.getIntExtra("timeLineID", 0);

        swipeComment.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeComment.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            currentPage = 0;
                            commentList.clear();
                            lastEnd = false;
                            getCommentList();
                            swipeComment.setRefreshing(false);
                        }
                    }
                }, 0);
            }
        });

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        listCommentList.setItemAnimator(new DefaultItemAnimator());
        listCommentList.setLayoutManager(linearLayoutManager);
        commentListAdapter = new CommentAdapter(getActivity(), commentList, CommentFragment.this,
                baseActivity, timeLineID);
        listCommentList.setAdapter(commentListAdapter);
        listCommentList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= 18) {
                        if (!lastEnd) {
                            currentPage = currentPage + 20;
                            getCommentList();
                        }
                    }
                }

            }


        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        getCommentList();
    }

    public void getCommentList() {
        CommentListInput commentListInput = new CommentListInput();
        commentListInput.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        commentListInput.setTimelineid(timeLineID);
        commentListInput.setOffset(currentPage);
        commentListInput.setLimit(20);
        if (currentPage == 0)
            commentList.clear();
        ///commentList.clear();
        if (Util.isNetworkAvailable()) {
            showProgress();
            //commentList.clear();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getCommentList(headerMap, commentListInput).enqueue(new Callback<CommentListResponse>() {
                @Override
                public void onResponse(Call<CommentListResponse> call, Response<CommentListResponse> response) {
                    hideProgress();
                    commentListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && commentListResponse != null) {
                        if (commentListResponse.getStatuscode() == 0) {
                            if (commentListResponse.getResults().size() > 0) {
                                imageNoComments.setVisibility(View.GONE);
                                textNoComm.setVisibility(View.GONE);
                                listCommentList.setVisibility(View.VISIBLE);
                                for (int i = 0; i < commentListResponse.getResults().size(); i++) {
                                    commentList.add(new CommentList(
                                            commentListResponse.getResults().get(i).getUser().getFirstname() != null ?
                                                    commentListResponse.getResults().get(i).getUser().getFirstname() : null
                                                    + " " + commentListResponse.getResults().get(i).getUser().getLastname() != null ?
                                                    commentListResponse.getResults().get(i).getUser().getLastname() : null,
                                            commentListResponse.getResults().get(i).getUser().getImage() != null ?
                                                    commentListResponse.getResults().get(i).getUser().getImage() : null,
                                            commentListResponse.getResults().get(i).getComment() != null ?
                                                    commentListResponse.getResults().get(i).getComment() : null,
                                            commentListResponse.getResults().get(i).getId() != 0 ?
                                                    commentListResponse.getResults().get(i).getId() : 0,
                                            timeLineID != 0 ? timeLineID : 0,
                                            commentListResponse.getResults().get(i).getUpdated_at() != null ?
                                                    getDateFormat(commentListResponse.getResults().get(i).getUpdated_at()) : null,
                                            commentListResponse.getResults().get(i).getUser().getId() != 0 ?
                                                    commentListResponse.getResults().get(i).getUser().getId() : 0,
                                            commentListResponse.getResults().get(i).getUser().getDesignation() != null ?
                                                    commentListResponse.getResults().get(i).getUser().getDesignation() : null,
                                            commentListResponse.getResults().get(i).getUser().getLevel() != 0 ?
                                                    commentListResponse.getResults().get(i).getUser().getLevel() : 0,
                                            commentListResponse.getResults().get(i).getUser().getDistrict() != null ?
                                                    commentListResponse.getResults().get(i).getUser().getDistrict() : null,
                                            commentListResponse.getResults().get(i).getUser().getConstituency() != null ?
                                                    commentListResponse.getResults().get(i).getUser().getConstituency() : null,
                                            commentListResponse.getResults().get(i).getUser().getWard() != null ?
                                                    commentListResponse.getResults().get(i).getUser().getWard() : null,
                                            commentListResponse.getResults().get(i).getUser().getPart() != null ?
                                                    commentListResponse.getResults().get(i).getUser().getPart() : null,
                                            commentListResponse.getResults().get(i).getUser().getBooth() != null ?
                                                    commentListResponse.getResults().get(i).getUser().getBooth() : null,
                                            commentListResponse.getResults().get(i).getUser().getVillage() != null ?
                                                    commentListResponse.getResults().get(i).getUser().getVillage() : null
                                    ));
                                }
                                Collections.sort(commentList, new Comparator<CommentList>() {
                                    public int compare(CommentList obj1, CommentList obj2) {
                                        // ## Ascending order
                                        //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                        // ## Descending order
                                        return obj1.getDate().compareToIgnoreCase(obj2.getDate()); // To compare string values
                                        // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                    }
                                });

                                commentListAdapter.notifyDataSetChanged();

                            } else {
                                if (commentList.size() == 0) {
                                    imageNoComments.setVisibility(View.VISIBLE);
                                    textNoComm.setVisibility(View.VISIBLE);
                                    imageNoComments.setImageResource(R.mipmap.icon_no_commets);
                                    textNoComm.setText(R.string.no_comments);
                                    listCommentList.setVisibility(View.GONE);
                                } else {
                                    lastEnd = true;
                                }
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, commentListResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, commentListResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(commentListResponse.getSource(),
                                    commentListResponse.getSourcedata()));
                            editor.commit();
                            getCommentList();
                        }
                    } else {
                        if (commentList.size() == 0) {
                            imageNoComments.setVisibility(View.VISIBLE);
                            textNoComm.setVisibility(View.VISIBLE);
                            imageNoComments.setImageResource(R.mipmap.icon_no_commets);
                            textNoComm.setText(R.string.no_comments);
                            listCommentList.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommentListResponse> call, Throwable t) {
                    hideProgress();
                    if (commentList.size() == 0) {
                        imageNoComments.setVisibility(View.VISIBLE);
                        textNoComm.setVisibility(View.VISIBLE);
                        imageNoComments.setImageResource(R.mipmap.icon_no_commets);
                        textNoComm.setText(R.string.no_comments);
                        listCommentList.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            imageNoComments.setVisibility(View.VISIBLE);
            textNoComm.setVisibility(View.VISIBLE);
            imageNoComments.setImageResource(R.mipmap.icon_no_network);
            textNoComm.setText(R.string.no_network);
            listCommentList.setVisibility(View.GONE);
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    private void postComment() {
        CommentPost commentPost = new CommentPost();
        commentPost.setComment(editWriteComment.getText().toString());
        commentPost.setParentid(0);
        commentPost.setTimelineid(timeLineID);
        commentPost.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.postComment(headerMap, commentPost).enqueue(new Callback<CommentPostResponse>() {
                @Override
                public void onResponse(Call<CommentPostResponse> call, Response<CommentPostResponse> response) {
                    hideProgress();
                    commentPostResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200
                            && commentPostResponse != null) {
                        if (commentPostResponse.getStatuscode() == 0) {
                            imageNoComments.setVisibility(View.GONE);
                            textNoComm.setVisibility(View.GONE);
                            listCommentList.setVisibility(View.VISIBLE);
                            commentList.add(new CommentList(
                                    sharedPreferences.getString(DmkConstants.FIRSTNAME, "")
                                            + " " + sharedPreferences.getString(DmkConstants.LASTNAME, ""),
                                    sharedPreferences.getString(DmkConstants.PROFILE_PICTURE, ""),
                                    commentPostResponse.getResults().getComment() != null ?
                                            commentPostResponse.getResults().getComment() : null,
                                    commentPostResponse.getResults().getId() != 0 ?
                                            commentPostResponse.getResults().getId() : 0,
                                    commentPostResponse.getResults().getTimeLineID() != null ?
                                            Integer.parseInt(commentPostResponse.getResults().getTimeLineID()) : null,
                                    commentPostResponse.getResults().getUpdated_at() != null ?
                                            commentPostResponse.getResults().getUpdated_at() : null,
                                    commentPostResponse.getResults().getPostedUserID() != null ?
                                            Integer.parseInt(commentPostResponse.getResults().getPostedUserID()) : null,
                                    sharedPreferences.getString(DmkConstants.DESGINATION, ""),
                                    sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0),
                                    "", "", "", "", "", ""));
//                        commentListAdapter = new CommentAdapter(getActivity(), commentList, CommentFragment.this,
//                                baseActivity, timeLineID);
//                        listCommentList.setAdapter(commentListAdapter);
                            commentListAdapter.notifyDataSetChanged();
//                        getCommentList();
                            editWriteComment.setText("");
                            listCommentList.scrollToPosition(listCommentList.getAdapter().getItemCount() - 1);
                            Toast.makeText(getActivity(), "Post Success", Toast.LENGTH_SHORT).show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, commentPostResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, commentPostResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(commentPostResponse.getSource(),
                                    commentPostResponse.getSourcedata()));
                            editor.commit();
                            postComment();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Post Failed", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }


    @OnClick(R.id.image_comment_send)
    public void onViewClicked() {
        if (editWriteComment.getText().toString().isEmpty())
            editWriteComment.setError(getString(R.string.empty_comment));
        else
            postComment();

    }
}
