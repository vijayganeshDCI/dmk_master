package com.dci.dmkitwings.model;

public class UserStatusResultsItem {
	private int id;
	private String LastName;
	private String FirstName;
	private String PhoneNumber;
	private String UniquieID;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getUniquieID() {
		return UniquieID;
	}

	public void setUniquieID(String uniquieID) {
		UniquieID = uniquieID;
	}
}
