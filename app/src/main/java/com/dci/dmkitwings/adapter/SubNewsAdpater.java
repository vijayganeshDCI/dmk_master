package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.NewsComposeActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.SubNewsFragment;
import com.dci.dmkitwings.model.NewsDeletePostParams;
import com.dci.dmkitwings.model.NewsDeleteResponse;
import com.dci.dmkitwings.model.NewsListResultsItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Java_AES_Cipher;
import com.dci.dmkitwings.utils.Util;
import com.firebase.client.utilities.Base64;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 4/9/2018.
 */

public class SubNewsAdpater extends BaseAdapter {
    private SharedPreferences.Editor editor;

    public SubNewsAdpater(ArrayList<NewsListResultsItem> newsListResultsItems, Context context, BaseActivity baseActivity, SubNewsFragment subNewsFragment) {
        this.newsListResultsItems = newsListResultsItems;
        this.context = context;
        this.baseActivity = baseActivity;
        this.subNewsFragment = subNewsFragment;
        DmkApplication.getContext().getComponent().inject(this);
    }

    ArrayList<NewsListResultsItem> newsListResultsItems;
    Context context;
    BaseActivity baseActivity;
    SubNewsFragment subNewsFragment;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    NewsDeleteResponse newsDeleteResponse;

    @Override
    public int getCount() {
        return newsListResultsItems.size();
    }

    @Override
    public NewsListResultsItem getItem(int position) {
        return newsListResultsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        editor = sharedPreferences.edit();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_sub_news, null);
        }

        ImageView imageArtPicture = (ImageView) convertView.findViewById(R.id.image_art_pic);
        ImageView imageShare = (ImageView) convertView.findViewById(R.id.image_news_share);
        TextView textArtTitle = (TextView) convertView.findViewById(R.id.text_art_title);
        TextView textArtSubTitle = (TextView) convertView.findViewById(R.id.text_art_sub_title);
        TextView textArtPostedDate = (TextView) convertView.findViewById(R.id.text_art_posted_date);
        final ImageView imagemenu = (ImageView) convertView.findViewById(R.id.image_menu);

        universalImageLoader(imageArtPicture, context.getString(R.string.s3_bucket_news_path),
                newsListResultsItems.get(position).getMediapath().size() > 0
                        && newsListResultsItems.get(position).getMediapath() != null ?
                        newsListResultsItems.get(position).getMediapath().get(0) : " ", R.mipmap.icon_loading_100x100,
                R.mipmap.icon_no_image_100x100);

        textArtPostedDate.setText(getDateFormat(newsListResultsItems.get(position).getCreated_at()));
        textArtTitle.setText(newsListResultsItems.get(position).getTitle());
        textArtSubTitle.setText(newsListResultsItems.get(position).getContent());
        if (newsListResultsItems.get(position).getUser().getId() == sharedPreferences.getInt(DmkConstants.USERID, 0)) {
            imagemenu.setVisibility(View.VISIBLE);

        } else {
            imagemenu.setVisibility(View.GONE);
        }
        imagemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, imagemenu);
                //inflating menu_icon from xml resource
                popup.inflate(R.menu.comment_item_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit:
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setMessage(R.string.confirm_to_edit);
                                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(context, NewsComposeActivity.class);
                                        intent.putExtra("newsID", newsListResultsItems.get(position).getId());
                                        intent.putExtra("newsTitle", newsListResultsItems.get(position).getTitle());
                                        intent.putExtra("newsContent", newsListResultsItems.get(position).getContent());
                                        intent.putExtra("newsDistrict", newsListResultsItems.get(position).getDistrictID());
                                        intent.putExtra("newsConstituency", newsListResultsItems.get(position).getConstituencyID());
                                        intent.putExtra("Newstypeid", newsListResultsItems.get(position).getNewstypeid());
                                        intent.putExtra("Newstype", newsListResultsItems.get(position).getNewstype());
                                        intent.putExtra("fromNews", true);
                                        subNewsFragment.startActivityForResult(intent, 1);
                                    }

                                });
                                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                                return true;
                            case R.id.delete:
                                //handle menu2 click
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(context);
                                alertDialog1.setMessage(R.string.confirm_to_delete);
                                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        deleteNews(newsListResultsItems.get(position).getId());
                                    }

                                });
                                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog1.show();
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
            }
        });

        imageShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ps = "Dmk_encryption " + newsListResultsItems.get(position).getId();
                String tmp = Base64.encodeBytes(ps.getBytes());
                tmp = Base64.encodeBytes(tmp.getBytes());
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, newsListResultsItems.get(position).getTitle() + "\n" + context.getString(R.string.for_more) +
                        context.getString(R.string.deeplink_url) + "news/" + tmp);
                context.startActivity(Intent.createChooser(i, "Choose share option"));
            }
        });
        return convertView;
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }

    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd-MMM-yyyy hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    private void deleteNews(final int newsID) {
        NewsDeletePostParams newsDeletePostParams = new NewsDeletePostParams();
        newsDeletePostParams.setNewsid(newsID);
        newsDeletePostParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.deletenews(headerMap, newsDeletePostParams).enqueue(new Callback<NewsDeleteResponse>() {
                @Override
                public void onResponse(Call<NewsDeleteResponse> call, Response<NewsDeleteResponse> response) {
                    newsDeleteResponse = response.body();
                    baseActivity.hideProgress();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (newsDeleteResponse.getStatuscode() == 0) {
                            if (newsDeleteResponse.getStatus().contains("Success")) {
                                Toast.makeText(context, context.getString(R.string.news_delete_sucuess), Toast.LENGTH_SHORT).show();
                                subNewsFragment.getNewsList();
                            } else {
                                Toast.makeText(context, context.getString(R.string.news_delete_failed), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, newsDeleteResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, newsDeleteResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(newsDeleteResponse.getSource(),
                                    newsDeleteResponse.getSourcedata()));
                            editor.commit();
                            deleteNews(newsID);
                        }
                    } else {
                        Toast.makeText(context, context.getString(R.string.news_delete_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<NewsDeleteResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public String getEncodedHeader(String sourceResponse, String sourceDataResponse) {

        byte[] sourceBase64 = android.util.Base64.decode(sourceResponse, android.util.Base64.NO_WRAP);
        byte[] sourceDataBase64 = android.util.Base64.decode(sourceDataResponse, android.util.Base64.NO_WRAP);
        String source = new String(sourceBase64, StandardCharsets.UTF_8);
        String sourceData = new String(sourceDataBase64, StandardCharsets.UTF_8);
        String data = source.substring(0, 12) + sourceData.substring(0, 8);
        String key = source.substring(12, 19) + sourceData.substring(8, 13);
        String iv = source.substring(19, 28) + sourceData.substring(13, 20);
        String encrpytData = Java_AES_Cipher.encrypt(key, iv, data);
        byte[] dataBase64Encode = encrpytData.getBytes(StandardCharsets.UTF_8);
        String base64 = android.util.Base64.encodeToString(dataBase64Encode, android.util.Base64.NO_WRAP);
        return base64;
    }
}
