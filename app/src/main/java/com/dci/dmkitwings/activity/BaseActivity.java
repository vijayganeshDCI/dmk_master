package com.dci.dmkitwings.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.util.IOUtils;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.NavDrawerListViewAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.AlertDialogFragment;
import com.dci.dmkitwings.model.NavDrawerItem;
import com.dci.dmkitwings.model.UserError;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Java_AES_Cipher;
import com.dci.dmkitwings.utils.UiUtils;
import com.dci.dmkitwings.view.CustomTextView;
import com.google.firebase.database.DatabaseReference;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import cn.jzvd.JZVideoPlayer;

@SuppressWarnings("ResourceType")
public class BaseActivity extends AppCompatActivity {

    private static final float PICTURE_SIZE = 640;
    protected Handler mHandler = new Handler();
    protected ProgressDialog mDialog;
    public static final int ERROR_SHOW_TYPE_LOG = 0;
    public static final int ERROR_SHOW_TYPE_TOAST = 1;
    public static final int ERROR_SHOW_TYPE_DIALOG = 2;
    public static final String DIALOG_API_ERROR_TAG = "apiTag";
    @Inject
    public DmkAPI dmkAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    int REQUEST_STORAGE = 1;
    String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_SMS,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECORD_AUDIO};
    UserError userError;
    private static final float END_SCALE = 0.7f;
    public TextView textTitle;
    public ConstraintLayout consToolBarElements;
    public Toolbar toolbarHome;
    public ListView mDrawerList;
    public DrawerLayout mDrawerLayout;
    public RelativeLayout relativeHome;
    public String[] mPlanetTitles;
    public TypedArray mDrawerIcon;
    public ArrayList<NavDrawerItem> navDrawerItems;
    public NavDrawerListViewAdapter adapter;
    public ActionBarDrawerToggle mDrawerToggle;
    public TextView userNameNavHeader, designationNavHeader, userPhoneNumberNavHeader;
    ImageView imageProPic;
    ViewGroup header;
    public ImageView userprofileedit, imageProPicEdit;
    public ImageView image_notification;
    public TextView textcart_badge;
    public FrameLayout frameLayoutNotification;
    public ConstraintLayout constNavHeader;
    View view;
    public AmazonS3Client s3Client;
    public BasicAWSCredentials credentials;
    public TransferUtility transferUtility;
    public String[] imageFormat = {"tif", "tiff", "gif", "jpeg", "jpg", "jif", "jfif"
            , "jp2", "jpx", "j2k", "j2c"
            , "fpx", "pcd", "png"};
    public String[] audioFormat = {"aa", "aac", "aax", "act", "aiff", "amr", "ape", "au", "awb"
            , "amr", "ape", "au", "awb", "dct", "dss", "dvf", "dvf", "flac", "gsm", "iklax", "ivs", "m4a", "m4b",
            "m4p", "mmf", "mp3", "mpc", "msv", "nsf", "ogg", "opus", "ra", "raw", "sln", "tta"
            , "vox", "wav", "wma", "webm", "8svx"};
    public String[] audioimageFormat = {"tif", "tiff", "gif", "jpeg", "jpg", "jif", "jfif"
            , "jp2", "jpx", "j2k", "j2c"
            , "fpx", "pcd", "png", "pdf", "aa", "aac", "aax", "act", "aiff", "amr", "ape", "au", "awb"
            , "amr", "ape", "au", "awb", "dct", "dss", "dvf", "dvf", "flac", "gsm", "iklax", "ivs", "m4a", "m4b",
            "m4p", "mmf", "mp3", "mpc", "msv", "nsf", "ogg", "opus", "ra", "raw", "sln", "tta"
            , "vox", "wav", "wma", "webm", "8svx"};
    private final String expression = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";
    public ImageView imageViewNavToggle, imageBaseProPic;
    private DatabaseReference mFirebaseDatabaseReferenceInbox;
    private DatabaseReference messagesRef;
    public String RECENT_CHAT = "RECENT_CHAT_MESSAGES";
    private String receiverUserName;
    private int receiverUserID;
    private String RECENT_LIST_CHILD;
    public CountDownTimer cTimer = null;
    private LogoutReceiver logoutReceiver;
    public SwitchCompat switchSendToLevelTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_base);
        DmkApplication.getContext().getComponent().inject(this);
        mDialog = new ProgressDialog(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        }
        handleTimer();

        textTitle = (TextView) findViewById(R.id.text_title);
        imageViewNavToggle = (ImageView) findViewById(R.id.image_nav_toogle);
        imageBaseProPic = (ImageView) findViewById(R.id.image_base_pro_pic);
//        consToolBarElements = (ConstraintLayout) findViewById(R.id.cons_tool_bar_elements);
        toolbarHome = (Toolbar) findViewById(R.id.toolbar_home);
        mDrawerList = (ListView) findViewById(R.id.nav_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_home);
        relativeHome = (RelativeLayout) findViewById(R.id.relative_home);
        editor = sharedPreferences.edit();
        userError = new UserError();

        setSupportActionBar(toolbarHome);
        //Navigation Control
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(true);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        mPlanetTitles = getResources().getStringArray(R.array.nav_listitem);
        mDrawerIcon = getResources().obtainTypedArray(R.array.nav_listicon);
        LayoutInflater nav_inflater = getLayoutInflater();

        header = (ViewGroup) nav_inflater.inflate(R.layout.nav_header, mDrawerList, false);

        userNameNavHeader = (TextView) header.findViewById(R.id.text_nav_header_user_name);
        userPhoneNumberNavHeader = (TextView) header.findViewById(R.id.text_nav_header_user_phone_number);
        designationNavHeader = (TextView) header.findViewById(R.id.text_nav_header_desigantion);
        userprofileedit = (ImageView) header.findViewById(R.id.image_profile_edit);
        imageProPicEdit = (ImageView) header.findViewById(R.id.image_profile_pic_nav);
        universalImageLoader(imageProPicEdit, getString(R.string.s3_bucket_profile_path),
                sharedPreferences.getString(DmkConstants.PROFILE_PICTURE, ""), R.mipmap.icon_pro_image_loading_256,
                R.mipmap.icon_pro_image_loading_256);
        frameLayoutNotification = (FrameLayout) findViewById(R.id.framelayout_notification);
        image_notification = (ImageView) findViewById(R.id.image_notification);
        textcart_badge = (TextView) findViewById(R.id.cart_badge);
        switchSendToLevelTwo = (SwitchCompat) findViewById(R.id.switch_send_to_level_2);


        mDrawerList.addHeaderView(header, null, false);
        navDrawerItems = new ArrayList<NavDrawerItem>();
        toolbarHome.setNavigationIcon(R.mipmap.icon_nav_menu_toggle_dmk);
        //Home
        navDrawerItems.add(new NavDrawerItem(mPlanetTitles[0], mDrawerIcon.getResourceId(0, 0), 0));
        //History
        navDrawerItems.add(new NavDrawerItem(mPlanetTitles[1], mDrawerIcon.getResourceId(1, 0), 0));
        // Messages
        navDrawerItems.add(new NavDrawerItem(mPlanetTitles[2], mDrawerIcon.getResourceId(2, 0), 0));
        // Hirerachy
        navDrawerItems.add(new NavDrawerItem(mPlanetTitles[3], mDrawerIcon.getResourceId(3, 0), 0));
        //Complaint
        navDrawerItems.add(new NavDrawerItem(mPlanetTitles[4], mDrawerIcon.getResourceId(4, 0), 0));
        // identity card
        navDrawerItems.add(new NavDrawerItem(mPlanetTitles[5], mDrawerIcon.getResourceId(5, 0), 0));
        // feed back
        navDrawerItems.add(new NavDrawerItem(mPlanetTitles[6], mDrawerIcon.getResourceId(6, 0), 0));
        // settings
        navDrawerItems.add(new NavDrawerItem(mPlanetTitles[7], mDrawerIcon.getResourceId(7, 0), 0));

        userNameNavHeader.setText(sharedPreferences.getString(DmkConstants.FIRSTNAME, null));
        userPhoneNumberNavHeader.setText(sharedPreferences.getString(DmkConstants.PHONENUMBER, null));
        designationNavHeader.setText(sharedPreferences.getString(DmkConstants.DESGINATION, null));
        textTitle.setText(sharedPreferences.getString(DmkConstants.USERWINGNAME, null));
        adapter = new NavDrawerListViewAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                //setLocale("ta");
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                //setLocale("ta");
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                //setLocale("ta");
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                //setLocale("ta");
            }
        });
        userprofileedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(BaseActivity.this, UserProfileActivity.class);
                intent.putExtra("name", "navi");
//              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);


            }
        });
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(BaseActivity.this, UserProfileActivity.class);
                intent.putExtra("name", "navi");
//              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        credentials = new BasicAWSCredentials(getString(R.string.aws_access_key),
                getString(R.string.aws_secret_key));
        s3Client = new AmazonS3Client(credentials);
        transferUtility =
                TransferUtility.builder()
                        .context(this)
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();

//        mFirebaseDatabaseReferenceInbox = FirebaseDatabase.getInstance().getReference();
//        receiverUserName = sharedPreferences.getString(DmkConstants.FIRSTNAME, "") + "" +
//                sharedPreferences.getString(DmkConstants.LASTNAME, "");
//        receiverUserID = sharedPreferences.getInt(DmkConstants.USERID, 0);
//        RECENT_LIST_CHILD = receiverUserName + receiverUserID;
//        messagesRef = mFirebaseDatabaseReferenceInbox.child(RECENT_CHAT).child("Inbox")
//                .child(RECENT_LIST_CHILD);
//        messagesRef.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot snapshot) {
//                //Old User
//                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
//                    if (dataSnapshot.child("readStatus").getValue().equals(0)) {
//                        navDrawerItems.get(2).setChatReadStatus(0);
//                    }else {
//                        navDrawerItems.get(2).setChatReadStatus(1);
//                    }
//                    adapter.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        //Broadcast receiver
        logoutReceiver = new LogoutReceiver();
        IntentFilter intentFilter = new IntentFilter("forceLogout");
        registerReceiver(logoutReceiver, intentFilter);

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimer();
        unregisterReceiver(logoutReceiver);
    }


    @Override
    public void onBackPressed() {
        if (JZVideoPlayer.backPress()) {
            return;
        } else if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        JZVideoPlayer.releaseAllVideos();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mDialog != null) {
            mDialog.dismiss();
        }
        JZVideoPlayer.releaseAllVideos();
        stopTimer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (item.getItemId() == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[2] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[3] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[4] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[5] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[6] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[7] == PackageManager.PERMISSION_GRANTED) {
                //Storage permission is enabled
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    && ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.READ_SMS)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.CAMERA)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.READ_PHONE_STATE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.RECORD_AUDIO)

                    ) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActivity.this);
                alertDialog.setMessage(R.string.location_from_dialog);
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermission();
                    }
                });
                alertDialog.show();

            } else {
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActivity.this);
//                alertDialog.setMessage(R.string.location_settings);
//                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        Intent intent = new Intent();
//                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                        Uri uri = Uri.fromParts("package", BaseActivity.this.getPackageName(), null);
//                        intent.setData(uri);
//                        startActivity(intent);
//                    }
//                });
//                alertDialog.show();
            }
        }

    }


    // API Call handling
    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int messageId) {
        showProgress(getString(messageId));
    }

    public void showProgress(final CharSequence message) {

        if (getApplicationContext() != null) {
            mHandler.post(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    UiUtils.hideKeyboard(BaseActivity.this);
                    //mDialog.setMessage(message);
                    //mDialog.setIndeterminate(true);
                    CustomTextView text_progressupdate;
                    mDialog = new ProgressDialog(BaseActivity.this);
                    mDialog.getWindow().setBackgroundDrawable(new
                            ColorDrawable(android.graphics.Color.TRANSPARENT));
                    mDialog.setIndeterminate(true);
                    mDialog.setCancelable(false);
                    mDialog.show();
                    mDialog.setContentView(R.layout.custom_progress_view);
                    text_progressupdate = mDialog.findViewById(R.id.text_progressupdate);
                    text_progressupdate.setText(message);


                }
            });
        }
    }

    public void hideProgress() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDialog.dismiss();
            }
        });
    }

    public void showError(int showType, UserError error) {
        switch (showType) {
            case ERROR_SHOW_TYPE_LOG:
                Log.e("BaseActivity", "Error title : " + error.title + "; Message : " + error.message);
                break;
            case ERROR_SHOW_TYPE_TOAST:
                if (!TextUtils.isEmpty(error.message)) {
                    Toast.makeText(this, error.message, Toast.LENGTH_LONG).show();
                }
                break;
            case ERROR_SHOW_TYPE_DIALOG:
                dismissDialogFragment(DIALOG_API_ERROR_TAG);
                getSupportFragmentManager().beginTransaction()
                        .add(AlertDialogFragment.newOkDialog(error.title, error.message), DIALOG_API_ERROR_TAG)
                        .commitAllowingStateLoss();
                break;
        }
    }

    public void dismissDialogFragment(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(tag);
        if (fragment != null && fragment.isVisible()) {
            fragment.dismissAllowingStateLoss();
        }
    }


    private void checkPermissions() {
        if (!hasPermissionGranted()) {
            requestPermission();
        }
    }

    public boolean hasPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) ==
                        PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) ==
                        PackageManager.PERMISSION_GRANTED;
    }

    public boolean hasLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_STORAGE);
        }
    }

    public void checkLocationPermission() {
        if (this != null && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    public static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    public String getBase64Image(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                new RectF(0, 0, PICTURE_SIZE, PICTURE_SIZE),
                Matrix.ScaleToFit.CENTER);
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] bytes = stream.toByteArray();
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


    //    public void picasoImageLoader(ImageView imageView, String url, Context context) {
//        Picasso.with(context)
//                .load(getString(R.string.falcon_image_url) + url)
//                .placeholder(R.mipmap.img_sample_pro_pic)   // optional
////                                    .error(R.drawable.ic_error_fallback)      // optional
////                                    .resize(250, 200)                        // optional
////                                    .rotate(90)                             // optional
//                .into(imageView);
//    }
//
    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(getString(R.string.s3_image_url_download) + s3bucketName + image, imageView, options);
    }

    //For Single Images
    public static void compressInputImage(Intent data, Context context, ImageView newIV) {
        Bitmap bitmap;
        Uri inputImageData = data.getData();
        try {
            Bitmap bitmapInputImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), inputImageData);
            if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1920, 1200, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, bitmapInputImage.getWidth(), bitmapInputImage.getHeight(), true);
                newIV.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    //For Multiple Images
    public static Bitmap compressInputImage(Uri uri, Context context) {
        Bitmap bitmap = null;
        try {
            Bitmap bitmapInputImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
            } else if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1920, 1200, true);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, bitmapInputImage.getWidth(), bitmapInputImage.getHeight(), true);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }
        return bitmap;
    }

    public Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o2);
    }


    // UPDATED!
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
//        mDrawerToggle.syncState();
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
            switch (position) {
//                Home
                case 1:
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            Intent intent = new Intent(BaseActivity.this, HomeActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }, 100);


                    break;
//                    History
                case 2:
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(BaseActivity.this, HistoryActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }, 100);
                    break;
//                    Message
                case 3:
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(BaseActivity.this, HomeMessageActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }, 100);
                    break;
//                    Hirerachy
                case 4:
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(BaseActivity.this, HirerachyActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }, 100);
                    break;
//                    complaints
                case 5:
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(BaseActivity.this, ComplaintActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }, 100);
                    break;

                case 6:
                    //                    identity card
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(BaseActivity.this, UserProfileActivity.class);
                            intent.putExtra("name", "base");

//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }, 100);
                    break;
                case 7:
                    //feedback
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(BaseActivity.this, FeedBackActivity.class);


//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }, 100);
                    break;
                case 8:
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(BaseActivity.this, SettingsActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        }
                    }, 100);
                    //settings
                    break;

            }
        }
    }


    private void selectItem(int position) {
        // Highlight the selected item, update the title, and close the drawer
        mDrawerList.setItemChecked(position, true);
        // setTitle(mPlanetTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd-MMM-yyyy hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getCurrentTimeandDate() {
        Date currentDate = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        String currentTimeandTime = simpleDateFormat.format(currentDate);
        return currentTimeandTime;
    }

    //s3 upload image
    public void uploadFile(Uri uri, String s3bucketPath, String imageName, boolean isAudioFromRecord) {
        if (uri != null) {
            final File destination;
            if (!isAudioFromRecord)
                destination = new File(getExternalFilesDir(null), imageName);
            else
                destination = new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString() +
                        "/Voice Recorder/RECORDING_"
                        + imageName);
            createFile(getApplicationContext(), uri, destination);

            TransferObserver uploadObserver =
                    transferUtility.upload(s3bucketPath + imageName, destination);
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.COMPLETED == state) {
                        destination.delete();
                    } else if (TransferState.FAILED == state) {
                        destination.delete();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                }

                @Override
                public void onError(int id, Exception ex) {
                    ex.printStackTrace();
                }

            });
        }
    }

    //to get path from uri
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    // to get URI from bitmap
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        String path = null;
        Uri pathURI = null;
        try {
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        path = MediaStore.Images.Media.insertImage
                (inContext.getContentResolver(), inImage, "Title", null);
        try {
            pathURI = Uri.parse(path);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pathURI;
    }

    //to get file name from URI
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    // to get file type
    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }


    public boolean isImageFile(String fileNamePath) {
        for (int i = 0; i < imageFormat.length; i++) {
            if (getFileExt(fileNamePath).equalsIgnoreCase(imageFormat[i])) {
                return true;
            }
        }
        return false;
    }

    public boolean isAudioFile(String fileNamePath) {
        for (int i = 0; i < audioFormat.length; i++) {
            if (getFileExt(fileNamePath).equalsIgnoreCase(audioFormat[i])) {
                return true;
            }
        }
        return false;
    }

    // to create file
    public void createFile(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public static boolean isAudioFile(String path) {
//        String mimeType = URLConnection.guessContentTypeFromName(path);
//        return mimeType != null && mimeType.endsWith(".mp3");
//    }

    public InputFilter editTextTitleLength() {

        InputFilter setLength = new InputFilter.LengthFilter(100);
        return setLength;
    }

    public InputFilter editTextContenLength() {

        InputFilter setLength = new InputFilter.LengthFilter(500);
        return setLength;
    }

    public InputFilter editTextArticleTitleLength() {

        InputFilter setLength = new InputFilter.LengthFilter(150);
        return setLength;
    }

    public InputFilter editTextArticleContenLength() {

        InputFilter setLength = new InputFilter.LengthFilter(1000);
        return setLength;
    }

    //getvideo size from URI
    private static final DecimalFormat format = new DecimalFormat("#.##");
    private static final long MiB = 1024 * 1024;
    private static final long KiB = 1024;

    public boolean getFileSize(Uri file) {
        File path = null;

        path = new File(file.getPath());


        long expectedSizeInMB = 20;
        long expectedSizeInBytes = 1024 * 1024 * expectedSizeInMB;

        long sizeInBytes = -1;
        sizeInBytes = path.length();

        if (sizeInBytes > expectedSizeInBytes) {
            return false;
        } else {
            return true;
        }

    }

    //get Image Path
    public String getImagePath(Uri uri) {
        String path = null;
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();

            cursor = getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();


        }

        return path;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        handleTimer();
    }

    //start timer
    public void handleTimer() {
        if (cTimer != null) {
            stopTimer();
            startTimer();
        } else {
            startTimer();
        }
    }

    //stop timer
    public void stopTimer() {
        if (cTimer != null)
            cTimer.cancel();
    }

    public void startTimer() {
//        Inactive interval duration 1 hour--3600000
        cTimer = new CountDownTimer(3600000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                Intent intent_admin_logout = new Intent(BaseActivity.this, LoginActivity.class);
//                sharedPreferences.edit().clear().commit();
                intent_admin_logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                finish();
                startActivity(intent_admin_logout);
                Toast.makeText(BaseActivity.this, "Time out", Toast.LENGTH_SHORT).show();
            }
        };
        cTimer.start();
    }

    public class LogoutReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("logout", false)) {
                Intent intent_admin_logout = new Intent(BaseActivity.this, LoginActivity.class);
//                sharedPreferences.edit().clear().commit();
                intent_admin_logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                finish();
                startActivity(intent_admin_logout);
            }
        }
    }

    public String getEncodedHeader(String sourceResponse, String sourceDataResponse) {

        if (sourceResponse != null && sourceDataResponse != null) {
            byte[] sourceBase64 = Base64.decode(sourceResponse, Base64.NO_WRAP);
            byte[] sourceDataBase64 = Base64.decode(sourceDataResponse, Base64.NO_WRAP);
            String source = new String(sourceBase64, StandardCharsets.UTF_8);
            String sourceData = new String(sourceDataBase64, StandardCharsets.UTF_8);
            String data = source.substring(0, 12) + sourceData.substring(0, 8);
            String key = source.substring(12, 19) + sourceData.substring(8, 13);
            String iv = source.substring(19, 28) + sourceData.substring(13, 20);
            String encrpytData = Java_AES_Cipher.encrypt(key, iv, data);
            byte[] dataBase64Encode;
            String base64 = null;
            if (encrpytData != null) {
                dataBase64Encode = encrpytData.getBytes(StandardCharsets.UTF_8);
                base64 = Base64.encodeToString(dataBase64Encode, Base64.NO_WRAP);
            }
            return base64;
        }
        return null;
    }

    public void playYouTubeVideo(final String videoID, YouTubePlayerView youTubePlayerView) {
        getLifecycle().addObserver(youTubePlayerView);
        youTubePlayerView.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        if (videoID.length() > 0) {
                            initializedYouTubePlayer.loadVideo(videoID, 0);
                        }

                    }
                });
            }
        }, true);
    }

    public String getVideoId(String videoUrl) {
        if (videoUrl == null || videoUrl.trim().length() <= 0) {
            return null;
        }
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(videoUrl);
        try {
            if (matcher.find())
                return matcher.group();
        } catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
