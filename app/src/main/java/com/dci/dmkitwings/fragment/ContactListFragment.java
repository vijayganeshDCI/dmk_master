package com.dci.dmkitwings.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.HapticFeedbackConstants;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ChatAct;
import com.dci.dmkitwings.activity.HomeMessageActivity;
import com.dci.dmkitwings.adapter.ContactListAdapter;
import com.dci.dmkitwings.adapter.HirerachyAdapter;
import com.dci.dmkitwings.adapter.ReecentChatListAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.ChatMessageParams;
import com.dci.dmkitwings.model.GetChatUserListResultsItem;
import com.dci.dmkitwings.model.HirerachyResponse;
import com.dci.dmkitwings.model.HirerachyResultsItem;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 4/12/2018.
 */

public class ContactListFragment extends BaseFragment {

    @BindView(R.id.cons_contact_list)
    ConstraintLayout consContactList;
    Unbinder unbinder;
    ContactListAdapter contactListAdapter;
    @BindView(R.id.list_user_list)
    ListView listUserList;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    @BindView(R.id.image_no_network)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_network)
    CustomTextView textNoNetwork;
    private Type type;
    private ArrayList<GetChatUserListResultsItem> memberListsFromFilter;
    private ArrayList<GetChatUserListResultsItem> selcetedMemberList;

    private String memberListsString;
    private boolean isMultipleUserSelected;
    private Vibrator vibe;
    private MenuItem newBroadcastList;
    private int selectedCount = 0;
    private HirerachyResponse hirerachyResponse;
    private SharedPreferences.Editor editor;


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        memberListsFromFilter = new ArrayList<GetChatUserListResultsItem>();
        editor = sharedPreferences.edit();
        selcetedMemberList = new ArrayList<GetChatUserListResultsItem>();
        type = new TypeToken<ArrayList<GetChatUserListResultsItem>>() {
        }.getType();
        Bundle bundle = getArguments();
        if (getArguments() != null) {
//            user list for level 1
            memberListsString = bundle.getString("chatUserList");
            memberListsFromFilter = new Gson().fromJson(memberListsString, type);
        } else {
//            user list for other than level 1
            getChildHirerachyList();
        }
        contactListAdapter = new ContactListAdapter(getActivity(), memberListsFromFilter);
        listUserList.setAdapter(contactListAdapter);
        listUserList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        vibe = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        setHasOptionsMenu(true);
//        listView.setItemsCanFocus(false);


        listUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int postion, long l) {
                if (!isMultipleUserSelected) {
                    Intent intent = new Intent(getActivity(), ChatAct.class);
                    intent.putExtra("chatWith", memberListsFromFilter.get(postion).getFirstName() != null ?
                            memberListsFromFilter.get(postion).getFirstName() : ""
                            + "" +
                            memberListsFromFilter.get(postion).getLastName() != null ?
                            memberListsFromFilter.get(postion).getLastName() : "");
                    intent.putExtra("chatWithUserID", memberListsFromFilter.get(postion).getId());
                    intent.putExtra("proPic", memberListsFromFilter.get(postion).getAvatar() != null ?
                            memberListsFromFilter.get(postion).getAvatar() : "");
                    intent.putExtra("memberID", memberListsFromFilter.get(postion).getUniquieID() != null ?
                            memberListsFromFilter.get(postion).getUniquieID() : "");
                    intent.putExtra("designation", memberListsFromFilter.get(postion).getRoles() != null ?
                            memberListsFromFilter.get(postion).getRoles().getRolename() : "");
                    intent.putExtra("fcmToken", memberListsFromFilter.get(postion).getDeviceToken() != null ?
                            memberListsFromFilter.get(postion).getDeviceToken() : "");
                    intent.putExtra("isFromContactList", true);
                    startActivity(intent);
                } else {
                    if (memberListsFromFilter.get(postion).getIsSelected() == 0) {
                        memberListsFromFilter.get(postion).setIsSelected(1);
                        selectedCount++;
                    } else {
                        memberListsFromFilter.get(postion).setIsSelected(0);
                        selectedCount--;
                        if (selectedCount == 0) {
                            newBroadcastList.setVisible(false);
                            isMultipleUserSelected = false;
                        }

                    }
                    contactListAdapter.notifyDataSetChanged();
                }

            }
        });

        listUserList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                vibe.vibrate(50);
                memberListsFromFilter.get(position).setIsSelected(1);
                contactListAdapter.notifyDataSetChanged();
                isMultipleUserSelected = true;
                newBroadcastList.setVisible(true);
                selectedCount++;
//                listUserList.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                return true;
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (selectedCount > 0) {
                        selectedCount = 0;
                        for (int i = 0; i < memberListsFromFilter.size(); i++) {
                            if (memberListsFromFilter.get(i).getIsSelected() == 1) {
                                memberListsFromFilter.get(i).setIsSelected(0);
                            }
                        }
                        contactListAdapter.notifyDataSetChanged();
                        newBroadcastList.setVisible(false);
                        isMultipleUserSelected = false;
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.new_broatcast, menu);
        newBroadcastList = menu.findItem(R.id.new_broadcast_list);
        newBroadcastList.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_broadcast_list:
                for (int i = 0; i < memberListsFromFilter.size(); i++) {
                    if (memberListsFromFilter.get(i).getIsSelected() == 1) {
                        selcetedMemberList.add(new GetChatUserListResultsItem(
                                memberListsFromFilter.get(i).getId(),
                                memberListsFromFilter.get(i).getLastName(),
                                memberListsFromFilter.get(i).getRoles(),
                                memberListsFromFilter.get(i).getFirstName(),
                                memberListsFromFilter.get(i).getAvatar(),
                                memberListsFromFilter.get(i).getUniquieID(),
                                memberListsFromFilter.get(i).getDeviceToken(),
                                memberListsFromFilter.get(i).getPhoneNumber(), 0));
                    }
                }
                Intent intent = new Intent(getActivity(), ChatAct.class);
                Gson gson = new Gson();
                String listValue = gson.toJson(selcetedMemberList);
                intent.putExtra("oneTomany", listValue);
                startActivity(intent);
                if (selectedCount > 0) {
                    selectedCount = 0;
                    for (int i = 0; i < memberListsFromFilter.size(); i++) {
                        if (memberListsFromFilter.get(i).getIsSelected() == 1) {
                            memberListsFromFilter.get(i).setIsSelected(0);
                        }
                    }
                    contactListAdapter.notifyDataSetChanged();
                    newBroadcastList.setVisible(false);
                    isMultipleUserSelected = false;
                    selcetedMemberList.clear();
                    return true;
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void getChildHirerachyList() {
        TimeLineDelete timeLineDelete = new TimeLineDelete();
        timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getParentChildList(headerMap, timeLineDelete).enqueue(new Callback<HirerachyResponse>() {
                @Override
                public void onResponse(Call<HirerachyResponse> call, Response<HirerachyResponse> response) {
                    hideProgress();
                    hirerachyResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (hirerachyResponse.getStatuscode() == 0) {
                            if (hirerachyResponse.getResults().size() > 0) {
                                imageNoNetwork.setVisibility(View.GONE);
                                textNoNetwork.setVisibility(View.GONE);
                                listUserList.setVisibility(View.VISIBLE);
                                for (int i = 0; i < hirerachyResponse.getResults().size(); i++) {
                                    memberListsFromFilter.add(new
                                            GetChatUserListResultsItem(
                                            hirerachyResponse.getResults().get(i).getId(),
                                            hirerachyResponse.getResults().get(i).getLastName(),
                                            hirerachyResponse.getResults().get(i).getRoles(),
                                            hirerachyResponse.getResults().get(i).getFirstName(),
                                            hirerachyResponse.getResults().get(i).getAvatar(),
                                            hirerachyResponse.getResults().get(i).getUniqueID(),
                                            hirerachyResponse.getResults().get(i).getDevicetoken(),
                                            hirerachyResponse.getResults().get(i).getPhonenumber(), 0
                                    ));
                                }
                                contactListAdapter.notifyDataSetChanged();


                            } else {
                                imageNoNetwork.setVisibility(View.VISIBLE);
                                textNoNetwork.setVisibility(View.VISIBLE);
                                imageNoNetwork.setImageResource(R.mipmap.icon_no_members);
                                textNoNetwork.setText(R.string.no_members);
                                listUserList.setVisibility(View.GONE);
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, hirerachyResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, hirerachyResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(hirerachyResponse.getSource(),
                                    hirerachyResponse.getSourcedata()));
                            editor.commit();
                            getChildHirerachyList();
                        }
                    } else {
                        imageNoNetwork.setVisibility(View.VISIBLE);
                        textNoNetwork.setVisibility(View.VISIBLE);
                        imageNoNetwork.setImageResource(R.mipmap.icon_no_members);
                        textNoNetwork.setText(R.string.no_members);
                        listUserList.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<HirerachyResponse> call, Throwable t) {
                    hideProgress();
                    imageNoNetwork.setVisibility(View.VISIBLE);
                    textNoNetwork.setVisibility(View.VISIBLE);
                    imageNoNetwork.setImageResource(R.mipmap.icon_no_members);
                    textNoNetwork.setText(R.string.no_members);
                    listUserList.setVisibility(View.GONE);
                }
            });
        } else {
            imageNoNetwork.setVisibility(View.VISIBLE);
            textNoNetwork.setVisibility(View.VISIBLE);
            imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
            textNoNetwork.setText(R.string.no_network);
            listUserList.setVisibility(View.GONE);
        }
    }
}
