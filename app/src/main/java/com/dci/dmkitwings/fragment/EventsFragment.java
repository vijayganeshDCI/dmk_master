package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.EventComposeActivity;
import com.dci.dmkitwings.activity.EventDetailsActivity;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.adapter.EventListAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.EventResponse;
import com.dci.dmkitwings.model.EventsResultsItem;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.RecyclerItemClickListener;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.wooplr.spotlight.utils.SpotlightSequence;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by vijayaganesh on 3/15/2018.
 */

public class EventsFragment extends BaseFragment {
    private static final String SHOWCASE_ID = "SHOWCASE_ID_EVENTS";
    @BindView(R.id.recycler_event)
    RecyclerView recycler_event;
    Unbinder unbinder;
    //    @BindView(R.id.text_month_event)
//    TextView textMonthEvent;
    @BindView(R.id.image_no_events)
    ImageView imageNoEvents;
    @BindView(R.id.text_no_events)
    CustomTextView textNoEvents;
    @BindView(R.id.cons_layout_events)
    ConstraintLayout consLayoutEvents;
    @BindView(R.id.float_event_compose)
    FloatingActionButton floatEventCompose;
    @BindView(R.id.co_home)
    CoordinatorLayout coHome;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.text_event_date)
    CustomTextView textEventDate;
    private EventListAdapter eventListAdapter;
    private HomeActivity homeActivity;
    private BaseActivity baseActivity;
    @BindView(R.id.compact_calendar_view)
    CompactCalendarView compactCalendarView;
    @BindView(R.id.eventsnestedscrrol)
    NestedScrollView nestedScrollView;

    private ArrayList<String> monthlist = new ArrayList<String>();

    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;
    EventResponse eventResponse;
    List<EventsResultsItem> eventsResultsItemList;
    Date currentDate, startDate, endDate;
    SimpleDateFormat simpleDateFormat;

    private Boolean isStarted = false;
    private Boolean isVisible = false;
    String eventStartDate, eventEndDate, selectedDate;
    private boolean isFromCalender = false;
    LinearLayoutManager linearLayoutManager;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = false;
    private Date selDate;

    @Override
    public void onStart() {
        super.onStart();
        if (selDate != null)
            currentDate = selDate;
        else
            currentDate = Calendar.getInstance().getTime();

        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        textEventDate.setText("Events on: " + simpleDateFormat.format(currentDate));
        compactCalendarView.setCurrentDate(currentDate);
        isStarted = true;
        if (isVisible) {
            isFromCalender = false;
//            showProgress();
            getEventListInCalendar();
            getEventList(currentDate);
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            compactCalendarView.setCurrentDate(currentDate);
            getEventList(currentDate);
            getEventListInCalendar();
        } else {
            currentDate = Calendar.getInstance().getTime();
            selDate = currentDate;
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        currentPage = 0;
        isLoading = false;
        lastEnd = false;
        unbinder.unbind();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events, container, false);

        unbinder = ButterKnife.bind(this, view);
        homeActivity = (HomeActivity) getActivity();
        baseActivity = (BaseActivity) getActivity();

        DmkApplication.getContext().getComponent().inject(this);
        eventsResultsItemList = new ArrayList<EventsResultsItem>();
        editor = sharedPreferences.edit();
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_event.setItemAnimator(new DefaultItemAnimator());
        recycler_event.setLayoutManager(linearLayoutManager);
        eventListAdapter = new EventListAdapter(eventsResultsItemList, getActivity(), baseActivity,
                EventsFragment.this);
        recycler_event.setAdapter(eventListAdapter);
        compactCalendarView.setFirstDayOfWeek(Calendar.MONDAY);
        compactCalendarView.setDayColumnNames(getResources().getStringArray(R.array.weekdays));
        compactCalendarView.setShouldDrawDaysHeader(true);
        compactCalendarView.setUseThreeLetterAbbreviation(true);


        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {

            @Override
            public void onDayClick(Date dateClicked) {
                isFromCalender = true;
                currentPage = 0;
                isLoading = false;
                lastEnd = false;
                eventsResultsItemList.clear();
                //textEventDate.setText("Events on : " + simpleDateFormat.format(currentDate));
                getEventList(dateClicked);
                selDate = dateClicked;
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
            }
        });


        if (sharedPreferences.getString(DmkConstants.USERPERMISSION_EVENTS, null).equals("0")) {
            floatEventCompose.setVisibility(View.GONE);
        }
        nestedScrollView.setSmoothScrollingEnabled(true);

        if (nestedScrollView != null) {
            nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight()
                                - v.getMeasuredHeight())) && scrollY > oldScrollY) {
                            if (!isLoading) {
                                if (!lastEnd) {
                                    if (eventsResultsItemList.size() >= 30) {
                                        currentPage = currentPage + 31;
                                        getEventList(currentDate);
                                    }

                                }
                            }
                        }
                    }
                }

            });

        }

        return view;
    }


    public long getTimeInmilliSeconds(String givenDateString) {
        long timeInMilliseconds = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd yyyy");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    public void getEventList(final Date currentDate) {
        if (isAdded())
            textEventDate.setText("Events on : " + simpleDateFormat.format(currentDate));
        if (Util.isNetworkAvailable()) {
            if (isFromCalender)
                showProgress();
            // eventsResultsItemList.clear();
            TimeLineDelete timeLineDelete = new TimeLineDelete();
            timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            timeLineDelete.setLimit(30);
            timeLineDelete.setOffset(currentPage);
            isLoading = true;
            if (currentPage == 0) {
                eventsResultsItemList.clear();
            }
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getEventList(headerMap, timeLineDelete).enqueue(new Callback<EventResponse>() {
                @Override
                public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                    isLoading = false;
                    hideProgress();
                    eventResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (eventResponse.getStatuscode() == 0) {
                            if (eventResponse.getResults().size() > 0) {
                                if (isAdded()) {
                                    imageNoEvents.setVisibility(View.GONE);
                                    textNoEvents.setVisibility(View.GONE);
                                    recycler_event.setVisibility(View.VISIBLE);
                                }

                                for (int i = 0; i < eventResponse.getResults().size(); i++) {
                                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                                    try {
                                        startDate = format1.parse(eventResponse.getResults().get(i).getStartDate() != null ?
                                                eventResponse.getResults().get(i).getStartDate() : "");
                                        endDate = format1.parse(eventResponse.getResults().get(i).getEndDate() != null ?
                                                eventResponse.getResults().get(i).getEndDate() : "");
                                        eventStartDate = simpleDateFormat.format(startDate);
                                        eventEndDate = simpleDateFormat.format(endDate);
                                        selectedDate = simpleDateFormat.format(currentDate);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    if (selectedDate.equalsIgnoreCase(eventStartDate) || selectedDate.equalsIgnoreCase(eventEndDate)
                                            || (currentDate.before(endDate) && currentDate.after(startDate))) {
                                        eventsResultsItemList.add(new EventsResultsItem(
                                                eventResponse.getResults().get(i).getId(),
                                                eventResponse.getResults().get(i).getName() != null ?
                                                        eventResponse.getResults().get(i).getName() : "",
                                                eventResponse.getResults().get(i).getDescription() != null ?
                                                        eventResponse.getResults().get(i).getDescription() : "",
                                                eventResponse.getResults().get(i).getMediafiles(),
                                                eventResponse.getResults().get(i).getTitleimage() != null ?
                                                        eventResponse.getResults().get(i).getTitleimage() : "",
                                                eventResponse.getResults().get(i).getStartDate() != null ?
                                                        getDateFormat(eventResponse.getResults().get(i).getStartDate()) : "",
                                                eventResponse.getResults().get(i).getEndDate() != null ?
                                                        getDateFormat(eventResponse.getResults().get(i).getEndDate()) : "",
                                                eventResponse.getResults().get(i).getCreatedBy(),
                                                eventResponse.getResults().get(i).getDistrictID(),
                                                eventResponse.getResults().get(i).getSharecount()));


                                    }
                                    //
                                }
                                if (eventsResultsItemList.size() > 0) {
                                    //Collections.reverse(eventsResultsItemList);
                                    isLoading = false;
                                    eventListAdapter.notifyDataSetChanged();
                                } else {
                                    if (eventsResultsItemList.size() == 0) {
                                        if (isAdded()) {
                                            imageNoEvents.setVisibility(View.VISIBLE);
                                            textNoEvents.setVisibility(View.VISIBLE);
                                            textNoEvents.setText(R.string.no_events);
                                            imageNoEvents.setImageResource(R.mipmap.icon_no_events);
                                            recycler_event.setVisibility(View.GONE);
                                        }
                                    } else {
                                        lastEnd = true;
                                    }
                                }

                            } else {
                                //                            eventResponse.getResults().size() == 0
                                if (eventsResultsItemList.size() == 0) {
                                    if (isAdded()) {
                                        imageNoEvents.setVisibility(View.VISIBLE);
                                        textNoEvents.setVisibility(View.VISIBLE);
                                        recycler_event.setVisibility(View.GONE);
                                        textNoEvents.setText(R.string.no_events);
                                        imageNoEvents.setImageResource(R.mipmap.icon_no_events);
                                    }
                                } else {

                                    lastEnd = true;

                                }
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, eventResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, eventResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(eventResponse.getSource(),
                                    eventResponse.getSourcedata()));
                            editor.commit();
                            getEventList(currentDate);
                        }
                    } else {
                        // response.body()!=200
                        if (eventsResultsItemList.size() == 0) {
                            if (isAdded()) {
                                imageNoEvents.setVisibility(View.VISIBLE);
                                textNoEvents.setVisibility(View.VISIBLE);
                                recycler_event.setVisibility(View.GONE);
                                textNoEvents.setText(R.string.no_events);
                                imageNoEvents.setImageResource(R.mipmap.icon_no_events);
                            }
                        } else {

                            lastEnd = true;

                        }
                    }

                }

                @Override
                public void onFailure(Call<EventResponse> call, Throwable t) {
                    hideProgress();
                    if (eventsResultsItemList.size() == 0) {
                        if (isAdded()) {
                            isLoading = false;
                            imageNoEvents.setVisibility(View.VISIBLE);
                            textNoEvents.setVisibility(View.VISIBLE);
                            recycler_event.setVisibility(View.GONE);
                            textNoEvents.setText(R.string.no_events);
                            imageNoEvents.setImageResource(R.mipmap.icon_no_events);
                        }

                    }
                }
            });
        } else {
            hideProgress();
            if (isAdded()) {
                imageNoEvents.setVisibility(View.VISIBLE);
                textNoEvents.setVisibility(View.VISIBLE);
                recycler_event.setVisibility(View.GONE);
                imageNoEvents.setImageResource(R.mipmap.icon_no_network);
                textNoEvents.setText(R.string.no_network);
            }
        }

    }

    public void getEventListInCalendar() {
        if (Util.isNetworkAvailable()) {
            TimeLineDelete timeLineDelete = new TimeLineDelete();
            timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getEventList(headerMap, timeLineDelete).enqueue(new Callback<EventResponse>() {
                @Override
                public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                    if (isAdded()) compactCalendarView.removeAllEvents();
                    eventResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (eventResponse.getStatuscode() == 0) {
                            if (eventResponse.getResults().size() > 0) {
                                for (int i = 0; i < eventResponse.getResults().size(); i++) {
                                    List<Date> betweenDates = getDates(
                                            eventResponse.getResults().get(i).getStartDate() != null ?
                                                    eventResponse.getResults().get(i).getStartDate() : "",
                                            eventResponse.getResults().get(i).getEndDate() != null ?
                                                    eventResponse.getResults().get(i).getEndDate() : "");
                                    List<Event> eventList = new ArrayList<Event>();
                                    eventList.clear();
                                    for (int j = 0; j < betweenDates.size(); j++) {
                                        eventList.add(new Event(
                                                Color.RED, betweenDates.get(j).getTime(), "Events"));
                                    }
                                    if (isAdded()) {
                                        compactCalendarView.addEvents(eventList);
                                    }
                                }

                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, eventResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, eventResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(eventResponse.getSource(),
                                    eventResponse.getSourcedata()));
                            editor.commit();
                            getEventListInCalendar();
                        }
                    }

                }

                @Override
                public void onFailure(Call<EventResponse> call, Throwable t) {
                    hideProgress();
                }
            });
        } else {
            hideProgress();
            if (isAdded()) {
                imageNoEvents.setVisibility(View.VISIBLE);
                textNoEvents.setVisibility(View.VISIBLE);
                recycler_event.setVisibility(View.GONE);
                imageNoEvents.setImageResource(R.mipmap.icon_no_network);
                textNoEvents.setText(R.string.no_network);
            }
        }

    }

    private static List<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }


    @OnClick(R.id.float_event_compose)
    public void onViewClicked() {
        Intent intent = new Intent(getActivity(), EventComposeActivity.class);
        startActivity(intent);
    }
}
