package com.dci.dmkitwings.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dci.dmkitwings.R;

/**
 * Created by vijayaganesh on 3/22/2018.
 */

public class CommonUserRegFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_common_user,container,false);
        return view;
    }
}
