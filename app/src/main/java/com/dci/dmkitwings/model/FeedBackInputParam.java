package com.dci.dmkitwings.model;

public class FeedBackInputParam
{
    private String title;
    private String categoryid;
    private String created_at;
    private int id;
    private int Userid;
    private String feedback;
    private String status;
    public FeedBackInputParam(int id,int userid , String feedback,  String categoryid, String status, String created_at) {
        this.title = title;
        this.categoryid = categoryid;
        this.created_at = created_at;
        this.id = id;
        this.Userid = userid;
        this.feedback=feedback;
        this.status=status;

    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSuperiorUserID() {
        return id;
    }

    public void setSuperiorUserID(int superiorUserID) {
        id = superiorUserID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserid() {
        return Userid;
    }

    public void setUserid(int userid) {
        Userid = userid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }






    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
