package com.dci.dmkitwings.model;

public class GetChatUserListRoles {
	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String rolename;
	private int id;
}
