package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.PollListResponse;
import com.dci.dmkitwings.model.PollListResultsItem;
import com.dci.dmkitwings.model.PollQuestionslistItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 4/16/2018.
 */

public class PollsListAdapter extends BaseAdapter {


    public PollsListAdapter(Context context,List<PollListResultsItem> pollListResponses) {
        this.pollListResponses=pollListResponses;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }




    Context context;
    LayoutInflater mInflater;
    List<PollListResultsItem> pollListResponses;
    @Override
    public int getCount() {
        return pollListResponses.size();
    }

    @Override
    public PollListResultsItem getItem(int position) {

        return pollListResponses.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_polls_name, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


            viewHolder.textPollTitle.setText(pollListResponses.get(position).getName());
            viewHolder.textPollDescription.setText(pollListResponses.get(position).getDescription());
            viewHolder.textPollDescription.setVisibility(View.GONE);






        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.text_polltitle)
        TextView textPollTitle;

        @BindView(R.id.text_polldescription)
        TextView textPollDescription;

        ConstraintLayout consItem;
//        @BindView(R.id.card_polls)
//        CardView cardPolls;
        @BindView(R.id.cons_item_polls)
        ConstraintLayout consItemPolls;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
