package com.dci.dmkitwings.model;

import java.util.List;

public class VillagePanchayatListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<VillagePanchayatResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<VillagePanchayatResultsItem> results) {
		Results = results;
	}

	private String Status;
	private List<VillagePanchayatResultsItem> Results;
	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}