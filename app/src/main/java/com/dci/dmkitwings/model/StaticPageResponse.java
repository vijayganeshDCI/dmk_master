package com.dci.dmkitwings.model;

public class StaticPageResponse{


	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}



	private String Status;

	public StaticPageResults getResults() {
		return Results;
	}

	public void setResults(StaticPageResults results) {
		Results = results;
	}

	private StaticPageResults Results;

	public int getStatuscode() {
		return Statuscode;
	}

	public void setStatuscode(int statuscode) {
		Statuscode = statuscode;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourcedata() {
		return sourcedata;
	}

	public void setSourcedata(String sourcedata) {
		this.sourcedata = sourcedata;
	}

	private int Statuscode;
	private int userid;
	private String source;
	private String sourcedata;
}
