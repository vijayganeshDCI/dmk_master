package com.dci.dmkitwings.model;

public class TownPanchayatResultsItem {
	public int getDistrictid() {
		return districtid;
	}

	public void setDistrictid(int districtid) {
		this.districtid = districtid;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getConstituencyid() {
		return constituencyid;
	}

	public void setConstituencyid(int constituencyid) {
		this.constituencyid = constituencyid;
	}

	public String getTownpanchayatname() {
		return townpanchayatname;
	}

	public void setTownpanchayatname(String townpanchayatname) {
		this.townpanchayatname = townpanchayatname;
	}

	public String getTownpanchayatdescription() {
		return townpanchayatdescription;
	}

	public void setTownpanchayatdescription(String townpanchayatdescription) {
		this.townpanchayatdescription = townpanchayatdescription;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	private int districtid;
	private String updated_at;
	private int constituencyid;

	public TownPanchayatResultsItem(String townpanchayatname, int id) {
		this.townpanchayatname = townpanchayatname;
		this.id = id;
	}

	private String townpanchayatname;
	private String townpanchayatdescription;
	private String created_at;
	private int id;
	private int status;
}
