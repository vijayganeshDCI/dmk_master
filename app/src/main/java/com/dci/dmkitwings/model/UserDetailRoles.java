package com.dci.dmkitwings.model;

public class UserDetailRoles {
    private int Status;
    //private int ParentID;
    private String rolecode;
    private String updated_at;
    private int level;
    private String rolename;
    private int WingID;

    public String getRoleDescription() {
        return RoleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        RoleDescription = roleDescription;
    }

    private String RoleDescription;
    private String created_at;
    private int id;
    private int DivisionID;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

//    public int getParentID() {
//        return ParentID;
//    }
//
//    public void setParentID(int parentID) {
//        ParentID = parentID;
//    }

    public String getRolecode() {
        return rolecode;
    }

    public void setRolecode(String rolecode) {
        this.rolecode = rolecode;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public int getWingID() {
        return WingID;
    }

    public void setWingID(int wingID) {
        WingID = wingID;
    }


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(int divisionID) {
        DivisionID = divisionID;
    }
}
