package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.activity.PollListActivity;
import com.dci.dmkitwings.adapter.PollsListAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.PollList;
import com.dci.dmkitwings.model.PollListResponse;
import com.dci.dmkitwings.model.PollListResultsItem;
import com.dci.dmkitwings.model.PollQuestionslistItem;
import com.dci.dmkitwings.model.Polls;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 4/16/2018.
 */

public class MyPollsFragment extends BaseFragment {

    @BindView(R.id.list_polls)
    ListView listPolls;
    @BindView(R.id.swipe_poll_list)
    SwipeRefreshLayout swipePollList;

    Unbinder unbinder;

    PollsListAdapter pollsListAdapter;
    List<Polls> pollsList;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    PollListResponse pollListResponse;

    ArrayList<PollQuestionslistItem> pollListResultsItemArrayList;
    List<PollListResultsItem> pollListResponses;
    HomeActivity homeActivity;
    @Nullable
    private Boolean isStarted=false;
    private Boolean isVisible=false;
    @BindView(R.id.image_no_polls)
    ImageView imageNoPolllist;
    @BindView(R.id.text_no_polls)
    CustomTextView textNoPolls;
    private int preLast;
    boolean lastEnd, onCreatebool=false;
    int totalcount=0,pagecount=0;
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
           getPollList();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_polls, container, false);
        DmkApplication.getContext().getComponent().inject(this);
        unbinder = ButterKnife.bind(this, view);
        homeActivity=(HomeActivity) getActivity();
        editor = sharedPreferences.edit();
        pollsList=new ArrayList<Polls>();
        pollListResultsItemArrayList = new ArrayList<PollQuestionslistItem>();
        pollListResponses=new ArrayList<PollListResultsItem>();
        pollsListAdapter = new PollsListAdapter(getActivity(), pollListResponses);
        listPolls.setAdapter(pollsListAdapter);
        listPolls.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getActivity(), PollListActivity.class);
                //intent.putExtra("polllist", pollListResultsItemArrayList);
                intent.putExtra("pollid",pollListResponse.getResults().get(position).getId());
                intent.putExtra("PollType",pollListResponse.getResults().get(position).getPolltype());
                pollListResponses.clear();
                startActivity(intent);


            }
        });

        swipePollList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipePollList.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            pagecount=0;
                            onCreatebool=false;
                            lastEnd=false;
                            preLast=0;
                            pollListResponses.clear();

                            getPollList();
                            swipePollList.setRefreshing(false);
                        }
                    }
                }, 1000);
            }
        });
        listPolls.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (listPolls.getLastVisiblePosition() - listPolls.getHeaderViewsCount() -
                        listPolls.getFooterViewsCount()) >= (pollsListAdapter.getCount() - 1)) {

                    // Now your listview has hit the bottom
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                final int lastItem = firstVisibleItem + visibleItemCount;


                if(lastItem == totalItemCount)
                {
                    if (pollListResponses.size()>=10) {
                        if (preLast != lastItem) {
                            try {
                                if (!lastEnd && onCreatebool) {
                                    pagecount = pagecount + 11;
                                    //LoadData(pagecount,"");

                                    getPollList();

                                    preLast = lastItem;
                                } else {
                                    //Log.d("Last", "fail");
                                }
                            } catch (Exception e) {
                            }

                        }
                    }

                }


            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();


    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
        getPollList();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        }

    private void getPollList()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            //pollListResponses.clear();
            final PollList pollListparams=new PollList();
            pollListparams.setUserid(sharedPreferences.getInt(DmkConstants.USERID,0));
            if (pagecount==0)
            {
                pollListResponses.clear();
            }
            pollListparams.setLimit(10);
            pollListparams.setOffset(pagecount);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getMypolls(headerMap,pollListparams).enqueue(new Callback<PollListResponse>() {
                @Override
                public void onResponse(Call<PollListResponse> call, Response<PollListResponse> response) {
                    hideProgress();
                    pollListResponse=response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        hideProgress();
                        if (pollListResponse.getStatuscode()==0) {
                            onCreatebool=true;
                            if (pollListResponse.getResults().size()>0) {
                                for (int u = 0; u < pollListResponse.getResults().size(); u++) {
                                    pollListResponses.add(pollListResponse.getResults().get(u));
                                }
                                pollsListAdapter.notifyDataSetChanged();
                                hideProgress();
                            }
                            else
                            {
                                if (pollListResponses.size()==0)
                                {
                                    imageNoPolllist.setVisibility(View.VISIBLE);
                                    imageNoPolllist.setImageResource(R.mipmap.icon_no_timeline);
                                    textNoPolls.setVisibility(View.VISIBLE);
                                    textNoPolls.setText(R.string.no_polllist);
                                    listPolls.setVisibility(View.GONE);
                                }
                                else
                                {
                                    lastEnd = true;
                                }


                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, pollListResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, pollListResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(pollListResponse.getSource(),
                                    pollListResponse.getSourcedata()));
                            editor.commit();
                            getPollList();
                        }


                    } else {

                        if (pollListResponses.size()==0)
                        {

                            Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                            imageNoPolllist.setVisibility(View.VISIBLE);
                            imageNoPolllist.setImageResource(R.mipmap.icon_no_timeline);
                            textNoPolls.setVisibility(View.VISIBLE);
                            textNoPolls.setText(R.string.no_polllist);
                            listPolls.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                        }
                        else
                        {

                            //Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<PollListResponse> call, Throwable t) {
                    hideProgress();
                    if (pollListResponses.size()==0)
                    {

                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                        imageNoPolllist.setVisibility(View.VISIBLE);
                        imageNoPolllist.setImageResource(R.mipmap.icon_no_timeline);
                        textNoPolls.setVisibility(View.VISIBLE);
                        textNoPolls.setText(R.string.no_polllist);
                        listPolls.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                    }


                }
            });
        }
        else
        {
            Toast.makeText(getContext(),getString(R.string.network_error_message),Toast.LENGTH_SHORT).show();
        }
    }




}
