package com.dci.dmkitwings.model;

public class PartyRoleList
{
    private  int wingID;
    private  int DivisionID;
    private  int UnionType;


    public int getUnionType() {
        return UnionType;
    }

    public void setUnionType(int unionType) {
        UnionType = unionType;
    }



    public int getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(int divisionID) {
        DivisionID = divisionID;
    }




    public int getwingID() {
        return wingID;
    }

    public void setwingID(int wingID) {
        this.wingID = wingID;
    }
}
