package com.dci.dmkitwings.model;

public class DistrictListResponse{
	private String Status;
	private DistrictListResults Results;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public DistrictListResults getResults() {
		return Results;
	}

	public void setResults(DistrictListResults results) {
		Results = results;
	}
	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}
