package com.dci.dmkitwings.model;

public class GetChatUserListParam {

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getDistrict() {
        return district;
    }

    public void setDistrict(int district) {
        this.district = district;
    }

    public int getConstituency() {
        return constituency;
    }

    public void setConstituency(int constituency) {
        this.constituency = constituency;
    }

    public int getPartydistrict() {
        return partydistrict;
    }

    public void setPartydistrict(int partydistrict) {
        this.partydistrict = partydistrict;
    }

    public int getDivision() {
        return division;
    }

    public void setDivision(int division) {
        this.division = division;
    }

    public int getPart() {
        return part;
    }

    public void setPart(int part) {
        this.part = part;
    }

    public int getMunicipality() {
        return municipality;
    }

    public void setMunicipality(int municipality) {
        this.municipality = municipality;
    }

    public int getTownship() {
        return township;
    }

    public void setTownship(int township) {
        this.township = township;
    }

    public int getPanchayatunion() {
        return panchayatunion;
    }

    public void setPanchayatunion(int panchayatunion) {
        this.panchayatunion = panchayatunion;
    }

    public int getVillagepanchayat() {
        return villagepanchayat;
    }

    public void setVillagepanchayat(int villagepanchayat) {
        this.villagepanchayat = villagepanchayat;
    }

    public int getWard() {
        return ward;
    }

    public void setWard(int ward) {
        this.ward = ward;
    }

    public int getBooth() {
        return booth;
    }

    public void setBooth(int booth) {
        this.booth = booth;
    }

    private int userid;
    private int limit;
    private int offset;
    private int district;
    private int constituency;
    private int partydistrict;
    private int division;
    private int part;
    private int municipality;
    private int township;
    private int panchayatunion;
    private int villagepanchayat;
    private int ward;
    private int booth;


}
