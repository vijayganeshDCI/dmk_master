package com.dci.dmkitwings.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.activity.LoginActivity;
import com.dci.dmkitwings.activity.SettingsActivity;
import com.dci.dmkitwings.activity.SplashActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.LanguageListener;
import com.dci.dmkitwings.utils.Util;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.dci.dmkitwings.app.DmkApplication.getContext;

/**
 * Created by vijayaganesh on 4/13/2018.
 */

public class LanguageFragment extends BaseFragment {

    @BindView(R.id.text_label_select_language)
    TextView textLabelSelectLanguage;
    @BindView(R.id.radio_button_english)
    RadioButton radioButtonEnglish;
    @BindView(R.id.radio_button_tamil)
    RadioButton radioButtonTamil;
    @BindView(R.id.radio_language)
    RadioGroup radioLanguage;
    @BindView(R.id.cons_language)
    ConstraintLayout consLanguage;
    Unbinder unbinder;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    LanguageListener languageListener;
    String language = "ta";
    private MenuItem change;
    SettingsActivity settingsActivity;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_language, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        settingsActivity = (SettingsActivity) getActivity();
        editor = sharedPreferences.edit();
        ((SettingsActivity) getActivity()).textTitle.setText(getString(R.string.language));
        settingsActivity.mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //Navigation Control
        settingsActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        settingsActivity.getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        settingsActivity.getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        settingsActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);

        setHasOptionsMenu(true);
        radioLanguage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.radio_button_english:
                        editor.putInt(DmkConstants.SELECTEDLANGUAGES, 0).commit();
                        language = "en";

                        break;
                    case R.id.radio_button_tamil:
                        editor.putInt(DmkConstants.SELECTEDLANGUAGES, 1).commit();
                        language = "ta";
                        break;

                }
            }
        });
        setLangauge(sharedPreferences.getInt(DmkConstants.SELECTEDLANGUAGES, 1));

//        view.setFocusableInTouchMode(true);
//        view.requestFocus();
//        view.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
//                    setLocale(language);
//                    return true;
//                }
//                return false;
//            }
//        });


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save, menu);
        change = menu.findItem(R.id.menu_bar_save);
        change.setTitle(R.string.Change);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_save:
                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                alertDialog1.setMessage(getString(R.string.alert_change_language));
                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //setLocale(language);
                        LanguageHelper.setLanguage(getContext(), language);
                        Intent intent_homeacvity = new Intent(getContext(), HomeActivity.class);
                        intent_homeacvity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent_homeacvity);


                    }
                });
                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog1.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void setLocale(String lang) {

        Resources res = getContext().getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(language));
        // API 17+ only.
        res.updateConfiguration(conf, dm);
        Intent intent_homeacvity = new Intent(getContext(), SplashActivity.class);
        intent_homeacvity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent_homeacvity);


    }


    private void setLangauge(int selectedLanguage) {
        switch (selectedLanguage) {
            case 0:
                radioButtonEnglish.setChecked(true);
                break;
            case 1:
                radioButtonTamil.setChecked(true);
                break;
        }

    }

}
