package com.dci.dmkitwings.model;

import java.util.List;

public class PartListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<PartListResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<PartListResultsItem> results) {
		Results = results;
	}

	private String Status;
	private List<PartListResultsItem> Results;
	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}