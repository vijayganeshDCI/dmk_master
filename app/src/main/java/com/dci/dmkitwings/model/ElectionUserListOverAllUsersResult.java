package com.dci.dmkitwings.model;

public class ElectionUserListOverAllUsersResult {
	private int overallpending;
	private int overallcompleted;

	public int getOverallpending() {
		return overallpending;
	}

	public void setOverallpending(int overallpending) {
		this.overallpending = overallpending;
	}

	public int getOverallcompleted() {
		return overallcompleted;
	}

	public void setOverallcompleted(int overallcompleted) {
		this.overallcompleted = overallcompleted;
	}
}
