package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.UserslistItem;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import static com.dci.dmkitwings.app.DmkApplication.getProxy;

/**
 * Created by vijayaganesh on 4/2/2018.
 */

public class ElectionOverAllListAdpater extends RecyclerView.Adapter<ElectionOverAllListAdpater.ElectionOVerAll> {


    ArrayList<UserslistItem> electionuserListArray;
    Context context;
    HttpProxyCacheServer proxy;
    boolean clickablecheck = false;
    int userid;

    public ElectionOverAllListAdpater(ArrayList<UserslistItem> electionuserListArray, Context context, Boolean clickablecheck) {
        this.electionuserListArray = electionuserListArray;
        this.context = context;
        this.clickablecheck = clickablecheck;
        this.userid = userid;

    }


    @Override
    public ElectionOVerAll onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_election_overrall, parent, false);
        proxy = getProxy(context);
        return new ElectionOVerAll(itemView);
    }

    @Override
    public void onBindViewHolder(ElectionOVerAll holder, final int position) {
        UserslistItem election = electionuserListArray.get(position);
        holder.textName.setText(" " + election.getFirstname() + " " + election.getLastname());
        holder.textBooth.setText(context.getString(R.string.booth) + "-" + election.getBooth());
        universalImageLoader(holder.imageProPic, context.getString(R.string.s3_bucket_profile_path),
                election.getAvatar(), R.mipmap.icon_pro_image_loading_256,
                R.mipmap.icon_pro_image_loading_256);

        switch (election.getTypeid()) {
            case 1:
                //Corpation
                holder.textWard.setText(context.getString(R.string.ward) + "-" + election.getWarddetail());
                holder.textPart.setText(context.getString(R.string.part) + "-" + election.getPartdetails());
                holder.textCons.setText(context.getString(R.string.volume) + "-" + election.getConstituency());
                holder.textPanchayatUnion.setText(context.getString(R.string.district)+"-"+election.getDistrict());
                break;
            case 2:
                //Municipality
                holder.textWard.setText(context.getString(R.string.ward) + "-" + election.getWarddetail());
                holder.textPart.setText(context.getString(R.string.municipallity) + "-" + election.getMunicipalitydetails());
                holder.textCons.setText(context.getString(R.string.volume) + "-" + election.getConstituency());
                holder.textPanchayatUnion.setText(context.getString(R.string.district)+"-"+election.getDistrict());
                break;
            case 3:
                //PanchayatUnion
                switch (election.getUniontype()) {
                    case 1:
                        //TownPanchayat
                        holder.textWard.setText(context.getString(R.string.ward) + " " + election.getWarddetail());
                        holder.textPart.setText(context.getString(R.string.pachayat_union) + "-" +election.getPanchayatuniondetail());
                        holder.textPanchayatUnion.setText(context.getString(R.string.volume) + "-" +election.getConstituency());
                        holder.textCons.setText(context.getString(R.string.town_pachayat) + "-" +election.getTowndetails());
                        holder.textDistrict.setText(context.getString(R.string.district)+"-"+election.getDistrict());
                        break;
                    case 2:
                        //VillagePanchayat
                        holder.textPart.setText(context.getString(R.string.pachayat_union) + "-" +election.getPanchayatuniondetail());
                        holder.textPanchayatUnion.setText(context.getString(R.string.volume) + "-" +election.getConstituency());
                        holder.textCons.setText(context.getString(R.string.village_panchayat) + "-" +election.getVillagepanchayatdetail());
                        holder.textDistrict.setText(context.getString(R.string.district)+"-"+election.getDistrict());
                        break;
                }
                break;

        }


        int totalnovotes = election.getCompleted() + election.getPending();
        float bmi = (float) election.getCompleted() / (float) (totalnovotes);
        double roundOff = Math.round(bmi * 100.0) / 100.0;
        int pollre = (int) ((float) roundOff * 100);
        float bmi1 = (float) election.getPending() / (float) (totalnovotes);
        double roundOff1 = Math.round(bmi1 * 100.0) / 100.0;
        int pollre1 = (int) ((float) roundOff1 * 100);
        holder.text_completed.setText(context.getString(R.string.completed));
        holder.text_pending.setText(context.getString(R.string.remaining));
        holder.text_completedper.setText(+pollre + " % ");
        holder.text_pendingper.setText(+pollre1 + " % ");
        holder.progressBarone.setProgress(pollre);
        holder.progressBartwo.setProgress(pollre1);


    }

    public Bitmap createThumbnailFromPath(String filePath, int type) {
        return ThumbnailUtils.createVideoThumbnail(filePath, type);
    }

    @Override
    public int getItemCount() {
        return electionuserListArray.size();
    }


    public class ElectionOVerAll extends RecyclerView.ViewHolder {


        TextView textName, textBooth, textWard;
        TextView textPart, textCons, textPanchayatUnion;
        TextView textDistrict;
        TextView text_completed;
        ProgressBar progressBarone;
        TextView text_completedper;
        TextView text_pending;
        ProgressBar progressBartwo;
        TextView text_pendingper;
        ImageView imageProPic;

        public ElectionOVerAll(View itemView) {
            super(itemView);
            textName = (TextView) itemView.findViewById(R.id.text_name);
            textBooth = (TextView) itemView.findViewById(R.id.text_booth);
            textWard = (TextView) itemView.findViewById(R.id.text_ward);
            textPart = (TextView) itemView.findViewById(R.id.text_part);
            textCons = (TextView) itemView.findViewById(R.id.text_cons);
            textPanchayatUnion = (TextView) itemView.findViewById(R.id.text_panchayt_union);
            textDistrict = (TextView) itemView.findViewById(R.id.text_district);
            imageProPic = (ImageView) itemView.findViewById(R.id.image_pro_pic);
            textName = (TextView) itemView.findViewById(R.id.text_name);
            text_completed = (TextView) itemView.findViewById(R.id.text_completed);
            progressBarone = (ProgressBar) itemView.findViewById(R.id.progressBarone);
            text_completedper = (TextView) itemView.findViewById(R.id.text_completedper);
            text_pending = (TextView) itemView.findViewById(R.id.text_pending);
            progressBartwo = (ProgressBar) itemView.findViewById(R.id.progressBartwo);
            text_pendingper = (TextView) itemView.findViewById(R.id.text_pendingper);


        }
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }
}
