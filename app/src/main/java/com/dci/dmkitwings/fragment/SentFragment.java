package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ChatAct;
import com.dci.dmkitwings.adapter.ReecentChatListAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.ChatMessageParams;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.view.CustomTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by vijayaganesh on 4/12/2018.
 */

public class SentFragment extends BaseFragment {
    @BindView(R.id.list_user_list)
    ListView listUserList;
    @BindView(R.id.image_no_network)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_network)
    CustomTextView textNoNetwork;
    @BindView(R.id.cons_contact_list)
    ConstraintLayout consContactList;
    Unbinder unbinder;
    private ArrayList<ChatMessageParams> membersSentList;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    DmkAPI dmkAPI;
    private String senderUserName;
    private int senderUserID;
    public String RECENT_LIST_CHILD;
    private DatabaseReference mFirebaseDatabaseReference;
    public String RECENT_CHAT = "RECENT_CHAT_MESSAGES";
    private ReecentChatListAdapter reecentChatListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inbox, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        membersSentList = new ArrayList<ChatMessageParams>();
        senderUserName = sharedPreferences.getString(DmkConstants.FIRSTNAME, "") + "" +
                sharedPreferences.getString(DmkConstants.LASTNAME, "");
        senderUserID = sharedPreferences.getInt(DmkConstants.USERID, 0);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        RECENT_LIST_CHILD = senderUserName + senderUserID;
        DatabaseReference messagesRef = mFirebaseDatabaseReference.child(RECENT_CHAT).child("Sent").
                child(RECENT_LIST_CHILD);
        messagesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                membersSentList.clear();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    ChatMessageParams chatMessageParams = postSnapshot.getValue(ChatMessageParams.class);
                    membersSentList.add(chatMessageParams);
                    // here you can access to name property like university.name

                }
                reecentChatListAdapter = new ReecentChatListAdapter(getActivity(), membersSentList,SentFragment.this);
                if (isAdded())listUserList.setAdapter(reecentChatListAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });

        listUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int postion, long l) {
                Intent intent = new Intent(getActivity(), ChatAct.class);
                intent.putExtra("chatWith", membersSentList.get(postion).getToName());
                intent.putExtra("chatWithUserID", membersSentList.get(postion).getChatWithID());
                intent.putExtra("proPic", membersSentList.get(postion).getReceiverPhotoUrl());
                intent.putExtra("memberID", membersSentList.get(postion).getReceiverUniqueID());
                intent.putExtra("designation", membersSentList.get(postion).getReceiverRoleName());
                intent.putExtra("fcmToken", membersSentList.get(postion).getReceiverFcmToken());
                startActivity(intent);

            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
