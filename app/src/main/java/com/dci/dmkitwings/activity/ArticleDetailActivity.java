package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.ImageViewPageAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.ArticleDeletePostParams;
import com.dci.dmkitwings.model.ArticleDetailParams;
import com.dci.dmkitwings.model.ArticleDetailResponse;
import com.dci.dmkitwings.model.NewsDeleteResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.dci.dmkitwings.view.CustomTextViewBold;
import com.firebase.client.utilities.Base64;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.jzvd.JZVideoPlayerStandard;
import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.dmkitwings.app.DmkApplication.getProxy;

public class ArticleDetailActivity extends BaseActivity {
    @BindView(R.id.image_time)
    ImageView imageTime;
    @BindView(R.id.videoplayer_time)
    JZVideoPlayerStandard videoplayerTime;
    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    @BindView(R.id.text_posted_by)
    CustomTextView textPostedBy;
    @BindView(R.id.text_designation)
    CustomTextViewBold textDesignation;
    @BindView(R.id.text_post_date)
    CustomTextView textPostDate;
    @BindView(R.id.text_label_time_line_title)
    CustomTextViewBold textLabelTimeLineTitle;
    @BindView(R.id.text_label_time_line_content)
    CustomTextView textLabelTimeLineContent;

    Unbinder unbinder;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;
    int articleID, districtID, constituencyID, newsType;
    String newsTypeID;
    ArticleDetailResponse articleDetailResponse;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.view_bottom_1)
    View viewBottom1;
    @BindView(R.id.cons_time_item)
    ConstraintLayout consTimeItem;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;
    @BindView(R.id.image_menu)
    ImageView imageMenu;
    @BindView(R.id.image_view_pager)
    ViewPager imageViewPager;
    @BindView(R.id.view_pager_indicator)
    CircleIndicator viewPagerIndicator;
    @BindView(R.id.videoplayer_detail)
    JZVideoPlayerStandard videoplayerDetail;
    private NewsDeleteResponse newsDeleteResponse;
    private HttpProxyCacheServer proxy;
    ImageViewPageAdapter imageViewPageAdapter;
    @BindView(R.id.image_share)
    ImageView imageshare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_article_detail, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        frameLayoutNotification.setVisibility(View.GONE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toolbarHome.setVisibility(View.GONE);
        proxy = getProxy(ArticleDetailActivity.this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Intent bundle = getIntent();
        if (getIntent() != null) {
            articleID = bundle.getIntExtra("articleID", 0);
            districtID = bundle.getIntExtra("districtID", 0);
            constituencyID = bundle.getIntExtra("constituencyID", 0);
            newsTypeID = bundle.getStringExtra("newsTypeID");
            newsType = bundle.getIntExtra("newsType", 0);

        }
        videoplayerDetail.setVisibility(View.GONE);
        videoplayerTime.setVisibility(View.GONE);


    }

    @Override
    public void onResume() {
        super.onResume();
        getDetailArticle();
    }

    private void getDetailArticle() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            final ArticleDetailParams articleDetailParams = new ArticleDetailParams();
            articleDetailParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            articleDetailParams.setArticlesid(articleID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getArticleDetails(headerMap, articleDetailParams).enqueue(new Callback<ArticleDetailResponse>() {
                @Override
                public void onResponse(Call<ArticleDetailResponse> call, Response<ArticleDetailResponse> response) {
                    articleDetailResponse = response.body();
                    hideProgress();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (articleDetailResponse.getStatuscode() == 0) {
                            textDesignation.setVisibility(View.GONE);
                            textPostDate.setText(getDateFormat(articleDetailResponse.getResults().getCreated_at()));
                            if (articleDetailResponse.getResults().getUser().getId() == sharedPreferences.getInt(DmkConstants.USERID, 0)) {
                                textPostedBy.setText("You");
                                imageMenu.setVisibility(View.VISIBLE);
                            } else {
                                textPostedBy.setText(articleDetailResponse.getResults().getUser().getFirstName() + " " +
                                        articleDetailResponse.getResults().getUser().getLastName());
                                imageMenu.setVisibility(View.GONE);

                            }
//                        if (articleDetailResponse.getResults().getUser().getId()!=1)
//                        {
//                            textDesignation.setVisibility(View.VISIBLE);
//                            textDesignation.setText(articleDetailResponse.getResults().getUser().getDesignation()
//                                    + " " + articleDetailResponse.getResults().getUpdatedAt());
//                        }
                            textLabelTimeLineTitle.setText(articleDetailResponse.getResults().getTitle());
                            textLabelTimeLineContent.setText(articleDetailResponse.getResults().getContent());

                            universalImageLoader(imageProPic, getString(R.string.s3_image_url_download),
                                    articleDetailResponse.getResults().getUser().getAvatar(),
                                    R.mipmap.icon_pro_image_loading_256,
                                    R.mipmap.icon_pro_image_loading_256);


                            List<String> imageFiles = new ArrayList<String>();
                            List<String> videoFile = new ArrayList<String>();


                            for (int i = 0; i < articleDetailResponse.getResults().getMediapath().size(); i++) {
                                if (isImageFile(articleDetailResponse.getResults().getMediapath().get(i))) {
                                    imageFiles.add(articleDetailResponse.getResults().getMediapath().get(i));
                                } else {
                                    videoFile.add(articleDetailResponse.getResults().getMediapath().get(i));
                                }
                            }

                            if (imageFiles.size() > 0 && videoFile.size() > 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.VISIBLE);
                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            ArticleDetailActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);

                                videoplayerDetail.setUp(proxy.getProxyUrl(
                                        getString(R.string.s3_image_url_download) +
                                                getString(R.string.s3_bucket_article_path) +
                                                videoFile.get(0))
                                        , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                                videoplayerDetail.setSoundEffectsEnabled(true);
                                //thumbnail generation
                                if (isAudioFile(getString(R.string.s3_image_url_download) +
                                        getString(R.string.s3_bucket_article_path) +
                                        videoFile.get(0))) {
                                    Glide.with(ArticleDetailActivity.this)
                                            .load(R.mipmap.icon_audio_thumbnail).into(videoplayerDetail.thumbImageView);
                                } else {
                                    if (!articleDetailResponse.getResults().getVideothumb().equalsIgnoreCase("0")) {
                                        Glide.with(ArticleDetailActivity.this).
                                                load(articleDetailResponse.getResults().getVideothumb()).
                                                into(videoplayerDetail.thumbImageView);
                                    } else {
                                        Glide.with(ArticleDetailActivity.this).
                                                load(R.mipmap.icon_video_thumbnail)
                                                .into(videoplayerDetail.thumbImageView);
                                    }
                                }

                            } else if (imageFiles.size() == 0 && videoFile.size() > 0) {
                                imageViewPager.setVisibility(View.INVISIBLE);
                                videoplayerTime.setVisibility(View.VISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                videoplayerTime.setUp(proxy.getProxyUrl(getString(R.string.s3_image_url_download) +
                                                getString(R.string.s3_bucket_article_path) +
                                                videoFile.get(0))
                                        , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                                videoplayerTime.setSystemTimeAndBattery();
                                //thumbnail generation
                                if (isAudioFile(getString(R.string.s3_image_url_download) +
                                        getString(R.string.s3_bucket_article_path) +
                                        videoFile.get(0))) {
                                    Glide.with(ArticleDetailActivity.this)
                                            .load(R.mipmap.icon_audio_thumbnail).into(videoplayerTime.thumbImageView);
                                } else {
                                    if (!articleDetailResponse.getResults().getVideothumb().equalsIgnoreCase("0")) {
                                        Glide.with(ArticleDetailActivity.this).
                                                load(articleDetailResponse.getResults().getVideothumb()).
                                                into(videoplayerTime.thumbImageView);
                                    } else {
                                        Glide.with(ArticleDetailActivity.this).
                                                load(R.mipmap.icon_video_thumbnail)
                                                .into(videoplayerTime.thumbImageView);
                                    }
                                }

                            } else if (imageFiles.size() > 0 && videoFile.size() == 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            ArticleDetailActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);
                                //
                            } else if (imageFiles.size() == 0 && videoFile.size() == 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                imageFiles.add("");
                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            ArticleDetailActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, articleDetailResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, articleDetailResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(articleDetailResponse.getSource(),
                                    articleDetailResponse.getSourcedata()));
                            editor.commit();
                            getDetailArticle();
                        }
                    } else {
                        hideProgress();
                        Toast.makeText(ArticleDetailActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArticleDetailResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ArticleDetailActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(ArticleDetailActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }


    @OnClick({R.id.image_menu, R.id.image_share, R.id.image_pro_pic})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_menu:
                PopupMenu popup = new PopupMenu(this, imageMenu);
                //inflating menu_icon from xml resource
                popup.inflate(R.menu.comment_item_menu);
                //adding click listener


                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit:
                                //handle menu1 click
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ArticleDetailActivity.this);
                                alertDialog.setMessage(R.string.confirm_to_edit);
                                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(ArticleDetailActivity.this, ArticleComposeActivity.class);
                                        intent.putExtra("articleID", articleID);
                                        intent.putExtra("newsTitle", textLabelTimeLineTitle.getText().toString());
                                        intent.putExtra("newsContent", textLabelTimeLineContent.getText().toString());
                                        intent.putExtra("newsDistrict", districtID);
                                        intent.putExtra("newsConstituency", constituencyID);
                                        intent.putExtra("Newstypeid", newsTypeID);
                                        intent.putExtra("Newstype", newsType);
                                        intent.putExtra("fromNews", true);
                                        startActivity(intent);
                                    }

                                });
                                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                                return true;
                            case R.id.delete:
                                //handle menu2 click
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(ArticleDetailActivity.this);
                                alertDialog1.setMessage(R.string.confirm_to_delete);
                                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        deleteNews(articleID);
                                    }

                                });
                                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog1.show();
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
                break;

            case R.id.image_share:
                String ps = "Dmk_encryption " + articleDetailResponse.getResults().getId();
                String tmp = Base64.encodeBytes(ps.getBytes());
                tmp = Base64.encodeBytes(tmp.getBytes());
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, articleDetailResponse.getResults().getTitle() +
                        "\n" + getString(R.string.for_more) +
                        getString(R.string.deeplink_url) + "articles/" + tmp);
                startActivity(Intent.createChooser(i, "Choose share option"));
                break;
            case R.id.image_pro_pic:
                BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.profile_bubble, null);
                PopupWindow popupWindow = BubblePopupHelper.create(this, bubbleLayout);
                CircleImageView imageprofilepic = (CircleImageView) bubbleLayout.findViewById(R.id.image_pro_pic_bubble);
                CustomTextView textpostedby = (CustomTextView) bubbleLayout.findViewById(R.id.text_posted_by_bubble);
                CustomTextView textdesgination = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_designation_bubble);
                CustomTextView textdistrict = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_district_bubble);
                CustomTextView textconstituency = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_constituency_bubble);
                CustomTextView textPartBoothWardVillage = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_common_bubble);
                if (articleDetailResponse.getResults().getUser().getId() != 1 && articleDetailResponse.getResults().getUser().getId() != sharedPreferences.getInt(DmkConstants.USERID, 0)) {

                    switch (articleDetailResponse.getResults().getUser().getLevel()) {
                        case 1:

                            // செயலாளர்
//                            துணை செயலாளர்
//                            மண்டல ஒருங்கிணைப்பாளர்
                            textpostedby.setText(articleDetailResponse.getResults().getUser().getFirstName() + " " +
                                    articleDetailResponse.getResults().getUser().getLastName());
                            textdesgination.setText(articleDetailResponse.getResults().getUser().getDesignation());
                            textdistrict.setVisibility(View.GONE);
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 2:
                            //மாவட்ட ஒருங்கிணைப்பாளர் district
                            textpostedby.setText(articleDetailResponse.getResults().getUser().getFirstName() + " " +
                                    articleDetailResponse.getResults().getUser().getLastName());
                            textdesgination.setText(articleDetailResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(articleDetailResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 3:
                            //தொகுதி ஒருங்கிணைப்பாளர் constituency
                            textpostedby.setText(articleDetailResponse.getResults().getUser().getFirstName() + " " +
                                    articleDetailResponse.getResults().getUser().getLastName());
                            textdesgination.setText(articleDetailResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(articleDetailResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setText(articleDetailResponse.getResults().getUser().getConstituency().getConstituencyName());
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                            //பகுதி ஒருங்கிணைப்பாளர் part
                            //நகராட்சி ஒருங்கிணைப்பாளர் municipality
                            //பேரூராட்சி ஒருங்கிணைப்பாளர் township
                            //ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                            textpostedby.setText(articleDetailResponse.getResults().getUser().getFirstName() + " " +
                                    articleDetailResponse.getResults().getUser().getLastName());
                            textdesgination.setText(articleDetailResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(articleDetailResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setText(articleDetailResponse.getResults().getUser().getConstituency().getConstituencyName());
                            textPartBoothWardVillage.setText(articleDetailResponse.getResults().getUser().getPart().getPartname());
                            break;
                        case 5:
                        case 6:
                        case 9:
                        case 10:
                        case 13:
                        case 14:
                        case 18:
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            //கிளை ஒருங்கிணைப்பாளர் ward
                            // வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //கிளை ஒருங்கிணைப்பாளர்  ward
                            textpostedby.setText(articleDetailResponse.getResults().getUser().getFirstName() + " " +
                                    articleDetailResponse.getResults().getUser().getLastName());
                            textdesgination.setText(articleDetailResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(articleDetailResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setText(articleDetailResponse.getResults().getUser().getConstituency().getConstituencyName());
                            textPartBoothWardVillage.setText(articleDetailResponse.getResults().getUser().getWard().getWardname());
                            break;
                        case 7:
                        case 11:
                        case 15:
                        case 19:
                            //வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                            textpostedby.setText(articleDetailResponse.getResults().getUser().getFirstName() + " " +
                                    articleDetailResponse.getResults().getUser().getLastName());
                            textdesgination.setText(articleDetailResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(articleDetailResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setText(articleDetailResponse.getResults().getUser().getConstituency().getConstituencyName());
                            textPartBoothWardVillage.setText(articleDetailResponse.getResults().getUser().getBooth().getBoothname());
                            break;
                        case 17:
                            //ராட்சிஊ ஒருங்கிணைப்பாளர் villagepanchayat
                            textpostedby.setText(articleDetailResponse.getResults().getUser().getFirstName() + " " +
                                    articleDetailResponse.getResults().getUser().getLastName());
                            textdesgination.setText(articleDetailResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(articleDetailResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setText(articleDetailResponse.getResults().getUser().getConstituency().getConstituencyName());
                            textPartBoothWardVillage.setText(articleDetailResponse.getResults().getUser().getVillage().getVillageName());
                            break;
                    }


                    final Random random = new Random();
                    int[] location = new int[2];
                    imageProPic.getLocationInWindow(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.LEFT);
                    popupWindow.showAtLocation(imageProPic, Gravity.AXIS_PULL_BEFORE, imageProPic.getWidth() + 25, -185);
                    universalImageLoader(imageprofilepic, getString(R.string.s3_bucket_profile_path),
                            articleDetailResponse.getResults().getUser().getAvatar(),
                            R.mipmap.icon_pro_image_loading_256,
                            R.mipmap.icon_pro_image_loading_256);

                    break;
                }


        }


    }

    private void deleteNews(final int newsID) {
        ArticleDeletePostParams articleDeletePostParams = new ArticleDeletePostParams();
        articleDeletePostParams.setArticlesid(newsID);
        articleDeletePostParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.deletearticle(headerMap, articleDeletePostParams).enqueue(new Callback<NewsDeleteResponse>() {
                @Override
                public void onResponse(Call<NewsDeleteResponse> call, Response<NewsDeleteResponse> response) {
                    newsDeleteResponse = response.body();
                    hideProgress();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (newsDeleteResponse.getStatuscode()==0) {
                            if (newsDeleteResponse.getStatus().contains("Success")) {
                                Toast.makeText(ArticleDetailActivity.this, getString(R.string.article_delete_sucuess), Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(ArticleDetailActivity.this, getString(R.string.article_delete_failed), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, newsDeleteResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, newsDeleteResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(newsDeleteResponse.getSource(),
                                    newsDeleteResponse.getSourcedata()));
                            editor.commit();
                            deleteNews(newsID);
                        }
                    } else {
                        Toast.makeText(ArticleDetailActivity.this, getString(R.string.article_delete_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<NewsDeleteResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ArticleDetailActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(ArticleDetailActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        setResult(3, intent);
        finish();
    }
}
