package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ArticleComposeActivity;
import com.dci.dmkitwings.activity.ArticleDetailActivity;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.NewsComposeActivity;
import com.dci.dmkitwings.activity.NewsDetailaActivity;
import com.dci.dmkitwings.activity.NewsLinkActivity;
import com.dci.dmkitwings.adapter.ArticlesAdpater;
import com.dci.dmkitwings.adapter.SubNewsAdpater;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.ArticleListParams;
import com.dci.dmkitwings.model.ArticleListResponse;
import com.dci.dmkitwings.model.ArticleListResultsItem;
import com.dci.dmkitwings.model.NewsListParams;
import com.dci.dmkitwings.model.NewsListResponse;
import com.dci.dmkitwings.model.NewsListResultsItem;
import com.dci.dmkitwings.model.UserStatusParam;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.wooplr.spotlight.utils.SpotlightSequence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ArticleFragment extends BaseFragment {

    @BindView(R.id.list_articles)
    ListView listArticles;
    @BindView(R.id.swipe_article_line)
    SwipeRefreshLayout swipeArticle;
    ArticlesAdpater articlesAdpater;
    @BindView(R.id.float_article_compose)
    FloatingActionButton floatArticleCompose;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ArticleListResponse articleListResponse;
    private ArrayList<ArticleListResultsItem> articleListResponseArrayList;
    BaseActivity baseActivity;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    Unbinder unbinder;
    @BindView(R.id.image_no_comments)
    ImageView imageNoTimeLine;
    @BindView(R.id.text_no_comm)
    CustomTextView textNoTimeLine;
    private int preLast;
    boolean lastEnd, onCreatebool = false;
    int totalcount = 0, pagecount = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        baseActivity = (BaseActivity) getActivity();
        articleListResponseArrayList = new ArrayList<ArticleListResultsItem>();
        listArticles.setDivider(null);
        articlesAdpater = new ArticlesAdpater(articleListResponseArrayList, getActivity(), baseActivity, ArticleFragment.this);
        listArticles.setAdapter(articlesAdpater);
        swipeArticle.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeArticle.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            //showProgress();
                            pagecount = 0;
                            onCreatebool = false;
                            lastEnd = false;
                            preLast = 0;
                            articleListResponseArrayList.clear();
                            getArticleList();
                            swipeArticle.setRefreshing(false);
                        }
                    }
                }, 000);
            }
        });
        listArticles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getContext(), ArticleDetailActivity.class);
                intent.putExtra("articleID", articleListResponseArrayList.get(position).getId() != 0 ?
                        articleListResponseArrayList.get(position).getId() : 0);
                intent.putExtra("districtID", articleListResponseArrayList.get(position).getDistrictID() != 0 ?
                        articleListResponseArrayList.get(position).getDistrictID() : 0);
                intent.putExtra("constituencyID", articleListResponseArrayList.get(position).getConstituencyID() != 0 ?
                        articleListResponseArrayList.get(position).getConstituencyID() : 0);
                intent.putExtra("newsTypeID", articleListResponseArrayList.get(position).getArticlestypeid() != null ?
                        articleListResponseArrayList.get(position).getArticlestypeid() : null);
                intent.putExtra("newsType", articleListResponseArrayList.get(position).getArticlestype() != 0 ?
                        articleListResponseArrayList.get(position).getArticlestype() : 0);
                startActivityForResult(intent, 4);


            }
        });
        listArticles.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (listArticles.getLastVisiblePosition() - listArticles.getHeaderViewsCount() -
                        listArticles.getFooterViewsCount()) >= (articlesAdpater.getCount() - 1)) {
                    // Now your listview has hit the bottom
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                final int lastItem = firstVisibleItem + visibleItemCount;


                if (lastItem == totalItemCount) {
                    if (articleListResponseArrayList.size() >= 10) {
                        if (preLast != lastItem) {
                            try {
                                if (!lastEnd && onCreatebool) {
                                    pagecount = pagecount + 11;
                                    //LoadData(pagecount,"");
                                    //showProgress();
                                    getArticleList();

                                    preLast = lastItem;
                                } else {
                                    //Log.d("Last", "fail");
                                }
                            } catch (Exception e) {
                            }

                        }
                    }
                }


            }
        });
        if (sharedPreferences.getString(DmkConstants.USERPERMISSION_ARTICLE, null).equals("0")) {
            floatArticleCompose.setVisibility(View.GONE);
        }



        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        pagecount = 0;
        onCreatebool = false;
        lastEnd = false;
        preLast = 0;
        if (isVisible) {
            getArticleList();

        }

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            pagecount = 0;
            onCreatebool = false;
            lastEnd = false;
            preLast = 0;
            getArticleList();

        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @OnClick(R.id.float_article_compose)
    public void onViewClicked() {
        Intent intent = new Intent(getActivity(), ArticleComposeActivity.class);
        startActivityForResult(intent, 3);
    }

    public void getArticleList() {
        if (Util.isNetworkAvailable()) {
//            showProgress();
            //articleListResponseArrayList.clear();
            final ArticleListParams articleListParams = new ArticleListParams();
            articleListParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            articleListParams.setLimit(10);
            articleListParams.setOffset(pagecount);
            if (pagecount == 0) {
                articleListResponseArrayList.clear();
            }
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getArticleList(headerMap,articleListParams).enqueue(new Callback<ArticleListResponse>() {
                @Override
                public void onResponse(Call<ArticleListResponse> call, Response<ArticleListResponse> response) {
                    hideProgress();
                    articleListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (articleListResponse.getStatuscode()==0) {
                            onCreatebool = true;
                            if (articleListResponse != null && articleListResponse.getResults().size() > 0) {
                                if (isAdded()) {
                                    imageNoTimeLine.setVisibility(View.GONE);
                                    textNoTimeLine.setVisibility(View.GONE);
                                    listArticles.setVisibility(View.VISIBLE);
                                }
                                for (int u = 0; u < articleListResponse.getResults().size(); u++) {
                                    if (articleListResponse.getResults() != null) {
                                        articleListResponseArrayList.add(articleListResponse.getResults().get(u));
                                    }
                                }
                                articlesAdpater.notifyDataSetChanged();
                                hideProgress();
                            } else {
                                if (articleListResponseArrayList.size() == 0) {
                                    imageNoTimeLine.setVisibility(View.VISIBLE);
                                    imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                                    textNoTimeLine.setText(R.string.no_article_line);
                                    textNoTimeLine.setVisibility(View.VISIBLE);
                                    listArticles.setVisibility(View.GONE);
                                } else {
                                    lastEnd = true;
                                }


                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, articleListResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, articleListResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(articleListResponse.getSource(),
                                    articleListResponse.getSourcedata()));
                            editor.commit();
                            getArticleList();
                        }
                    } else {
                        hideProgress();
                        if (articleListResponseArrayList.size() == 0) {
                            imageNoTimeLine.setVisibility(View.VISIBLE);
                            imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                            textNoTimeLine.setText(R.string.no_article_line);
                            textNoTimeLine.setVisibility(View.VISIBLE);
                            listArticles.setVisibility(View.GONE);
                            hideProgress();
                            //Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                        } else {
                            //Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<ArticleListResponse> call, Throwable t) {
                    hideProgress();
                    if (articleListResponseArrayList.size() == 0) {

                        imageNoTimeLine.setVisibility(View.VISIBLE);
                        imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                        textNoTimeLine.setText(R.string.no_article_line);
                        textNoTimeLine.setVisibility(View.VISIBLE);
                        listArticles.setVisibility(View.GONE);
                        //Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }


                }
            });
        } else {
            if (isAdded()) {
                imageNoTimeLine.setVisibility(View.VISIBLE);
                textNoTimeLine.setVisibility(View.VISIBLE);
                imageNoTimeLine.setImageResource(R.mipmap.icon_no_network);
                textNoTimeLine.setText(R.string.no_network);
                listArticles.setVisibility(View.GONE);
            }
        }
    }

}
