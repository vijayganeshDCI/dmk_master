package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.HirerachyResultsItem;
import com.dci.dmkitwings.view.CustomTextView;
import com.dci.dmkitwings.view.CustomTextViewBold;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MemberListAdapter extends BaseAdapter {


    public MemberListAdapter(List<HirerachyResultsItem> memberLists, Context context) {
        this.memberLists = memberLists;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    List<HirerachyResultsItem> memberLists;
    Context context;
    LayoutInflater mInflater;

    @Override
    public int getCount() {
        return memberLists.size();
    }

    @Override
    public HirerachyResultsItem getItem(int position) {
        return memberLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_member_list, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        universalImageLoader(viewHolder.imageProPic, context.getString(R.string.s3_bucket_profile_path),
                memberLists.get(position).getAvatar(), R.mipmap.icon_pro_image_loading_256,
                R.mipmap.icon_pro_image_loading_256);

        viewHolder.textLabelName.setText(memberLists.get(position).getFirstName());
        viewHolder.textDesigantion.setText(memberLists.get(position).getDesignation());
        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.image_pro_pic)
        CircleImageView imageProPic;
        @BindView(R.id.text_label_name)
        CustomTextView textLabelName;
        @BindView(R.id.text_label_designation)
        CustomTextViewBold textDesigantion;
        @BindView(R.id.cons_member_list)
        ConstraintLayout consMemberList;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }
}
