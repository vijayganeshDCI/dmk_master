package com.dci.dmkitwings.model;

public class UserStatusResults {

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourcedata() {
        return sourcedata;
    }

    public void setSourcedata(String sourcedata) {
        this.sourcedata = sourcedata;
    }

    private int userid;
    private String source;
    private String sourcedata;

}
