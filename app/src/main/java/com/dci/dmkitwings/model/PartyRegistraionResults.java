package com.dci.dmkitwings.model;

public class PartyRegistraionResults {

    public int getTypeid() {
        return Typeid;
    }

    public void setTypeid(int typeid) {
        Typeid = typeid;
    }


    public void setWing(UserDetailWing wing) {
        this.wing = wing;
    }

    public UserDetailRoles getRoles() {
        return roles;
    }

    public void setRoles(UserDetailRoles roles) {
        this.roles = roles;
    }


    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public int getDistrictID() {
        return DistrictID;
    }

    public void setDistrictID(int districtID) {
        DistrictID = districtID;
    }

    public String getOTPValidTime() {
        return OTPValidTime;
    }

    public void setOTPValidTime(String OTPValidTime) {
        this.OTPValidTime = OTPValidTime;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getSuperiorRoleId() {
        return SuperiorRoleId;
    }

    public void setSuperiorRoleId(int superiorRoleId) {
        SuperiorRoleId = superiorRoleId;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }

    public int getVillagepanchayat() {
        return Villagepanchayat;
    }

    public void setVillagepanchayat(int villagepanchayat) {
        Villagepanchayat = villagepanchayat;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getIMEIID() {
        return IMEIID;
    }

    public void setIMEIID(String IMEIID) {
        this.IMEIID = IMEIID;
    }

    public int getRoleID() {
        return RoleID;
    }

    public void setRoleID(int roleID) {
        RoleID = roleID;
    }

//    public String getPassword() {
//        return Password;
//    }
//
//    public void setPassword(String password) {
//        Password = password;
//    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getMunicipalityid() {
        return municipalityid;
    }

    public void setMunicipalityid(int municipalityid) {
        this.municipalityid = municipalityid;
    }

    public int getPartid() {
        return partid;
    }

    public void setPartid(int partid) {
        this.partid = partid;
    }

    public String getBoothID() {
        return BoothID;
    }

    public void setBoothID(String boothID) {
        BoothID = boothID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public int getPartydistrict() {
        return partydistrict;
    }

    public void setPartydistrict(int partydistrict) {
        this.partydistrict = partydistrict;
    }

    public int getSuperiorUserID() {
        return SuperiorUserID;
    }

    public void setSuperiorUserID(int superiorUserID) {
        SuperiorUserID = superiorUserID;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDeviceChangeCount() {
        return DeviceChangeCount;
    }

    public void setDeviceChangeCount(String deviceChangeCount) {
        DeviceChangeCount = deviceChangeCount;
    }

    public String getUniquieID() {
        return UniquieID;
    }

    public void setUniquieID(String uniquieID) {
        UniquieID = uniquieID;
    }

    public int getWardID() {
        return WardID;
    }

    public void setWardID(int wardID) {
        WardID = wardID;
    }

    public String getDeviceToken() {
        return DeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }

    public String getSignature() {
        return Signature;
    }

    public void setSignature(String signature) {
        Signature = signature;
    }

    public String getLastLogin() {
        return LastLogin;
    }

    public void setLastLogin(String lastLogin) {
        LastLogin = lastLogin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

//    public int getVattamid() {
//        return vattamid;
//    }
//
//    public void setVattamid(int vattamid) {
//        this.vattamid = vattamid;
//    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserPermission() {
        return UserPermission;
    }

    public void setUserPermission(String userPermission) {
        UserPermission = userPermission;
    }

    public String getConstituencyName() {
        return ConstituencyName;
    }

    public void setConstituencyName(String constituencyName) {
        ConstituencyName = constituencyName;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public String getWardName() {
        return WardName;
    }

    public void setWardName(String wardName) {
        WardName = wardName;
    }

    public com.dci.dmkitwings.model.UserPermissionset getUserPermissionset() {
        return UserPermissionset;
    }

    public void setUserPermissionset(com.dci.dmkitwings.model.UserPermissionset userPermissionset) {
        UserPermissionset = userPermissionset;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getVoterID() {
        return VoterID;
    }

    public void setVoterID(String voterID) {
        VoterID = voterID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getConstituencyID() {
        return ConstituencyID;
    }

    public void setConstituencyID(int constituencyID) {
        ConstituencyID = constituencyID;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public int getPanchayatunion() {
        return Panchayatunion;
    }

    public void setPanchayatunion(int panchayatunion) {
        Panchayatunion = panchayatunion;
    }

    public int getTownshipid() {
        return townshipid;
    }

    public void setTownshipid(int townshipid) {
        this.townshipid = townshipid;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    private String address;

    private int Typeid;
    private String Email;
    private int DistrictID;
    private String OTPValidTime;
    private String created_at;
    private int SuperiorRoleId;
    private String OTP;
    private int Villagepanchayat;
    private String Gender;
    private String IMEIID;
    private int RoleID;
    private String updated_at;
    private String DOB;
    private String VoterID;
    private int id;
    private int ConstituencyID;
    private String remember_token;
    private int Panchayatunion;
    private int townshipid;
    private int Age;
    private String PaymentStatus;
    private int Status;
    private int municipalityid;
    private int partid;
    private String BoothID;
    private String FirstName;
    private int partydistrict;
    private int SuperiorUserID;
    private String avatar;
    private String DeviceChangeCount;
    private String UniquieID;
    private int WardID;
    private String DeviceToken;
    private String Signature;
    private String LastLogin;
    private String name;
    private String PhoneNumber;
    private String LastName;
    //    private int vattamid;
    private int UserType;
    private String username;
    private String UserPermission;
    private String ConstituencyName;
    private String DistrictName;
    private String WardName;

    private UserPermissionset UserPermissionset;

    public UserDetailWing getWing() {
        return wing;
    }

    private UserDetailWing wing;
    private UserDetailRoles roles;

    public int getUniontype() {
        return uniontype;
    }

    public void setUniontype(int uniontype) {
        this.uniontype = uniontype;
    }

    private int uniontype;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourcedata() {
        return sourcedata;
    }

    public void setSourcedata(String sourcedata) {
        this.sourcedata = sourcedata;
    }

    private String source;
    private String sourcedata;


}
