package com.dci.dmkitwings.model;

import java.util.List;

public class NotificationListResponse {
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public List<NotificationListResultsItem> getResults() {
        return Results;
    }

    public void setResults(List<NotificationListResultsItem> results) {
        Results = results;
    }

    private String Status;

    public String getUsertime() {
        return usertime;
    }

    public void setUsertime(String usertime) {
        this.usertime = usertime;
    }

    private String usertime;
    private List<NotificationListResultsItem> Results;

    public int getStatuscode() {
        return Statuscode;
    }

    public void setStatuscode(int statuscode) {
        Statuscode = statuscode;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourcedata() {
        return sourcedata;
    }

    public void setSourcedata(String sourcedata) {
        this.sourcedata = sourcedata;
    }

    private int Statuscode;
    private int userid;
    private String source;
    private String sourcedata;
}