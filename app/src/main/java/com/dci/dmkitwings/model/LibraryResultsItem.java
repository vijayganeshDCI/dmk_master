package com.dci.dmkitwings.model;

public class LibraryResultsItem {
	private String bookpdf;
	private String created_at;
	private String updated_at;
	private int id;
	private String bookimage;
	private String bookname;
	private int status;

	public String getBookpdf() {
		return bookpdf;
	}

	public void setBookpdf(String bookpdf) {
		this.bookpdf = bookpdf;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBookimage() {
		return bookimage;
	}

	public void setBookimage(String bookimage) {
		this.bookimage = bookimage;
	}

	public String getBookname() {
		return bookname;
	}

	public void setBookname(String bookname) {
		this.bookname = bookname;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
