package com.dci.dmkitwings.model;

public class MunicipalityListResultsItem {
	public int getDistrictid() {
		return districtid;
	}

	public void setDistrictid(int districtid) {
		this.districtid = districtid;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getConstituencyid() {
		return constituencyid;
	}

	public void setConstituencyid(int constituencyid) {
		this.constituencyid = constituencyid;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getMunicipalitydescription() {
		return municipalitydescription;
	}

	public void setMunicipalitydescription(String municipalitydescription) {
		this.municipalitydescription = municipalitydescription;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMunicipalityname() {
		return municipalityname;
	}

	public void setMunicipalityname(String municipalityname) {
		this.municipalityname = municipalityname;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	private int districtid;
	private String updated_at;
	private int constituencyid;
	private String created_at;
	private String municipalitydescription;

	public MunicipalityListResultsItem(int id, String municipalityname) {
		this.id = id;
		this.municipalityname = municipalityname;
	}

	private int id;
	private String municipalityname;
	private int status;
}
