package com.dci.dmkitwings.model;

public class GetChatUserListResultsItem {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public GetChatUserListRoles getRoles() {
        return roles;
    }

    public void setRoles(GetChatUserListRoles roles) {
        this.roles = roles;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUniquieID() {
        return UniquieID;
    }

    public void setUniquieID(String uniquieID) {
        UniquieID = uniquieID;
    }

    public String getDeviceToken() {
        return DeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public GetChatUserListResultsItem(int id, String lastName, GetChatUserListRoles roles,
                                      String firstName, String avatar, String uniquieID, String deviceToken, String phoneNumber
            , int isSelected) {
        this.id = id;
        LastName = lastName;
        this.roles = roles;
        FirstName = firstName;
        this.avatar = avatar;
        UniquieID = uniquieID;
        DeviceToken = deviceToken;
        PhoneNumber = phoneNumber;
        this.isSelected = isSelected;
    }

    private int id;
    private String LastName;
    private GetChatUserListRoles roles;
    private String FirstName;
    private String avatar;
    private String UniquieID;
    private String DeviceToken;
    private String PhoneNumber;

    public int getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(int isSelected) {
        this.isSelected = isSelected;
    }

    private int isSelected;
}
