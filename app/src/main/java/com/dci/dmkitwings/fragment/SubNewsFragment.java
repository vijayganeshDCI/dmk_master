package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.NewsComposeActivity;
import com.dci.dmkitwings.activity.NewsDetailaActivity;
import com.dci.dmkitwings.activity.NewsLinkActivity;
import com.dci.dmkitwings.adapter.SubNewsAdpater;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.NewsListParams;
import com.dci.dmkitwings.model.NewsListResponse;
import com.dci.dmkitwings.model.NewsListResultsItem;
import com.dci.dmkitwings.model.UserStatusParam;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by vijayaganesh on 3/16/2018.
 */

public class SubNewsFragment extends BaseFragment {


    private static final String SHOWCASE_ID = "SHOWCASE_ID_NEWS";
    @BindView(R.id.list_articles)
    ListView listArticles;
    @BindView(R.id.swipe_news_line)
    SwipeRefreshLayout swipeNewsLine;
    SubNewsAdpater subNewsAdpater;
    @BindView(R.id.float_news_compose)
    FloatingActionButton floatNewsCompose;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    NewsListResponse newsListResponse;
    private ArrayList<NewsListResultsItem> newsListResultsItems;
    BaseActivity baseActivity;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    Unbinder unbinder;
    @BindView(R.id.image_no_comments)
    ImageView imageNoTimeLine;
    @BindView(R.id.text_no_comm)
    CustomTextView textNoTimeLine;
    private int preLast;
    boolean lastEnd, onCreatebool = false;
    int totalcount = 0, pagecount = 0;

    @Nullable
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        pagecount = 0;
        onCreatebool = false;
        lastEnd = false;
        preLast = 0;
        if (isVisible) {
            if (isAdded()) {
                getNewsList();
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sub_news, container, false);
        ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        baseActivity = (BaseActivity) getActivity();
        newsListResultsItems = new ArrayList<NewsListResultsItem>();
        listArticles.setDivider(null);
        newsListResultsItems.clear();
        subNewsAdpater = new SubNewsAdpater(newsListResultsItems, getActivity(), baseActivity,
                SubNewsFragment.this);
        listArticles.setAdapter(subNewsAdpater);

        listArticles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (newsListResultsItems.get(position).getLinktype() == 0) {
                    Intent intent = new Intent(getContext(), NewsDetailaActivity.class);
                    intent.putExtra("newsID", newsListResultsItems.get(position).getId());
                    intent.putExtra("districtID", newsListResultsItems.get(position).getDistrictID());
                    intent.putExtra("constituencyID", newsListResultsItems.get(position).getConstituencyID());
                    intent.putExtra("newsTypeID", newsListResultsItems.get(position).getNewstypeid());
                    intent.putExtra("newsType", newsListResultsItems.get(position).getNewstype());
                    startActivityForResult(intent, 2);
                } else {
                    Intent intent = new Intent(getContext(), NewsLinkActivity.class);
                    intent.putExtra("content", newsListResultsItems.get(position).getContent());
                    startActivityForResult(intent, 2);
                }
            }
        });

        swipeNewsLine.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeNewsLine.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            pagecount = 0;
                            onCreatebool = false;
                            lastEnd = false;
                            preLast = 0;
                            newsListResultsItems.clear();
                            if (isAdded()) {
                                showProgress();
                                getNewsList();
                            }
                            swipeNewsLine.setRefreshing(false);
                        }
                    }
                }, 000);
            }
        });
        listArticles.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (listArticles.getLastVisiblePosition() - listArticles.getHeaderViewsCount() -
                        listArticles.getFooterViewsCount()) >= (subNewsAdpater.getCount() - 1)) {

                    // Now your listview has hit the bottom
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                final int lastItem = firstVisibleItem + visibleItemCount;


                if (lastItem == totalItemCount) {
                    if (newsListResultsItems.size() >= 10) {
                        if (preLast != lastItem) {
                            try {
                                if (!lastEnd && onCreatebool) {
                                    pagecount = pagecount + 11;
                                    //LoadData(pagecount,"");
                                    if (isAdded()) {
                                        showProgress();
                                        getNewsList();
                                    }

                                    preLast = lastItem;
                                } else {
                                    //Log.d("Last", "fail");
                                }
                            } catch (Exception e) {
                            }

                        }
                    }
                }


            }
        });
        if (sharedPreferences.getString(DmkConstants.USERPERMISSION_NEWS, null).equals("0")) {
            floatNewsCompose.setVisibility(View.GONE);
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            pagecount = 0;
            onCreatebool = false;
            lastEnd = false;
            preLast = 0;
            if (isAdded()) {
                getNewsList();
            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.float_news_compose)
    public void onViewClicked() {
        Intent intent = new Intent(getActivity(), NewsComposeActivity.class);
        startActivityForResult(intent, 2);
    }

    public void getNewsList() {

        if (Util.isNetworkAvailable()) {
            //showProgress();
            // newsListResultsItems.clear();
            final NewsListParams newsListParams = new NewsListParams();
            newsListParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            newsListParams.setLimit(10);
            newsListParams.setOffset(pagecount);
            if (pagecount == 0) {
                newsListResultsItems.clear();
            }

            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getNewsList(headerMap, newsListParams).enqueue(new Callback<NewsListResponse>() {
                @Override
                public void onResponse(Call<NewsListResponse> call, Response<NewsListResponse> response) {
                    newsListResponse = response.body();
                    hideProgress();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        onCreatebool = true;
                        if (newsListResponse.getStatuscode() == 0) {
                            if (newsListResponse.getResults().size() > 0) {
                                if (isAdded()) {
                                    imageNoTimeLine.setVisibility(View.GONE);
                                    textNoTimeLine.setVisibility(View.GONE);
                                    listArticles.setVisibility(View.VISIBLE);
                                }
                                for (int u = 0; u < newsListResponse.getResults().size(); u++) {
                                    newsListResultsItems.add(newsListResponse.getResults().get(u));
                                }
                                subNewsAdpater.notifyDataSetChanged();
                                hideProgress();
                            } else {
                                if (newsListResultsItems.size() == 0) {
                                    imageNoTimeLine.setVisibility(View.VISIBLE);
                                    imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                                    textNoTimeLine.setText(R.string.no_news_line);
                                    textNoTimeLine.setVisibility(View.VISIBLE);
                                    listArticles.setVisibility(View.GONE);
                                } else {
                                    lastEnd = true;
                                }


                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, newsListResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, newsListResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(newsListResponse.getSource(),
                                    newsListResponse.getSourcedata()));
                            editor.commit();
                            getNewsList();
                        }
                    } else {
                        if (newsListResultsItems.size() == 0) {
                            imageNoTimeLine.setVisibility(View.VISIBLE);
                            imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                            textNoTimeLine.setText(R.string.no_news_line);
                            textNoTimeLine.setVisibility(View.VISIBLE);
                            listArticles.setVisibility(View.GONE);

                            //Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                        } else {
                            //Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<NewsListResponse> call, Throwable t) {
                    hideProgress();
                    if (newsListResultsItems.size() == 0) {
                        imageNoTimeLine.setVisibility(View.VISIBLE);
                        imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                        textNoTimeLine.setText(R.string.no_news_line);
                        textNoTimeLine.setVisibility(View.VISIBLE);
                        listArticles.setVisibility(View.GONE);
                        //Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    } else {
                        // Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }


                }
            });
        } else {
            hideProgress();
            if (isAdded()) {
                imageNoTimeLine.setVisibility(View.VISIBLE);
                textNoTimeLine.setVisibility(View.VISIBLE);
                imageNoTimeLine.setImageResource(R.mipmap.icon_no_network);
                textNoTimeLine.setText(R.string.no_network);
                listArticles.setVisibility(View.GONE);
            }
        }
    }
}
