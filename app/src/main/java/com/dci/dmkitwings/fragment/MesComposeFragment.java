package com.dci.dmkitwings.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.MessageComposeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.jzvd.JZVideoPlayerStandard;

/**
 * Created by vijayaganesh on 4/12/2018.
 */

public class MesComposeFragment extends BaseFragment {

    @BindView(R.id.edit_title)
    EditText editTitle;
    @BindView(R.id.view_time_compose)
    View viewTimeCompose;
    @BindView(R.id.edit_time_sub)
    EditText editTimeSub;
    @BindView(R.id.image_selected)
    ImageView imageSelected;
    @BindView(R.id.video_selected)
    JZVideoPlayerStandard videoSelected;
    @BindView(R.id.image_photo)
    ImageView imagePhoto;
    @BindView(R.id.image_video)
    ImageView imageVideo;
    @BindView(R.id.image_audio)
    ImageView imageAudio;
    @BindView(R.id.image_attach_file)
    ImageView imageAttachFile;
    @BindView(R.id.image_send)
    ImageView imageSend;
    @BindView(R.id.image_send1)
    ImageView imageSend1;
    @BindView(R.id.cons_send)
    ConstraintLayout consSend;
    @BindView(R.id.cons_bottom_view)
    ConstraintLayout consBottomView;
    @BindView(R.id.cons_mes_compose)
    ConstraintLayout consMesCompose;
    Unbinder unbinder;
    MessageComposeActivity homeMessageActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mes_compose, container, false);
        unbinder = ButterKnife.bind(this, view);
        homeMessageActivity= (MessageComposeActivity) getActivity();
        Bundle bundle=getArguments();
        if (bundle!=null){
            homeMessageActivity.textTitle.setText(bundle.getString("name"));
        }
        setHasOptionsMenu(true);
        homeMessageActivity.mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        homeMessageActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        homeMessageActivity.getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        homeMessageActivity.getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        homeMessageActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.image_photo, R.id.image_video, R.id.image_audio, R.id.image_attach_file, R.id.image_send1})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_photo:
                break;
            case R.id.image_video:
                break;
            case R.id.image_audio:
                break;
            case R.id.image_attach_file:
                break;
            case R.id.image_send1:
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_save:
                //feedbackNullCheck();
                Toast.makeText(getContext(),"Under Development",Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
