package com.dci.dmkitwings.model;

public class NotificationListResultsItem {
    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public NotificationListResultsItem(String created_at, int id, String title, int type, int viewStatus) {
        this.created_at = created_at;
        this.id = id;
        this.title = title;
        this.type = type;
        this.viewStatus = viewStatus;
    }

    public int getViewStatus() {
        return viewStatus;
    }

    public void setViewStatus(int viewStatus) {
        this.viewStatus = viewStatus;
    }

    private int viewStatus;
    private String created_at;
    private int id;
    private String title;
    private int type;
}
