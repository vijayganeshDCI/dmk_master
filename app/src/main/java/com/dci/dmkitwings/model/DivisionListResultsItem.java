package com.dci.dmkitwings.model;

public class DivisionListResultsItem {

	public DivisionListResultsItem(String divisionname, int id) {
		this.divisionname = divisionname;
		this.id = id;
	}

	private String divisionname;
	private int id;

	public void setDivisionname(String divisionname){
		this.divisionname = divisionname;
	}

	public String getDivisionname(){
		return divisionname;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}
