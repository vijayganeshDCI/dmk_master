package com.dci.dmkitwings.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.LoginActivity;
import com.dci.dmkitwings.activity.SettingsActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.view.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by vijayaganesh on 4/13/2018.
 */

public class SettingsFragment extends BaseFragment {

    @BindView(R.id.text_label_settings)
    TextView textLabelSettings;
    @BindView(R.id.text_selected_language)
    TextView textSelectedLanguage;
    @BindView(R.id.view_bottom_1)
    View viewBottom1;
    @BindView(R.id.text_label_privacy)
    TextView textLabelPrivacy;
    @BindView(R.id.view_bottom_2)
    View viewBottom2;
    @BindView(R.id.text_label_notification)
    TextView textLabelNotification;
    @BindView(R.id.view_bottom_3)
    View viewBottom3;
    @BindView(R.id.switch_notify)
    Switch switchNotify;
    @BindView(R.id.cons_settings)
    ConstraintLayout consSettings;
    Unbinder unbinder;
    SettingsActivity settingsActivity;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @BindView(R.id.text_label_logout)
    CustomTextView textLabelLogout;
    @BindView(R.id.view_bottom_4)
    View viewBottom4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, view);
        settingsActivity = (SettingsActivity) getActivity();
        //Navigation Control
        settingsActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        Show Activity Logo
        settingsActivity.getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        settingsActivity.getSupportActionBar().setHomeButtonEnabled(true);
//        ActionBar title
        settingsActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        settingsActivity.toolbarHome.setNavigationIcon(R.mipmap.icon_nav_menu_toggle_dmk);
        ((SettingsActivity) getActivity()).textTitle.setText(getString(R.string.settings));
        DmkApplication.getContext().getComponent().inject(this);

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (sharedPreferences.getInt(DmkConstants.SELECTEDLANGUAGES, 1) == 0) {
            textSelectedLanguage.setText(R.string.english);
        } else {
            textSelectedLanguage.setText(R.string.tamil);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.text_label_settings, R.id.text_selected_language, R.id.text_label_privacy, R.id.text_label_logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_label_settings:
                settingsActivity.push(new LanguageFragment(), "langauge");
                break;
            case R.id.text_selected_language:
                settingsActivity.push(new LanguageFragment(), "langauge");
                break;
            case R.id.text_label_privacy:
                settingsActivity.push(new PrivacyPolicyFragment(), "privacy");
                break;
            case R.id.text_label_logout:
                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                alertDialog1.setMessage(getString(R.string.confirm_to_exit));
                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent_admin_logout = new Intent(getActivity(), LoginActivity.class);
//                        sharedPreferences.edit().clear().commit();
                        intent_admin_logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        getActivity().finish();
                        startActivity(intent_admin_logout);
                    }
                });
                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog1.show();

                break;
        }
    }
}
