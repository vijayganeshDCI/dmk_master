package com.dci.dmkitwings.model;

import java.util.List;

public class MunicipalityListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<MunicipalityListResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<MunicipalityListResultsItem> results) {
		Results = results;
	}

	private String Status;
	private List<MunicipalityListResultsItem> Results;
	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}