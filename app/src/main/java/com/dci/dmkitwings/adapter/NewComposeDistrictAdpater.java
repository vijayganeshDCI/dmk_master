package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.DistrictsItem;
import com.dci.dmkitwings.view.CustomCheckBox;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewComposeDistrictAdpater extends BaseAdapter {

    private final LayoutInflater mInflater;
    List<DistrictsItem> districtsItemList;
    Context context;
    public ArrayList<String> arraylistDistrictId=new ArrayList<String>();
    public ArrayList<String> arraylistDistrictName=new ArrayList<String>();


    public ArrayList<String> arraylistDistrictIdTest=new ArrayList<String>();
    public NewComposeDistrictAdpater(List<DistrictsItem> districtsItemList, Context context, ArrayList<String> arraylistDistrictIdTest ) {
        this.districtsItemList = districtsItemList;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arraylistDistrictIdTest=arraylistDistrictIdTest;






    }


    @Override
    public int getCount() {
        return districtsItemList.size();
    }

    @Override
    public DistrictsItem getItem(int position) {
        return districtsItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_spinner_item_checkbox, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();

        }

        //viewHolder.text_Checkbox.setText(districtsItemList.get(position).getDistrictName());
        viewHolder.checkboxSpinnerItemOne.setText(districtsItemList.get(position).getDistrictName());
        viewHolder.checkboxSpinnerItemOne.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    if (Arrays.asList(arraylistDistrictId).contains(districtsItemList.get(position).getId())) {
                        // true
                        arraylistDistrictId.remove(String.valueOf(districtsItemList.get(position).getId()));
                        arraylistDistrictId.add(String.valueOf(districtsItemList.get(position).getId()));
                        Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(arraylistDistrictId);
                        arraylistDistrictId.clear();
                        arraylistDistrictId.addAll(primesWithoutDuplicates);
                        arraylistDistrictName.remove(districtsItemList.get(position).getDistrictName());
                        arraylistDistrictName.add(districtsItemList.get(position).getDistrictName());
                        Set<String> primesWithoutConstituency = new LinkedHashSet<String>(arraylistDistrictName);
                        arraylistDistrictName.clear();
                        arraylistDistrictName.addAll(primesWithoutConstituency);

                    }
                    else
                    {
                        arraylistDistrictId.add(String.valueOf(districtsItemList.get(position).getId()));
                        Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(arraylistDistrictId);
                        arraylistDistrictId.clear();
                        arraylistDistrictId.addAll(primesWithoutDuplicates);
                        arraylistDistrictName.add(districtsItemList.get(position).getDistrictName());
                        Set<String> primesWithoutConstituency = new LinkedHashSet<String>(arraylistDistrictName);
                        arraylistDistrictName.clear();
                        arraylistDistrictName.addAll(primesWithoutConstituency);

                    }
                }
                else
                {
                    arraylistDistrictId.remove(String.valueOf(districtsItemList.get(position).getId()));
                    Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(arraylistDistrictId);
                    arraylistDistrictId.clear();
                    arraylistDistrictId.addAll(primesWithoutDuplicates);
                    arraylistDistrictName.remove(districtsItemList.get(position).getDistrictName());
                    Set<String> primesWithoutConstituency = new LinkedHashSet<String>(arraylistDistrictName);
                    arraylistDistrictName.clear();
                    arraylistDistrictName.addAll(primesWithoutConstituency);


                }
            }
        });
        if (arraylistDistrictIdTest.size()>0) {
            for (int y = 0; y < arraylistDistrictIdTest.size(); y++) {
                int DisID = Integer.parseInt(arraylistDistrictIdTest.get(y));

                //viewHolder.checkboxSpinnerItemOne.setChecked(districtsItemList.get(position).getId()==DisID);
                if (districtsItemList.get(position).getId() == DisID) {
                    viewHolder.checkboxSpinnerItemOne.setChecked(districtsItemList.get(position).getId() == DisID);

                } else {
                    Log.d("Water", "else");
                }


            }

        }


        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.checkbox_spinner_item_one)
        CheckBox checkboxSpinnerItemOne;
        @BindView(R.id.text_checkbox)
        TextView text_Checkbox;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
