package com.dci.dmkitwings.model;

import java.util.List;

public class NewsListResultsItem  {
	private String LastUpdatedBy;
	private int Status;
	private int CreatedBy;
	private String PublishedOn;
	private int DistrictID;
	private String Title;
	private String created_at;


	public List<String> getMediapath() {
		return Mediapath;
	}

	public void setMediapath(List<String> mediapath) {
		Mediapath = mediapath;
	}

	private List<String> Mediapath;
	private int PublishedBy;
	private int PostAdminId;
	private int Mediatype;
	private String updated_at;
	private String Content;
	private int id;
	private int ConstituencyID;
	private NewListUser user;
	private String Newstypeid;
	private String Newstype;
	private int Linktype;

	public int getLinktype() {
		return Linktype;
	}

	public void setLinktype(int linktype) {
		Linktype = linktype;
	}

	public String getNewstype() {
		return Newstype;
	}

	public void setNewstype(String newstype) {
		Newstype = newstype;
	}

	public String getNewstypeid() {
		return Newstypeid;
	}

	public void setNewstypeid(String newstypeid) {
		Newstypeid = newstypeid;
	}

	public NewListUser getUser() {
		return user;
	}

	public void setUser(NewListUser user) {
		this.user = user;
	}

	public String getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public int getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(int createdBy) {
		CreatedBy = createdBy;
	}

	public String getPublishedOn() {
		return PublishedOn;
	}

	public void setPublishedOn(String publishedOn) {
		PublishedOn = publishedOn;
	}

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}



	public int getPublishedBy() {
		return PublishedBy;
	}

	public void setPublishedBy(int publishedBy) {
		PublishedBy = publishedBy;
	}

	public int getPostAdminId() {
		return PostAdminId;
	}

	public void setPostAdminId(int postAdminId) {
		PostAdminId = postAdminId;
	}

	public int getMediatype() {
		return Mediatype;
	}

	public void setMediatype(int mediatype) {
		Mediatype = mediatype;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getConstituencyID() {
		return ConstituencyID;
	}

	public void setConstituencyID(int constituencyID) {
		ConstituencyID = constituencyID;
	}
}
