package com.dci.dmkitwings.model;

import java.util.List;

public class TownPanchayatListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<TownPanchayatResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<TownPanchayatResultsItem> results) {
		Results = results;
	}

	private String Status;
	private List<TownPanchayatResultsItem> Results;
	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}