package com.dci.dmkitwings.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by vijayaganesh on 4/9/2018.
 */

public class MurasoliFragment extends BaseFragment {

    @BindView(R.id.webview_murasoli)
    WebView webviewMurasoli;
    Unbinder unbinder;
    @Nullable
    BaseActivity baseActivity;
    @BindView(R.id.image_no_network)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_network)
    CustomTextView textNoNetwork;
    @BindView(R.id.cons_notification)
    ConstraintLayout consNotification;
    private Boolean isStarted = false;
    private Boolean isVisible = false;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible)
            loadmurasoli();

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_murasoli, container, false);
        unbinder = ButterKnife.bind(this, view);
        WebSettings webSettings = webviewMurasoli.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowContentAccess(true);
        WebViewClient webViewClient = new WebViewClient();
        webviewMurasoli.setWebViewClient(webViewClient);
        webviewMurasoli.getSettings().setJavaScriptEnabled(true);

        webviewMurasoli.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                hideProgress();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                hideProgress();

            }
        });


        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted)
            loadmurasoli();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void loadmurasoli() {
        if (Util.isNetworkAvailable()) {
            if (isAdded()) {
                imageNoNetwork.setVisibility(View.GONE);
                textNoNetwork.setVisibility(View.GONE);
                webviewMurasoli.setVisibility(View.VISIBLE);
            }
            showProgress();
            webviewMurasoli.loadUrl("http://www.murasoli.in/epaper/");
        } else {
            if (isAdded()) {
                imageNoNetwork.setVisibility(View.VISIBLE);
                textNoNetwork.setVisibility(View.VISIBLE);
                imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
                textNoNetwork.setText(R.string.no_network);
                webviewMurasoli.setVisibility(View.GONE);
            }
        }
    }
}
