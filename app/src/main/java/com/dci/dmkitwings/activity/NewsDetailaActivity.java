package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.ImageViewPageAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.NewsDeletePostParams;
import com.dci.dmkitwings.model.NewsDeleteResponse;
import com.dci.dmkitwings.model.NewsDetailParams;
import com.dci.dmkitwings.model.NewsDetailsResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.dci.dmkitwings.view.CustomTextViewBold;
import com.firebase.client.utilities.Base64;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.jzvd.JZVideoPlayerStandard;
import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.dmkitwings.app.DmkApplication.getContext;
import static com.dci.dmkitwings.app.DmkApplication.getProxy;

public class NewsDetailaActivity extends BaseActivity {
    @BindView(R.id.image_time)
    ImageView imageTime;
    @BindView(R.id.videoplayer_time)
    JZVideoPlayerStandard videoplayerTime;
    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    @BindView(R.id.text_posted_by)
    CustomTextView textPostedBy;
    @BindView(R.id.text_designation)
    CustomTextViewBold textDesignation;
    @BindView(R.id.text_post_date)
    CustomTextView textPostDate;
    @BindView(R.id.text_label_time_line_title)
    CustomTextViewBold textLabelTimeLineTitle;
    @BindView(R.id.text_label_time_line_content)
    CustomTextView textLabelTimeLineContent;

    Unbinder unbinder;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;
    int newsid, districtID, constituencyID;
    String newsType, newsTypeID;
    NewsDetailsResponse newsDetailsResponse;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.view_bottom_1)
    View viewBottom1;
    @BindView(R.id.cons_time_item)
    ConstraintLayout consTimeItem;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;
    @BindView(R.id.image_menu)
    ImageView imageMenu;
    @BindView(R.id.image_view_pager)
    ViewPager imageViewPager;
    @BindView(R.id.view_pager_indicator)
    CircleIndicator viewPagerIndicator;
    @BindView(R.id.videoplayer_detail)
    JZVideoPlayerStandard videoplayerDetail;
    private NewsDeleteResponse newsDeleteResponse;
    private HttpProxyCacheServer proxy;
    ImageViewPageAdapter imageViewPageAdapter;
    @BindView(R.id.image_share)
    ImageView imageshare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_news_detail, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        frameLayoutNotification.setVisibility(View.GONE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toolbarHome.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        proxy = getProxy(this);
        Intent bundle = getIntent();
        if (getIntent() != null) {
            newsid = bundle.getIntExtra("newsID", 0);
            districtID = bundle.getIntExtra("districtID", 0);
            constituencyID = bundle.getIntExtra("constituencyID", 0);
            newsTypeID = bundle.getStringExtra("newsTypeID");
            newsType = bundle.getStringExtra("newsType");
        }
        videoplayerTime.setVisibility(View.GONE);
        videoplayerDetail.setVisibility(View.GONE);


    }

    @Override
    public void onResume() {
        super.onResume();
        getDetailNews();
    }

    private void getDetailNews() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            final NewsDetailParams newsDetailParams = new NewsDetailParams();
            newsDetailParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            newsDetailParams.setNewsid(newsid);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getNewsDetails(headerMap, newsDetailParams).enqueue(new Callback<NewsDetailsResponse>() {
                @Override
                public void onResponse(Call<NewsDetailsResponse> call, Response<NewsDetailsResponse> response) {
                    hideProgress();
                    newsDetailsResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (newsDetailsResponse.getStatuscode() == 0) {
                            textDesignation.setVisibility(View.GONE);
                            textPostedBy.setVisibility(View.GONE);
                            textPostDate.setText(getDateFormat(newsDetailsResponse.getResults().getCreated_at()));
                            if (newsDetailsResponse.getResults().getUser().getId() ==
                                    sharedPreferences.getInt(DmkConstants.USERID, 0)) {
                                textPostedBy.setText("You");
                                imageMenu.setVisibility(View.VISIBLE);
                            } else {
                                textPostedBy.setText(newsDetailsResponse.getResults().getUser().getFirstName() + " " +
                                        newsDetailsResponse.getResults().getUser().getLastName());
                                imageMenu.setVisibility(View.GONE);

                            }
                            if (newsDetailsResponse.getResults().getUser().getId() != 1) {
                                textDesignation.setVisibility(View.GONE);
                                textDesignation.setText(newsDetailsResponse.getResults().getUser().getDesignation() + " " +
                                        getDateFormat(newsDetailsResponse.getResults().getCreated_at()));
                            }
                            textLabelTimeLineTitle.setText(newsDetailsResponse.getResults().getTitle());
                            textLabelTimeLineContent.setText(newsDetailsResponse.getResults().getContent());


                            universalImageLoader(imageProPic, getString(R.string.s3_bucket_profile_path),
                                    newsDetailsResponse.getResults().getUser().getAvatar(),
                                    R.mipmap.icon_pro_image_loading_256,
                                    R.mipmap.icon_pro_image_loading_256);

                            List<String> imageFiles = new ArrayList<String>();
                            List<String> videoFile = new ArrayList<String>();


                            for (int i = 0; i < newsDetailsResponse.getResults().getMediapath().size(); i++) {
                                if (isImageFile(newsDetailsResponse.getResults().getMediapath().get(i))) {
                                    imageFiles.add(newsDetailsResponse.getResults().getMediapath().get(i));
                                } else {
                                    videoFile.add(newsDetailsResponse.getResults().getMediapath().get(i));
                                }
                            }

                            if (imageFiles.size() > 0 && videoFile.size() > 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.VISIBLE);
                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            NewsDetailaActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);

                                videoplayerDetail.setUp(proxy.getProxyUrl(
                                        getString(R.string.s3_image_url_download) +
                                                getString(R.string.s3_bucket_news_path) +
                                                videoFile.get(0))
                                        , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                                videoplayerDetail.setSoundEffectsEnabled(true);
                                //thumbnail generation
                                if (isAudioFile(getString(R.string.s3_image_url_download) +
                                        getString(R.string.s3_bucket_news_path) +
                                        videoFile.get(0))) {
                                    Glide.with(NewsDetailaActivity.this)
                                            .load(R.mipmap.icon_audio_thumbnail).into(videoplayerDetail.thumbImageView);
                                } else {
                                    RequestOptions requestOptions = new RequestOptions();
                                    requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                                    requestOptions.error(R.mipmap.icon_video_thumbnail);
                                    Glide.with(getContext())
                                            .load(getString(R.string.s3_image_url_download) +
                                                    getString(R.string.s3_bucket_news_path) +
                                                    videoFile.get(0))
                                            .apply(requestOptions)
                                            .thumbnail(Glide.with(getContext()).load(getString(R.string.s3_image_url_download) +
                                                    getString(R.string.s3_bucket_news_path) +
                                                    videoFile.get(0)))
                                            .into(videoplayerDetail.thumbImageView);
                                }

                            } else if (imageFiles.size() == 0 && videoFile.size() > 0) {
                                imageViewPager.setVisibility(View.INVISIBLE);
                                videoplayerTime.setVisibility(View.VISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                videoplayerTime.setUp(proxy.getProxyUrl(getString(R.string.s3_image_url_download) +
                                                getString(R.string.s3_bucket_news_path) +
                                                videoFile.get(0))
                                        , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                                videoplayerTime.setSoundEffectsEnabled(true);
                                //thumbnail generation
                                if (isAudioFile(getString(R.string.s3_image_url_download) +
                                        getString(R.string.s3_bucket_news_path) +
                                        videoFile.get(0))) {
                                    Glide.with(NewsDetailaActivity.this)
                                            .load(R.mipmap.icon_audio_thumbnail).into(videoplayerTime.thumbImageView);
                                } else {
                                    RequestOptions requestOptions = new RequestOptions();
                                    requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                                    requestOptions.error(R.mipmap.icon_video_thumbnail);
                                    Glide.with(getContext())
                                            .load(getString(R.string.s3_image_url_download) +
                                                    getString(R.string.s3_bucket_news_path) +
                                                    videoFile.get(0))
                                            .apply(requestOptions)
                                            .thumbnail(Glide.with(getContext()).
                                                    load(getString(R.string.s3_image_url_download) +
                                                            getString(R.string.s3_bucket_news_path) +
                                                            videoFile.get(0)))
                                            .into(videoplayerTime.thumbImageView);
                                }

                            } else if (imageFiles.size() > 0 && videoFile.size() == 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            NewsDetailaActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);
                                //
                            } else if (imageFiles.size() == 0 && videoFile.size() == 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                imageFiles.add("");
                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            NewsDetailaActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, newsDetailsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, newsDetailsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(newsDetailsResponse.getSource(),
                                    newsDetailsResponse.getSourcedata()));
                            editor.commit();
                            getDetailNews();
                        }

                    } else {
                        Toast.makeText(NewsDetailaActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<NewsDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(NewsDetailaActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            Toast.makeText(NewsDetailaActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }


    @OnClick({R.id.image_menu, R.id.image_share, R.id.image_pro_pic})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_menu:
                PopupMenu popup = new PopupMenu(this, imageMenu);
                //inflating menu_icon from xml resource
                popup.inflate(R.menu.comment_item_menu);
                //adding click listener


                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit:
                                //handle menu1 click
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(NewsDetailaActivity.this);
                                alertDialog.setMessage(R.string.confirm_to_edit);
                                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(NewsDetailaActivity.this, NewsComposeActivity.class);
                                        intent.putExtra("newsID", newsid);
                                        intent.putExtra("newsTitle", textLabelTimeLineTitle.getText().toString());
                                        intent.putExtra("newsContent", textLabelTimeLineContent.getText().toString());
                                        intent.putExtra("newsDistrict", districtID);
                                        intent.putExtra("newsConstituency", constituencyID);
                                        intent.putExtra("Newstypeid", newsTypeID);
                                        intent.putExtra("Newstype", newsType);
                                        intent.putExtra("fromNews", true);
                                        startActivity(intent);
                                    }

                                });
                                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                                return true;
                            case R.id.delete:
                                //handle menu2 click
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(NewsDetailaActivity.this);
                                alertDialog1.setMessage(R.string.confirm_to_delete);
                                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        deleteNews(newsid);
                                    }

                                });
                                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog1.show();
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
                break;

            case R.id.image_share:
                String ps = "Dmk_encryption " + newsDetailsResponse.getResults().getId();
                String tmp = Base64.encodeBytes(ps.getBytes());
                tmp = Base64.encodeBytes(tmp.getBytes());
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, newsDetailsResponse.getResults().getTitle() + "\n" + getString(R.string.for_more) +
                        getString(R.string.deeplink_url) + "news/" + tmp);
                startActivity(Intent.createChooser(i, "Choose share option"));
                break;

            case R.id.image_pro_pic:
                BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.profile_bubble, null);
                PopupWindow popupWindow = BubblePopupHelper.create(this, bubbleLayout);
                CircleImageView imageprofilepic = (CircleImageView) bubbleLayout.findViewById(R.id.image_pro_pic_bubble);
                CustomTextView textpostedby = (CustomTextView) bubbleLayout.findViewById(R.id.text_posted_by_bubble);
                CustomTextView textdesgination = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_designation_bubble);
                CustomTextView textdistrict = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_district_bubble);
                CustomTextView textconstituency = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_constituency_bubble);
                CustomTextView textPartBoothWardVillage = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_common_bubble);
                if (newsDetailsResponse.getResults().getUser().getId() != 1 && newsDetailsResponse.getResults().getUser().getId() != sharedPreferences.getInt(DmkConstants.USERID, 0)) {
                    switch (newsDetailsResponse.getResults().getUser().getLevel()) {
                        case 1:

                            // செயலாளர்
//                            துணை செயலாளர்
//                            மண்டல ஒருங்கிணைப்பாளர்
                            textpostedby.setText(newsDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    newsDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(newsDetailsResponse.getResults().getUser().getDesignation());
                            textdistrict.setVisibility(View.GONE);
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 2:
                            //மாவட்ட ஒருங்கிணைப்பாளர் district
                            textpostedby.setText(newsDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    newsDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(newsDetailsResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(newsDetailsResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 3:
                            //தொகுதி ஒருங்கிணைப்பாளர் constituency
                            textpostedby.setText(newsDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    newsDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(newsDetailsResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(newsDetailsResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setText(newsDetailsResponse.getResults().getUser().getConstituency().getConstituencyName());
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                            //பகுதி ஒருங்கிணைப்பாளர் part
                            //நகராட்சி ஒருங்கிணைப்பாளர் municipality
                            //பேரூராட்சி ஒருங்கிணைப்பாளர் township
                            //ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                            textpostedby.setText(newsDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    newsDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(newsDetailsResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(newsDetailsResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setText(newsDetailsResponse.getResults().getUser().getConstituency().getConstituencyName());
                            textPartBoothWardVillage.setText(newsDetailsResponse.getResults().getUser().getPart().getPartname());
                            break;
                        case 5:
                        case 6:
                        case 9:
                        case 10:
                        case 13:
                        case 14:
                        case 18:
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            //கிளை ஒருங்கிணைப்பாளர் ward
                            // வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //கிளை ஒருங்கிணைப்பாளர்  ward
                            textpostedby.setText(newsDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    newsDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(newsDetailsResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(newsDetailsResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setText(newsDetailsResponse.getResults().getUser().getConstituency().getConstituencyName());
                            textPartBoothWardVillage.setText(newsDetailsResponse.getResults().getUser().getWard().getWardname());
                            break;
                        case 7:
                        case 11:
                        case 15:
                        case 19:
                            //வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                            textpostedby.setText(newsDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    newsDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(newsDetailsResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(newsDetailsResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setText(newsDetailsResponse.getResults().getUser().getConstituency().getConstituencyName());
                            textPartBoothWardVillage.setText(newsDetailsResponse.getResults().getUser().getBooth().getBoothname());
                            break;
                        case 17:
                            //ராட்சிஊ ஒருங்கிணைப்பாளர் villagepanchayat
                            textpostedby.setText(newsDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    newsDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(newsDetailsResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(newsDetailsResponse.getResults().getUser().getDistrict().getDistrictName());
                            textconstituency.setText(newsDetailsResponse.getResults().getUser().getConstituency().getConstituencyName());
                            textPartBoothWardVillage.setText(newsDetailsResponse.getResults().getUser().getVillage().getVillageName());
                            break;

                    }


                    final Random random = new Random();
                    int[] location = new int[2];
                    imageProPic.getLocationInWindow(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.LEFT);
                    popupWindow.showAtLocation(imageProPic, Gravity.AXIS_PULL_BEFORE, imageProPic.getWidth() + 25, -185);
                    universalImageLoader(imageprofilepic, getString(R.string.s3_bucket_profile_path),
                            newsDetailsResponse.getResults().getUser().getAvatar(),
                            R.mipmap.icon_pro_image_loading_256,
                            R.mipmap.icon_pro_image_loading_256);

                    break;

                }
        }

    }

    private void deleteNews(final int newsID) {
        NewsDeletePostParams newsDeletePostParams = new NewsDeletePostParams();
        newsDeletePostParams.setNewsid(newsID);
        newsDeletePostParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.deletenews(headerMap, newsDeletePostParams).enqueue(new Callback<NewsDeleteResponse>() {
                @Override
                public void onResponse(Call<NewsDeleteResponse> call, Response<NewsDeleteResponse> response) {
                    newsDeleteResponse = response.body();
                    hideProgress();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (newsDeleteResponse.getStatuscode() == 0) {
                            if (newsDeleteResponse.getStatus().contains("Success")) {
                                Toast.makeText(NewsDetailaActivity.this, getString(R.string.news_delete_sucuess), Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(NewsDetailaActivity.this, getString(R.string.news_delete_failed), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, newsDeleteResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, newsDeleteResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(newsDeleteResponse.getSource(),
                                    newsDeleteResponse.getSourcedata()));
                            editor.commit();
                            deleteNews(newsID);
                        }
                    } else {
                        Toast.makeText(NewsDetailaActivity.this, getString(R.string.news_delete_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<NewsDeleteResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(NewsDetailaActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(NewsDetailaActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        setResult(1, intent);
        finish();
    }

}
