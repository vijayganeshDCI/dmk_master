package com.dci.dmkitwings.model;

import java.util.List;

public class ArticleListResultsItem {
	private int Articlestype;
	private int DistrictID;
	private String Content;
	private String Title;
	private String created_at;
	private List<String> Mediapath;
	private int id;
	private String MediaFilesPath;
	private int Mediatype;
	private int ConstituencyID;
	private ArticleListUser user;

	public ArticleListUser getUser() {
		return user;
	}

	public void setUser(ArticleListUser user) {
		this.user = user;
	}

	private String Articlestypeid;

	public int getArticlestype() {
		return Articlestype;
	}

	public void setArticlestype(int articlestype) {
		Articlestype = articlestype;
	}

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public List<String> getMediapath() {
		return Mediapath;
	}

	public void setMediapath(List<String> mediapath) {
		Mediapath = mediapath;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMediaFilesPath() {
		return MediaFilesPath;
	}

	public void setMediaFilesPath(String mediaFilesPath) {
		MediaFilesPath = mediaFilesPath;
	}

	public int getMediatype() {
		return Mediatype;
	}

	public void setMediatype(int mediatype) {
		Mediatype = mediatype;
	}

	public int getConstituencyID() {
		return ConstituencyID;
	}

	public void setConstituencyID(int constituencyID) {
		ConstituencyID = constituencyID;
	}

	public String getArticlestypeid() {
		return Articlestypeid;
	}

	public void setArticlestypeid(String articlestypeid) {
		Articlestypeid = articlestypeid;
	}
}