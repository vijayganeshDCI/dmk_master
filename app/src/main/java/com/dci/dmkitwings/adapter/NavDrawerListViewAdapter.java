package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.NavDrawerItem;

import java.util.ArrayList;


public class NavDrawerListViewAdapter extends BaseAdapter {


    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;

    public NavDrawerListViewAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.image_item);
        ImageView imgUnRead = (ImageView) convertView.findViewById(R.id.image_mes_notify);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.text_label_item);
        imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
        txtTitle.setText(navDrawerItems.get(position).getTitle());

        if (position == 2) {
            if (navDrawerItems.get(2).getChatReadStatus() == 0)
                imgUnRead.setVisibility(View.GONE);
            else
                imgUnRead.setVisibility(View.GONE);
        } else {
            imgUnRead.setVisibility(View.GONE);
        }
        return convertView;
    }


}
