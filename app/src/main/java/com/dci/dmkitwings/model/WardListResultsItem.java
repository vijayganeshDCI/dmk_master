package com.dci.dmkitwings.model;

public class WardListResultsItem{
	public String getWardname() {
		return wardname;
	}

	public void setWardname(String wardname) {
		this.wardname = wardname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String wardname;
	private int id;
	private String districtid;
	private String constituencyid;
	private String divisionid;
	private String partid;
	private String municipalityid;
	private String townid;
	private String vattamid;
	private String wardcode;
	private String status;
	private String unionid;
	private String villageid;
	private String created_at;
	private String updated_at;

	public String getDistrictid() {
		return districtid;
	}

	public void setDistrictid(String districtid) {
		this.districtid = districtid;
	}

	public String getConstituencyid() {
		return constituencyid;
	}

	public void setConstituencyid(String constituencyid) {
		this.constituencyid = constituencyid;
	}

	public String getDivisionid() {
		return divisionid;
	}

	public void setDivisionid(String divisionid) {
		this.divisionid = divisionid;
	}

	public String getPartid() {
		return partid;
	}

	public void setPartid(String partid) {
		this.partid = partid;
	}

	public String getMunicipalityid() {
		return municipalityid;
	}

	public void setMunicipalityid(String municipalityid) {
		this.municipalityid = municipalityid;
	}

	public String getTownid() {
		return townid;
	}

	public void setTownid(String townid) {
		this.townid = townid;
	}

	public String getVattamid() {
		return vattamid;
	}

	public void setVattamid(String vattamid) {
		this.vattamid = vattamid;
	}

	public String getWardcode() {
		return wardcode;
	}

	public void setWardcode(String wardcode) {
		this.wardcode = wardcode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public String getVillageid() {
		return villageid;
	}

	public void setVillageid(String villageid) {
		this.villageid = villageid;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public WardListResultsItem(String wardname, int id) {
		this.wardname = wardname;
		this.id = id;
	}




}
