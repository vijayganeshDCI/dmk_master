package com.dci.dmkitwings.model;

public class VattamResultsItem {
	public VattamResultsItem(int id, String vattamname) {
		this.id = id;
		this.vattamname = vattamname;
	}

	public String getMunicipalityid() {

		return municipalityid;
	}

	public void setMunicipalityid(String municipalityid) {
		this.municipalityid = municipalityid;
	}

	public int getDistrictid() {
		return districtid;
	}

	public void setDistrictid(int districtid) {
		this.districtid = districtid;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getPartid() {
		return partid;
	}

	public void setPartid(String partid) {
		this.partid = partid;
	}

	public int getConstituencyid() {
		return constituencyid;
	}

	public void setConstituencyid(int constituencyid) {
		this.constituencyid = constituencyid;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDivisionid() {
		return divisionid;
	}

	public void setDivisionid(int divisionid) {
		this.divisionid = divisionid;
	}

	public String getTownid() {
		return townid;
	}

	public void setTownid(String townid) {
		this.townid = townid;
	}

	public String getVattamcode() {
		return vattamcode;
	}

	public void setVattamcode(String vattamcode) {
		this.vattamcode = vattamcode;
	}

	public String getVattamname() {
		return vattamname;
	}

	public void setVattamname(String vattamname) {
		this.vattamname = vattamname;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	private String municipalityid;
	private int districtid;
	private String updated_at;
	private String partid;
	private int constituencyid;
	private String created_at;
	private int id;
	private int divisionid;
	private String townid;
	private String vattamcode;
	private String vattamname;
	private int status;



}
