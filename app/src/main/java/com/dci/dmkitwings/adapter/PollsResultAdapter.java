package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.PollSaveParams;
import com.dci.dmkitwings.model.QuestionslistItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 4/16/2018.
 */

public class PollsResultAdapter extends BaseAdapter {


    public PollsResultAdapter(List<QuestionslistItem> pollListResultsItemArrayList, List<QuestionslistItem> pollListResultsItemArrayListResult, Context context) {
        this.pollListResultsItemArrayList = pollListResultsItemArrayList;
        this.pollListResultsItemArrayListResult=pollListResultsItemArrayListResult;
        this.context = context;

        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    List<QuestionslistItem> pollListResultsItemArrayList;
    List<QuestionslistItem> pollListResultsItemArrayListResult;
    Context context;
    LayoutInflater mInflater;
    PollSaveParams pollSaveParams;

    public HashMap<Integer,String> questions = new HashMap<>();;
    @Override
    public int getCount() {
        return pollListResultsItemArrayList.size();
    }

    @Override
    public QuestionslistItem getItem(int position) {

        return pollListResultsItemArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_pollsresult, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }






        viewHolder.textQuestion.setText(pollListResultsItemArrayList.get(position).getQuestion());

        if (pollListResultsItemArrayList.get(position).getOption1() != null && (!TextUtils.equals(pollListResultsItemArrayList.get(position).getOption1() ,"null")) && (!TextUtils.isEmpty(pollListResultsItemArrayList.get(position).getOption1())))
        {
            viewHolder.textOptionone.setText(pollListResultsItemArrayList.get(position).getOption1());
        }
        else
        {
            viewHolder.textOptionone.setVisibility(View.GONE);
            viewHolder.progressBarone.setVisibility(View.GONE);
            viewHolder.textOptionthreeper.setVisibility(View.GONE);
        }
        if (pollListResultsItemArrayList.get(position).getOption2() != null && (!TextUtils.equals(pollListResultsItemArrayList.get(position).getOption2() ,"null")) && (!TextUtils.isEmpty(pollListResultsItemArrayList.get(position).getOption2())))
        {
            viewHolder.textOptiontwo.setText(pollListResultsItemArrayList.get(position).getOption2());
        }
        else
        {
            viewHolder.textOptiontwo.setVisibility(View.GONE);
            viewHolder.progressBartwo.setVisibility(View.GONE);
            viewHolder.textOptiontwoper.setVisibility(View.GONE);
        }
        if (pollListResultsItemArrayList.get(position).getOption3() != null && (!TextUtils.equals(pollListResultsItemArrayList.get(position).getOption3() ,"null")) && (!TextUtils.isEmpty(pollListResultsItemArrayList.get(position).getOption3())))
        {
            viewHolder.textOptionthree.setText(pollListResultsItemArrayList.get(position).getOption3());
        }
        else
        {
            viewHolder.textOptionthree.setVisibility(View.GONE);
            viewHolder.progressBarthree.setVisibility(View.GONE);
            viewHolder.textOptionthreeper.setVisibility(View.GONE);
        }
        if (pollListResultsItemArrayList.get(position).getOption4() != null && (!TextUtils.equals(pollListResultsItemArrayList.get(position).getOption4() ,"null")) && (!TextUtils.isEmpty(pollListResultsItemArrayList.get(position).getOption4())))
        {
            viewHolder.textOptionfour.setText(pollListResultsItemArrayList.get(position).getOption4());
        }
        else
        {
            viewHolder.textOptionfour.setVisibility(View.GONE);
            viewHolder.progressBarfour.setVisibility(View.GONE);
            viewHolder.textOptionfourper.setVisibility(View.GONE);
        }



        if (!pollListResultsItemArrayListResult.get(position).getOptionOnePer().equals("0"))
        {


            int totalnovotes=Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionOnePer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionTwoPer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionThreePer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionFourPer());
            float bmi = (float)Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionOnePer())/(float)(totalnovotes);
            double roundOff = Math.round(bmi * 100.0) / 100.0;
            int pollre= (int) ((float) roundOff*100);
            viewHolder.progressBarone.setProgress(pollre);
            viewHolder.textOptiononeper.setText(" "+pollre+"%");
        }
        if (!pollListResultsItemArrayListResult.get(position).getOptionTwoPer().equals("0"))
        {
            //viewHolder.progressBartwo.setScaleY(2);
            int totalnovotes=Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionOnePer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionTwoPer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionThreePer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionFourPer());
            float bmi=(float)Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionTwoPer())/(float)(totalnovotes);
            double roundOff = Math.round(bmi * 100.0) / 100.0;
            int pollre= (int) ((float) roundOff*100);
            viewHolder.progressBartwo.setProgress(pollre);
            viewHolder.textOptiontwoper.setText(" "+pollre+"%");
        }
        if (!pollListResultsItemArrayListResult.get(position).getOptionThreePer().equals("0"))
        {

            //viewHolder.progressBarthree.setScaleY(2);
            int totalnovotes=Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionOnePer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionTwoPer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionThreePer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionFourPer());
            float bmi=(float)Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionThreePer())/(float)(totalnovotes);
            double roundOff = Math.round(bmi * 100.0) / 100.0;
            int pollre= (int) ((float) roundOff*100);
            viewHolder.progressBarthree.setProgress(pollre);
            viewHolder.textOptionthreeper.setText(" "+pollre+"%");
        }
        if (!pollListResultsItemArrayListResult.get(position).getOptionFourPer().equals("0"))
        {

           // viewHolder.progressBarfour.setScaleY(2);
            int totalnovotes=Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionOnePer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionTwoPer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionThreePer())+Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionFourPer());
            float bmi=(float)Integer.parseInt(pollListResultsItemArrayListResult.get(position).getOptionFourPer())/(float)(totalnovotes);
            double roundOff = Math.round(bmi * 100.0) / 100.0;
            int pollre= (int) ((float) roundOff*100);
            viewHolder.textOptionfourper.setText(" "+pollre+"%");
            viewHolder.progressBarfour.setProgress(pollre);
        }







        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.text_question)
        TextView textQuestion;

        @BindView(R.id.radio_group_answer)
        RadioGroup radioGroupAnswer;
//        @BindView(R.id.cons_item)
//        ConstraintLayout consItem;
        //        @BindView(R.id.card_polls)
//        CardView cardPolls;
        @BindView(R.id.cons_item_polls)
        ConstraintLayout consItemPolls;
        @BindView(R.id.progressBarone)
        ProgressBar progressBarone;
        @BindView(R.id.progressBartwo)
        ProgressBar progressBartwo;
        @BindView(R.id.progressBarthree)
        ProgressBar progressBarthree;
        @BindView(R.id.progressBarfour)
        ProgressBar progressBarfour;
        @BindView(R.id.optionone)
        TextView textOptionone;
        @BindView(R.id.optiontwo)
        TextView textOptiontwo;
        @BindView(R.id.optionthree)
        TextView textOptionthree;
        @BindView(R.id.optionfour)
        TextView textOptionfour;
        @BindView(R.id.optiononeper)
        TextView textOptiononeper;
        @BindView(R.id.optiontwoper)
        TextView textOptiontwoper;
        @BindView(R.id.optionthreeper)
        TextView textOptionthreeper;
        @BindView(R.id.optionfourper)
        TextView textOptionfourper;
        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
