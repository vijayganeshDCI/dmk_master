package com.dci.dmkitwings.model;

import java.util.List;

public class BoothListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<BoothListResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<BoothListResultsItem> results) {
		Results = results;
	}

	private String Status;
	private List<BoothListResultsItem> Results;
	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}