package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.HirerachyFragment;
import com.dci.dmkitwings.model.HirerachyResponse;
import com.dci.dmkitwings.model.HirerachyResultsItem;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Java_AES_Cipher;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.dci.dmkitwings.view.ExpandableHeightGridView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HirerachyAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    // MemberListAdapter memberListAdapter;

    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    Context context;
    List<HirerachyResultsItem> memberLists, subMemberList;
    BaseActivity baseActivity;
    private HirerachyResponse hirerachyResponse;
    HirerachyFragment hirerachyFragment;
    private SharedPreferences.Editor editor;


    public HirerachyAdapter(Context context, List<HirerachyResultsItem> memberLists, BaseActivity baseActivity,
                            HirerachyFragment hirerachyFragment) {
        this.context = context;
        this.memberLists = memberLists;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        subMemberList = new ArrayList<HirerachyResultsItem>();
        this.baseActivity = baseActivity;
        this.hirerachyFragment = hirerachyFragment;
        DmkApplication.getContext().getComponent().inject(this);
    }

    @Override
    public int getCount() {
        return memberLists.size();
    }

    @Override
    public HirerachyResultsItem getItem(int position) {
        return memberLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        editor=sharedPreferences.edit();
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_hirerachy, null);
            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.gridSubMemberList.setExpanded(true);
        if (memberLists.get(position).getFirstName()!=null && !memberLists.get(position).getFirstName().equals("") )
        viewHolder.textName.setText(memberLists.get(position).getFirstName());
        else
            viewHolder.textName.setText(context.getString(R.string.no_member));

        viewHolder.textDesignation.setText(memberLists.get(position).getDesignation());

        universalImageLoader(viewHolder.imageProPic, context.getString(R.string.s3_bucket_profile_path),
                memberLists.get(position).getAvatar(), R.mipmap.icon_pro_image_loading_256,
                R.mipmap.icon_pro_image_loading_256);


        if (memberLists.get(position).isInitialItem())
            viewHolder.viewTop.setVisibility(View.VISIBLE);
        else
            viewHolder.viewTop.setVisibility(View.GONE);


        if (memberLists.get(position).getSubMember().size() > 0) {
            viewHolder.memberListAdapter = new MemberListAdapter(memberLists.get(position).getSubMember(), context);
//            viewHolder.textSubMembersDesignation.setText(memberLists.get(position).getSubMember().get(0).getDesignation());
            viewHolder.gridSubMemberList.setAdapter(viewHolder.memberListAdapter);
            viewHolder.consSubPartyMembers.setVisibility(View.VISIBLE);
            viewHolder.viewBottom.setVisibility(View.VISIBLE);
            viewHolder.gridSubMemberList.setSelection(viewHolder.gridSubMemberList.getAdapter().getCount() - 1);

        } else {
            viewHolder.consSubPartyMembers.setVisibility(View.GONE);

            // Toast.makeText(context,position+""+memberLists.get(position).getSubMember().size(),1000).show();
            // viewHolder.gridSubMemberList.setVisibility(View.GONE);
        }

        final ViewHolder finalViewHolder = viewHolder;

        viewHolder.imageProPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memberLists.get(position).isClickable()) {
                    memberLists.get(position).getSubMember().clear();
                    if (finalViewHolder.consSubPartyMembers.isShown()) {
                        finalViewHolder.consSubPartyMembers.setVisibility(View.GONE);
                    } else {
                        getChildHirerachyList(finalViewHolder, position/*memberLists.get(position).getId()*/);
                    }
                }
            }

        });
        if (sharedPreferences.getInt(DmkConstants.USERID, 0) ==
                memberLists.get(position).getId()) {
            viewHolder.imageProPicBorder.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imageProPicBorder.setVisibility(View.GONE);
        }

        final ViewHolder finalViewHolder1 = viewHolder;
        viewHolder.gridSubMemberList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int subPosition, long id) {

                hirerachyFragment.subMemberListView(
                        memberLists.get(position).getSubMember().get(subPosition).getDesignation(),
                        memberLists.get(position).getSubMember().get(subPosition).getFirstName(),
                        memberLists.get(position).getSubMember().get(subPosition).getLevel(),
                        memberLists.get(position).getSubMember().get(subPosition).getId(),
                        memberLists.get(position).getSubMember().get(subPosition).getLastName(),
                        memberLists.get(position).getSubMember().get(subPosition).getAvatar(),
                        memberLists.get(position).getSubMember().get(subPosition).getUserType(),
                        memberLists.get(position).getId());
            }

        });


        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.view_top)
        View viewTop;
        @BindView(R.id.image_pro_pic)
        CircleImageView imageProPic;
        @BindView(R.id.view_bottom)
        View viewBottom;
        @BindView(R.id.text_name)
        CustomTextView textName;
        @BindView(R.id.text_designation)
        CustomTextView textDesignation;
        @BindView(R.id.view_hide)
        View viewHide;
        @BindView(R.id.text_sub_members_designation)
        CustomTextView textSubMembersDesignation;
        @BindView(R.id.grid_sub_member_list)
        ExpandableHeightGridView gridSubMemberList;
        @BindView(R.id.cons_sub_party_members)
        ConstraintLayout consSubPartyMembers;
        @BindView(R.id.image_pro_pic_border)
        ImageView imageProPicBorder;
        MemberListAdapter memberListAdapter;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void getChildHirerachyList(final ViewHolder viewHolder, final int position) {
        int userID = memberLists.get(position).getId();
        TimeLineDelete timeLineDelete = new TimeLineDelete();
        timeLineDelete.setUserid(userID);
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getParentChildList(headerMap,timeLineDelete).enqueue(new Callback<HirerachyResponse>() {
                @Override
                public void onResponse(Call<HirerachyResponse> call, Response<HirerachyResponse> response) {
                    baseActivity.hideProgress();
                    hirerachyResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (hirerachyResponse.getStatuscode()==0) {
                            if (hirerachyResponse.getResults().size() > 0) {
                                for (int i = 0; i < hirerachyResponse.getResults().size(); i++) {
                                    memberLists.get(position).getSubMember().add(new
                                            HirerachyResultsItem(hirerachyResponse.getResults().get(i).getDesignation(),
                                            hirerachyResponse.getResults().get(i).getFirstName(),
                                            hirerachyResponse.getResults().get(i).getLevel(),
                                            hirerachyResponse.getResults().get(i).getId(),
                                            hirerachyResponse.getResults().get(i).getLastName(),
                                            hirerachyResponse.getResults().get(i).getAvatar(),
                                            hirerachyResponse.getResults().get(i).getUserType(), true, false));
                                }

                            } else {
                                Toast.makeText(context, context.getString(R.string.no_members), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, hirerachyResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, hirerachyResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(hirerachyResponse.getSource(),
                                    hirerachyResponse.getSourcedata()));
                            editor.commit();
                            getChildHirerachyList(viewHolder,position);
                        }
                    } else {
                        Toast.makeText(context, context.getString(R.string.no_members), Toast.LENGTH_SHORT).show();
                    }
                    notifyDataSetChanged();

                }

                @Override
                public void onFailure(Call<HirerachyResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.no_members), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public String getEncodedHeader(String sourceResponse, String sourceDataResponse) {

        byte[] sourceBase64 = Base64.decode(sourceResponse, Base64.NO_WRAP);
        byte[] sourceDataBase64 = Base64.decode(sourceDataResponse, Base64.NO_WRAP);
        String source = new String(sourceBase64, StandardCharsets.UTF_8);
        String sourceData = new String(sourceDataBase64, StandardCharsets.UTF_8);
        String data = source.substring(0, 12) + sourceData.substring(0, 8);
        String key = source.substring(12, 19) + sourceData.substring(8, 13);
        String iv = source.substring(19, 28) + sourceData.substring(13, 20);
        String encrpytData = Java_AES_Cipher.encrypt(key, iv, data);
        byte[] dataBase64Encode = encrpytData.getBytes(StandardCharsets.UTF_8);
        String base64 = Base64.encodeToString(dataBase64Encode, Base64.NO_WRAP);
        return base64;
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }

    public void removeItem(int position) {
        memberLists.remove(position);
    }
}
