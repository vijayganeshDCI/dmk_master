package com.dci.dmkitwings.model;

import java.util.List;

public class TransparentMode {
    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public List<Integer> getChildid() {
        return childid;
    }

    public void setChildid(List<Integer> childid) {
        this.childid = childid;
    }

    private int  userid;
    private int  roleid;
    private List<Integer> childid;
}
