package com.dci.dmkitwings.model;

public class TimelineDetailsBooth
{
    private int id;
    private String boothname;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBoothname() {
        return boothname;
    }

    public void setBoothname(String boothname) {
        this.boothname = boothname;
    }
}
