package com.dci.dmkitwings.model;

public class NewsComposeConstituencyResultsItem {
	private String ConstituencyName;
	private int id;

	public String getConstituencyName() {
		return ConstituencyName;
	}

	public void setConstituencyName(String constituencyName) {
		ConstituencyName = constituencyName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public NewsComposeConstituencyResultsItem(String constituencyName, int id) {
		ConstituencyName = constituencyName;
		this.id = id;
	}
}
