package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.GetChatUserListResultsItem;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by vijayaganesh on 4/12/2018.
 */

public class ContactListAdapter extends BaseAdapter {

    public ContactListAdapter(Context context, List<GetChatUserListResultsItem> contactLists) {
        this.context = context;
        this.contactLists = contactLists;
    }

    Context context;
    List<GetChatUserListResultsItem> contactLists;

    @Override
    public int getCount() {
        return contactLists.size();
    }

    @Override
    public GetChatUserListResultsItem getItem(int position) {
        return contactLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_contact_list, null);
        }

        ImageView imageProPic = (ImageView) convertView.findViewById(R.id.image_profile_pic);
        ImageView imageSelected = (ImageView) convertView.findViewById(R.id.image_selected);
        imageSelected.setImageResource(R.mipmap.icon_selected);
        ImageView imageCall = (ImageView) convertView.findViewById(R.id.image_call);
        TextView textName = (TextView) convertView.findViewById(R.id.text_name);
        TextView textDes = (TextView) convertView.findViewById(R.id.text_designation);

        universalImageLoader(imageProPic, context.getString(R.string.s3_bucket_profile_path),
                contactLists.get(position).getAvatar(), R.mipmap.icon_pro_image_loading_256,
                R.mipmap.icon_pro_image_loading_256);
        textName.setText(contactLists.get(position).getFirstName() != null ?
                contactLists.get(position).getFirstName() : "" + " " +
                contactLists.get(position).getLastName() != null ?
                contactLists.get(position).getLastName() : "");
        textDes.setText(contactLists.get(position).getRoles()!= null ?
                contactLists.get(position).getRoles().getRolename() : "");

        if (contactLists.get(position).getIsSelected() == 0) {
            imageSelected.setVisibility(View.GONE);
        } else {
            imageSelected.setVisibility(View.VISIBLE);
        }


        return convertView;
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }
}
