package com.dci.dmkitwings.model;

public class CommentListResultsItem {
	public int getPostuserid() {
		return postuserid;
	}

	public void setPostuserid(int postuserid) {
		this.postuserid = postuserid;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPostid() {
		return postid;
	}

	public void setPostid(int postid) {
		this.postid = postid;
	}

	public CommentListUser getUser() {
		return user;
	}

	public void setUser(CommentListUser user) {
		this.user = user;
	}

	public int getParentid() {
		return parentid;
	}

	public void setParentid(int parentid) {
		this.parentid = parentid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	private int postuserid;
	private String updated_at;
	private String created_at;
	private String comment;
	private int id;
	private int postid;
	private CommentListUser user;
	private int parentid;
	private int status;

}
