package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.fragment.ArticleFragment;
import com.dci.dmkitwings.model.LibraryResultsItem;
import com.dci.dmkitwings.model.NewsDeleteResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

/**
 * Created by vijayaganesh on 4/9/2018.
 */

public class DigitalLibraryAdpater extends BaseAdapter   {
    public DigitalLibraryAdpater(ArrayList<LibraryResultsItem> libraryListResultsItems, Context context) {
        this.libraryListResultsItems = libraryListResultsItems;
        this.context = context;
        this.baseActivity = baseActivity;
        this.articleFragment = articleFragment;


    }

    ArrayList<LibraryResultsItem> libraryListResultsItems;
    Context context;
    BaseActivity baseActivity;
    ArticleFragment articleFragment;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    NewsDeleteResponse newsDeleteResponse;
    HashMap<String, Integer> alphaIndexer;
    String[] sections;
    private LayoutInflater inflater;
    String[][] items;
    @Override
    public int getCount() {
        return libraryListResultsItems.size();
    }

    @Override
    public LibraryResultsItem getItem(int position) {
        return libraryListResultsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_digital_library, null);
        }

        ImageView imageLibrary = (ImageView) convertView.findViewById(R.id.image_lib_pic);
        TextView textLibTitle = (TextView) convertView.findViewById(R.id.text_lib_title);
        textLibTitle.setText(libraryListResultsItems.get(position).getBookname());
        universalImageLoader(imageLibrary, "Library/",
                libraryListResultsItems.get(position).getBookimage(), R.mipmap.icon_loading_100x100,
                R.mipmap.icon_no_image_100x100);





        return convertView;
    }
    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }



}
