package com.dci.dmkitwings.model;

public class TimeLineDetailUser {
	public int getWing() {
		return Wing;
	}

	public void setWing(int wing) {
		Wing = wing;
	}

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getSuperiorRoleId() {
		return SuperiorRoleId;
	}

	public void setSuperiorRoleId(int superiorRoleId) {
		SuperiorRoleId = superiorRoleId;
	}

	public int getVillagepanchayat() {
		return Villagepanchayat;
	}

	public void setVillagepanchayat(int villagepanchayat) {
		Villagepanchayat = villagepanchayat;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public int getRoleID() {
		return RoleID;
	}

	public void setRoleID(int roleID) {
		RoleID = roleID;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String DOB) {
		this.DOB = DOB;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getConstituencyID() {
		return ConstituencyID;
	}

	public void setConstituencyID(int constituencyID) {
		ConstituencyID = constituencyID;
	}

	public int getPanchayatunion() {
		return Panchayatunion;
	}

	public void setPanchayatunion(int panchayatunion) {
		Panchayatunion = panchayatunion;
	}

	public int getTownshipid() {
		return townshipid;
	}

	public void setTownshipid(int townshipid) {
		this.townshipid = townshipid;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public int getMunicipalityid() {
		return municipalityid;
	}

	public void setMunicipalityid(int municipalityid) {
		this.municipalityid = municipalityid;
	}

	public int getPartid() {
		return partid;
	}

	public void setPartid(int partid) {
		this.partid = partid;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public int getPartydistrict() {
		return partydistrict;
	}

	public void setPartydistrict(int partydistrict) {
		this.partydistrict = partydistrict;
	}

	public int getSuperiorUserID() {
		return SuperiorUserID;
	}

	public void setSuperiorUserID(int superiorUserID) {
		SuperiorUserID = superiorUserID;
	}

	public String getUniquieID() {
		return UniquieID;
	}

	public void setUniquieID(String uniquieID) {
		UniquieID = uniquieID;
	}

	public int getWardID() {
		return WardID;
	}

	public void setWardID(int wardID) {
		WardID = wardID;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public int getVattamid() {
		return vattamid;
	}

	public void setVattamid(int vattamid) {
		this.vattamid = vattamid;
	}

	public int getUserType() {
		return UserType;
	}

	public void setUserType(int userType) {
		UserType = userType;
	}

	private int Wing;
	private int DistrictID;
	private String created_at;
	private int SuperiorRoleId;
	private int Villagepanchayat;
	private String Gender;
	private int RoleID;
	private String updated_at;
	private String DOB;
	private int id;
	private int ConstituencyID;
	private int Panchayatunion;
	private int townshipid;
	private int Age;
	private int Status;
	private int municipalityid;
	private int partid;
	private String FirstName;
	private int partydistrict;
	private int SuperiorUserID;
	private String UniquieID;
	private int WardID;
	private String PhoneNumber;
	private String LastName;
	private int vattamid;
	private int UserType;

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	private String avatar;

	public TimeLineDetailRoles getRoles() {
		return roles;
	}

	public void setRoles(TimeLineDetailRoles roles) {
		this.roles = roles;
	}

	private TimeLineDetailRoles roles;
	private TimelineDetailsDistrict districtdetail;

	public TimelineDetailsDistrict getDistrictdetail() {
		return districtdetail;
	}

	public void setDistrictdetail(TimelineDetailsDistrict districtdetail) {
		this.districtdetail = districtdetail;
	}
	private TimelineDetailsConstituency constituencydetail;

	public TimelineDetailsConstituency getConstituencydetail() {
		return constituencydetail;
	}

	public void setConstituencydetail(TimelineDetailsConstituency constituencydetail) {
		this.constituencydetail = constituencydetail;
	}
	private TimelineDetailsPanchayatunion panchayatuniondetail;

	public TimelineDetailsPanchayatunion getPanchayatuniondetail() {
		return panchayatuniondetail;
	}

	public void setPanchayatuniondetail(TimelineDetailsPanchayatunion panchayatuniondetail) {
		this.panchayatuniondetail = panchayatuniondetail;
	}
	private TimelineDetailsvillagePanchayat villagepanchayatdetail;

	public TimelineDetailsvillagePanchayat getVillagepanchayatdetail() {
		return villagepanchayatdetail;
	}

	public void setVillagepanchayatdetail(TimelineDetailsvillagePanchayat villagepanchayatdetail) {
		this.villagepanchayatdetail = villagepanchayatdetail;
	}
	private TimelineDetailsPart partdetails;

	public TimelineDetailsPart getPartdetails() {
		return partdetails;
	}

	public void setPartdetails(TimelineDetailsPart partdetails) {
		this.partdetails = partdetails;
	}

	private TimelineDetailsWard warddetail;

	public TimelineDetailsWard getWarddetail() {
		return warddetail;
	}

	public void setWarddetail(TimelineDetailsWard warddetail) {
		this.warddetail = warddetail;
	}
	private TimelineDetailsBooth booth;

	public TimelineDetailsBooth getBooth() {
		return booth;
	}

	public void setBooth(TimelineDetailsBooth booth) {
		this.booth = booth;
	}
}
