package com.dci.dmkitwings.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.activity.RegistrationActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.UserDetailsResponse;
import com.dci.dmkitwings.model.UserError;
import com.dci.dmkitwings.model.UserStatusParam;
import com.dci.dmkitwings.model.UserStatusResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by vijayaganesh on 3/26/2018.
 */

public class OtpVerificationFragment extends BaseFragment implements
        ActivityCompat.OnRequestPermissionsResultCallback, VerificationListener {

    @BindView(R.id.view_top)
    View viewTop;
    @BindView(R.id.image_app_icon)
    ImageView imageAppIcon;
    @BindView(R.id.text_label_otp)
    TextView textLabelOtp;
    @BindView(R.id.image_resend)
    ImageView imageResend;
    @BindView(R.id.text_timer)
    TextView textTimer;
    @BindView(R.id.cons_otp_verify)
    ConstraintLayout consOtpVerify;
    Unbinder unbinder;
    private static final String TAG = Verification.class.getSimpleName();
    @BindView(R.id.text_otp_number)
    TextView textOtpNumber;
    @BindView(R.id.otp_pinView)
    PinView otpPinView;
    @BindView(R.id.text_label_otp_waiting_description)
    TextView textLabelOtpWaitingDescription;
    @BindView(R.id.text_resend_otp)
    TextView textResendOtp;
    @BindView(R.id.button_otp_verify)
    Button buttonOtpVerify;
    private Verification mVerification;
    Bundle bundle;
    private String phoneNumber, countryCode;
    private Boolean isBack = false;
    CountDownTimer countDownTimer;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;
    UserError userError;
    UserStatusResponse userStatusResponse;
    UserDetailsResponse userDetailsResponse;
    private SharedPreferences fcmSharedPrefrences;
    boolean isForRegistration;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp_verifcation, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(DmkConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        bundle = getArguments();
        initiateVerification();
        otpPinView.requestFocusFromTouch();
        userError = new UserError();
        startTimer();
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        countDownTimer.cancel();
        unbinder.unbind();
    }

    @OnClick({R.id.image_resend, R.id.text_timer, R.id.button_otp_verify})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_resend:
                ResendCode();
                startTimer();
                break;
            case R.id.text_timer:

                break;
            case R.id.button_otp_verify:
//                Intent intent = new Intent(getActivity(), RegistrationActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
                countDownTimer.cancel();
                textTimer.setVisibility(View.GONE);
                if (!otpPinView.getText().toString().isEmpty()) {
                    hideKeypad();
                    if (mVerification != null) {
                        showProgress();
                        mVerification.verify(otpPinView.getText().toString());
                    }
                } else {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater1 = this.getLayoutInflater();
                    final View dialogView = inflater1.inflate(R.layout.alert_otp_verified, null);
                    alertDialog.setView(dialogView);
                    TextView textView = (TextView) dialogView.findViewById(R.id.text_alert_mes);
                    ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_alert_icon);

                    Button button = (Button) dialogView.findViewById(R.id.button_ok);
                    textView.setText(getString(R.string.Otp_failed));
                    imageView.setImageResource(R.drawable.animated_vector_cross);
//imageView.setImageResource(R.mipmap.icon_failed);
                    final AlertDialog show = alertDialog.show();
                    ((Animatable) imageView.getDrawable()).start();
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            show.dismiss();
                        }
                    });
                }
                break;
        }
    }

    void createVerification(String phoneNumber, boolean skipPermissionCheck, String countryCode) {
//        if (!skipPermissionCheck && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_SMS) ==
//                PackageManager.PERMISSION_DENIED) {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_SMS}, 0);
//            hideProgress();
//        } else {
            mVerification = SendOtpVerification.createSmsVerification
                    (SendOtpVerification
                            .config(countryCode + phoneNumber)
                            .context(getActivity())
                            .autoVerification(true)
                            .otplength("4")
                            .expiry("5")
                            .httpsConnection(false)
                            .senderId("IMDMKOTP")
                            .build(), this);
            mVerification.initiate();
       // }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissions[0])) {
                Toast.makeText(getActivity(), "This application needs permission to read your SMS to automatically verify your "
                        + "phone, you may disable the permission once you have been verified.", Toast.LENGTH_LONG)
                        .show();
            }
        }
        initiateVerificationAndSuppressPermissionCheck();
    }

    void initiateVerification() {
        initiateVerification(false);
    }

    void initiateVerificationAndSuppressPermissionCheck() {
        initiateVerification(true);
    }

    void initiateVerification(boolean skipPermissionCheck) {

        if (bundle != null) {
            phoneNumber = bundle.getString("phoneNumber");
            countryCode = bundle.getString("countryCode");
            isForRegistration = bundle.getBoolean("isForRegistration");
            textOtpNumber.setText("+" + countryCode + "-" + phoneNumber);
            createVerification(phoneNumber, skipPermissionCheck, countryCode);
        }
    }

    public void ResendCode() {
//        startTimer();
//        mVerification.resend("text");
        initiateVerification();
    }


    @Override
    public void onInitiated(String response) {
        Log.d(TAG, "Initialized!" + response);
//        showProgress();
    }

    @Override
    public void onInitiationFailed(Exception exception) {
        Log.e(TAG, "Verification initialization failed: " + exception.getMessage());
        hideProgress();
    }

    @Override
    public void onVerified(String response) {
        Log.d(TAG, "Verified!\n" + response);
        editor.putString(DmkConstants.PHONENUMBER, phoneNumber).commit();
//        getUserStatus();
        if (isForRegistration) {
            Intent intent = new Intent(getActivity(), RegistrationActivity.class);
            if (getActivity().getIntent().getStringExtra("timeLineID") != null)
                intent.putExtra("timeLineID", getActivity().getIntent().getStringExtra("timeLineID"));
            else if (getActivity().getIntent().getStringExtra("newsID") != null)
                intent.putExtra("newsID", getActivity().getIntent().getStringExtra("newsID"));
            else if (getActivity().getIntent().getStringExtra("eventID") != null)
                intent.putExtra("eventID", getActivity().getIntent().getStringExtra("eventID"));
            else if (getActivity().getIntent().getStringExtra("articleID") != null)
                intent.putExtra("articleID", getActivity().getIntent().getStringExtra("articleID"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            getUserDetails();
        }

    }

    @Override
    public void onVerificationFailed(Exception exception) {
        hideProgress();
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater1 = this.getLayoutInflater();
        final View dialogView = inflater1.inflate(R.layout.alert_otp_verified, null);
        alertDialog.setView(dialogView);
        TextView textView = (TextView) dialogView.findViewById(R.id.text_alert_mes);
        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_alert_icon);
        Button button = (Button) dialogView.findViewById(R.id.button_ok);
        textView.setText(getString(R.string.Otp_failed));
        imageView.setImageResource(R.drawable.animated_vector_cross);
        //imageView.setImageResource(R.mipmap.icon_failed);
        final AlertDialog show = alertDialog.show();
        ((Animatable) imageView.getDrawable()).start();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
        textTimer.setVisibility(View.GONE);
        imageResend.setVisibility(View.VISIBLE);
        textResendOtp.setVisibility(View.VISIBLE);
        Log.e(TAG, "Verification failed: " + exception.getMessage());
    }

    private void startTimer() {
        textTimer.setVisibility(View.VISIBLE);
        imageResend.setVisibility(View.GONE);
        textResendOtp.setVisibility(View.GONE);
        isBack = false;
        countDownTimer = new CountDownTimer(60000, 1000) {
            int secondsLeft = 0;

            public void onTick(long ms) {
                if (Math.round((float) ms / 1000.0f) != secondsLeft) {
                    secondsLeft = Math.round((float) ms / 1000.0f);
                    textTimer.setText(getString(R.string.otp_expries) + " " + secondsLeft + " " + getString(R.string.otp_expries_seconds));
                }
            }

            public void onFinish() {
                textTimer.setVisibility(View.GONE);
                imageResend.setVisibility(View.VISIBLE);
                textResendOtp.setVisibility(View.VISIBLE);
                isBack = true;
            }
        }.start();
    }

    private void getUserStatus() {
        if (Util.isNetworkAvailable()) {
//            showProgress();
            final UserStatusParam userStatusParam = new UserStatusParam();
            userStatusParam.setPhoneNumber(phoneNumber);
            userStatusParam.setAppversion(sharedPreferences.getString(DmkConstants.APPVERSION_CODE, ""));
//            1-android 2-ios
            userStatusParam.setApptype(1);
            userStatusParam.setOsversion(String.valueOf(sharedPreferences.getInt(DmkConstants.OS_VERSION, 0)));
            userStatusParam.setImeiid(sharedPreferences.getString(DmkConstants.IMEIID, ""));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getUserStatus(headerMap, userStatusParam).enqueue(new Callback<UserStatusResponse>() {
                @Override
                public void onResponse(Call<UserStatusResponse> call, Response<UserStatusResponse> response) {
                    userStatusResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {

                        switch (userStatusResponse.getLoginstatus()) {
                            case 1:
//                                Old user && IMEIID matched
                                editor.putInt(DmkConstants.USERID, userStatusResponse.getResults().getUserid());
                                editor.putString(DmkConstants.HEADER_SOURCE, userStatusResponse.getResults().getSource());
                                editor.putString(DmkConstants.HEADER_SOURCE_DATA, userStatusResponse.getResults().getSourcedata());
                                editor.putString(DmkConstants.HEADER, getEncodedHeader(userStatusResponse.getResults().getSource(),
                                        userStatusResponse.getResults().getSourcedata()));
                                editor.commit();
                                getUserDetails();
                                break;
                            case 2:
//                                Old user && IMEIID not matched
//                                New user && IMEIID not matched
                                final android.support.v7.app.AlertDialog.Builder alertDialog =
                                        new android.support.v7.app.AlertDialog.Builder(getActivity());
                                alertDialog.setTitle(getString(R.string.app_title_dmk));
                                alertDialog.setMessage(getString(R.string.Unauthorized_login));
                                alertDialog.setPositiveButton(getString(R.string.Alert_Ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                                break;
                            case 3:
//                                New user
//                                openActivity(getE164Number(), true);
                                break;
                            case 4:
//                                App verison not matched
                                final android.support.v7.app.AlertDialog.Builder alertDialog1 =
                                        new android.support.v7.app.AlertDialog.Builder(getActivity());
                                alertDialog1.setTitle(getString(R.string.app_title_dmk));
                                alertDialog1.setMessage(getString(R.string.App_Update_mes));
                                alertDialog1.setCancelable(false);
                                alertDialog1.setPositiveButton(getString(R.string.Alert_Ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog1.show();
                                break;
                            case 5:
                                //Old user && IMEIID is 0 or ""
//                                openActivity(getE164Number(), false);
                                editor.putInt(DmkConstants.USERID, userStatusResponse.getResults().getUserid());
                                editor.putString(DmkConstants.HEADER_SOURCE, userStatusResponse.getResults().getSource());
                                editor.putString(DmkConstants.HEADER_SOURCE_DATA, userStatusResponse.getResults().getSourcedata());
                                editor.putString(DmkConstants.HEADER, getEncodedHeader(userStatusResponse.getResults().getSource(),
                                        userStatusResponse.getResults().getSourcedata()));
                                editor.commit();
                                getUserDetails();
                                break;
                        }
                    } else {
                        hideProgress();
                        Toast.makeText(getActivity(), userStatusResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserStatusResponse> call, Throwable t) {
                    hideProgress();

                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    //
    private void getUserDetails() {
        if (Util.isNetworkAvailable()) {
//            showProgress();
            UserStatusParam userStatusParam = new UserStatusParam();
//            userStatusParam.setPhoneNumber(editOtpLoginPhoneNumber.getText().toString());
            userStatusParam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            userStatusParam.setDeviceToken(fcmSharedPrefrences.getString(DmkConstants.FCMTOKEN, ""));
            userStatusParam.setImeiid(sharedPreferences.getString(DmkConstants.IMEIID, ""));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getUserDetails(headerMap, userStatusParam).enqueue(new Callback<UserDetailsResponse>() {
                @Override
                public void onResponse(Call<UserDetailsResponse> call, Response<UserDetailsResponse> response) {
                    hideProgress();
                    userDetailsResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (userDetailsResponse.getStatuscode() == 0) {
                            if (userDetailsResponse.getResults() != null) {
                                editor.putString(DmkConstants.FIRSTNAME, userDetailsResponse.getResults().getFirstName());
                                editor.putString(DmkConstants.LASTNAME, userDetailsResponse.getResults().getLastName());
                                editor.putString(DmkConstants.MOBILENUMBER, userDetailsResponse.getResults().getPhoneNumber());
                                editor.putInt(DmkConstants.USERID, userDetailsResponse.getResults().getId());
                                editor.putString(DmkConstants.USERDISTRICTNAME, userDetailsResponse.getResults().getDistrictName());
                                editor.putString(DmkConstants.USERWINGNAME, userDetailsResponse.getResults().getWings().getWingName());
                                editor.putString(DmkConstants.DESGINATION, userDetailsResponse.getResults().getRoles().getRolename());
                                editor.putString(DmkConstants.UNIQUEDMKID, userDetailsResponse.getResults().getUniquieID());
                                editor.putInt(DmkConstants.USERAGE, userDetailsResponse.getResults().getAge());
                                editor.putInt(DmkConstants.SUPERIORID, userDetailsResponse.getResults().getSuperiorUserID());
                                editor.putInt(DmkConstants.USER_DISTRICT_ID, userDetailsResponse.getResults().getDistrictID());
                                editor.putInt(DmkConstants.USER_PARTY_DISTRICT_ID, userDetailsResponse.getResults().getPartydistrict());
                                editor.putInt(DmkConstants.USER_CONSTITUENCY_ID, userDetailsResponse.getResults().getConstituencyID());
                                editor.putInt(DmkConstants.USER_DIVISION_ID, userDetailsResponse.getResults().getTypeid());
                                editor.putInt(DmkConstants.USER_PART_ID, userDetailsResponse.getResults().getPartid());
                                //editor.putInt(DmkConstants.USER_VATTAM_ID, userDetailsResponse.getResults().getVattamid());
                                editor.putInt(DmkConstants.USER_WARD_ID, userDetailsResponse.getResults().getWardID());
                                editor.putInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, userDetailsResponse.getResults().getVillagepanchayat());
                                editor.putInt(DmkConstants.USER_PANCHAYAT_UNION_ID, userDetailsResponse.getResults().getPanchayatunion());
                                editor.putInt(DmkConstants.USER_TOWNSHIP_ID, userDetailsResponse.getResults().getTownshipid());
                                editor.putInt(DmkConstants.USER_BOOTH_ID, userDetailsResponse.getResults().getBoothID());
                                editor.putInt(DmkConstants.DESIGNATIONID, userDetailsResponse.getResults().getRoles().getLevel());
                                editor.putInt(DmkConstants.USERTTYPE, userDetailsResponse.getResults().getUserType());
                                editor.putString(DmkConstants.PROFILE_PICTURE, userDetailsResponse.getResults().getAvatar());
                                editor.putString(DmkConstants.USERPERMISSION_COMMON, userDetailsResponse.getResults().getUserPermissionset().getCommon());
                                editor.putString(DmkConstants.USERPERMISSION_ARTICLE, userDetailsResponse.getResults().getUserPermissionset().getArticle());
                                editor.putString(DmkConstants.USERPERMISSION_EVENTS, userDetailsResponse.getResults().getUserPermissionset().getEvents());
                                editor.putString(DmkConstants.USERPERMISSION_NEWS, userDetailsResponse.getResults().getUserPermissionset().getNews());
                                editor.putString(DmkConstants.USERPERMISSION_ELECTION, userDetailsResponse.getResults().getUserPermissionset().getElection());
                                editor.putString(DmkConstants.USER_VOTER_ID, userDetailsResponse.getResults().getVoterID());
                                editor.putString(DmkConstants.USER_ADDRESS, userDetailsResponse.getResults().getAddress());
                                editor.putInt(DmkConstants.USER_UNION_TYPE_ID, userDetailsResponse.getResults().getUniontype());
                                editor.commit();
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                if (getActivity().getIntent().getStringExtra("timeLineID") != null)
                                    intent.putExtra("timeLineID", getActivity().getIntent().getStringExtra("timeLineID"));
                                else if (getActivity().getIntent().getStringExtra("newsID") != null)
                                    intent.putExtra("newsID", getActivity().getIntent().getStringExtra("newsID"));
                                else if (getActivity().getIntent().getStringExtra("eventID") != null)
                                    intent.putExtra("eventID", getActivity().getIntent().getStringExtra("eventID"));
                                else if (getActivity().getIntent().getStringExtra("articleID") != null)
                                    intent.putExtra("articleID", getActivity().getIntent().getStringExtra("articleID"));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getActivity(), userDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, userDetailsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, userDetailsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(userDetailsResponse.getSource(),
                                    userDetailsResponse.getSourcedata()));
                            editor.commit();
                            getUserDetails();
                        }
                    } else {
                        Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<UserDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void hideKeypad() {

        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}
