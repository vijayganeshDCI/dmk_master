package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;

import com.dci.dmkitwings.model.PollListResponse;
import com.dci.dmkitwings.model.PollSaveResponse;

public class PollExpandListAdapter extends BaseExpandableListAdapter {
    Context context;
    PollListResponse pollListResponse;
    public PollExpandListAdapter(Context context, PollListResponse pollListResponse)
    {
        this.pollListResponse=pollListResponse;
        this.context=context;
    }

    @Override
    public int getGroupCount() {
        return this.pollListResponse.getResults().size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.pollListResponse.getResults().get(groupPosition).getQuestionslist().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        return this.pollListResponse.getResults().get(groupPosition).getQuestionslist().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        return null;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        return null;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
