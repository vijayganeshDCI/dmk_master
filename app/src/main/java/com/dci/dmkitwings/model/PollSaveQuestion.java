package com.dci.dmkitwings.model;

public class PollSaveQuestion
{
    private int questionid;
    private String value;

    public int getQuestionid() {
        return questionid;
    }

    public void setQuestionid(int questionid) {
        this.questionid = questionid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public PollSaveQuestion(int questionid, String value) {
        this.questionid = questionid;
        this.value = value;
    }
}
