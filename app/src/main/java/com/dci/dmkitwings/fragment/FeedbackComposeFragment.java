package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.FeedBackActivity;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.adapter.DistrictAdpater;
import com.dci.dmkitwings.adapter.FeedbackCategoryAdpater;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.DistrictsItem;
import com.dci.dmkitwings.model.FeedbackCategoryListResponse;
import com.dci.dmkitwings.model.FeedbackCategoryResultsItem;
import com.dci.dmkitwings.model.FeedbackCategorylistparams;
import com.dci.dmkitwings.model.FeedbackComposeParam;
import com.dci.dmkitwings.model.FeedbackPostResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackComposeFragment extends BaseFragment {

    @BindView(R.id.spinner_title)
    Spinner feedbackcategorySpinner;
    @BindView(R.id.edit_feedback_sub)
    EditText editfeedbackDes;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;
    FeedbackPostResponse feedbackPostResponse;
    Unbinder unbinder;
    FeedBackActivity feedBackActivity;
    FeedbackCategoryListResponse feedbackCategoryListResponse;
    ArrayList<FeedbackCategoryResultsItem> feedbackcategotyList;
    private int feedbackCategoryId = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_feedbackcompose, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        feedBackActivity = (FeedBackActivity) getActivity();
        setHasOptionsMenu(true);
        feedBackActivity.textTitle.setText(R.string.feedback);
        feedbackcategotyList = new ArrayList<FeedbackCategoryResultsItem>();
        getfeedBackCategory();
//        feedBackActivity.mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//        feedBackActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
////        Show Activity Logo
//        feedBackActivity.getSupportActionBar().setDisplayShowHomeEnabled(false);
//        //Navigation Control
//        feedBackActivity.getSupportActionBar().setHomeButtonEnabled(false);
////        ActionBar title
//        feedBackActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        feedbackcategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                FeedbackCategoryResultsItem feedbackCategoryResultsItem = feedbackcategotyList.get(position);
                if (feedbackCategoryResultsItem.getId() != 0) {
                    if (feedbackCategoryId != feedbackCategoryResultsItem.getId()) {
                        feedbackCategoryId = feedbackCategoryResultsItem.getId();
                    }

                } else {

                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    private void feedbackNullCheck() {

        if (feedbackcategorySpinner.getSelectedItemPosition() != 0 && editfeedbackDes.getText().toString().trim().length() > 0
                && editfeedbackDes.getText().toString().trim().length() <= DmkConstants.FEEDBACK_COMPOSE_TITLE_LIMIT) {

            createfeedback();

        } else {
            if (feedbackcategorySpinner.getSelectedItemPosition() == 0) {
                ((TextView) feedbackcategorySpinner.getSelectedView()).setError(getString(R.string.select_feed_category));
            }
            if (editfeedbackDes.getText().toString().trim().length() <= 0) {
                editfeedbackDes.setError(getString(R.string.write_some_thing_missing));
            }
            if (editfeedbackDes.getText().toString().trim().length() > DmkConstants.FEEDBACK_COMPOSE_TITLE_LIMIT) {
                editfeedbackDes.setError(getString(R.string.Feed_title_limit));
            }
        }
    }

    private void getfeedBackCategory() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            feedbackcategotyList.clear();
            feedbackcategotyList.add(0, new FeedbackCategoryResultsItem(getString(R.string.select_feed_category), 0));
            FeedbackCategorylistparams feedbackCategorylistparams = new FeedbackCategorylistparams();
            feedbackCategorylistparams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            feedbackCategorylistparams.setType(1);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getFeedbackcategoryList(headerMap, feedbackCategorylistparams).enqueue(new Callback<FeedbackCategoryListResponse>() {
                @Override
                public void onResponse(Call<FeedbackCategoryListResponse> call, Response<FeedbackCategoryListResponse> response) {
                    feedbackCategoryListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && feedbackCategoryListResponse != null) {
                        //onBackPressed();
                        hideProgress();
                        if (feedbackCategoryListResponse.getStatuscode() == 0) {
                            for (int i = 0; i < feedbackCategoryListResponse.getResults().size(); i++) {
                                feedbackcategotyList.add(new FeedbackCategoryResultsItem(
                                        feedbackCategoryListResponse.getResults().get(i).getCategory() != null ?
                                                feedbackCategoryListResponse.getResults().get(i).getCategory() : "",
                                        feedbackCategoryListResponse.getResults().get(i).getId() != 0 ?
                                                feedbackCategoryListResponse.getResults().get(i).getId() : 0));
                            }
                            FeedbackCategoryAdpater feedbackCategoryAdpater = new FeedbackCategoryAdpater(feedbackcategotyList, getActivity());
                            feedbackcategorySpinner.setAdapter(feedbackCategoryAdpater);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, feedbackCategoryListResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, feedbackCategoryListResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(feedbackCategoryListResponse.getSource(),
                                    feedbackCategoryListResponse.getSourcedata()));
                            editor.commit();
                            getfeedBackCategory();
                        }

                    } else {
                        hideProgress();
                        Toast.makeText(getContext(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<FeedbackCategoryListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void createfeedback() {

        if (Util.isNetworkAvailable()) {
            showProgress();
            FeedbackComposeParam feedbackComposeParam = new FeedbackComposeParam();
            feedbackComposeParam.setFeedback(editfeedbackDes.getText().toString());
            feedbackComposeParam.setCategoryid(feedbackCategoryId);
            feedbackComposeParam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            //feedbackComposeParam.setSuperioruserid(sharedPreferences.getInt(DmkConstants.SUPERIORID,0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.setFeedback(headerMap, feedbackComposeParam).enqueue(new Callback<FeedbackPostResponse>() {
                @Override
                public void onResponse(Call<FeedbackPostResponse> call, Response<FeedbackPostResponse> response) {
                    hideProgress();
                    feedbackPostResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (feedbackPostResponse.getStatuscode() == 0) {
                            //onBackPressed();
                            Toast.makeText(getContext(), getString(R.string.feedback_sucess), Toast.LENGTH_SHORT).show();
                            onBackPressed();
                            getActivity().finish();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, feedbackPostResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, feedbackPostResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(feedbackPostResponse.getSource(),
                                    feedbackPostResponse.getSourcedata()));
                            editor.commit();
                            createfeedback();
                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.feedback_failed), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                        getActivity().finish();
                    }
                }

                @Override
                public void onFailure(Call<FeedbackPostResponse> call, Throwable t) {
                    hideProgress();
                    //onBackPressed();
                    Toast.makeText(getContext(), getString(R.string.feedback_failed), Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    getActivity().finish();
                }
            });

        } else {
            Toast.makeText(getContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_save:
                feedbackNullCheck();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
