package com.dci.dmkitwings.model;

public class VillagePanchayatResultsItem {
	public int getVillagePanchayatID() {
		return VillagePanchayatID;
	}

	public void setVillagePanchayatID(int villagePanchayatID) {
		VillagePanchayatID = villagePanchayatID;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getVillageName() {
		return VillageName;
	}

	public void setVillageName(String villageName) {
		VillageName = villageName;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getConstituencyID() {
		return ConstituencyID;
	}

	public void setConstituencyID(int constituencyID) {
		ConstituencyID = constituencyID;
	}

	public String getVillageDescription() {
		return VillageDescription;
	}

	public void setVillageDescription(String villageDescription) {
		VillageDescription = villageDescription;
	}

	private int VillagePanchayatID;
	private int Status;
	private String VillageName;
	private String updated_at;
	private int DistrictID;

	public VillagePanchayatResultsItem(String villageName, int id) {
		VillageName = villageName;
		this.id = id;
	}

	private String created_at;
	private int id;
	private int ConstituencyID;
	private String VillageDescription;
}
