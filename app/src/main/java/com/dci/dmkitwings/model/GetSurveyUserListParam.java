package com.dci.dmkitwings.model;

public class GetSurveyUserListParam {
    private int userid;
    private int DistrictID;
    private int ConstituencyID;
    private int typeid;
    private int wardid;
    private int partid;
    private int municipalityid;
    private int villageid;
    private int Panchayatunion;
    private int townshipid;
    private int partydistrictid;
    private int BoothID;
    private int uniontype;
    private int limit;
    private int offset;

    public int getPartydistrictid() {
        return partydistrictid;
    }

    public void setPartydistrictid(int partydistrictid) {
        this.partydistrictid = partydistrictid;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getDistrictID() {
        return DistrictID;
    }

    public void setDistrictID(int districtID) {
        DistrictID = districtID;
    }

    public int getConstituencyID() {
        return ConstituencyID;
    }

    public void setConstituencyID(int constituencyID) {
        ConstituencyID = constituencyID;
    }

    public int getTypeid() {
        return typeid;
    }

    public void setTypeid(int typeid) {
        this.typeid = typeid;
    }

    public int getWardid() {
        return wardid;
    }

    public void setWardid(int wardid) {
        this.wardid = wardid;
    }

    public int getPartid() {
        return partid;
    }

    public void setPartid(int partid) {
        this.partid = partid;
    }

    public int getMunicipalityid() {
        return municipalityid;
    }

    public void setMunicipalityid(int municipalityid) {
        this.municipalityid = municipalityid;
    }

    public int getVillageid() {
        return villageid;
    }

    public void setVillageid(int villageid) {
        this.villageid = villageid;
    }

    public int getPanchayatunion() {
        return Panchayatunion;
    }

    public void setPanchayatunion(int panchayatunion) {
        Panchayatunion = panchayatunion;
    }

    public int getTownshipid() {
        return townshipid;
    }

    public void setTownshipid(int townshipid) {
        this.townshipid = townshipid;
    }

    public int getBoothID() {
        return BoothID;
    }

    public void setBoothID(int boothID) {
        BoothID = boothID;
    }

    public int getUniontype() {
        return uniontype;
    }

    public void setUniontype(int uniontype) {
        this.uniontype = uniontype;
    }
}
