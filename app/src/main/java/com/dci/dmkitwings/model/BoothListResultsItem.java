package com.dci.dmkitwings.model;

public class BoothListResultsItem {
    public String getBoothcode() {
        return boothcode;
    }

    public void setBoothcode(String boothcode) {
        this.boothcode = boothcode;
    }

    public int getMunicipalityid() {
        return municipalityid;
    }

    public void setMunicipalityid(int municipalityid) {
        this.municipalityid = municipalityid;
    }

    public int getUnionid() {
        return unionid;
    }

    public void setUnionid(int unionid) {
        this.unionid = unionid;
    }

    public int getPartid() {
        return partid;
    }

    public void setPartid(int partid) {
        this.partid = partid;
    }

    public String getBoothname() {
        return boothname;
    }

    public void setBoothname(String boothname) {
        this.boothname = boothname;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getTownid() {
        return townid;
    }

    public void setTownid(int townid) {
        this.townid = townid;
    }

    public String getWardid() {
        return wardid;
    }

    public void setWardid(String wardid) {
        this.wardid = wardid;
    }

    public int getDistrictid() {
        return districtid;
    }

    public void setDistrictid(int districtid) {
        this.districtid = districtid;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getConstituencyid() {
        return constituencyid;
    }

    public void setConstituencyid(int constituencyid) {
        this.constituencyid = constituencyid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDivisionid() {
        return divisionid;
    }

    public void setDivisionid(int divisionid) {
        this.divisionid = divisionid;
    }

    public String getVattamid() {
        return vattamid;
    }

    public void setVattamid(String vattamid) {
        this.vattamid = vattamid;
    }

    public int getVillageid() {
        return villageid;
    }

    public void setVillageid(int villageid) {
        this.villageid = villageid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    private String boothcode;

    public BoothListResultsItem(String boothname, int id) {
        this.boothname = boothname;
        this.id = id;
    }

    private int municipalityid;
    private int unionid;
    private int partid;
    private String boothname;
    private String created_at;
    private int townid;
    private String wardid;
    private int districtid;
    private String updated_at;
    private int constituencyid;
    private int id;
    private int divisionid;
    private String vattamid;
    private int villageid;
    private int status;
}
