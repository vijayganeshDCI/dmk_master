package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ComplaintActivity;
import com.dci.dmkitwings.activity.CompliantComposeActivity;
import com.dci.dmkitwings.adapter.MyCompliantsAdpater;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.ComplaintListParams;
import com.dci.dmkitwings.model.MyComplaintResultsItem;
import com.dci.dmkitwings.model.MyComplaintsResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 3/16/2018.
 */

public class ReceivedComplaintfragment extends BaseFragment {

    @BindView(R.id.list_compliant)
    ListView listCompliant;
    @BindView(R.id.swipe_mycompliant_line)
    SwipeRefreshLayout swipeCompliant;
    @BindView(R.id.float_compliant_compose)
    FloatingActionButton floatCompliantCompose;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private Boolean isStarted=false;
    private Boolean isVisible=false;
    Unbinder unbinder;
    @BindView(R.id.image_no_comments)
    ImageView imageNoTimeLine;
    @BindView(R.id.text_no_comm)
    CustomTextView textNoTimeLine;
    private int preLast;
    boolean lastEnd, onCreatebool=false;
    int totalcount=0,pagecount=0;
    ComplaintActivity complaintActivity;
    MyComplaintsResponse myComplaintsResponse;
    ArrayList<MyComplaintResultsItem> myComplaintResultsItems;
    MyCompliantsAdpater myCompliantsAdpater;
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible)
            getMyCompliantsList();

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mycompliants, container, false);
        unbinder = ButterKnife.bind(this, view);
        ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        complaintActivity = (ComplaintActivity) getActivity();
        listCompliant.setDivider(null);
        floatCompliantCompose.setVisibility(View.GONE);
        myComplaintResultsItems=new ArrayList<MyComplaintResultsItem>();
        myCompliantsAdpater=new MyCompliantsAdpater(myComplaintResultsItems,getContext());
        listCompliant.setAdapter(myCompliantsAdpater);
        swipeCompliant.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeCompliant.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pagecount=0;
                        onCreatebool=false;
                        lastEnd=false;
                        preLast=0;
                        myComplaintResultsItems.clear();
                        getMyCompliantsList();
                        swipeCompliant.setRefreshing(false);

                    }
                }, 000);
            }
        });
        listCompliant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(getContext(), CompliantComposeActivity.class);
                intent.putExtra("from",2);
                intent.putExtra("id", myComplaintResultsItems.get(position).getId());
                intent.putExtra("status", myComplaintResultsItems.get(position).getStatus());
                intent.putExtra("feedback", myComplaintResultsItems.get(position).getContent());
                intent.putExtra("SubjectID", myComplaintResultsItems.get(position).getSubjectID());
                intent.putExtra("Escalated", myComplaintResultsItems.get(position).getEscalated());
                intent.putExtra("Replied", myComplaintResultsItems.get(position).getReplied());
                intent.putExtra("Categoryid", myComplaintResultsItems.get(position).getCategoryid());
                intent.putExtra("FirstName", myComplaintResultsItems.get(position).getPosteduser().getFirstName());
                intent.putExtra("LastName", myComplaintResultsItems.get(position).getPosteduser().getLastName());
                intent.putExtra("Designation", myComplaintResultsItems.get(position).getPosteduser().getDesignation());
                intent.putExtra("avatar", myComplaintResultsItems.get(position).getPosteduser().getAvatar());
                startActivity(intent);
            }
        });
        listCompliant.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (listCompliant.getLastVisiblePosition() - listCompliant.getHeaderViewsCount() -
                        listCompliant.getFooterViewsCount()) >= (myCompliantsAdpater.getCount() - 1)) {

                    // Now your listview has hit the bottom
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                final int lastItem = firstVisibleItem + visibleItemCount;


                if(lastItem == totalItemCount)
                {
                    if (myComplaintResultsItems.size()>=10) {
                        if (preLast != lastItem) {
                            try {
                                if (!lastEnd && onCreatebool) {
                                    pagecount = pagecount + 11;
                                    //LoadData(pagecount,"");
                                    //showProgress();
                                    getMyCompliantsList();

                                    preLast = lastItem;
                                } else {
                                    //Log.d("Last", "fail");
                                }
                            } catch (Exception e) {
                            }

                        }
                    }
                }


            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    public void getMyCompliantsList()
    {
        if(Util.isNetworkAvailable())
        {
            final ComplaintListParams complaintListParams=new ComplaintListParams();
            complaintListParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID,0));
            complaintListParams.setType(2);
            complaintListParams.setLimit(10);
            complaintListParams.setOffset(pagecount);
            if (pagecount==0)
            {
                myComplaintResultsItems.clear();
            }
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getCompliantList(headerMap,complaintListParams).enqueue(new Callback<MyComplaintsResponse>() {
                @Override
                public void onResponse(Call<MyComplaintsResponse> call, Response<MyComplaintsResponse> response) {
                    hideProgress();
                    myComplaintsResponse=response.body();
                    onCreatebool=true;
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (myComplaintsResponse.getStatuscode()==0) {
                            if (myComplaintsResponse.getResults().size()>0)
                            {
                                if (isAdded()) {
                                    imageNoTimeLine.setVisibility(View.GONE);
                                    textNoTimeLine.setVisibility(View.GONE);
                                    listCompliant.setVisibility(View.VISIBLE);
                                }
                                for (int i=0;i<myComplaintsResponse.getResults().size();i++)
                                {
                                    myComplaintResultsItems.add(myComplaintsResponse.getResults().get(i));
                                }
                                myCompliantsAdpater.notifyDataSetChanged();
                            }
                            else
                            {
                                if (myComplaintResultsItems.size()==0)
                                {

                                    imageNoTimeLine.setVisibility(View.VISIBLE);
                                    imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                                    textNoTimeLine.setVisibility(View.VISIBLE);
                                    textNoTimeLine.setText(R.string.no_complaint);
                                    listCompliant.setVisibility(View.GONE);
                                }
                                else
                                {
                                    lastEnd=true;
                                }

                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, myComplaintsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, myComplaintsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(myComplaintsResponse.getSource(),
                                    myComplaintsResponse.getSourcedata()));
                            editor.commit();
                            getMyCompliantsList();
                        }
                    } else {
                        if (myComplaintResultsItems.size()==0) {
                            imageNoTimeLine.setVisibility(View.VISIBLE);
                        imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                        textNoTimeLine.setVisibility(View.VISIBLE);
                        textNoTimeLine.setText(R.string.no_complaint);
                        listCompliant.setVisibility(View.GONE);
                    }
                    }
                }

                @Override
                public void onFailure(Call<MyComplaintsResponse> call, Throwable t) {
                    hideProgress();
                    if (myComplaintResultsItems.size()==0) {
                        imageNoTimeLine.setVisibility(View.VISIBLE);
                        imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                        textNoTimeLine.setVisibility(View.VISIBLE);
                        textNoTimeLine.setText(R.string.no_complaint);
                        listCompliant.setVisibility(View.GONE);
                    }

                }
            });
        }
        else
        {
            if (isAdded()) {
                imageNoTimeLine.setVisibility(View.VISIBLE);
                textNoTimeLine.setVisibility(View.VISIBLE);
                imageNoTimeLine.setImageResource(R.mipmap.icon_no_network);
                textNoTimeLine.setText(R.string.no_network);
                listCompliant.setVisibility(View.GONE);
            }
        }
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted)
            getMyCompliantsList();

    }
}
