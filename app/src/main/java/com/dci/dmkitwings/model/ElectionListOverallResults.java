package com.dci.dmkitwings.model;

import java.util.List;

public class ElectionListOverallResults {
	private ElectionUserListOverAllUsersResult result;
	private List<UserslistItem> userslist;

	public ElectionUserListOverAllUsersResult getResult() {
		return result;
	}

	public void setResult(ElectionUserListOverAllUsersResult result) {
		this.result = result;
	}

	public List<UserslistItem> getUserslist() {
		return userslist;
	}

	public void setUserslist(List<UserslistItem> userslist) {
		this.userslist = userslist;
	}
}