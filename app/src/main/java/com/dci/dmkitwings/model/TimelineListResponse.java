package com.dci.dmkitwings.model;

import java.util.List;

public class TimelineListResponse {
    private String Status;
    private List<TimeLineListResultsItem> Results;
    private String Mediapath;

    public int getStatuscode() {
        return Statuscode;
    }

    public void setStatuscode(int statuscode) {
        Statuscode = statuscode;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourcedata() {
        return sourcedata;
    }

    public void setSourcedata(String sourcedata) {
        this.sourcedata = sourcedata;
    }

    private int Statuscode;
    private int userid;
    private String source;
    private String sourcedata;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public List<TimeLineListResultsItem> getResults() {
        return Results;
    }

    public void setResults(List<TimeLineListResultsItem> results) {
        Results = results;
    }

    public String getMediapath() {
        return Mediapath;
    }

    public void setMediapath(String mediapath) {
        Mediapath = mediapath;
    }

}