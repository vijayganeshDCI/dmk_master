package com.dci.dmkitwings.model;

import java.util.List;

public class TimeLineListResultsItem {

    private int DistrictID;
    private int Likescount;

    public int getSharecount() {
        return Sharecount;
    }

    public void setSharecount(int sharecount) {
        Sharecount = sharecount;
    }

    private int Sharecount;
    private int PostedUserID;
    private String Title;
    private String MediaFilesPath;
    private int Mediatype;
    private int PostAdminId;
    private int userlikedstatus;
    private int Commentscount;
    private String Content;
    private int id;
    private TimeLineListsUser user;

    public List<String> getMediaFiles() {
        return MediaFiles;
    }

    public void setMediaFiles(List<String> mediaFiles) {
        MediaFiles = mediaFiles;
    }

    private List<String> MediaFiles;

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    private String updated_at;

    public String getVideothumb() {
        return Videothumb;
    }

    public void setVideothumb(String videothumb) {
        Videothumb = videothumb;
    }

    private String Videothumb;


    public int getDistrictID() {
        return DistrictID;
    }

    public void setDistrictID(int districtID) {
        DistrictID = districtID;
    }

    public int getLikescount() {
        return Likescount;
    }

    public void setLikescount(int likescount) {
        Likescount = likescount;
    }

    public int getPostedUserID() {
        return PostedUserID;
    }

    public void setPostedUserID(int postedUserID) {
        PostedUserID = postedUserID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMediaFilesPath() {
        return MediaFilesPath;
    }

    public void setMediaFilesPath(String mediaFilesPath) {
        MediaFilesPath = mediaFilesPath;
    }

    public int getMediatype() {
        return Mediatype;
    }

    public void setMediatype(int mediatype) {
        Mediatype = mediatype;
    }

    public int getPostAdminId() {
        return PostAdminId;
    }

    public void setPostAdminId(int postAdminId) {
        PostAdminId = postAdminId;
    }

    public int getUserlikedstatus() {
        return userlikedstatus;
    }

    public void setUserlikedstatus(int userlikedstatus) {
        this.userlikedstatus = userlikedstatus;
    }

    public int getCommentscount() {
        return Commentscount;
    }

    public void setCommentscount(int commentscount) {
        Commentscount = commentscount;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TimeLineListsUser getUser() {
        return user;
    }

    public void setUser(TimeLineListsUser user) {
        this.user = user;
    }
}
