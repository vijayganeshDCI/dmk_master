package com.dci.dmkitwings.model;

public class CommentList {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public CommentList(String name, String profilePicture, String comment, int commentID ,int timeLineID,
                       String date,int userID,String desigination,
                       int level,String districtName,String constituencyName,String ward,String part,String booth,String village) {
        this.name = name;
        this.profilePicture = profilePicture;
        this.comment = comment;
        this.commentID = commentID;
        this.date=date;
        this.timeLineID = timeLineID;
        this.userID = userID;
        this.desigination=desigination;
        this.level=level;
        this.DistrictName=districtName;
        this.constituencyName=constituencyName;
        this.ward=ward;
        this.part=part;
        this.booth=booth;
        this.village=village;
    }

    private String name;
    private String profilePicture;
    private String comment;
    private int commentID;
    private String desigination;
    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    private int userID;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String date;

    public int getTimeLineID() {
        return timeLineID;
    }

    public void setTimeLineID(int timeLineID) {
        this.timeLineID = timeLineID;
    }

    private int timeLineID;

    public String getDesigination() {
        return desigination;
    }

    public void setDesigination(String desigination) {
        this.desigination = desigination;
    }
    private int level;
    private String DistrictName;
    private String constituencyName;
    private String ward;
    private String part;
    private String booth;
    private String village;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public String getConstituencyName() {
        return constituencyName;
    }

    public void setConstituencyName(String constituencyName) {
        this.constituencyName = constituencyName;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getBooth() {
        return booth;
    }

    public void setBooth(String booth) {
        this.booth = booth;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }
}
