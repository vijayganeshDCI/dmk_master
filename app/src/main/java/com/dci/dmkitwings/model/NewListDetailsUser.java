package com.dci.dmkitwings.model;

public class NewListDetailsUser {
    private int id;
    private String FirstName;
    private String LastName;
    private String Designation;
    private int level;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    private String avatar;

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }
    private TimelineDetailsDistrict district;
    private TimelineDetailsConstituency constituency;
    private TimelineDetailsPanchayatunion panchayatunion;
    private TimelineDetailsvillagePanchayat village;
    private TimelineDetailsPart part;
    private TimelineDetailsWard ward;
    private TimelineDetailsBooth booth;

    public TimelineDetailsDistrict getDistrict() {
        return district;
    }

    public void setDistrict(TimelineDetailsDistrict district) {
        this.district = district;
    }

    public TimelineDetailsConstituency getConstituency() {
        return constituency;
    }

    public void setConstituency(TimelineDetailsConstituency constituency) {
        this.constituency = constituency;
    }

    public TimelineDetailsPanchayatunion getPanchayatunion() {
        return panchayatunion;
    }

    public void setPanchayatunion(TimelineDetailsPanchayatunion panchayatunion) {
        this.panchayatunion = panchayatunion;
    }

    public TimelineDetailsvillagePanchayat getVillage() {
        return village;
    }

    public void setVillage(TimelineDetailsvillagePanchayat village) {
        this.village = village;
    }

    public TimelineDetailsPart getPart() {
        return part;
    }

    public void setPart(TimelineDetailsPart part) {
        this.part = part;
    }

    public TimelineDetailsWard getWard() {
        return ward;
    }

    public void setWard(TimelineDetailsWard ward) {
        this.ward = ward;
    }

    public TimelineDetailsBooth getBooth() {
        return booth;
    }

    public void setBooth(TimelineDetailsBooth booth) {
        this.booth = booth;
    }
}