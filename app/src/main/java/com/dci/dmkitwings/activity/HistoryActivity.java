package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.dmkitwings.BuildConfig;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.StaticPageResponse;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 4/13/2018.
 */

public class HistoryActivity extends BaseActivity {

    @BindView(R.id.webview_history)
    WebView webviewHistory;
    @BindView(R.id.image_no_network)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_network)
    CustomTextView textNoNetwork;
    @BindView(R.id.cons_history)
    ConstraintLayout consHistory;
    private StaticPageResponse staticPageResponse;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_history, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        textTitle.setText(R.string.history);
        frameLayoutNotification.setVisibility(View.GONE);

        WebSettings webSettings = webviewHistory.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowContentAccess(true);
        WebViewClient webViewClient = new WebViewClient();
        webviewHistory.setWebViewClient(webViewClient);
        webviewHistory.getSettings().setJavaScriptEnabled(true);
        loadHistory();
        webviewHistory.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                hideProgress();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                hideProgress();

            }
        });


//        getHistory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base));
    }

    private void getHistory() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            TimeLineDelete timeLineDelete = new TimeLineDelete();
            timeLineDelete.setPageid(2);
            timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getStaticPage(headerMap, timeLineDelete).enqueue(new Callback<StaticPageResponse>() {
                @Override
                public void onResponse(Call<StaticPageResponse> call, Response<StaticPageResponse> response) {
                    hideProgress();
                    staticPageResponse = response.body();
                    if (response.isSuccessful() && response.isSuccessful() && response.code() == 200) {
                        if (staticPageResponse.getStatuscode() == 0) {
                            webviewHistory.loadData(staticPageResponse.getResults().getDescription()
                                    , "text/html; charset=UTF-8;", null);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, staticPageResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, staticPageResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(staticPageResponse.getSource(),
                                    staticPageResponse.getSourcedata()));
                            editor.commit();
                            getHistory();
                        }
                    } else {
                        Toast.makeText(HistoryActivity.this, getString(R.string.please_try_again)
                                , Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<StaticPageResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(HistoryActivity.this, getString(R.string.please_try_again)
                            , Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            imageNoNetwork.setVisibility(View.VISIBLE);
            textNoNetwork.setVisibility(View.VISIBLE);
            imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
            textNoNetwork.setText(R.string.no_network);
            webviewHistory.setVisibility(View.GONE);
        }
    }

    public void loadHistory() {
        if (Util.isNetworkAvailable()) {
            imageNoNetwork.setVisibility(View.GONE);
            textNoNetwork.setVisibility(View.GONE);
            webviewHistory.setVisibility(View.VISIBLE);
            showProgress();
            webviewHistory.loadUrl(BuildConfig.DMK_BASE_URL + "history");
        } else {
            imageNoNetwork.setVisibility(View.VISIBLE);
            textNoNetwork.setVisibility(View.VISIBLE);
            imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
            textNoNetwork.setText(R.string.no_network);
            webviewHistory.setVisibility(View.GONE);
        }
    }
}
