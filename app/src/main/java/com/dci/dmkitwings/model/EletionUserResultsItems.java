package com.dci.dmkitwings.model;

import java.util.List;

public class EletionUserResultsItems {
	private List<ElectionUserlistItem> userlist;
	private int pending;
	private int completed;

	public List<ElectionUserlistItem> getUserlist() {
		return userlist;
	}

	public void setUserlist(List<ElectionUserlistItem> userlist) {
		this.userlist = userlist;
	}

	public int getPending() {
		return pending;
	}

	public void setPending(int pending) {
		this.pending = pending;
	}

	public int getCompleted() {
		return completed;
	}

	public void setCompleted(int completed) {
		this.completed = completed;
	}
}