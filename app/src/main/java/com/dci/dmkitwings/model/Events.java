package com.dci.dmkitwings.model;

/**
 * Created by Vigneshwarans on 4/18/2018.
 */

public class Events {
    private int media;
    private String eventname;

    public Events(int media, String eventname) {
        this.media = media;
        this.eventname = eventname;
    }

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public int getMedia() {
        return media;
    }

    public void setMedia(int media) {
        this.media = media;
    }
}
