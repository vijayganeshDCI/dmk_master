package com.dci.dmkitwings.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.ComplaintViewPagerAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.Mycompliantfragment;
import com.dci.dmkitwings.fragment.ReceivedComplaintfragment;
import com.dci.dmkitwings.utils.LanguageHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by vijayaganesh on 4/13/2018.
 */

public class ComplaintActivity extends BaseActivity {

    Unbinder unbinder;
    private ComplaintViewPagerAdapter complaintViewPagerAdapter;
    @BindView(R.id.viewpager_home)
    ViewPager viewpagerHome;
    @BindView(R.id.tabs_home)
    TabLayout tabsHome;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_complaint, null, false);
        toolbarHome.setBackgroundColor(getResources().getColor(R.color.black));
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        textTitle.setText(R.string.complaint);
        frameLayoutNotification.setVisibility(View.GONE);
        complaintViewPagerAdapter = new ComplaintViewPagerAdapter(ComplaintActivity.this.getSupportFragmentManager());
        complaintViewPagerAdapter.addFrag(new Mycompliantfragment(), getResources().getString(R.string.My_Complaint));
        complaintViewPagerAdapter.addFrag(new ReceivedComplaintfragment(), getResources().getString(R.string.Received_Complaint));
        viewpagerHome.setAdapter(complaintViewPagerAdapter);
        tabsHome.setupWithViewPager(viewpagerHome, false);
        tabsHome.setTabMode(TabLayout.MODE_FIXED);
        tabsHome.setTabGravity(TabLayout.GRAVITY_FILL);
        setupTabIcons();

        tabsHome.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                complaintViewPagerAdapter.getCount();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                complaintViewPagerAdapter.getCount();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                complaintViewPagerAdapter.getCount();
            }
        });

        tabsHome.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                complaintViewPagerAdapter.getCount();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                complaintViewPagerAdapter.getCount();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                complaintViewPagerAdapter.getCount();
            }
        });
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }


    private void setupTabIcons() {
        TextView textHome = (TextView) LayoutInflater.from(ComplaintActivity.this).inflate(R.layout.tab_text, null);
        textHome.setText(getResources().getString(R.string.My_Complaint));
        textHome.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_my_complaints, 0, 0, 0);
        tabsHome.getTabAt(0).setCustomView(textHome);
        TextView textNews = (TextView) LayoutInflater.from(ComplaintActivity.this).inflate(R.layout.tab_text, null);
        textNews.setText(getResources().getString(R.string.Received_Complaint));
        textNews.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_received_comp, 0, 0, 0);
        tabsHome.getTabAt(1).setCustomView(textNews);


    }


}
