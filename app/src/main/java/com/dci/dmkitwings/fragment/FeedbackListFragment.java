package com.dci.dmkitwings.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.FeedBackActivity;
import com.dci.dmkitwings.adapter.FeedAdpater;
import com.dci.dmkitwings.adapter.FeedBackAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.FeedBackInputParam;
import com.dci.dmkitwings.model.FeedbackListResponse;
import com.dci.dmkitwings.model.NewsListParams;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.RecyclerItemClickListener;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackListFragment extends BaseFragment {
    @BindView(R.id.cons_home)
    ConstraintLayout consHome;
    Unbinder unbinder;
    @BindView(R.id.co_home)
    CoordinatorLayout coHome;
    @BindView(R.id.listview_feedback)
    ListView recyclerHome;
    @BindView(R.id.float_feedback_compose)
    FloatingActionButton floatMesCompose;
    View view;
    @BindView(R.id.swipe_feedback)
    SwipeRefreshLayout swipeFeedback;
    @BindView(R.id.image_no_feedback)
    ImageView imageNoTimeLine;
    @BindView(R.id.text_no_feedback)
    CustomTextView textNoTimeLine;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;
    FeedbackListResponse feedbackListResponse;
    ArrayList<FeedBackInputParam> feedBackInputParamsList;
    FeedBackAdapter feedBackAdapter;
    FeedBackActivity feedBackActivity;
    private int preLast;
    boolean lastEnd, onCreatebool = false;
    int totalcount = 0, pagecount = 0;
    FeedAdpater feedAdpater;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        feedBackActivity = (FeedBackActivity) getActivity();
        editor = sharedPreferences.edit();
        feedBackInputParamsList = new ArrayList<FeedBackInputParam>();
        //feedBackAdapter = new FeedBackAdapter(feedBackInputParamsList,  getContext(),feedBackActivity);
        //recyclerHome.setAdapter(feedBackAdapter);
        feedBackActivity.mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //Navigation Control
        feedBackActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        Show Activity Logo
        feedBackActivity.getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        feedBackActivity.getSupportActionBar().setHomeButtonEnabled(true);
//        ActionBar title
        feedBackActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        feedBackActivity.toolbarHome.setNavigationIcon(R.mipmap.icon_nav_menu_toggle_dmk);

        feedAdpater = new FeedAdpater(feedBackInputParamsList, getContext());
        recyclerHome.setAdapter(feedAdpater);
        recyclerHome.setDivider(null);
        swipeFeedback.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeFeedback.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pagecount = 0;
                        onCreatebool = false;
                        lastEnd = false;
                        preLast = 0;
                        feedBackInputParamsList.clear();
                        getFeedbackList();
                        swipeFeedback.setRefreshing(false);

                    }
                }, 000);
            }
        });

        recyclerHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pagecount = 0;
                onCreatebool = false;
                lastEnd = false;
                preLast = 0;
                FeedbackDetailsFragment feedbackDetailsFragment = new FeedbackDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("status", feedBackInputParamsList.get(position).getStatus());
                bundle.putString("feedback", feedBackInputParamsList.get(position).getFeedback());
                bundle.putString("categoryid", feedBackInputParamsList.get(position).getCategoryid());
                bundle.putString("date", feedBackInputParamsList.get(position).getCreated_at());
                feedbackDetailsFragment.setArguments(bundle);
                feedBackActivity.push(feedbackDetailsFragment, "FeedbackDetailsFragment");
            }
        });
        recyclerHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (recyclerHome.getLastVisiblePosition() - recyclerHome.getHeaderViewsCount() -
                        recyclerHome.getFooterViewsCount()) >= (feedAdpater.getCount() - 1)) {

                    // Now your listview has hit the bottom
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                final int lastItem = firstVisibleItem + visibleItemCount;


                if (lastItem == totalItemCount) {
                    if (feedBackInputParamsList.size() >= 10) {
                        if (preLast != lastItem) {
                            try {
                                if (!lastEnd && onCreatebool) {
                                    pagecount = pagecount + 11;

                                    getFeedbackList();

                                    preLast = lastItem;
                                } else {
                                    //Log.d("Last", "fail");
                                }
                            } catch (Exception e) {
                            }

                        }
                    }
                }


            }
        });


        return view;
    }

    @OnClick(R.id.float_feedback_compose)
    public void onViewClicked() {
        if (Util.isNetworkAvailable()) {
            pagecount = 0;
            onCreatebool = false;
            lastEnd = false;
            preLast = 0;
            feedBackActivity.push(new FeedbackComposeFragment(), "FeedbackComposeFragment");
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_network), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getFeedbackList();
    }

    public void getFeedbackList() {

        final NewsListParams newsListParams = new NewsListParams();
        newsListParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        newsListParams.setLimit(10);
        newsListParams.setOffset(pagecount);
        if (Util.isNetworkAvailable()) {

            showProgress();
            if (pagecount == 0) {
                feedBackInputParamsList.clear();
            }
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getFeedbackList(headerMap, newsListParams).enqueue(new Callback<FeedbackListResponse>() {
                @Override
                public void onResponse(Call<FeedbackListResponse> call, Response<FeedbackListResponse> response) {
                    hideProgress();
                    feedbackListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (feedbackListResponse.getStatuscode() == 0) {
                            onCreatebool = true;
                            if (feedbackListResponse.getResults().size() > 0) {
                                if (isAdded()) {
                                    imageNoTimeLine.setVisibility(View.GONE);
                                    textNoTimeLine.setVisibility(View.GONE);
                                    recyclerHome.setVisibility(View.VISIBLE);
                                }
                                for (int i = 0; i < feedbackListResponse.getResults().size(); i++) {
                                    feedBackInputParamsList.add(
                                            new FeedBackInputParam(
                                                    feedbackListResponse.getResults().get(i).getId(),
                                                    feedbackListResponse.getResults().get(i).getUserid(),
                                                    feedbackListResponse.getResults().get(i).getFeedback() != null ?
                                                            feedbackListResponse.getResults().get(i).getFeedback() : "",
                                                    feedbackListResponse.getResults().get(i).getCategoryid(),
                                                    feedbackListResponse.getResults().get(i).getStatus() != null ?
                                                            feedbackListResponse.getResults().get(i).getStatus() : "",
                                                    feedbackListResponse.getResults().get(i).getCreated_at() != null ?
                                                            feedbackListResponse.getResults().get(i).getCreated_at() : ""

                                            ));

                                }

                                feedAdpater.notifyDataSetChanged();


                            } else {
                                if (feedBackInputParamsList.size() == 0) {
                                    imageNoTimeLine.setVisibility(View.VISIBLE);
                                    imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                                    textNoTimeLine.setText(R.string.no_feedback);
                                    textNoTimeLine.setVisibility(View.VISIBLE);
                                    recyclerHome.setVisibility(View.GONE);
                                } else {
                                    lastEnd = true;
                                }
                            }
                        }else {
                            editor.putString(DmkConstants.HEADER_SOURCE, feedbackListResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, feedbackListResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(feedbackListResponse.getSource(),
                                    feedbackListResponse.getSourcedata()));
                            editor.commit();
                            getFeedbackList();
                        }

                    } else {

                        if (feedBackInputParamsList.size() == 0) {
                            imageNoTimeLine.setVisibility(View.VISIBLE);
                            imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                            textNoTimeLine.setText(R.string.no_feedback);
                            textNoTimeLine.setVisibility(View.VISIBLE);
                            recyclerHome.setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<FeedbackListResponse> call, Throwable t) {
                    hideProgress();
                    if (feedBackInputParamsList.size() == 0) {
                        imageNoTimeLine.setVisibility(View.VISIBLE);
                        imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                        textNoTimeLine.setText(R.string.no_feedback);
                        textNoTimeLine.setVisibility(View.VISIBLE);
                        recyclerHome.setVisibility(View.GONE);
                        //Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            if (isAdded()) {
                imageNoTimeLine.setVisibility(View.VISIBLE);
                imageNoTimeLine.setImageResource(R.mipmap.icon_no_network);
                textNoTimeLine.setText(R.string.no_network);
                textNoTimeLine.setVisibility(View.VISIBLE);
                recyclerHome.setVisibility(View.GONE);
            }
        }
    }
}
