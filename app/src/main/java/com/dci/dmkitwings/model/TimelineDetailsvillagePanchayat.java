package com.dci.dmkitwings.model;

public class TimelineDetailsvillagePanchayat
{
    private int id;
    private String VillageName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVillageName() {
        return VillageName;
    }

    public void setVillageName(String villageName) {
        VillageName = villageName;
    }
}
