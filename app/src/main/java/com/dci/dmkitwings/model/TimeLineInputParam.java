package com.dci.dmkitwings.model;

public class TimeLineInputParam {

    public TimeLineInputParam(String title, String content, String firstName,
                              String lastName, String date, String designation, String profilePicture,
                              String media, int likeStatus, int userID,
                              int timeLineID, int likeCount, int mediaType,
                              String mediaUrl,int commetsCount ,String videoThumb,int Level,
                              String district,String constituency,String ward,
                              String part,String booth,String village,int shareCount) {
        this.title = title;
        this.content = content;
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
        this.designation = designation;
        this.profilePicture = profilePicture;
        this.media = media;
        this.likeStatus = likeStatus;
        this.userID = userID;
        this.timeLineID = timeLineID;
        this.likeCount = likeCount;
        this.commetsCount = commetsCount;
        this.mediaType = mediaType;
        this.mediaUrl = mediaUrl;
        this.videoThumb = videoThumb;
        this.Level=Level;
        this.district=district;
        this.constituency=constituency;
        this.ward=ward;
        this.part=part;
        this.booth=booth;
        this.village=village;
        this.shareCount=shareCount;
    }

    private String title;
    private String content;
    private String firstName;
    private String lastName;
    private String date;
    private String designation;
    private String profilePicture;
    private String media;
    private int likeStatus;
    private int userID;
    private int timeLineID;
    private int likeCount;

    public int getShareCount() {
        return shareCount;
    }

    public void setShareCount(int shareCount) {
        this.shareCount = shareCount;
    }

    private int shareCount;
    private int Level;
    private String district;
    private String constituency;
    private String ward;
    private String part;
    private String booth;
    private String village;

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getBooth() {
        return booth;
    }

    public void setBooth(String booth) {
        this.booth = booth;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getConstituency() {
        return constituency;
    }

    public void setConstituency(String constituency) {
        this.constituency = constituency;
    }

    public int getLevel() {
        return Level;
    }

    public void setLevel(int level) {
        Level = level;
    }

    public String getVideoThumb() {
        return videoThumb;
    }

    public void setVideoThumb(String videoThumb) {
        this.videoThumb = videoThumb;
    }

    private String videoThumb;

    public int getCommetsCount() {
        return commetsCount;
    }

    public void setCommetsCount(int commetsCount) {
        this.commetsCount = commetsCount;
    }

    private int commetsCount;
    private int mediaType;

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    private String mediaUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public int getLikeStatus() {
        return likeStatus;
    }

    public void setLikeStatus(int likeStatus) {
        this.likeStatus = likeStatus;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getTimeLineID() {
        return timeLineID;
    }

    public void setTimeLineID(int timeLineID) {
        this.timeLineID = timeLineID;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getMediaType() {
        return mediaType;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }
}
