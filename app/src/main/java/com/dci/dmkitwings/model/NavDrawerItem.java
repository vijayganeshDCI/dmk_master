package com.dci.dmkitwings.model;

/**
 * Created by vijayaganesh on 11/7/2017.
 */

public class NavDrawerItem {


    private String title;
    private int icon;

    public int getChatReadStatus() {
        return chatReadStatus;
    }

    public void setChatReadStatus(int chatReadStatus) {
        this.chatReadStatus = chatReadStatus;
    }

    private int chatReadStatus;


    public NavDrawerItem() {
    }

    public NavDrawerItem(String title, int icon,int chatReadStatus) {
        this.title = title;
        this.icon = icon;
        this.chatReadStatus=chatReadStatus;
    }


    public String getTitle() {
        return this.title;
    }

    public int getIcon() {
        return this.icon;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }


}
