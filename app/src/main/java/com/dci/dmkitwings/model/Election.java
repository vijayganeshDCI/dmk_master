package com.dci.dmkitwings.model;

/**
 * Created by Vigneshwarans on 4/18/2018.
 */

public class Election {
    private String electionid;
    private String electionidname;
    private String electionidlink;

    public String getElectionid() {
        return electionid;
    }

    public void setElectionid(String electionid) {
        this.electionid = electionid;
    }

    public String getElectionidlink() {
        return electionidlink;
    }

    public void setElectionidlink(String electionidlink) {
        this.electionidlink = electionidlink;
    }

    public Election(String electionid, String electionidlink,String electionidname) {
        this.electionid = electionid;
        this.electionidlink = electionidlink;
        this.electionidname=electionidname;
    }

    public String getElectionidname() {
        return electionidname;
    }

    public void setElectionidname(String electionidname) {
        this.electionidname = electionidname;
    }
}
