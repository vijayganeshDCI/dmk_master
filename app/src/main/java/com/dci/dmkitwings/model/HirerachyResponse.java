package com.dci.dmkitwings.model;

import java.util.List;

public class HirerachyResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<HirerachyResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<HirerachyResultsItem> results) {
		Results = results;
	}

	public String getMediapath() {
		return Mediapath;
	}

	public void setMediapath(String mediapath) {
		Mediapath = mediapath;
	}

	private String Status;
	private List<HirerachyResultsItem> Results;
	private String Mediapath;

	public int getStatuscode() {
		return Statuscode;
	}

	public void setStatuscode(int statuscode) {
		Statuscode = statuscode;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourcedata() {
		return sourcedata;
	}

	public void setSourcedata(String sourcedata) {
		this.sourcedata = sourcedata;
	}

	private int Statuscode;
	private int userid;
	private String source;
	private String sourcedata;
}