package com.dci.dmkitwings.model;

public class FeedbackCategoryResultsItem {
	private int id;
	private String category;

	public FeedbackCategoryResultsItem(String category, int id) {
		this.category=category;
		this.id=id;
	}

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
