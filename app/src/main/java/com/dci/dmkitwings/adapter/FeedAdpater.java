package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.NewsComposeActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.SubNewsFragment;
import com.dci.dmkitwings.model.FeedBackInputParam;
import com.dci.dmkitwings.model.NewsDeletePostParams;
import com.dci.dmkitwings.model.NewsDeleteResponse;
import com.dci.dmkitwings.model.NewsListResultsItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 4/9/2018.
 */

public class FeedAdpater extends BaseAdapter {
    public FeedAdpater(List<FeedBackInputParam> feedbackList, Context context) {
        this.feedbackList = feedbackList;
        this.context = context;

    }


    List<FeedBackInputParam> feedbackList;
    Context context;

    @Override
    public int getCount() {
        return feedbackList.size();
    }

    @Override
    public FeedBackInputParam getItem(int position) {
        return feedbackList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_feedback, null);
        }

        TextView textFeedbackTitle = (TextView) convertView.findViewById(R.id.text_feedtitle);
        TextView textFeedbackDescription = (TextView) convertView.findViewById(R.id.text_feedbackdescription);
        TextView textFeedbackDate = (TextView) convertView.findViewById(R.id.text_feedbacktime);
        TextView textFeedstatus = (TextView) convertView.findViewById(R.id.text_feedstatus);
        TextView textstatus = (TextView) convertView.findViewById(R.id.text_status);
//        textstatus.setText(context.getString(R.string.Status));
//        textstatus.setTextColor(Color.GRAY);
        if (feedbackList.size() > 0) {
            textFeedbackTitle.setText(feedbackList.get(position).getCategoryid());
//        textFeedbackDescription.setText(feedbackList.get(position).getFeedback());
//        textFeedbackDescription.setVisibility(View.GONE);
//        textFeedbackDate.setText(feedbackList.get(position).getCreated_at());
            if (feedbackList.get(position).getStatus().equals("Reviewed")) {
                textFeedstatus.setText(" " + context.getString(R.string.Reviewed));
                textFeedstatus.setTextColor(context.getResources().getColor(R.color.accept_green));
                textFeedstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_resolved_32_green, 0);
            } else {
                textFeedstatus.setText(" " + context.getString(R.string.Not_Reviewed));
                textFeedstatus.setTextColor(context.getResources().getColor(R.color.red));
                textFeedstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_not_resolved_32_red, 0);
            }
        }

        return convertView;
    }


}
