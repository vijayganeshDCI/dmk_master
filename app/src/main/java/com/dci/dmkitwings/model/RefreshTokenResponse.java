package com.dci.dmkitwings.model;

public class RefreshTokenResponse{
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public RefreshTokenResults getResults() {
		return results;
	}

	public void setResults(RefreshTokenResults results) {
		this.results = results;
	}

	private String status;
	private RefreshTokenResults results;
}
