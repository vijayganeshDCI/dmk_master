package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.ConstituencyListResultsItem;
import com.dci.dmkitwings.model.NewsComposeConstituencyResultsItem;
import com.dci.dmkitwings.view.CustomCheckBox;
import com.dci.dmkitwings.view.CustomRadioButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsComposeConstituencyAdapter extends BaseAdapter {


    private final LayoutInflater mInflater;
    List<NewsComposeConstituencyResultsItem> constituencyListResultsItemList;
    Context context;
    public ArrayList<String> arrayListconstiunecyid=new ArrayList<String>();
    public ArrayList<String> arrayListconstiunecyname=new ArrayList<String>();
    public ArrayList<String> arraylistConstituencyIdTest=new ArrayList<String>();
    public NewsComposeConstituencyAdapter(List<NewsComposeConstituencyResultsItem> constituencyListResultsItemList, Context context, ArrayList<String> arraylistConstituencyIdTest) {
        this.constituencyListResultsItemList = constituencyListResultsItemList;
        this.context = context;
        this.arraylistConstituencyIdTest=arraylistConstituencyIdTest;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return constituencyListResultsItemList.size();
    }

    @Override
    public NewsComposeConstituencyResultsItem getItem(int position) {
        return constituencyListResultsItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_spinner_item_checkbox, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //viewHolder.textSpinnerItemOne.setText(constituencyListResultsItemList.get(position).getConstituencyName());

        //viewHolder.text_Checkbox.setText(constituencyListResultsItemList.get(position).getConstituencyName());
        viewHolder.checkboxSpinnerItemOne.setText(constituencyListResultsItemList.get(position).getConstituencyName());
        viewHolder.checkboxSpinnerItemOne.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    if (Arrays.asList(arrayListconstiunecyid).contains(constituencyListResultsItemList.get(position).getId())) {
                        // true
                        arrayListconstiunecyid.remove(String.valueOf(constituencyListResultsItemList.get(position).getId()));
                        arrayListconstiunecyid.add(String.valueOf(constituencyListResultsItemList.get(position).getId()));
                        Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(arrayListconstiunecyid);
                        arrayListconstiunecyid.clear();
                        arrayListconstiunecyid.addAll(primesWithoutDuplicates);
                        arrayListconstiunecyname.remove(constituencyListResultsItemList.get(position).getConstituencyName());
                        arrayListconstiunecyname.add(constituencyListResultsItemList.get(position).getConstituencyName());
                        Set<String> primesWithoutConstituency = new LinkedHashSet<String>(arrayListconstiunecyname);
                        arrayListconstiunecyname.clear();
                        arrayListconstiunecyname.addAll(primesWithoutConstituency);

                    }
                    else
                    {
                        arrayListconstiunecyid.add(String.valueOf(constituencyListResultsItemList.get(position).getId()));
                        Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(arrayListconstiunecyid);
                        arrayListconstiunecyid.clear();
                        arrayListconstiunecyid.addAll(primesWithoutDuplicates);
                        arrayListconstiunecyname.add(constituencyListResultsItemList.get(position).getConstituencyName());
                        Set<String> primesWithoutConstituency = new LinkedHashSet<String>(arrayListconstiunecyname);
                        arrayListconstiunecyname.clear();
                        arrayListconstiunecyname.addAll(primesWithoutConstituency);
                    }
                }
                else
                {
                    arrayListconstiunecyid.remove(String.valueOf(constituencyListResultsItemList.get(position).getId()));
                    Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(arrayListconstiunecyid);
                    arrayListconstiunecyid.clear();
                    arrayListconstiunecyid.addAll(primesWithoutDuplicates);
                    arrayListconstiunecyname.remove(constituencyListResultsItemList.get(position).getConstituencyName());
                    Set<String> primesWithoutConstituency = new LinkedHashSet<String>(arrayListconstiunecyname);
                    arrayListconstiunecyname.clear();
                    arrayListconstiunecyname.addAll(primesWithoutConstituency);

                }
            }
        });
        if (arraylistConstituencyIdTest.size()>0) {
            for (int y = 0; y < arraylistConstituencyIdTest.size(); y++) {
                int ConID = Integer.parseInt(arraylistConstituencyIdTest.get(y));
                //Log.d("Water", "districtid" + ConID);
                //viewHolder.checkboxSpinnerItemOne.setChecked(districtsItemList.get(position).getId()==DisID);
                if (constituencyListResultsItemList.get(position).getId() == ConID) {
                    viewHolder.checkboxSpinnerItemOne.setChecked(constituencyListResultsItemList.get(position).getId() == ConID);
                    //Log.d("Water", "else");
                } else {
                    //Log.d("Water", "else");
                }


            }

        }
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.checkbox_spinner_item_one)
        CheckBox checkboxSpinnerItemOne;
        @BindView(R.id.text_checkbox)
        TextView text_Checkbox;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
