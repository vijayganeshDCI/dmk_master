package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.DivisionListResultsItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DivisionAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;

    public DivisionAdapter(List<DivisionListResultsItem> divisionListResultsItemList, Context context) {
        this.divisionListResultsItemList = divisionListResultsItemList;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    List<DivisionListResultsItem> divisionListResultsItemList;
    Context context;

    @Override
    public int getCount() {
        return divisionListResultsItemList.size();
    }

    @Override
    public DivisionListResultsItem getItem(int position) {
        return divisionListResultsItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_spinner_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textSpinnerItemOne.setText(divisionListResultsItemList.get(position).getDivisionname());
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.text_spinner_item_one)
        TextView textSpinnerItemOne;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
