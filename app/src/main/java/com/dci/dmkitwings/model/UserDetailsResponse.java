package com.dci.dmkitwings.model;

public class UserDetailsResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public UserDetailResults getResults() {
		return Results;
	}

	public void setResults(UserDetailResults results) {
		Results = results;
	}

	private String Status;
	private String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    private UserDetailResults Results;

	public int getStatuscode() {
		return Statuscode;
	}

	public void setStatuscode(int statuscode) {
		Statuscode = statuscode;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourcedata() {
		return sourcedata;
	}

	public void setSourcedata(String sourcedata) {
		this.sourcedata = sourcedata;
	}

	private int Statuscode;
	private int userid;
	private String source;
	private String sourcedata;
}
