package com.dci.dmkitwings.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.bumptech.glide.Glide;
import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.NewComposeDistrictAdpater;
import com.dci.dmkitwings.adapter.NewsComposeConstituencyAdapter;
import com.dci.dmkitwings.adapter.SelectedImageAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.ArticleComposeParams;
import com.dci.dmkitwings.model.ArticleDetailParams;
import com.dci.dmkitwings.model.ArticleDetailResponse;
import com.dci.dmkitwings.model.ArticleUpdatePostParams;
import com.dci.dmkitwings.model.DistrictListResponse;
import com.dci.dmkitwings.model.DistrictsItem;
import com.dci.dmkitwings.model.NewComposeConstituenctPost;
import com.dci.dmkitwings.model.NewsComposeConstituencyListResponse;
import com.dci.dmkitwings.model.NewsComposeConstituencyResultsItem;
import com.dci.dmkitwings.model.NewsPartyDistrictResponse;
import com.dci.dmkitwings.model.NewsPostResponse;
import com.dci.dmkitwings.model.NewsUpdateResponse;
import com.dci.dmkitwings.model.SelectedImage;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomEditText;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.jzvd.JZVideoPlayerStandard;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.dmkitwings.activity.TimeLineComposeActivity.isVideoFile;
import static com.dci.dmkitwings.app.DmkApplication.getProxy;

public class ArticleComposeActivity extends BaseActivity {
    @BindView(R.id.edit_title)
    EditText editTitle;
    @BindView(R.id.view_time_compose)
    View viewTimeCompose;
    @BindView(R.id.edit_time_sub)
    EditText editTimeSub;
    @BindView(R.id.image_selected)
    ImageView imageSelected;
    @BindView(R.id.video_selected)
    JZVideoPlayerStandard videoSelected;
    @BindView(R.id.image_photo)
    ImageView imagePhoto;
    @BindView(R.id.image_video)
    ImageView imageVideo;
    @BindView(R.id.image_audio)
    ImageView imageAudio;
    @BindView(R.id.image_attach_file)
    ImageView imageAttachFile;
    @BindView(R.id.image_send)
    ImageView imageSend;
    @BindView(R.id.image_send1)
    ImageView imageSend1;
    @BindView(R.id.spinner_newstype)
    Spinner spinnerNewstype;
    @BindView(R.id.spinner_selected_district)
    Spinner spinnerDistrict;
    @BindView(R.id.spinner_selected_constituency)
    Spinner spinnerConstituency;
    @BindView(R.id.edit_selected_district)
    CustomEditText editSelectedDistrict;
    @BindView(R.id.edit_selected_constituency)
    CustomEditText editSelectedConstituency;
    Unbinder unbinder;
    @BindView(R.id.grid_image)
    GridView gridImage;
    @BindView(R.id.image_delete)
    ImageView imageDelete;
    @BindView(R.id.float_attach_image)
    FloatingActionButton floatAttachImage;
    @BindView(R.id.float_attach_video)
    FloatingActionButton floatAttachVideo;
    @BindView(R.id.float_attach_audio)
    FloatingActionButton floatAttachAudio;
    @BindView(R.id.float_mes_compose)
    FloatingActionButton floatMesCompose;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;
    ArrayList<DistrictsItem> districtList;
    DistrictListResponse districtListResponse;
    NewsPartyDistrictResponse newsPartyDistrictResponse;
    @BindView(R.id.view_spinner_news_type)
    View viewSpinnerNewsType;
    @BindView(R.id.view_spinner_selected_district)
    View viewSpinnerSelectedDistrict;
    @BindView(R.id.view_spinner_selected_constitunecy)
    View viewSpinnerSelectedConstitunecy;
    @BindView(R.id.cons_send)
    ConstraintLayout consSend;
    @BindView(R.id.cons_bottom_view)
    ConstraintLayout consBottomView;
    @BindView(R.id.cons_news_compose)
    ConstraintLayout consNewsCompose;
    @BindView(R.id.cord_news_compose)
    CoordinatorLayout cordNewsCompose;
    private int districtID = -1;
    ArrayList<NewsComposeConstituencyResultsItem> constituencyList;
    NewsComposeConstituencyListResponse newsComposeConstituencyListResponse;
    int selectedTypePos = 0;
    String districtid;
    String constituencyid;
    Dialog districtDialog;
    TextView textDialogDistrict;
    Button dialogDistrictButton;
    ListView listviewDistrict;
    Dialog constituencyDialog;
    TextView textDialogconstituency;
    Button dialogconstituencyButton;
    ListView listviewconstituency;
    NewComposeDistrictAdpater adapterdivisionList;
    NewsComposeConstituencyAdapter adapterConstituencyList;
    boolean isFromNewsLine;
    int articleID, newsDistrictID, newsConstiuencyID;
    private String filemanagerPath;
    private String recordedVideoPath;
    private int REQUEST_TAKE_GALLERY_VIDEO_AUDIO = 2;
    private static final int MY_REQUEST_CODE_CAMERA = 3;
    private static final int SELECT_IMAGE_FROM_CAMERA = 4;
    private static final int MY_REQUEST_CODE_GALLERY = 5;
    private int SELECT_IMAGE_FROM_GALLERY = 6;
    private static final int SELECT_VIDEO_FROM_CAMERA = 7;
    private int SELECT_VIDEO_FROM_GALLERY = 8;
    private List<SelectedImage> selectedImageList;
    private MediaRecorder mRecorder;
    private long mStartTime = 0;

    private int[] amplitudes = new int[100];

    private int i = 0;
    private File mOutputFile;
    private TextView textTimer;
    private Runnable mTickExecutor = new Runnable() {
        @Override
        public void run() {
            audioTimer();
            mHandler.postDelayed(mTickExecutor, 100);
        }
    };
    private SelectedImageAdapter selectedImageAdapter;
    String selectedDistrictName;
    String selectedConstituencyName;
    public ArrayList<String> arraylistDistrictIdforedit = new ArrayList<String>();
    public ArrayList<String> arraylistConstituencyforedit = new ArrayList<String>();
    String distriIDforedit;
    String conIDTestforedit;
    NewsPostResponse newsPostResponse;
    NewsUpdateResponse newsUpdateResponse;
    int editNewsType;
    Intent intent;
    private boolean isFABOpen;
    private MenuItem sumbit;
    private ArticleDetailResponse articleDetailResponse;
    private List<String> selectedMediaListName;
    private int mediaType = 2;
    private Uri selectedVideoAudioUri;
    private String galleryVideoPath;
    private String galleryAudioPath;
    private String audioRecordedPathName;
    private boolean isAudioFromRecord;
    private boolean videoFromResponse = false;
    private String videoAudioFileNameInternal;
    private HttpProxyCacheServer proxy;
    private File destination;
    String videoThubmnail;

    boolean DistrictBoolean = true;
    boolean ConstituencyBoolean = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_article_compose, null, false);
        unbinder = ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        mDrawerLayout.addView(contentView, 0);
        proxy = getProxy(ArticleComposeActivity.this);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        frameLayoutNotification.setVisibility(View.GONE);
        //Navigation Control
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        textTitle.setText(R.string.article_compose);
        districtDialog = new Dialog(ArticleComposeActivity.this);
        districtDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        districtDialog.setCancelable(true);
        districtDialog.setContentView(R.layout.custom_dialog);
        listviewDistrict = (ListView) districtDialog.findViewById(R.id.listView1);
        dialogDistrictButton = (Button) districtDialog.findViewById(R.id.btn_dialog);
        textDialogDistrict = (TextView) districtDialog.findViewById(R.id.text_dialog);
        constituencyDialog = new Dialog(ArticleComposeActivity.this);
        constituencyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        constituencyDialog.setCancelable(true);
        constituencyDialog.setContentView(R.layout.custom_dialog);
        listviewconstituency = (ListView) constituencyDialog.findViewById(R.id.listView1);
        dialogconstituencyButton = (Button) constituencyDialog.findViewById(R.id.btn_dialog);
        textDialogconstituency = (TextView) constituencyDialog.findViewById(R.id.text_dialog);
        frameLayoutNotification.setVisibility(View.GONE);
        spinnerConstituency.setVisibility(View.GONE);
        spinnerDistrict.setVisibility(View.GONE);
        editSelectedDistrict.setVisibility(View.GONE);
        editSelectedConstituency.setVisibility(View.GONE);
        districtList = new ArrayList<DistrictsItem>();
        constituencyList = new ArrayList<NewsComposeConstituencyResultsItem>();
        ArrayAdapter<String> adapterType = new ArrayAdapter<String>(this, R.layout.custom_spinner_item, getResources().getStringArray(R.array.newstype));
        spinnerNewstype.setAdapter(adapterType);
        selectedImageList = new ArrayList<SelectedImage>();
        intent = getIntent();
        if (getIntent() != null) {
            isFromNewsLine = intent.getBooleanExtra("fromNews", false);
            if (isFromNewsLine) {
                articleID = intent.getIntExtra("articleID", 0);
                newsDistrictID = intent.getIntExtra("newsDistrict", 0);
                newsConstiuencyID = intent.getIntExtra("newsConstituency", 0);
                editNewsType = intent.getIntExtra("Newstype", 0);
                getDetailArticle();
            }
        }
        selectedMediaListName = new ArrayList<String>();

        spinnerNewstype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedTypePos = adapterView.getSelectedItemPosition();
                if (selectedTypePos != 0) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    switch (selectedTypePos) {
                        case 1:
                            //all
                            editSelectedDistrict.setVisibility(View.GONE);
//                            editSelectedConstituency.setVisibility(View.GONE);
                            viewSpinnerSelectedDistrict.setVisibility(View.GONE);
//                            viewSpinnerSelectedConstitunecy.setVisibility(View.GONE);

                            break;
                        case 2:
                            //District
                            editSelectedDistrict.setVisibility(View.VISIBLE);
//                            editSelectedConstituency.setVisibility(View.GONE);
                            editSelectedDistrict.setText(getString(R.string.select_district));
                            viewSpinnerSelectedDistrict.setVisibility(View.VISIBLE);
//                            viewSpinnerSelectedConstitunecy.setVisibility(View.GONE);
                            if (isFromNewsLine) {
                                if (DistrictBoolean) {
                                    getDistrictList();
                                }
                            } else {
                                getDistrictList();
                            }

                            break;
                        case 3:
                            //constituency
                            editSelectedDistrict.setVisibility(View.VISIBLE);
//                            editSelectedConstituency.setVisibility(View.GONE);
                            editSelectedDistrict.setText(getString(R.string.select_constituency));
                            viewSpinnerSelectedDistrict.setVisibility(View.VISIBLE);
//                            viewSpinnerSelectedConstitunecy.setVisibility(View.GONE);

                            if (isFromNewsLine) {
                                if (ConstituencyBoolean) {
                                    getDistrictList();
                                }
                            } else {
                                getConsituencyList();
                            }
                            break;

                    }

                } else {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        editSelectedDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedTypePos == 2) {
                    dialogDistrictButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            districtid = "";
                            StringBuilder builder = new StringBuilder();
                            for (String s : adapterdivisionList.arraylistDistrictId) {
                                builder.append(s);
                                builder.append(",");

                            }
                            districtid = builder.toString();
                            if (!districtid.toString().isEmpty()) {
                                districtid = districtid.substring(0, districtid.length() - 1);


                            }
                            selectedDistrictName = "";
                            StringBuilder builder1 = new StringBuilder();
                            for (String s : adapterdivisionList.arraylistDistrictName) {
                                builder1.append(s);
                                builder1.append(",");

                            }
                            selectedDistrictName = builder1.toString();
                            if (!selectedDistrictName.toString().isEmpty()) {
                                selectedDistrictName = selectedDistrictName.substring(0, selectedDistrictName.length() - 1);
                                editSelectedDistrict.setText(selectedDistrictName);
                            } else {
                                editSelectedDistrict.setText(getString(R.string.select_district));
                            }

                            districtDialog.dismiss();
                        }
                    });
                    textDialogDistrict.setText(getString(R.string.district));
                    districtDialog.show();
                } else if (selectedTypePos == 3) {
                    dialogconstituencyButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            constituencyid = "";
                            StringBuilder builder = new StringBuilder();
                            for (String s : adapterConstituencyList.arrayListconstiunecyid) {
                                builder.append(s);
                                builder.append(",");

                            }
                            constituencyid = builder.toString();
                            if (!constituencyid.toString().isEmpty()) {
                                constituencyid = constituencyid.substring(0, constituencyid.length() - 1);
                                //editSelectedConstituency.setText(constituencyid);

                            }
                            selectedConstituencyName = "";
                            StringBuilder builder1 = new StringBuilder();
                            for (String s : adapterConstituencyList.arrayListconstiunecyname) {
                                builder1.append(s);
                                builder1.append(",");

                            }
                            selectedConstituencyName = builder1.toString();
                            if (!selectedConstituencyName.toString().isEmpty()) {
                                selectedConstituencyName = selectedConstituencyName.substring(0, selectedConstituencyName.length() - 1);
                                editSelectedDistrict.setText(selectedConstituencyName);
                            } else {
                                editSelectedDistrict.setText(getString(R.string.select_constituency));
                            }
                            constituencyDialog.dismiss();
                        }
                    });
                    textDialogconstituency.setText("Constituency");
                    constituencyDialog.show();
                }
            }
        });
//        editSelectedConstituency.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                dialogconstituencyButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        constituencyid = "";
//                        StringBuilder builder = new StringBuilder();
//                        for (String s : adapterConstituencyList.arrayListconstiunecyid) {
//                            builder.append(s);
//                            builder.append(",");
//
//                        }
//                        constituencyid = builder.toString();
//                        if (!constituencyid.toString().isEmpty()) {
//                            constituencyid = constituencyid.substring(0, constituencyid.length() - 1);
//                            //editSelectedConstituency.setText(constituencyid);
//
//                        }
//                        selectedConstituencyName = "";
//                        StringBuilder builder1 = new StringBuilder();
//                        for (String s : adapterConstituencyList.arrayListconstiunecyname) {
//                            builder1.append(s);
//                            builder1.append(",");
//
//                        }
//                        selectedConstituencyName = builder1.toString();
//                        if (!selectedConstituencyName.toString().isEmpty()) {
//                            selectedConstituencyName = selectedConstituencyName.substring(0, selectedConstituencyName.length() - 1);
//                            editSelectedConstituency.setText(selectedConstituencyName);
//                        } else {
//                            editSelectedConstituency.setText(getString(R.string.select_constituency));
//                        }
//                        constituencyDialog.dismiss();
//                    }
//                });
//                textDialogconstituency.setText("Constituency");
//                constituencyDialog.show();
//
//            }
//        });

    }

    private void showFABMenu() {
        isFABOpen = true;
        floatAttachAudio.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        floatAttachImage.animate().translationX(-getResources().getDimension(R.dimen.standard_55));
        floatAttachVideo.animate().translationY(getResources().getDimension(R.dimen.standard_55));
    }

    private void closeFABMenu() {
        isFABOpen = false;
        floatAttachAudio.animate().translationY(0);
        floatAttachImage.animate().translationX(0);
        floatAttachVideo.animate().translationY(0);
    }

    @Override
    public void onBackPressed() {
        if (!isFABOpen) {
            Intent intent = new Intent();
            setResult(3, intent);
            finish();
        } else {
            closeFABMenu();
        }
    }


    @OnClick({R.id.image_photo, R.id.image_video, R.id.image_audio, R.id.image_attach_file, R.id.image_send1,
            R.id.image_delete, R.id.float_mes_compose, R.id.float_attach_audio, R.id.float_attach_video, R.id.float_attach_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_photo:
//                getImageFiles();
                break;
            case R.id.image_video:
//                getVideoFiles();
                break;
            case R.id.image_audio:
//                getAudiofile();
                break;
            case R.id.image_attach_file:
                break;
            case R.id.image_send1:
//                timelineNullCheck();
                break;
            case R.id.image_delete:
//                selectedVideoAudioUri = null;
//                if (articleDetailResponse != null) {
//                    if (videoFromResponse)
//                        videoFromResponse = false;
//                }
//                videoSelected.setVisibility(View.GONE);
//                imageDelete.setVisibility(View.GONE);
                break;
            case R.id.float_mes_compose:
                getImageFiles();
//                if (!isFABOpen) {
//                    showFABMenu();
//                } else {
//                    closeFABMenu();
//                }
                break;
            case R.id.float_attach_audio:
//                closeFABMenu();
//                getAudiofile();
                break;
            case R.id.float_attach_video:
//                closeFABMenu();
//                getVideoFiles();
                break;
            case R.id.float_attach_image:
//                closeFABMenu();
//                getImageFiles();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save, menu);
        sumbit = menu.findItem(R.id.menu_bar_save);
        sumbit.setTitle(R.string.submit);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_save:
                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(ArticleComposeActivity.this);
                alertDialog1.setMessage(getString(R.string.confirm_to_post));
                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        newpostNullCheck();
                    }
                });
                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        hideProgress();
                    }
                });
                alertDialog1.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void newpostNullCheck() {

        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0) {
            if (editTitle.getText().toString().trim().length() > 0 && editTimeSub.getText().toString().trim().length() > 0
                    && editTitle.getText().toString().trim().length() <= DmkConstants.COMPOSE_TITLE_LIMIT
                    && selectedImageAdapter.getCount() <= 3 ? true : false) {
                switch (selectedTypePos) {
                    case 0:
                        //NO
                        ((TextView) spinnerNewstype.getSelectedView()).setError(getString(R.string.select_news_type));
                        break;
                    case 1:
                        //all
                        uploadMedia();
                        break;
                    case 2:
                        //district
                        if (districtid.length() != 0) {
                            uploadMedia();
                        } else {
                            editSelectedDistrict.setError(getString(R.string.select_district));
                        }
                        break;
                    case 3:
                        //constiuenty
                        if (constituencyid.length() != 0) {
                            uploadMedia();
                        } else {
                            editSelectedDistrict.setError(getString(R.string.select_constituency));
                        }

                        break;
                }


            } else {
                if (editTitle.getText().toString().trim().length() <= 0) {
                    editTitle.setError(getString(R.string.title_missing));
                }
                if (editTimeSub.getText().toString().trim().length() <= 0) {
                    editTimeSub.setError(getString(R.string.write_some_thing_missing));
                }
                if (editTitle.getText().toString().trim().length() > DmkConstants.COMPOSE_TITLE_LIMIT) {
                    editTitle.setError(getString(R.string.title_limit));
                }
                if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 3)
                    Toast.makeText(ArticleComposeActivity.this, getString(R.string.image_limit),
                            Toast.LENGTH_SHORT).show();
            }
        } else {
            if (editTitle.getText().toString().trim().length() > 0 && editTimeSub.getText().toString().trim().length() > 0
                    && editTitle.getText().toString().trim().length() <= DmkConstants.COMPOSE_TITLE_LIMIT) {
                switch (selectedTypePos) {
                    case 0:
                        //NO
                        ((TextView) spinnerNewstype.getSelectedView()).setError(getString(R.string.select_news_type));
                        break;
                    case 1:
                        //all
                        uploadMedia();

                        break;
                    case 2:
                        //district
                        if (districtid.length() != 0) {
                            uploadMedia();
                        } else {
                            editSelectedDistrict.setError(getString(R.string.select_district));
                        }
                        break;
                    case 3:
                        //constiuenty
                        if (constituencyid.length() != 0) {
                            uploadMedia();
                        } else {
                            editSelectedDistrict.setError(getString(R.string.select_constituency));
                        }

                        break;
                }


            } else {
                if (editTitle.getText().toString().trim().length() <= 0) {
                    editTitle.setError(getString(R.string.title_missing));
                }
                if (editTimeSub.getText().toString().trim().length() <= 0) {
                    editTimeSub.setError(getString(R.string.write_some_thing_missing));
                }
                if (editTitle.getText().toString().trim().length() > DmkConstants.COMPOSE_TITLE_LIMIT) {
                    editTitle.setError(getString(R.string.title_limit));
                }
            }
        }


    }

    private void createArticle() {
        final ArticleComposeParams articleComposeParams = new ArticleComposeParams();
        articleComposeParams.setContent(editTimeSub.getText().toString());
        articleComposeParams.setTitle(editTitle.getText().toString());
        articleComposeParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        articleComposeParams.setMediatype(2);
        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0) {
            for (int i = 0; i < selectedImageAdapter.getCount(); i++) {
                selectedMediaListName.add(selectedImageAdapter.getItem(i).getImageName());
            }
        }
//        if (selectedVideoAudioUri != null) {
//            selectedMediaListName.add(videoAudioFileNameInternal);
//            newsPostParams.setVideothumb(videoThubmnail);
//        } else {
//            newsPostParams.setVideothumb("0");
//        }
        articleComposeParams.setMediaFiles(selectedMediaListName);
        ;

        switch (selectedTypePos) {
            case 1:
                //all
                articleComposeParams.setNewstype(0);
                articleComposeParams.setDistrictid(String.valueOf(0));
                articleComposeParams.setConstituencyid(String.valueOf(0));
                break;
            case 2:
                //district
                articleComposeParams.setNewstype(1);
                articleComposeParams.setDistrictid(districtid);
                articleComposeParams.setConstituencyid(String.valueOf(0));
                break;
            case 3:
                //constituency
                articleComposeParams.setNewstype(2);
                articleComposeParams.setDistrictid(String.valueOf(0));
                articleComposeParams.setConstituencyid(constituencyid);
                break;
        }

        if (Util.isNetworkAvailable()) {
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.setArticlepost(headerMap, articleComposeParams).enqueue(new Callback<NewsPostResponse>() {
                @Override
                public void onResponse(Call<NewsPostResponse> call, Response<NewsPostResponse> response) {
                    newsPostResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (newsPostResponse.getStatuscode() == 0) {
                            hideProgress();
                            if (newsPostResponse.getStatus().contains("Success")) {
                                onBackPressed();
                                Toast.makeText(ArticleComposeActivity.this, getString(R.string.article_compose_sucuess)
                                        , Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ArticleComposeActivity.this, getString(R.string.article_compose_failed)
                                        , Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, newsPostResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, newsPostResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(newsPostResponse.getSource(),
                                    newsPostResponse.getSourcedata()));
                            editor.commit();
                            showProgress();
                            createArticle();
                        }

                    } else {
                        hideProgress();
                        Toast.makeText(ArticleComposeActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                }

                @Override
                public void onFailure(Call<NewsPostResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ArticleComposeActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    onBackPressed();

                }
            });


        } else {
            Toast.makeText(this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    private void updateArticle() {
        final ArticleUpdatePostParams articleUpdatePostParams = new ArticleUpdatePostParams();
        articleUpdatePostParams.setContent(editTimeSub.getText().toString());
        articleUpdatePostParams.setTitle(editTitle.getText().toString());
        articleUpdatePostParams.setArticlesid(articleID);
        articleUpdatePostParams.setMediatype(2);

        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0) {
            for (int i = 0; i < selectedImageAdapter.getCount(); i++) {
                selectedMediaListName.add(selectedImageAdapter.getItem(i).getImageName());
            }
        }
//        if (videoAudioFileNameInternal != null) {
//            selectedMediaListName.add(videoAudioFileNameInternal);
//            newsPostParams.setVideothumb(videoThubmnail);
//        } else {
//            if (videoFromResponse) {
//                String videoFile = null;
//                for (int i = 0; i < newsDetailsResponse.getResults().getMediapath().size(); i++) {
//                    if (!isImageFile(newsDetailsResponse.getResults().getMediapath().get(i))) {
//                        videoFile = newsDetailsResponse.getResults().getMediapath().get(i);
//                    }
//                }
//                selectedMediaListName.add(videoFile);
//                newsPostParams.setVideothumb(newsDetailsResponse.getResults().getVideothumb());
//            } else {
//                newsPostParams.setVideothumb("0");
//            }
//        }

        articleUpdatePostParams.setMediaFiles(selectedMediaListName);

        articleUpdatePostParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        switch (selectedTypePos) {
            case 1:
                //all
                articleUpdatePostParams.setNewstype(0);
                articleUpdatePostParams.setDistrictid(String.valueOf(0));
                articleUpdatePostParams.setConstituencyid(String.valueOf(0));
                break;
            case 2:
                //district
                articleUpdatePostParams.setNewstype(1);
                articleUpdatePostParams.setDistrictid(districtid);
                articleUpdatePostParams.setConstituencyid(String.valueOf(0));
                break;
            case 3:
                //constituency
                articleUpdatePostParams.setNewstype(2);
                articleUpdatePostParams.setDistrictid(String.valueOf(0));
                articleUpdatePostParams.setConstituencyid(constituencyid);
                break;
        }

        if (Util.isNetworkAvailable()) {
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.updatearticle(headerMap, articleUpdatePostParams).enqueue(new Callback<NewsUpdateResponse>() {
                @Override
                public void onResponse(Call<NewsUpdateResponse> call, Response<NewsUpdateResponse> response) {
                    newsUpdateResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (newsUpdateResponse.getStatuscode() == 0) {
                            hideProgress();
                            if (newsUpdateResponse.getStatus().contains("Success")) {
                                onBackPressed();
                                Toast.makeText(ArticleComposeActivity.this, getString(R.string.article_update_sucuess)
                                        , Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ArticleComposeActivity.this, getString(R.string.article_update_failed)
                                        , Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, newsUpdateResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, newsUpdateResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(newsUpdateResponse.getSource(),
                                    newsUpdateResponse.getSourcedata()));
                            editor.commit();
                            showProgress();
                            updateArticle();
                        }
                    } else {
                        hideProgress();
                        Toast.makeText(ArticleComposeActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                }

                @Override
                public void onFailure(Call<NewsUpdateResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ArticleComposeActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    onBackPressed();

                }
            });


        } else {
            Toast.makeText(this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    private void uploadMedia() {
        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0 && selectedVideoAudioUri != null) {
//            both image and video
            mediaType = 1;
        } else if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0 && selectedVideoAudioUri == null) {
            //only image
            mediaType = 2;
        } else if (selectedImageAdapter == null && selectedVideoAudioUri != null) {
            //only video
            mediaType = 3;
        } else {
            //no media
            mediaType = 4;
        }

        if (mediaType == 1 || mediaType == 2) {
            showProgress(getString(R.string.media_uploading));
            uploadImage();
        } else if (mediaType == 3) {
            showProgress(getString(R.string.media_uploading));
            uploadVideoandAudio();
        } else {
            showProgress();
            if (isFromNewsLine)
                updateArticle();
            else
                createArticle();

        }
    }

    private void uploadImage() {
        for (int i = 0; i < selectedImageAdapter.getCount(); i++) {
            //        to send media name in s3 server
            destination = new File(getExternalFilesDir(null),
                    selectedImageAdapter.getItem(i).getImageName());
            if (selectedImageAdapter.getItem(i).getSelectedImage() != null && getImageUri(ArticleComposeActivity.this,
                    selectedImageAdapter.getItem(i).getSelectedImage()) != null) {
                createFile(ArticleComposeActivity.this, getImageUri(ArticleComposeActivity.this,
                        selectedImageAdapter.getItem(i).getSelectedImage()), destination);
                TransferObserver uploadObserver =
                        transferUtility.upload(getString(R.string.s3_bucket_article_path) +
                                selectedImageAdapter.getItem(i).getImageName(), destination);
                final int finalI = i;
                uploadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            destination.delete();
                            if (finalI == selectedImageAdapter.getCount() - 1) {
                                if (mediaType == 1)
                                    uploadVideoandAudio();
                                else {
                                    if (isFromNewsLine)
                                        updateArticle();
                                    else
                                        createArticle();
                                }

                            }
                        } else if (TransferState.FAILED == state) {
                            destination.delete();
                            if (finalI == selectedImageAdapter.getCount() - 1) {
                                if (mediaType == 1)
                                    uploadVideoandAudio();
                                else {
                                    hideProgress();
                                    Toast.makeText(ArticleComposeActivity.this,
                                            getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;


                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                        destination.delete();
                        if (finalI == selectedImageAdapter.getCount() - 1) {
                            if (mediaType == 1)
                                uploadVideoandAudio();
                            else {
                                hideProgress();
                                Toast.makeText(ArticleComposeActivity.this,
                                        getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                            }


                        }
                    }

                });
            } else {
                if (mediaType == 1)
                    uploadVideoandAudio();
                else if (isFromNewsLine)
                    // if not changed anything in media from already selected(update timeline)
                    updateArticle();
                else {
                    hideProgress();
                    Toast.makeText(ArticleComposeActivity.this,
                            getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                }

            }
        }

    }


    private void uploadVideoandAudio() {
        //Audio and video file
        if (isAudioFromRecord) {
            //audio from record
            destination = new File(
                    Environment.getExternalStorageDirectory().getAbsolutePath().toString() +
                            "/Voice Recorder/RECORDING_"
                            + videoAudioFileNameInternal);
        } else {
            // audio from gallery
            destination = new File(getExternalFilesDir(null), videoAudioFileNameInternal);
        }
        createFile(ArticleComposeActivity.this, selectedVideoAudioUri, destination);
        TransferObserver uploadObserver =
                transferUtility.upload(getString(R.string.s3_bucket_article_path) +
                        videoAudioFileNameInternal, destination);
        final File finalDestination = destination;
        uploadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    destination.delete();
                    if (isFromNewsLine)
                        updateArticle();
                    else
                        createArticle();
                } else if (TransferState.FAILED == state) {
                    destination.delete();
                    hideProgress();
                    Toast.makeText(ArticleComposeActivity.this,
                            getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;
            }

            @Override
            public void onError(int id, Exception ex) {
                ex.printStackTrace();
                destination.delete();
                hideProgress();
                Toast.makeText(ArticleComposeActivity.this,
                        getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
            }

        });
    }


    public void getDistrictList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            DistrictBoolean = false;
            districtList.clear();
            final NewComposeConstituenctPost newComposeConstituenctPost = new NewComposeConstituenctPost();
            newComposeConstituenctPost.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            //districtList.add(0, new DistrictsItem(getString(R.string.select_district), 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getDistrictListforNewsCompose(headerMap, newComposeConstituenctPost).enqueue(new Callback<NewsPartyDistrictResponse>() {
                @Override
                public void onResponse(Call<NewsPartyDistrictResponse> call, Response<NewsPartyDistrictResponse> response) {
                    newsPartyDistrictResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        hideProgress();
                        if (newsPartyDistrictResponse.getStatuscode() == 0) {
                            for (int i = 0; i < newsPartyDistrictResponse.getResults().size(); i++) {
                                districtList.add(new DistrictsItem(newsPartyDistrictResponse.getResults().get(i).getDistrictName(),
                                        newsPartyDistrictResponse.getResults().get(i).getId()));

                            }
                            adapterdivisionList = new NewComposeDistrictAdpater(districtList, ArticleComposeActivity.this, arraylistDistrictIdforedit);
                            listviewDistrict.setAdapter(adapterdivisionList);
                            dialogDistrictButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    StringBuilder builder = new StringBuilder();
                                    for (String s : adapterdivisionList.arraylistDistrictId) {
                                        builder.append(s);
                                        builder.append(",");

                                    }
                                    districtid = builder.toString();
                                    if (!districtid.toString().isEmpty()) {
                                        districtid = districtid.substring(0, districtid.length() - 1);


                                    }
                                    selectedDistrictName = "";
                                    StringBuilder builder1 = new StringBuilder();
                                    for (String s : adapterdivisionList.arraylistDistrictName) {
                                        builder1.append(s);
                                        builder1.append(",");

                                    }
                                    selectedDistrictName = builder1.toString();
                                    if (!selectedDistrictName.toString().isEmpty()) {
                                        selectedDistrictName = selectedDistrictName.substring(0, selectedDistrictName.length() - 1);
                                        editSelectedDistrict.setText(selectedDistrictName);

                                    } else {
                                        editSelectedDistrict.setText(getString(R.string.select_district));
                                    }
                                    districtDialog.dismiss();
                                }
                            });
                            textDialogDistrict.setText(getString(R.string.district));
                            districtDialog.show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, newsPartyDistrictResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, newsPartyDistrictResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(newsPartyDistrictResponse.getSource(),
                                    newsPartyDistrictResponse.getSourcedata()));
                            editor.commit();
                            getDistrictList();
                        }


                    } else {
                        hideProgress();
                        Toast.makeText(ArticleComposeActivity.this, getString(R.string.no_it_district), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<NewsPartyDistrictResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ArticleComposeActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                }
            });
//            dmkAPI.getDistrictList().enqueue(new Callback<DistrictListResponse>() {
//                @Override
//                public void onResponse(Call<DistrictListResponse> call, Response<DistrictListResponse> response) {
//                    districtListResponse = response.body();
//                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
//                        hideProgress();
//                        for (int i = 0; i < districtListResponse.getResults().getDistricts().size(); i++) {
//                            districtList.add(new DistrictsItem(districtListResponse.getResults().getDistricts().get(i).getDistrictName(),
//                                    districtListResponse.getResults().getDistricts().get(i).getId()));
//
//                        }
//
//
//                        adapterdivisionList = new NewComposeDistrictAdpater(districtList,ArticleComposeActivity.this,arraylistDistrictIdforedit);
//                        listviewDistrict.setAdapter(adapterdivisionList);
//                        dialogDistrictButton.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                StringBuilder builder = new StringBuilder();
//                                for(String s : adapterdivisionList.arraylistDistrictId) {
//                                    builder.append(s);
//                                    builder.append(",");
//
//                                }
//                                districtid = builder.toString();
//                                if (!districtid.toString().isEmpty())
//                                {
//                                    districtid= districtid.substring(0, districtid.length() - 1);
//
//
//                                }
//                                selectedDistrictName="";
//                                StringBuilder builder1 = new StringBuilder();
//                                for(String s : adapterdivisionList.arraylistDistrictName) {
//                                    builder1.append(s);
//                                    builder1.append(",");
//
//                                }
//                                selectedDistrictName = builder1.toString();
//                                if (!selectedDistrictName.toString().isEmpty())
//                                {
//                                    selectedDistrictName= selectedDistrictName.substring(0, selectedDistrictName.length() - 1);
//                                    editSelectedDistrict.setText(selectedDistrictName);
//
//                                }
//                                else
//                                {
//                                    editSelectedDistrict.setText(getString(R.string.select_district));
//                                }
//                                districtDialog.dismiss();
//                            }
//                        });
//                        textDialogDistrict.setText(getString(R.string.district));
//                        districtDialog.show();
//
//                    } else {
//                        hideProgress();
//                        Toast.makeText(ArticleComposeActivity.this, getString(R.string.no_it_district), Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<DistrictListResponse> call, Throwable t) {
//                    hideProgress();
//                    Toast.makeText(ArticleComposeActivity.this, getString(R.string.no_it_district), Toast.LENGTH_SHORT).show();
//                }
//            });
        } else {
            Toast.makeText(ArticleComposeActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    public void getConsituencyList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            ConstituencyBoolean = false;
            constituencyList.clear();
            //constituencyList.add(0, new NewsComposeConstituencyResultsItem(getString(R.string.select_constituency), 0));
            final NewComposeConstituenctPost newComposeConstituenctPost = new NewComposeConstituenctPost();
            newComposeConstituenctPost.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getConstituencyListforNewsCompose(headerMap, newComposeConstituenctPost).enqueue(new Callback<NewsComposeConstituencyListResponse>() {
                @Override
                public void onResponse(Call<NewsComposeConstituencyListResponse> call, Response<NewsComposeConstituencyListResponse> response) {
                    newsComposeConstituencyListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        hideProgress();
                        if (newsComposeConstituencyListResponse.getStatuscode() == 0) {
                            for (int i = 0; i < newsComposeConstituencyListResponse.getResults().size(); i++) {
                                constituencyList.add(new NewsComposeConstituencyResultsItem(newsComposeConstituencyListResponse.getResults().get(i).getConstituencyName(),
                                        newsComposeConstituencyListResponse.getResults().get(i).getId()));
                            }
                            adapterConstituencyList = new NewsComposeConstituencyAdapter(constituencyList, ArticleComposeActivity.this, arraylistConstituencyforedit);
                            listviewconstituency.setAdapter(adapterConstituencyList);
                            dialogconstituencyButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    StringBuilder builder = new StringBuilder();
                                    for (String s : adapterConstituencyList.arrayListconstiunecyid) {
                                        builder.append(s);
                                        builder.append(",");

                                    }
                                    constituencyid = builder.toString();
                                    if (!constituencyid.toString().isEmpty()) {
                                        constituencyid = constituencyid.substring(0, constituencyid.length() - 1);

                                    }
                                    selectedConstituencyName = "";
                                    StringBuilder builder1 = new StringBuilder();
                                    for (String s : adapterConstituencyList.arrayListconstiunecyname) {
                                        builder1.append(s);
                                        builder1.append(",");

                                    }
                                    selectedConstituencyName = builder1.toString();
                                    if (!selectedConstituencyName.toString().isEmpty()) {
                                        selectedConstituencyName = selectedConstituencyName.substring(0, selectedConstituencyName.length() - 1);
                                        editSelectedDistrict.setText(selectedConstituencyName);
                                    } else {
                                        editSelectedDistrict.setText(getString(R.string.select_constituency));
                                    }
                                    constituencyDialog.dismiss();
                                }
                            });
                            textDialogconstituency.setText("Constituency");
                            constituencyDialog.show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, newsComposeConstituencyListResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, newsComposeConstituencyListResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(newsComposeConstituencyListResponse.getSource(),
                                    newsComposeConstituencyListResponse.getSourcedata()));
                            editor.commit();
                            getConsituencyList();
                        }
                    } else {
                        hideProgress();
                        Toast.makeText(ArticleComposeActivity.this, getString(R.string.no_it_constituency), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<NewsComposeConstituencyListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ArticleComposeActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(ArticleComposeActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    private void getImageFiles() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.media_dialog);
        // Set dialog title
        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
        textCamera.setText(R.string.camera);
        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
        imageCamera.setImageResource(R.mipmap.icon_photo_camera);
        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
        textGallery.setText(R.string.gallery);
        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
        imageGallery.setImageResource(R.mipmap.icon_gallery);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ArticleComposeActivity.this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_REQUEST_CODE_CAMERA);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, SELECT_IMAGE_FROM_CAMERA);
                    }
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, SELECT_IMAGE_FROM_CAMERA);
                }
                dialog.dismiss();
            }
        });
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ArticleComposeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE_GALLERY);
                    } else {
//                        Intent intent = new Intent();
//                        intent.setType("image/*");
//                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
//                        intent.setAction(Intent.ACTION_GET_CONTENT);//
//                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_IMAGE_FROM_GALLERY);
                        Intent intent = new Intent(
                                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                        intent.setType("image/*");
                        startActivityForResult(
                                Intent.createChooser(intent, "Select File"),
                                SELECT_IMAGE_FROM_GALLERY);

                    }
                } else {
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
//                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_IMAGE_FROM_GALLERY);

                    Intent intent = new Intent(
                            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_IMAGE_FROM_GALLERY);

                }
                dialog.dismiss();
            }

        });

        // set values for custom dialog components - text, image and button
        dialog.show();


    }

    private void getVideoFiles() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.media_dialog);
        // Set dialog title
        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
        textCamera.setText(R.string.video);
        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
        imageCamera.setImageResource(R.mipmap.icon_photo_camera);
        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
        textGallery.setText(R.string.gallery);
        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
        imageGallery.setImageResource(R.mipmap.icon_gallery);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ArticleComposeActivity.this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_REQUEST_CODE_CAMERA);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        startActivityForResult(intent, SELECT_VIDEO_FROM_CAMERA);
                    }
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    startActivityForResult(intent, SELECT_VIDEO_FROM_CAMERA);
                }
                dialog.dismiss();
            }
        });
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ArticleComposeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE_GALLERY);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("icon_attach_video/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_VIDEO_FROM_GALLERY);
                    }
                } else {
                    Intent intent = new Intent();
                    intent.setType("icon_attach_video/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_VIDEO_FROM_GALLERY);
                }
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void getAudiofile() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.media_dialog);
        // Set dialog title
        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
        textCamera.setText(R.string.record);
        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
        imageCamera.setImageResource(R.mipmap.icon_mic);
        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
        textGallery.setText(R.string.gallery);
        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
        imageGallery.setImageResource(R.mipmap.icon_gallery);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAudioRecordDialog();
                dialog.dismiss();
            }
        });
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("icon_attach_audio/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Media Files"), REQUEST_TAKE_GALLERY_VIDEO_AUDIO);
                dialog.dismiss();
            }
        });
        dialog.show();


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == SELECT_IMAGE_FROM_GALLERY && resultCode == RESULT_OK
                    && null != data) {
                imageFromGalleryResult(data);
            } else if (requestCode == SELECT_IMAGE_FROM_CAMERA && resultCode == RESULT_OK &&
                    data != null) {
                imageFromCameraResult(data);
            } else if (requestCode == SELECT_VIDEO_FROM_GALLERY && resultCode == RESULT_OK &&
                    data != null) {
                videoFromGalleryResult(data);
            } else if (requestCode == SELECT_VIDEO_FROM_CAMERA && resultCode == RESULT_OK &&
                    data != null) {
                videoFromCameraResult(data);
            } else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO_AUDIO && resultCode == RESULT_OK &&
                    data != null) {
                getAudioGalleryResult(data);
            }
        } catch (Exception e) {
            Toast.makeText(this, "select image only", Toast.LENGTH_LONG)
                    .show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void imageFromCameraResult(Intent data) {

        gridImage.setVisibility(View.VISIBLE);
        if (selectedVideoAudioUri != null || videoFromResponse) {
            videoSelected.setVisibility(View.VISIBLE);
            imageDelete.setVisibility(View.VISIBLE);
        } else {
            videoSelected.setVisibility(View.INVISIBLE);
            imageDelete.setVisibility(View.INVISIBLE);
        }

        Bitmap capturedImageBitmap = null;
        if (data != null) {
            capturedImageBitmap = (Bitmap) data.getExtras().get("data");
        }

        if (capturedImageBitmap != null) {
            selectedImageList.add(new SelectedImage(null, 1,
                    capturedImageBitmap, false, System.currentTimeMillis() + ".jpg"));
        }
        selectedImageAdapter = new SelectedImageAdapter(selectedImageList, this);
        gridImage.setAdapter(selectedImageAdapter);
    }

    private void imageFromGalleryResult(Intent data) {

        boolean found = false;

        Uri mImageUri = data.getData();
        String rettt = getImagePath(mImageUri);
        String extenstion = getFileExt(rettt);
        if (extenstion != null) {
            for (String element : imageFormat) {
                if (element.equals(extenstion)) {
                    found = true;
                    break;
                }
            }
            if (found) {
                gridImage.setVisibility(View.VISIBLE);
                if (selectedVideoAudioUri != null || videoFromResponse) {
                    videoSelected.setVisibility(View.VISIBLE);
                    imageDelete.setVisibility(View.VISIBLE);
                } else {
                    videoSelected.setVisibility(View.INVISIBLE);
                    imageDelete.setVisibility(View.INVISIBLE);
                }

                //                Fetch Single Image
                if (data.getData() != null) {
                    if (mImageUri != null) {
                        selectedImageList.add(new SelectedImage(null, 1,
                                compressInputImage(mImageUri, this), false, System.currentTimeMillis() + ".jpg"));
                    }
                    selectedImageAdapter = new SelectedImageAdapter(selectedImageList, this);
                    gridImage.setAdapter(selectedImageAdapter);
                } else {
//                    Fecth MultipleImage
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            if (uri != null) {
                                selectedImageList.add(new SelectedImage(null, 1,
                                        compressInputImage(uri, this), false, System.currentTimeMillis() + ".jpg"));
                            }
                        }


                        selectedImageAdapter = new SelectedImageAdapter(selectedImageList, this);
                        gridImage.setAdapter(selectedImageAdapter);


                    }
                }
            } else {
                System.out.println("The value is Not found!");
                Toast.makeText(ArticleComposeActivity.this, "select image only", Toast.LENGTH_SHORT).show();
            }
        }
//        else
//        {
//            Toast.makeText(ArticleComposeActivity.this, "select image only", Toast.LENGTH_SHORT).show();
//        }

    }

    private void videoFromGalleryResult(Intent data) {

        gridImage.setVisibility(View.VISIBLE);
        videoSelected.setVisibility(View.VISIBLE);
        imageDelete.setVisibility(View.VISIBLE);

        selectedVideoAudioUri = data.getData();
        // OI FILE Manager Path
        galleryVideoPath = selectedVideoAudioUri.getPath();
        // MEDIA GALLERY Path
//        recordedVideoPath = getPath(selectedImageUri);
//        "/storage/2348-13EA/DCIM/Camera/VID_20180511_182119029.mp4"
//        /storage/2348-13EA/DCIM/Camera/VID_20180511_184203378.mp4
        videoSelected.setUp(galleryVideoPath
                , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
        Glide.with(this)
                .load(Uri.fromFile(new File(galleryVideoPath)))
                .into(videoSelected.thumbImageView);
        if (selectedVideoAudioUri != null) {
            videoAudioFileNameInternal = System.currentTimeMillis() + ".mp4";
        }
    }

    private void videoFromCameraResult(Intent data) {

        gridImage.setVisibility(View.VISIBLE);
        videoSelected.setVisibility(View.VISIBLE);
        imageDelete.setVisibility(View.VISIBLE);

        selectedVideoAudioUri = data.getData();
        // OI FILE Manager Path
//        galleryVideoPath = selectedImageUri.getPath();
        // MEDIA GALLERY Path
        recordedVideoPath = getPath(selectedVideoAudioUri);
        videoSelected.setUp(recordedVideoPath
                , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
        Glide.with(this)
                .load(Uri.fromFile(new File(recordedVideoPath)))
                .into(videoSelected.thumbImageView);
        if (selectedVideoAudioUri != null) {
            videoAudioFileNameInternal = System.currentTimeMillis() + ".mp4";
        }

    }


    private void getAudioGalleryResult(Intent data) {
        gridImage.setVisibility(View.VISIBLE);
        videoSelected.setVisibility(View.VISIBLE);
        imageDelete.setVisibility(View.VISIBLE);

        //Fetch Video and Audio File
        selectedVideoAudioUri = data.getData();
        // OI FILE Manager Path
        galleryAudioPath = selectedVideoAudioUri.getPath();
        // MEDIA GALLERY Path
        //recordedVideoPath = getPath(selectedImageUri);
        videoSelected.setUp(galleryAudioPath
                , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
        if (isVideoFile(galleryAudioPath)) {
            Glide.with(this)
                    .load(Uri.fromFile(new File(galleryAudioPath)))
                    .into(videoSelected.thumbImageView);
        } else {
            Glide.with(this)
                    .load(R.mipmap.icon_audio_thumbnail).into(videoSelected.thumbImageView);

        }
        if (selectedVideoAudioUri != null) {
            videoAudioFileNameInternal = System.currentTimeMillis() + ".mp3";
        }
    }

    private void getAudioRecordedResult() {

        gridImage.setVisibility(View.VISIBLE);
        videoSelected.setVisibility(View.VISIBLE);
        imageDelete.setVisibility(View.VISIBLE);

        selectedVideoAudioUri = Uri.parse(mOutputFile.getAbsolutePath());
        videoSelected.setUp(mOutputFile.getAbsolutePath()
                , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");

        //Load thumbnail image for only local storage videos
        Glide.with(this)
                .load(R.mipmap.icon_audio_thumbnail).into(videoSelected.thumbImageView);
        if (selectedVideoAudioUri != null) {
            videoAudioFileNameInternal = audioRecordedPathName;
        }
        isAudioFromRecord = true;
    }

    public void showAudioRecordDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_timeline_audiorecord);
        textTimer = (TextView) dialog.findViewById(R.id.text_dialog);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording(true);
                getAudioRecordedResult();
                dialog.dismiss();
            }
        });
        dialog.show();
        startAudioRecording();
    }


    private void startAudioRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
            mRecorder.setAudioEncodingBitRate(48000);
        } else {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.setAudioEncodingBitRate(64000);
        }
        mRecorder.setAudioSamplingRate(16000);
        mOutputFile = getOutputFile();
        mOutputFile.getParentFile().mkdirs();
        mRecorder.setOutputFile(mOutputFile.getAbsolutePath());

        try {
            mRecorder.prepare();
            mRecorder.start();
            mStartTime = SystemClock.elapsedRealtime();
            mHandler.postDelayed(mTickExecutor, 100);
        } catch (IOException e) {
        }
    }

    protected void stopRecording(boolean saveFile) {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        mStartTime = 0;

        mHandler.removeCallbacks(mTickExecutor);
        if (!saveFile && mOutputFile != null) {
            mOutputFile.delete();
        }

    }

    private void audioTimer() {
        long time = (mStartTime < 0) ? 0 : (SystemClock.elapsedRealtime() - mStartTime);
        int minutes = (int) (time / 60000);
        int seconds = (int) (time / 1000) % 60;
        int milliseconds = (int) (time / 100) % 10;
        textTimer.setText(minutes + ":" + (seconds < 10 ? "0" + seconds : seconds));
        if (mRecorder != null) {
            amplitudes[i] = mRecorder.getMaxAmplitude();
            //Log.d("Voice Recorder","amplitude: "+(amplitudes[i] * 100 / 32767));
            if (i >= amplitudes.length - 1) {
                i = 0;
            } else {
                ++i;
            }
        }
    }


    private File getOutputFile() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US);
        audioRecordedPathName = dateFormat.format(new Date()) + ".m4a";
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/Voice Recorder/RECORDING_"
                + audioRecordedPathName);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    private void getDetailArticle() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            final ArticleDetailParams articleDetailParams = new ArticleDetailParams();
            articleDetailParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            articleDetailParams.setArticlesid(articleID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getArticleDetails(headerMap, articleDetailParams).enqueue(new Callback<ArticleDetailResponse>() {
                @Override
                public void onResponse(Call<ArticleDetailResponse> call, Response<ArticleDetailResponse> response) {
                    hideProgress();
                    articleDetailResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (articleDetailResponse.getStatuscode() == 0) {
                            editTimeSub.setText(articleDetailResponse.getResults().getContent());
                            editTitle.setText(articleDetailResponse.getResults().getTitle());
                            if (editNewsType == 0) {
                                editSelectedDistrict.setVisibility(View.GONE);
                                //                    editSelectedConstituency.setVisibility(View.GONE);
                                spinnerNewstype.setSelection(1);
                                selectedTypePos = 1;
                            } else if (editNewsType == 1) {
                                distriIDforedit = intent.getStringExtra("Newstypeid");
                                editSelectedDistrict.setVisibility(View.VISIBLE);
                                //                    editSelectedConstituency.setVisibility(View.GONE);
                                spinnerNewstype.setSelection(2);
                                selectedTypePos = 2;
                                arraylistDistrictIdforedit = new ArrayList<String>(Arrays.asList(distriIDforedit.split(",")));
                            } else if (editNewsType == 2) {
                                conIDTestforedit = intent.getStringExtra("Newstypeid");
                                //                    editSelectedConstituency.setVisibility(View.VISIBLE);
                                editSelectedDistrict.setVisibility(View.VISIBLE);
                                spinnerNewstype.setSelection(3);
                                selectedTypePos = 3;
                                arraylistConstituencyforedit = new ArrayList<String>(Arrays.asList(conIDTestforedit.split(",")));
                            }

                            for (int i = 0; i < articleDetailResponse.getResults().getMediapath().size(); i++) {
                                if (isImageFile(articleDetailResponse.getResults().getMediapath().get(i))) {
                                    //Image Type
                                    gridImage.setVisibility(View.VISIBLE);
                                    selectedImageList.add(new SelectedImage(null, 1, null,
                                            true, articleDetailResponse.getResults().getMediapath().get(i)));
                                    selectedImageAdapter = new SelectedImageAdapter(selectedImageList, ArticleComposeActivity.this);
                                    gridImage.setAdapter(selectedImageAdapter);
                                } else {
                                    //Video and audio Type
                                    String videoPath = null;
                                    if (!isImageFile(articleDetailResponse.getResults().getMediapath().get(i))) {
                                        videoPath = articleDetailResponse.getResults().getMediapath().get(i);
                                    }
                                    videoSelected.setVisibility(View.VISIBLE);
                                    imageDelete.setVisibility(View.VISIBLE);
                                    videoSelected.setUp(proxy.getProxyUrl(getString(R.string.s3_image_url_download) +
                                                    getString(R.string.s3_bucket_article_path) +
                                                    videoPath)
                                            , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                                    videoSelected.setSoundEffectsEnabled(true);
                                    if (isAudioFile(videoPath)) {
                                        Glide.with(ArticleComposeActivity.this).
                                                load(R.mipmap.icon_audio_thumbnail).
                                                into(videoSelected.thumbImageView);
                                    } else {
                                        if (!articleDetailResponse.getResults().getVideothumb().equalsIgnoreCase("0")) {
                                            Glide.with(ArticleComposeActivity.this).
                                                    load(articleDetailResponse.getResults().getVideothumb()).
                                                    into(videoSelected.thumbImageView);
                                        } else {
                                            Glide.with(ArticleComposeActivity.this).
                                                    load(R.mipmap.icon_video_thumbnail)
                                                    .into(videoSelected.thumbImageView);
                                        }


                                    }

                                    videoFromResponse = true;
                                }
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, articleDetailResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, articleDetailResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(articleDetailResponse.getSource(),
                                    articleDetailResponse.getSourcedata()));
                            editor.commit();
                            getDetailArticle();
                        }


                    } else {
                        Toast.makeText(ArticleComposeActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ArticleDetailResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ArticleComposeActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(ArticleComposeActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }
}
