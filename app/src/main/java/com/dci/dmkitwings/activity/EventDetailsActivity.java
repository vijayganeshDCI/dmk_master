package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.ImageViewPageAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.AddEventsResponse;
import com.dci.dmkitwings.model.ShareCount;
import com.dci.dmkitwings.model.ShareCountResponse;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.model.ViewEventResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.dci.dmkitwings.view.CustomTextViewBold;
import com.firebase.client.utilities.Base64;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.jzvd.JZVideoPlayerStandard;
import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.dmkitwings.app.DmkApplication.getContext;
import static com.dci.dmkitwings.app.DmkApplication.getProxy;

public class EventDetailsActivity extends BaseActivity {

    @BindView(R.id.image_time)
    ImageView imageTime;
    @BindView(R.id.videoplayer_time)
    JZVideoPlayerStandard videoplayerTime;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    @BindView(R.id.text_posted_by)
    CustomTextView textPostedBy;
    @BindView(R.id.image_menu)
    ImageView imageMenu;
    @BindView(R.id.text_designation)
    CustomTextViewBold textDesignation;
    @BindView(R.id.text_post_date)
    CustomTextView textPostDate;
    @BindView(R.id.text_label_event_title)
    CustomTextViewBold textLabelEventTitle;
    @BindView(R.id.text_label_event_content)
    CustomTextView textLabelEventContent;
    @BindView(R.id.view_bottom_1)
    View viewBottom1;
    @BindView(R.id.cons_time_item)
    ConstraintLayout consTimeItem;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;
    @BindView(R.id.image_view_pager)
    ViewPager imageViewPager;
    @BindView(R.id.view_pager_indicator)
    CircleIndicator viewPagerIndicator;
    @BindView(R.id.videoplayer_detail)
    JZVideoPlayerStandard videoplayerDetail;
    @BindView(R.id.youtube_player_view)
    YouTubePlayerView youtubePlayerView;
    @BindView(R.id.youtube_player_view_detail)
    YouTubePlayerView youtubePlayerViewDetail;
    @BindView(R.id.text_posted_view_count)
    CustomTextView textPostedViewCount;
    private ArrayList<Integer> imageList;

    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    private int eventID;
    private ViewEventResponse viewEventResponse;
    private Date currentDate;
    private HttpProxyCacheServer proxy;
    ImageViewPageAdapter imageViewPageAdapter;
    @BindView(R.id.image_share)
    CustomTextView textShare;
    int districtID;
    AddEventsResponse addEventsResponse;
    private ShareCountResponse shareCountResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_events_details, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        frameLayoutNotification.setVisibility(View.GONE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toolbarHome.setVisibility(View.GONE);
        proxy = getProxy(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Intent bundle = getIntent();
        if (getIntent() != null) {
            eventID = bundle.getIntExtra("eventID", 0);
        }

        imageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(EventDetailsActivity.this, imageMenu);
                //inflating menu_icon from xml resource
                popup.inflate(R.menu.comment_item_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit:
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(EventDetailsActivity.this);
                                alertDialog.setMessage(R.string.confirm_to_edit);
                                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(EventDetailsActivity.this, EventComposeActivity.class);
                                        intent.putExtra("eventID", eventID);
                                        intent.putExtra("isFromEdit", true);
                                        intent.putExtra("districtID", districtID);
                                        startActivity(intent);
                                    }

                                });
                                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                                return true;
                            case R.id.delete:
                                //handle menu2 click
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(EventDetailsActivity.this);
                                alertDialog1.setMessage(R.string.confirm_to_delete);
                                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        deleteEvent(eventID);
                                    }

                                });
                                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog1.show();
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
            }
        });

        textShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addShareCount();
            }
        });

        imageProPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(getContext()).inflate(R.layout.profile_bubble, null);
                PopupWindow popupWindow = BubblePopupHelper.create(getContext(), bubbleLayout);
                CircleImageView imageprofilepic = (CircleImageView) bubbleLayout.findViewById(R.id.image_pro_pic_bubble);
                CustomTextView textpostedby = (CustomTextView) bubbleLayout.findViewById(R.id.text_posted_by_bubble);
                CustomTextView textdesgination = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_designation_bubble);
                CustomTextView textdistrict = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_district_bubble);
                CustomTextView textconstituency = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_constituency_bubble);
                CustomTextView textPartBoothWardVillage = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_common_bubble);
                if (viewEventResponse.getResults().getUser().getUserid() != 1 && viewEventResponse.getResults().getUser().getUserid() != sharedPreferences.getInt(DmkConstants.USERID, 0)) {

                    switch (viewEventResponse.getResults().getUser().getLevel()) {
                        case 1:

                            // செயலாளர்
//                            துணை செயலாளர்
//                            மண்டல ஒருங்கிணைப்பாளர்
                            textpostedby.setText(viewEventResponse.getResults().getUser().getFirstName() + " " +
                                    viewEventResponse.getResults().getUser().getLastName());
                            textdesgination.setText(viewEventResponse.getResults().getUser().getDesignation());
                            textdistrict.setVisibility(View.GONE);
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 2:
                            //மாவட்ட ஒருங்கிணைப்பாளர் district
                            textpostedby.setText(viewEventResponse.getResults().getUser().getFirstName() + " " +
                                    viewEventResponse.getResults().getUser().getLastName());
                            textdesgination.setText(viewEventResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(viewEventResponse.getResults().getUser().getDistrict());
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 3:
                            //தொகுதி ஒருங்கிணைப்பாளர் constituency
                            textpostedby.setText(viewEventResponse.getResults().getUser().getFirstName() + " " +
                                    viewEventResponse.getResults().getUser().getLastName());
                            textdesgination.setText(viewEventResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(viewEventResponse.getResults().getUser().getDistrict());
                            textconstituency.setText(viewEventResponse.getResults().getUser().getConstituency());
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                            //பகுதி ஒருங்கிணைப்பாளர் part
                            //நகராட்சி ஒருங்கிணைப்பாளர் municipality
                            //பேரூராட்சி ஒருங்கிணைப்பாளர் township
                            //ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                            textpostedby.setText(viewEventResponse.getResults().getUser().getFirstName() + " " +
                                    viewEventResponse.getResults().getUser().getLastName());
                            textdesgination.setText(viewEventResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(viewEventResponse.getResults().getUser().getDistrict());
                            textconstituency.setText(viewEventResponse.getResults().getUser().getConstituency());
                            textPartBoothWardVillage.setText(viewEventResponse.getResults().getUser().getPart());
                            break;
                        case 5:
                        case 6:
                        case 9:
                        case 10:
                        case 13:
                        case 14:
                        case 18:
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            //கிளை ஒருங்கிணைப்பாளர் ward
                            // வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //கிளை ஒருங்கிணைப்பாளர்  ward
                            textpostedby.setText(viewEventResponse.getResults().getUser().getFirstName() + " " +
                                    viewEventResponse.getResults().getUser().getLastName());
                            textdesgination.setText(viewEventResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(viewEventResponse.getResults().getUser().getDistrict());
                            textconstituency.setText(viewEventResponse.getResults().getUser().getConstituency());
                            textPartBoothWardVillage.setText(viewEventResponse.getResults().getUser().getWard());
                            break;
                        case 7:
                        case 11:
                        case 15:
                        case 19:
                            //வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                            textpostedby.setText(viewEventResponse.getResults().getUser().getFirstName() + " " +
                                    viewEventResponse.getResults().getUser().getLastName());
                            textdesgination.setText(viewEventResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(viewEventResponse.getResults().getUser().getDistrict());
                            textconstituency.setText(viewEventResponse.getResults().getUser().getConstituency());
                            textPartBoothWardVillage.setText(viewEventResponse.getResults().getUser().getBooth());
                            break;
                        case 17:
                            //ராட்சிஊ ஒருங்கிணைப்பாளர் villagepanchayat
                            textpostedby.setText(viewEventResponse.getResults().getUser().getFirstName() + " " +
                                    viewEventResponse.getResults().getUser().getLastName());
                            textdesgination.setText(viewEventResponse.getResults().getUser().getDesignation());
                            textdistrict.setText(viewEventResponse.getResults().getUser().getDistrict());
                            textconstituency.setText(viewEventResponse.getResults().getUser().getConstituency());
                            textPartBoothWardVillage.setText(viewEventResponse.getResults().getUser().getVillage());
                            break;
                    }


                    final Random random = new Random();
                    int[] location = new int[2];
                    imageProPic.getLocationInWindow(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.LEFT);
                    popupWindow.showAtLocation(imageProPic, Gravity.AXIS_PULL_BEFORE, imageProPic.getWidth() + 25, -185);
                    universalImageLoader(imageprofilepic, getString(R.string.s3_bucket_profile_path),
                            viewEventResponse.getResults().getUser().getAvatar(),
                            R.mipmap.icon_pro_image_loading_256,
                            R.mipmap.icon_pro_image_loading_256);
                }
            }
        });
        videoplayerTime.setVisibility(View.GONE);
        videoplayerDetail.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();
        getEventDetails();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    private void getEventDetails() {
        TimeLineDelete timeLineDelete = new TimeLineDelete();
        timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        timeLineDelete.setEventid(eventID);
        if (Util.isNetworkAvailable()) {
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getEventDetails(headerMap, timeLineDelete).enqueue(new Callback<ViewEventResponse>() {
                @Override
                public void onResponse(Call<ViewEventResponse> call, Response<ViewEventResponse> response) {
                    hideProgress();
                    viewEventResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (viewEventResponse.getStatuscode() == 0) {
                            districtID = viewEventResponse.getResults().getDistrict_id();


                            if (viewEventResponse.getResults().getEvent_createdby()
                                    == sharedPreferences.getInt(DmkConstants.USERID, 0))
                                textPostedBy.setText("You");
                            else
                                textPostedBy.setText(
                                        viewEventResponse.getResults().getUser() != null ? (viewEventResponse.getResults().getUser().getFirstName() + " " +
                                                viewEventResponse.getResults().getUser().getLastName()) : ""
                                );
                            textDesignation.setText(
                                    viewEventResponse.getResults().getUser() != null ?
                                            viewEventResponse.getResults().getUser().getDesignation() : "");
                            textPostDate.setText(getDateFormat(viewEventResponse.getResults().getEvent_start_date())
                                    + " to " + getDateFormat(viewEventResponse.getResults().getEvent_end_date()));
                            textLabelEventTitle.setText(viewEventResponse.getResults().getEvent_title());
                            textLabelEventContent.setText(viewEventResponse.getResults().getEvent_desc());

                            if (viewEventResponse.getResults().getViewcount() != 0) {
                                textPostedViewCount.setVisibility(View.VISIBLE);
                                textPostedViewCount.setText("" + viewEventResponse.getResults().getViewcount());
                            } else {
                                textPostedViewCount.setVisibility(View.GONE);
                            }

                            if (viewEventResponse.getResults().getSharecount() > 99) {
                                textShare.setText("(99+)");
                            } else if (viewEventResponse.getResults().getSharecount() == 0) {
                                textShare.setText("");
                            } else {
                                textShare.setText("(" + viewEventResponse.getResults().getSharecount() + ")");
                            }

                            universalImageLoader(imageProPic, getString(R.string.s3_bucket_profile_path),
                                    viewEventResponse.getResults().getUser().getAvatar(),
                                    R.mipmap.icon_pro_image_loading_256,
                                    R.mipmap.icon_pro_image_loading_256);

                            List<String> imageFiles = new ArrayList<String>();
                            List<String> videoFile = new ArrayList<String>();


                            for (int i = 0; i < viewEventResponse.getResults().getMediafiles().size(); i++) {
                                if (isImageFile(viewEventResponse.getResults().getMediafiles().get(i))) {
                                    imageFiles.add(viewEventResponse.getResults().getMediafiles().get(i));
                                } else {
                                    videoFile.add(viewEventResponse.getResults().getMediafiles().get(i));
                                }
                            }

                            if (imageFiles.size() > 0 && videoFile.size() > 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                youtubePlayerView.setVisibility(View.INVISIBLE);
                                if (viewEventResponse.getResults().getMedia_type() != 1) {
                                    videoplayerDetail.setVisibility(View.VISIBLE);
                                    youtubePlayerViewDetail.setVisibility(View.GONE);

                                    videoplayerDetail.setUp(proxy.getProxyUrl(
                                            getString(R.string.s3_image_url_download) +
                                                    getString(R.string.s3_bucket_events_path) +
                                                    videoFile.get(0))
                                            , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                                    videoplayerDetail.setSoundEffectsEnabled(true);
                                    //thumbnail generation
                                    if (isAudioFile(getString(R.string.s3_image_url_download) +
                                            getString(R.string.s3_bucket_events_path) +
                                            videoFile.get(0))) {
                                        Glide.with(EventDetailsActivity.this)
                                                .load(R.mipmap.icon_audio_thumbnail).into(videoplayerDetail.thumbImageView);
                                    } else {
                                        RequestOptions requestOptions = new RequestOptions();
                                        requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                                        requestOptions.error(R.mipmap.icon_video_thumbnail);
                                        Glide.with(getContext())
                                                .load(getString(R.string.s3_image_url_download) +
                                                        getString(R.string.s3_bucket_events_path) +
                                                        videoFile.get(0))
                                                .apply(requestOptions)
                                                .thumbnail(Glide.with(getContext()).load(getString(R.string.s3_image_url_download) +
                                                        getString(R.string.s3_bucket_events_path) +
                                                        videoFile.get(0)))
                                                .into(videoplayerTime.thumbImageView);
                                    }
                                } else {
                                    videoplayerDetail.setVisibility(View.GONE);
                                    youtubePlayerViewDetail.setVisibility(View.INVISIBLE);
                                    playYouTubeVideo(viewEventResponse.getResults().getMediafiles().get(0), youtubePlayerViewDetail);
                                }


                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            EventDetailsActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);


                            } else if (imageFiles.size() == 0 && videoFile.size() > 0) {
                                imageViewPager.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                youtubePlayerViewDetail.setVisibility(View.GONE);
                                if (viewEventResponse.getResults().getMedia_type() != 1) {
                                    videoplayerTime.setVisibility(View.VISIBLE);
                                    youtubePlayerView.setVisibility(View.INVISIBLE);
                                    videoplayerTime.setUp(proxy.getProxyUrl(getString(R.string.s3_image_url_download) +
                                                    getString(R.string.s3_bucket_events_path) +
                                                    videoFile.get(0))
                                            , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                                    videoplayerTime.setSystemTimeAndBattery();
                                    //thumbnail generation
                                    if (isAudioFile(getString(R.string.s3_image_url_download) +
                                            getString(R.string.s3_bucket_events_path) +
                                            videoFile.get(0))) {
                                        Glide.with(EventDetailsActivity.this)
                                                .load(R.mipmap.icon_audio_thumbnail).into(videoplayerTime.thumbImageView);
                                    } else {
                                        RequestOptions requestOptions = new RequestOptions();
                                        requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                                        requestOptions.error(R.mipmap.icon_video_thumbnail);
                                        Glide.with(getContext())
                                                .load(getString(R.string.s3_image_url_download) +
                                                        getString(R.string.s3_bucket_events_path) +
                                                        videoFile.get(0))
                                                .apply(requestOptions)
                                                .thumbnail(Glide.with(getContext()).load(getString(R.string.s3_image_url_download) +
                                                        getString(R.string.s3_bucket_events_path) +
                                                        videoFile.get(0)))
                                                .into(videoplayerTime.thumbImageView);
                                    }

                                } else {
                                    videoplayerTime.setVisibility(View.INVISIBLE);
                                    youtubePlayerView.setVisibility(View.VISIBLE);
                                    playYouTubeVideo(viewEventResponse.getResults().getMediafiles().get(0), youtubePlayerView);
                                }


                            } else if (imageFiles.size() > 0 && videoFile.size() == 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                youtubePlayerView.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                youtubePlayerViewDetail.setVisibility(View.GONE);
                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            EventDetailsActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);
                                //
                            } else if (imageFiles.size() == 0 && videoFile.size() == 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                youtubePlayerView.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                youtubePlayerViewDetail.setVisibility(View.GONE);
                                imageFiles.add("");
                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            EventDetailsActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);
                            }

                            if (sharedPreferences.getInt(DmkConstants.USERID, 0) ==
                                    viewEventResponse.getResults().getEvent_createdby())
                                imageMenu.setVisibility(View.VISIBLE);
                            else
                                imageMenu.setVisibility(View.GONE);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, viewEventResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, viewEventResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(viewEventResponse.getSource(),
                                    viewEventResponse.getSourcedata()));
                            editor.commit();
                            getEventDetails();
                        }


                    } else {
                        Toast.makeText(EventDetailsActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ViewEventResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(EventDetailsActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(EventDetailsActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteEvent(final int eventid) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            TimeLineDelete timeLineDelete = new TimeLineDelete();
            timeLineDelete.setEventid(eventid);
            timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.deleteEvents(headerMap, timeLineDelete).enqueue(new Callback<AddEventsResponse>() {
                @Override
                public void onResponse(Call<AddEventsResponse> call, Response<AddEventsResponse> response) {
                    hideProgress();
                    addEventsResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (addEventsResponse.getStatuscode() == 0) {
                            onBackPressed();
                            Toast.makeText(EventDetailsActivity.this, getString(R.string.event_del_sucess), Toast.LENGTH_SHORT).show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, addEventsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, addEventsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(addEventsResponse.getSource(),
                                    addEventsResponse.getSourcedata()));
                            editor.commit();
                            deleteEvent(eventid);
                        }
                    } else {
                        Toast.makeText(EventDetailsActivity.this, getString(R.string.event_del_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddEventsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(EventDetailsActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(EventDetailsActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void addShareCount() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            ShareCount shareCount = new ShareCount();
            shareCount.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            shareCount.setMainid(viewEventResponse.getResults().getEvent_id());
            shareCount.setType(2);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.addShareCount(headerMap, shareCount).enqueue(new Callback<ShareCountResponse>() {
                @Override
                public void onResponse(Call<ShareCountResponse> call, Response<ShareCountResponse> response) {
                    hideProgress();
                    shareCountResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (shareCountResponse.getStatuscode() == 0) {
                            viewEventResponse.getResults().setSharecount(
                                    viewEventResponse.getResults().getSharecount() + 1);
                            if (viewEventResponse.getResults().getSharecount() > 99) {
                                textShare.setText("(99+)");
                            } else if (viewEventResponse.getResults().getSharecount() == 0) {
                                textShare.setText("");
                            } else {
                                textShare.setText("(" + viewEventResponse.getResults().getSharecount() + ")");
                            }
                            String ps = "Dmk_encryption " + viewEventResponse.getResults().getEvent_id();
                            String tmp = Base64.encodeBytes(ps.getBytes());
                            tmp = Base64.encodeBytes(tmp.getBytes());
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_TEXT, viewEventResponse.getResults().getEvent_title()
                                    + "\n " + getString(R.string.from) + " " + viewEventResponse.getResults().getEvent_start_date() + " " +
                                    getString(R.string.to) + " " + viewEventResponse.getResults().getEvent_end_date()
                                    + "\n" + getString(R.string.for_more) + getString(R.string.deeplink_url) + "events/" + tmp);
                            startActivity(Intent.createChooser(i, "Choose share option"));
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, shareCountResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, shareCountResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(shareCountResponse.getSource(),
                                    shareCountResponse.getSourcedata()));
                            editor.commit();
                            addShareCount();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ShareCountResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(EventDetailsActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(EventDetailsActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }
}
