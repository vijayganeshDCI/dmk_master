package com.dci.dmkitwings.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v7.widget.LinearLayoutCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.dci.dmkitwings.BuildConfig;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.RegistrationActivity;
import com.dci.dmkitwings.adapter.BoothAdapter;
import com.dci.dmkitwings.adapter.ConstituencyAdapter;
import com.dci.dmkitwings.adapter.DistrictAdpater;
import com.dci.dmkitwings.adapter.DivisionAdapter;
import com.dci.dmkitwings.adapter.ITWingsAdapter;
import com.dci.dmkitwings.adapter.MunicipalityAdapter;
import com.dci.dmkitwings.adapter.PanchayatUnionListResponse;
import com.dci.dmkitwings.adapter.PanchayatUnionResultsItem;
import com.dci.dmkitwings.adapter.PanchyatUnionAdapter;
import com.dci.dmkitwings.adapter.PartAdapter;
import com.dci.dmkitwings.adapter.PartyRoleAdapter;
import com.dci.dmkitwings.adapter.TownPanchayatAdapter;
import com.dci.dmkitwings.adapter.VillagePanchayatAdapter;
import com.dci.dmkitwings.adapter.WardAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.BoothListInputParam;
import com.dci.dmkitwings.model.BoothListResponse;
import com.dci.dmkitwings.model.BoothListResultsItem;
import com.dci.dmkitwings.model.ConsituencyList;
import com.dci.dmkitwings.model.ConsituencyListResponse;
import com.dci.dmkitwings.model.ConstituencyListResultsItem;
import com.dci.dmkitwings.model.DistrictList;
import com.dci.dmkitwings.model.DistrictListResponse;
import com.dci.dmkitwings.model.DistrictsItem;
import com.dci.dmkitwings.model.DivisionListResponse;
import com.dci.dmkitwings.model.DivisionListResultsItem;
import com.dci.dmkitwings.model.ItWingsListResponse;
import com.dci.dmkitwings.model.ItWingsListResultsItem;
import com.dci.dmkitwings.model.MunicipalityListResponse;
import com.dci.dmkitwings.model.MunicipalityListResultsItem;
import com.dci.dmkitwings.model.PartList;
import com.dci.dmkitwings.model.PartListResponse;
import com.dci.dmkitwings.model.PartListResultsItem;
import com.dci.dmkitwings.model.PartyRegistraionResponse;
import com.dci.dmkitwings.model.PartyRoleList;
import com.dci.dmkitwings.model.PartyRoleListResponse;
import com.dci.dmkitwings.model.PartyRoleListResultsItem;
import com.dci.dmkitwings.model.Registration;
import com.dci.dmkitwings.model.TownPanchayatListResponse;
import com.dci.dmkitwings.model.TownPanchayatResultsItem;
import com.dci.dmkitwings.model.UserError;
import com.dci.dmkitwings.model.VattamListResponse;
import com.dci.dmkitwings.model.VattamResultsItem;
import com.dci.dmkitwings.model.VattamWardList;
import com.dci.dmkitwings.model.VillagePanchayatListResponse;
import com.dci.dmkitwings.model.VillagePanchayatResultsItem;
import com.dci.dmkitwings.model.WardListResponse;
import com.dci.dmkitwings.model.WardListResultsItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomEditText;
import com.dci.dmkitwings.view.CustomTextView;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import lib.kingja.switchbutton.SwitchMultiButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.dci.dmkitwings.activity.BaseActivity.ERROR_SHOW_TYPE_DIALOG;

//import com.androidnetworking.AndroidNetworking;
//import com.androidnetworking.common.Priority;
//import com.androidnetworking.error.ANError;
//import com.androidnetworking.interfaces.JSONArrayRequestListener;

/**
 * Created by vijayaganesh on 3/22/2018.
 */

public class PartyMemberRegFragment extends BaseFragment
        implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.guideline_profile)
    Guideline guidelineProfile;
    @BindView(R.id.image_profile_pic)
    CircleImageView imageProfilePic;
    @BindView(R.id.image_app_icon)
    ImageView imageAppIcon;
    @BindView(R.id.text_label_firstname)
    TextView textLabelName;
    @BindView(R.id.edit_first_name)
    EditText editFirstName;
    @BindView(R.id.view_bottom_first_name)
    View viewBottom1;
    @BindView(R.id.text_label_last_name)
    TextView textLabelEmail;
    @BindView(R.id.edit_last_name)
    EditText editLastName;
    @BindView(R.id.view_bottom_last_name)
    View viewBottom2;
    @BindView(R.id.text_label_dob)
    TextView textLabelDob;
    @BindView(R.id.edit_dob)
    EditText editDob;
    @BindView(R.id.view_bottom_gender)
    View viewBottom3;
    @BindView(R.id.text_label_district)
    TextView textLabelDistrict;
    @BindView(R.id.spinner_district)
    Spinner spinnerDistrict;
    @BindView(R.id.view_bottom_district)
    View viewBottom4;
    @BindView(R.id.text_label_constituency)
    TextView textLabelCons;
    @BindView(R.id.spinner_constituency)
    Spinner spinnerConstituency;
    @BindView(R.id.view_bottom_cons)
    View viewBottom5;
    @BindView(R.id.text_label_vilage_panchayat)
    TextView textLabelVillagePanchayat;
    @BindView(R.id.spinner_village_panchayat)
    Spinner spinnerVillagePanchayat;
    @BindView(R.id.view_bottom_village_panchayat)
    View viewBottom6;
    @BindView(R.id.text_label_ward)
    TextView textLabelWard;
    @BindView(R.id.spinner_ward)
    Spinner spinnerWard;
    @BindView(R.id.view_bottom_ward)
    View viewBottom7;
    @BindView(R.id.text_label_wings)
    TextView textLabelWings;
    @BindView(R.id.spinner_wings)
    Spinner spinnerWings;
    @BindView(R.id.view_bottom_wings)
    View viewBottom8;
    @BindView(R.id.button_submit)
    Button buttonSubmit;
    @BindView(R.id.cons_party_member)
    ConstraintLayout consPartyMember;
    Unbinder unbinder;
    @BindView(R.id.text_label_division)
    TextView textLabelType;
    @BindView(R.id.spinner_division)
    Spinner spinnerDivision;
    @BindView(R.id.view_bottom_division)
    View view_bottom_division;
    @BindView(R.id.text_label_part)
    TextView textLabelPart;
    @BindView(R.id.spinner_part)
    Spinner spinnerPart;
    @BindView(R.id.view_bottom_part)
    View viewBottom10;
    @BindView(R.id.text_label_party_role)
    TextView textLabelPartyRole;
    @BindView(R.id.spinner_party_role)
    Spinner spinnerPartyRole;
    @BindView(R.id.view_bottom_party_role)
    View viewBottom12;
    @BindView(R.id.switch_gender)
    SwitchMultiButton switchGender;
    @BindView(R.id.text_label_gender_error)
    TextView textlabelGenderError;
    @BindView(R.id.edit_email)
    EditText editEmailid;
    @BindView(R.id.text_label_voter_id)
    CustomTextView textLabelVoterId;
    @BindView(R.id.edit_voter_id)
    CustomEditText editVoterId;
    @BindView(R.id.view_bottom_voter_id)
    View viewBottom20;
    @BindView(R.id.text_label_address)
    CustomTextView textLabelAddress;
    @BindView(R.id.edit_address)
    CustomEditText editAddress;
    @BindView(R.id.view_bottom_address)
    View viewBottom21;
    @BindView(R.id.text_label_booth)
    CustomTextView textLabelBooth;
    @BindView(R.id.spinner_booth)
    Spinner spinnerBooth;
    @BindView(R.id.view_bottom_booth)
    View viewBottom23;
    @BindView(R.id.linear_firstname)
    LinearLayout linearFirstname;
    @BindView(R.id.linear_lastname)
    LinearLayout linearLastname;
    @BindView(R.id.linear_email)
    LinearLayout linearEmail;
    @BindView(R.id.view_bottom_email)
    View viewBottomEmail;
    @BindView(R.id.view_bottom_dob)
    View viewBottomDob;
    @BindView(R.id.text_label_gender)
    CustomTextView textLabelGender;
    @BindView(R.id.text_label_union_type)
    CustomTextView textLabelUnionType;
    @BindView(R.id.spinner_union_type)
    Spinner spinnerUnionType;
    @BindView(R.id.view_bottom_union_type)
    View viewBottomUnionType;
    @BindView(R.id.scroll_party_mem_reg)
    ScrollView scrollPartyMemReg;
    private Calendar currentD;
    private Calendar calendardate;
    private SimpleDateFormat simpleDateFormat;
    private String dob;
    private static final int MY_REQUEST_CODE_CAMERA = 5;
    private static final int MY_REQUEST_CODE_GALLERY = 6;
    private int REQUEST_CAMERA = 1;
    private int SELECT_FILE = 2;
    private String profilePicture;
    private Uri uri;
    RegistrationActivity profileActivity;
    UserError userError;
    private int age;
    Uri outputUri;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    ItWingsListResponse itWingsListResponse;
    ConsituencyListResponse consituencyListResponse;
    DivisionListResponse divisionListResponse;
    DistrictListResponse districtListResponse;
    PartListResponse partListResponse;
    PanchayatUnionListResponse panchayatUnionListResponse;
    TownPanchayatListResponse townPanchayatListResponse;
    VattamListResponse vattamListResponse;
    WardListResponse wardListResponse;
    VillagePanchayatListResponse villagePanchayatListResponse;
    MunicipalityListResponse municipalityListResponse;
    PartyRegistraionResponse registrationListResponse;
    PartyRoleListResponse partyRoleListResponse;
    ArrayList<String> itWingsList;
    ArrayList<ConstituencyListResultsItem> constituencyList;
    ArrayList<DivisionListResultsItem> divisionlist;
    ArrayList<DistrictsItem> districtList;
    ArrayList<PartListResultsItem> partList;
    ArrayList<PanchayatUnionResultsItem> panchayatList;
    ArrayList<TownPanchayatResultsItem> townPanchayatList;
    ArrayList<VattamResultsItem> vattamList;
    ArrayList<VillagePanchayatResultsItem> villagePanchayatResultsItemArrayList;
    ArrayList<String> partyRoleList;
    ArrayList<WardListResultsItem> wardList;
    ArrayList<MunicipalityListResultsItem> municipalityList;
    ArrayList<PartyRoleListResultsItem> partyRoleListResultsItems;
    ArrayList<ItWingsListResultsItem> itWingsListResultsItems;
    ArrayList<String> unionTypeList;
    private int districtID = -1;
    private int divisionID = -1;
    private int constituencyID, unionType;
    private int partID;
    private String divisionName;
    private int itWingID;
    Field popup;
    TelephonyManager telephonyManager;
    private int gender = 2;
    private MenuItem register;
    private Uri galleryUri;
    private String selectedImage;
    BoothListResponse boothListResponse;
    ArrayList<BoothListResultsItem> boothList;
    private int villageID = 0;
    private int wardID;
    private int boothID;
    private SharedPreferences fcmSharedPrefrences;
    Bitmap bundleImageBitmap;
    private PickSetup setup;

    @SuppressLint("MissingPermission")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_party_member, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        setHasOptionsMenu(true);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(DmkConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        editor.putString(DmkConstants.IMEIID, telephonyManager.getDeviceId()).commit();
        editFirstName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSpecialCharacters()});
        editLastName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSpecialCharacters()});
        editVoterId.setFilters(new InputFilter[]{editTextRestrictLength()});
//        editAddress.setFilters(new InputFilter[]{editTextRestrictSmileys()});
//        editEmailid.setFilters(new InputFilter[]{editTextRestrictSmileys()});
        simpleDateFormat = new SimpleDateFormat();
        profileActivity = (RegistrationActivity) getActivity();
        buttonSubmit.setVisibility(View.VISIBLE);
        userError = new UserError();
        editor.putString(DmkConstants.DEVICEID, Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID)).commit();
        editor.putString(DmkConstants.APPID, getActivity().getPackageName()).commit();
        editor.putString(DmkConstants.APPVERSION_CODE, BuildConfig.VERSION_NAME).commit();
        try {
            popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);
            ListPopupWindow popupWindow = (ListPopupWindow) popup.get(spinnerDistrict);
            popupWindow.setHeight(500);
            ListPopupWindow popupWindow1 = (ListPopupWindow) popup.get(spinnerConstituency);
            popupWindow1.setHeight(500);
            ListPopupWindow popupWindow2 = (ListPopupWindow) popup.get(spinnerDivision);
            popupWindow2.setHeight(500);
            ListPopupWindow popupWindow3 = (ListPopupWindow) popup.get(spinnerPart);
            popupWindow3.setHeight(500);
            ListPopupWindow popupWindow4 = (ListPopupWindow) popup.get(spinnerWings);
            popupWindow4.setHeight(500);
            ListPopupWindow popupWindow5 = (ListPopupWindow) popup.get(spinnerPartyRole);
            popupWindow5.setHeight(500);
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }

        itWingsList = new ArrayList<String>();
        partyRoleList = new ArrayList<String>();
        constituencyList = new ArrayList<ConstituencyListResultsItem>();
        divisionlist = new ArrayList<DivisionListResultsItem>();
        districtList = new ArrayList<DistrictsItem>();
        partList = new ArrayList<PartListResultsItem>();
        wardList = new ArrayList<WardListResultsItem>();
        vattamList = new ArrayList<VattamResultsItem>();
        panchayatList = new ArrayList<PanchayatUnionResultsItem>();
        townPanchayatList = new ArrayList<TownPanchayatResultsItem>();
        municipalityList = new ArrayList<MunicipalityListResultsItem>();
        villagePanchayatResultsItemArrayList = new ArrayList<VillagePanchayatResultsItem>();
        partyRoleListResultsItems = new ArrayList<PartyRoleListResultsItem>();
        itWingsListResultsItems = new ArrayList<ItWingsListResultsItem>();
        boothList = new ArrayList<BoothListResultsItem>();
        unionTypeList = new ArrayList<String>();
        partyRoleListResultsItems.add(0, new PartyRoleListResultsItem(0, getString(R.string.select_Party_role)));
        spinnerPartyRole.setVisibility(View.GONE);
        textLabelPartyRole.setVisibility(View.GONE);
        viewBottom12.setVisibility(View.GONE);
        ArrayAdapter<String> adapterUnionType = new
                ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().getStringArray(R.array.panchayat_union_list));
        spinnerUnionType.setAdapter(adapterUnionType);

        switchGender.setOnSwitchListener

                (new SwitchMultiButton.OnSwitchListener() {
                    @Override
                    public void onSwitch(int position, String tabText) {
                        textlabelGenderError.setError(null);
                        gender = position;
                    }
                });

        if (Util.isNetworkAvailable()) {

            getDistrictList();
            //getDivisionList();
            //getItWingsList();
        } else {

            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

        textLabelType.setVisibility(View.GONE);
        spinnerDivision.setVisibility(View.GONE);
        view_bottom_division.setVisibility(View.GONE);
        textLabelWings.setVisibility(View.GONE);
        spinnerWings.setVisibility(View.GONE);
        viewBottom12.setVisibility(View.GONE);
        viewBottom8.setVisibility(View.GONE);
        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                DistrictsItem districtsItem = districtList.get(position);
                if (districtsItem.getId() != 0) {
                    if (districtID != districtsItem.getId()) {
                        showType(false, "");
                        spinnerVillagePanchayat.setVisibility(View.GONE);
                        textLabelVillagePanchayat.setVisibility(View.GONE);
                        viewBottom6.setVisibility(View.GONE);
                        spinnerWard.setVisibility(View.GONE);
                        textLabelWard.setVisibility(View.GONE);
                        viewBottom7.setVisibility(View.GONE);
                    }
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    districtID = districtsItem.getId();
                    spinnerConstituency.setVisibility(View.GONE);
                    textLabelCons.setVisibility(View.GONE);
                    viewBottom5.setVisibility(View.GONE);
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottom7.setVisibility(View.GONE);
                    spinnerBooth.setVisibility(View.GONE);
                    textLabelBooth.setVisibility(View.GONE);
                    viewBottom23.setVisibility(View.GONE);
                    spinnerWings.setVisibility(View.GONE);
                    textLabelWings.setVisibility(View.GONE);
                    viewBottom8.setVisibility(View.GONE);
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);
                    textLabelPart.setVisibility(View.GONE);
                    spinnerPart.setVisibility(View.GONE);
                    viewBottom10.setVisibility(View.GONE);
                    textLabelType.setVisibility(View.GONE);
                    spinnerDivision.setVisibility(View.GONE);
                    view_bottom_division.setVisibility(View.GONE);
                    spinnerUnionType.setVisibility(View.GONE);
                    textLabelUnionType.setVisibility(View.GONE);
                    viewBottomUnionType.setVisibility(View.GONE);
                    spinnerVillagePanchayat.setVisibility(View.GONE);
                    textLabelVillagePanchayat.setVisibility(View.GONE);
                    viewBottom6.setVisibility(View.GONE);
                    getConsituencyList();

                } else {
                    spinnerConstituency.setVisibility(View.GONE);
                    textLabelCons.setVisibility(View.GONE);
                    viewBottom5.setVisibility(View.GONE);
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottom7.setVisibility(View.GONE);
                    spinnerBooth.setVisibility(View.GONE);
                    textLabelBooth.setVisibility(View.GONE);
                    viewBottom23.setVisibility(View.GONE);
                    spinnerWings.setVisibility(View.GONE);
                    textLabelWings.setVisibility(View.GONE);
                    viewBottom8.setVisibility(View.GONE);
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);
                    textLabelPart.setVisibility(View.GONE);
                    spinnerPart.setVisibility(View.GONE);
                    viewBottom10.setVisibility(View.GONE);
                    textLabelType.setVisibility(View.GONE);
                    spinnerDivision.setVisibility(View.GONE);
                    view_bottom_division.setVisibility(View.GONE);
                    spinnerUnionType.setVisibility(View.GONE);
                    textLabelUnionType.setVisibility(View.GONE);
                    viewBottomUnionType.setVisibility(View.GONE);
                    spinnerVillagePanchayat.setVisibility(View.GONE);
                    textLabelVillagePanchayat.setVisibility(View.GONE);
                    viewBottom6.setVisibility(View.GONE);
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerConstituency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                ConstituencyListResultsItem constituencyListResultsItem = constituencyList.get(position);
                if (constituencyListResultsItem.getId() != 0) {
                    constituencyID = constituencyListResultsItem.getId();
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottom7.setVisibility(View.GONE);
                    spinnerBooth.setVisibility(View.GONE);
                    textLabelBooth.setVisibility(View.GONE);
                    viewBottom23.setVisibility(View.GONE);
                    spinnerWings.setVisibility(View.GONE);
                    textLabelWings.setVisibility(View.GONE);
                    viewBottom8.setVisibility(View.GONE);
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);
                    textLabelPart.setVisibility(View.GONE);
                    spinnerPart.setVisibility(View.GONE);
                    viewBottom10.setVisibility(View.GONE);
                    textLabelType.setVisibility(View.GONE);
                    spinnerDivision.setVisibility(View.GONE);
                    view_bottom_division.setVisibility(View.GONE);
                    spinnerUnionType.setVisibility(View.GONE);
                    textLabelUnionType.setVisibility(View.GONE);
                    viewBottomUnionType.setVisibility(View.GONE);
                    spinnerVillagePanchayat.setVisibility(View.GONE);
                    textLabelVillagePanchayat.setVisibility(View.GONE);
                    viewBottom6.setVisibility(View.GONE);
                    getDivisionList();
                } else {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottom7.setVisibility(View.GONE);
                    spinnerBooth.setVisibility(View.GONE);
                    textLabelBooth.setVisibility(View.GONE);
                    viewBottom23.setVisibility(View.GONE);
                    spinnerWings.setVisibility(View.GONE);
                    textLabelWings.setVisibility(View.GONE);
                    viewBottom8.setVisibility(View.GONE);
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);
                    textLabelPart.setVisibility(View.GONE);
                    spinnerPart.setVisibility(View.GONE);
                    viewBottom10.setVisibility(View.GONE);
                    textLabelType.setVisibility(View.GONE);
                    spinnerDivision.setVisibility(View.GONE);
                    view_bottom_division.setVisibility(View.GONE);
                    spinnerUnionType.setVisibility(View.GONE);
                    textLabelUnionType.setVisibility(View.GONE);
                    viewBottomUnionType.setVisibility(View.GONE);
                    spinnerVillagePanchayat.setVisibility(View.GONE);
                    textLabelVillagePanchayat.setVisibility(View.GONE);
                    viewBottom6.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                DivisionListResultsItem divisionListResultsItem = divisionlist.get(position);
                switch (divisionListResultsItem.getId()) {
                    case 0:
                        showType(false, "");
                        ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                        spinnerWard.setVisibility(View.GONE);
                        textLabelWard.setVisibility(View.GONE);
                        viewBottom7.setVisibility(View.GONE);
                        spinnerBooth.setVisibility(View.GONE);
                        textLabelBooth.setVisibility(View.GONE);
                        viewBottom23.setVisibility(View.GONE);
                        spinnerWings.setVisibility(View.GONE);
                        textLabelWings.setVisibility(View.GONE);
                        viewBottom8.setVisibility(View.GONE);
                        spinnerPartyRole.setVisibility(View.GONE);
                        textLabelPartyRole.setVisibility(View.GONE);
                        viewBottom12.setVisibility(View.GONE);
                        textLabelPart.setVisibility(View.GONE);
                        spinnerPart.setVisibility(View.GONE);
                        viewBottom10.setVisibility(View.GONE);
                        spinnerUnionType.setVisibility(View.GONE);
                        textLabelUnionType.setVisibility(View.GONE);
                        viewBottomUnionType.setVisibility(View.GONE);
                        spinnerVillagePanchayat.setVisibility(View.GONE);
                        textLabelVillagePanchayat.setVisibility(View.GONE);
                        viewBottom6.setVisibility(View.GONE);

                        break;
                    case 1:
//                        Corporation
                        spinnerUnionType.setVisibility(View.GONE);
                        textLabelUnionType.setVisibility(View.GONE);
                        viewBottomUnionType.setVisibility(View.GONE);
                        unionType = 0;
                        divisionID = divisionListResultsItem.getId();
                        divisionName = divisionListResultsItem.getDivisionname();

                        if (constituencyID != 0 && districtID != 0 && divisionID != 0) {
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                            getPartList();
                        } else {

                            Toast.makeText(getActivity(), getString(R.string.choose_district), Toast.LENGTH_SHORT).show();

                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom7.setVisibility(View.GONE);
                            spinnerBooth.setVisibility(View.GONE);
                            textLabelBooth.setVisibility(View.GONE);
                            viewBottom23.setVisibility(View.GONE);
                            spinnerWings.setVisibility(View.GONE);
                            textLabelWings.setVisibility(View.GONE);
                            viewBottom8.setVisibility(View.GONE);
                            spinnerPartyRole.setVisibility(View.GONE);
                            textLabelPartyRole.setVisibility(View.GONE);
                            viewBottom12.setVisibility(View.GONE);
                            textLabelPart.setVisibility(View.GONE);
                            spinnerPart.setVisibility(View.GONE);
                            viewBottom10.setVisibility(View.GONE);
                            spinnerUnionType.setVisibility(View.GONE);
                            textLabelUnionType.setVisibility(View.GONE);
                            viewBottomUnionType.setVisibility(View.GONE);
                            spinnerVillagePanchayat.setVisibility(View.GONE);
                            textLabelVillagePanchayat.setVisibility(View.GONE);
                            viewBottom6.setVisibility(View.GONE);

                        }

                        break;
                    case 2:
//                        Municipality
                        divisionID = divisionListResultsItem.getId();
                        divisionName = divisionListResultsItem.getDivisionname();
                        unionType = 0;
                        spinnerUnionType.setVisibility(View.GONE);
                        textLabelUnionType.setVisibility(View.GONE);
                        viewBottomUnionType.setVisibility(View.GONE);
                        if (constituencyID != 0 && districtID != 0 && divisionID != 0) {
                            getMunicipalityList();
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        } else {

                            Toast.makeText(getActivity(), getString(R.string.choose_district), Toast.LENGTH_SHORT).show();
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom7.setVisibility(View.GONE);
                            spinnerBooth.setVisibility(View.GONE);
                            textLabelBooth.setVisibility(View.GONE);
                            viewBottom23.setVisibility(View.GONE);
                            spinnerWings.setVisibility(View.GONE);
                            textLabelWings.setVisibility(View.GONE);
                            viewBottom8.setVisibility(View.GONE);
                            spinnerPartyRole.setVisibility(View.GONE);
                            textLabelPartyRole.setVisibility(View.GONE);
                            viewBottom12.setVisibility(View.GONE);
                            textLabelPart.setVisibility(View.GONE);
                            spinnerPart.setVisibility(View.GONE);
                            viewBottom10.setVisibility(View.GONE);
                            spinnerUnionType.setVisibility(View.GONE);
                            textLabelUnionType.setVisibility(View.GONE);
                            viewBottomUnionType.setVisibility(View.GONE);
                            spinnerVillagePanchayat.setVisibility(View.GONE);
                            textLabelVillagePanchayat.setVisibility(View.GONE);
                            viewBottom6.setVisibility(View.GONE);
                        }

                        break;
                    case 3:
//                        PanchayatUnion
                        spinnerUnionType.setVisibility(View.VISIBLE);
                        spinnerUnionType.setSelection(0);
                        textLabelUnionType.setVisibility(View.VISIBLE);
                        viewBottomUnionType.setVisibility(View.VISIBLE);
                        divisionID = divisionListResultsItem.getId();
                        spinnerWard.setVisibility(View.GONE);
                        textLabelWard.setVisibility(View.GONE);
                        viewBottom7.setVisibility(View.GONE);
                        spinnerBooth.setVisibility(View.GONE);
                        textLabelBooth.setVisibility(View.GONE);
                        viewBottom23.setVisibility(View.GONE);
                        spinnerWings.setVisibility(View.GONE);
                        textLabelWings.setVisibility(View.GONE);
                        viewBottom8.setVisibility(View.GONE);
                        spinnerPartyRole.setVisibility(View.GONE);
                        textLabelPartyRole.setVisibility(View.GONE);
                        viewBottom12.setVisibility(View.GONE);
                        textLabelPart.setVisibility(View.GONE);
                        spinnerPart.setVisibility(View.GONE);
                        viewBottom10.setVisibility(View.GONE);
                        spinnerVillagePanchayat.setVisibility(View.GONE);
                        textLabelVillagePanchayat.setVisibility(View.GONE);
                        viewBottom6.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerUnionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int selectedTypePos = parent.getSelectedItemPosition();
                if (selectedTypePos != 1) {
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottom7.setVisibility(View.GONE);
                    spinnerVillagePanchayat.setVisibility(View.GONE);
                    textLabelVillagePanchayat.setVisibility(View.GONE);
                    viewBottom6.setVisibility(View.GONE);
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottom7.setVisibility(View.GONE);
                    spinnerBooth.setVisibility(View.GONE);
                    textLabelBooth.setVisibility(View.GONE);
                    viewBottom23.setVisibility(View.GONE);
                    spinnerWings.setVisibility(View.GONE);
                    textLabelWings.setVisibility(View.GONE);
                    viewBottom8.setVisibility(View.GONE);
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);
                    textLabelPart.setVisibility(View.GONE);
                    spinnerPart.setVisibility(View.GONE);
                    viewBottom10.setVisibility(View.GONE);
                }

                if (selectedTypePos != 0) {
                    if (constituencyID != 0 && districtID != 0 && divisionID != 0) {
                        //                1-  //TownPanchayat 2-//VillagePanchayat
                        unionType = selectedTypePos;
                        spinnerVillagePanchayat.setVisibility(View.GONE);
                        textLabelVillagePanchayat.setVisibility(View.GONE);
                        viewBottom6.setVisibility(View.GONE);
                        spinnerWard.setVisibility(View.GONE);
                        textLabelWard.setVisibility(View.GONE);
                        viewBottom7.setVisibility(View.GONE);
                        spinnerBooth.setVisibility(View.GONE);
                        textLabelBooth.setVisibility(View.GONE);
                        viewBottom23.setVisibility(View.GONE);
                        spinnerWings.setVisibility(View.GONE);
                        textLabelWings.setVisibility(View.GONE);
                        viewBottom8.setVisibility(View.GONE);
                        spinnerPartyRole.setVisibility(View.GONE);
                        textLabelPartyRole.setVisibility(View.GONE);
                        viewBottom12.setVisibility(View.GONE);
                        textLabelPart.setVisibility(View.GONE);
                        spinnerPart.setVisibility(View.GONE);
                        viewBottom10.setVisibility(View.GONE);
                        getPanchayatUnionList();
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.choose_district), Toast.LENGTH_SHORT).show();
                        //spinnerDivision.setSelection(0);
                        spinnerVillagePanchayat.setVisibility(View.GONE);
                        textLabelVillagePanchayat.setVisibility(View.GONE);
                        viewBottom6.setVisibility(View.GONE);
                        spinnerWard.setVisibility(View.GONE);
                        textLabelWard.setVisibility(View.GONE);
                        viewBottom7.setVisibility(View.GONE);
                        spinnerBooth.setVisibility(View.GONE);
                        textLabelBooth.setVisibility(View.GONE);
                        viewBottom23.setVisibility(View.GONE);
                        spinnerWings.setVisibility(View.GONE);
                        textLabelWings.setVisibility(View.GONE);
                        viewBottom8.setVisibility(View.GONE);
                        spinnerPartyRole.setVisibility(View.GONE);
                        textLabelPartyRole.setVisibility(View.GONE);
                        viewBottom12.setVisibility(View.GONE);
                        textLabelPart.setVisibility(View.GONE);
                        spinnerPart.setVisibility(View.GONE);
                        viewBottom10.setVisibility(View.GONE);
                    }
                } else {
                    spinnerVillagePanchayat.setVisibility(View.GONE);
                    textLabelVillagePanchayat.setVisibility(View.GONE);
                    viewBottom6.setVisibility(View.GONE);
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottom7.setVisibility(View.GONE);
                    spinnerBooth.setVisibility(View.GONE);
                    textLabelBooth.setVisibility(View.GONE);
                    viewBottom23.setVisibility(View.GONE);
                    spinnerWings.setVisibility(View.GONE);
                    textLabelWings.setVisibility(View.GONE);
                    viewBottom8.setVisibility(View.GONE);
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);
                    textLabelPart.setVisibility(View.GONE);
                    spinnerPart.setVisibility(View.GONE);
                    viewBottom10.setVisibility(View.GONE);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerPart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                switch (divisionID) {
                    case 1:
//                        Corporation
                        PartListResultsItem partListResultsItem = partList.get(position);
                        if (partListResultsItem.getId() != 0) {
                            partID = partListResultsItem.getId();
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom7.setVisibility(View.GONE);
                            spinnerBooth.setVisibility(View.GONE);
                            textLabelBooth.setVisibility(View.GONE);
                            viewBottom23.setVisibility(View.GONE);
                            spinnerWings.setVisibility(View.GONE);
                            textLabelWings.setVisibility(View.GONE);
                            viewBottom8.setVisibility(View.GONE);
                            spinnerPartyRole.setVisibility(View.GONE);
                            textLabelPartyRole.setVisibility(View.GONE);
                            viewBottom12.setVisibility(View.GONE);

                            getWardList();
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        } else {
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom7.setVisibility(View.GONE);
                            spinnerBooth.setVisibility(View.GONE);
                            textLabelBooth.setVisibility(View.GONE);
                            viewBottom23.setVisibility(View.GONE);
                            spinnerWings.setVisibility(View.GONE);
                            textLabelWings.setVisibility(View.GONE);
                            viewBottom8.setVisibility(View.GONE);
                            spinnerPartyRole.setVisibility(View.GONE);
                            textLabelPartyRole.setVisibility(View.GONE);
                            viewBottom12.setVisibility(View.GONE);

                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                        }
                        break;
                    case 2:
//                        Municipality
                        MunicipalityListResultsItem municipalityListResultsItem = municipalityList.get(position);
                        if (municipalityListResultsItem.getId() != 0) {
                            partID = municipalityListResultsItem.getId();
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom7.setVisibility(View.GONE);
                            spinnerBooth.setVisibility(View.GONE);
                            textLabelBooth.setVisibility(View.GONE);
                            viewBottom23.setVisibility(View.GONE);
                            spinnerWings.setVisibility(View.GONE);
                            textLabelWings.setVisibility(View.GONE);
                            viewBottom8.setVisibility(View.GONE);
                            spinnerPartyRole.setVisibility(View.GONE);
                            textLabelPartyRole.setVisibility(View.GONE);
                            viewBottom12.setVisibility(View.GONE);
                            getWardList();
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        } else {
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom7.setVisibility(View.GONE);
                            spinnerBooth.setVisibility(View.GONE);
                            textLabelBooth.setVisibility(View.GONE);
                            viewBottom23.setVisibility(View.GONE);
                            spinnerWings.setVisibility(View.GONE);
                            textLabelWings.setVisibility(View.GONE);
                            viewBottom8.setVisibility(View.GONE);
                            spinnerPartyRole.setVisibility(View.GONE);
                            textLabelPartyRole.setVisibility(View.GONE);
                            viewBottom12.setVisibility(View.GONE);

                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                        }
                        break;
                    case 3:
                        //PanchayatUnion
                        if (unionType == 1) {
                            //TownPanchayat
                            textLabelVillagePanchayat.setText(R.string.town_pachayat);
                            PanchayatUnionResultsItem panchayatUnionResultsItem = panchayatList.get(position);
                            if (panchayatUnionResultsItem.getId() != 0) {
                                partID = panchayatUnionResultsItem.getId();
                                spinnerVillagePanchayat.setVisibility(View.GONE);
                                textLabelVillagePanchayat.setVisibility(View.GONE);
                                viewBottom6.setVisibility(View.GONE);
                                spinnerBooth.setVisibility(View.GONE);
                                textLabelBooth.setVisibility(View.GONE);
                                viewBottom23.setVisibility(View.GONE);
                                spinnerWings.setVisibility(View.GONE);
                                textLabelWings.setVisibility(View.GONE);
                                viewBottom8.setVisibility(View.GONE);
                                spinnerPartyRole.setVisibility(View.GONE);
                                textLabelPartyRole.setVisibility(View.GONE);
                                viewBottom12.setVisibility(View.GONE);
                                getTownPanchayatList();
                                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                            } else {
                                spinnerVillagePanchayat.setVisibility(View.GONE);
                                textLabelVillagePanchayat.setVisibility(View.GONE);
                                viewBottom6.setVisibility(View.GONE);
                                spinnerBooth.setVisibility(View.GONE);
                                textLabelBooth.setVisibility(View.GONE);
                                viewBottom23.setVisibility(View.GONE);
                                spinnerWings.setVisibility(View.GONE);
                                textLabelWings.setVisibility(View.GONE);
                                viewBottom8.setVisibility(View.GONE);
                                spinnerPartyRole.setVisibility(View.GONE);
                                textLabelPartyRole.setVisibility(View.GONE);
                                viewBottom12.setVisibility(View.GONE);

                                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                            }
                        } else {
                            //VillagePanchayat
                            textLabelVillagePanchayat.setText(R.string.village_panchayat);
                            PanchayatUnionResultsItem panchayatUnionResultsItem = panchayatList.get(position);
                            if (panchayatUnionResultsItem.getId() != 0) {
                                partID = panchayatUnionResultsItem.getId();
                                spinnerVillagePanchayat.setVisibility(View.GONE);
                                textLabelVillagePanchayat.setVisibility(View.GONE);
                                viewBottom6.setVisibility(View.GONE);
                                spinnerBooth.setVisibility(View.GONE);
                                textLabelBooth.setVisibility(View.GONE);
                                viewBottom23.setVisibility(View.GONE);
                                spinnerWings.setVisibility(View.GONE);
                                textLabelWings.setVisibility(View.GONE);
                                viewBottom8.setVisibility(View.GONE);
                                spinnerPartyRole.setVisibility(View.GONE);
                                textLabelPartyRole.setVisibility(View.GONE);
                                viewBottom12.setVisibility(View.GONE);
                                getVillagePanchayatList();
                                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                            } else {
                                spinnerVillagePanchayat.setVisibility(View.GONE);
                                textLabelVillagePanchayat.setVisibility(View.GONE);
                                viewBottom6.setVisibility(View.GONE);
                                spinnerBooth.setVisibility(View.GONE);
                                textLabelBooth.setVisibility(View.GONE);
                                viewBottom23.setVisibility(View.GONE);
                                spinnerWings.setVisibility(View.GONE);
                                textLabelWings.setVisibility(View.GONE);
                                viewBottom8.setVisibility(View.GONE);
                                spinnerPartyRole.setVisibility(View.GONE);
                                textLabelPartyRole.setVisibility(View.GONE);
                                viewBottom12.setVisibility(View.GONE);

                                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                            }
                        }


                        break;

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerVillagePanchayat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                switch (unionType) {
                    case 1:
                        //TownPanchayat
                        TownPanchayatResultsItem townPanchayatResultsItem = townPanchayatList.get(position);
                        if (townPanchayatResultsItem.getId() != 0) {
                            getWardList();
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                        } else {
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom7.setVisibility(View.GONE);
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));

                        }
                        break;
                    case 2:
                        //Village panchayat
                        VillagePanchayatResultsItem villagePanchayatResultsItem = villagePanchayatResultsItemArrayList.get(position);
                        if (villagePanchayatResultsItem.getId() != 0) {
                            villageID = villagePanchayatResultsItem.getId();

                            getBoothList();
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                        } else {
                            spinnerBooth.setVisibility(View.GONE);
                            textLabelBooth.setVisibility(View.GONE);
                            viewBottom23.setVisibility(View.GONE);
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));

                        }
                        break;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerWard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                WardListResultsItem wardListResultsItem = wardList.get(position);
                if (wardListResultsItem.getId() != 0) {
                    wardID = wardListResultsItem.getId();
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    spinnerBooth.setVisibility(View.GONE);
                    textLabelBooth.setVisibility(View.GONE);
                    viewBottom23.setVisibility(View.GONE);
                    spinnerWings.setVisibility(View.GONE);
                    textLabelWings.setVisibility(View.GONE);
                    viewBottom8.setVisibility(View.GONE);
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);

                    getBoothList();
                } else {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                    spinnerBooth.setVisibility(View.GONE);
                    textLabelBooth.setVisibility(View.GONE);
                    viewBottom23.setVisibility(View.GONE);
                    spinnerWings.setVisibility(View.GONE);
                    textLabelWings.setVisibility(View.GONE);
                    viewBottom8.setVisibility(View.GONE);
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerBooth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BoothListResultsItem boothResultsItem = boothList.get(position);
                if (boothResultsItem.getId() != 0) {
                    boothID = boothResultsItem.getId();
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    spinnerWings.setVisibility(View.GONE);
                    textLabelWings.setVisibility(View.GONE);
                    viewBottom8.setVisibility(View.GONE);
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);
                    getItWingsList();
                } else {
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                    spinnerWings.setVisibility(View.GONE);
                    textLabelWings.setVisibility(View.GONE);
                    viewBottom8.setVisibility(View.GONE);
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerWings.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getSelectedItemPosition() != 0) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    itWingID = itWingsListResultsItems.get(spinnerWings.getSelectedItemPosition()).getId();
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);
                    getPartyRoleList();
                } else {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                    spinnerPartyRole.setVisibility(View.GONE);
                    textLabelPartyRole.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerPartyRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getSelectedItemPosition() != 0) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                } else {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        scrollPartyMemReg.post(new Runnable() {
            public void run() {
                scrollPartyMemReg.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });

        setup = new PickSetup()
                .setTitle("")
                .setProgressText(getString(R.string.loading))
                .setCancelText(getString(R.string.Alert_Cancel))
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText(getString(R.string.camera))
                .setGalleryButtonText(getString(R.string.gallery))
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setGalleryIcon(R.mipmap.icon_gallery)
                .setCameraIcon(R.mipmap.icon_photo_camera).setWidth(100).setHeight(100).setFlip(true);


        return view;
    }

    private void showType(boolean visibility, String type) {
        if (visibility) {
            textLabelPart.setVisibility(View.VISIBLE);
            spinnerPart.setVisibility(View.VISIBLE);
            viewBottom10.setVisibility(View.VISIBLE);
            textLabelPart.setText(type);
        } else {
            textLabelPart.setVisibility(View.GONE);
            spinnerPart.setVisibility(View.GONE);
            viewBottom10.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.image_profile_pic, R.id.edit_dob, R.id.button_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_profile_pic:
                pickImage();
                break;
            case R.id.edit_dob:
                editDob.setError(null);
                datePickerDialog();
                break;
            case R.id.button_submit:
                profileNullCheck();
                break;
        }
    }

    private void pickImage() {
        PickImageDialog.build(setup)
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //Image path
                            //r.getPath();

                            //If you want the Bitmap.
                            imageProfilePic.setImageBitmap(r.getBitmap());
                            galleryUri = r.getUri();
                            selectedImage = System.currentTimeMillis() + ".jpg";
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getActivity(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }

                    }
                })
                .setOnPickCancel(new IPickCancel() {
                    @Override
                    public void onCancelClick() {
                    }
                }).show(getActivity().getSupportFragmentManager());
    }


    private void datePickerDialog() {
        currentD = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        Calendar minDate = Calendar.getInstance();
        DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance(this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        maxDate.add(Calendar.YEAR, -18);
        datePickerDialog.setMaxDate(maxDate);
//        minDate.add(Calendar.YEAR, -60);
//        datePickerDialog.setMinDate(minDate);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.red));
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dob = simpleDateFormat.format(calendardate.getTime());
        editDob.setText(dob);
        age = Integer.parseInt(getAge(year, monthOfYear, dayOfMonth));


    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            if (requestCode == SELECT_FILE) {
//                //Crop.of(data.getData(), outputUri).asSquare().start(getActivity());
//                onSelectFromGalleryResult(data);
//
//            } else if (requestCode == REQUEST_CAMERA) {
//                onCaptureImageResult(data);
//            }
//
//        }
//    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_REQUEST_CODE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close icon_like your
                // original question
            }
        } else if (requestCode == MY_REQUEST_CODE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
            } else {

            }
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save, menu);
        register = menu.findItem(R.id.menu_bar_save);
        register.setTitle(R.string.submit);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_save:
                android.support.v7.app.AlertDialog.Builder alertDialog1 = new android.support.v7.app.AlertDialog.Builder(getActivity());
                alertDialog1.setMessage(getString(R.string.confirm_to_register));
                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        profileNullCheck();
                    }
                });
                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        hideProgress();
                    }
                });
                alertDialog1.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


//    private void getImageFiles() {
//        final Dialog dialog = new Dialog(getActivity());
//        // Include dialog.xml file
//        dialog.setContentView(R.layout.media_dialog);
//        // Set dialog title
//        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
//        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
//        textCamera.setText(R.string.camera);
//        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
//        imageCamera.setImageResource(R.mipmap.icon_photo_camera);
//        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
//        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
//        textGallery.setText(R.string.gallery);
//        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
//        imageGallery.setImageResource(R.mipmap.icon_gallery);
//        imageCamera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
//                            != PackageManager.PERMISSION_GRANTED) {
//                        requestPermissions(new String[]{Manifest.permission.CAMERA},
//                                MY_REQUEST_CODE_CAMERA);
//                    } else {
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(intent, REQUEST_CAMERA);
//                    }
//                } else {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(intent, REQUEST_CAMERA);
//                }
//                dialog.dismiss();
//            }
//        });
//        imageGallery.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
//                            != PackageManager.PERMISSION_GRANTED) {
//                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                                MY_REQUEST_CODE_GALLERY);
//                    } else {
//                        Intent intent = new Intent();
//                        intent.setType("image/*");
//                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                        intent.setAction(Intent.ACTION_GET_CONTENT);//
//                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
//
//                    }
//                } else {
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
//
//                }
//                dialog.dismiss();
//            }
//
//        });
//
//        // set values for custom dialog components - text, image and button
//        dialog.show();
//
//
//    }
//
//    private void getProfilePicture() {
//        final CharSequence[] options = {"Choose from Gallery", "Cancel"};
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        //builder.setTitle("Add icon_attach_photo");
//        builder.setItems(options, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                if (options[item].equals("Take Photo")) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
//                                != PackageManager.PERMISSION_GRANTED) {
//                            requestPermissions(new String[]{Manifest.permission.CAMERA},
//                                    MY_REQUEST_CODE_CAMERA);
//                        } else {
//                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            startActivityForResult(intent, REQUEST_CAMERA);
//                        }
//                    } else {
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(intent, REQUEST_CAMERA);
//                    }
//
//                } else if (options[item].equals("Choose from Gallery")) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
//                                != PackageManager.PERMISSION_GRANTED) {
//                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                                    MY_REQUEST_CODE_GALLERY);
//                        } else {
//                            Intent intent = new Intent();
//                            intent.setType("image/*");
//                            intent.setAction(Intent.ACTION_GET_CONTENT);//
//                            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
//                        }
//                    } else {
//                        Intent intent = new Intent();
//                        intent.setType("image/*");
//                        intent.setAction(Intent.ACTION_GET_CONTENT);//
//                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
//                    }
//
//                } else if (options[item].equals("Cancel")) {
//                    dialog.dismiss();
//                }
//            }
//        });
//        builder.show();
//    }
//
//    @SuppressWarnings("deprecation")
//    private void onSelectFromGalleryResult(Intent data) {
//        if (data != null) {
//            galleryUri = data.getData();
//            try {
//                bundleImageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), galleryUri);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            compressInputImage(data, getActivity(), imageProfilePic);
//        }
//        if (galleryUri != null)
//            selectedImage = System.currentTimeMillis() + ".jpg";
//    }
//
//    private void onCaptureImageResult(Intent data) {
//        if (data != null) {
//            Bitmap capturedImageBitmap = (Bitmap) data.getExtras().get("data");
//            bundleImageBitmap = capturedImageBitmap;
//            if (getImageUri(getActivity(), capturedImageBitmap) != null) {
//                galleryUri = getImageUri(getActivity(), capturedImageBitmap);
//            } else {
//                galleryUri = data.getData();
//            }
//            imageProfilePic.setImageBitmap(capturedImageBitmap);
//        }
//        if (galleryUri != null)
//            selectedImage = System.currentTimeMillis() + ".jpg";
//    }

    public void getDistrictList() {
        showProgress();
        districtList.clear();
        districtList.add(0, new DistrictsItem(getString(R.string.select_district), 0));
        DistrictList districtListparam = new DistrictList();
        districtListparam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
        dmkAPI.getDistrictList(headerMap, districtListparam).enqueue(new Callback<DistrictListResponse>() {
            @Override
            public void onResponse(Call<DistrictListResponse> call, Response<DistrictListResponse> response) {
                districtListResponse = response.body();
                hideProgress();
                if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                    if (districtListResponse.getResults() != null) {
                        for (int i = 0; i < districtListResponse.getResults().getDistricts().size(); i++) {
                            districtList.add(new DistrictsItem(
                                    districtListResponse.getResults().getDistricts().get(i).getDistrictName() != null ?
                                            districtListResponse.getResults().getDistricts().get(i).getDistrictName() : "",
                                    districtListResponse.getResults().getDistricts().get(i).getId()));
                        }
                        DistrictAdpater adapterdivisionList = new DistrictAdpater(districtList, getActivity());
                        spinnerDistrict.setAdapter(adapterdivisionList);
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_district), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), districtListResponse.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<DistrictListResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getConsituencyList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            constituencyList.clear();
            constituencyList.add(0, new ConstituencyListResultsItem(getString(R.string.select_constituency), 0));
            ConsituencyList consituencyList = new ConsituencyList();
            consituencyList.setDistrictID(districtID);
            consituencyList.setType(1);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getConstituencyList(headerMap, consituencyList).enqueue(new Callback<ConsituencyListResponse>() {
                @Override
                public void onResponse(Call<ConsituencyListResponse> call, Response<ConsituencyListResponse> response) {
                    hideProgress();
                    consituencyListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (consituencyListResponse.getResults() != null) {
                            if (consituencyListResponse.getResults().size() > 0) {
                                spinnerConstituency.setVisibility(View.VISIBLE);
                                textLabelCons.setVisibility(View.VISIBLE);
                                viewBottom5.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < consituencyListResponse.getResults().size(); i++) {
                                constituencyList.add(new ConstituencyListResultsItem(
                                        consituencyListResponse.getResults().get(i).getConstituencyName() != null ?
                                                consituencyListResponse.getResults().get(i).getConstituencyName() : "",
                                        consituencyListResponse.getResults().get(i).getId()));
                            }
                            ConstituencyAdapter adapterConstituencyList = new ConstituencyAdapter(constituencyList, getContext());
                            spinnerConstituency.setAdapter(adapterConstituencyList);
                        } else {
                            Toast.makeText(getActivity(), consituencyListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            spinnerConstituency.setVisibility(View.GONE);
                            textLabelCons.setVisibility(View.GONE);
                            viewBottom5.setVisibility(View.GONE);
                        }
                    } else {
                        Toast.makeText(getActivity(), consituencyListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        spinnerConstituency.setVisibility(View.GONE);
                        textLabelCons.setVisibility(View.GONE);
                        viewBottom5.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ConsituencyListResponse> call, Throwable t) {
                    hideProgress();
                    spinnerConstituency.setVisibility(View.GONE);
                    textLabelCons.setVisibility(View.GONE);
                    viewBottom5.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }


    }


    public void getDivisionList() {
        showProgress();
        divisionlist.clear();
        divisionlist.add(0, new DivisionListResultsItem(getString(R.string.select_division), 0));
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
        dmkAPI.getDivisionList(headerMap).enqueue(new Callback<DivisionListResponse>() {
            @Override
            public void onResponse(Call<DivisionListResponse> call, Response<DivisionListResponse> response) {
                hideProgress();
                divisionListResponse = response.body();
                if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                    if (divisionListResponse.getResults() != null) {
                        textLabelType.setVisibility(View.VISIBLE);
                        spinnerDivision.setVisibility(View.VISIBLE);
                        view_bottom_division.setVisibility(View.VISIBLE);
                        for (int i = 0; i < divisionListResponse.getResults().size(); i++) {
                            divisionlist.add(new DivisionListResultsItem(
                                    divisionListResponse.getResults().get(i).getDivisionname() != null ?
                                            divisionListResponse.getResults().get(i).getDivisionname() : "",
                                    divisionListResponse.getResults().get(i).getId()));
                        }
                        DivisionAdapter adapterdivisionList = new DivisionAdapter(divisionlist, getActivity());
                        spinnerDivision.setAdapter(adapterdivisionList);
                    } else {
                        Toast.makeText(getActivity(), divisionListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), divisionListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DivisionListResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getPartList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            partList.clear();
            partList.add(0, new PartListResultsItem(getString(R.string.select_part), 0));
            PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setDivisionID(divisionID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPartList(headerMap, partListParam).enqueue(new Callback<PartListResponse>() {
                @Override
                public void onResponse(Call<PartListResponse> call, Response<PartListResponse> response) {
                    hideProgress();
                    partListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (partListResponse.getResults() != null) {
                            if (partListResponse.getResults().size() > 0) {
                                switch (divisionID) {
                                    case 1:
                                        showType(true, getString(R.string.part));
                                        break;
                                    case 2:
                                        showType(true, getString(R.string.municipallity));
                                        break;
                                    case 3:
                                        showType(true, getString(R.string.pachayat_union));
                                        break;

                                }
                            }
                            for (int i = 0; i < partListResponse.getResults().size(); i++) {
                                partList.add(new PartListResultsItem(
                                        partListResponse.getResults().get(i).getPartname() != null ?
                                                partListResponse.getResults().get(i).getPartname() : "",
                                        partListResponse.getResults().get(i).getId()));
                            }
                            PartAdapter partAdapter = new PartAdapter(partList, getActivity());
                            spinnerPart.setAdapter(partAdapter);
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.no_it_part), Toast.LENGTH_SHORT).show();
                            showType(false, "");
                        }

                    } else {
                        Toast.makeText(getActivity(), partListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        showType(false, "");
                    }
                }

                @Override
                public void onFailure(Call<PartListResponse> call, Throwable t) {
                    hideProgress();
                    showType(false, "");
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }


    }

    public void getMunicipalityList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            municipalityList.clear();
            municipalityList.add(0, new MunicipalityListResultsItem(0, getString(R.string.select_town_municipality)));
            PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setDivisionID(divisionID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getMunicipalitytList(headerMap, partListParam).enqueue(new Callback<MunicipalityListResponse>() {
                @Override
                public void onResponse(Call<MunicipalityListResponse> call, Response<MunicipalityListResponse> response) {
                    hideProgress();
                    municipalityListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (municipalityListResponse.getResults() != null) {
                            if (municipalityListResponse.getResults().size() > 0) {
                                showType(true, getString(R.string.municipallity));
                            }
                            for (int i = 0; i < municipalityListResponse.getResults().size(); i++) {
                                municipalityList.add(new MunicipalityListResultsItem(
                                        municipalityListResponse.getResults().get(i).getId(),
                                        municipalityListResponse.getResults().get(i).getMunicipalityname() != null ?
                                                municipalityListResponse.getResults().get(i).getMunicipalityname() : ""));
                            }
                            MunicipalityAdapter partAdapter = new MunicipalityAdapter(municipalityList, getActivity());
                            spinnerPart.setAdapter(partAdapter);
                        } else {
                            showType(false, "");
                            Toast.makeText(getActivity(), getString(R.string.no_it_municipality), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        showType(false, "");
                        Toast.makeText(getActivity(), municipalityListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<MunicipalityListResponse> call, Throwable t) {
                    hideProgress();
                    showType(false, "");
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }


    }


    public void getPanchayatUnionList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            panchayatList.clear();
            panchayatList.add(0, new PanchayatUnionResultsItem(getString(R.string.select_panchayat_union), 0));
            final PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setDivisionID(divisionID);
            partListParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPanchayatUnionList(headerMap, partListParam).enqueue(new Callback<PanchayatUnionListResponse>() {
                @Override
                public void onResponse(Call<PanchayatUnionListResponse> call, Response<PanchayatUnionListResponse> response) {
                    hideProgress();
                    panchayatUnionListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (panchayatUnionListResponse.getResults() != null) {
                            if (panchayatUnionListResponse.getResults().size() > 0) {
                                showType(true, getString(R.string.pachayat_union));
                            }
                            for (int i = 0; i < panchayatUnionListResponse.getResults().size(); i++) {
                                panchayatList.add(new PanchayatUnionResultsItem(
                                        panchayatUnionListResponse.getResults().get(i).getVillagePanchayatName() != null ?
                                                panchayatUnionListResponse.getResults().get(i).getVillagePanchayatName() : "",
                                        panchayatUnionListResponse.getResults().get(i).getId()));
                            }
                            PanchyatUnionAdapter partAdapter = new PanchyatUnionAdapter(panchayatList, getActivity());
                            spinnerPart.setAdapter(partAdapter);
                        } else {
                            showType(false, "");
                            Toast.makeText(getActivity(), panchayatUnionListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        showType(false, "");
                        Toast.makeText(getActivity(), panchayatUnionListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PanchayatUnionListResponse> call, Throwable t) {
                    hideProgress();
                    showType(false, "");
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            userError.message = getString(R.string.network_error_message);
            showError(ERROR_SHOW_TYPE_DIALOG, userError);
        }


    }

    public void getTownPanchayatList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            townPanchayatList.clear();
            townPanchayatList.add(0, new TownPanchayatResultsItem(getString(R.string.select_town_panchayat), 0));
            final PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setUnionID(partID);
            partListParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getTownPanchayatList(headerMap, partListParam).enqueue(new Callback<TownPanchayatListResponse>() {
                @Override
                public void onResponse(Call<TownPanchayatListResponse> call, Response<TownPanchayatListResponse> response) {
                    hideProgress();
                    townPanchayatListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (townPanchayatListResponse.getResults() != null) {
                            if (townPanchayatListResponse.getResults().size() > 0) {
                                spinnerVillagePanchayat.setVisibility(View.VISIBLE);
                                textLabelVillagePanchayat.setVisibility(View.VISIBLE);
                                viewBottom6.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < townPanchayatListResponse.getResults().size(); i++) {
                                townPanchayatList.add(new TownPanchayatResultsItem(
                                        townPanchayatListResponse.getResults().get(i).getTownpanchayatname() != null ?
                                                townPanchayatListResponse.getResults().get(i).getTownpanchayatname() : "",
                                        townPanchayatListResponse.getResults().get(i).getId()));
                            }
                            TownPanchayatAdapter partAdapter = new TownPanchayatAdapter(townPanchayatList, getActivity());
                            spinnerVillagePanchayat.setAdapter(partAdapter);
                        } else {
                            spinnerVillagePanchayat.setVisibility(View.GONE);
                            textLabelVillagePanchayat.setVisibility(View.GONE);
                            viewBottom6.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), townPanchayatListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        spinnerVillagePanchayat.setVisibility(View.GONE);
                        textLabelVillagePanchayat.setVisibility(View.GONE);
                        viewBottom6.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), townPanchayatListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TownPanchayatListResponse> call, Throwable t) {
                    hideProgress();
                    spinnerVillagePanchayat.setVisibility(View.GONE);
                    textLabelVillagePanchayat.setVisibility(View.GONE);
                    viewBottom6.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }


    //    public void getVattamList() {
//        if (Util.isNetworkAvailable()) {
//            showProgress();
//            vattamList.clear();
//            vattamList.add(0, new VattamResultsItem(0, getString(R.string.select_vattam)));
//            final VattamWardList vattamWardListParam = new VattamWardList();
//            vattamWardListParam.setDivisionID(divisionID);
//            vattamWardListParam.setPartID(partID);
//            dmkAPI.getVattamList(vattamWardListParam).enqueue(new Callback<VattamListResponse>() {
//                @Override
//                public void onResponse(Call<VattamListResponse> call, Response<VattamListResponse> response) {
//                    hideProgress();
//                    vattamListResponse = response.body();
//                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
//                        if (vattamListResponse.getResults().size() > 0) {
//                            spinnerVillagePanchayat.setVisibility(View.VISIBLE);
//                            textLabelVillagePanchayat.setVisibility(View.VISIBLE);
//                            viewBottom6.setVisibility(View.VISIBLE);
//                        }
//                        for (int i = 0; i < vattamListResponse.getResults().size(); i++) {
//                            vattamList.add(new VattamResultsItem(
//                                    vattamListResponse.getResults().get(i).getId(),
//                                    vattamListResponse.getResults().get(i).getVattamname()));
//                        }
//                        VattamAdapter vattamAdapter = new VattamAdapter(vattamList, getActivity());
//                        spinnerVillagePanchayat.setAdapter(vattamAdapter);
//
//                    } else {
//                        spinnerVillagePanchayat.setVisibility(View.GONE);
//                        textLabelVillagePanchayat.setVisibility(View.GONE);
//                        viewBottom6.setVisibility(View.GONE);
//                        Toast.makeText(getActivity(), getString(R.string.no_it_vattam), Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<VattamListResponse> call, Throwable t) {
//                    hideProgress();
//                    spinnerVillagePanchayat.setVisibility(View.GONE);
//                    textLabelVillagePanchayat.setVisibility(View.GONE);
//                    viewBottom6.setVisibility(View.GONE);
//                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
//
//                }
//            });
//
//
//        } else {
//            userError.message = getString(R.string.network_error_message);
//            showError(ERROR_SHOW_TYPE_DIALOG, userError);
//        }
//    }
//
    public void getVillagePanchayatList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            villagePanchayatResultsItemArrayList.clear();
            villagePanchayatResultsItemArrayList.add(0, new VillagePanchayatResultsItem(getString(R.string.select_village_panchayat), 0));
            final VattamWardList partListParam = new VattamWardList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setUnionID(partID);
            partListParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getVillagePanchayatList(headerMap, partListParam).enqueue(new Callback<VillagePanchayatListResponse>() {
                @Override
                public void onResponse(Call<VillagePanchayatListResponse> call, Response<VillagePanchayatListResponse> response) {
                    hideProgress();
                    villagePanchayatListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (villagePanchayatListResponse.getResults().size() > 0) {
                            spinnerVillagePanchayat.setVisibility(View.VISIBLE);
                            textLabelVillagePanchayat.setVisibility(View.VISIBLE);
                            viewBottom6.setVisibility(View.VISIBLE);
                        }
                        for (int i = 0; i < villagePanchayatListResponse.getResults().size(); i++) {
                            villagePanchayatResultsItemArrayList.add(new VillagePanchayatResultsItem(
                                    villagePanchayatListResponse.getResults().get(i).getVillageName() != null ?
                                            villagePanchayatListResponse.getResults().get(i).getVillageName() : "",
                                    villagePanchayatListResponse.getResults().get(i).getId()));
                        }
                        VillagePanchayatAdapter vattamAdapter = new
                                VillagePanchayatAdapter(villagePanchayatResultsItemArrayList, getActivity());
                        spinnerVillagePanchayat.setAdapter(vattamAdapter);

                    } else {
                        spinnerVillagePanchayat.setVisibility(View.GONE);
                        textLabelVillagePanchayat.setVisibility(View.GONE);
                        viewBottom6.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), getString(R.string.no_it_village_panchayat), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<VillagePanchayatListResponse> call, Throwable t) {
                    hideProgress();
                    spinnerVillagePanchayat.setVisibility(View.GONE);
                    textLabelVillagePanchayat.setVisibility(View.GONE);
                    viewBottom6.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getWardList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            wardList.clear();
            wardList.add(0, new WardListResultsItem(getString(R.string.select_ward), 0));
            final VattamWardList vattamWardListParam = new VattamWardList();
            vattamWardListParam.setDivisionID(divisionID);
            vattamWardListParam.setPartID(partID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getWardList(headerMap, vattamWardListParam).enqueue(new Callback<WardListResponse>() {
                @Override
                public void onResponse(Call<WardListResponse> call, Response<WardListResponse> response) {
                    hideProgress();
                    wardListResponse = response.body();

                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (wardListResponse.getResults() != null) {
                            if (wardListResponse.getResults().size() > 0) {
                                spinnerWard.setVisibility(View.VISIBLE);
                                textLabelWard.setVisibility(View.VISIBLE);
                                viewBottom7.setVisibility(View.VISIBLE);
                            }

                            for (int i = 0; i < wardListResponse.getResults().size(); i++) {
                                wardList.add(new WardListResultsItem(
                                        wardListResponse.getResults().get(i).getWardname() != null ?
                                                wardListResponse.getResults().get(i).getWardname() : "",
                                        wardListResponse.getResults().get(i).getId()));
                            }
                            WardAdapter wardAdapter = new WardAdapter(wardList, getActivity());
                            spinnerWard.setAdapter(wardAdapter);

                        } else {
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom7.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), wardListResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_ward), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<WardListResponse> call, Throwable t) {
                    hideProgress();
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottom7.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    public void getBoothList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            boothList.clear();
            boothList.add(0, new BoothListResultsItem(getString(R.string.select_booth), 0));
            BoothListInputParam boothListInputParam = new BoothListInputParam();
            boothListInputParam.setDivisionID(divisionID);
            boothListInputParam.setPartID(partID);
            boothListInputParam.setWardID(wardID);
            boothListInputParam.setVillageID(villageID);
            boothListInputParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getBoothList(headerMap, boothListInputParam).enqueue(new Callback<BoothListResponse>() {
                @Override
                public void onResponse(Call<BoothListResponse> call, Response<BoothListResponse> response) {
                    hideProgress();
                    boothListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (boothListResponse.getResults() != null) {
                            if (boothListResponse.getResults().size() > 0) {
                                spinnerBooth.setVisibility(View.VISIBLE);
                                textLabelBooth.setVisibility(View.VISIBLE);
                                viewBottom23.setVisibility(View.VISIBLE);
                            }

                            for (int i = 0; i < boothListResponse.getResults().size(); i++) {
                                boothList.add(new BoothListResultsItem(
                                        boothListResponse.getResults().get(i).getBoothname() != null ?
                                                boothListResponse.getResults().get(i).getBoothname() : "",
                                        boothListResponse.getResults().get(i).getId()));
                            }
                            BoothAdapter boothAdapter = new BoothAdapter(boothList, getActivity());
                            spinnerBooth.setAdapter(boothAdapter);

                        } else {
                            spinnerBooth.setVisibility(View.GONE);
                            textLabelBooth.setVisibility(View.GONE);
                            viewBottom23.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), boothListResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_booth_list_found), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<BoothListResponse> call, Throwable t) {
                    hideProgress();
                    spinnerBooth.setVisibility(View.GONE);
                    textLabelBooth.setVisibility(View.GONE);
                    viewBottom23.setVisibility(View.GONE);
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getItWingsList() {
        showProgress();
        itWingsListResultsItems.clear();
        itWingsListResultsItems.add(0, new ItWingsListResultsItem(0, getString(R.string.select_wing)));
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
        dmkAPI.getItWingsList(headerMap).enqueue(new Callback<ItWingsListResponse>() {
            @Override
            public void onResponse(Call<ItWingsListResponse> call, Response<ItWingsListResponse> response) {
                itWingsListResponse = response.body();
                hideProgress();
                if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                    if (itWingsListResponse.getResults() != null) {
                        textLabelWings.setVisibility(View.VISIBLE);
                        spinnerWings.setVisibility(View.VISIBLE);
                        viewBottom8.setVisibility(View.VISIBLE);
                        for (int i = 0; i < itWingsListResponse.getResults().size(); i++) {
                            itWingsListResultsItems.add(new ItWingsListResultsItem(
                                    itWingsListResponse.getResults().get(i).getId(),
                                    itWingsListResponse.getResults().get(i).getWingName() != null ?
                                            itWingsListResponse.getResults().get(i).getWingName() : ""));
                        }
                        ITWingsAdapter itWingsAdapter = new ITWingsAdapter(itWingsListResultsItems, getActivity());
                        spinnerWings.setAdapter(itWingsAdapter);
                    } else {
                        Toast.makeText(getActivity(), itWingsListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getActivity(), getString(R.string.no_it_wing), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ItWingsListResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getPartyRoleList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            partyRoleListResultsItems.clear();
            partyRoleListResultsItems.add(0, new PartyRoleListResultsItem(0, getString(R.string.select_Party_role)));
            final PartyRoleList partyRoleListparam = new PartyRoleList();
            partyRoleListparam.setwingID(itWingID);
            partyRoleListparam.setDivisionID(divisionID);
            partyRoleListparam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPartyRoleList(headerMap, partyRoleListparam).enqueue(new Callback<PartyRoleListResponse>() {
                @Override
                public void onResponse(Call<PartyRoleListResponse> call, Response<PartyRoleListResponse> response) {
                    partyRoleListResponse = response.body();
                    hideProgress();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (partyRoleListResponse.getResults() != null) {
                            spinnerPartyRole.setVisibility(View.VISIBLE);
                            textLabelPartyRole.setVisibility(View.VISIBLE);
                            viewBottom12.setVisibility(View.VISIBLE);
                            for (int i = 0; i < partyRoleListResponse.getResults().size(); i++) {
                                partyRoleListResultsItems.add(new PartyRoleListResultsItem(
                                        partyRoleListResponse.getResults().get(i).getId(),
                                        partyRoleListResponse.getResults().get(i).getRolename() != null ?
                                                partyRoleListResponse.getResults().get(i).getRolename() : ""));
                            }
                            PartyRoleAdapter partyRoleAdapter = new PartyRoleAdapter(partyRoleListResultsItems, getActivity());
                            spinnerPartyRole.setAdapter(partyRoleAdapter);
                        } else {
                            Toast.makeText(getActivity(), partyRoleListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        Toast.makeText(getActivity(), getString(R.string.no_it_party_role), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<PartyRoleListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
        }
    }

    private void profileNullCheck() {


        if (editFirstName.getText().toString() != null && editFirstName.getText().toString().length() > 0 && !editFirstName.getText().toString().isEmpty() &&
                editLastName.getText().toString() != null && editLastName.getText().toString().length() > 0 && !editLastName.getText().toString().isEmpty() &&
                editDob.getText().toString() != null && editDob.getText().toString().length() > 0 && !editDob.getText().toString().isEmpty() &&
                editAddress.getText().toString() != null && editAddress.getText().toString().length() > 0 && !editAddress.getText().toString().isEmpty() &&
                editVoterId.getText().toString() != null && editVoterId.getText().toString().length() > 0 && !editVoterId.getText().toString().isEmpty()
                && Util.isValidEmailAddress(editEmailid.getText().toString()) && gender != 2) {

            if (spinnerDivision.isShown() ? true : false) {
                switch (spinnerDivision.getSelectedItemPosition()) {
                    case 0:
                        ((TextView) spinnerDivision.getSelectedView()).setError(getString(R.string.select_division));
                        break;
                    case 1:
                        if (spinnerPart.isShown()) {
                            if (spinnerPart.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerPart.getSelectedView()).setError(getString(R.string.select_part));
                                break;
                            }

                        }
                        if (spinnerWard.isShown()) {
                            if (spinnerWard.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerWard.getSelectedView()).setError(getString(R.string.select_ward));
                                break;
                            }

                        }
                        if (spinnerBooth.isShown()) {
                            if (spinnerBooth.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerBooth.getSelectedView()).setError(getString(R.string.select_booth));
                                break;
                            }
                        }
                        if (spinnerWings.isShown()) {
                            if (spinnerWings.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerWings.getSelectedView()).setError(getString(R.string.select_wing));
                                break;
                            }
                        }
                        if (spinnerPartyRole.isShown()) {
                            if (spinnerPartyRole.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerPartyRole.getSelectedView()).setError(getString(R.string.select_Party_role));
                                break;
                            } else {
                                if (TextUtils.isEmpty(selectedImage)) {
                                    Toast.makeText(getContext(), "Please select profile picture", Toast.LENGTH_SHORT).show();
                                } else {
                                    uploadMedia();
                                }
                            }
                        }
                        break;
                    case 2:
                        if (spinnerPart.isShown()) {
                            if (spinnerPart.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerPart.getSelectedView()).setError(getString(R.string.select_part));
                                break;
                            }

                        }
                        if (spinnerWard.isShown()) {
                            if (spinnerWard.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerWard.getSelectedView()).setError(getString(R.string.select_ward));
                                break;
                            }

                        }
                        if (spinnerBooth.isShown()) {
                            if (spinnerBooth.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerBooth.getSelectedView()).setError(getString(R.string.select_booth));
                                break;
                            }
                        }
                        if (spinnerWings.isShown()) {
                            if (spinnerWings.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerWings.getSelectedView()).setError(getString(R.string.select_wing));
                                break;
                            }
                        }
                        if (spinnerPartyRole.isShown()) {
                            if (spinnerPartyRole.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerPartyRole.getSelectedView()).setError(getString(R.string.select_Party_role));
                                break;
                            } else {
                                if (TextUtils.isEmpty(selectedImage)) {
                                    Toast.makeText(getContext(), "Please select profile picture", Toast.LENGTH_SHORT).show();
                                } else if (gender == 2) {
                                    textlabelGenderError.setError(getString(R.string.gender_error));
                                } else {
                                    uploadMedia();
                                }
                            }
                        }
                        break;

                    case 3:
                        if (spinnerPart.isShown()) {
                            if (spinnerPart.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerPart.getSelectedView()).setError(getString(R.string.select_part));
                                break;
                            }

                        }
                        if (spinnerUnionType.isShown()) {
                            if (spinnerUnionType.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerUnionType.getSelectedView()).setError(getString(R.string.select_panchayat_union));
                                break;
                            }
                        }
                        if (spinnerVillagePanchayat.isShown()) {
                            if (spinnerVillagePanchayat.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerVillagePanchayat.getSelectedView()).setError(getString(R.string.select_village_panchayat));
                                break;
                            }
                        }

                        if (spinnerWard.isShown()) {
                            if (spinnerWard.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerWard.getSelectedView()).setError(getString(R.string.select_ward));
                                break;
                            }

                        }
                        if (spinnerBooth.isShown()) {
                            if (spinnerBooth.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerBooth.getSelectedView()).setError(getString(R.string.select_booth));
                                break;
                            }
                        }
                        if (spinnerWings.isShown()) {
                            if (spinnerWings.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerWings.getSelectedView()).setError(getString(R.string.select_wing));
                                break;
                            }
                        }
                        if (spinnerPartyRole.isShown()) {
                            if (spinnerPartyRole.getSelectedItemPosition() == 0) {
                                ((TextView) spinnerPartyRole.getSelectedView()).setError(getString(R.string.select_Party_role));
                                break;
                            } else {
                                if (TextUtils.isEmpty(selectedImage)) {
                                    Toast.makeText(getContext(), "Please select profile picture", Toast.LENGTH_SHORT).show();
                                } else {
                                    uploadMedia();
                                }

                            }
                        }
                        break;


                }

            } else {
                if (spinnerDistrict.isShown()) {
                    if (spinnerDistrict.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerDistrict.getSelectedView()).setError(getString(R.string.select_district));
                    }

                }
                if (spinnerConstituency.isShown()) {

                    if (spinnerConstituency.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerConstituency.getSelectedView()).setError(getString(R.string.select_constituency));
                    }

                }


            }


        } else {
            if (editFirstName.getText().toString() == null || editFirstName.getText().toString().isEmpty() ||
                    editFirstName.getText().toString().length() < 0) {
                editFirstName.setError(getString(R.string.name_missing));
            }
            if (editLastName.getText().toString() == null || editLastName.getText().toString().isEmpty()
                    || editLastName.getText().toString().length() < 0) {
                editLastName.setError(getString(R.string.last_name));
            }
            if (editDob.getText().toString() == null || editDob.getText().toString().isEmpty()
                    || editDob.getText().toString().length() < 0) {
                editDob.setError(getString(R.string.dob_missing));
            }
            if (editVoterId.getText().toString() == null || editVoterId.getText().toString().isEmpty()
                    || editVoterId.getText().toString().length() < 0) {
                editVoterId.setError(getString(R.string.voter_ID_missing));
            }
            if (editAddress.getText().toString() == null || editAddress.getText().toString().isEmpty()
                    || editAddress.getText().toString().length() < 0) {
                editAddress.setError(getString(R.string.address_missing));
            }

            if (gender == 2) {
                textlabelGenderError.setError(getString(R.string.gender_error));
            }
            if (!Util.isValidEmailAddress(editEmailid.getText().toString())) {
                editEmailid.setError(getString(R.string.email_missing));
            }

        }

    }

    //s3 upload image
    public void uploadMedia() {
        if (selectedImage != null) {
            showProgress();
            final File destination;
            destination = new File(getActivity().getExternalFilesDir(null), selectedImage);
            createFile(getActivity(), galleryUri, destination);
            TransferObserver uploadObserver =
                    transferUtility.upload(getString(R.string.s3_bucket_profile_path) + selectedImage, destination);
            uploadObserver.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.COMPLETED == state) {
                        destination.delete();
                        Partyregistration();
                    } else if (TransferState.FAILED == state) {
                        hideProgress();
                        destination.delete();
                        Toast.makeText(getActivity(), getString(R.string.party_register_error), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                }

                @Override
                public void onError(int id, Exception ex) {
                    ex.printStackTrace();
                    hideProgress();
                    destination.delete();
                    Toast.makeText(getActivity(), getString(R.string.party_register_error), Toast.LENGTH_SHORT).show();
                }

            });
        }else {
            Toast.makeText(getActivity(), getString(R.string.party_register_error), Toast.LENGTH_SHORT).show();
        }
    }


    public void Partyregistration() {
        final Registration registrationParam = new Registration();
        registrationParam.setUserType(DmkConstants.PARTY_USERTYPE);
        registrationParam.setImeiid(sharedPreferences.getString(DmkConstants.IMEIID, null));
        registrationParam.setOsversion(String.valueOf(sharedPreferences.getInt(DmkConstants.OS_VERSION, 0)));
        registrationParam.setAppversion(sharedPreferences.getString(DmkConstants.APPVERSION_CODE, ""));
//        1-android 2-ios
        registrationParam.setApptype(1);
//        registrationParam.setDevicetoken(sharedPreferences.getString(DmkConstants.DEVICEID, null));
        registrationParam.setFirstName(editFirstName.getText().toString());
        registrationParam.setLastName(editLastName.getText().toString());
        registrationParam.setStatus(1);
        registrationParam.setGender(String.valueOf(gender));
        registrationParam.setPhoneNumber(sharedPreferences.getString(DmkConstants.PHONENUMBER, null));
        registrationParam.setDOB(editDob.getText().toString());
        registrationParam.setAge(age);
        registrationParam.setEmailid(editEmailid.getText().toString());
        registrationParam.setAddress(editAddress.getText().toString());
        registrationParam.setVoterID(editVoterId.getText().toString());
        registrationParam.setDevicetoken(fcmSharedPrefrences.getString(DmkConstants.FCMTOKEN, ""));
        registrationParam.setUniontype(unionType);
        if (selectedImage != null) {
            registrationParam.setImage_pro_pic(selectedImage);
        } else {
            registrationParam.setImage_pro_pic("");
        }
        switch (spinnerDivision.isShown() ? divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0) {
            case 1:
                //Part
                registrationParam.setDistrictID(spinnerDistrict.isShown() ?
                        districtList.get(spinnerDistrict.getSelectedItemPosition()).getId() : 0);
                registrationParam.setConstituencyID(spinnerConstituency.isShown() ?
                        constituencyList.get(spinnerConstituency.getSelectedItemPosition()).getId() : 0);
                registrationParam.setTypeid(spinnerDivision.isShown() ?
                        String.valueOf(divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId()) : "");
                registrationParam.setPartid(spinnerPart.isShown() ?
                        partList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
//                registrationParam.setVattamid(vattamList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId());
                registrationParam.setWardID(spinnerWard.isShown() ?
                        wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                registrationParam.setWing(spinnerWings.isShown() ?
                        itWingsListResultsItems.get(spinnerWings.getSelectedItemPosition()).getId() : 0);
                registrationParam.setRoleID(
                        spinnerPartyRole.isShown() ?
                                partyRoleListResultsItems.get(spinnerPartyRole.getSelectedItemPosition()).getId() : 0);
                registrationParam.setBoothID(spinnerBooth.isShown() ?
                        boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                break;
            case 2:
                //municipality
                registrationParam.setDistrictID(spinnerDistrict.isShown() ?
                        districtList.get(spinnerDistrict.getSelectedItemPosition()).getId() : 0);
                registrationParam.setConstituencyID(
                        spinnerConstituency.isShown() ?
                                constituencyList.get(spinnerConstituency.getSelectedItemPosition()).getId() : 0);
                registrationParam.setTypeid(spinnerDivision.isShown() ?
                        String.valueOf(divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId()) : "");
                registrationParam.setMunicipalityid(spinnerPart.isShown() ?
                        municipalityList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
//                registrationParam.setVattamid(vattamList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId());
                registrationParam.setWardID(spinnerWard.isShown() ?
                        wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                registrationParam.setWing(spinnerWings.isShown() ?
                        itWingsListResultsItems.get(spinnerWings.getSelectedItemPosition()).getId() : 0);
                registrationParam.setRoleID(spinnerPartyRole.isShown() ?
                        partyRoleListResultsItems.get(spinnerPartyRole.getSelectedItemPosition()).getId() : 0);
                registrationParam.setBoothID(spinnerBooth.isShown() ?
                        boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                break;
            case 3:
                switch (unionType) {
                    //PanchayatUnion
                    case 1:
                        //TownShip Panchayat
                        registrationParam.setDistrictID(spinnerDistrict.isShown() ?
                                districtList.get(spinnerDistrict.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setConstituencyID(spinnerConstituency.isShown() ?
                                constituencyList.get(spinnerConstituency.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setTypeid(spinnerDivision.isShown() ?
                                String.valueOf(divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId()) : "");
                        registrationParam.setPanchayatunion(spinnerPart.isShown() ?
                                panchayatList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setTownshipid(spinnerVillagePanchayat.isShown() ?
                                townPanchayatList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setWardID(spinnerWard.isShown() ?
                                wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setWing(spinnerWings.isShown() ?
                                itWingsListResultsItems.get(spinnerWings.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setRoleID(spinnerPartyRole.isShown() ?
                                partyRoleListResultsItems.get(spinnerPartyRole.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setBoothID(spinnerBooth.isShown() ?
                                boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                        break;
                    case 2:
                        //Village Panchayat
                        registrationParam.setDistrictID(spinnerDistrict.isShown() ?
                                districtList.get(spinnerDistrict.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setConstituencyID(spinnerConstituency.isShown() ?
                                constituencyList.get(spinnerConstituency.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setTypeid(spinnerDivision.isShown() ?
                                String.valueOf(divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId()) : "");
                        registrationParam.setPanchayatunion(spinnerPart.isShown() ?
                                panchayatList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setVillagepanchayat(spinnerVillagePanchayat.isShown() ?
                                villagePanchayatResultsItemArrayList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setWardID(spinnerWard.isShown() ?
                                wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setWing(spinnerWings.isShown() ?
                                itWingsListResultsItems.get(spinnerWings.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setRoleID(spinnerPartyRole.isShown() ?
                                partyRoleListResultsItems.get(spinnerPartyRole.getSelectedItemPosition()).getId() : 0);
                        registrationParam.setBoothID(spinnerBooth.isShown() ?
                                boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                        break;
                }
                break;
        }

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
        dmkAPI.getRegistraion(headerMap, registrationParam).enqueue(new Callback<PartyRegistraionResponse>() {
            @Override
            public void onResponse(Call<PartyRegistraionResponse> call, Response<PartyRegistraionResponse> response) {
                hideProgress();
                registrationListResponse = response.body();
                if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                    if (registrationListResponse.getResults() != null) {
                        Toast.makeText(getActivity(), getString(R.string.reg_success), Toast.LENGTH_SHORT).show();
                        PartyMemberCardFragment partyMemberCardFragment = new PartyMemberCardFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("firstname", registrationListResponse.getResults().getFirstName() != null ?
                                registrationListResponse.getResults().getFirstName() : "");
                        bundle.putString("lastname", registrationListResponse.getResults().getLastName() != null ?
                                registrationListResponse.getResults().getLastName() : "");
                        bundle.putString("phonenumber", registrationListResponse.getResults().getPhoneNumber() != null ?
                                registrationListResponse.getResults().getPhoneNumber() : "");
                        bundle.putInt("age", registrationListResponse.getResults().getAge());
                        bundle.putString("district", registrationListResponse.getResults().getDistrictName() != null ?
                                registrationListResponse.getResults().getDistrictName() : "");
                        bundle.putString("role", registrationListResponse.getResults().getRoles().getRolename() != null ?
                                registrationListResponse.getResults().getRoles().getRolename() : "");
                        bundle.putParcelable("galleryUri", bundleImageBitmap);
                        partyMemberCardFragment.setArguments(bundle);
                        editor.putString(DmkConstants.FIRSTNAME, registrationListResponse.getResults().getFirstName() != null ?
                                registrationListResponse.getResults().getFirstName() : "");
                        editor.putString(DmkConstants.LASTNAME, registrationListResponse.getResults().getLastName() != null ?
                                registrationListResponse.getResults().getLastName() : "");
                        editor.putString(DmkConstants.MOBILENUMBER, registrationListResponse.getResults().getPhoneNumber() != null ?
                                registrationListResponse.getResults().getPhoneNumber() : "");
                        editor.putInt(DmkConstants.USERID, registrationListResponse.getResults().getId());
                        editor.putString(DmkConstants.USERDISTRICTNAME, registrationListResponse.getResults().getDistrictName() != null ?
                                registrationListResponse.getResults().getDistrictName() : "");
                        editor.putString(DmkConstants.USERWINGNAME, registrationListResponse.getResults().getWing().getWingName() != null ?
                                registrationListResponse.getResults().getWing().getWingName() : "");
                        editor.putString(DmkConstants.DESGINATION, registrationListResponse.getResults().getRoles().getRolename() != null ?
                                registrationListResponse.getResults().getRoles().getRolename() : "");
                        editor.putString(DmkConstants.UNIQUEDMKID, registrationListResponse.getResults().getUniquieID() != null ?
                                registrationListResponse.getResults().getUniquieID() : "");
                        editor.putInt(DmkConstants.USERAGE, registrationListResponse.getResults().getAge());
                        editor.putString(DmkConstants.USERDISTRICTNAME, registrationListResponse.getResults().getDistrictName() != null ?
                                registrationListResponse.getResults().getDistrictName() : "");
                        editor.putInt(DmkConstants.SUPERIORID, registrationListResponse.getResults().getSuperiorUserID());
                        editor.putInt(DmkConstants.USER_DISTRICT_ID, registrationListResponse.getResults().getDistrictID());
                        editor.putInt(DmkConstants.USER_PARTY_DISTRICT_ID, registrationListResponse.getResults().getPartydistrict());
                        editor.putInt(DmkConstants.USER_CONSTITUENCY_ID, registrationListResponse.getResults().getConstituencyID());
                        editor.putInt(DmkConstants.USER_DIVISION_ID,
                                registrationListResponse.getResults().getTypeid() != 0 ?
                                        registrationListResponse.getResults().getTypeid() : 0);
                        editor.putInt(DmkConstants.USER_PART_ID, registrationListResponse.getResults().getPartid());
//                    editor.putInt(DmkConstants.USER_VATTAM_ID, registrationListResponse.getResults().getVattamid());
                        editor.putInt(DmkConstants.USER_WARD_ID, registrationListResponse.getResults().getWardID());
                        editor.putInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, registrationListResponse.getResults().getVillagepanchayat());
                        editor.putInt(DmkConstants.USER_PANCHAYAT_UNION_ID, registrationListResponse.getResults().getPanchayatunion());
                        editor.putInt(DmkConstants.USER_TOWNSHIP_ID, registrationListResponse.getResults().getTownshipid());
                        editor.putInt(DmkConstants.USER_BOOTH_ID,
                                !registrationListResponse.getResults().getBoothID().isEmpty() ?
                                        Integer.parseInt(registrationListResponse.getResults().getBoothID()) : 0);
                        editor.putInt(DmkConstants.DESIGNATIONID, registrationListResponse.getResults().getRoles().getLevel());
                        editor.putInt(DmkConstants.ROLL_ID, registrationListResponse.getResults().getRoles().getId());
                        editor.putInt(DmkConstants.USERTTYPE, registrationListResponse.getResults().getUserType());
                        editor.putString(DmkConstants.PROFILE_PICTURE, registrationListResponse.getResults().getAvatar() != null ?
                                registrationListResponse.getResults().getAvatar() : "");
                        editor.putString(DmkConstants.USERPERMISSION_COMMON, registrationListResponse.getResults().getUserPermissionset().getCommon() != null ?
                                registrationListResponse.getResults().getUserPermissionset().getCommon() : "");
                        editor.putString(DmkConstants.USERPERMISSION_ARTICLE, registrationListResponse.getResults().getUserPermissionset().getArticle() != null ?
                                registrationListResponse.getResults().getUserPermissionset().getArticle() : "");
                        editor.putString(DmkConstants.USERPERMISSION_EVENTS, registrationListResponse.getResults().getUserPermissionset().getEvents() != null ?
                                registrationListResponse.getResults().getUserPermissionset().getEvents() : "");
                        editor.putString(DmkConstants.USERPERMISSION_NEWS, registrationListResponse.getResults().getUserPermissionset().getNews() != null ?
                                registrationListResponse.getResults().getUserPermissionset().getNews() : "");
                        editor.putString(DmkConstants.USERPERMISSION_ELECTION, registrationListResponse.getResults().getUserPermissionset().getElection() != null ?
                                registrationListResponse.getResults().getUserPermissionset().getElection() : "");
                        editor.putString(DmkConstants.USER_ADDRESS, registrationListResponse.getResults().getAddress() != null ?
                                registrationListResponse.getResults().getAddress() : "");
                        editor.putString(DmkConstants.USER_VOTER_ID, registrationListResponse.getResults().getVoterID() != null ?
                                registrationListResponse.getResults().getVoterID() : "");
                        editor.putInt(DmkConstants.USER_UNION_TYPE_ID, registrationListResponse.getResults().getUniontype());
                        editor.putString(DmkConstants.HEADER_SOURCE, registrationListResponse.getResults().getSource());
                        editor.putString(DmkConstants.HEADER_SOURCE_DATA, registrationListResponse.getResults().getSourcedata());
                        editor.putString(DmkConstants.HEADER, getEncodedHeader(registrationListResponse.getResults().getSource(),
                                registrationListResponse.getResults().getSourcedata()));
                        editor.commit();
                        profileActivity.push(partyMemberCardFragment);
                    } else {
                        Toast.makeText(getActivity(), registrationListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.party_register_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PartyRegistraionResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(getActivity(), "There is no superior for this role", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
