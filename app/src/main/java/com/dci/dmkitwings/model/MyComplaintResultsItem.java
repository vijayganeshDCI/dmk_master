package com.dci.dmkitwings.model;

public class MyComplaintResultsItem {
	private String Status;
	private int ParentID;
	private CompliantsPosteduser posteduser;
	private int ReceiverUserID	;
	private CompliantReceiveduser receiveduser;
	private String Escalated;
	private String Content;
	private int PostedUserID;
	private int id;
	private String SubjectID;
	private String Replied;
	private String Reason;
	private int Categoryid;


	public CompliantReceiveduser getReceiveduser() {
		return receiveduser;
	}

	public void setReceiveduser(CompliantReceiveduser receiveduser) {
		this.receiveduser = receiveduser;
	}

	public int getCategoryid() {
		return Categoryid;
	}

	public CompliantsPosteduser getPosteduser() {
		return posteduser;
	}

	public void setPosteduser(CompliantsPosteduser posteduser) {
		this.posteduser = posteduser;
	}

	public void setCategoryid(int categoryid) {
		Categoryid = categoryid;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public int getParentID() {
		return ParentID;
	}

	public void setParentID(int parentID) {
		ParentID = parentID;
	}
//
//	public CompliantsPosteduser getPosteduser() {
//		return posteduser;
//	}
//
//	public void setPosteduser(CompliantsPosteduser posteduser) {
//		this.posteduser = posteduser;
//	}

	public int getReceiverUserID() {
		return ReceiverUserID;
	}

	public void setReceiverUserID(int receiverUserID) {
		ReceiverUserID = receiverUserID;
	}

//	public CompliantReceiveduser getReceiveduser() {
//		return receiveduser;
//	}
//
//	public void setReceiveduser(CompliantReceiveduser receiveduser) {
//		this.receiveduser = receiveduser;
//	}

	public String getEscalated() {
		return Escalated;
	}

	public void setEscalated(String escalated) {
		Escalated = escalated;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public int getPostedUserID() {
		return PostedUserID;
	}

	public void setPostedUserID(int postedUserID) {
		PostedUserID = postedUserID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubjectID() {
		return SubjectID;
	}

	public void setSubjectID(String subjectID) {
		SubjectID = subjectID;
	}

	public String getReplied() {
		return Replied;
	}

	public void setReplied(String replied) {
		Replied = replied;
	}

	public String getReason() {
		return Reason;
	}

	public void setReason(String reason) {
		Reason = reason;
	}
}
