package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.TimeLineDetailPageActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.CommentFragment;
import com.dci.dmkitwings.model.CommentEditParams;
import com.dci.dmkitwings.model.CommentEditResponse;
import com.dci.dmkitwings.model.CommentList;
import com.dci.dmkitwings.model.TimeLineDeleteResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Java_AES_Cipher;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomEditText;
import com.dci.dmkitwings.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentListAdapter extends BaseAdapter {


    public CommentListAdapter(Context context, List<CommentList> commentLists, CommentFragment commentFragment,
                              Activity activity) {
        this.context = context;
        this.commentLists = commentLists;
        this.activity = activity;
        this.commentFragment = commentFragment;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DmkApplication.getContext().getComponent().inject(this);
    }

    Context context;
    CommentFragment commentFragment;
    Activity activity, activity1;
    BaseActivity baseActivity;
    TimeLineDetailPageActivity timeLineDetailPageActivity;
    List<CommentList> commentLists;
    private final LayoutInflater mInflater;
    ViewHolder viewHolder = null;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    CommentEditResponse commentEditResponse;
    TimeLineDeleteResponse timeLineDeleteResponse;


    @Override
    public int getCount() {
        return commentLists.size();
    }

    @Override
    public CommentList getItem(int position) {
        return commentLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        editor = sharedPreferences.edit();
        if (activity instanceof BaseActivity)
            baseActivity = (BaseActivity) activity;
        else
            timeLineDetailPageActivity = (TimeLineDetailPageActivity) activity;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_comment_list, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
//        viewHolder.imageProPic.setImageResource();
        viewHolder.textUserComment.setText(commentLists.get(position).getComment());

        if (commentLists.get(position).getUserID() == sharedPreferences.getInt(DmkConstants.USERID, 0)) {
            viewHolder.textUserName.setText("You");
            viewHolder.imageEdit.setVisibility(View.VISIBLE);
            viewHolder.imageDelete.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imageEdit.setVisibility(View.GONE);
            viewHolder.imageDelete.setVisibility(View.GONE);
            viewHolder.textUserName.setText(commentLists.get(position).getName());
        }

        viewHolder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setMessage(R.string.confirm_to_delete);
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteComment(position);
                    }

                });
                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });

        viewHolder.imageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setMessage(R.string.confirm_to_edit);
                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(context);
                        View dialogView = mInflater.inflate(R.layout.alert_edit_comment, null);
                        alertDialog1.setView(dialogView);
                        final CustomEditText editText = (CustomEditText) dialogView.findViewById(R.id.edit_write_comment);
                        editText.setText(commentLists.get(position).getComment());
                        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_comment_send);
                        alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (editText.getText().toString().isEmpty())
                                    editText.setError("Empty Comment");
                                else
                                    updateComment(position, editText.getText().toString());
                            }

                        });
                        alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertDialog1.show();
                    }

                });
                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });

        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.image_pro_pic)
        CircleImageView imageProPic;
        @BindView(R.id.text_user_name)
        CustomTextView textUserName;
        @BindView(R.id.text_user_comment)
        CustomTextView textUserComment;
        @BindView(R.id.cons_com_item)
        ConstraintLayout consComItem;
        @BindView(R.id.cons_comment_list)
        ConstraintLayout consCommentList;
        @BindView(R.id.image_delete)
        ImageView imageDelete;
        @BindView(R.id.image_edit)
        ImageView imageEdit;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private void updateComment(final int position, final String comment) {
        CommentEditParams commentEditParams = new CommentEditParams();
        commentEditParams.setComment(comment);
        commentEditParams.setCommentid(commentLists.get(position).getCommentID());
        commentEditParams.setTimelineid(commentLists.get(position).getTimeLineID());
        commentEditParams.setParentid(0);
        commentEditParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            if (activity instanceof BaseActivity)
                baseActivity.showProgress();
            else
                timeLineDetailPageActivity.showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.updateComment(headerMap, commentEditParams).enqueue(new Callback<CommentEditResponse>() {
                @Override
                public void onResponse(Call<CommentEditResponse> call, Response<CommentEditResponse> response) {
                    commentEditResponse = response.body();
                    if (activity instanceof BaseActivity)
                        baseActivity.hideProgress();
                    else
                        timeLineDetailPageActivity.hideProgress();
                    if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                        if (commentEditResponse.getStatuscode() == 0) {
                            if (commentFragment instanceof CommentFragment)
                                commentFragment.getCommentList();
                            else
                                commentLists.get(position).setComment(commentEditResponse.getResults().getComment());

                            notifyDataSetChanged();
                            Toast.makeText(context, "Updated Success", Toast.LENGTH_SHORT).show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, commentEditResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, commentEditResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(commentEditResponse.getSource(),
                                    commentEditResponse.getSourcedata()));
                            editor.commit();
                            updateComment(position, comment);
                        }
                    } else {
                        Toast.makeText(context, "Updated Failed", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommentEditResponse> call, Throwable t) {
                    if (activity instanceof BaseActivity)
                        baseActivity.hideProgress();
                    else
                        timeLineDetailPageActivity.hideProgress();
                    Toast.makeText(context, "Updated Failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteComment(final int position) {
        CommentEditParams commentEditParams = new CommentEditParams();
        commentEditParams.setCommentid(commentLists.get(position).getCommentID());
        commentEditParams.setTimelineid(commentLists.get(position).getTimeLineID());
        commentEditParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            if (activity instanceof BaseActivity)
                baseActivity.showProgress();
            else
                timeLineDetailPageActivity.showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.deleteComment(headerMap, commentEditParams).enqueue(new Callback<TimeLineDeleteResponse>() {
                @Override
                public void onResponse(Call<TimeLineDeleteResponse> call, Response<TimeLineDeleteResponse> response) {
                    timeLineDeleteResponse = response.body();
                    if (activity instanceof BaseActivity)
                        baseActivity.hideProgress();
                    else
                        timeLineDetailPageActivity.hideProgress();
                    if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                        if (timeLineDeleteResponse.getStatuscode()==0) {
                            if (commentFragment instanceof CommentFragment)
                                commentFragment.getCommentList();
                            notifyDataSetChanged();
                            commentLists.remove(position);
                            Toast.makeText(context, "Deleted Success", Toast.LENGTH_SHORT).show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, timeLineDeleteResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, timeLineDeleteResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(timeLineDeleteResponse.getSource(),
                                    timeLineDeleteResponse.getSourcedata()));
                            editor.commit();
                            deleteComment(position);
                        }
                    } else {
                        Toast.makeText(context, "Deleted Failed", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TimeLineDeleteResponse> call, Throwable t) {
                    if (activity instanceof BaseActivity)
                        baseActivity.hideProgress();
                    else
                        timeLineDetailPageActivity.hideProgress();
                    Toast.makeText(context, "Deleted Failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public String getEncodedHeader(String sourceResponse, String sourceDataResponse) {

        byte[] sourceBase64 = Base64.decode(sourceResponse, Base64.NO_WRAP);
        byte[] sourceDataBase64 = Base64.decode(sourceDataResponse, Base64.NO_WRAP);
        String source = new String(sourceBase64, StandardCharsets.UTF_8);
        String sourceData = new String(sourceDataBase64, StandardCharsets.UTF_8);
        String data = source.substring(0, 12) + sourceData.substring(0, 8);
        String key = source.substring(12, 19) + sourceData.substring(8, 13);
        String iv = source.substring(19, 28) + sourceData.substring(13, 20);
        String encrpytData = Java_AES_Cipher.encrypt(key, iv, data);
        byte[] dataBase64Encode = encrpytData.getBytes(StandardCharsets.UTF_8);
        String base64 = Base64.encodeToString(dataBase64Encode, Base64.NO_WRAP);
        return base64;
    }
}
