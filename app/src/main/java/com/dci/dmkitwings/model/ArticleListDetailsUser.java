package com.dci.dmkitwings.model;

public class ArticleListDetailsUser {
    private int id;
    private  int UserType;
    private String FirstName;
    private String LastName;
    private String Designation;
    private String avatar;
    private int Level;
    private TimelineDetailsDistrict district;
    private TimelineDetailsConstituency constituency;
    private TimelineDetailsPart part;
    private TimelineDetailsWard ward;
    private TimelineDetailsBooth booth;
    private TimelineDetailsPanchayatunion panchayatunion;
    private TimelineDetailsvillagePanchayat village;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getLevel() {
        return Level;
    }

    public void setLevel(int level) {
        Level = level;
    }

    public TimelineDetailsDistrict getDistrict() {
        return district;
    }

    public void setDistrict(TimelineDetailsDistrict district) {
        this.district = district;
    }

    public TimelineDetailsConstituency getConstituency() {
        return constituency;
    }

    public void setConstituency(TimelineDetailsConstituency constituency) {
        this.constituency = constituency;
    }

    public TimelineDetailsPart getPart() {
        return part;
    }

    public void setPart(TimelineDetailsPart part) {
        this.part = part;
    }

    public TimelineDetailsWard getWard() {
        return ward;
    }

    public void setWard(TimelineDetailsWard ward) {
        this.ward = ward;
    }

    public TimelineDetailsBooth getBooth() {
        return booth;
    }

    public void setBooth(TimelineDetailsBooth booth) {
        this.booth = booth;
    }

    public TimelineDetailsPanchayatunion getPanchayatunion() {
        return panchayatunion;
    }

    public void setPanchayatunion(TimelineDetailsPanchayatunion panchayatunion) {
        this.panchayatunion = panchayatunion;
    }

    public TimelineDetailsvillagePanchayat getVillage() {
        return village;
    }

    public void setVillage(TimelineDetailsvillagePanchayat village) {
        this.village = village;
    }
}
