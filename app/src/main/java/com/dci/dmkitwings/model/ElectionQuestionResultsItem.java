package com.dci.dmkitwings.model;

public class ElectionQuestionResultsItem {
	private String Option1;
	private String Status;
	private String Option2;
	private String Option3;
	private String Option4;
	private int Surveyid;
	private String updated_at;
	private String created_at;
	private String Question;
	private int id;
	private String Option5;

	public String getOption1() {
		return Option1;
	}

	public void setOption1(String option1) {
		Option1 = option1;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getOption2() {
		return Option2;
	}

	public void setOption2(String option2) {
		Option2 = option2;
	}

	public String getOption3() {
		return Option3;
	}

	public void setOption3(String option3) {
		Option3 = option3;
	}

	public String getOption4() {
		return Option4;
	}

	public void setOption4(String option4) {
		Option4 = option4;
	}

	public int getSurveyid() {
		return Surveyid;
	}

	public void setSurveyid(int surveyid) {
		Surveyid = surveyid;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getQuestion() {
		return Question;
	}

	public void setQuestion(String question) {
		Question = question;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOption5() {
		return Option5;
	}

	public void setOption5(String option5) {
		Option5 = option5;
	}

	public ElectionQuestionResultsItem(String option1, String status, String option2, String option3, String option4, int surveyid, String updated_at, String created_at, String question, int id, String option5) {
		Option1 = option1;
		Status = status;
		Option2 = option2;
		Option3 = option3;
		Option4 = option4;
		Surveyid = surveyid;
		this.updated_at = updated_at;
		this.created_at = created_at;
		Question = question;
		this.id = id;
		Option5 = option5;
	}
}
