package com.dci.dmkitwings.model;

import java.util.List;

public class NewDetailsResultItem {


    private int id;
    private String title;
    private int mediatype;
    private NewListDetailsUser user;
    private String content;
    private int status;
    private String newstypeid;
    private String created_at;
    private String updated_at;

    public String getVideothumb() {
        return Videothumb;
    }

    public void setVideothumb(String videothumb) {
        Videothumb = videothumb;
    }

    private String Videothumb;

    public List<String> getMediapath() {
        return mediapath;
    }

    public void setMediapath(List<String> mediapath) {
        this.mediapath = mediapath;
    }

    private List<String> mediapath;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getNewstypeid() {
        return newstypeid;
    }

    public void setNewstypeid(String newstypeid) {
        this.newstypeid = newstypeid;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMediatype() {
        return mediatype;
    }

    public void setMediatype(int mediatype) {
        this.mediatype = mediatype;
    }

    public NewListDetailsUser getUser() {
        return user;
    }

    public void setUser(NewListDetailsUser user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
