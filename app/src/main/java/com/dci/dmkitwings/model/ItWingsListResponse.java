package com.dci.dmkitwings.model;

import java.util.List;

public class ItWingsListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<ItWingsListResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<ItWingsListResultsItem> results) {
		Results = results;
	}

	private String Status;
	private List<ItWingsListResultsItem> Results;

	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}