package com.dci.dmkitwings.model;

public class NewsPartyDistrictResultsItem {
	private String DistrictName;
	private int id;

	public String getDistrictName() {
		return DistrictName;
	}

	public void setDistrictName(String districtName) {
		DistrictName = districtName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
