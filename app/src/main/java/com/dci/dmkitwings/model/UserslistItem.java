package com.dci.dmkitwings.model;

public class UserslistItem {
    private int surveyid;
    private String firstname;
    private int pending;
    private String voterid;
    private int completed;
    private int userid;
    private String lastname;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    private String avatar;

    public int getTypeid() {
        return typeid;
    }

    public void setTypeid(int typeid) {
        this.typeid = typeid;
    }

    public int getUniontype() {
        return uniontype;
    }

    public void setUniontype(int uniontype) {
        this.uniontype = uniontype;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getConstituency() {
        return Constituency;
    }

    public void setConstituency(String constituency) {
        Constituency = constituency;
    }

    public String getPartdetails() {
        return partdetails;
    }

    public void setPartdetails(String partdetails) {
        this.partdetails = partdetails;
    }

    public String getMunicipalitydetails() {
        return municipalitydetails;
    }

    public void setMunicipalitydetails(String municipalitydetails) {
        this.municipalitydetails = municipalitydetails;
    }

    public String getWarddetail() {
        return warddetail;
    }

    public void setWarddetail(String warddetail) {
        this.warddetail = warddetail;
    }

    public String getPanchayatuniondetail() {
        return panchayatuniondetail;
    }

    public void setPanchayatuniondetail(String panchayatuniondetail) {
        this.panchayatuniondetail = panchayatuniondetail;
    }

    public String getTowndetails() {
        return towndetails;
    }

    public void setTowndetails(String towndetails) {
        this.towndetails = towndetails;
    }

    public String getVillagepanchayatdetail() {
        return villagepanchayatdetail;
    }

    public void setVillagepanchayatdetail(String villagepanchayatdetail) {
        this.villagepanchayatdetail = villagepanchayatdetail;
    }

    public String getBooth() {
        return Booth;
    }

    public void setBooth(String booth) {
        Booth = booth;
    }

    private int typeid;
    private int uniontype;
    private String district;
    private String Constituency;
    private String partdetails;
    private String municipalitydetails;
    private String warddetail;
    private String panchayatuniondetail;
    private String towndetails;
    private String villagepanchayatdetail;
    private String Booth;


    public int getSurveyid() {
        return surveyid;
    }

    public void setSurveyid(int surveyid) {
        this.surveyid = surveyid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public int getPending() {
        return pending;
    }

    public void setPending(int pending) {
        this.pending = pending;
    }

    public String getVoterid() {
        return voterid;
    }

    public void setVoterid(String voterid) {
        this.voterid = voterid;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
