package com.dci.dmkitwings.model;

public class PartyDistrictInputParam {

    public int getDistrictid() {
        return districtid;
    }

    public void setDistrictid(int districtid) {
        this.districtid = districtid;
    }

    private int districtid;
}
