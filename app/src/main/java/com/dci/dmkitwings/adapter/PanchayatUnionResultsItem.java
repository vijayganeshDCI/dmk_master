package com.dci.dmkitwings.adapter;

public class PanchayatUnionResultsItem {
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getVillagePanchayatName() {
		return VillagePanchayatName;
	}

	public void setVillagePanchayatName(String villagePanchayatName) {
		VillagePanchayatName = villagePanchayatName;
	}

	public String getVillagePanchayatDescription() {
		return VillagePanchayatDescription;
	}

	public void setVillagePanchayatDescription(String villagePanchayatDescription) {
		VillagePanchayatDescription = villagePanchayatDescription;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getConstituencyID() {
		return ConstituencyID;
	}

	public void setConstituencyID(int constituencyID) {
		ConstituencyID = constituencyID;
	}

	private int Status;
	private String VillagePanchayatName;

	public PanchayatUnionResultsItem(String villagePanchayatName, int id) {
		VillagePanchayatName = villagePanchayatName;
		this.id = id;
	}

	private String VillagePanchayatDescription;
	private String updated_at;
	private int DistrictID;
	private String created_at;
	private int id;
	private int ConstituencyID;
	private int uniontype;


	public int getUniontype() {
		return uniontype;
	}

	public void setUniontype(int uniontype) {
		this.uniontype = uniontype;
	}
}
