package com.dci.dmkitwings.model;

public class TimeLineLikeParams
{
    private int userid;
    private int timelineid;
    private int status;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getTimelineid() {
        return timelineid;
    }

    public void setTimelineid(int timelineid) {
        this.timelineid = timelineid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
