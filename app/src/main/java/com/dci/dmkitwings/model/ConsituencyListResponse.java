package com.dci.dmkitwings.model;

import java.util.List;

public class ConsituencyListResponse {
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<ConstituencyListResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<ConstituencyListResultsItem> results) {
		Results = results;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	private String Status;
	private List<ConstituencyListResultsItem> Results;
	private String Message;

}