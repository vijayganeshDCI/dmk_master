package com.dci.dmkitwings.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.activity.LoginActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.UserDetailsResponse;
import com.dci.dmkitwings.model.UserStatusParam;
import com.dci.dmkitwings.model.UserStatusResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.msg91.sendotp.library.PhoneNumberUtils;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by vijayaganesh on 3/26/2018.
 */

public class OtpFragment extends BaseFragment {

    @BindView(R.id.view_top)
    View viewTop;
    @BindView(R.id.image_app_icon)
    ImageView imageAppIcon;
    @BindView(R.id.text_label_otp)
    TextView textLabelCountry;
    //    @BindView(R.id.country_spinner)
    @BindView(R.id.edit_otp_login_phone_number)
    EditText editOtpLoginPhoneNumber;
    //    CountrySpinner countrySpinner;
    @BindView(R.id.button_otp)
    Button buttonOtp;
    @BindView(R.id.cons_otp)
    ConstraintLayout consOtp;
    Unbinder unbinder;
    @BindView(R.id.edit_country_code)
    EditText editCountryCode;
    private String mCountryIso;
    LoginActivity loginActivity;
    private static final String SHOWCASE_ID = "SHow12345";
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;
    private UserStatusResponse userStatusResponse;
    private SharedPreferences fcmSharedPrefrences;
    private UserDetailsResponse userDetailsResponse;
    private TelephonyManager telephonyManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp, container, false);
        DmkApplication.getContext().getComponent().inject(this);
        unbinder = ButterKnife.bind(this, view);
        loginActivity = (LoginActivity) getActivity();
        mCountryIso = PhoneNumberUtils.getDefaultCountryIso(getActivity());
        fcmSharedPrefrences = getActivity().getSharedPreferences(DmkConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editor = sharedPreferences.edit();
//        final String defaultCountryName = new Locale("", mCountryIso).getDisplayName();
//        countrySpinner.init(defaultCountryName);
//        countrySpinner.addCountryIsoSelectedListener(new CountrySpinner.CountryIsoSelectedListener() {
//            @Override
//            public void onCountryIsoSelected(String selectedIso) {
//                if (selectedIso != null) {
//                    mCountryIso = selectedIso;
//                }
//            }
//        });

//        String encrpytData = Java_AES_Cipher.encrypt("dwlr2oEsmUIk", "EjnavfwyEESgRGLg", "zOFXD8kprWjvppVK6D94");
//        byte[] dataBase64Encode = encrpytData.getBytes(StandardCharsets.UTF_8);
//        String base64 = Base64.encodeToString(dataBase64Encode, Base64.NO_WRAP);


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button_otp)
    public void onViewClicked() {
        telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) !=
                PackageManager.PERMISSION_GRANTED) {
            loginActivity.requestPermission();
            return;
        }
        editor.putString(DmkConstants.IMEIID, telephonyManager.getDeviceId()).commit();

        if (editOtpLoginPhoneNumber.getText().toString().isEmpty() ||
                editOtpLoginPhoneNumber.getText().toString().length() < 10) {
            editOtpLoginPhoneNumber.setError(getString(R.string.phone_number_missing));
        } else {
            isUserExist();
        }
    }


    private void openActivity(String phoneNumber, boolean isForRegistration) {
        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
        Bundle bundle = new Bundle();
        bundle.putString("phoneNumber", phoneNumber);
//        bundle.putString("countryCode", Iso2Phone.getPhone(mCountryIso));
        bundle.putString("countryCode", "91");
        bundle.putBoolean("isForRegistration", isForRegistration);
        otpVerificationFragment.setArguments(bundle);
        loginActivity.push(otpVerificationFragment, getString(R.string.otp_verify));
    }

    private String getE164Number() {
        return editOtpLoginPhoneNumber.getText().toString().replaceAll("\\D", "").trim();
        // return PhoneNumberUtils.formatNumberToE164(mPhoneNumber.getText().toString(), mCountryIso);
    }

    private void isUserExist() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            final UserStatusParam userStatusParam = new UserStatusParam();
            userStatusParam.setPhoneNumber(editOtpLoginPhoneNumber.getText().toString());
            userStatusParam.setAppversion(sharedPreferences.getString(DmkConstants.APPVERSION_CODE, ""));
//            1-android 2-ios
            userStatusParam.setApptype(1);
            userStatusParam.setOsversion(String.valueOf(sharedPreferences.getInt(DmkConstants.OS_VERSION, 0)));
            userStatusParam.setImeiid(sharedPreferences.getString(DmkConstants.IMEIID, ""));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getUserStatus(headerMap, userStatusParam).enqueue(new Callback<UserStatusResponse>() {
                @Override
                public void onResponse(Call<UserStatusResponse> call, Response<UserStatusResponse> response) {
                    hideProgress();
                    userStatusResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {

                        switch (userStatusResponse.getLoginstatus()) {
                            case 1:
//                                Old user && IMEIID matched
                                editor.putInt(DmkConstants.USERID, userStatusResponse.getResults().getUserid());
                                editor.putString(DmkConstants.HEADER_SOURCE, userStatusResponse.getResults().getSource());
                                editor.putString(DmkConstants.HEADER_SOURCE_DATA, userStatusResponse.getResults().getSourcedata());
                                editor.putString(DmkConstants.HEADER, getEncodedHeader(userStatusResponse.getResults().getSource(),
                                        userStatusResponse.getResults().getSourcedata()));
                                editor.commit();
                                getUserDetails();
                                break;
                            case 0:
                            case 2:
//                                Old user && IMEIID not matched
//                                New user && IMEIID not matched
                                alertDialog(getString(R.string.Unauthorized_login),1);
                                break;
                            case 3:
//                                New user
                                openActivity(getE164Number(), true);
                                break;
                            case 4:
//                                App verison not matched
                                alertDialog(getString(R.string.App_Update_mes),2);
                                break;
                            case 5:
                                //Old user && IMEIID is 0 or ""
                                editor.putInt(DmkConstants.USERID, userStatusResponse.getResults().getUserid());
                                editor.putString(DmkConstants.HEADER_SOURCE, userStatusResponse.getResults().getSource());
                                editor.putString(DmkConstants.HEADER_SOURCE_DATA, userStatusResponse.getResults().getSourcedata());
                                editor.putString(DmkConstants.HEADER, getEncodedHeader(userStatusResponse.getResults().getSource(),
                                        userStatusResponse.getResults().getSourcedata()));
                                editor.commit();
                                openActivity(getE164Number(), false);
                                break;
                        }

                    } else {
                        alertDialog(getString(R.string.Unauthorized_login),1);
                    }
                }

                @Override
                public void onFailure(Call<UserStatusResponse> call, Throwable t) {
                    hideProgress();

                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void alertDialog(String title, final int type) {
        final android.support.v7.app.AlertDialog.Builder alertDialog1 =
                new android.support.v7.app.AlertDialog.Builder(getActivity());
        alertDialog1.setTitle(getString(R.string.app_title_dmk));
        alertDialog1.setMessage(title);
        alertDialog1.setCancelable(false);
        alertDialog1.setPositiveButton(getString(R.string.Alert_Ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (type!=1)
                    navigateToPlayStore();
            }
        });
        alertDialog1.show();
    }

    private void navigateToPlayStore(){
        final String appPackageName = getActivity().getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.play_store_url) + appPackageName)));
        }
    }


    private void getUserDetails() {
        if (Util.isNetworkAvailable()) {
//            showProgress();
            UserStatusParam userStatusParam = new UserStatusParam();
//            userStatusParam.setPhoneNumber(editOtpLoginPhoneNumber.getText().toString());
            userStatusParam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            userStatusParam.setDeviceToken(fcmSharedPrefrences.getString(DmkConstants.FCMTOKEN, ""));
            userStatusParam.setImeiid(sharedPreferences.getString(DmkConstants.IMEIID, ""));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getUserDetails(headerMap, userStatusParam).enqueue(new Callback<UserDetailsResponse>() {
                @Override
                public void onResponse(Call<UserDetailsResponse> call, Response<UserDetailsResponse> response) {
                    hideProgress();
                    userDetailsResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (userDetailsResponse.getStatuscode() == 0) {
                            if (userDetailsResponse.getResults() != null) {
                                editor.putString(DmkConstants.FIRSTNAME, userDetailsResponse.getResults().getFirstName());
                                editor.putString(DmkConstants.LASTNAME, userDetailsResponse.getResults().getLastName());
                                editor.putString(DmkConstants.MOBILENUMBER, userDetailsResponse.getResults().getPhoneNumber());
                                editor.putString(DmkConstants.PHONENUMBER, userDetailsResponse.getResults().getPhoneNumber());
                                editor.putInt(DmkConstants.USERID, userDetailsResponse.getResults().getId());
                                editor.putString(DmkConstants.USERDISTRICTNAME, userDetailsResponse.getResults().getDistrictName());
                                editor.putString(DmkConstants.USERWINGNAME, userDetailsResponse.getResults().getWings().getWingName());
                                editor.putString(DmkConstants.DESGINATION, userDetailsResponse.getResults().getRoles().getRolename());
                                editor.putString(DmkConstants.UNIQUEDMKID, userDetailsResponse.getResults().getUniquieID());
                                editor.putInt(DmkConstants.USERAGE, userDetailsResponse.getResults().getAge());
                                editor.putInt(DmkConstants.SUPERIORID, userDetailsResponse.getResults().getSuperiorUserID());
                                editor.putInt(DmkConstants.USER_DISTRICT_ID, userDetailsResponse.getResults().getDistrictID());
                                editor.putInt(DmkConstants.USER_PARTY_DISTRICT_ID, userDetailsResponse.getResults().getPartydistrict());
                                editor.putInt(DmkConstants.USER_CONSTITUENCY_ID, userDetailsResponse.getResults().getConstituencyID());
                                editor.putInt(DmkConstants.USER_DIVISION_ID, userDetailsResponse.getResults().getTypeid());
                                editor.putInt(DmkConstants.USER_PART_ID, userDetailsResponse.getResults().getPartid());
                                //editor.putInt(DmkConstants.USER_VATTAM_ID, userDetailsResponse.getResults().getVattamid());
                                editor.putInt(DmkConstants.USER_WARD_ID, userDetailsResponse.getResults().getWardID());
                                editor.putInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, userDetailsResponse.getResults().getVillagepanchayat());
                                editor.putInt(DmkConstants.USER_PANCHAYAT_UNION_ID, userDetailsResponse.getResults().getPanchayatunion());
                                editor.putInt(DmkConstants.USER_TOWNSHIP_ID, userDetailsResponse.getResults().getTownshipid());
                                editor.putInt(DmkConstants.USER_BOOTH_ID, userDetailsResponse.getResults().getBoothID());
                                editor.putInt(DmkConstants.DESIGNATIONID, userDetailsResponse.getResults().getRoles().getLevel());
                                editor.putInt(DmkConstants.ROLL_ID, userDetailsResponse.getResults().getRoles().getId());
                                editor.putInt(DmkConstants.USERTTYPE, userDetailsResponse.getResults().getUserType());
                                editor.putString(DmkConstants.PROFILE_PICTURE, userDetailsResponse.getResults().getAvatar());
                                editor.putString(DmkConstants.USERPERMISSION_COMMON, userDetailsResponse.getResults().getUserPermissionset().getCommon());
                                editor.putString(DmkConstants.USERPERMISSION_ARTICLE, userDetailsResponse.getResults().getUserPermissionset().getArticle());
                                editor.putString(DmkConstants.USERPERMISSION_EVENTS, userDetailsResponse.getResults().getUserPermissionset().getEvents());
                                editor.putString(DmkConstants.USERPERMISSION_NEWS, userDetailsResponse.getResults().getUserPermissionset().getNews());
                                editor.putString(DmkConstants.USERPERMISSION_ELECTION, userDetailsResponse.getResults().getUserPermissionset().getElection());
                                editor.putString(DmkConstants.USER_VOTER_ID, userDetailsResponse.getResults().getVoterID());
                                editor.putString(DmkConstants.USER_ADDRESS, userDetailsResponse.getResults().getAddress());
                                editor.putInt(DmkConstants.USER_UNION_TYPE_ID, userDetailsResponse.getResults().getUniontype());
                                editor.commit();
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                if (getActivity().getIntent().getStringExtra("timeLineID") != null)
                                    intent.putExtra("timeLineID", getActivity().getIntent().getStringExtra("timeLineID"));
                                else if (getActivity().getIntent().getStringExtra("newsID") != null)
                                    intent.putExtra("newsID", getActivity().getIntent().getStringExtra("newsID"));
                                else if (getActivity().getIntent().getStringExtra("eventID") != null)
                                    intent.putExtra("eventID", getActivity().getIntent().getStringExtra("eventID"));
                                else if (getActivity().getIntent().getStringExtra("articleID") != null)
                                    intent.putExtra("articleID", getActivity().getIntent().getStringExtra("articleID"));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                alertDialog(getString(R.string.Unauthorized_login),1);
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, userDetailsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, userDetailsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(userDetailsResponse.getSource(),
                                    userDetailsResponse.getSourcedata()));
                            editor.commit();
                            getUserDetails();
                        }
                    } else {
                        Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<UserDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


}
