package com.dci.dmkitwings.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.fragment.CommentFragment;
import com.dci.dmkitwings.utils.LanguageHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentActivity extends BaseActivity {

    @BindView(R.id.frame_comment_container)
    FrameLayout frameCommentContainer;
    @BindView(R.id.cons_comment_ac)
    ConstraintLayout consCommentAc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_comment, null, false);
        ButterKnife.bind(this, contentView);
        mDrawerLayout.addView(contentView, 0);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        textTitle.setText(R.string.comment);
        frameLayoutNotification.setVisibility(View.GONE);
        //Navigation Control
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        push(new CommentFragment());
    }


    public void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void popAllBackstack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();
        if (backCount > 0) {
            fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_comment_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_comment_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_comment_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_comment_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }

    public void pushWithAnimation(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        pushWithAnimation(fragment, null);
    }

    public void pushWithAnimation(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_comment_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_comment_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager
                        .beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_comment_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_comment_container, fragment, tag).commitAllowingStateLoss();
            }
        }
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}
