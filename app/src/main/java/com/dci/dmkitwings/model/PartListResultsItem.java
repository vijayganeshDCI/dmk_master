package com.dci.dmkitwings.model;

public class PartListResultsItem {
	public PartListResultsItem(String partname, int id) {
		this.partname = partname;
		this.id = id;
	}

	public String getPartname() {
		return partname;
	}

	public void setPartname(String partname) {
		this.partname = partname;
	}

	public int getDistrictid() {
		return districtid;
	}

	public void setDistrictid(int districtid) {
		this.districtid = districtid;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getConstituencyid() {
		return constituencyid;
	}

	public void setConstituencyid(int constituencyid) {
		this.constituencyid = constituencyid;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPartdescription() {
		return partdescription;
	}

	public void setPartdescription(String partdescription) {
		this.partdescription = partdescription;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	private String partname;
	private int districtid;
	private String updated_at;
	private int constituencyid;
	private String created_at;
	private int id;
	private String partdescription;
	private int status;
}
