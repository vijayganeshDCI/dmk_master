package com.dci.dmkitwings.model;

public class TimelineDetailsDistrict
{
    private int id;
    private String DistrictName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }
}
