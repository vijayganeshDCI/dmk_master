package com.dci.dmkitwings.fragment;

import android.animation.ValueAnimator;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.util.IOUtils;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.model.UserError;
import com.dci.dmkitwings.utils.Java_AES_Cipher;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.enums.EPickType;
import com.wooplr.spotlight.SpotlightConfig;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vijayaganesh on 10/17/2017.
 */

public class BaseFragment extends Fragment {

    private static final float PICTURE_SIZE = 640;
    String blockCharacterSet = "~#^|$%&*!)(?/-+@:,.;'=_1234567890#{}[]";
    public AmazonS3Client s3Client;
    public BasicAWSCredentials credentials;
    public TransferUtility transferUtility;
    public String[] imageFormat = {"tif", "tiff", "gif", "jpeg", "jpg", "jif", "jfif"
            , "jp2", "jpx", "j2k", "j2c"
            , "fpx", "pcd", "png", "pdf"};
    public String[] audioFormat = {"3gp", "aa", "aac", "aax", "act", "aiff", "amr", "ape", "au", "awb"
            , "amr", "ape", "au", "awb", "dct", "dss", "dvf", "dvf", "flac", "gsm", "iklax", "ivs", "m4a", "m4b",
            "m4p", "mmf", "mp3", "mpc", "msv", "nsf", "ogg", "opus", "ra", "raw", "sln", "tta"
            , "vox", "wav", "wma", "webm", "8svx"};
    public String[] audioimageFormat = {"tif", "tiff", "gif", "jpeg", "jpg", "jif", "jfif"
            , "jp2", "jpx", "j2k", "j2c"
            , "fpx", "pcd", "png", "pdf", "aa", "aac", "aax", "act", "aiff", "amr", "ape", "au", "awb"
            , "amr", "ape", "au", "awb", "dct", "dss", "dvf", "dvf", "flac", "gsm", "iklax", "ivs", "m4a", "m4b",
            "m4p", "mmf", "mp3", "mpc", "msv", "nsf", "ogg", "opus", "ra", "raw", "sln", "tta"
            , "vox", "wav", "wma", "webm", "8svx"};

    private final String expression = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";


    SpotlightConfig config;
    private boolean isRevealEnabled = true;
    private PickSetup setup;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        s3SetUp();
        showCaseViewSetUp();
    }

    private void showCaseViewSetUp() {
        config = new SpotlightConfig();
        config.setIntroAnimationDuration(400);
        config.setRevealAnimationEnabled(isRevealEnabled);
        config.isPerformClick();
        config.setFadingTextDuration(400);
        config.setHeadingTvColor(getResources().getColor(R.color.yellow));
        config.setHeadingTvSize(32);
        config.setSubHeadingTvSize(16);
        config.setSubHeadingTvColor(getResources().getColor(R.color.white));
        config.setMaskColor(getResources().getColor(R.color.trans_black));
        config.setLineAnimationDuration(400);
        config.setLineAndArcColor(getResources().getColor(R.color.red));
        config.setDismissOnBackpress(true);
        config.setDismissOnTouch(true);
    }

    private void s3SetUp() {
        credentials = new BasicAWSCredentials(getString(R.string.aws_access_key),
                getString(R.string.aws_secret_key));
        s3Client = new AmazonS3Client(credentials);
        transferUtility =
                TransferUtility.builder()
                        .context(getActivity())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int stringId) {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.showProgress(stringId);
        }
    }

    public void hideProgress() {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.hideProgress();
        }
    }

    public void showError(int errorType, UserError error) {
        ((BaseActivity) getActivity()).showError(errorType, error);
    }

    public String parseOrderDate(long date) {
//        convert requried date format
        SimpleDateFormat mSdf = new SimpleDateFormat("yyyy-MM-dd");
//        getCurrentDate
        Calendar currentDate = Calendar.getInstance();
//        getDayBeforDate
        Calendar dayBeforeDate = Calendar.getInstance();
        dayBeforeDate.roll(Calendar.DATE, -1);
//        order date
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
//        setOrderDate in Calendar
        Calendar orderPlacedCalendar = Calendar.getInstance();
        Date orderPlacedDate = null;
        orderPlacedDate = calendar.getTime();
        orderPlacedCalendar.setTime(orderPlacedDate);
//        compare dates
        if (isSameDay(currentDate, orderPlacedCalendar)) {
            return "Today, " + mSdf.format(orderPlacedDate);
        } else if (isSameDay(dayBeforeDate, orderPlacedCalendar)) {
            return "Yesterday, " + mSdf.format(orderPlacedDate);
        } else {
            return "" + mSdf.format(orderPlacedDate);
        }
    }

    private boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public boolean onBackPressed() {
        return false;
    }

    public static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    public String getBase64Image(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                new RectF(0, 0, PICTURE_SIZE, PICTURE_SIZE),
                Matrix.ScaleToFit.CENTER);
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] bytes = stream.toByteArray();
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public InputFilter editTextRestrictSmileys() {

        InputFilter restrictSmileys = new InputFilter() {

            public CharSequence filter(CharSequence src, int start,
                                       int end, Spanned dst, int dstart, int dend) {
                if (src.equals("")) { // for backspace
                    return src;
                }
                if (src.toString().matches("[\\x00-\\x7F]+")) {
                    return src;
                }
                return "";
            }
        };
        return restrictSmileys;
    }

    public InputFilter editTextRestrictLength() {

        InputFilter setLength = new InputFilter.LengthFilter(30);
        return setLength;
    }

    public InputFilter editTextEmailRestrictLength() {

        InputFilter setLength = new InputFilter.LengthFilter(30);
        return setLength;
    }


    public InputFilter editTextRestrictSpecialCharacters() {
        InputFilter restrictSpecialCharacter = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                if (source != null && blockCharacterSet.contains(("" + source))) {
                    return "";
                }
                return null;
            }
        };
        return restrictSpecialCharacter;
    }

    public void startCountAnimation(int count, final TextView textView) {
        ValueAnimator animator = ValueAnimator.ofInt(0, count);
        animator.setDuration(100);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                textView.setText(animation.getAnimatedValue().toString());
            }
        });
        animator.start();
    }

    public String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);

        String ageS = ageInt.toString();

        return ageS;
    }

    //    public void picasoImageLoader(ImageView imageView, String url, Context context) {
//        Picasso.with(context)
//                .load(getString(R.string.falcon_image_url) + url)
//                .placeholder(R.mipmap.img_sample_pro_pic)   // optional
////                                    .error(R.drawable.ic_error_fallback)      // optional
////                                    .resize(250, 200)                        // optional
////                                    .rotate(90)                             // optional
//                .into(imageView);
//    }
//
    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyURIImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyURIImage)
                .showImageOnFail(emptyURIImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(getString(R.string.s3_image_url_download) + s3bucketName + image, imageView, options);
    }

    //For Single Images
    public static void compressInputImage(Intent data, Context context, ImageView newIV) {
        Bitmap bitmap;
        Uri inputImageData = data.getData();
        try {
            Bitmap bitmapInputImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), inputImageData);
            if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1920, 1200, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, bitmapInputImage.getWidth(), bitmapInputImage.getHeight(), true);
                newIV.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    //For Multiple Images
    public static Bitmap compressInputImage(Uri uri, Context context) {
        Bitmap bitmap = null;
        try {
            Bitmap bitmapInputImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
            } else if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1920, 1200, true);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, bitmapInputImage.getWidth(), bitmapInputImage.getHeight(), true);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }
        return bitmap;
    }

    // UPDATED!
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }


    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd-MMM-yyyy hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getDateFormatWitoutTime(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd-MMM-yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    //s3 upload image
    public void uploadFile(Uri uri, String s3bucketPath, String imageName, boolean isAudioFromRecord) {
        if (uri != null) {
            final File destination;
            if (!isAudioFromRecord)
                destination = new File(getActivity().getExternalFilesDir(null), imageName);
            else
                destination = new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString() +
                        "/Voice Recorder/RECORDING_"
                        + imageName);
            createFile(getActivity(), uri, destination);
            transferUtility =
                    TransferUtility.builder()
                            .context(getActivity())
                            .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                            .s3Client(s3Client)
                            .build();
            TransferObserver uploadObserver =
                    transferUtility.upload(s3bucketPath + imageName, destination);
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.COMPLETED == state) {
                        destination.delete();
                    } else if (TransferState.FAILED == state) {
                        destination.delete();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                }

                @Override
                public void onError(int id, Exception ex) {
                    ex.printStackTrace();
                }

            });
        }
    }

    //to get path from uri
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    // to get URI from bitmap
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage
                (inContext.getContentResolver(), inImage, "Title", null);
        return path != null ? Uri.parse(path) : null;
    }

    //to get file name from URI
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    // to get file type
    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    // to create file
    public void createFile(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isAudioFile(String fileNamePath) {
        for (int i = 0; i < audioFormat.length; i++) {
            if (getFileExt(fileNamePath).equalsIgnoreCase(audioFormat[i])) {
                return true;
            }
        }
        return false;
    }

//    public static boolean isAudioFile(String path) {
//        String mimeType = URLConnection.guessContentTypeFromName(path);
//        return mimeType != null && mimeType.endsWith(".mp3");
//    }

    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }


    public boolean isImageFile(String fileNamePath) {
        for (int i = 0; i < imageFormat.length; i++) {
            if (getFileExt(fileNamePath).equalsIgnoreCase(imageFormat[i])) {
                return true;
            }
        }
        return false;
    }

    public boolean getFileSize(Uri file) {
        File path = null;

        path = new File(file.getPath());


        long expectedSizeInMB = 20;
        long expectedSizeInBytes = 1024 * 1024 * expectedSizeInMB;

        long sizeInBytes = -1;
        sizeInBytes = path.length();

        if (sizeInBytes > expectedSizeInBytes) {
            System.out.println("Bigger than " + expectedSizeInMB + " MB");
            return false;
        } else {
            System.out.println("Not bigger than " + expectedSizeInMB + " MB");
            return true;
        }

    }

    //get Image Path
    public String getImagePath(Uri uri) {
        String path = null;
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();

            cursor = getActivity().getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();


        }

        return path;
    }

    public String getEncodedHeader(String sourceResponse, String sourceDataResponse) {

        if (sourceResponse != null && sourceDataResponse != null) {
            byte[] sourceBase64 = Base64.decode(sourceResponse, Base64.NO_WRAP);
            byte[] sourceDataBase64 = Base64.decode(sourceDataResponse, Base64.NO_WRAP);
            String source = new String(sourceBase64, StandardCharsets.UTF_8);
            String sourceData = new String(sourceDataBase64, StandardCharsets.UTF_8);
            String data = source.substring(0, 12) + sourceData.substring(0, 8);
            String key = source.substring(12, 19) + sourceData.substring(8, 13);
            String iv = source.substring(19, 28) + sourceData.substring(13, 20);
            String encrpytData = Java_AES_Cipher.encrypt(key, iv, data);
            byte[] dataBase64Encode;
            String base64 = null;
            if (encrpytData != null) {
                dataBase64Encode = encrpytData.getBytes(StandardCharsets.UTF_8);
                base64 = Base64.encodeToString(dataBase64Encode, Base64.NO_WRAP);
            }
            return base64;
        }
        return null;
    }

    private void playYouTubeVideo(final String videoUrl, YouTubePlayerView youTubePlayerView) {
        final String videoID = getVideoId(videoUrl);
        getActivity().getLifecycle().addObserver(youTubePlayerView);
        youTubePlayerView.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        if (videoID.length() > 0) {
                            initializedYouTubePlayer.loadVideo(videoID, 0);
                        }

                    }
                });
            }
        }, true);
    }

    public String getVideoId(String videoUrl) {
        if (videoUrl == null || videoUrl.trim().length() <= 0) {
            return null;
        }
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(videoUrl);
        try {
            if (matcher.find())
                return matcher.group();
        } catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
