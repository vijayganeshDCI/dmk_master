package com.dci.dmkitwings.model;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by vijayaganesh on 4/5/2018.
 */

public class SelectedImage {

    public SelectedImage(Uri selectedVideo, int mediaType, Bitmap selectedImage,
                         boolean isForEdit,String imageName) {
        this.selectedVideo = selectedVideo;
        this.mediaType = mediaType;
        this.selectedImage = selectedImage;
        this.isForEdit = isForEdit;
        this.imageName=imageName;
    }

    public Uri getSelectedVideo() {
        return selectedVideo;
    }

    public void setSelectedVideo(Uri selectedVideo) {
        this.selectedVideo = selectedVideo;
    }

    private Uri selectedVideo;
    private int mediaType;
    private Bitmap selectedImage;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    private String imageName;

    public boolean isForEdit() {
        return isForEdit;
    }

    public void setForEdit(boolean forEdit) {
        isForEdit = forEdit;
    }

    private boolean isForEdit;




    public int getMediaType() {
        return mediaType;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }


    public Bitmap getSelectedImage() {
        return selectedImage;
    }

    public void setSelectedImage(Bitmap selectedImage) {
        this.selectedImage = selectedImage;
    }
}
