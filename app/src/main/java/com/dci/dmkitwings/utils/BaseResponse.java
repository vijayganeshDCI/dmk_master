package com.dci.dmkitwings.utils;

import com.dci.dmkitwings.model.UserDetailResults;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BaseResponse<T>
{
    @SerializedName("Status")
    @Expose
    private String ack;

    @SerializedName("Results")
    @Expose
    private UserDetailResults Results;

    @SerializedName("Data")
    @Expose
    private T data;

    /**
     *
     * @return
     * The ack
     */
    public String getAck() {
        return ack;
    }

    /**
     *
     * @param ack
     * The Ack
     */
    public void setAck(String ack) {
        this.ack = ack;
    }

    public UserDetailResults getResults() {
        return Results;
    }

    public void setResults(UserDetailResults results) {
        Results = results;
    }

    /**
     *
     * @return
     * The message



    /**
     *
     * @return
     * The data
     */
    public T getData() {
        return data;
    }

    /**
     *
     * @param data
     * The Data
     */
    public void setData(T data) {
        this.data = data;
    }
}
