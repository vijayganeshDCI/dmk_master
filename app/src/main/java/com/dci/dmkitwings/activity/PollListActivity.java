package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.PollsAdapter;
import com.dci.dmkitwings.adapter.PollsAdapterTypeTwo;
import com.dci.dmkitwings.adapter.PollsResultAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.PollDetailsResponse;
import com.dci.dmkitwings.model.PollSaveParams;
import com.dci.dmkitwings.model.PollSaveResponse;
import com.dci.dmkitwings.model.PolldetailsParams;
import com.dci.dmkitwings.model.Polls;
import com.dci.dmkitwings.model.QuestionslistItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PollListActivity extends BaseActivity {
    @BindView(R.id.list_polls)
    ListView listPolls;
    @BindView(R.id.cons_polls)
    ConstraintLayout consPolls;
    @BindView(R.id.button_pollsubmit)
    Button buttonPollSubmit;
    PollsAdapter pollsAdapter;
    PollsAdapterTypeTwo pollsAdapterTypeTwo;
    List<Polls> pollsList;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    PollSaveResponse pollSaveResponse;
    ArrayList<QuestionslistItem> pollListResultsItemArrayList;
    ArrayList<QuestionslistItem> pollListResultsItemArrayList1;
    int pollid;
    String pollResult;
    PollDetailsResponse pollDetailsResponse;
    int PollType;
    @BindView(R.id.text_Poll_title)
    TextView textPolltitle;
    @BindView(R.id.view_top)
    View viewTop;
    @BindView(R.id.image_no_network)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_network)
    CustomTextView textNoNetwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.fragment_pollslist, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        mDrawerLayout.addView(contentView, 0);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        textTitle.setText(R.string.polling);
        frameLayoutNotification.setVisibility(View.GONE);
        //Navigation Control
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        buttonPollSubmit.setVisibility(View.VISIBLE);
        Intent bundle = getIntent();
        if (getIntent() != null) {
            pollid = bundle.getIntExtra("pollid", 0);
            //PollType = bundle.getIntExtra("PollType", 0);
            //pollListResultsItemArrayList= (ArrayList<PollQuestionslistItem>) bundle.getSerializableExtra("polllist");;
        }
        if (Util.isNetworkAvailable()) {
            getPollDetails();
        }


    }

    @OnClick(R.id.button_pollsubmit)
    public void onViewClicked(View view) {

        if (PollType == 1) {
            //Radio
            if (pollsAdapter.questions.size() == pollListResultsItemArrayList.size()) {
                savePolllist();
            } else {
                Toast.makeText(PollListActivity.this, getString(R.string.ansswer_all_polls), Toast.LENGTH_SHORT).show();
            }
        } else if (PollType == 2) {
            //CheckBox
            if (pollsAdapterTypeTwo.TEST.size() == pollListResultsItemArrayList.size()) {
                savePolllist();
            } else {
                Toast.makeText(PollListActivity.this, getString(R.string.ansswer_all_polls), Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void getPollDetails() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            final PolldetailsParams polldetailsParams = new PolldetailsParams();
            polldetailsParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            polldetailsParams.setPollid(pollid);
            pollListResultsItemArrayList = new ArrayList<QuestionslistItem>();
            pollListResultsItemArrayList.clear();
            pollListResultsItemArrayList1 = new ArrayList<QuestionslistItem>();
            pollListResultsItemArrayList1.clear();
            //buttonPollSubmit.setVisibility(View.GONE);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));

            dmkAPI.getPolldetailsListBystring(headerMap, polldetailsParams).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    hideProgress();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        System.out.println("user Info :" + response.body().toString());
                        pollResult = response.body().toString();
                        try {
                            JSONObject obj = new JSONObject(pollResult);
                            if (!obj.has("Statuscode")) {

                                    JSONArray resultsJsonarray = obj.getJSONArray("Results");
                                    int pollcomplete = 0;
                                    for (int s = 1; s <= 1; s++) {
                                        JSONObject resultfinderJsonobject = resultsJsonarray.getJSONObject(s);
                                        JSONArray resultfinderJsonarray = resultfinderJsonobject.getJSONArray("resultfinder");
                                        for (int j = 0; j < resultfinderJsonarray.length(); j++) {
                                            JSONObject resultfinderJson = resultfinderJsonarray.getJSONObject(j);
                                            pollcomplete = resultfinderJson.getInt("pollcomplete");
                                            JSONArray questions = resultfinderJson.getJSONArray("questions");

                                            ArrayList<String> answerkeys = new ArrayList<>();
                                            JSONObject test = null;
                                            for (int js = 0; js < questions.length(); js++) {
                                                test = questions.getJSONObject(js);
                                                Iterator<String> keys = test.keys();

                                                while (keys.hasNext()) {
                                                    String key = keys.next();
                                                    answerkeys.add(key);


                                                }
                                            }

                                            for (int js = 0; js < answerkeys.size(); js++) {
                                                QuestionslistItem questionslistItem = new QuestionslistItem();
                                                //questionslistItem.setQuestion(answerkeys.get(js));
                                                System.out.println("answerkeys :" + answerkeys.get(js));
                                                for (int jas = 0; jas < 1; jas++) {
                                                    JSONObject anserOptions = questions.getJSONObject(jas);
                                                    //System.out.println("answerkeys-Options :" + moviesone.getJSONObject(answerkeys.get(js)).getString("Option2"));
                                                    if (anserOptions.getJSONObject(answerkeys.get(js)).has("Option1")) {
                                                        System.out.println("Options1 :" + anserOptions.getJSONObject(answerkeys.get(js)).getString("Option1"));
                                                        questionslistItem.setOptionOnePer(anserOptions.getJSONObject(answerkeys.get(js)).getString("Option1"));

                                                    } else {
                                                        questionslistItem.setOptionOnePer("0");

                                                    }
                                                    if (anserOptions.getJSONObject(answerkeys.get(js)).has("Option2")) {
                                                        System.out.println("Options2 :" + anserOptions.getJSONObject(answerkeys.get(js)).getString("Option2"));
                                                        questionslistItem.setOptionTwoPer(anserOptions.getJSONObject(answerkeys.get(js)).getString("Option2"));

                                                    } else {
                                                        questionslistItem.setOptionTwoPer("0");

                                                    }

                                                    if (anserOptions.getJSONObject(answerkeys.get(js)).has("Option3")) {
                                                        System.out.println("Options3 :" + anserOptions.getJSONObject(answerkeys.get(js)).getString("Option3"));
                                                        questionslistItem.setOptionThreePer(anserOptions.getJSONObject(answerkeys.get(js)).getString("Option3"));

                                                    } else {
                                                        questionslistItem.setOptionThreePer("0");

                                                    }
                                                    if (anserOptions.getJSONObject(answerkeys.get(js)).has("Option4")) {
                                                        System.out.println("Options4 :" + anserOptions.getJSONObject(answerkeys.get(js)).getString("Option4"));
                                                        questionslistItem.setOptionFourPer(anserOptions.getJSONObject(answerkeys.get(js)).getString("Option4"));

                                                    } else

                                                    {
                                                        questionslistItem.setOptionFourPer("0");

                                                    }

                                                    pollListResultsItemArrayList1.add(questionslistItem);


                                                }


                                            }


                                        }

                                    }

                                    for (int s = 0; s < 1; s++) {

                                        JSONObject questionListJsonObject = resultsJsonarray.getJSONObject(s);
                                        textPolltitle.setText(questionListJsonObject.getString("Name"));
                                        PollType = questionListJsonObject.getInt("Polltype");
                                        JSONArray questionListJsonArray = questionListJsonObject.getJSONArray("questionslist");
                                        // Log.d("My App","characters"+characters);
                                        for (int j = 0; j < questionListJsonArray.length(); j++) {
                                            // temp.add(characters.getString(j));
                                            JSONObject questionListObject = questionListJsonArray.getJSONObject(j);
                                            QuestionslistItem questionslistItem = new QuestionslistItem();
                                            questionslistItem.setId(questionListObject.getInt("id"));
                                            questionslistItem.setPollID(questionListObject.getInt("PollID"));
                                            questionslistItem.setQuestion(questionListObject.getString("Question"));
                                            questionslistItem.setOption1(questionListObject.getString("Option1"));
                                            questionslistItem.setOption2(questionListObject.getString("Option2"));
                                            questionslistItem.setOption3(questionListObject.getString("Option3"));
                                            questionslistItem.setOption4(questionListObject.getString("Option4"));
                                            pollListResultsItemArrayList.add(questionslistItem);

                                        }


                                    }


                                    if (pollcomplete == 0) {

                                        if (PollType == 1) {
                                            pollsAdapter = new PollsAdapter(pollListResultsItemArrayList, getApplicationContext());
                                            listPolls.setAdapter(pollsAdapter);

                                        } else if (PollType == 2) {
                                            pollsAdapterTypeTwo = new PollsAdapterTypeTwo(pollListResultsItemArrayList, getApplicationContext());
                                            listPolls.setAdapter(pollsAdapterTypeTwo);

                                        }
                                    } else if (pollcomplete == 1) {
                                        buttonPollSubmit.setVisibility(View.GONE);
                                        PollsResultAdapter pollsResultAdapter;
                                        pollsResultAdapter = new PollsResultAdapter(pollListResultsItemArrayList, pollListResultsItemArrayList1, getApplicationContext());
                                        listPolls.setAdapter(pollsResultAdapter);
                                        //pollsAdapter=new PollsAdapter(pollListResultsItemArrayList,getApplicationContext());
                                        //listPolls.setAdapter(pollsAdapter);
                                        //Toast.makeText(PollListActivity.this,"Already poll..",Toast.LENGTH_SHORT).show();
                                    }
                                }

                            else {
//                                if key status code there as 1
                                if (obj.getInt("Statuscode") == 1) {
                                    editor.putString(DmkConstants.HEADER_SOURCE, obj.getString("source"));
                                    editor.putString(DmkConstants.HEADER_SOURCE_DATA, obj.getString("sourcedata"));
                                    editor.putString(DmkConstants.HEADER, getEncodedHeader(obj.getString("source"),
                                            obj.getString("sourcedata")));
                                    editor.commit();
                                    getPollDetails();
                                }
                            }

                        } catch (Throwable tx) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + pollResult + "\"");
                        }
                    } else {
                        System.out.println("Error :");
                    }

                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(PollListActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            imageNoNetwork.setVisibility(View.VISIBLE);
            textNoNetwork.setVisibility(View.VISIBLE);
            imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
            textNoNetwork.setText(R.string.no_network);
            listPolls.setVisibility(View.GONE);
        }

    }

    private void savePolllist() {
        showProgress();
        HashMap<Integer, String> questionsTypeTwo = new HashMap<>();
        final PollSaveParams pollSaveParams = new PollSaveParams();
        pollSaveParams.setPollid(pollid);
        pollSaveParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (PollType == 1) {
            pollSaveParams.setQuestions(pollsAdapter.questions);
        } else if (PollType == 2) {
            for (Map.Entry<Integer, ArrayList<String>> entry : pollsAdapterTypeTwo.TEST.entrySet()) {
                Integer key = entry.getKey();
                List<String> values = entry.getValue();
                //System.out.println("Key = " + key);
                //System.out.println("Values = " + values + "n");
                HashSet<String> hashSet = new HashSet<String>();
                hashSet.addAll(values);
                values.clear();
                values.addAll(hashSet);
                hashSet.clear();
                String granddTotal = new String();

                for (String s : values) {
                    granddTotal += s + ",";
                }
                questionsTypeTwo.put(key, granddTotal);
                System.out.println(key + granddTotal);
            }
            pollSaveParams.setQuestions(questionsTypeTwo);
        }

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
        dmkAPI.setPolldata(headerMap, pollSaveParams).enqueue(new Callback<PollSaveResponse>() {
            @Override
            public void onResponse(Call<PollSaveResponse> call, Response<PollSaveResponse> response) {
                pollSaveResponse = response.body();
                if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                    hideProgress();
                    if (pollSaveResponse.getStatuscode() == 0) {
                        if (pollSaveResponse.getStatus().contains("Success")) {

                            Toast.makeText(PollListActivity.this, pollSaveResponse.getResults()
                                    , Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        } else {

                            Toast.makeText(PollListActivity.this, pollSaveResponse.getResults()
                                    , Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }
                    } else {
                        editor.putString(DmkConstants.HEADER_SOURCE, pollSaveResponse.getSource());
                        editor.putString(DmkConstants.HEADER_SOURCE_DATA, pollSaveResponse.getSourcedata());
                        editor.putString(DmkConstants.HEADER, getEncodedHeader(pollSaveResponse.getSource(),
                                pollSaveResponse.getSourcedata()));
                        editor.commit();
                        savePolllist();
                    }


                } else {
                    hideProgress();
                    Toast.makeText(PollListActivity.this, "error", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<PollSaveResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(PollListActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                onBackPressed();

            }
        });
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        setResult(4, intent);
        finish();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }
}
