package com.dci.dmkitwings.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.SelectedImageAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.SelectedImage;
import com.dci.dmkitwings.model.TimeLineComposeParams;
import com.dci.dmkitwings.model.TimeLineComposeResponse;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.model.TimeLineDetailsResponse;
import com.dci.dmkitwings.model.TimeLineUpdateParams;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.jzvd.JZVideoPlayerStandard;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.dmkitwings.app.DmkApplication.getContext;
import static com.dci.dmkitwings.app.DmkApplication.getProxy;

/**
 * Created by vijayaganesh on 4/5/2018.
 */

public class TimeLineComposeActivity extends BaseActivity {

    @BindView(R.id.edit_title)
    EditText editTitle;
    @BindView(R.id.view_time_compose)
    View viewTimeCompose;
    @BindView(R.id.edit_time_sub)
    EditText editTimeSub;
    @BindView(R.id.image_selected)
    ImageView imageSelected;
    @BindView(R.id.video_selected)
    JZVideoPlayerStandard videoSelected;
    @BindView(R.id.image_photo)
    ImageView imagePhoto;
    @BindView(R.id.image_video)
    ImageView imageVideo;
    @BindView(R.id.image_audio)
    ImageView imageAudio;
    @BindView(R.id.image_attach_file)
    ImageView imageAttachFile;
    @BindView(R.id.image_send)
    ImageView imageSend;
    @BindView(R.id.image_send1)
    ImageView imageSend1;
    @BindView(R.id.cons_send)
    ConstraintLayout consSend;
    @BindView(R.id.cons_bottom_view)
    ConstraintLayout consBottomView;
    @BindView(R.id.cons_time_line_compose)
    ConstraintLayout consTimeLineCompose;
    @BindView(R.id.grid_image)
    GridView gridImage;
    @BindView(R.id.image_delete)
    ImageView imageDelete;
    @BindView(R.id.float_attach_image)
    FloatingActionButton floatAttachImage;
    @BindView(R.id.float_attach_video)
    FloatingActionButton floatAttachVideo;
    @BindView(R.id.float_attach_audio)
    FloatingActionButton floatAttachAudio;
    @BindView(R.id.float_mes_compose)
    FloatingActionButton floatMesCompose;
    @BindView(R.id.cord_time_compose)
    CoordinatorLayout cordTimeCompose;
    private String galleryVideoPath, galleryAudioPath;
    private String recordedVideoPath;
    private int REQUEST_TAKE_GALLERY_VIDEO_AUDIO = 2;
    private static final int MY_REQUEST_CODE_CAMERA = 3;
    private static final int SELECT_IMAGE_FROM_CAMERA = 4;
    private static final int MY_REQUEST_CODE_GALLERY = 5;
    private int SELECT_IMAGE_FROM_GALLERY = 6;
    private static final int SELECT_VIDEO_FROM_CAMERA = 7;
    private int SELECT_VIDEO_FROM_GALLERY = 8;
    private List<SelectedImage> selectedImageList;
    private List<String> selectedMediaListName;
    private MediaRecorder mRecorder;
    private long mStartTime = 0;

    private int[] amplitudes = new int[100];

    private int i = 0;
    private File mOutputFile;
    private TextView textTimer;
    private Runnable mTickExecutor = new Runnable() {
        @Override
        public void run() {
            audioTimer();
            mHandler.postDelayed(mTickExecutor, 100);
        }
    };
    private SelectedImageAdapter selectedImageAdapter;

    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;

    TimeLineComposeResponse timeLineComposeResponse;
    boolean isFromTimeLine;
    int timeLineID, timeLineTitle, timeLineContent;
    private boolean isFABOpen;
    private MenuItem sumbit;
    private Uri selectedVideoAudioUri;
    private boolean isAudioFromRecord = false;
    private String audioRecordedPathName;
    private TimeLineDetailsResponse timeLineDetailsResponse;
    private HttpProxyCacheServer proxy;
    private boolean videoFromResponse = false;
    private String videoAudioFileNameInternal;
    File destination;
    String videoThubmnail;
    private int mediaType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_time_compose, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        proxy = getProxy(TimeLineComposeActivity.this);
        mDrawerLayout.addView(contentView, 0);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        textTitle.setText(R.string.timeline_compose);
        frameLayoutNotification.setVisibility(View.GONE);
        //Navigation Control
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        editTitle.setFocusable(true);
        editTitle.setFocusableInTouchMode(true);
        selectedImageList = new ArrayList<SelectedImage>();
        selectedMediaListName = new ArrayList<String>();

        Intent intent = getIntent();
        if (getIntent() != null) {
            isFromTimeLine = intent.getBooleanExtra("fromTimeLine", false);
            if (isFromTimeLine) {
                timeLineID = intent.getIntExtra("timeLineID", 0);
                getTimeLineDetail();

            }
        }


    }


    @OnClick({R.id.image_photo, R.id.image_video, R.id.image_audio, R.id.image_attach_file, R.id.image_send1,
            R.id.image_delete, R.id.float_mes_compose, R.id.float_attach_audio, R.id.float_attach_video, R.id.float_attach_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_photo:
//                getImageFiles();
                break;
            case R.id.image_video:
//                getVideoFiles();
                break;
            case R.id.image_audio:
//                getAudiofile();
                break;
            case R.id.image_attach_file:
                break;
            case R.id.image_send1:
//                timelineNullCheck();
                break;
            case R.id.image_delete:
                selectedVideoAudioUri = null;
                if (timeLineDetailsResponse != null) {
                    if (videoFromResponse)
                        videoFromResponse = false;
                }
                videoSelected.releaseAllVideos();
                videoSelected.setVisibility(View.GONE);
                imageDelete.setVisibility(View.GONE);
                break;
            case R.id.float_mes_compose:
                if (!isFABOpen) {
                    showFABMenu();
                } else {
                    closeFABMenu();
                }
                break;
            case R.id.float_attach_audio:
                closeFABMenu();
                getAudiofile();
                break;
            case R.id.float_attach_video:
                closeFABMenu();
                getVideoFiles();
                break;
            case R.id.float_attach_image:
                closeFABMenu();
                getImageFiles();
                break;
        }
    }

    private void showFABMenu() {
        isFABOpen = true;
        floatAttachAudio.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        floatAttachImage.animate().translationX(-getResources().getDimension(R.dimen.standard_55));
        floatAttachVideo.animate().translationY(getResources().getDimension(R.dimen.standard_55));
    }

    private void closeFABMenu() {
        isFABOpen = false;
        floatAttachAudio.animate().translationY(0);
        floatAttachImage.animate().translationX(0);
        floatAttachVideo.animate().translationY(0);
    }

    @Override
    public void onBackPressed() {
        if (!isFABOpen) {
            super.onBackPressed();
        } else {
            closeFABMenu();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save, menu);
        sumbit = menu.findItem(R.id.menu_bar_save);
        sumbit.setTitle(R.string.submit);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_save:

                android.support.v7.app.AlertDialog.Builder alertDialog1 = new android.support.v7.app.AlertDialog.Builder(TimeLineComposeActivity.this);
                alertDialog1.setMessage(getString(R.string.confirm_to_post));
                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        timelineNullCheck();
                    }
                });
                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        hideProgress();
                    }
                });
                alertDialog1.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == SELECT_IMAGE_FROM_GALLERY && resultCode == RESULT_OK && null != data) {
                imageFromGalleryResult(data);
            } else if (requestCode == SELECT_IMAGE_FROM_CAMERA && resultCode == RESULT_OK &&
                    data != null) {
                imageFromCameraResult(data);
            } else if (requestCode == SELECT_VIDEO_FROM_GALLERY && resultCode == RESULT_OK &&
                    data != null) {
                videoFromGalleryResult(data);
            } else if (requestCode == SELECT_VIDEO_FROM_CAMERA && resultCode == RESULT_OK &&
                    data != null) {
                videoFromCameraResult(data);
            } else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO_AUDIO && resultCode == RESULT_OK &&
                    data != null) {
                getAudioGalleryResult(data);
            }
        } catch (Exception e) {
            Toast.makeText(this, "Please select vaild media", Toast.LENGTH_SHORT)
                    .show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }


    public static boolean isVideoFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("video");
    }

    private void getImageFiles() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.media_dialog);
        // Set dialog title
        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
        textCamera.setText(R.string.camera);
        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
        imageCamera.setImageResource(R.mipmap.icon_photo_camera);
        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
        textGallery.setText(R.string.gallery);
        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
        imageGallery.setImageResource(R.mipmap.icon_gallery);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(TimeLineComposeActivity.this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_REQUEST_CODE_CAMERA);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, SELECT_IMAGE_FROM_CAMERA);
                    }
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, SELECT_IMAGE_FROM_CAMERA);
                }
                dialog.dismiss();
            }
        });
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(TimeLineComposeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE_GALLERY);
                    } else {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                        intent.setType("image/*");
                        startActivityForResult(
                                Intent.createChooser(intent, "Select File"),
                                SELECT_IMAGE_FROM_GALLERY);
                    }
                } else {

                    Intent intent = new Intent(
                            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_IMAGE_FROM_GALLERY);

                }
                dialog.dismiss();
            }

        });

        // set values for custom dialog components - text, image and button
        dialog.show();


    }

    private void getVideoFiles() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.media_dialog);
        // Set dialog title
        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
        textCamera.setText(R.string.video);
        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
        imageCamera.setImageResource(R.mipmap.icon_photo_camera);
        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
        textGallery.setText(R.string.gallery);
        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
        imageGallery.setImageResource(R.mipmap.icon_gallery);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(TimeLineComposeActivity.this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_REQUEST_CODE_CAMERA);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        startActivityForResult(intent, SELECT_VIDEO_FROM_CAMERA);
                    }
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    startActivityForResult(intent, SELECT_VIDEO_FROM_CAMERA);
                }
                dialog.dismiss();
            }
        });
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(TimeLineComposeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE_GALLERY);
                    } else {
//                        Intent intent = new Intent();
//                        intent.setType("video/*");
//                        intent.setAction(Intent.ACTION_GET_CONTENT);
//                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_VIDEO_FROM_GALLERY);
                        Intent intent = new Intent(
                                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("video/*");
                        startActivityForResult(
                                Intent.createChooser(intent, "Select File"),
                                SELECT_VIDEO_FROM_GALLERY);
                    }
                } else {
//                    Intent intent = new Intent();
//                    intent.setType("video/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_VIDEO_FROM_GALLERY);
                    Intent intent = new Intent(
                            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("video/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_VIDEO_FROM_GALLERY);
                }
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void getAudiofile() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.media_dialog);
        // Set dialog title
        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
        textCamera.setText(R.string.record);
        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
        imageCamera.setImageResource(R.mipmap.icon_mic);
        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
        textGallery.setText(R.string.gallery);
        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
        imageGallery.setImageResource(R.mipmap.icon_gallery);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAudioRecordDialog();
                dialog.dismiss();
            }
        });
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent();
//                intent.setType("audio/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, "Select Media Files"), REQUEST_TAKE_GALLERY_VIDEO_AUDIO);
//                Intent intent = new Intent(
//                        Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                intent.setType("audio/*");
//                startActivityForResult(
//                        Intent.createChooser(intent,"Select Media Files"),
//                        REQUEST_TAKE_GALLERY_VIDEO_AUDIO);
//                Intent intent_upload = new Intent();
//                intent_upload.setType("audio/*");
//                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(intent_upload,REQUEST_TAKE_GALLERY_VIDEO_AUDIO);
                Intent videoIntent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                videoIntent.putExtra(Intent.ACTION_SEND_MULTIPLE, false);
                startActivityForResult(Intent.createChooser(videoIntent, "Select Audio"),
                        REQUEST_TAKE_GALLERY_VIDEO_AUDIO);
                dialog.dismiss();
            }
        });
        dialog.show();


    }

    private void imageFromCameraResult(Intent data) {

        gridImage.setVisibility(View.VISIBLE);
        if (selectedVideoAudioUri != null || videoFromResponse) {
            videoSelected.setVisibility(View.VISIBLE);
            imageDelete.setVisibility(View.VISIBLE);
        } else {
            videoSelected.setVisibility(View.INVISIBLE);
            imageDelete.setVisibility(View.INVISIBLE);
        }

        Bitmap capturedImageBitmap = null;
        if (data != null) {
            capturedImageBitmap = (Bitmap) data.getExtras().get("data");
        }

        if (capturedImageBitmap != null) {
            selectedImageList.add(new SelectedImage(null, 1,
                    capturedImageBitmap, false, System.currentTimeMillis() + ".jpg"));
        }
        selectedImageAdapter = new SelectedImageAdapter(selectedImageList, this);
        gridImage.setAdapter(selectedImageAdapter);

    }

    private void imageFromGalleryResult(Intent data) {


        boolean found = false;
        if (data.getData() != null) {
            Uri mImageUri = data.getData();
            String rettt = getImagePath(mImageUri);
            if (rettt == null) {
                String endextension = mImageUri.toString();
                int lenght = endextension.length();
                endextension = endextension.substring(lenght - 3);
                if (endextension != null) {
                    for (String element : imageFormat) {
                        if (element.equals(endextension)) {
                            found = true;
                            if (!videoFromResponse) videoFromResponse = false;
                            break;
                        }
                    }
                    if (found) {
                        gridImage.setVisibility(View.VISIBLE);
                        if (selectedVideoAudioUri != null || videoFromResponse) {
                            videoSelected.setVisibility(View.VISIBLE);
                            imageDelete.setVisibility(View.VISIBLE);
                        } else {
                            videoSelected.setVisibility(View.INVISIBLE);
                            imageDelete.setVisibility(View.INVISIBLE);
                        }
                        if (mImageUri != null) {
                            selectedImageList.add(new SelectedImage(null, 1,
                                    compressInputImage(mImageUri, this), false, System.currentTimeMillis() + ".jpg"));
                        }
                        selectedImageAdapter = new SelectedImageAdapter(selectedImageList, this);
                        gridImage.setAdapter(selectedImageAdapter);
                    } else {
                        Toast.makeText(TimeLineComposeActivity.this, "select image only", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                String extenstion = getFileExt(rettt);
                if (extenstion != null) {
                    for (String element : imageFormat) {
                        if (element.equals(extenstion)) {
                            found = true;
                            if (!videoFromResponse) videoFromResponse = false;
                            break;
                        }
                    }
                    if (found) {
                        gridImage.setVisibility(View.VISIBLE);
                        if (selectedVideoAudioUri != null || videoFromResponse) {
                            videoSelected.setVisibility(View.VISIBLE);
                            imageDelete.setVisibility(View.VISIBLE);
                        } else {
                            videoSelected.setVisibility(View.INVISIBLE);
                            imageDelete.setVisibility(View.INVISIBLE);
                        }

                        //                Fetch Single Image
                        if (mImageUri != null) {
                            selectedImageList.add(new SelectedImage(null, 1,
                                    compressInputImage(mImageUri, this), false, System.currentTimeMillis() + ".jpg"));
                        }
                        selectedImageAdapter = new SelectedImageAdapter(selectedImageList, this);
                        gridImage.setAdapter(selectedImageAdapter);

                    }


                } else {
                    System.out.println("The value is Not found!");
                    Toast.makeText(TimeLineComposeActivity.this, "select image only", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (data.getClipData() != null) {
            gridImage.setVisibility(View.VISIBLE);
            if (selectedVideoAudioUri != null || videoFromResponse) {
                videoSelected.setVisibility(View.VISIBLE);
                imageDelete.setVisibility(View.VISIBLE);
            } else {
                videoSelected.setVisibility(View.INVISIBLE);
                imageDelete.setVisibility(View.INVISIBLE);
            }
            ClipData mClipData = data.getClipData();
            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
            for (int i = 0; i < mClipData.getItemCount(); i++) {
                ClipData.Item item = mClipData.getItemAt(i);
                Uri uri = item.getUri();
                mArrayUri.add(uri);
                if (uri != null) {
                    selectedImageList.add(new SelectedImage(null, 1,
                            compressInputImage(uri, this), false, System.currentTimeMillis() + ".jpg"));
                }
            }


            selectedImageAdapter = new SelectedImageAdapter(selectedImageList, this);
            gridImage.setAdapter(selectedImageAdapter);


        } else {
            System.out.println("The value is Not found!");
            Toast.makeText(TimeLineComposeActivity.this, "select image only", Toast.LENGTH_SHORT).show();
        }


    }

    private void videoFromGalleryResult(Intent data) {


        boolean found = false;
        selectedVideoAudioUri = data.getData();
        galleryVideoPath = getPath(selectedVideoAudioUri);
        String extenstion = getFileExt(galleryVideoPath);
        for (String element : audioimageFormat) {
            if (element.equals(extenstion)) {
                found = true;
                break;
            }
        }
        if (found) {

            //gridImage.setVisibility(View.INVISIBLE);
            //videoSelected.setVisibility(View.INVISIBLE);
            //imageDelete.setVisibility(View.INVISIBLE);
            selectedVideoAudioUri = null;
            Toast.makeText(TimeLineComposeActivity.this, "Upload Only video", Toast.LENGTH_SHORT).show();
        } else {
            gridImage.setVisibility(View.VISIBLE);
            videoSelected.setVisibility(View.VISIBLE);
            imageDelete.setVisibility(View.VISIBLE);
            videoSelected.setUp(galleryVideoPath
                    , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
            Glide.with(this)
                    .load(Uri.fromFile(new File(galleryVideoPath)))
                    .into(videoSelected.thumbImageView);
            if (selectedVideoAudioUri != null) {
                videoAudioFileNameInternal = System.currentTimeMillis() + ".mp4";
                videoThubmnail = galleryVideoPath;
            }
        }
        // MEDIA GALLERY Path
//        recordedVideoPath = getPath(selectedImageUri);
//        "/storage/2348-13EA/DCIM/Camera/VID_20180511_182119029.mp4"
//        /storage/2348-13EA/DCIM/Camera/VID_20180511_184203378.mp4

    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }


    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void videoFromCameraResult(Intent data) {

        gridImage.setVisibility(View.VISIBLE);
        videoSelected.setVisibility(View.VISIBLE);
        imageDelete.setVisibility(View.VISIBLE);

        selectedVideoAudioUri = data.getData();
        // OI FILE Manager Path
//        galleryVideoPath = selectedImageUri.getPath();
        // MEDIA GALLERY Path
        recordedVideoPath = getPath(selectedVideoAudioUri);

        videoSelected.setUp(recordedVideoPath
                , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
        Glide.with(this)
                .load(Uri.fromFile(new File(recordedVideoPath)))
                .into(videoSelected.thumbImageView);
        if (selectedVideoAudioUri != null) {
            videoAudioFileNameInternal = System.currentTimeMillis() + ".mp4";
            videoThubmnail = recordedVideoPath;
        }

    }


    private void getAudioGalleryResult(Intent data) {


        //Fetch Video and Audio File

        selectedVideoAudioUri = data.getData();
        // OI FILE Manager Path
        galleryAudioPath = selectedVideoAudioUri.getPath();
        boolean found = false;
        galleryVideoPath = getPath(selectedVideoAudioUri);
        String extenstion = getFileExt(galleryVideoPath);
        for (String element : audioFormat) {
            if (element.equals(extenstion)) {
                found = true;
                if (!videoFromResponse) videoFromResponse = false;
                System.out.println("The value is found!");
                break;
            }
        }
        if (!found) {
            //galleryAudioPath=null;
            //gridImage.setVisibility(View.INVISIBLE);
            videoSelected.setVisibility(View.INVISIBLE);
            imageDelete.setVisibility(View.INVISIBLE);
            selectedVideoAudioUri = null;
            Toast.makeText(TimeLineComposeActivity.this, "Upload Only Audio", Toast.LENGTH_SHORT).show();
        } else {
            gridImage.setVisibility(View.VISIBLE);
            videoSelected.setVisibility(View.VISIBLE);
            imageDelete.setVisibility(View.VISIBLE);
            videoSelected.setUp(galleryAudioPath
                    , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
            if (isVideoFile(galleryAudioPath)) {
                Glide.with(this)
                        .load(Uri.fromFile(new File(galleryAudioPath)))
                        .into(videoSelected.thumbImageView);
            } else {
                Glide.with(this)
                        .load(R.mipmap.icon_audio_thumbnail).into(videoSelected.thumbImageView);

            }
            if (selectedVideoAudioUri != null) {
                videoAudioFileNameInternal = System.currentTimeMillis() + ".mp3";
            }
        }
        // MEDIA GALLERY Path
        //recordedVideoPath = getPath(selectedImageUri);

    }

    private void getAudioRecordedResult() {

        gridImage.setVisibility(View.VISIBLE);
        videoSelected.setVisibility(View.VISIBLE);
        imageDelete.setVisibility(View.VISIBLE);

        selectedVideoAudioUri = Uri.parse(mOutputFile.getAbsolutePath());
        videoSelected.setUp(mOutputFile.getAbsolutePath()
                , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");

        //Load thumbnail image for only local storage videos
        Glide.with(this)
                .load(R.mipmap.icon_audio_thumbnail).into(videoSelected.thumbImageView);
        if (selectedVideoAudioUri != null) {

            videoAudioFileNameInternal = audioRecordedPathName;
        }
        isAudioFromRecord = true;
    }

    public void showAudioRecordDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_timeline_audiorecord);
        textTimer = (TextView) dialog.findViewById(R.id.text_dialog);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording(true);
                getAudioRecordedResult();
                dialog.dismiss();
            }
        });
        dialog.show();
        startAudioRecording();
    }


    private void startAudioRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
            mRecorder.setAudioEncodingBitRate(48000);
        } else {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.setAudioEncodingBitRate(64000);
        }
        mRecorder.setAudioSamplingRate(16000);
        mOutputFile = getOutputFile();
        mOutputFile.getParentFile().mkdirs();
        mRecorder.setOutputFile(mOutputFile.getAbsolutePath());

        try {
            mRecorder.prepare();
            mRecorder.start();
            mStartTime = SystemClock.elapsedRealtime();
            mHandler.postDelayed(mTickExecutor, 100);
        } catch (IOException e) {
        }
    }

    protected void stopRecording(boolean saveFile) {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        mStartTime = 0;

        mHandler.removeCallbacks(mTickExecutor);
        if (!saveFile && mOutputFile != null) {
            mOutputFile.delete();
        }

    }

    private void audioTimer() {
        long time = (mStartTime < 0) ? 0 : (SystemClock.elapsedRealtime() - mStartTime);
        int minutes = (int) (time / 60000);
        int seconds = (int) (time / 1000) % 60;
        int milliseconds = (int) (time / 100) % 10;
        textTimer.setText(minutes + ":" + (seconds < 10 ? "0" + seconds : seconds));
        if (mRecorder != null) {
            amplitudes[i] = mRecorder.getMaxAmplitude();
            //Log.d("Voice Recorder","amplitude: "+(amplitudes[i] * 100 / 32767));
            if (i >= amplitudes.length - 1) {
                i = 0;
            } else {
                ++i;
            }
        }
    }


    private File getOutputFile() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US);
        audioRecordedPathName = dateFormat.format(new Date()) + ".m4a";
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/Voice Recorder/RECORDING_"
                + audioRecordedPathName);
    }

    @SuppressLint("NewApi")
    private void timelineNullCheck() {


        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0) {
            if (editTitle.getText().toString().trim().length() > 0 && editTitle.getText().toString().trim().length() <= DmkConstants.COMPOSE_TITLE_LIMIT &&
                    editTimeSub.getText().toString().trim().length() > 0 && selectedImageAdapter.getCount() <= 3 ? true : false) {
                if (isFromTimeLine) {
                    if (selectedVideoAudioUri != null) {
                        if (!getFileSize(selectedVideoAudioUri)) {
                            Toast.makeText(TimeLineComposeActivity.this, getString(R.string.video_limit),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            uploadMedia();
                        }
                    } else {
                        uploadMedia();
                    }
                } else {
                    if (selectedVideoAudioUri != null) {
                        if (!getFileSize(selectedVideoAudioUri)) {
                            Toast.makeText(TimeLineComposeActivity.this, getString(R.string.video_limit),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            uploadMedia();
                        }
                    } else {
                        uploadMedia();
                    }


                }
            } else {
                if (editTitle.getText().toString().trim().length() <= 0) {
                    editTitle.setError(getString(R.string.title_missing));
                }
                if (editTimeSub.getText().toString().trim().length() <= 0) {
                    editTimeSub.setError(getString(R.string.write_some_thing_missing));
                }
                if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 3) {
                    Toast.makeText(TimeLineComposeActivity.this, getString(R.string.image_limit),
                            Toast.LENGTH_SHORT).show();
                }
                if (editTitle.getText().toString().trim().length() > DmkConstants.COMPOSE_TITLE_LIMIT) {
                    editTitle.setError(getString(R.string.title_limit));
                }


            }
        } else {
            if (editTitle.getText().toString().trim().length() > 0 && editTitle.getText().toString().trim().length() <= DmkConstants.COMPOSE_TITLE_LIMIT &&
                    editTimeSub.getText().toString().trim().length() > 0) {
                if (isFromTimeLine) {
                    if (selectedVideoAudioUri != null) {

                        if (!getFileSize(selectedVideoAudioUri)) {
                            Toast.makeText(TimeLineComposeActivity.this, getString(R.string.video_limit),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            uploadMedia();
                        }
                    } else {
                        uploadMedia();
                    }
                } else {

                    if (selectedVideoAudioUri != null) {
                        if (!getFileSize(selectedVideoAudioUri)) {

                            Toast.makeText(TimeLineComposeActivity.this, getString(R.string.video_limit),
                                    Toast.LENGTH_SHORT).show();

                        } else {
                            uploadMedia();
                        }
                    } else {
                        uploadMedia();
                    }


                }
            } else {
                if (editTitle.getText().toString().trim().length() <= 0) {
                    editTitle.setError(getString(R.string.title_missing));
                }
                if (editTimeSub.getText().toString().trim().length() <= 0) {
                    editTimeSub.setError(getString(R.string.write_some_thing_missing));
                }
                if (editTitle.getText().toString().trim().length() > DmkConstants.COMPOSE_TITLE_LIMIT) {
                    editTitle.setError(getString(R.string.title_limit));
                }
            }
        }


    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public static byte[] compress(String data) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length());
        GZIPOutputStream gzip = new GZIPOutputStream(bos);
        gzip.write(data.getBytes());
        gzip.close();
        byte[] compressed = bos.toByteArray();
        bos.close();
        return compressed;
    }

    private void createTimeLine() {
        TimeLineComposeParams timeLineComposeParams = new TimeLineComposeParams();
        timeLineComposeParams.setContent(editTimeSub.getText().toString());
        timeLineComposeParams.setTitle(editTitle.getText().toString());
        timeLineComposeParams.setMediapath("");
        timeLineComposeParams.setMediatype(0);
        //For dmk server image
        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0) {
            for (int i = 0; i < selectedImageAdapter.getCount(); i++) {
                selectedMediaListName.add(selectedImageAdapter.getItem(i).getImageName());
            }
        }
        //For dmk server video
        if (selectedVideoAudioUri != null) {
            selectedMediaListName.add(videoAudioFileNameInternal);
            if (videoThubmnail != null) {
                //Bitmap thumb = ThumbnailUtils.createVideoThumbnail(videoThubmnail, MediaStore.Images.Thumbnails.MINI_KIND);
                timeLineComposeParams.setVideothumb(videoThubmnail);

            } else {
                timeLineComposeParams.setVideothumb("0");
            }

        } else {
            timeLineComposeParams.setVideothumb("0");
        }
        //        to send image name in dmk server
        timeLineComposeParams.setMediaFiles(selectedMediaListName);
        timeLineComposeParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.createTimeLine(headerMap, timeLineComposeParams).enqueue(new Callback<TimeLineComposeResponse>() {
                @Override
                public void onResponse(Call<TimeLineComposeResponse> call, Response<TimeLineComposeResponse> response) {
                    timeLineComposeResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (timeLineComposeResponse.getStatuscode() == 0) {
                            hideProgress();
                            if (timeLineComposeResponse.getStatus().contains("Success")) {
                                onBackPressed();
                                Toast.makeText(TimeLineComposeActivity.this, getString(R.string.timeline_compose_sucuess),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(TimeLineComposeActivity.this, getString(R.string.timeline_compose_failed)
                                        , Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, timeLineComposeResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, timeLineComposeResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(timeLineComposeResponse.getSource(),
                                    timeLineComposeResponse.getSourcedata()));
                            editor.commit();
                            showProgress();
                            createTimeLine();
                        }

                    } else {
                        hideProgress();
                        Toast.makeText(TimeLineComposeActivity.this, getString(R.string.timeline_compose_failed)
                                , Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<TimeLineComposeResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(TimeLineComposeActivity.this, getString(R.string.timeline_compose_failed)
                            , Toast.LENGTH_SHORT).show();
                }
            });
        } else

        {
            Toast.makeText(this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadMedia() {
        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0 && selectedVideoAudioUri != null) {
//            both image and video
            mediaType = 1;
        } else if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0 && selectedVideoAudioUri == null) {
            //only image
            mediaType = 2;
        } else if (selectedImageAdapter == null && selectedVideoAudioUri != null) {
            //only video
            mediaType = 3;
        } else {
            //no media
            mediaType = 4;
        }

        if (mediaType == 1 || mediaType == 2) {
            showProgress(getString(R.string.media_uploading));
            uploadImage();
        } else if (mediaType == 3) {
            showProgress(getString(R.string.media_uploading));
            uploadVideoandAudio();
        } else {
            showProgress();
            if (isFromTimeLine)
                updateTimeLine();
            else
                createTimeLine();

        }
    }

    private void uploadImage() {
        for (int i = 0; i < selectedImageAdapter.getCount(); i++) {
            //        to send media name in s3 server
            destination = new File(getExternalFilesDir(null),
                    selectedImageAdapter.getItem(i).getImageName());
            if (selectedImageAdapter.getItem(i).getSelectedImage() != null &&
                    getImageUri(TimeLineComposeActivity.this,
                    selectedImageAdapter.getItem(i).getSelectedImage()) != null) {
                createFile(TimeLineComposeActivity.this, getImageUri(TimeLineComposeActivity.this,
                        selectedImageAdapter.getItem(i).getSelectedImage()), destination);
                TransferObserver uploadObserver =
                        transferUtility.upload(getString(R.string.s3_bucket_time_line_path) +
                                selectedImageAdapter.getItem(i).getImageName(), destination);
                final int finalI = i;
                uploadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            destination.delete();
                            if (finalI == selectedImageAdapter.getCount() - 1) {
                                if (mediaType == 1)
                                    uploadVideoandAudio();
                                else {
                                    if (isFromTimeLine)
                                        updateTimeLine();
                                    else
                                        createTimeLine();
                                }

                            }
                        } else if (TransferState.FAILED == state) {
                            destination.delete();
                            if (finalI == selectedImageAdapter.getCount() - 1) {
                                if (mediaType == 1)
                                    uploadVideoandAudio();
                                else {
                                    hideProgress();
                                    Toast.makeText(TimeLineComposeActivity.this,
                                            getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;


                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                        destination.delete();
                        if (finalI == selectedImageAdapter.getCount() - 1) {
                            if (mediaType == 1)
                                uploadVideoandAudio();
                            else {
                                hideProgress();
                                Toast.makeText(TimeLineComposeActivity.this,
                                        getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                            }


                        }
                    }

                });
            } else {
                if (mediaType == 1)
                    uploadVideoandAudio();
                else if (isFromTimeLine)
                    // if not changed anything in media from already selected(update timeline)
                    updateTimeLine();
                else {
                    hideProgress();
                    Toast.makeText(TimeLineComposeActivity.this,
                            getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                }

            }
        }

    }


    private void uploadVideoandAudio() {
        //Audio and video file
        if (isAudioFromRecord) {
            //audio from record
            destination = new File(
                    Environment.getExternalStorageDirectory().getAbsolutePath().toString() +
                            "/Voice Recorder/RECORDING_"
                            + videoAudioFileNameInternal);
        } else {
            // audio from gallery
            destination = new File(getExternalFilesDir(null), videoAudioFileNameInternal);
        }
        createFile(TimeLineComposeActivity.this, selectedVideoAudioUri, destination);
        TransferObserver uploadObserver =
                transferUtility.upload(getString(R.string.s3_bucket_time_line_path) +
                        videoAudioFileNameInternal, destination);
        final File finalDestination = destination;
        uploadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    destination.delete();
                    if (isFromTimeLine)
                        updateTimeLine();
                    else
                        createTimeLine();
                } else if (TransferState.FAILED == state) {
                    destination.delete();
                    hideProgress();
                    Toast.makeText(TimeLineComposeActivity.this,
                            getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;
            }

            @Override
            public void onError(int id, Exception ex) {
                ex.printStackTrace();
                destination.delete();
                hideProgress();
                Toast.makeText(TimeLineComposeActivity.this,
                        getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
            }

        });
    }


    private void updateTimeLine() {
        TimeLineUpdateParams timeLineUpdateParams = new TimeLineUpdateParams();
        timeLineUpdateParams.setContent(editTimeSub.getText().toString());
        timeLineUpdateParams.setTitle(editTitle.getText().toString());
        timeLineUpdateParams.setMediapath("");
        timeLineUpdateParams.setMediatype(2);
        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0) {
            for (int i = 0; i < selectedImageAdapter.getCount(); i++) {
                selectedMediaListName.add(selectedImageAdapter.getItem(i).getImageName());
            }
        }
        if (videoAudioFileNameInternal != null) {
            selectedMediaListName.add(videoAudioFileNameInternal);
            timeLineUpdateParams.setVideothumb(videoThubmnail);
        } else {
            if (videoFromResponse) {
                String videoFile = null;
                for (int i = 0; i < timeLineDetailsResponse.getResults().getMediapath().size(); i++) {
                    if (!isImageFile(timeLineDetailsResponse.getResults().getMediapath().get(i))) {
                        videoFile = timeLineDetailsResponse.getResults().getMediapath().get(i);
                    }
                }
                selectedMediaListName.add(videoFile);
                timeLineUpdateParams.setVideothumb(timeLineDetailsResponse.getResults().getVideothumb());
            } else {
                timeLineUpdateParams.setVideothumb("0");
            }
        }
        timeLineUpdateParams.setMediaFiles(selectedMediaListName);
        timeLineUpdateParams.setTimelineid(timeLineID);
        timeLineUpdateParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.updateTimeLine(headerMap, timeLineUpdateParams).enqueue(new Callback<TimeLineComposeResponse>() {
                @Override
                public void onResponse(Call<TimeLineComposeResponse> call, Response<TimeLineComposeResponse> response) {
                    timeLineComposeResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (timeLineComposeResponse.getStatuscode() == 0) {
                            hideProgress();
                            if (timeLineComposeResponse.getStatus().contains("Success")) {
                                onBackPressed();
                                Toast.makeText(TimeLineComposeActivity.this, getString(R.string.timeline_update_sucuess),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(TimeLineComposeActivity.this, getString(R.string.timeline_update_failed)
                                        , Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, timeLineComposeResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, timeLineComposeResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(timeLineComposeResponse.getSource(),
                                    timeLineComposeResponse.getSourcedata()));
                            editor.commit();
                            showProgress();
                            updateTimeLine();
                        }

                    } else {
                        hideProgress();
                        Toast.makeText(TimeLineComposeActivity.this, getString(R.string.timeline_update_failed)
                                , Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TimeLineComposeResponse> call, Throwable t) {
                    hideProgress();
                    if (t.getMessage().contains("timeout")) {
                        Toast.makeText(TimeLineComposeActivity.this, getString(R.string.please_try_again)
                                , Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(TimeLineComposeActivity.this, getString(R.string.timeline_update_failed)
                                , Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } else {
            Toast.makeText(this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    private void getTimeLineDetail() {
        TimeLineDelete timeLineDelete = new TimeLineDelete();
        timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        timeLineDelete.setTimelineid(timeLineID);
        if (Util.isNetworkAvailable()) {
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getTimeLineDetails(headerMap, timeLineDelete).enqueue(new Callback<TimeLineDetailsResponse>() {
                @Override
                public void onResponse(Call<TimeLineDetailsResponse> call, Response<TimeLineDetailsResponse> response) {
                    hideProgress();
                    timeLineDetailsResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {

                        if (timeLineDetailsResponse.getStatuscode() == 0) {
                            editTimeSub.setText(timeLineDetailsResponse.getResults().getContent());
                            editTitle.setText(timeLineDetailsResponse.getResults().getTitle());

                            for (int i = 0; i < timeLineDetailsResponse.getResults().getMediapath().size(); i++) {
                                if (isImageFile(timeLineDetailsResponse.getResults().getMediapath().get(i))) {
                                    //Image Type
                                    gridImage.setVisibility(View.VISIBLE);
                                    selectedImageList.add(new SelectedImage(null, 1, null,
                                            true, timeLineDetailsResponse.getResults().getMediapath().get(i)));
                                    selectedImageAdapter = new SelectedImageAdapter(selectedImageList, TimeLineComposeActivity.this);
                                    gridImage.setAdapter(selectedImageAdapter);
                                } else {
                                    //Video and audio Type
                                    String videoPath = null;
                                    if (!isImageFile(timeLineDetailsResponse.getResults().getMediapath().get(i))) {
                                        videoPath = timeLineDetailsResponse.getResults().getMediapath().get(i);
                                    }
                                    videoSelected.setVisibility(View.VISIBLE);
                                    imageDelete.setVisibility(View.VISIBLE);
                                    videoSelected.setUp(proxy.getProxyUrl(getString(R.string.s3_image_url_download) +
                                                    getString(R.string.s3_bucket_time_line_path) +
                                                    videoPath)
                                            , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                                    videoSelected.setSoundEffectsEnabled(true);


                                    if (isAudioFile(videoPath)) {
                                        //audio file
                                        Glide.with(TimeLineComposeActivity.this).
                                                load(R.mipmap.icon_audio_thumbnail).
                                                into(videoSelected.thumbImageView);
                                    } else {
                                        //video file
                                        //                                    if (!timeLineDetailsResponse.getResults().getVideothumb().
                                        //                                            equalsIgnoreCase("0")) {
                                        //                                        Glide.with(TimeLineComposeActivity.this).
                                        //                                                load(timeLineDetailsResponse.getResults().getVideothumb())
                                        //                                                .into(videoSelected.thumbImageView);
                                        //                                    } else {
                                        //                                        Glide.with(TimeLineComposeActivity.this).
                                        //                                                load(R.mipmap.icon_video_thumbnail).
                                        //                                                into(videoSelected.thumbImageView);
                                        //                                    }

                                        RequestOptions requestOptions = new RequestOptions();
                                        requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                                        requestOptions.error(R.mipmap.icon_video_thumbnail);
                                        Glide.with(getContext())
                                                .load(getString(R.string.s3_image_url_download) +
                                                        getString(R.string.s3_bucket_time_line_path) +
                                                        videoPath)
                                                .apply(requestOptions)
                                                .thumbnail(Glide.with(getContext()).load(getString(R.string.s3_image_url_download) +
                                                        getString(R.string.s3_bucket_time_line_path) +
                                                        videoPath))
                                                .into(videoSelected.thumbImageView);
                                    }

                                    videoFromResponse = true;
                                }
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, timeLineDetailsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, timeLineDetailsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(timeLineDetailsResponse.getSource(),
                                    timeLineDetailsResponse.getSourcedata()));
                            editor.commit();
                            getTimeLineDetail();
                        }

                    } else {
                        Toast.makeText(TimeLineComposeActivity.this, getString(R.string.error),
                                Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TimeLineDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(TimeLineComposeActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(TimeLineComposeActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


}
