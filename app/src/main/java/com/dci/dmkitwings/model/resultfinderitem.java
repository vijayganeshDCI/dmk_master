package com.dci.dmkitwings.model;

import java.util.List;

public class resultfinderitem
{
    private String pollcomplete;

    public String getPollcomplete() {
        return pollcomplete;
    }

    public void setPollcomplete(String pollcomplete) {
        this.pollcomplete = pollcomplete;
    }
    private List<String> questions;

    public List<String> getQuestions() {
        return questions;
    }

    public void setQuestions(List<String> questions) {
        this.questions = questions;
    }
}
