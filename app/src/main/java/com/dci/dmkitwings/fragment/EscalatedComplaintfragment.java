package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ComplaintActivity;
import com.dci.dmkitwings.activity.CompliantComposeActivity;
import com.dci.dmkitwings.activity.FeedBackActivity;
import com.dci.dmkitwings.adapter.MyCompliantsAdpater;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.ComplaintListParams;
import com.dci.dmkitwings.model.ComposeCompliantParams;
import com.dci.dmkitwings.model.FeedbackPostResponse;
import com.dci.dmkitwings.model.MyComplaintResultsItem;
import com.dci.dmkitwings.model.MyComplaintsResponse;
import com.dci.dmkitwings.model.UpdateReceivedCompliantParams;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 3/16/2018.
 */

public class EscalatedComplaintfragment extends BaseFragment {


    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Unbinder unbinder;
    @BindView(R.id.text_posted_by)
    TextView textPostedName;
    @BindView(R.id.text_post_designation)
    TextView textPostedDesignaion;
    @BindView(R.id.text_label_feed_back_title)
    TextView textTitle;
    @BindView(R.id.text_label_feed_back_content)
    TextView textContent;
    @BindView(R.id.text_label_feed_back_edittext)
    EditText editReplied;
    @BindView(R.id.button_escalated)
    Button buttonEscalate;
    @BindView(R.id.button_resolved)
    Button buttonResolved;
    @BindView(R.id.button_notresolved)
    Button buttonNotResolved;
    Bundle bundle;
    String escalated, replied;
    CompliantComposeActivity compliantComposeActivity;
    int id, Categoryid;
    @BindView(R.id.text_post_status)
    TextView textStatus;
    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    String postedUserprofilepic;
    FeedbackPostResponse feedbackPostResponse;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receivedcompliantdetails, container, false);
        unbinder = ButterKnife.bind(this, view);
        ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        compliantComposeActivity = (CompliantComposeActivity) getActivity();
        compliantComposeActivity.textTitle.setText(R.string.Received_Complaint);
        editor = sharedPreferences.edit();
        bundle = this.getArguments();
        if (bundle != null) {
            textPostedName.setText(bundle.getString("FirstName") + " " + bundle.getString("LastName"));
            textPostedDesignaion.setText(bundle.getString("Designation"));
            textTitle.setText(bundle.getString("SubjectID"));
            textContent.setText(bundle.getString("feedback"));
            escalated = bundle.getString("Escalated");
            replied = bundle.getString("Replied");
            id = bundle.getInt("id");
            Categoryid = bundle.getInt("Categoryid");
            postedUserprofilepic = bundle.getString("avatar");
            universalImageLoader(imageProPic, getString(R.string.s3_bucket_profile_path),
                    postedUserprofilepic, R.mipmap.icon_pro_image_loading_256,
                    R.mipmap.icon_pro_image_loading_256);
            if (bundle.getString("status").equals("Resolved")) {
                textStatus.setTextColor(getActivity().getResources().getColor(R.color.accept_green));
                textStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_resolved_32_green, 0);
                textStatus.setText(getString(R.string.Resolved));
            } else {
                textStatus.setTextColor(Color.RED);
                textStatus.setText(getString(R.string.NotResolved));
                textStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_not_resolved_32_red, 0);
            }
        }
        if (escalated.equals("Yes") || replied.equals("Yes")) {
            editReplied.setVisibility(View.GONE);
            buttonEscalate.setVisibility(View.GONE);
            buttonResolved.setVisibility(View.GONE);
            buttonNotResolved.setVisibility(View.GONE);
        }

        return view;
    }

    @OnClick({R.id.button_escalated, R.id.button_resolved, R.id.button_notresolved})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_escalated:
                if (editReplied.getText().toString().trim().length() > 0) {
                    escalateComplaint();

                }
                if (editReplied.getText().toString().trim().length() <= 0) {
                    editReplied.setError(getString(R.string.write_some_thing_missing));
                }

                break;
            case R.id.button_resolved:

                if (editReplied.getText().toString().trim().length() > 0) {
                    UpdateCompliant("1");

                }
                if (editReplied.getText().toString().trim().length() <= 0) {
                    editReplied.setError(getString(R.string.write_some_thing_missing));
                }


                break;
            case R.id.button_notresolved:
                if (editReplied.getText().toString().trim().length() > 0) {
                    UpdateCompliant("0");

                }
                if (editReplied.getText().toString().trim().length() <= 0) {
                    editReplied.setError(getString(R.string.write_some_thing_missing));
                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void UpdateCompliant(final String status) {
        if (Util.isNetworkAvailable()) {
            final UpdateReceivedCompliantParams updateReceivedCompliantParams = new UpdateReceivedCompliantParams();
            updateReceivedCompliantParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            updateReceivedCompliantParams.setReason(editReplied.getText().toString());
            updateReceivedCompliantParams.setComplaintid(id);
            updateReceivedCompliantParams.setStatus(status);
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.updateCompliant(headerMap, updateReceivedCompliantParams).enqueue(new Callback<FeedbackPostResponse>() {
                @Override
                public void onResponse(Call<FeedbackPostResponse> call, Response<FeedbackPostResponse> response) {
                    hideProgress();
                    feedbackPostResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200
                            && feedbackPostResponse != null) {
                        if (feedbackPostResponse.getStatuscode() == 0) {
                            Toast.makeText(getActivity(), getString(R.string.success), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), ComplaintActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, feedbackPostResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, feedbackPostResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(feedbackPostResponse.getSource(),
                                    feedbackPostResponse.getSourcedata()));
                            editor.commit();
                            UpdateCompliant(status);
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), ComplaintActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<FeedbackPostResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), ComplaintActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });

        } else

        {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void escalateComplaint() {

        if (Util.isNetworkAvailable()) {
            showProgress();
            ComposeCompliantParams composeCompliantParams = new ComposeCompliantParams();
            composeCompliantParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            composeCompliantParams.setComplaint(editReplied.getText().toString());
            composeCompliantParams.setCategoryid(Categoryid);
            composeCompliantParams.setParentid(String.valueOf(id));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.composenewComplaint(headerMap, composeCompliantParams).enqueue(new Callback<FeedbackPostResponse>() {
                @Override
                public void onResponse(Call<FeedbackPostResponse> call, Response<FeedbackPostResponse> response) {
                    hideProgress();
                    feedbackPostResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && feedbackPostResponse != null) {
                        if (feedbackPostResponse.getStatuscode() == 0) {
                            Toast.makeText(getContext(), getString(R.string.complaint_sucess), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), ComplaintActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, feedbackPostResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, feedbackPostResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(feedbackPostResponse.getSource(),
                                    feedbackPostResponse.getSourcedata()));
                            editor.commit();
                            escalateComplaint();
                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.complaint_failed), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), ComplaintActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    }
                }

                @Override
                public void onFailure(Call<FeedbackPostResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), ComplaintActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);


                }
            });

        } else {
            Toast.makeText(getContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

}
