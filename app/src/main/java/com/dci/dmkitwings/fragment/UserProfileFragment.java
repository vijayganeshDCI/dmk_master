package com.dci.dmkitwings.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v7.widget.LinearLayoutCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.dci.dmkitwings.BuildConfig;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.adapter.BoothAdapter;
import com.dci.dmkitwings.adapter.ConstituencyAdapter;
import com.dci.dmkitwings.adapter.DistrictAdpater;
import com.dci.dmkitwings.adapter.DivisionAdapter;
import com.dci.dmkitwings.adapter.ITWingsAdapter;
import com.dci.dmkitwings.adapter.MunicipalityAdapter;
import com.dci.dmkitwings.adapter.PanchayatUnionListResponse;
import com.dci.dmkitwings.adapter.PanchayatUnionResultsItem;
import com.dci.dmkitwings.adapter.PanchyatUnionAdapter;
import com.dci.dmkitwings.adapter.PartAdapter;
import com.dci.dmkitwings.adapter.PartyRoleAdapter;
import com.dci.dmkitwings.adapter.TownPanchayatAdapter;
import com.dci.dmkitwings.adapter.VillagePanchayatAdapter;
import com.dci.dmkitwings.adapter.WardAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.BoothListInputParam;
import com.dci.dmkitwings.model.BoothListResponse;
import com.dci.dmkitwings.model.BoothListResultsItem;
import com.dci.dmkitwings.model.ConsituencyList;
import com.dci.dmkitwings.model.ConsituencyListResponse;
import com.dci.dmkitwings.model.ConstituencyListResultsItem;
import com.dci.dmkitwings.model.DistrictList;
import com.dci.dmkitwings.model.DistrictListResponse;
import com.dci.dmkitwings.model.DistrictsItem;
import com.dci.dmkitwings.model.DivisionListResponse;
import com.dci.dmkitwings.model.DivisionListResultsItem;
import com.dci.dmkitwings.model.ItWingsListResponse;
import com.dci.dmkitwings.model.ItWingsListResultsItem;
import com.dci.dmkitwings.model.MunicipalityListResponse;
import com.dci.dmkitwings.model.MunicipalityListResultsItem;
import com.dci.dmkitwings.model.PartList;
import com.dci.dmkitwings.model.PartListResponse;
import com.dci.dmkitwings.model.PartListResultsItem;
import com.dci.dmkitwings.model.PartyRoleList;
import com.dci.dmkitwings.model.PartyRoleListResponse;
import com.dci.dmkitwings.model.PartyRoleListResultsItem;
import com.dci.dmkitwings.model.TownPanchayatListResponse;
import com.dci.dmkitwings.model.TownPanchayatResultsItem;
import com.dci.dmkitwings.model.UserDetailsResponse;
import com.dci.dmkitwings.model.UserProfileUpdate;
import com.dci.dmkitwings.model.UserStatusParam;
import com.dci.dmkitwings.model.VattamListResponse;
import com.dci.dmkitwings.model.VattamResultsItem;
import com.dci.dmkitwings.model.VattamWardList;
import com.dci.dmkitwings.model.VillagePanchayatListResponse;
import com.dci.dmkitwings.model.VillagePanchayatResultsItem;
import com.dci.dmkitwings.model.WardListResponse;
import com.dci.dmkitwings.model.WardListResultsItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomEditText;
import com.dci.dmkitwings.view.CustomTextView;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import lib.kingja.switchbutton.SwitchMultiButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by vijayaganesh on 3/22/2018.
 */

public class UserProfileFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {


    @BindView(R.id.guideline_profile)
    Guideline guidelineProfile;
    @BindView(R.id.image_profile_pic)
    CircleImageView imageProfilePic;
    @BindView(R.id.image_app_icon)
    ImageView imageAppIcon;
    @BindView(R.id.text_label_firstname)
    TextView textLabelName;
    @BindView(R.id.edit_first_name)
    EditText editFirstName;
    @BindView(R.id.view_bottom_first_name)
    View viewBottom1;
    @BindView(R.id.text_label_last_name)
    TextView textLabelEmail;
    @BindView(R.id.edit_last_name)
    EditText editLastName;
    @BindView(R.id.view_bottom_last_name)
    View viewBottom2;
    @BindView(R.id.text_label_dob)
    TextView textLabelDob;
    @BindView(R.id.edit_dob)
    EditText editDob;
    @BindView(R.id.view_bottom_gender)
    View viewBottom3;
    @BindView(R.id.text_label_district)
    TextView textLabelDistrict;
    @BindView(R.id.spinner_district)
    Spinner spinnerDistrict;
    @BindView(R.id.view_bottom_district)
    View viewBottom4;
    @BindView(R.id.text_label_constituency)
    TextView textLabelCons;
    @BindView(R.id.spinner_constituency)
    Spinner spinnerConstituency;
    @BindView(R.id.view_bottom_cons)
    View viewBottom5;
    @BindView(R.id.text_label_vilage_panchayat)
    TextView textLabelVillagePanchayat;
    @BindView(R.id.spinner_village_panchayat)
    Spinner spinnerVillagePanchayat;
    @BindView(R.id.view_bottom_village_panchayat)
    View viewBottom6;
    @BindView(R.id.text_label_ward)
    TextView textLabelWard;
    @BindView(R.id.spinner_ward)
    Spinner spinnerWard;
    @BindView(R.id.view_bottom_ward)
    View viewBottom7;
    @BindView(R.id.text_label_wings)
    TextView textLabelWings;
    @BindView(R.id.spinner_wings)
    Spinner spinnerWings;
    @BindView(R.id.view_bottom_wings)
    View viewBottom8;
    @BindView(R.id.button_submit)
    Button buttonSubmit;
    @BindView(R.id.cons_party_member)
    ConstraintLayout consPartyMember;
    Unbinder unbinder;
    @BindView(R.id.text_label_division)
    TextView textLabelType;
    @BindView(R.id.spinner_division)
    Spinner spinnerDivision;
    @BindView(R.id.view_bottom_division)
    View viewBottom9;
    @BindView(R.id.text_label_part)
    TextView textLabelPart;
    @BindView(R.id.spinner_part)
    Spinner spinnerPart;
    @BindView(R.id.view_bottom_part)
    View viewBottom10;
    @BindView(R.id.text_label_party_role)
    TextView textLabelPartyRole;
    @BindView(R.id.spinner_party_role)
    Spinner spinnerPartyRole;
    @BindView(R.id.view_bottom_party_role)
    View viewBottom12;
    @BindView(R.id.switch_gender)
    SwitchMultiButton switchGender;
    @BindView(R.id.text_label_gender_error)
    TextView textlabelGenderError;
    @BindView(R.id.edit_email)
    EditText editEmailid;
    @BindView(R.id.text_label_voter_id)
    CustomTextView textLabelVoterId;
    @BindView(R.id.edit_voter_id)
    CustomEditText editVoterId;
    @BindView(R.id.view_bottom_voter_id)
    View viewBottom20;
    @BindView(R.id.text_label_address)
    CustomTextView textLabelAddress;
    @BindView(R.id.edit_address)
    CustomEditText editAddress;
    @BindView(R.id.view_bottom_address)
    View viewBottom21;
    @BindView(R.id.text_label_booth)
    CustomTextView textLabelBooth;
    @BindView(R.id.spinner_booth)
    Spinner spinnerBooth;
    @BindView(R.id.view_bottom_booth)
    View viewBottom23;
    @BindView(R.id.linear_firstname)
    LinearLayout linearFirstname;
    @BindView(R.id.linear_lastname)
    LinearLayout linearLastname;
    @BindView(R.id.linear_email)
    LinearLayout linearEmail;
    @BindView(R.id.view_bottom_email)
    View viewBottomEmail;
    @BindView(R.id.view_bottom_dob)
    View viewBottomDob;
    @BindView(R.id.text_label_gender)
    CustomTextView textLabelGender;
    @BindView(R.id.text_label_union_type)
    CustomTextView textLabelUnionType;
    @BindView(R.id.spinner_union_type)
    Spinner spinnerUnionType;
    @BindView(R.id.view_bottom_union_type)
    View viewBottomUnionType;
    private Calendar currentD;
    private Calendar calendardate;
    private SimpleDateFormat simpleDateFormat;
    private String dob;
    private static final int MY_REQUEST_CODE_CAMERA = 5;
    private static final int MY_REQUEST_CODE_GALLERY = 6;
    private int REQUEST_CAMERA = 1;
    private int SELECT_FILE = 2;
    private String profilePicture;
    private Uri galleryUri, cameraUri;
    private int age;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    UserDetailsResponse userDetailsResponse;
    UserDetailsResponse userDetailsUpdateResponse;
    ArrayList<String> partyRoleitems = new ArrayList<String>();
    ArrayList<DistrictsItem> districtList;
    DistrictListResponse districtListResponse;
    ArrayList<ItWingsListResultsItem> itWingsListResultsItems;
    ItWingsListResponse itWingsListResponse;
    ArrayList<DivisionListResultsItem> divisionlist;
    DivisionListResponse divisionListResponse;
    ArrayList<PartyRoleListResultsItem> partyRoleListResultsItems;
    PartyRoleListResponse partyRoleListResponse;
    ArrayList<ConstituencyListResultsItem> constituencyList;
    ConsituencyListResponse consituencyListResponse;
    ArrayList<TownPanchayatResultsItem> townPanchayatList;
    TownPanchayatListResponse townPanchayatListResponse;
    ArrayList<VattamResultsItem> vattamList;
    VattamListResponse vattamListResponse;
    ArrayList<WardListResultsItem> wardList;
    WardListResponse wardListResponse;
    ArrayList<MunicipalityListResultsItem> municipalityList;
    MunicipalityListResponse municipalityListResponse;
    ArrayList<PanchayatUnionResultsItem> panchayatList;
    PanchayatUnionListResponse panchayatUnionListResponse;
    ArrayList<VillagePanchayatResultsItem> villagePanchayatResultsItemArrayList;
    VillagePanchayatListResponse villagePanchayatListResponse;
    ArrayList<PartListResultsItem> partList;
    PartListResponse partListResponse;
    private int gender = 2;
    TelephonyManager telephonyManager;
    private MenuItem update;
    private String selectedImage;
    BoothListResponse boothListResponse;
    ArrayList<BoothListResultsItem> boothList;
    private int villageID;
    private int wardID;

    private SharedPreferences fcmSharedPrefrences;
    private PickSetup setup;

    @SuppressLint("MissingPermission")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_party_member, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        setHasOptionsMenu(true);
        editor = sharedPreferences.edit();
        editor.putString(DmkConstants.DEVICEID, Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID)).commit();
        editor.putString(DmkConstants.APPID, getActivity().getPackageName()).commit();
        editor.putString(DmkConstants.APPVERSION_CODE, BuildConfig.VERSION_NAME).commit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(DmkConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        editor.putString(DmkConstants.IMEIID, telephonyManager.getDeviceId()).commit();

        editFirstName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSpecialCharacters()});
        editLastName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSpecialCharacters()});
        editVoterId.setFilters(new InputFilter[]{editTextRestrictLength()});
//        editEmailid.setFilters(new InputFilter[]{editTextRestrictSmileys()});
//        editAddress.setFilters(new InputFilter[]{editTextRestrictSmileys()});


        districtList = new ArrayList<DistrictsItem>();
        itWingsListResultsItems = new ArrayList<ItWingsListResultsItem>();
        divisionlist = new ArrayList<DivisionListResultsItem>();
        partyRoleListResultsItems = new ArrayList<PartyRoleListResultsItem>();
        constituencyList = new ArrayList<ConstituencyListResultsItem>();
        townPanchayatList = new ArrayList<TownPanchayatResultsItem>();
        vattamList = new ArrayList<VattamResultsItem>();
        wardList = new ArrayList<WardListResultsItem>();
        municipalityList = new ArrayList<MunicipalityListResultsItem>();
        panchayatList = new ArrayList<PanchayatUnionResultsItem>();
        villagePanchayatResultsItemArrayList = new ArrayList<VillagePanchayatResultsItem>();
        partList = new ArrayList<PartListResultsItem>();
        boothList = new ArrayList<BoothListResultsItem>();
        buttonSubmit.setText(getString(R.string.save));
        buttonSubmit.setVisibility(View.GONE);
        ArrayAdapter<String> adapterUnionType = new
                ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().getStringArray(R.array.panchayat_union_list));
        spinnerUnionType.setAdapter(adapterUnionType);
        if (Util.isNetworkAvailable()) {
            showProgress();
            getUserDetails();

        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
        switchGender.setOnSwitchListener
                (new SwitchMultiButton.OnSwitchListener() {
                    @Override
                    public void onSwitch(int position, String tabText) {
                        textlabelGenderError.setError(null);
                        gender = position;
                    }
                });

        setup = new PickSetup()
                .setTitle("")
                .setProgressText(getString(R.string.loading))
                .setCancelText(getString(R.string.Alert_Cancel))
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText(getString(R.string.camera))
                .setGalleryButtonText(getString(R.string.gallery))
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setGalleryIcon(R.mipmap.icon_gallery)
                .setCameraIcon(R.mipmap.icon_photo_camera).setWidth(100).setHeight(100).setFlip(true);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save, menu);
        update = menu.findItem(R.id.menu_bar_save);
        update.setTitle(R.string.submit);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_save:
                android.support.v7.app.AlertDialog.Builder alertDialog1 =
                        new android.support.v7.app.AlertDialog.Builder(getActivity());
                alertDialog1.setMessage(getString(R.string.confirm_to_update));
                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        profileNullCheck();
                    }
                });
                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        hideProgress();
                    }
                });
                alertDialog1.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        try {
//            if (resultCode == Activity.RESULT_OK) {
//                if (requestCode == SELECT_FILE)
//                    onSelectFromGalleryResult(data);
//                else if (requestCode == REQUEST_CAMERA)
//                    onCaptureImageResult(data);
//            }
//        } catch (Exception e) {
//            Toast.makeText(getActivity(), "Upload only Image", Toast.LENGTH_SHORT)
//                    .show();
//        }
//    }

    @OnClick({R.id.image_profile_pic, R.id.edit_dob, R.id.button_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_profile_pic:
                pickImage();
                break;
            case R.id.edit_dob:
                editDob.setError(null);
                datePickerDialog();
                break;
            case R.id.button_submit:
                profileNullCheck();

                break;
        }
    }

    private void pickImage() {
        PickImageDialog.build(setup)
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //Image path
                            //r.getPath();

                            //If you want the Bitmap.
                            imageProfilePic.setImageBitmap(r.getBitmap());
                            galleryUri = r.getUri();
                            selectedImage = System.currentTimeMillis() + ".jpg";
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getActivity(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }

                    }
                })
                .setOnPickCancel(new IPickCancel() {
                    @Override
                    public void onCancelClick() {
                    }
                }).show(getActivity().getSupportFragmentManager());
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dob = simpleDateFormat.format(calendardate.getTime());
        editDob.setText(dob);
        age = Integer.parseInt(getAge(year, monthOfYear, dayOfMonth));

    }


    private void getUserDetails() {
        if (Util.isNetworkAvailable()) {
            partyRoleitems.clear();
            UserStatusParam userStatusParam = new UserStatusParam();
//            userStatusParam.setPhoneNumber((sharedPreferences.getString(DmkConstants.PHONENUMBER, null)));
            userStatusParam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            userStatusParam.setImeiid(sharedPreferences.getString(DmkConstants.IMEIID, ""));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getUserDetails(headerMap, userStatusParam).enqueue(new Callback<UserDetailsResponse>() {
                @Override
                public void onResponse(Call<UserDetailsResponse> call, Response<UserDetailsResponse> response) {
                    userDetailsResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (userDetailsResponse.getStatuscode() == 0) {
                            if (userDetailsResponse.getResults() != null) {
                                editor.putString(DmkConstants.USERPERMISSION_COMMON,
                                        userDetailsResponse.getResults().getUserPermissionset().getCommon() != null ?
                                                userDetailsResponse.getResults().getUserPermissionset().getCommon() : "");
                                editor.putString(DmkConstants.USERPERMISSION_ARTICLE,
                                        userDetailsResponse.getResults().getUserPermissionset().getArticle() != null ?
                                                userDetailsResponse.getResults().getUserPermissionset().getArticle() : "");
                                editor.putString(DmkConstants.USERPERMISSION_EVENTS,
                                        userDetailsResponse.getResults().getUserPermissionset().getEvents() != null ?
                                                userDetailsResponse.getResults().getUserPermissionset().getEvents() : "");
                                editor.putString(DmkConstants.USERPERMISSION_NEWS,
                                        userDetailsResponse.getResults().getUserPermissionset().getNews() != null ?
                                                userDetailsResponse.getResults().getUserPermissionset().getNews() : "");
                                editor.putString(DmkConstants.USERPERMISSION_ELECTION,
                                        userDetailsResponse.getResults().getUserPermissionset().getElection() != null ?
                                                userDetailsResponse.getResults().getUserPermissionset().getElection() : "");
                                editor.commit();
                                getDistrictList();
                                universalImageLoader(imageProfilePic, getString(R.string.s3_bucket_profile_path),
                                        userDetailsResponse.getResults().getAvatar(), R.mipmap.icon_pro_image_loading_256,
                                        R.mipmap.icon_pro_image_loading_256);
                            } else {
                                hideProgress();
                                Toast.makeText(getActivity(), userDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, userDetailsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, userDetailsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(userDetailsResponse.getSource(),
                                    userDetailsResponse.getSourcedata()));
                            editor.commit();
                            getUserDetails();
                        }
                    } else {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<UserDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();


                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    public void getDistrictList() {
        if (Util.isNetworkAvailable()) {
            districtList.clear();
            districtList.add(0, new DistrictsItem(getString(R.string.select_district), 0));
            DistrictList districtListparam = new DistrictList();
            districtListparam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getDistrictList(headerMap, districtListparam).enqueue(new Callback<DistrictListResponse>() {
                @Override
                public void onResponse(Call<DistrictListResponse> call, Response<DistrictListResponse> response) {
                    districtListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (districtListResponse.getResults() != null) {
                            for (int i = 0; i < districtListResponse.getResults().getDistricts().size(); i++) {

                                districtList.add(new DistrictsItem(
                                        districtListResponse.getResults().getDistricts().get(i).getDistrictName() != null ?
                                                districtListResponse.getResults().getDistricts().get(i).getDistrictName() : "",
                                        districtListResponse.getResults().getDistricts().get(i).getId()));
                            }
                            DistrictAdpater adapterdivisionList = new DistrictAdpater(districtList, getActivity());
                            spinnerDistrict.setAdapter(adapterdivisionList);
                            getConsituencyList();
                        } else {
                            Toast.makeText(getActivity(), districtListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.no_it_district), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DistrictListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    public void getConsituencyList() {
        if (Util.isNetworkAvailable()) {
            constituencyList.clear();
            constituencyList.add(0, new ConstituencyListResultsItem(getString(R.string.select_constituency), 0));
            ConsituencyList consituencyList = new ConsituencyList();
            consituencyList.setDistrictID(userDetailsResponse.getResults().getDistrictID());
            consituencyList.setType(1);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getConstituencyList(headerMap, consituencyList).enqueue(new Callback<ConsituencyListResponse>() {
                @Override
                public void onResponse(Call<ConsituencyListResponse> call, Response<ConsituencyListResponse> response) {
                    consituencyListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (consituencyListResponse.getResults() != null) {
                            if (consituencyListResponse.getResults().size() > 0) {
                                spinnerConstituency.setVisibility(View.VISIBLE);
                                textLabelCons.setVisibility(View.VISIBLE);
                                viewBottom5.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < consituencyListResponse.getResults().size(); i++) {
                                constituencyList.add(new ConstituencyListResultsItem(
                                        consituencyListResponse.getResults().get(i).getConstituencyName() != null ?
                                                consituencyListResponse.getResults().get(i).getConstituencyName() : "",
                                        consituencyListResponse.getResults().get(i).getId()));
                            }
                            ConstituencyAdapter adapterConstituencyList = new ConstituencyAdapter(constituencyList, getContext());
                            spinnerConstituency.setAdapter(adapterConstituencyList);
                            getDivisionList();
                        } else {
                            Toast.makeText(getActivity(), consituencyListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.no_it_constituency), Toast.LENGTH_SHORT).show();
                        spinnerConstituency.setVisibility(View.GONE);
                        textLabelCons.setVisibility(View.GONE);
                        viewBottom5.setVisibility(View.GONE);

                    }
                }

                @Override
                public void onFailure(Call<ConsituencyListResponse> call, Throwable t) {
                    hideProgress();
                    spinnerConstituency.setVisibility(View.GONE);
                    textLabelCons.setVisibility(View.GONE);
                    viewBottom5.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();


                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();

        }
    }


    public void getDivisionList() {
        if (Util.isNetworkAvailable()) {
            divisionlist.clear();
            divisionlist.add(0, new DivisionListResultsItem(getString(R.string.select_division), 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getDivisionList(headerMap).enqueue(new Callback<DivisionListResponse>() {
                @Override
                public void onResponse(Call<DivisionListResponse> call, Response<DivisionListResponse> response) {
                    divisionListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (divisionListResponse.getResults() != null) {
                            for (int i = 0; i < divisionListResponse.getResults().size(); i++) {
                                divisionlist.add(new DivisionListResultsItem(
                                        divisionListResponse.getResults().get(i).getDivisionname() != null ?
                                                divisionListResponse.getResults().get(i).getDivisionname() : "",
                                        divisionListResponse.getResults().get(i).getId()));
                            }
                            DivisionAdapter adapterdivisionList = new DivisionAdapter(divisionlist, getActivity());
                            spinnerDivision.setAdapter(adapterdivisionList);

                            switch (userDetailsResponse.getResults().getTypeid()) {
                                case 1:
                                    getPartList();
                                    break;
                                case 2:
                                    getMunicipalityList();
                                    break;
                                case 3:
                                    spinnerUnionType.setVisibility(View.VISIBLE);
                                    textLabelUnionType.setVisibility(View.VISIBLE);
                                    viewBottomUnionType.setVisibility(View.VISIBLE);
                                    getPanchayatUnionList();
                                    break;
                            }
                        } else {
                            Toast.makeText(getActivity(), divisionListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.no_it_division), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DivisionListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getPartList() {
        if (Util.isNetworkAvailable()) {
            partList.clear();
            partList.add(0, new PartListResultsItem(getString(R.string.select_part), 0));
            PartList partListParam = new PartList();
            partListParam.setConstituencyID(userDetailsResponse.getResults().getConstituencyID());
            partListParam.setDistrictID(userDetailsResponse.getResults().getDistrictID());
            partListParam.setDivisionID(userDetailsResponse.getResults().getTypeid());
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPartList(headerMap, partListParam).enqueue(new Callback<PartListResponse>() {
                @Override
                public void onResponse(Call<PartListResponse> call, Response<PartListResponse> response) {
                    partListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (partListResponse.getResults() != null) {
                            showType(true, getString(R.string.part));

                            for (int i = 0; i < partListResponse.getResults().size(); i++) {
                                partList.add(new PartListResultsItem(
                                        partListResponse.getResults().get(i).getPartname() != null ?
                                                partListResponse.getResults().get(i).getPartname() : "",
                                        partListResponse.getResults().get(i).getId()));
                            }
                            PartAdapter partAdapter = new PartAdapter(partList, getActivity());
                            spinnerPart.setAdapter(partAdapter);
                            getWardList();
                        } else {
                            hideProgress();
                            Toast.makeText(getActivity(), partListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            showType(false, "");
                        }
                    } else {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.no_it_part), Toast.LENGTH_SHORT).show();
                        showType(false, "");
                    }
                }

                @Override
                public void onFailure(Call<PartListResponse> call, Throwable t) {
                    hideProgress();
                    showType(false, "");
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    public void getMunicipalityList() {
        if (Util.isNetworkAvailable()) {
            municipalityList.clear();
            municipalityList.add(0, new MunicipalityListResultsItem(0, getString(R.string.select_town_municipality)));
            PartList partListParam = new PartList();
            partListParam.setConstituencyID(userDetailsResponse.getResults().getConstituencyID());
            partListParam.setDistrictID(userDetailsResponse.getResults().getDistrictID());
            partListParam.setDivisionID(userDetailsResponse.getResults().getTypeid());
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getMunicipalitytList(headerMap, partListParam).enqueue(new Callback<MunicipalityListResponse>() {
                @Override
                public void onResponse(Call<MunicipalityListResponse> call, Response<MunicipalityListResponse> response) {

                    municipalityListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (municipalityListResponse.getResults() != null) {
                            if (municipalityListResponse.getResults().size() > 0) {
                                showType(true, getString(R.string.municipallity));
                            }
                            for (int i = 0; i < municipalityListResponse.getResults().size(); i++) {
                                municipalityList.add(new MunicipalityListResultsItem(
                                        municipalityListResponse.getResults().get(i).getId(),
                                        municipalityListResponse.getResults().get(i).getMunicipalityname() != null ?
                                                municipalityListResponse.getResults().get(i).getMunicipalityname() : ""));
                            }
                            MunicipalityAdapter partAdapter = new MunicipalityAdapter(municipalityList, getActivity());
                            spinnerPart.setAdapter(partAdapter);
                            getWardList();
                        } else {
                            hideProgress();
                            showType(false, "");
                            Toast.makeText(getActivity(), municipalityListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        //getVattamList();
                    } else {
                        hideProgress();
                        showType(false, "");
                        Toast.makeText(getActivity(), getString(R.string.no_it_municipality), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<MunicipalityListResponse> call, Throwable t) {
                    hideProgress();
                    showType(false, "");
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    public void getPanchayatUnionList() {
        if (Util.isNetworkAvailable()) {
            panchayatList.clear();
            panchayatList.add(0, new PanchayatUnionResultsItem(getString(R.string.select_panchayat_union), 0));
            final PartList partListParam = new PartList();
            partListParam.setConstituencyID(userDetailsResponse.getResults().getConstituencyID());
            partListParam.setDistrictID(userDetailsResponse.getResults().getDistrictID());
            partListParam.setDivisionID(userDetailsResponse.getResults().getTypeid());
            partListParam.setUnionType(userDetailsResponse.getResults().getUniontype());
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPanchayatUnionList(headerMap, partListParam).enqueue(new Callback<PanchayatUnionListResponse>() {
                @Override
                public void onResponse(Call<PanchayatUnionListResponse> call, Response<PanchayatUnionListResponse> response) {

                    panchayatUnionListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (panchayatUnionListResponse.getResults() != null) {
                            if (panchayatUnionListResponse.getResults().size() > 0) {
                                showType(true, getString(R.string.pachayat_union));
                            }
                            for (int i = 0; i < panchayatUnionListResponse.getResults().size(); i++) {
                                panchayatList.add(new PanchayatUnionResultsItem(
                                        panchayatUnionListResponse.getResults().get(i).getVillagePanchayatName() != null ?
                                                panchayatUnionListResponse.getResults().get(i).getVillagePanchayatName() : "",
                                        panchayatUnionListResponse.getResults().get(i).getId()));
                            }
                            PanchyatUnionAdapter partAdapter = new PanchyatUnionAdapter(panchayatList, getActivity());
                            spinnerPart.setAdapter(partAdapter);
                            if (userDetailsResponse.getResults().getUniontype() == 1) {
                                getTownPanchayatList();
                            } else {
                                getVillagePanchayatList();
                            }
                        } else {
                            hideProgress();
                            showType(false, "");
                            Toast.makeText(getActivity(), panchayatUnionListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        hideProgress();
                        showType(false, "");
                        Toast.makeText(getActivity(), getString(R.string.no_it_panchayat), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PanchayatUnionListResponse> call, Throwable t) {
                    hideProgress();
                    showType(false, "");
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    public void getTownPanchayatList() {
        if (Util.isNetworkAvailable()) {
            townPanchayatList.clear();
            townPanchayatList.add(0, new TownPanchayatResultsItem(getString(R.string.select_town_panchayat), 0));
            final PartList partListParam = new PartList();
            partListParam.setConstituencyID(userDetailsResponse.getResults().getConstituencyID());
            partListParam.setDistrictID(userDetailsResponse.getResults().getDistrictID());
            partListParam.setUnionID(userDetailsResponse.getResults().getPanchayatunion());
            partListParam.setUnionType(userDetailsResponse.getResults().getUniontype());
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getTownPanchayatList(headerMap, partListParam).enqueue(new Callback<TownPanchayatListResponse>() {
                @Override
                public void onResponse(Call<TownPanchayatListResponse> call, Response<TownPanchayatListResponse> response) {

                    townPanchayatListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (townPanchayatListResponse.getResults() != null) {
                            spinnerPart.setVisibility(View.VISIBLE);
                            if (townPanchayatListResponse.getResults().size() > 0) {
                                showType(true, getString(R.string.town_pachayat));
                            }
                            for (int i = 0; i < townPanchayatListResponse.getResults().size(); i++) {
                                townPanchayatList.add(new TownPanchayatResultsItem(
                                        townPanchayatListResponse.getResults().get(i).getTownpanchayatname() != null ?
                                                townPanchayatListResponse.getResults().get(i).getTownpanchayatname() : "",
                                        townPanchayatListResponse.getResults().get(i).getId()));
                            }
                            TownPanchayatAdapter partAdapter = new TownPanchayatAdapter(townPanchayatList, getActivity());
                            spinnerVillagePanchayat.setAdapter(partAdapter);
                            getWardList();
                        } else {
                            hideProgress();
                            Toast.makeText(getActivity(), townPanchayatListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        //getVattamList();
                    } else {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.no_it_town_panchayat), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TownPanchayatListResponse> call, Throwable t) {
                    hideProgress();

                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    public void getVillagePanchayatList() {
        if (Util.isNetworkAvailable()) {

            villagePanchayatResultsItemArrayList.clear();
            villagePanchayatResultsItemArrayList.add(0, new VillagePanchayatResultsItem(getString(R.string.select_village_panchayat), 0));
            final VattamWardList vattamWardListParam = new VattamWardList();
            vattamWardListParam.setUnionID(userDetailsResponse.getResults().getPanchayatunion());
            vattamWardListParam.setConstituencyID(userDetailsResponse.getResults().getConstituencyID());
            vattamWardListParam.setDistrictID(userDetailsResponse.getResults().getDistrictID());
            vattamWardListParam.setUnionType(userDetailsResponse.getResults().getUniontype());
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getVillagePanchayatList(headerMap, vattamWardListParam).enqueue(new Callback<VillagePanchayatListResponse>() {
                @Override
                public void onResponse(Call<VillagePanchayatListResponse> call, Response<VillagePanchayatListResponse> response) {

                    villagePanchayatListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (villagePanchayatListResponse.getResults() != null) {
                            if (villagePanchayatListResponse.getResults().size() > 0) {
                                spinnerVillagePanchayat.setVisibility(View.VISIBLE);
                                textLabelVillagePanchayat.setVisibility(View.VISIBLE);
                                viewBottom6.setVisibility(View.VISIBLE);
                                textLabelVillagePanchayat.setText(getString(R.string.village_panchayat));
                            }
                            for (int i = 0; i < villagePanchayatListResponse.getResults().size(); i++) {
                                villagePanchayatResultsItemArrayList.add(new VillagePanchayatResultsItem(
                                        villagePanchayatListResponse.getResults().get(i).getVillageName() != null ?
                                                villagePanchayatListResponse.getResults().get(i).getVillageName() : "",
                                        villagePanchayatListResponse.getResults().get(i).getId()));
                            }
                            VillagePanchayatAdapter vattamAdapter = new VillagePanchayatAdapter(villagePanchayatResultsItemArrayList, getActivity());
                            spinnerVillagePanchayat.setAdapter(vattamAdapter);
                            getBoothList();
                        } else {
                            hideProgress();
                            spinnerVillagePanchayat.setVisibility(View.GONE);
                            textLabelVillagePanchayat.setVisibility(View.GONE);
                            viewBottom6.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), villagePanchayatListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        hideProgress();
                        spinnerVillagePanchayat.setVisibility(View.GONE);
                        textLabelVillagePanchayat.setVisibility(View.GONE);
                        viewBottom6.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), getString(R.string.no_it_village_panchayat), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<VillagePanchayatListResponse> call, Throwable t) {
                    hideProgress();
                    spinnerVillagePanchayat.setVisibility(View.GONE);
                    textLabelVillagePanchayat.setVisibility(View.GONE);
                    viewBottom6.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    public void getWardList() {
        if (Util.isNetworkAvailable()) {
            wardList.clear();
            wardList.add(0, new WardListResultsItem(getString(R.string.select_ward), 0));
            final VattamWardList vattamWardListParam = new VattamWardList();
            switch (userDetailsResponse.getResults().getTypeid()) {
                case 1:
                    vattamWardListParam.setDivisionID(userDetailsResponse.getResults().getTypeid());
                    vattamWardListParam.setPartID(userDetailsResponse.getResults().getPartid());
                    break;
                case 2:
                    vattamWardListParam.setDivisionID(userDetailsResponse.getResults().getTypeid());
                    vattamWardListParam.setPartID(userDetailsResponse.getResults().getMunicipalityid());
                    break;
                case 3:
                    vattamWardListParam.setDivisionID(userDetailsResponse.getResults().getTypeid());
                    vattamWardListParam.setPartID(userDetailsResponse.getResults().getPanchayatunion());
                    break;
            }
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));

            dmkAPI.getWardList(headerMap, vattamWardListParam).enqueue(new Callback<WardListResponse>() {
                @Override
                public void onResponse(Call<WardListResponse> call, Response<WardListResponse> response) {

                    wardListResponse = response.body();

                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (wardListResponse.getResults() != null) {
                            if (wardListResponse.getResults().size() > 0) {
                                spinnerWard.setVisibility(View.VISIBLE);
                                textLabelWard.setVisibility(View.VISIBLE);
                                viewBottom7.setVisibility(View.VISIBLE);
                            }

                            for (int i = 0; i < wardListResponse.getResults().size(); i++) {
                                wardList.add(new WardListResultsItem(
                                        wardListResponse.getResults().get(i).getWardname() != null ?
                                                wardListResponse.getResults().get(i).getWardname() : "",
                                        wardListResponse.getResults().get(i).getId()));
                            }
                            WardAdapter wardAdapter = new WardAdapter(wardList, getActivity());
                            spinnerWard.setAdapter(wardAdapter);
//                        getItWingsList();
                            getBoothList();
                        } else {
                            hideProgress();
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom7.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), wardListResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        hideProgress();
                        spinnerWard.setVisibility(View.GONE);
                        textLabelWard.setVisibility(View.GONE);
                        viewBottom7.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), getString(R.string.no_it_ward), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<WardListResponse> call, Throwable t) {
                    hideProgress();
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottom7.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getBoothList() {
        if (Util.isNetworkAvailable()) {

            boothList.clear();
            boothList.add(0, new BoothListResultsItem(getString(R.string.select_booth), 0));
            BoothListInputParam boothListInputParam = new BoothListInputParam();
            boothListInputParam.setDivisionID(userDetailsResponse.getResults().getTypeid());
            //boothListInputParam.setPartID(userDetailsResponse.getResults().getPartid());
            boothListInputParam.setWardID(userDetailsResponse.getResults().getWardID());
            boothListInputParam.setVillageID(userDetailsResponse.getResults().getVillagepanchayat());
            boothListInputParam.setUnionType(userDetailsResponse.getResults().getUniontype());
            switch (userDetailsResponse.getResults().getTypeid()) {
                case 1:
                    boothListInputParam.setPartID(userDetailsResponse.getResults().getPartid());
                    break;
                case 2:
                    boothListInputParam.setPartID(userDetailsResponse.getResults().getMunicipalityid());
                    break;
                case 3:
                    boothListInputParam.setPartID(userDetailsResponse.getResults().getPanchayatunion());
                    break;

            }
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getBoothList(headerMap, boothListInputParam).enqueue(new Callback<BoothListResponse>() {
                @Override
                public void onResponse(Call<BoothListResponse> call, Response<BoothListResponse> response) {

                    boothListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (boothListResponse.getResults() != null) {
                            if (boothListResponse.getResults().size() > 0) {
                                spinnerBooth.setVisibility(View.VISIBLE);
                                textLabelBooth.setVisibility(View.VISIBLE);
                                viewBottom23.setVisibility(View.VISIBLE);
                            }

                            for (int i = 0; i < boothListResponse.getResults().size(); i++) {
                                boothList.add(new BoothListResultsItem(
                                        boothListResponse.getResults().get(i).getBoothname() != null ?
                                                boothListResponse.getResults().get(i).getBoothname() : "",
                                        boothListResponse.getResults().get(i).getId()));
                            }
                            BoothAdapter boothAdapter = new BoothAdapter(boothList, getActivity());
                            spinnerBooth.setAdapter(boothAdapter);
                            getItWingsList();
                        } else {
                            hideProgress();
                            spinnerBooth.setVisibility(View.GONE);
                            textLabelBooth.setVisibility(View.GONE);
                            viewBottom23.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), boothListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        hideProgress();
                        spinnerBooth.setVisibility(View.GONE);
                        textLabelBooth.setVisibility(View.GONE);
                        viewBottom23.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), getString(R.string.no_booth_list_found), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<BoothListResponse> call, Throwable t) {
                    hideProgress();
                    spinnerBooth.setVisibility(View.GONE);
                    textLabelBooth.setVisibility(View.GONE);
                    viewBottom23.setVisibility(View.GONE);
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    public void getItWingsList() {
        itWingsListResultsItems.clear();
        itWingsListResultsItems.add(0, new ItWingsListResultsItem(0, getString(R.string.select_wing)));
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
        dmkAPI.getItWingsList(headerMap).enqueue(new Callback<ItWingsListResponse>() {
            @Override
            public void onResponse(Call<ItWingsListResponse> call, Response<ItWingsListResponse> response) {
                itWingsListResponse = response.body();
                if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                    if (itWingsListResponse.getResults() != null) {
                        for (int i = 0; i < itWingsListResponse.getResults().size(); i++) {
                            itWingsListResultsItems.add(new ItWingsListResultsItem(
                                    itWingsListResponse.getResults().get(i).getId(),
                                    itWingsListResponse.getResults().get(i).getWingName() != null ?
                                            itWingsListResponse.getResults().get(i).getWingName() : ""));
                        }
                        ITWingsAdapter itWingsAdapter = new ITWingsAdapter(itWingsListResultsItems, getActivity());
                        spinnerWings.setAdapter(itWingsAdapter);
                        getPartyRoleList();
                    } else {
                        hideProgress();
                        Toast.makeText(getActivity(), itWingsListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ItWingsListResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(getActivity(), getString(R.string.no_it_wing), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getPartyRoleList() {
        if (Util.isNetworkAvailable()) {
            partyRoleListResultsItems.clear();
            partyRoleListResultsItems.add(0, new PartyRoleListResultsItem(0, getString(R.string.select_Party_role)));
            final PartyRoleList partyRoleListparam = new PartyRoleList();
            partyRoleListparam.setwingID(userDetailsResponse.getResults().getWings().getId());
            partyRoleListparam.setDivisionID(userDetailsResponse.getResults().getTypeid());
            partyRoleListparam.setUnionType(userDetailsResponse.getResults().getUniontype());
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPartyRoleList(headerMap, partyRoleListparam).enqueue(new Callback<PartyRoleListResponse>() {
                @Override
                public void onResponse(Call<PartyRoleListResponse> call, Response<PartyRoleListResponse> response) {
                    hideProgress();
                    partyRoleListResponse = response.body();

                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {

                        for (int i = 0; i < partyRoleListResponse.getResults().size(); i++) {
                            partyRoleListResultsItems.add(new PartyRoleListResultsItem(
                                    partyRoleListResponse.getResults().get(i).getId(),
                                    partyRoleListResponse.getResults().get(i).getRolename() != null ?
                                            partyRoleListResponse.getResults().get(i).getRolename() : ""));
                        }
                        PartyRoleAdapter partyRoleAdapter = new PartyRoleAdapter(partyRoleListResultsItems, getActivity());
                        spinnerPartyRole.setAdapter(partyRoleAdapter);
                        setUserDetails();

                    } else {
                        hideProgress();
                        Toast.makeText(getActivity(), getString(R.string.no_it_party_role), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<PartyRoleListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void setUserDetails() {
        hideProgress();
        editFirstName.setText(userDetailsResponse.getResults().getFirstName());
        editLastName.setText(userDetailsResponse.getResults().getLastName());
        editDob.setText(userDetailsResponse.getResults().getDOB());
        editEmailid.setText(userDetailsResponse.getResults().getEmail());
        editAddress.setText(userDetailsResponse.getResults().getAddress());
        editVoterId.setText(userDetailsResponse.getResults().getVoterID());

        age = userDetailsResponse.getResults().getAge();
        if (userDetailsResponse.getResults().getGender().equals("0")) {
            switchGender.setSelectedTab(0);
        } else {
            switchGender.setSelectedTab(1);
        }
        for (int i = 0; i < districtList.size(); i++) {
            if (districtList.get(i).getId() == userDetailsResponse.getResults().getDistrictID()) {
                spinnerDistrict.setSelection(i);
                break;
            }
        }

        for (int i = 0; i < itWingsListResultsItems.size(); i++) {
            if (itWingsListResultsItems.get(i).getId() == userDetailsResponse.getResults().getWings().getId()) {
                spinnerWings.setSelection(i);
                break;
            }
        }


        for (int i = 0; i < partyRoleListResultsItems.size(); i++) {
            if (partyRoleListResultsItems.get(i).getId() == userDetailsResponse.getResults().getRoles().getId()) {
                spinnerPartyRole.setSelection(i);
                break;
            }
        }

        for (int i = 0; i < constituencyList.size(); i++) {
            if (constituencyList.get(i).getId() == userDetailsResponse.getResults().getConstituencyID()) {
                spinnerConstituency.setSelection(i);
                break;
            }
        }
        for (int i = 0; i < boothList.size(); i++) {
            if (boothList.get(i).getId() == userDetailsResponse.getResults().getBoothID()) {
                spinnerBooth.setSelection(i);
                break;
            }
        }
        switch (userDetailsResponse.getResults().getTypeid()) {
            case 1:
                //Corporation
                for (int i = 0; i < divisionlist.size(); i++) {
                    if (divisionlist.get(i).getId() == userDetailsResponse.getResults().getTypeid()) {
                        spinnerDivision.setSelection(i);
                        break;
                    }
                }
                for (int i = 0; i < partList.size(); i++) {
                    if (partList.get(i).getId() == userDetailsResponse.getResults().getPartid()) {
                        spinnerPart.setSelection(i);
                        break;
                    }
                }
                for (int i = 0; i < wardList.size(); i++) {
                    if (wardList.get(i).getId() == userDetailsResponse.getResults().getWardID()) {
                        spinnerWard.setSelection(i);
                        break;
                    }
                }
                break;
            case 2:
                //Municipality
                for (int i = 0; i < divisionlist.size(); i++) {
                    if (divisionlist.get(i).getId() == userDetailsResponse.getResults().getTypeid()) {
                        spinnerDivision.setSelection(i);

                        break;
                    }
                }
                for (int i = 0; i < municipalityList.size(); i++) {
                    if (municipalityList.get(i).getId() == userDetailsResponse.getResults().getMunicipalityid()) {
                        spinnerPart.setSelection(i);

                        break;
                    }
                }
                for (int i = 0; i < wardList.size(); i++) {
                    if (wardList.get(i).getId() == userDetailsResponse.getResults().getWardID()) {
                        spinnerWard.setSelection(i);
                        break;
                    }
                }
                break;
            case 3:
                //PanchayatUnion
                for (int i = 0; i < divisionlist.size(); i++) {
                    if (divisionlist.get(i).getId() == userDetailsResponse.getResults().getTypeid()) {
                        spinnerDivision.setSelection(i);

                        break;
                    }
                }
                spinnerUnionType.setSelection(userDetailsResponse.getResults().getUniontype());
                for (int i = 0; i < panchayatList.size(); i++) {
                    if (panchayatList.get(i).getId() == userDetailsResponse.getResults().getPanchayatunion()) {
                        spinnerPart.setSelection(i);
                        break;
                    }
                }

                if (userDetailsResponse.getResults().getUniontype() == 1) {
                    for (int i = 0; i < townPanchayatList.size(); i++) {
                        if (townPanchayatList.get(i).getId() == userDetailsResponse.getResults().getTownshipid()) {
                            spinnerVillagePanchayat.setSelection(i);
                            break;
                        }
                    }
                } else {
                    for (int i = 0; i < villagePanchayatResultsItemArrayList.size(); i++) {
                        if (villagePanchayatResultsItemArrayList.get(i).getId() == userDetailsResponse.getResults().getVillagepanchayat()) {
                            spinnerVillagePanchayat.setSelection(i);
                            break;
                        }
                    }
                }

                for (int i = 0; i < wardList.size(); i++) {
                    if (wardList.get(i).getId() == userDetailsResponse.getResults().getWardID()) {
                        spinnerWard.setSelection(i);
                        break;
                    }
                }
                break;
        }
//        editVoterId.setEnabled(false);
//        editVoterId.setFocusable(false);
        spinnerDistrict.setEnabled(false);
        spinnerWings.setEnabled(false);
        spinnerDivision.setEnabled(false);
        spinnerConstituency.setEnabled(false);
        spinnerPartyRole.setEnabled(false);
        spinnerPart.setEnabled(false);
        spinnerVillagePanchayat.setEnabled(false);
        spinnerWard.setEnabled(false);
        spinnerBooth.setEnabled(false);
        spinnerUnionType.setEnabled(false);
    }


    private void profileNullCheck() {
        if (editFirstName.getText().toString() != null && editFirstName.getText().toString().length() > 0 && !editFirstName.getText().toString().isEmpty()
                && !editLastName.getText().toString().isEmpty() && editLastName.getText().toString().length() > 0 && editLastName.getText().toString() != null
                && !editVoterId.getText().toString().isEmpty() && editVoterId.getText().toString().length() > 0 && editVoterId.getText().toString() != null
                && !editAddress.getText().toString().isEmpty() && editAddress.getText().toString().length() > 0 && editAddress.getText().toString() != null
                && !editDob.getText().toString().isEmpty() && editDob.getText().toString() != null && editDob.getText().toString().length() > 0
                && gender != 2 && Util.isValidEmailAddress(editEmailid.getText().toString())) {
            uploadMedia();
        } else {
            if (editFirstName.getText().toString() == null || editFirstName.getText().toString().isEmpty() ||
                    editFirstName.getText().toString().length() < 0) {
                editFirstName.setError(getString(R.string.name_missing));
            }
            if (editLastName.getText().toString() == null || editLastName.getText().toString().isEmpty()
                    || editLastName.getText().toString().length() < 0) {
                editLastName.setError(getString(R.string.last_name));
            }
            if (editDob.getText().toString() == null || editDob.getText().toString().isEmpty()
                    || editDob.getText().toString().length() < 0) {
                editDob.setError(getString(R.string.dob_missing));
            }
            if (editVoterId.getText().toString() == null || editVoterId.getText().toString().isEmpty()
                    || editVoterId.getText().toString().length() < 0) {
                editVoterId.setError(getString(R.string.voter_ID_missing));
            }
            if (editAddress.getText().toString() == null || editAddress.getText().toString().isEmpty()
                    || editAddress.getText().toString().length() < 0) {
                editAddress.setError(getString(R.string.address_missing));
            }

            if (!Util.isValidEmailAddress(editEmailid.getText().toString())) {
                editEmailid.setError(getString(R.string.email_missing));
            }

            if (gender == 2) {
                textlabelGenderError.setError(getString(R.string.gender_error));
            }
        }
    }

    //s3 upload image
    public void uploadMedia() {

        if (!TextUtils.isEmpty(selectedImage) && galleryUri != null) {
            showProgress();
            final File destination;
            destination = new File(getActivity().getExternalFilesDir(null), selectedImage);
            createFile(getActivity(), galleryUri, destination);
            TransferObserver uploadObserver =
                    transferUtility.upload(getString(R.string.s3_bucket_profile_path) + selectedImage, destination);
            uploadObserver.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.COMPLETED == state) {
                        destination.delete();
                        partyUserUpdate();
                    } else if (TransferState.FAILED == state) {
                        hideProgress();
                        destination.delete();
                        Toast.makeText(getActivity(), getString(R.string.party_register_error), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                }

                @Override
                public void onError(int id, Exception ex) {
                    ex.printStackTrace();
                    hideProgress();
                    destination.delete();
                    Toast.makeText(getActivity(), getString(R.string.party_register_error), Toast.LENGTH_SHORT).show();
                }

            });
        } else if (userDetailsResponse.getResults() != null &&
                !userDetailsResponse.getResults().getAvatar().isEmpty()) {
            showProgress();
            partyUserUpdate();
        } else {
            Toast.makeText(getActivity(), getString(R.string.party_register_error), Toast.LENGTH_SHORT).show();
        }


    }


    public void partyUserUpdate() {
        if (Util.isNetworkAvailable()) {
            final UserProfileUpdate userProfileUpdateparams = new UserProfileUpdate();
            userProfileUpdateparams.setImeiid(sharedPreferences.getString(DmkConstants.IMEIID, null));
            userProfileUpdateparams.setOsversion(String.valueOf(sharedPreferences.getInt(DmkConstants.OS_VERSION, 0)));
            userProfileUpdateparams.setAppversion(sharedPreferences.getString(DmkConstants.APPVERSION_CODE, ""));
//            1-android 2-ios
            userProfileUpdateparams.setApptype(1);
            userProfileUpdateparams.setDevicetoken(fcmSharedPrefrences.getString(DmkConstants.FCMTOKEN, ""));
            userProfileUpdateparams.setFirstName(editFirstName.getText().toString());
            userProfileUpdateparams.setLastName(editLastName.getText().toString());
            userProfileUpdateparams.setGender(String.valueOf(gender));
            userProfileUpdateparams.setPhoneNumber(sharedPreferences.getString(DmkConstants.PHONENUMBER, null));
            userProfileUpdateparams.setDob(editDob.getText().toString());
            userProfileUpdateparams.setAge(age);
            userProfileUpdateparams.setEmail(editEmailid.getText().toString());
            userProfileUpdateparams.setAddress(editAddress.getText().toString());
            userProfileUpdateparams.setVoterID(editVoterId.getText().toString());
            userProfileUpdateparams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));


            if (!TextUtils.isEmpty(selectedImage) && galleryUri != null) {
                userProfileUpdateparams.setProfilePhoto(selectedImage);
            } else if (userDetailsResponse.getResults() != null &&
                    !userDetailsResponse.getResults().getAvatar().isEmpty()) {
                userProfileUpdateparams.setProfilePhoto(userDetailsResponse.getResults().getAvatar());
            }


            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.updateUserDetails(headerMap, userProfileUpdateparams).enqueue(new Callback<UserDetailsResponse>() {
                @Override
                public void onResponse(Call<UserDetailsResponse> call, Response<UserDetailsResponse> response) {
                    hideProgress();
                    userDetailsResponse = response.body();
                    //userDetailsUpdateResponse=response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (userDetailsResponse.getStatuscode() == 0) {
                            if (userDetailsResponse.getResults() != null) {
                                editor.putString(DmkConstants.FIRSTNAME, userDetailsResponse.getResults().getFirstName() != null ?
                                        userDetailsResponse.getResults().getFirstName() : "");
                                editor.putString(DmkConstants.LASTNAME, userDetailsResponse.getResults().getLastName() != null ?
                                        userDetailsResponse.getResults().getLastName() : "");
                                editor.putString(DmkConstants.MOBILENUMBER, userDetailsResponse.getResults().getPhoneNumber() != null ?
                                        userDetailsResponse.getResults().getPhoneNumber() : "");
                                editor.putInt(DmkConstants.USERID, userDetailsResponse.getResults().getId());
                                editor.putString(DmkConstants.USERDISTRICTNAME, userDetailsResponse.getResults().getDistrictName() != null ?
                                        userDetailsResponse.getResults().getDistrictName() : "");
                                editor.putString(DmkConstants.USERWINGNAME, userDetailsResponse.getResults().getWings().getWingName() != null ?
                                        userDetailsResponse.getResults().getWings().getWingName() : "");
                                editor.putString(DmkConstants.DESGINATION, userDetailsResponse.getResults().getRoles().getRolename() != null ?
                                        userDetailsResponse.getResults().getRoles().getRolename() : "");
                                editor.putString(DmkConstants.UNIQUEDMKID, userDetailsResponse.getResults().getUniquieID() != null ?
                                        userDetailsResponse.getResults().getUniquieID() : "");
                                editor.putInt(DmkConstants.USERAGE, userDetailsResponse.getResults().getAge());
                                editor.putInt(DmkConstants.SUPERIORID, userDetailsResponse.getResults().getSuperiorUserID());
                                editor.putInt(DmkConstants.USER_DISTRICT_ID, userDetailsResponse.getResults().getDistrictID());
                                editor.putInt(DmkConstants.USER_PARTY_DISTRICT_ID, userDetailsResponse.getResults().getPartydistrict());
                                editor.putInt(DmkConstants.USER_CONSTITUENCY_ID, userDetailsResponse.getResults().getConstituencyID());
                                editor.putInt(DmkConstants.USER_DIVISION_ID, userDetailsResponse.getResults().getTypeid());
                                editor.putInt(DmkConstants.USER_PART_ID, userDetailsResponse.getResults().getPartid());
                                //editor.putInt(DmkConstants.USER_VATTAM_ID, userDetailsResponse.getResults().getVattamid());
                                editor.putInt(DmkConstants.USER_WARD_ID, userDetailsResponse.getResults().getWardID());
                                editor.putInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, userDetailsResponse.getResults().getVillagepanchayat());
                                editor.putInt(DmkConstants.USER_PANCHAYAT_UNION_ID, userDetailsResponse.getResults().getPanchayatunion());
                                editor.putInt(DmkConstants.USER_TOWNSHIP_ID, userDetailsResponse.getResults().getTownshipid());
                                editor.putInt(DmkConstants.USER_BOOTH_ID, userDetailsResponse.getResults().getBoothID());
                                editor.putInt(DmkConstants.DESIGNATIONID, userDetailsResponse.getResults().getRoles().getLevel());
                                editor.putInt(DmkConstants.ROLL_ID, userDetailsResponse.getResults().getRoles().getId());
                                editor.putInt(DmkConstants.USERTTYPE, userDetailsResponse.getResults().getUserType());
                                editor.putString(DmkConstants.PROFILE_PICTURE, userDetailsResponse.getResults().getAvatar() != null ?
                                        userDetailsResponse.getResults().getAvatar() : "");
                                editor.putString(DmkConstants.USERPERMISSION_COMMON, userDetailsResponse.getResults().getUserPermissionset().getCommon() != null ?
                                        userDetailsResponse.getResults().getUserPermissionset().getCommon() : "");
                                editor.putString(DmkConstants.USERPERMISSION_ARTICLE, userDetailsResponse.getResults().getUserPermissionset().getArticle() != null ?
                                        userDetailsResponse.getResults().getUserPermissionset().getArticle() : "");
                                editor.putString(DmkConstants.USERPERMISSION_EVENTS, userDetailsResponse.getResults().getUserPermissionset().getEvents() != null ?
                                        userDetailsResponse.getResults().getUserPermissionset().getEvents() : "");
                                editor.putString(DmkConstants.USERPERMISSION_NEWS, userDetailsResponse.getResults().getUserPermissionset().getNews() != null ?
                                        userDetailsResponse.getResults().getUserPermissionset().getNews() : "");
                                editor.putString(DmkConstants.USERPERMISSION_ELECTION, userDetailsResponse.getResults().getUserPermissionset().getElection() != null ?
                                        userDetailsResponse.getResults().getUserPermissionset().getElection() : "");
                                editor.putString(DmkConstants.USER_VOTER_ID, userDetailsResponse.getResults().getVoterID() != null ?
                                        userDetailsResponse.getResults().getVoterID() : "");
                                editor.putString(DmkConstants.USER_ADDRESS, userDetailsResponse.getResults().getAddress() != null ?
                                        userDetailsResponse.getResults().getAddress() : "");
                                editor.commit();
                                android.support.v7.app.AlertDialog.Builder alertDialog1 = new android.support.v7.app.AlertDialog.Builder(getActivity());
                                alertDialog1.setMessage(getString(R.string.update_success));
                                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                });
                                alertDialog1.show();
                            } else {
                                Toast.makeText(getActivity(), userDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, userDetailsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, userDetailsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(userDetailsResponse.getSource(),
                                    userDetailsResponse.getSourcedata()));
                            editor.commit();
                            showProgress();
                            partyUserUpdate();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else

        {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    private void datePickerDialog() {
        currentD = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        Calendar minDate = Calendar.getInstance();
        DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance(this, currentD.get(Calendar.YEAR),
                        currentD.get(Calendar.MONTH), currentD.get(Calendar.DAY_OF_MONTH));
        maxDate.add(Calendar.YEAR, -18);
        datePickerDialog.setMaxDate(maxDate);
//        minDate.add(Calendar.YEAR, -60);
//        datePickerDialog.setMinDate(minDate);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.red));
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }


//    private void getImageFiles() {
//        final Dialog dialog = new Dialog(getActivity());
//        // Include dialog.xml file
//        dialog.setContentView(R.layout.media_dialog);
//        // Set dialog title
//        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
//        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
//        textCamera.setText(R.string.camera);
//        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
//        imageCamera.setImageResource(R.mipmap.icon_photo_camera);
//        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
//        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
//        textGallery.setText(R.string.gallery);
//        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
//        imageGallery.setImageResource(R.mipmap.icon_gallery);
//        imageCamera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
//                            != PackageManager.PERMISSION_GRANTED) {
//                        requestPermissions(new String[]{Manifest.permission.CAMERA},
//                                MY_REQUEST_CODE_CAMERA);
//                    } else {
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(intent, REQUEST_CAMERA);
//                    }
//                } else {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(intent, REQUEST_CAMERA);
//                }
//                dialog.dismiss();
//            }
//        });
//        imageGallery.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
//                            != PackageManager.PERMISSION_GRANTED) {
//                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                                MY_REQUEST_CODE_GALLERY);
//                    } else {
//                        Intent intent = new Intent();
//                        intent.setType("image/*");
//                        //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                        intent.setAction(Intent.ACTION_GET_CONTENT);//
//                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
//
//                    }
//                } else {
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
//
//                }
//                dialog.dismiss();
//            }
//
//        });
//
//        // set values for custom dialog components - text, image and button
//        dialog.show();
//    }
//
//
//    private void getProfilePicture() {
//        final CharSequence[] options = {"Choose from Gallery", "Cancel"};
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        //builder.setTitle("Add icon_attach_photo");
//        builder.setItems(options, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                if (options[item].equals("Take Photo")) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
//                                != PackageManager.PERMISSION_GRANTED) {
//                            requestPermissions(new String[]{Manifest.permission.CAMERA},
//                                    MY_REQUEST_CODE_CAMERA);
//                        } else {
//                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            startActivityForResult(intent, REQUEST_CAMERA);
//                        }
//                    } else {
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(intent, REQUEST_CAMERA);
//                    }
//
//                } else if (options[item].equals("Choose from Gallery")) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
//                                != PackageManager.PERMISSION_GRANTED) {
//                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                                    MY_REQUEST_CODE_GALLERY);
//                        } else {
//                            Intent intent = new Intent();
//                            intent.setType("image/*");
//                            intent.setAction(Intent.ACTION_GET_CONTENT);//
//                            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
//                        }
//                    } else {
//                        Intent intent = new Intent();
//                        intent.setType("image/*");
//                        intent.setAction(Intent.ACTION_GET_CONTENT);//
//                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
//                    }
//
//                } else if (options[item].equals("Cancel")) {
//                    dialog.dismiss();
//                }
//            }
//        });
//        builder.show();
//    }
//
//    @SuppressWarnings("deprecation")
//    private void onSelectFromGalleryResult(Intent data) {
//        if (data != null) {
//            galleryUri = data.getData();
//            compressInputImage(data, getActivity(), imageProfilePic);
//        }
//        if (galleryUri != null)
//            selectedImage = System.currentTimeMillis() + ".jpg";
//    }
//
//    private void onCaptureImageResult(Intent data) {
//        if (data != null) {
//            Bitmap capturedImageBitmap = (Bitmap) data.getExtras().get("data");
//            if (getImageUri(getActivity(), capturedImageBitmap) != null) {
//                galleryUri = getImageUri(getActivity(), capturedImageBitmap);
//            } else {
//                galleryUri = data.getData();
//            }
//            imageProfilePic.setImageBitmap(capturedImageBitmap);
//        }
//        if (galleryUri != null)
//            selectedImage = System.currentTimeMillis() + ".jpg";
//    }

    private void showType(boolean visibility, String type) {
        if (visibility) {
            textLabelPart.setVisibility(View.VISIBLE);
            spinnerPart.setVisibility(View.VISIBLE);
            viewBottom10.setVisibility(View.VISIBLE);
            textLabelPart.setText(type);
        } else {
            textLabelPart.setVisibility(View.GONE);
            spinnerPart.setVisibility(View.GONE);
            viewBottom10.setVisibility(View.GONE);
        }
    }


}
