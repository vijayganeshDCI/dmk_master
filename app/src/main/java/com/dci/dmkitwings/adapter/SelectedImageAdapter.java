package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ArticleComposeActivity;
import com.dci.dmkitwings.activity.EventComposeActivity;
import com.dci.dmkitwings.activity.NewsComposeActivity;
import com.dci.dmkitwings.activity.TimeLineComposeActivity;
import com.dci.dmkitwings.model.SelectedImage;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import cn.jzvd.JZVideoPlayerStandard;

import static com.dci.dmkitwings.app.DmkApplication.getProxy;

/**
 * Created by vijayaganesh on 4/5/2018.
 */

public class SelectedImageAdapter extends BaseAdapter {

    public SelectedImageAdapter(List<SelectedImage> bitmapList, Context context) {
        this.bitmapList = bitmapList;
        this.context = context;
    }

    List<SelectedImage> bitmapList;
    Context context;

    @Override
    public int getCount() {

        return bitmapList.size();



    }

    @Override
    public SelectedImage getItem(int position) {
        return bitmapList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_selected_image, null);
        }
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.image_selected);
        ImageView imgDelete = (ImageView) convertView.findViewById(R.id.image_delete);
        JZVideoPlayerStandard jzVideoPlayerStandard = (JZVideoPlayerStandard) convertView.findViewById(R.id.video_selected);
        switch (bitmapList.get(position).getMediaType()) {
            case 1:
                imgIcon.setVisibility(View.VISIBLE);
                jzVideoPlayerStandard.setVisibility(View.GONE);
                if (bitmapList.get(position).isForEdit()) {
                    if (context instanceof TimeLineComposeActivity) {
                        universalImageLoader(imgIcon, context.getString(R.string.s3_bucket_time_line_path),
                                bitmapList.get(position).getImageName(), R.mipmap.icon_loading_100x100,
                                R.mipmap.icon_no_image_100x100);
                    } else if (context instanceof NewsComposeActivity) {
                        universalImageLoader(imgIcon, context.getString(R.string.s3_bucket_news_path),
                                bitmapList.get(position).getImageName(), R.mipmap.icon_loading_100x100,
                                R.mipmap.icon_no_image_100x100);
                    }else if (context instanceof ArticleComposeActivity){
                        universalImageLoader(imgIcon,
                                context.getString(R.string.s3_bucket_article_path),
                                bitmapList.get(position).getImageName(), R.mipmap.icon_loading_100x100,
                                R.mipmap.icon_no_image_100x100);
                    }else if (context instanceof EventComposeActivity){
                        universalImageLoader(imgIcon,
                                context.getString(R.string.s3_bucket_events_path),
                                bitmapList.get(position).getImageName(), R.mipmap.icon_loading_100x100,
                                R.mipmap.icon_no_image_100x100);
                    }
                } else
                    imgIcon.setImageBitmap(bitmapList.get(position).getSelectedImage());
                break;
            case 2:
                imgIcon.setVisibility(View.GONE);
                jzVideoPlayerStandard.setVisibility(View.VISIBLE);
                HttpProxyCacheServer proxy = getProxy(context);
                jzVideoPlayerStandard.setUp(proxy.getProxyUrl(String.valueOf(bitmapList.get(position).getSelectedVideo()))
                        , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "sample");
                jzVideoPlayerStandard.setSoundEffectsEnabled(true);
                jzVideoPlayerStandard.setSystemTimeAndBattery();
                Glide.with(context).load(createThumbnailFromPath(String.valueOf(bitmapList.get(position).getSelectedVideo()), MediaStore.Video.Thumbnails.MINI_KIND)).apply(new RequestOptions().override(50, 50)).into(jzVideoPlayerStandard.thumbImageView);
                break;
        }

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmapList.remove(position);
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    public Bitmap createThumbnailFromPath(String filePath, int type) {
        return ThumbnailUtils.createVideoThumbnail(filePath, type);
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) + s3bucketName + image, imageView, options);
    }
}
