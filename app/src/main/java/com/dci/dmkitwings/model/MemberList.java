package com.dci.dmkitwings.model;

public class MemberList {

    public int getProPicture() {
        return proPicture;
    }

    public void setProPicture(int proPicture) {
        this.proPicture = proPicture;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public MemberList(int proPicture, String designation,String name) {
        this.proPicture = proPicture;
        this.designation = designation;
        this.name = name;
    }

    private int proPicture;
    private String designation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
