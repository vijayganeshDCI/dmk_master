package com.dci.dmkitwings.model;

public class VattamWardList {

    public int getPartID() {
        return PartID;
    }

    public void setPartID(int partID) {
        PartID = partID;
    }

    public int getDistrictID() {
        return DistrictID;
    }

    public void setDistrictID(int districtID) {
        DistrictID = districtID;
    }

    public int getConstituencyID() {
        return ConstituencyID;
    }

    public void setConstituencyID(int constituencyID) {
        ConstituencyID = constituencyID;
    }

    public int getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(int divisionID) {
        DivisionID = divisionID;
    }


    private int PartID;
    private int DistrictID;
    private int ConstituencyID;
    private int DivisionID;
    private int UnionID;
    private int UnionType;



    public int getUnionID() {
        return UnionID;
    }

    public void setUnionID(int unionID) {
        UnionID = unionID;
    }

    public int getUnionType() {
        return UnionType;
    }

    public void setUnionType(int unionType) {
        UnionType = unionType;
    }
}
