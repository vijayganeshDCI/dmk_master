package com.dci.dmkitwings.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.EventDetailsActivity;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.activity.LoginActivity;
import com.dci.dmkitwings.activity.PollListActivity;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Date;

import static com.dci.dmkitwings.app.DmkApplication.getContext;

/**
 * Created by vijayaganesh on 11/13/2017.
 */

public class FirebaseMessaging extends com.google.firebase.messaging.FirebaseMessagingService {

    Intent intent;
    String navID, typeID;
    private NotificationManager notificationManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //Calling method to generate icon_notification
        sendNotification(remoteMessage);
        Log.d("FirebaseMessaging", "Message data payload: " + remoteMessage.getData());


    }

    private void sendNotification(RemoteMessage remoteMessage) {
//        {receiver_id= 0, type=1, sender_id=, navid=74,
// sound=1, title=DMK Member Portal,
//                message=கரும்புக்கு டன் ஒன்றுக்கு 3500
// வழங்காவிட்டால் விவசாய சங்கங்களை ஒருங்கிணைத்து போராட்டம்}

        String channelId = "DMK_CHANNEL_ID";
        String channelName = "Dmk Notification";
        notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.icon_large_notification);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        if (remoteMessage.getData() != null) {
            navID = remoteMessage.getData().get("navid");
            typeID = remoteMessage.getData().get("type");
        }

//        type 1-event 2-poll 3-log out
        if (typeID.equalsIgnoreCase("1")) {
            intent = new Intent(this, EventDetailsActivity.class);
            intent.putExtra("eventID", Integer.parseInt(navID));
        } else if (typeID.equalsIgnoreCase("2")) {
            intent = new Intent(this, PollListActivity.class);
            intent.putExtra("pollid", Integer.parseInt(navID));
        } else if (typeID.equalsIgnoreCase("3")) {
            intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //BroadCast Receiver to log out
            Intent intentLogout = new Intent("forceLogout");
            intentLogout.putExtra("logout", true);
            sendBroadcast(intentLogout);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent,
                PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setLargeIcon(largeIcon)
                .setSmallIcon(R.mipmap.icon_small_noti)
                .setContentTitle(remoteMessage.getData().get("title") != null ? remoteMessage.getData().get("title") : "")
                .setContentText(remoteMessage.getData().get("message") != null ? remoteMessage.getData().get("message") : "")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);
        int notificationID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(notificationID, notificationBuilder.build());


        //BroadCast Receiver to notification count
        Intent intentNo = new Intent("notificationListener");
        intentNo.putExtra("notify", true);
        sendBroadcast(intentNo);


    }


    @Override
    public void onCreate() {
        super.onCreate();

    }
}
