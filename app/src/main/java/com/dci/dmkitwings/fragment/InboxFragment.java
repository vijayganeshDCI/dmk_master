package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ChatAct;
import com.dci.dmkitwings.activity.HomeMessageActivity;
import com.dci.dmkitwings.adapter.ReecentChatListAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.ChatMessageParams;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by vijayaganesh on 4/12/2018.
 */

public class InboxFragment extends BaseFragment {

    ReecentChatListAdapter reecentChatListAdapter;
    @BindView(R.id.list_user_list)
    ListView listUserList;
    @BindView(R.id.image_no_network)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_network)
    CustomTextView textNoNetwork;
    @BindView(R.id.cons_contact_list)
    ConstraintLayout consContactList;
    @BindView(R.id.float_mes_compose)
    FloatingActionButton floatMesCompose;
    @BindView(R.id.co_home)
    CoordinatorLayout coHome;
    private ArrayList<ChatMessageParams> membersReceiverList;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    DmkAPI dmkAPI;
    private String receiverUserName;
    private int receiverUserID;
    public String RECENT_LIST_CHILD;
    private DatabaseReference mFirebaseDatabaseReferenceInbox;
    public String RECENT_CHAT = "RECENT_CHAT_MESSAGES";
    HomeMessageActivity homeMessageActivity;
    private DatabaseReference messagesRef;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inbox, container, false);
        ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        homeMessageActivity = (HomeMessageActivity) getActivity();
        membersReceiverList = new ArrayList<ChatMessageParams>();
        receiverUserName = sharedPreferences.getString(DmkConstants.FIRSTNAME, "") + "" +
                sharedPreferences.getString(DmkConstants.LASTNAME, "");
        receiverUserID = sharedPreferences.getInt(DmkConstants.USERID, 0);
        RECENT_LIST_CHILD = ""+receiverUserID;
        if (Util.isNetworkAvailable()) {
            showProgress();
            mFirebaseDatabaseReferenceInbox = FirebaseDatabase.getInstance().getReference();
            messagesRef = mFirebaseDatabaseReferenceInbox.child(RECENT_CHAT).child("Inbox")
                    .child(RECENT_LIST_CHILD);
            messagesRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    hideProgress();
                    if (snapshot.getChildrenCount() > 0) {
                        imageNoNetwork.setVisibility(View.GONE);
                        textNoNetwork.setVisibility(View.GONE);
                        listUserList.setVisibility(View.VISIBLE);
                        membersReceiverList.clear();
                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                            ChatMessageParams chatMessageParams = postSnapshot.getValue(ChatMessageParams.class);
                            membersReceiverList.add(chatMessageParams);
                            // here you can access to name property like university.name

                        }
                        reecentChatListAdapter = new ReecentChatListAdapter(getActivity(), membersReceiverList, InboxFragment.this);
                        listUserList.setAdapter(reecentChatListAdapter);
                    } else {
                        imageNoNetwork.setVisibility(View.VISIBLE);
                        imageNoNetwork.setImageResource(R.mipmap.icon_no_inbox_mes);
                        textNoNetwork.setVisibility(View.VISIBLE);
                        textNoNetwork.setText(R.string.no_inbox);
                        listUserList.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    hideProgress();
                    System.out.println("The read failed: " + databaseError.getMessage());
                }
            });
        } else {
            imageNoNetwork.setVisibility(View.VISIBLE);
            textNoNetwork.setVisibility(View.VISIBLE);
            imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
            textNoNetwork.setText(R.string.no_network);
            listUserList.setVisibility(View.GONE);
        }

        listUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int postion, long l) {
                if (Util.isNetworkAvailable()) {
                    Intent intent = new Intent(getActivity(), ChatAct.class);
                    intent.putExtra("chatWith", membersReceiverList.get(postion).getName());
                    intent.putExtra("chatWithUserID", membersReceiverList.get(postion).getSenderUserID());
                    intent.putExtra("proPic", membersReceiverList.get(postion).getPhotoUrl());
                    intent.putExtra("memberID", membersReceiverList.get(postion).getUniqueID());
                    intent.putExtra("designation", membersReceiverList.get(postion).getRoleName());
                    intent.putExtra("fcmToken", membersReceiverList.get(postion).getFcmToken());
                    startActivity(intent);
                    messagesRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            //Old User
                            for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                if (membersReceiverList.get(postion).getUniqueID().
                                        equals(dataSnapshot.child("uniqueID").
                                                getValue())) {
                                    dataSnapshot.getRef().child("readStatus").setValue(1);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } else {
                    Toast.makeText(getActivity(), getString(R.string.no_network), Toast.LENGTH_SHORT).show();
                }


            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.float_mes_compose)
    public void onViewClicked() {
        if (Util.isNetworkAvailable()) {
            if (sharedPreferences.getString(DmkConstants.DESGINATION, "").equalsIgnoreCase("செயலாளர்")
                    || sharedPreferences.getString(DmkConstants.DESGINATION, "").equalsIgnoreCase("துணை செயலாளர்")) {
                // செயலாளர்
                homeMessageActivity.push(new GetChatUserListFragment(), "GetChatUserListFragment");
            } else {
                homeMessageActivity.pushWithAnimation(new ContactListFragment(), "ContactListFragment");
            }

        } else {
            Toast.makeText(getActivity(), getString(R.string.no_network), Toast.LENGTH_SHORT).show();
        }
    }
}
