package com.dci.dmkitwings.model;

import java.util.List;

public class ArticleDetailsResults {

    private String updated_at;
    private List<String> mediapath;
    private String created_at;
    private int id;
    private String title;
    private int mediatype;
    private String mediafilepath;
    private ArticleListDetailsUser user;
    private String content;
    private String newstypeid;
    private int newstype;
    private int status;

    public String getVideothumb() {
        return Videothumb;
    }

    public void setVideothumb(String videothumb) {
        Videothumb = videothumb;
    }

    private String Videothumb;

    public String getUpdatedAt() {
        return updated_at;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updated_at = updatedAt;
    }

    public List<String> getMediapath() {
        return mediapath;
    }

    public void setMediapath(List<String> mediapath) {
        this.mediapath = mediapath;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(String createdAt) {
        this.created_at = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMediatype() {
        return mediatype;
    }

    public void setMediatype(int mediatype) {
        this.mediatype = mediatype;
    }

    public String getMediafilepath() {
        return mediafilepath;
    }

    public void setMediafilepath(String mediafilepath) {
        this.mediafilepath = mediafilepath;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public ArticleListDetailsUser getUser() {
        return user;
    }

    public void setUser(ArticleListDetailsUser user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNewstypeid() {
        return newstypeid;
    }

    public void setNewstypeid(String newstypeid) {
        this.newstypeid = newstypeid;
    }

    public int getNewstype() {
        return newstype;
    }

    public void setNewstype(int newstype) {
        this.newstype = newstype;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
