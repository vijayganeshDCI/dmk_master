package com.dci.dmkitwings.model;

import java.util.List;

public class WardListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<WardListResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<WardListResultsItem> results) {
		Results = results;
	}

	private String Status;
	private List<WardListResultsItem> Results;
	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}