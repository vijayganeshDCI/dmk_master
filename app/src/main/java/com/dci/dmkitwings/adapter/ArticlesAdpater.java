package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ArticleComposeActivity;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.ArticleFragment;
import com.dci.dmkitwings.model.ArticleDeletePostParams;
import com.dci.dmkitwings.model.ArticleListResultsItem;
import com.dci.dmkitwings.model.NewsDeleteResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Java_AES_Cipher;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.firebase.client.utilities.Base64;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 4/9/2018.
 */

public class ArticlesAdpater extends BaseAdapter {
    public ArticlesAdpater(ArrayList<ArticleListResultsItem> articleListResultsItems, Context context, BaseActivity baseActivity, ArticleFragment articleFragment) {
        this.articleListResultsItems = articleListResultsItems;
        this.context = context;
        this.baseActivity = baseActivity;
        this.articleFragment = articleFragment;
        DmkApplication.getContext().getComponent().inject(this);
    }


    ArrayList<ArticleListResultsItem> articleListResultsItems;
    Context context;
    BaseActivity baseActivity;
    ArticleFragment articleFragment;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    NewsDeleteResponse newsDeleteResponse;

    @Override
    public int getCount() {
        return articleListResultsItems.size();
    }

    @Override
    public ArticleListResultsItem getItem(int position) {
        return articleListResultsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        editor=sharedPreferences.edit();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_article, null);
        }

        ImageView imageArtPicture = (ImageView) convertView.findViewById(R.id.image_art_pic);
        final ImageView image_pro_pic = (ImageView) convertView.findViewById(R.id.image_pro_pic);
        ImageView imageShare = (ImageView) convertView.findViewById(R.id.image_news_share);
        TextView textArtTitle = (TextView) convertView.findViewById(R.id.text_art_title);
//        TextView textArtSubTitle = (TextView) convertView.findViewById(R.id.text_art_sub_title);
        TextView textPostedBy = (TextView) convertView.findViewById(R.id.text_posted_by);
        TextView textArtPostedDate = (TextView) convertView.findViewById(R.id.text_art_posted_date);
        final ImageView imagemenu = (ImageView) convertView.findViewById(R.id.image_menu);

        universalImageLoader(image_pro_pic, context.getString(R.string.s3_bucket_profile_path),
                articleListResultsItems.get(position).getUser().getAvatar(), R.mipmap.icon_pro_image_loading_256,
                R.mipmap.icon_pro_image_loading_256);

        universalImageLoader(imageArtPicture, context.getString(R.string.s3_bucket_article_path),
                articleListResultsItems.get(position).getMediapath().size() > 0 ?
                        articleListResultsItems.get(position).getMediapath().get(0) : "", R.mipmap.icon_loading_image_400x100,
                R.mipmap.icon_no_image_400x200);

        textArtPostedDate.setText(getDateFormat(articleListResultsItems.get(position).getCreated_at()));
        textArtTitle.setText(articleListResultsItems.get(position).getTitle());
//        textArtSubTitle.setText(articleListResultsItems.get(position).getContent());
        textPostedBy.setText(articleListResultsItems.get(position).getUser().getFirstName() + " " +
                articleListResultsItems.get(position).getUser().getFirstName());
        if (articleListResultsItems.get(position).getUser().getId() == sharedPreferences.getInt(DmkConstants.USERID, 0)) {
            imagemenu.setVisibility(View.VISIBLE);

        } else {
            imagemenu.setVisibility(View.GONE);
        }
        imagemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, imagemenu);
                //inflating menu_icon from xml resource
                popup.inflate(R.menu.comment_item_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit:
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setMessage(R.string.confirm_to_edit);
                                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(context, ArticleComposeActivity.class);
                                        intent.putExtra("articleID", articleListResultsItems.get(position).getId());
                                        intent.putExtra("newsTitle", articleListResultsItems.get(position).getTitle());
                                        intent.putExtra("newsContent", articleListResultsItems.get(position).getContent());
                                        intent.putExtra("newsDistrict", articleListResultsItems.get(position).getDistrictID());
                                        intent.putExtra("newsConstituency", articleListResultsItems.get(position).getConstituencyID());
                                        intent.putExtra("Newstypeid", articleListResultsItems.get(position).getArticlestypeid());
                                        intent.putExtra("Newstype", articleListResultsItems.get(position).getArticlestype());
                                        intent.putExtra("fromNews", true);

                                        articleFragment.startActivityForResult(intent, 4);
                                    }

                                });
                                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                                return true;
                            case R.id.delete:
                                //handle menu2 click
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(context);
                                alertDialog1.setMessage(R.string.confirm_to_delete);
                                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        deleteNews(articleListResultsItems.get(position).getId());
                                    }

                                });
                                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog1.show();
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
            }
        });

        imageShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ps = "Dmk_encryption "+articleListResultsItems.get(position).getId();
                String tmp = Base64.encodeBytes(ps.getBytes());
                tmp=Base64.encodeBytes(tmp.getBytes());
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, articleListResultsItems.get(position).getTitle() +
                        "\n" + context.getString(R.string.for_more) +
                        context.getString(R.string.deeplink_url)+"articles/"+tmp);
                context.startActivity(Intent.createChooser(i, "Choose share option"));
            }
        });
        image_pro_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (articleListResultsItems.get(position).getUser().getId()!=1 &&articleListResultsItems.get(position).getUser().getId()!=sharedPreferences.getInt(DmkConstants.USERID,0))
                {

                    BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(context).inflate(R.layout.profile_bubble, null);
                    PopupWindow popupWindow = BubblePopupHelper.create(context, bubbleLayout);
                    CircleImageView imageprofilepic=(CircleImageView)bubbleLayout.findViewById(R.id.image_pro_pic_bubble);
                    CustomTextView textpostedby=(CustomTextView)bubbleLayout.findViewById(R.id.text_posted_by_bubble);
                    CustomTextView textdesgination=(CustomTextView)bubbleLayout.findViewById(R.id.text_post_designation_bubble);
                    CustomTextView textdistrict=(CustomTextView)bubbleLayout.findViewById(R.id.text_post_district_bubble);
                    CustomTextView textconstituency=(CustomTextView)bubbleLayout.findViewById(R.id.text_post_constituency_bubble);
                    CustomTextView textPartBoothWardVillage=(CustomTextView)bubbleLayout.findViewById(R.id.text_post_common_bubble);
                    Log.d("FRIDAY","level"+articleListResultsItems.get(position).getUser().getLevel());
                    switch (articleListResultsItems.get(position).getUser().getLevel())
                    {
                        case 1:
//                            செயலாளர்
//                            துணை செயலாளர்
//                            மண்டல ஒருங்கிணைப்பாளர்
                            textpostedby.setText(articleListResultsItems.get(position).getUser().getFirstName()+" "+
                                    articleListResultsItems.get(position).getUser().getLastName());
                            textdesgination.setText(articleListResultsItems.get(position).getUser().getDesignation());
                            textdistrict.setVisibility(View.GONE);
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 2:
                            //மாவட்ட ஒருங்கிணைப்பாளர் district
                            textpostedby.setText(articleListResultsItems.get(position).getUser().getFirstName()+" "+
                                    articleListResultsItems.get(position).getUser().getLastName());
                            textdesgination.setText(articleListResultsItems.get(position).getUser().getDesignation());
                            textdistrict.setText(articleListResultsItems.get(position).getUser().getDistrict());
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 3:
                            //தொகுதி ஒருங்கிணைப்பாளர் constituency
                            textpostedby.setText(articleListResultsItems.get(position).getUser().getFirstName()+" "+
                                    articleListResultsItems.get(position).getUser().getLastName());
                            textdesgination.setText(articleListResultsItems.get(position).getUser().getDesignation());
                            textdistrict.setText(articleListResultsItems.get(position).getUser().getDistrict());
                            textconstituency.setText(articleListResultsItems.get(position).getUser().getConstituency());
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                            //பகுதி ஒருங்கிணைப்பாளர் part
                            //நகராட்சி ஒருங்கிணைப்பாளர் municipality
                            //பேரூராட்சி ஒருங்கிணைப்பாளர் township
                            //ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                            textpostedby.setText(articleListResultsItems.get(position).getUser().getFirstName()+" "+
                                    articleListResultsItems.get(position).getUser().getLastName());
                            textdesgination.setText(articleListResultsItems.get(position).getUser().getDesignation());
                            textdistrict.setText(articleListResultsItems.get(position).getUser().getDistrict());
                            textconstituency.setText(articleListResultsItems.get(position).getUser().getConstituency());
                            textPartBoothWardVillage.setText(articleListResultsItems.get(position).getUser().getPart());
                            break;
                        case 5:
                        case 6:
                        case 9:
                        case 10:
                        case 13:
                        case 14:
                        case 18:
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            //கிளை ஒருங்கிணைப்பாளர் ward
                            // வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //கிளை ஒருங்கிணைப்பாளர்  ward
                            //ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                            textpostedby.setText(articleListResultsItems.get(position).getUser().getFirstName()+" "+
                                    articleListResultsItems.get(position).getUser().getLastName());
                            textdesgination.setText(articleListResultsItems.get(position).getUser().getDesignation());
                            textdistrict.setText(articleListResultsItems.get(position).getUser().getDistrict());
                            textconstituency.setText(articleListResultsItems.get(position).getUser().getConstituency());
                            textPartBoothWardVillage.setText(articleListResultsItems.get(position).getUser().getWard());
                            break;
                        case 7:
                        case 11:
                        case 15:
                        case 19:
                            //வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                            //ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                            textpostedby.setText(articleListResultsItems.get(position).getUser().getFirstName()+" "+
                                    articleListResultsItems.get(position).getUser().getLastName());
                            textdesgination.setText(articleListResultsItems.get(position).getUser().getDesignation());
                            textdistrict.setText(articleListResultsItems.get(position).getUser().getDistrict());
                            textconstituency.setText(articleListResultsItems.get(position).getUser().getConstituency());
                            textPartBoothWardVillage.setText(articleListResultsItems.get(position).getUser().getBooth());
                            break;
                        case 17:
                            //ராட்சிஊ ஒருங்கிணைப்பாளர் villagepanchayat
                            //ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                            textpostedby.setText(articleListResultsItems.get(position).getUser().getFirstName()+" "+
                                    articleListResultsItems.get(position).getUser().getLastName());
                            textdesgination.setText(articleListResultsItems.get(position).getUser().getDesignation());
                            textdistrict.setText(articleListResultsItems.get(position).getUser().getDistrict());
                            textconstituency.setText(articleListResultsItems.get(position).getUser().getConstituency());
                            textPartBoothWardVillage.setText(articleListResultsItems.get(position).getUser().getVillage());
                            break;




                    }


                    universalImageLoader(imageprofilepic, context.getString(R.string.s3_bucket_profile_path),
                            articleListResultsItems.get(position).getUser().getAvatar(), R.mipmap.icon_pro_image_loading_256,
                            R.mipmap.icon_pro_image_loading_256);
                    final Random random = new Random();
                    int[] location = new int[2];
                    image_pro_pic.getLocationInWindow(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP);
                    popupWindow.showAtLocation(v, Gravity.NO_GRAVITY, location[0], v.getHeight() + location[1]);
                }
            }
        });
        return convertView;
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }

    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd-MMM-yyyy hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    private void deleteNews(final int newsID) {
        ArticleDeletePostParams articleDeletePostParams = new ArticleDeletePostParams();
        articleDeletePostParams.setArticlesid(newsID);
        articleDeletePostParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.deletearticle(headerMap,articleDeletePostParams).enqueue(new Callback<NewsDeleteResponse>() {
                @Override
                public void onResponse(Call<NewsDeleteResponse> call, Response<NewsDeleteResponse> response) {
                    newsDeleteResponse = response.body();
                    baseActivity.hideProgress();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (newsDeleteResponse.getStatuscode()==0) {
                            if (newsDeleteResponse.getStatus().contains("Success")) {
                                Toast.makeText(context,  context.getString(R.string.article_delete_sucuess), Toast.LENGTH_SHORT).show();
                                articleFragment.getArticleList();
                            } else {
                                Toast.makeText(context,  context.getString(R.string.article_delete_failed), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, newsDeleteResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, newsDeleteResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(newsDeleteResponse.getSource(),
                                    newsDeleteResponse.getSourcedata()));
                            editor.commit();
                            deleteNews(newsID);
                        }
                    } else {
                        Toast.makeText(context,  context.getString(R.string.article_delete_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<NewsDeleteResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public String getEncodedHeader(String sourceResponse, String sourceDataResponse) {

        byte[] sourceBase64 = android.util.Base64.decode(sourceResponse, android.util.Base64.NO_WRAP);
        byte[] sourceDataBase64 = android.util.Base64.decode(sourceDataResponse, android.util.Base64.NO_WRAP);
        String source = new String(sourceBase64, StandardCharsets.UTF_8);
        String sourceData = new String(sourceDataBase64, StandardCharsets.UTF_8);
        String data = source.substring(0, 12) + sourceData.substring(0, 8);
        String key = source.substring(12, 19) + sourceData.substring(8, 13);
        String iv = source.substring(19, 28) + sourceData.substring(13, 20);
        String encrpytData = Java_AES_Cipher.encrypt(key, iv, data);
        byte[] dataBase64Encode = encrpytData.getBytes(StandardCharsets.UTF_8);
        String base64 = android.util.Base64.encodeToString(dataBase64Encode, android.util.Base64.NO_WRAP);
        return base64;
    }
}
