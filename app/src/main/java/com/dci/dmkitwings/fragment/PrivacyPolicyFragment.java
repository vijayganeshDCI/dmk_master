package com.dci.dmkitwings.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.SettingsActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.StaticPageResponse;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 4/13/2018.
 */

public class PrivacyPolicyFragment extends BaseFragment {


    @BindView(R.id.cons_privacy_policy)
    ConstraintLayout consPrivacyPolicy;
    Unbinder unbinder;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    StaticPageResponse staticPageResponse;
    @BindView(R.id.web_view_privacy)
    WebView webViewPrivacy;
    SettingsActivity settingsActivity;
    @BindView(R.id.image_no_network)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_network)
    CustomTextView textNoNetwork;
    private SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        DmkApplication.getContext().getComponent().inject(this);
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.textTitle.setText(R.string.privacy);
        unbinder = ButterKnife.bind(this, view);
        editor=sharedPreferences.edit();
        settingsActivity.mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //Navigation Control
        settingsActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        settingsActivity.getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        settingsActivity.getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        settingsActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        getPrivacyPolicy();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void getPrivacyPolicy() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            TimeLineDelete timeLineDelete = new TimeLineDelete();
            timeLineDelete.setPageid(1);
            timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getStaticPage(headerMap,timeLineDelete).enqueue(new Callback<StaticPageResponse>() {
                @Override
                public void onResponse(Call<StaticPageResponse> call, Response<StaticPageResponse> response) {
                    hideProgress();
                    staticPageResponse = response.body();
                    if (response.isSuccessful() && response.isSuccessful() && response.code() == 200) {
                        if (staticPageResponse.getStatuscode()==0) {
                            webViewPrivacy.loadData(staticPageResponse.getResults().getDescription()
                                    , "text/html; charset=UTF-8;", null);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, staticPageResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, staticPageResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(staticPageResponse.getSource(),
                                    staticPageResponse.getSourcedata()));
                            editor.commit();
                            getPrivacyPolicy();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.please_try_again)
                                , Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<StaticPageResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again)
                            , Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            imageNoNetwork.setVisibility(View.VISIBLE);
            textNoNetwork.setVisibility(View.VISIBLE);
            imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
            textNoNetwork.setText(R.string.no_network);
            webViewPrivacy.setVisibility(View.GONE);

        }
    }
}
