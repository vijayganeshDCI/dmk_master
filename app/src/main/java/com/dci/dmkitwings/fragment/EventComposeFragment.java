package com.dci.dmkitwings.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.bumptech.glide.Glide;
import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ArticleComposeActivity;
import com.dci.dmkitwings.adapter.BoothAdapter;
import com.dci.dmkitwings.adapter.ConstituencyAdapter;
import com.dci.dmkitwings.adapter.DistrictAdpater;
import com.dci.dmkitwings.adapter.DivisionAdapter;
import com.dci.dmkitwings.adapter.MunicipalityAdapter;
import com.dci.dmkitwings.adapter.PanchayatUnionListResponse;
import com.dci.dmkitwings.adapter.PanchayatUnionResultsItem;
import com.dci.dmkitwings.adapter.PanchyatUnionAdapter;
import com.dci.dmkitwings.adapter.PartAdapter;
import com.dci.dmkitwings.adapter.PartyDistrictAdapter;
import com.dci.dmkitwings.adapter.SelectedImageAdapter;
import com.dci.dmkitwings.adapter.TownPanchayatAdapter;
import com.dci.dmkitwings.adapter.VillagePanchayatAdapter;
import com.dci.dmkitwings.adapter.WardAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.AddEvents;
import com.dci.dmkitwings.model.AddEventsResponse;
import com.dci.dmkitwings.model.BoothListInputParam;
import com.dci.dmkitwings.model.BoothListResponse;
import com.dci.dmkitwings.model.BoothListResultsItem;
import com.dci.dmkitwings.model.ConsituencyList;
import com.dci.dmkitwings.model.ConsituencyListResponse;
import com.dci.dmkitwings.model.ConstituencyListResultsItem;
import com.dci.dmkitwings.model.DistrictList;
import com.dci.dmkitwings.model.DistrictListResponse;
import com.dci.dmkitwings.model.DistrictsItem;
import com.dci.dmkitwings.model.DivisionListResponse;
import com.dci.dmkitwings.model.DivisionListResultsItem;
import com.dci.dmkitwings.model.MunicipalityListResponse;
import com.dci.dmkitwings.model.MunicipalityListResultsItem;
import com.dci.dmkitwings.model.PartList;
import com.dci.dmkitwings.model.PartListResponse;
import com.dci.dmkitwings.model.PartListResultsItem;
import com.dci.dmkitwings.model.PartyDistrictInputParam;
import com.dci.dmkitwings.model.PartyDistrictResponse;
import com.dci.dmkitwings.model.PartyDistrictResultsItem;
import com.dci.dmkitwings.model.SelectedImage;
import com.dci.dmkitwings.model.TownPanchayatListResponse;
import com.dci.dmkitwings.model.TownPanchayatResultsItem;
import com.dci.dmkitwings.model.UserError;
import com.dci.dmkitwings.model.VattamListResponse;
import com.dci.dmkitwings.model.VattamResultsItem;
import com.dci.dmkitwings.model.VattamWardList;
import com.dci.dmkitwings.model.ViewEventResponse;
import com.dci.dmkitwings.model.VillagePanchayatListResponse;
import com.dci.dmkitwings.model.VillagePanchayatResultsItem;
import com.dci.dmkitwings.model.WardListResponse;
import com.dci.dmkitwings.model.WardListResultsItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomEditText;
import com.dci.dmkitwings.view.CustomRadioButton;
import com.dci.dmkitwings.view.CustomTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.jzvd.JZVideoPlayerStandard;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.dci.dmkitwings.app.DmkApplication.getProxy;

public class EventComposeFragment extends BaseFragment
        implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    @BindView(R.id.text_label_post_to)
    CustomTextView textLabelPostTo;

    @BindView(R.id.radio_all)
    CustomRadioButton radioAll;

    @BindView(R.id.radio_selected_district)
    CustomRadioButton radioSelectedDistrict;

    @BindView(R.id.radio_group_type)
    RadioGroup radioGroupType;


    @BindView(R.id.text_label_district)
    CustomTextView textLabelDistrict;

    @BindView(R.id.spinner_district)
    Spinner spinnerDistrict;

    @BindView(R.id.view_bottom_district)
    View viewBottomDistrict;

    @BindView(R.id.text_label_party_district)
    CustomTextView textLabelPartyDistrict;

    @BindView(R.id.spinner_party_district)
    Spinner spinnerPartyDistrict;

    @BindView(R.id.view_bottom_17)
    View viewBottomPartyDistrict;

    @BindView(R.id.text_label_constituency)
    CustomTextView textLabelConstituency;
    @BindView(R.id.spinner_constituency)
    Spinner spinnerConstituency;
    @BindView(R.id.view_bottom_cons)
    View viewBottomConstituency;
    @BindView(R.id.text_label_division)
    CustomTextView textLabelDivision;
    @BindView(R.id.spinner_division)
    Spinner spinnerDivision;
    @BindView(R.id.view_bottom_division)
    View viewBottomDivision;
    @BindView(R.id.text_label_part)
    CustomTextView textLabelPart;
    @BindView(R.id.spinner_part)
    Spinner spinnerPart;
    @BindView(R.id.view_bottom_part)
    View viewBottomPart;
    @BindView(R.id.text_label_village_panchayat)
    CustomTextView textLabelTownandVillagePanchayat;
    @BindView(R.id.spinner_village_panchayat)
    Spinner spinnerTownandVillagePanchayat;
    @BindView(R.id.view_bottom_village_panchayat)
    View viewBottomTownandVillagePanchayat;
    @BindView(R.id.text_label_ward)
    CustomTextView textLabelWard;
    @BindView(R.id.spinner_ward)
    Spinner spinnerWard;
    @BindView(R.id.view_bottom_party_role)
    View viewBottomWard;
    @BindView(R.id.text_label_Booth)
    CustomTextView textLabelBooth;
    @BindView(R.id.spinner_booth)
    Spinner spinnerBooth;
    @BindView(R.id.text_label_union_type)
    CustomTextView textLabelUnionType;
    @BindView(R.id.spinner_union_type)
    Spinner spinnerUnionType;
    @BindView(R.id.view_bottom_union_type)
    View viewBottomUnionType;
    Unbinder unbinder;
    @BindView(R.id.text_label_start_date)
    CustomTextView textLabelStartDate;
    @BindView(R.id.edit_start_date)
    CustomEditText editStartDate;
    @BindView(R.id.view_bottom_15)
    View viewBottom15;
    @BindView(R.id.text_label_end_date)
    CustomTextView textLabelEndDate;
    @BindView(R.id.edit_end_date)
    CustomEditText editEndDate;
    @BindView(R.id.view_bottom_13)
    View viewBottom13;
    @BindView(R.id.text_label_status)
    CustomTextView textLabelStatus;
    @BindView(R.id.radio_active)
    CustomRadioButton radioActive;
    @BindView(R.id.radio_inactive)
    CustomRadioButton radioInactive;
    @BindView(R.id.radio_group_status)
    RadioGroup radioGroupStatus;
    @BindView(R.id.view_bottom_16)
    View viewBottom16;
    @BindView(R.id.text_label_title)
    CustomTextView textLabelTitle;
    @BindView(R.id.edit_title)
    CustomEditText editTitle;
    @BindView(R.id.view_bottom_14)
    View viewBottom14;
    @BindView(R.id.text_label_descrip)
    CustomTextView textLabelDescrip;
    @BindView(R.id.edit_time_sub)
    CustomEditText editTimeSub;
    @BindView(R.id.image_selected)
    ImageView imageSelected;
    @BindView(R.id.image_delete)
    ImageView imageDelete;
    @BindView(R.id.video_selected)
    JZVideoPlayerStandard videoSelected;
    @BindView(R.id.grid_image)
    GridView gridImage;
    @BindView(R.id.cons_event_compose_item)
    ConstraintLayout consEventComposeItem;
    @BindView(R.id.scroll_event_compose)
    ScrollView scrollEventCompose;
    @BindView(R.id.image_photo)
    ImageView imagePhoto;
    @BindView(R.id.image_video)
    ImageView imageVideo;
    @BindView(R.id.image_audio)
    ImageView imageAudio;
    @BindView(R.id.image_attach_file)
    ImageView imageAttachFile;
    @BindView(R.id.image_send)
    ImageView imageSend;
    @BindView(R.id.image_send1)
    ImageView imageSend1;
    @BindView(R.id.cons_send)
    ConstraintLayout consSend;
    @BindView(R.id.cons_bottom_view)
    ConstraintLayout consBottomView;
    @BindView(R.id.cons_event_compose)
    ConstraintLayout consEventCompose;
    @BindView(R.id.float_attach_image)
    FloatingActionButton floatAttachImage;
    @BindView(R.id.float_attach_video)
    FloatingActionButton floatAttachVideo;
    @BindView(R.id.float_attach_audio)
    FloatingActionButton floatAttachAudio;
    @BindView(R.id.float_mes_compose)
    FloatingActionButton floatMesCompose;
    @BindView(R.id.cord_event_compose)
    CoordinatorLayout cordEventCompose;
    @BindView(R.id.view_bottom_18)
    View viewBottomBooth;


    private Calendar currentD;
    private Calendar calendarStartdate, calendarEnddate;
    private SimpleDateFormat simpleDateFormat, simpleDateFormat2;
    UserError userError;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    ConsituencyListResponse consituencyListResponse;
    DivisionListResponse divisionListResponse;
    DistrictListResponse districtListResponse;
    PartyDistrictResponse partyDistrictResponse;
    PartListResponse partListResponse;
    PanchayatUnionListResponse panchayatUnionListResponse;
    TownPanchayatListResponse townPanchayatListResponse;
    VattamListResponse vattamListResponse;
    WardListResponse wardListResponse;
    VillagePanchayatListResponse villagePanchayatListResponse;
    MunicipalityListResponse municipalityListResponse;
    ArrayList<ConstituencyListResultsItem> constituencyList;
    ArrayList<DivisionListResultsItem> divisionlist;
    ArrayList<DistrictsItem> districtList;
    ArrayList<PartListResultsItem> partList;
    ArrayList<PanchayatUnionResultsItem> panchayatList;
    ArrayList<TownPanchayatResultsItem> townPanchayatList;
    ArrayList<VattamResultsItem> vattamList;
    ArrayList<VillagePanchayatResultsItem> villagePanchayatResultsItemArrayList;
    ArrayList<String> partyRoleList;
    ArrayList<WardListResultsItem> wardList;
    ArrayList<MunicipalityListResultsItem> municipalityList;
    List<PartyDistrictResultsItem> partyDistrictResultsItems;
    private int districtID = -1;
    private int divisionID = -1;
    private int partyDistrictId;
    private int constituencyID;
    private int partID;
    private String divisionName;
    private static int selectedDate = 1;
    Field popup;
    private String eventStartDate, eventStartTime, eventStartDateFormat, eventEndDateFormat;
    private String eventEndDate, eventEndTime;
    private Calendar calendarStartTime, calendarEndTime;
    private Calendar currentT;

    private String filemanagerPath;
    private String recordedVideoPath;
    private int REQUEST_TAKE_GALLERY_VIDEO_AUDIO = 2;
    private static final int MY_REQUEST_CODE_CAMERA = 3;
    private static final int SELECT_IMAGE_FROM_CAMERA = 4;
    private static final int MY_REQUEST_CODE_GALLERY = 5;
    private int SELECT_IMAGE_FROM_GALLERY = 6;
    private static final int SELECT_VIDEO_FROM_CAMERA = 7;
    private int SELECT_VIDEO_FROM_GALLERY = 8;
    private List<SelectedImage> selectedImageList;
    private MediaRecorder mRecorder;
    private long mStartTime = 0;

    private int[] amplitudes = new int[100];

    private int i = 0;
    private File mOutputFile;
    private TextView textTimer;
    private Handler mHandler = new Handler();
    private Runnable mTickExecutor = new Runnable() {
        @Override
        public void run() {
            audioTimer();
            mHandler.postDelayed(mTickExecutor, 100);
        }
    };
    private SelectedImageAdapter selectedImageAdapter;
    private int activeStatus = 1, mediaType;
    private boolean isFABOpen;
    private String eventStartTimeFormat, eventEndTimeFormat;
    int eventID;
    boolean isFromEdit;
    private MenuItem sumbit;
    private ViewEventResponse viewEventResponse;
    private View view;
    private HttpProxyCacheServer proxy;
    private boolean videoFromResponse = false;
    private String videoAudioFileNameInternal;
    private Uri selectedVideoAudioUri;
    private String galleryVideoPath, galleryAudioPath, audioRecordedPathName;
    private boolean isAudioFromRecord;
    private List<String> selectedMediaListName;
    private File destination;
    private String videoThubmnail;
    BoothListResponse boothListResponse;
    ArrayList<BoothListResultsItem> boothList;
    private int villageID, designationID;
    private int wardID;
    private int boothID;
    AddEventsResponse addEventsResponse;
    private int unionType = 0;
    @BindView(R.id.edit_venue)
    CustomEditText editvenue;
    @BindView(R.id.view_bottom_4)
    View view_bottom_4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_compose, container, false);
        ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        setHasOptionsMenu(true);
        proxy = getProxy(getActivity());
        selectedMediaListName = new ArrayList<String>();

        partyRoleList = new ArrayList<String>();
        constituencyList = new ArrayList<ConstituencyListResultsItem>();
        divisionlist = new ArrayList<DivisionListResultsItem>();
        districtList = new ArrayList<DistrictsItem>();
        partList = new ArrayList<PartListResultsItem>();
        wardList = new ArrayList<WardListResultsItem>();
        vattamList = new ArrayList<VattamResultsItem>();
        panchayatList = new ArrayList<PanchayatUnionResultsItem>();
        townPanchayatList = new ArrayList<TownPanchayatResultsItem>();
        municipalityList = new ArrayList<MunicipalityListResultsItem>();
        villagePanchayatResultsItemArrayList = new ArrayList<VillagePanchayatResultsItem>();
        selectedImageList = new ArrayList<SelectedImage>();
        partyDistrictResultsItems = new ArrayList<PartyDistrictResultsItem>();
        boothList = new ArrayList<BoothListResultsItem>();

        Intent intent = getActivity().getIntent();
        if (getActivity().getIntent() != null) {
            eventID = intent.getIntExtra("eventID", 0);
        }
        partyDistrictResultsItems.add(new PartyDistrictResultsItem(getString(R.string.select_party_district), 0));
        PartyDistrictAdapter partyDistrictAdapter = new PartyDistrictAdapter(partyDistrictResultsItems, getActivity());
        spinnerPartyDistrict.setAdapter(partyDistrictAdapter);
        constituencyList.add(0, new ConstituencyListResultsItem(getString(R.string.select_constituency), 0));
        ConstituencyAdapter constituencyAdapter = new ConstituencyAdapter(constituencyList, getActivity());
        spinnerConstituency.setAdapter(constituencyAdapter);
        divisionlist.add(0, new DivisionListResultsItem(getString(R.string.select_division), 0));
        DivisionAdapter divisionAdapter = new DivisionAdapter(divisionlist, getActivity());
        spinnerDivision.setAdapter(divisionAdapter);

        partList.add(0, new PartListResultsItem(getString(R.string.select_part), 0));
        PartAdapter partAdapter = new PartAdapter(partList, getActivity());
        spinnerPart.setAdapter(partAdapter);

//        villagePanchayatResultsItemArrayList.add(0, new VillagePanchayatResultsItem(0, getString(R.string.select_village_panchayat)));
//        VattamAdapter vattamAdapter = new VattamAdapter(vattamList, getActivity());
//        spinnerTownandVillagePanchayat.setAdapter(vattamAdapter);

        wardList.add(0, new WardListResultsItem(getString(R.string.select_ward), 0));
        WardAdapter wardAdapter = new WardAdapter(wardList, getActivity());
        spinnerWard.setAdapter(wardAdapter);

        districtList.add(0, new DistrictsItem(getString(R.string.select_district), 0));
        DistrictAdpater districtAdpater = new DistrictAdpater(districtList, getActivity());
        spinnerDistrict.setAdapter(districtAdpater);

        boothList.add(0, new BoothListResultsItem(getString(R.string.select_booth), 0));
        BoothAdapter boothAdapter = new BoothAdapter(boothList, getActivity());
        spinnerBooth.setAdapter(boothAdapter);

        panchayatList.add(0, new PanchayatUnionResultsItem(getString(R.string.select_panchayat_union), 0));
        PanchyatUnionAdapter panchyatUnionAdapter = new PanchyatUnionAdapter(panchayatList, getActivity());
        spinnerPart.setAdapter(panchyatUnionAdapter);
        radioAll.setChecked(true);

        final ArrayAdapter<String> adapterUnionType = new
                ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item,
                getResources().getStringArray(R.array.panchayat_union_list));
        spinnerUnionType.setAdapter(adapterUnionType);
        designationID = sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0);

        switch (designationID) {
            case 1:
                //                செயலாளர்
//                துணை செயலாளர்
//                மண்டல ஒருங்கிணைப்பா
                radioGroupType.setVisibility(View.VISIBLE);
                view_bottom_4.setVisibility(View.GONE);
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottomDivision.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottomPart.setVisibility(View.GONE);

                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottomWard.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.INVISIBLE);
                textLabelBooth.setVisibility(View.INVISIBLE);
                viewBottomBooth.setVisibility(View.INVISIBLE);


                districtID = sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0);
                partyDistrictId = sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0);
                //getConsituencyList(districtID);
                break;
            case 2:

                //                மாவட்ட ஒருங்கிணைப்பாளர்  district

                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.VISIBLE);
                textLabelConstituency.setVisibility(View.VISIBLE);
                viewBottomConstituency.setVisibility(View.VISIBLE);

                spinnerDivision.setVisibility(View.VISIBLE);
                textLabelDivision.setVisibility(View.VISIBLE);
                viewBottomDivision.setVisibility(View.VISIBLE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.VISIBLE);
                textLabelPart.setVisibility(View.VISIBLE);
                viewBottomPart.setVisibility(View.VISIBLE);

                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottomWard.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                districtID = sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0);
                partyDistrictId = sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0);
                getConsituencyList(partyDistrictId);
                break;
            case 3:
//                தொகுதி ஒருங்கிணைப்பாளர்  constituency
//                Hide till constituency
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.VISIBLE);
                textLabelDivision.setVisibility(View.VISIBLE);
                viewBottomDivision.setVisibility(View.VISIBLE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.VISIBLE);
                textLabelPart.setVisibility(View.VISIBLE);
                viewBottomPart.setVisibility(View.VISIBLE);

                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottomWard.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                constituencyID = sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0);
                districtID = sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0);
                getDivisionList();
                break;

            case 4:
//                Corporation
//                பகுதி ஒருங்கிணைப்பாளர்  part
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottomDivision.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottomPart.setVisibility(View.GONE);


                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.VISIBLE);
                textLabelWard.setVisibility(View.VISIBLE);
                viewBottomWard.setVisibility(View.VISIBLE);


                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);


                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                getWardList(divisionID, partID);
                break;
            case 8:
//                Municipality
//                நகராட்சி ஒருங்கிணைப்பாளர் municipality
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottomDivision.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottomPart.setVisibility(View.GONE);


                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.VISIBLE);
                textLabelWard.setVisibility(View.VISIBLE);
                viewBottomWard.setVisibility(View.VISIBLE);


                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                getWardList(divisionID, partID);
                break;

            case 12:
//                Town
//                பேரூராட்சி ஒருங்கிணைப்பாளர் township
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottomDivision.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottomPart.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.VISIBLE);
                textLabelWard.setVisibility(View.VISIBLE);
                viewBottomWard.setVisibility(View.VISIBLE);

                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setText(getString(R.string.town_pachayat));

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);

                getWardList(divisionID, partID);


                break;
            case 16:
//                ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottomDivision.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.VISIBLE);
                textLabelUnionType.setVisibility(View.VISIBLE);
                viewBottomUnionType.setVisibility(View.VISIBLE);

                spinnerPart.setVisibility(View.VISIBLE);
                textLabelPart.setVisibility(View.VISIBLE);
                viewBottomPart.setVisibility(View.VISIBLE);

                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottomWard.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                constituencyID = sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0);
                districtID = sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0);
                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);

                break;

            case 5:
                //            Corporation
//                வட்டம் ஒருங்கிணைப்பாளர்  vattam
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottomDivision.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottomPart.setVisibility(View.GONE);

                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottomWard.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);


                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0);
                wardID = sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                villageID = sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0);


                getBoothList(divisionID, partID, wardID, villageID, unionType);

                break;
            case 9:
//                Municipality
//                வட்டம் ஒருங்கிணைப்பாளர்  vattam
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottomDivision.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottomPart.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottomWard.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0);
                wardID = sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                villageID = sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0);

                getBoothList(divisionID, partID, wardID, villageID, unionType);

                break;
            case 13:
//                Town
//                வட்டம் ஒருங்கிணைப்பாளர்  vattam
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottomDivision.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottomPart.setVisibility(View.GONE);

                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottomWard.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0);
                wardID = sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                villageID = sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0);

                getBoothList(divisionID, partID, wardID, villageID, unionType);

//                Panchayat union
                break;
            case 17:
//                ஊராட்சி ஒருங்கிணைப்பாளர் villagepanchayat
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottomDivision.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottomPart.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottomWard.setVisibility(View.GONE);

                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setText(getString(R.string.village_panchayat));

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);


                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0);
                wardID = sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                villageID = sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0);

                getBoothList(divisionID, partID, wardID, villageID, unionType);

                break;

//            Corporation
            case 7:
//                Municipality
            case 11:
//                Town
            case 15:
//                Panchayat union
            case 19:
//                வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottomDistrict.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottomPartyDistrict.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottomConstituency.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottomDivision.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottomPart.setVisibility(View.GONE);

                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottomWard.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.GONE);
                textLabelBooth.setVisibility(View.GONE);

                break;
        }


        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                DistrictsItem districtsItem = districtList.get(position);
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                if (districtsItem.getId() != 0) {
                    districtID = districtsItem.getId();
                    getPartyDistrictlist(districtID);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerPartyDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                PartyDistrictResultsItem partyDistrictResultsItem = partyDistrictResultsItems.get(position);
                partyDistrictId = partyDistrictResultsItem.getId();
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                if (partyDistrictId != 0) {
                    getConsituencyList(partyDistrictId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        radioSelectedDistrict.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    spinnerDistrict.setVisibility(View.VISIBLE);
                    textLabelDistrict.setVisibility(View.VISIBLE);
                    view_bottom_4.setVisibility(View.VISIBLE);
                    //viewBottomDistrict.setVisibility(View.VISIBLE);
                    spinnerPartyDistrict.setVisibility(View.VISIBLE);
                    textLabelPartyDistrict.setVisibility(View.VISIBLE);
                    viewBottomPartyDistrict.setVisibility(View.VISIBLE);
                    spinnerConstituency.setVisibility(View.VISIBLE);
                    textLabelConstituency.setVisibility(View.VISIBLE);
                    viewBottomConstituency.setVisibility(View.VISIBLE);
                    spinnerDivision.setVisibility(View.VISIBLE);
                    textLabelDivision.setVisibility(View.VISIBLE);
                    viewBottomDivision.setVisibility(View.VISIBLE);
                    spinnerPart.setVisibility(View.VISIBLE);
                    textLabelPart.setVisibility(View.VISIBLE);
                    viewBottomPart.setVisibility(View.VISIBLE);
                    spinnerWard.setVisibility(View.VISIBLE);
                    textLabelWard.setVisibility(View.VISIBLE);
                    viewBottomWard.setVisibility(View.VISIBLE);

                    spinnerBooth.setVisibility(View.VISIBLE);
                    textLabelBooth.setVisibility(View.VISIBLE);
                    viewBottomBooth.setVisibility(View.VISIBLE);
                    editStartDate.setText("");
                    editEndDate.setText("");
                    editvenue.setText("");
                    editTitle.setText("");
                    editTimeSub.setText("");
                    getDistrictList();

                }

            }
        });
        radioAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    spinnerDistrict.setVisibility(View.GONE);
                    textLabelDistrict.setVisibility(View.GONE);
                    viewBottomDistrict.setVisibility(View.GONE);
                    view_bottom_4.setVisibility(View.GONE);
                    spinnerPartyDistrict.setVisibility(View.GONE);
                    textLabelPartyDistrict.setVisibility(View.GONE);
                    viewBottomPartyDistrict.setVisibility(View.GONE);

                    spinnerConstituency.setVisibility(View.GONE);
                    textLabelConstituency.setVisibility(View.GONE);
                    viewBottomConstituency.setVisibility(View.GONE);

                    spinnerDivision.setVisibility(View.GONE);
                    textLabelDivision.setVisibility(View.GONE);
                    viewBottomDivision.setVisibility(View.GONE);


                    spinnerPart.setVisibility(View.GONE);
                    textLabelPart.setVisibility(View.GONE);
                    viewBottomPart.setVisibility(View.GONE);

                    spinnerUnionType.setVisibility(View.GONE);
                    textLabelUnionType.setVisibility(View.GONE);
                    viewBottomUnionType.setVisibility(View.GONE);

                    spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                    textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                    viewBottomTownandVillagePanchayat.setVisibility(View.GONE);

                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottomWard.setVisibility(View.GONE);

                    spinnerBooth.setVisibility(View.INVISIBLE);
                    textLabelBooth.setVisibility(View.INVISIBLE);
                    viewBottomBooth.setVisibility(View.INVISIBLE);
                    editStartDate.setText("");
                    editEndDate.setText("");
                    editvenue.setText("");
                    editTitle.setText("");
                    editTimeSub.setText("");


                }
            }
        });
        spinnerConstituency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                ConstituencyListResultsItem constituencyListResultsItem = constituencyList.get(position);
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                if (constituencyListResultsItem.getId() != 0) {
                    constituencyID = constituencyListResultsItem.getId();
                    getDivisionList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                DivisionListResultsItem divisionListResultsItem = divisionlist.get(position);
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                switch (divisionListResultsItem.getId()) {
                    case 0:
                        break;
                    case 1:
//                        Corporation
                        spinnerUnionType.setVisibility(View.GONE);
                        textLabelUnionType.setVisibility(View.GONE);
                        viewBottomUnionType.setVisibility(View.GONE);
                        unionType = 0;
                        divisionID = divisionListResultsItem.getId();
                        divisionName = divisionListResultsItem.getDivisionname();

                        if (constituencyID != 0 && districtID != 0 && divisionID != 0) {
                            getPartList(constituencyID, districtID, divisionID);
                        } else {

                            Toast.makeText(getActivity(), getString(R.string.choose_district), Toast.LENGTH_SHORT).show();
                            spinnerDivision.setSelection(0);
                        }

                        break;
                    case 2:
//                        Municipality
                        divisionID = divisionListResultsItem.getId();
                        divisionName = divisionListResultsItem.getDivisionname();
                        unionType = 0;
                        spinnerUnionType.setVisibility(View.GONE);
                        textLabelUnionType.setVisibility(View.GONE);
                        viewBottomUnionType.setVisibility(View.GONE);
                        if (constituencyID != 0 && districtID != 0 && divisionID != 0) {
                            getMunicipalityList(constituencyID, districtID, divisionID);
                        } else {

                            Toast.makeText(getActivity(), getString(R.string.choose_district), Toast.LENGTH_SHORT).show();
                            spinnerDivision.setSelection(0);
                        }

                        break;
                    case 3:
//                        PanchayatUnion
                        spinnerUnionType.setVisibility(View.VISIBLE);
                        textLabelUnionType.setVisibility(View.VISIBLE);
                        viewBottomUnionType.setVisibility(View.VISIBLE);
                        divisionID = divisionListResultsItem.getId();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerUnionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                int selectedTypePos = adapterView.getSelectedItemPosition();
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                if (selectedTypePos != 1) {
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottomWard.setVisibility(View.GONE);
                }
                if (selectedTypePos != 0) {
                    if (constituencyID != 0 && districtID != 0 && divisionID != 0) {
                        //                1-  //TownPanchayat 2-//VillagePanchayat
                        unionType = selectedTypePos;
                        getPanchayatUnionList(constituencyID, districtID, divisionID, unionType);
                        ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.choose_district), Toast.LENGTH_SHORT).show();
                        spinnerDivision.setSelection(0);
                    }
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerPart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                switch (divisionID) {
                    case 1:
//                        Corporation
                        PartListResultsItem partListResultsItem = partList.get(position);
                        if (partListResultsItem.getId() != 0) {
                            partID = partListResultsItem.getId();
                            getWardList(divisionID, partID);
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        } else {
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottomWard.setVisibility(View.GONE);
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                        }
                        break;
                    case 2:
//                        Municipality
                        MunicipalityListResultsItem municipalityListResultsItem = municipalityList.get(position);
                        if (municipalityListResultsItem.getId() != 0) {
                            partID = municipalityListResultsItem.getId();
                            getWardList(divisionID, partID);
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        } else {
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottomWard.setVisibility(View.GONE);
                            ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                        }
                        break;
                    case 3:
                        //PanchayatUnion
                        if (unionType == 1) {
                            //TownPanchayat

                            PanchayatUnionResultsItem panchayatUnionResultsItem = panchayatList.get(position);
                            if (panchayatUnionResultsItem.getId() != 0) {
                                partID = panchayatUnionResultsItem.getId();
                                getTownPanchayatList(constituencyID, districtID, partID, unionType);
                                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                            } else {
                                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);
                                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                            }
                        } else {
                            //VillagePanchayat

                            PanchayatUnionResultsItem panchayatUnionResultsItem = panchayatList.get(position);
                            if (panchayatUnionResultsItem.getId() != 0) {
                                partID = panchayatUnionResultsItem.getId();
                                getVillagePanchayatList(constituencyID, partID, districtID, unionType);
                                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                            } else {
                                spinnerTownandVillagePanchayat.setVisibility(View.GONE);
                                textLabelTownandVillagePanchayat.setVisibility(View.GONE);
                                viewBottomTownandVillagePanchayat.setVisibility(View.GONE);
                                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                            }
                        }


                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerTownandVillagePanchayat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                switch (unionType) {
                    case 1:
                        //TownPanchayat
                        TownPanchayatResultsItem townPanchayatResultsItem = townPanchayatList.get(position);
                        if (townPanchayatResultsItem.getId() != 0) {
                            getWardList(divisionID, partID);


                        } else {
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottomWard.setVisibility(View.GONE);

                        }
                        break;
                    case 2:
                        //Village panchayat
                        VillagePanchayatResultsItem villagePanchayatResultsItem = villagePanchayatResultsItemArrayList.get(position);
                        if (villagePanchayatResultsItem.getId() != 0) {
                            villageID = villagePanchayatResultsItem.getId();
                            getBoothList(divisionID, partID, wardID, villageID, unionType);
                        }

                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerWard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                WardListResultsItem wardListResultsItem = wardList.get(position);
                if (wardListResultsItem.getId() != 0) {
                    wardID = wardListResultsItem.getId();
                    getBoothList(divisionID, partID, wardID, villageID, unionType);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerBooth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                BoothListResultsItem boothResultsItem = boothList.get(position);
                if (boothResultsItem.getId() != 0) {
                    boothID = boothResultsItem.getId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (!isFABOpen) {
                        return false;
                    } else {
                        closeFABMenu();
                        return true;
                    }
                }
                return false;
            }
        });

        return view;
    }

    @OnClick({R.id.edit_start_date, R.id.edit_end_date, R.id.image_delete,
            R.id.image_photo, R.id.image_video, R.id.image_audio, R.id.image_send1,
            R.id.float_mes_compose, R.id.float_attach_audio, R.id.float_attach_video,
            R.id.float_attach_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_start_date:
                selectedDate = 1;
                datePickerDialog();
                break;
            case R.id.edit_end_date:
                selectedDate = 2;
                datePickerDialog();
                break;
            case R.id.image_delete:
                selectedVideoAudioUri = null;
                videoSelected.setVisibility(View.GONE);
                imageDelete.setVisibility(View.GONE);
                break;
            case R.id.image_photo:
//                getImageFiles();
                break;
            case R.id.image_video:
//                getVideoFiles();
                break;
            case R.id.image_audio:
//                getAudiofile();
                break;
            case R.id.image_send1:
                break;
            case R.id.float_mes_compose:
                if (!isFABOpen) {
                    showFABMenu();
                } else {
                    closeFABMenu();
                }
                break;
            case R.id.float_attach_audio:
                closeFABMenu();
//                getAudiofile();
                getImageFiles();
                break;
            case R.id.float_attach_video:
                closeFABMenu();
                getVideoFiles();
                break;
            case R.id.float_attach_image:
                closeFABMenu();
//                getImageFiles();
                break;
        }
    }

    private void showFABMenu() {
        isFABOpen = true;
        floatAttachAudio.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        floatAttachImage.animate().translationX(-getResources().getDimension(R.dimen.standard_55));
        floatAttachVideo.animate().translationY(getResources().getDimension(R.dimen.standard_55));
    }

    private void closeFABMenu() {
        isFABOpen = false;
        floatAttachAudio.animate().translationY(0);
        floatAttachImage.animate().translationX(0);
        floatAttachVideo.animate().translationY(0);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save, menu);
        sumbit = menu.findItem(R.id.menu_bar_save);
        sumbit.setTitle(R.string.submit);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bar_save:
                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(getActivity());
                alertDialog1.setMessage(getString(R.string.confirm_to_post));
                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        profileNullCheck();
                    }
                });
                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        hideProgress();
                    }
                });
                alertDialog1.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Date dob = null;
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        if (selectedDate == 1) {
            //startDate
            calendarStartdate = Calendar.getInstance();
            calendarStartdate.set(Calendar.MONTH, monthOfYear);
            calendarStartdate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            calendarStartdate.set(Calendar.YEAR, year);
            eventStartDate = simpleDateFormat.format(calendarStartdate.getTime());
            try {
                dob = simpleDateFormat.parse(eventStartDate);
                simpleDateFormat.applyPattern("yyyy-MM-dd");
                eventStartDateFormat = simpleDateFormat.format(dob);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (selectedDate == 2) {
            //endDate
            calendarEnddate = Calendar.getInstance();
            calendarEnddate.set(Calendar.MONTH, monthOfYear);
            calendarEnddate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            calendarEnddate.set(Calendar.YEAR, year);
            simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            eventEndDate = simpleDateFormat.format(calendarEnddate.getTime());
            try {
                dob = simpleDateFormat.parse(eventEndDate);
                simpleDateFormat.applyPattern("yyyy-MM-dd");
                eventEndDateFormat = simpleDateFormat.format(dob);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        timePickerDialog();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        simpleDateFormat = new SimpleDateFormat("hh:mm a");
        simpleDateFormat2 = new SimpleDateFormat("HH:mm:ss");
        if (selectedDate == 1) {
//            start date
            calendarStartTime = Calendar.getInstance();
            calendarStartTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendarStartTime.set(Calendar.MINUTE, minute);
            eventStartTime = simpleDateFormat.format(calendarStartTime.getTime());
            if (editStartDate.getText().toString() != null &&
                    !editStartDate.getText().toString().equalsIgnoreCase(eventStartDate + " " + eventStartTime)) {
                editEndDate.setText("");
            }
            editStartDate.setText(eventStartDate + " " + eventStartTime);
            editEndDate.setEnabled(true);
            eventStartTimeFormat = simpleDateFormat2.format(calendarStartTime.getTime());
        } else if (selectedDate == 2) {
            //end date
            calendarEndTime = Calendar.getInstance();
            calendarEndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendarEndTime.set(Calendar.MINUTE, minute);
            eventEndTime = simpleDateFormat.format(calendarEndTime.getTime());
            editEndDate.setText(eventEndDate + " " + eventEndTime);
            eventEndTimeFormat = simpleDateFormat2.format(calendarEndTime.getTime());

        }

    }


    private void datePickerDialog() {
        if (selectedDate == 1) {
            //start date
            currentD = Calendar.getInstance();
        } else if (selectedDate == 2) {
            //end date
            currentD = calendarStartdate;
        }
        DatePickerDialog datePickerDialog =
                DatePickerDialog.newInstance(this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setAccentColor(getResources().getColor(R.color.red));
        datePickerDialog.setMinDate(currentD);
        datePickerDialog.setCancelText(getString(R.string.Alert_Cancel));
        datePickerDialog.setOkText(getString(R.string.Alert_Ok));
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    private void timePickerDialog() {
        if (selectedDate == 2 && eventStartDateFormat.equalsIgnoreCase(eventEndDateFormat)) {
            // end date
            currentT = calendarStartTime;
        } else if (selectedDate == 1) {
//            start date
            currentT = Calendar.getInstance();
        }
        TimePickerDialog timePickerDialog =
                TimePickerDialog.newInstance(this, currentT.get(Calendar.HOUR_OF_DAY),
                        currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND), false);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.red));
        if (selectedDate == 2 && eventStartDateFormat.equalsIgnoreCase(eventEndDateFormat)) {
            //end date
            timePickerDialog.setMinTime(currentT.get(Calendar.HOUR_OF_DAY),
                    currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND));
        } else if (selectedDate == 1) {
            //start date
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String currentDate = df.format(c);
            if (currentDate.equalsIgnoreCase(eventStartDateFormat)) {
                timePickerDialog.setMinTime(currentT.get(Calendar.HOUR_OF_DAY),
                        currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND));
            }
        }
        timePickerDialog.setCancelText(getString(R.string.Alert_Cancel));
        timePickerDialog.setOkText(getString(R.string.Alert_Ok));
        timePickerDialog.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == SELECT_IMAGE_FROM_GALLERY && resultCode == RESULT_OK
                    && null != data) {
                imageFromGalleryResult(data);
            } else if (requestCode == SELECT_IMAGE_FROM_CAMERA && resultCode == RESULT_OK &&
                    data != null) {
                imageFromCameraResult(data);
            } else if (requestCode == SELECT_VIDEO_FROM_GALLERY && resultCode == RESULT_OK &&
                    data != null) {
                videoFromGalleryResult(data);
            } else if (requestCode == SELECT_VIDEO_FROM_CAMERA && resultCode == RESULT_OK &&
                    data != null) {
                videoFromCameraResult(data);
            } else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO_AUDIO && resultCode == RESULT_OK &&
                    data != null) {
                getAudioGalleryResult(data);
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    private void getImageFiles() {
        final Dialog dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.setContentView(R.layout.media_dialog);
        // Set dialog title
        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
        textCamera.setText(R.string.camera);
        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
        imageCamera.setImageResource(R.mipmap.icon_photo_camera);
        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
        textGallery.setText(R.string.gallery);
        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
        imageGallery.setImageResource(R.mipmap.icon_gallery);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_REQUEST_CODE_CAMERA);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, SELECT_IMAGE_FROM_CAMERA);
                    }
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, SELECT_IMAGE_FROM_CAMERA);
                }
                dialog.dismiss();
            }
        });
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE_GALLERY);
                    } else {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                        intent.setType("image/*");
                        startActivityForResult(
                                Intent.createChooser(intent, "Select File"),
                                SELECT_IMAGE_FROM_GALLERY);

                    }
                } else {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_IMAGE_FROM_GALLERY);

                }
                dialog.dismiss();
            }

        });

        // set values for custom dialog components - text, image and button
        dialog.show();


    }

    private void getVideoFiles() {
        final Dialog dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.setContentView(R.layout.media_dialog);
        // Set dialog title
        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
        textCamera.setText(R.string.video);
        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
        imageCamera.setImageResource(R.mipmap.icon_photo_camera);
        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
        textGallery.setText(R.string.gallery);
        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
        imageGallery.setImageResource(R.mipmap.icon_gallery);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_REQUEST_CODE_CAMERA);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        startActivityForResult(intent, SELECT_VIDEO_FROM_CAMERA);
                    }
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    startActivityForResult(intent, SELECT_VIDEO_FROM_CAMERA);
                }
                dialog.dismiss();
            }
        });
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE_GALLERY);
                    } else {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("video/*");
                        startActivityForResult(
                                Intent.createChooser(intent, "Select File"),
                                SELECT_VIDEO_FROM_GALLERY);
                    }
                } else {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("video/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_VIDEO_FROM_GALLERY);
                }
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void getAudiofile() {
        final Dialog dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.setContentView(R.layout.media_dialog);
        // Set dialog title
        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
        textCamera.setText(R.string.record);
        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
        imageCamera.setImageResource(R.mipmap.icon_mic);
        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
        textGallery.setText(R.string.gallery);
        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
        imageGallery.setImageResource(R.mipmap.icon_gallery);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAudioRecordDialog();
                dialog.dismiss();
            }
        });
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("icon_attach_audio/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Media Files"), REQUEST_TAKE_GALLERY_VIDEO_AUDIO);
                dialog.dismiss();
            }
        });
        dialog.show();


    }


    private void imageFromCameraResult(Intent data) {

        gridImage.setVisibility(View.VISIBLE);
        if (selectedVideoAudioUri != null || videoFromResponse) {
            videoSelected.setVisibility(View.VISIBLE);
            imageDelete.setVisibility(View.VISIBLE);
        } else {
            videoSelected.setVisibility(View.INVISIBLE);
            imageDelete.setVisibility(View.INVISIBLE);
        }

        Bitmap capturedImageBitmap = null;
        if (data != null) {
            capturedImageBitmap = (Bitmap) data.getExtras().get("data");
        }

        if (capturedImageBitmap != null) {
            selectedImageList.add(new SelectedImage(null, 1,
                    capturedImageBitmap, false, System.currentTimeMillis() + ".jpg"));
        }
        selectedImageAdapter = new SelectedImageAdapter(selectedImageList, getActivity());
        gridImage.setAdapter(selectedImageAdapter);
    }

    private void imageFromGalleryResult(Intent data) {


        boolean found = false;
        if (data.getData() != null) {
            Uri mImageUri = data.getData();

            String rettt = getImagePath(mImageUri);
            if (rettt == null) {
                String endextension = mImageUri.toString();
                int lenght = endextension.length();
                endextension = endextension.substring(lenght - 3);
                if (endextension != null) {
                    for (String element : imageFormat) {
                        if (element.equals(endextension)) {
                            found = true;
                            videoFromResponse = false;
                            break;
                        }
                    }
                    if (found) {
                        gridImage.setVisibility(View.VISIBLE);
                        if (selectedVideoAudioUri != null || videoFromResponse) {
                            videoSelected.setVisibility(View.VISIBLE);
                            imageDelete.setVisibility(View.VISIBLE);
                        } else {
                            videoSelected.setVisibility(View.INVISIBLE);
                            imageDelete.setVisibility(View.INVISIBLE);
                        }
                        if (mImageUri != null) {
                            selectedImageList.add(new SelectedImage(null, 1,
                                    compressInputImage(mImageUri, getContext()), false, System.currentTimeMillis() + ".jpg"));
                        }
                        selectedImageAdapter = new SelectedImageAdapter(selectedImageList, getContext());
                        gridImage.setAdapter(selectedImageAdapter);
                    } else {
                        Toast.makeText(getActivity(), "select image only", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {


                String extenstion = getFileExt(rettt);
                if (extenstion != null) {
                    for (String element : imageFormat) {
                        if (element.equals(extenstion)) {
                            found = true;
                            videoFromResponse = false;
                            break;
                        }
                    }
                    if (found) {
                        gridImage.setVisibility(View.VISIBLE);
                        if (selectedVideoAudioUri != null || videoFromResponse) {
                            videoSelected.setVisibility(View.VISIBLE);
                            imageDelete.setVisibility(View.VISIBLE);
                        } else {
                            videoSelected.setVisibility(View.INVISIBLE);
                            imageDelete.setVisibility(View.INVISIBLE);
                        }

                        //                Fetch Single Image
                        if (mImageUri != null) {
                            selectedImageList.add(new SelectedImage(null, 1,
                                    compressInputImage(mImageUri, getContext()), false, System.currentTimeMillis() + ".jpg"));
                        }
                        selectedImageAdapter = new SelectedImageAdapter(selectedImageList, getContext());
                        gridImage.setAdapter(selectedImageAdapter);

                    }


                } else {
                    System.out.println("The value is Not found!");
                    Toast.makeText(getActivity(), "select image only", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (data.getClipData() != null) {
            gridImage.setVisibility(View.VISIBLE);
            if (selectedVideoAudioUri != null || videoFromResponse) {
                videoSelected.setVisibility(View.VISIBLE);
                imageDelete.setVisibility(View.VISIBLE);
            } else {
                videoSelected.setVisibility(View.INVISIBLE);
                imageDelete.setVisibility(View.INVISIBLE);
            }
            ClipData mClipData = data.getClipData();
            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
            for (int i = 0; i < mClipData.getItemCount(); i++) {
                ClipData.Item item = mClipData.getItemAt(i);
                Uri uri = item.getUri();
                mArrayUri.add(uri);
                if (uri != null) {
                    selectedImageList.add(new SelectedImage(null, 1,
                            compressInputImage(uri, getContext()), false, System.currentTimeMillis() + ".jpg"));
                }
            }


            selectedImageAdapter = new SelectedImageAdapter(selectedImageList, getContext());
            gridImage.setAdapter(selectedImageAdapter);


        } else {
            System.out.println("The value is Not found!");
            Toast.makeText(getActivity(), "select image only", Toast.LENGTH_SHORT).show();
        }


    }

    private void videoFromGalleryResult(Intent data) {


        boolean found = false;
        selectedVideoAudioUri = data.getData();
        // OI FILE Manager Path
        galleryVideoPath = selectedVideoAudioUri.getPath();
        galleryVideoPath = getPath(selectedVideoAudioUri);
        String extenstion = getFileExt(galleryVideoPath);
        for (String element : audioimageFormat) {
            if (element.equals(extenstion)) {
                found = true;
                break;
            }
        }
        if (found) {

            //gridImage.setVisibility(View.INVISIBLE);
            //videoSelected.setVisibility(View.INVISIBLE);
            //imageDelete.setVisibility(View.INVISIBLE);
            selectedVideoAudioUri = null;
            Toast.makeText(getActivity(), "Upload Only video", Toast.LENGTH_SHORT).show();
        } else {
            gridImage.setVisibility(View.VISIBLE);
            videoSelected.setVisibility(View.VISIBLE);
            imageDelete.setVisibility(View.VISIBLE);
            videoSelected.setUp(galleryVideoPath
                    , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
            Glide.with(this)
                    .load(Uri.fromFile(new File(galleryVideoPath)))
                    .into(videoSelected.thumbImageView);
            if (selectedVideoAudioUri != null) {
                videoAudioFileNameInternal = System.currentTimeMillis() + ".mp4";
                videoThubmnail = galleryVideoPath;
            }
        }
        // MEDIA GALLERY Path
//        recordedVideoPath = getPath(selectedImageUri);
//        "/storage/2348-13EA/DCIM/Camera/VID_20180511_182119029.mp4"
//        /storage/2348-13EA/DCIM/Camera/VID_20180511_184203378.mp4

    }

    private void videoFromCameraResult(Intent data) {

        gridImage.setVisibility(View.VISIBLE);
        videoSelected.setVisibility(View.VISIBLE);
        imageDelete.setVisibility(View.VISIBLE);

        selectedVideoAudioUri = data.getData();
        // OI FILE Manager Path
//        galleryVideoPath = selectedImageUri.getPath();
        // MEDIA GALLERY Path
        recordedVideoPath = getPath(selectedVideoAudioUri);
        videoSelected.setUp(recordedVideoPath
                , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
        Glide.with(this)
                .load(Uri.fromFile(new File(recordedVideoPath)))
                .into(videoSelected.thumbImageView);
        if (selectedVideoAudioUri != null) {
            videoAudioFileNameInternal = System.currentTimeMillis() + ".mp4";
            videoThubmnail = recordedVideoPath;

        }

    }


    private void getAudioGalleryResult(Intent data) {


        //Fetch Video and Audio File
        selectedVideoAudioUri = data.getData();
        // OI FILE Manager Path
        galleryAudioPath = selectedVideoAudioUri.getPath();
        boolean found = false;
        String extenstion = getFileExt(galleryAudioPath);
        for (String element : audioFormat) {
            if (element.equals(extenstion)) {
                found = true;
                videoFromResponse = false;
                System.out.println("The value is found!");
                break;
            }
        }
        if (!found) {
            //galleryAudioPath=null;
            //gridImage.setVisibility(View.INVISIBLE);
            videoSelected.setVisibility(View.INVISIBLE);
            imageDelete.setVisibility(View.INVISIBLE);
            selectedVideoAudioUri = null;
            Toast.makeText(getActivity(), "Upload Only Audio", Toast.LENGTH_SHORT).show();
        } else {
            gridImage.setVisibility(View.VISIBLE);
            videoSelected.setVisibility(View.VISIBLE);
            imageDelete.setVisibility(View.VISIBLE);
            videoSelected.setUp(galleryAudioPath
                    , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
            if (isVideoFile(galleryAudioPath)) {
                Glide.with(this)
                        .load(Uri.fromFile(new File(galleryAudioPath)))
                        .into(videoSelected.thumbImageView);
            } else {
                Glide.with(this)
                        .load(R.mipmap.icon_audio_thumbnail).into(videoSelected.thumbImageView);

            }
            if (selectedVideoAudioUri != null) {
                videoAudioFileNameInternal = System.currentTimeMillis() + ".mp3";
            }
        }
        // MEDIA GALLERY Path
        //recordedVideoPath = getPath(selectedImageUri);

    }

    private void getAudioRecordedResult() {

        gridImage.setVisibility(View.VISIBLE);
        videoSelected.setVisibility(View.VISIBLE);
        imageDelete.setVisibility(View.VISIBLE);

        selectedVideoAudioUri = Uri.parse(mOutputFile.getAbsolutePath());
        videoSelected.setUp(mOutputFile.getAbsolutePath()
                , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");

        //Load thumbnail image for only local storage videos
        Glide.with(this)
                .load(R.mipmap.icon_audio_thumbnail).into(videoSelected.thumbImageView);
        if (selectedVideoAudioUri != null) {
            videoAudioFileNameInternal = audioRecordedPathName;
        }
        isAudioFromRecord = true;
    }

    public void showAudioRecordDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_timeline_audiorecord);
        textTimer = (TextView) dialog.findViewById(R.id.text_dialog);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording(true);
                getAudioRecordedResult();
                dialog.dismiss();
            }
        });
        dialog.show();
        startAudioRecording();
    }


    private void startAudioRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
            mRecorder.setAudioEncodingBitRate(48000);
        } else {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.setAudioEncodingBitRate(64000);
        }
        mRecorder.setAudioSamplingRate(16000);
        mOutputFile = getOutputFile();
        mOutputFile.getParentFile().mkdirs();
        mRecorder.setOutputFile(mOutputFile.getAbsolutePath());

        try {
            mRecorder.prepare();
            mRecorder.start();
            mStartTime = SystemClock.elapsedRealtime();
            mHandler.postDelayed(mTickExecutor, 100);
            Log.d("Voice Recorder", "started recording to " + mOutputFile.getAbsolutePath());
        } catch (IOException e) {
            Log.e("Voice Recorder", "prepare() failed " + e.getMessage());
        }
    }

    protected void stopRecording(boolean saveFile) {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        mStartTime = 0;

        mHandler.removeCallbacks(mTickExecutor);
        if (!saveFile && mOutputFile != null) {
            mOutputFile.delete();
        }

    }

    private void audioTimer() {
        long time = (mStartTime < 0) ? 0 : (SystemClock.elapsedRealtime() - mStartTime);
        int minutes = (int) (time / 60000);
        int seconds = (int) (time / 1000) % 60;
        int milliseconds = (int) (time / 100) % 10;
        textTimer.setText(minutes + ":" + (seconds < 10 ? "0" + seconds : seconds));
        if (mRecorder != null) {
            amplitudes[i] = mRecorder.getMaxAmplitude();
            //Log.d("Voice Recorder","amplitude: "+(amplitudes[i] * 100 / 32767));
            if (i >= amplitudes.length - 1) {
                i = 0;
            } else {
                ++i;
            }
        }
    }


    private File getOutputFile() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US);
        audioRecordedPathName = dateFormat.format(new Date()) + ".m4a";
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/Voice Recorder/RECORDING_"
                + audioRecordedPathName);
    }


    public static boolean isVideoFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("video");
    }


    public void getDistrictList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            districtList.clear();
            districtList.add(0, new DistrictsItem(getString(R.string.select_district), 0));
            DistrictList districtListparam = new DistrictList();
            districtListparam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getDistrictList(headerMap, districtListparam).enqueue(new Callback<DistrictListResponse>() {
                @Override
                public void onResponse(Call<DistrictListResponse> call, Response<DistrictListResponse> response) {
                    hideProgress();
                    districtListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (districtListResponse.getResults() != null) {
                            for (int i = 0; i < districtListResponse.getResults().getDistricts().size(); i++) {
                                districtList.add(new DistrictsItem(districtListResponse.getResults().getDistricts().get(i).getDistrictName(),
                                        districtListResponse.getResults().getDistricts().get(i).getId()));
                            }
                            DistrictAdpater adapterdivisionList = new DistrictAdpater(districtList, getActivity());
                            spinnerDistrict.setAdapter(adapterdivisionList);
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.no_it_district), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), districtListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DistrictListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.no_it_district), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    private void getPartyDistrictlist(int districtID) {
        PartyDistrictInputParam partyDistrictInputParam = new PartyDistrictInputParam();
        partyDistrictInputParam.setDistrictid(districtID);
        if (Util.isNetworkAvailable()) {
            showProgress();
            partyDistrictResultsItems.clear();
            partyDistrictResultsItems.add(new PartyDistrictResultsItem(getString(R.string.select_party_district), 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPartyDistrictList(headerMap, partyDistrictInputParam).enqueue(new Callback<PartyDistrictResponse>() {
                @Override
                public void onResponse(Call<PartyDistrictResponse> call, Response<PartyDistrictResponse> response) {
                    hideProgress();
                    partyDistrictResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (partyDistrictResponse.getResults() != null) {
                            for (int i = 0; i < partyDistrictResponse.getResults().size(); i++) {
                                partyDistrictResultsItems.add(new PartyDistrictResultsItem(
                                        partyDistrictResponse.getResults().get(i).getDistrictName(),
                                        partyDistrictResponse.getResults().get(i).getId()));
                            }
                            PartyDistrictAdapter partyDistrictAdapter = new PartyDistrictAdapter(
                                    partyDistrictResultsItems, getActivity());
                            spinnerPartyDistrict.setAdapter(partyDistrictAdapter);
                        } else {
                            Toast.makeText(getActivity(), partyDistrictResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_party_district), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<PartyDistrictResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getConsituencyList(int districtID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            constituencyList.clear();
            constituencyList.add(0, new ConstituencyListResultsItem(getString(R.string.select_constituency), 0));
            ConsituencyList consituencyList = new ConsituencyList();
            consituencyList.setDistrictID(districtID);
            consituencyList.setType(2);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getConstituencyList(headerMap, consituencyList).enqueue(new Callback<ConsituencyListResponse>() {
                @Override
                public void onResponse(Call<ConsituencyListResponse> call, Response<ConsituencyListResponse> response) {
                    hideProgress();
                    consituencyListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200
                            && consituencyListResponse != null) {
                        if (consituencyListResponse.getResults() != null) {
                            for (int i = 0; i < consituencyListResponse.getResults().size(); i++) {
                                constituencyList.add(new ConstituencyListResultsItem(
                                        consituencyListResponse.getResults().get(i).getConstituencyName() != null ?
                                                consituencyListResponse.getResults().get(i).getConstituencyName() : "",
                                        consituencyListResponse.getResults().get(i).getId() != 0 ?
                                                consituencyListResponse.getResults().get(i).getId() : 0));
                            }
                            ConstituencyAdapter adapterConstituencyList = new ConstituencyAdapter(constituencyList, getActivity());
                            spinnerConstituency.setAdapter(adapterConstituencyList);
                        } else {
                            Toast.makeText(getActivity(), consituencyListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_constituency), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ConsituencyListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.no_it_constituency), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    public void getDivisionList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            divisionlist.clear();
            divisionlist.add(0, new DivisionListResultsItem(getString(R.string.select_division), 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getDivisionList(headerMap).enqueue(new Callback<DivisionListResponse>() {
                @Override
                public void onResponse(Call<DivisionListResponse> call, Response<DivisionListResponse> response) {
                    hideProgress();
                    divisionListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200
                            && divisionListResponse != null) {
                        if (divisionListResponse.getResults() != null) {
                            for (int i = 0; i < divisionListResponse.getResults().size(); i++) {
                                divisionlist.add(new DivisionListResultsItem(
                                        divisionListResponse.getResults().get(i).getDivisionname() != null ?
                                                divisionListResponse.getResults().get(i).getDivisionname() : "",
                                        divisionListResponse.getResults().get(i).getId() != 0 ?
                                                divisionListResponse.getResults().get(i).getId() : 0));
                            }
                            DivisionAdapter adapterdivisionList = new DivisionAdapter(divisionlist, getActivity());
                            spinnerDivision.setAdapter(adapterdivisionList);
                        } else {
                            Toast.makeText(getActivity(), divisionListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_division), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DivisionListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.no_it_division), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getPartList(int constituencyID, int districtID, int divisionID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            partList.clear();
            partList.add(0, new PartListResultsItem(getString(R.string.select_part), 0));
            PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setDivisionID(divisionID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPartList(headerMap, partListParam).enqueue(new Callback<PartListResponse>() {
                @Override
                public void onResponse(Call<PartListResponse> call, Response<PartListResponse> response) {
                    hideProgress();
                    partListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && partListResponse != null) {
                        if (partListResponse.getResults() != null) {
                            for (int i = 0; i < partListResponse.getResults().size(); i++) {
                                partList.add(new PartListResultsItem(
                                        partListResponse.getResults().get(i).getPartname() != null ?
                                                partListResponse.getResults().get(i).getPartname() : "",
                                        partListResponse.getResults().get(i).getId() != 0 ?
                                                partListResponse.getResults().get(i).getId() : 0));
                            }
                            PartAdapter partAdapter = new PartAdapter(partList, getActivity());
                            spinnerPart.setAdapter(partAdapter);
                        } else {
                            Toast.makeText(getActivity(), partListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_part), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PartListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.no_it_part), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    public void getMunicipalityList(int constituencyID, int districtID, int divisionID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            municipalityList.clear();
            municipalityList.add(0, new MunicipalityListResultsItem(0, getString(R.string.select_town_municipality)));
            textLabelPart.setText(getString(R.string.municipallity));
            PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setDivisionID(divisionID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getMunicipalitytList(headerMap, partListParam).enqueue(new Callback<MunicipalityListResponse>() {
                @Override
                public void onResponse(Call<MunicipalityListResponse> call, Response<MunicipalityListResponse> response) {
                    hideProgress();
                    municipalityListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && municipalityListResponse != null) {
                        if (municipalityListResponse.getResults() != null) {
                            for (int i = 0; i < municipalityListResponse.getResults().size(); i++) {
                                municipalityList.add(new MunicipalityListResultsItem(
                                        municipalityListResponse.getResults().get(i).getId() != 0 ?
                                                municipalityListResponse.getResults().get(i).getId() : 0,
                                        municipalityListResponse.getResults().get(i).getMunicipalityname() != null ?
                                                municipalityListResponse.getResults().get(i).getMunicipalityname() : ""));
                            }
                            MunicipalityAdapter partAdapter = new MunicipalityAdapter(municipalityList, getActivity());
                            spinnerPart.setAdapter(partAdapter);
                        } else {
                            Toast.makeText(getActivity(), municipalityListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_municipality), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<MunicipalityListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.no_it_municipality), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getPanchayatUnionList(int constituencyID, int districtID, int divisionID, int unionType) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            panchayatList.clear();
            panchayatList.add(0, new PanchayatUnionResultsItem(getString(R.string.select_panchayat_union), 0));
            textLabelPart.setText(R.string.pachayat_union);
            final PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setDivisionID(divisionID);
            partListParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPanchayatUnionList(headerMap, partListParam).enqueue(new Callback<PanchayatUnionListResponse>() {
                @Override
                public void onResponse(Call<PanchayatUnionListResponse> call, Response<PanchayatUnionListResponse> response) {
                    hideProgress();
                    panchayatUnionListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && panchayatUnionListResponse != null) {
                        if (panchayatUnionListResponse.getResults() != null) {
                            for (int i = 0; i < panchayatUnionListResponse.getResults().size(); i++) {
                                panchayatList.add(new PanchayatUnionResultsItem(
                                        panchayatUnionListResponse.getResults().get(i).getVillagePanchayatName() != null
                                                ? panchayatUnionListResponse.getResults().get(i).getVillagePanchayatName() : "",
                                        panchayatUnionListResponse.getResults().get(i).getId() != 0 ?
                                                panchayatUnionListResponse.getResults().get(i).getId() : 0));
                            }
                            PanchyatUnionAdapter partAdapter = new PanchyatUnionAdapter(panchayatList, getActivity());
                            spinnerPart.setAdapter(partAdapter);
                        } else {
                            Toast.makeText(getActivity(), panchayatUnionListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_panchayat), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PanchayatUnionListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.no_it_panchayat), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    public void getTownPanchayatList(int constituencyID, int districtID, int partID, int unionType) {
        if (Util.isNetworkAvailable()) {
            showProgress();

            townPanchayatList.clear();
            townPanchayatList.add(0, new TownPanchayatResultsItem(getString(R.string.select_town_panchayat), 0));
            textLabelTownandVillagePanchayat.setText(R.string.town_pachayat);
            final PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setUnionID(partID);
            partListParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getTownPanchayatList(headerMap, partListParam).enqueue(new Callback<TownPanchayatListResponse>() {
                @Override
                public void onResponse(Call<TownPanchayatListResponse> call, Response<TownPanchayatListResponse> response) {
                    hideProgress();
                    townPanchayatListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && townPanchayatListResponse != null) {
                        if (townPanchayatListResponse.getResults() != null) {
                            if (townPanchayatListResponse.getResults().size() > 0) {
                                spinnerTownandVillagePanchayat.setVisibility(View.VISIBLE);
                                textLabelTownandVillagePanchayat.setVisibility(View.VISIBLE);
                                viewBottomTownandVillagePanchayat.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < townPanchayatListResponse.getResults().size(); i++) {
                                townPanchayatList.add(new TownPanchayatResultsItem(
                                        townPanchayatListResponse.getResults().get(i).getTownpanchayatname() != null ?
                                                townPanchayatListResponse.getResults().get(i).getTownpanchayatname() : "",
                                        townPanchayatListResponse.getResults().get(i).getId() != 0 ?
                                                townPanchayatListResponse.getResults().get(i).getId() : 0));
                            }
                            TownPanchayatAdapter partAdapter = new TownPanchayatAdapter(townPanchayatList, getActivity());
                            spinnerTownandVillagePanchayat.setAdapter(partAdapter);
                        } else {
                            Toast.makeText(getActivity(), townPanchayatListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_town_panchayat), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TownPanchayatListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.no_it_town_panchayat), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();

        }


    }


    public void getVillagePanchayatList(int constituencyID, int partID, int districtID, int unionType) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            villagePanchayatResultsItemArrayList.clear();
            villagePanchayatResultsItemArrayList.add(0, new VillagePanchayatResultsItem(getString(R.string.select_village_panchayat), 0));
            textLabelTownandVillagePanchayat.setText(R.string.village_panchayat);
            final VattamWardList partListParam = new VattamWardList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setUnionID(partID);
            partListParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getVillagePanchayatList(headerMap, partListParam).enqueue(new Callback<VillagePanchayatListResponse>() {
                @Override
                public void onResponse(Call<VillagePanchayatListResponse> call, Response<VillagePanchayatListResponse> response) {
                    hideProgress();
                    villagePanchayatListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200
                            && villagePanchayatListResponse != null) {
                        if (villagePanchayatListResponse.getResults() != null) {
                            if (villagePanchayatListResponse.getResults().size() > 0) {
                                spinnerTownandVillagePanchayat.setVisibility(View.VISIBLE);
                                textLabelTownandVillagePanchayat.setVisibility(View.VISIBLE);
                                viewBottomTownandVillagePanchayat.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < villagePanchayatListResponse.getResults().size(); i++) {
                                villagePanchayatResultsItemArrayList.add(new VillagePanchayatResultsItem(
                                        villagePanchayatListResponse.getResults().get(i).getVillageName() != null ?
                                                villagePanchayatListResponse.getResults().get(i).getVillageName() : "",
                                        villagePanchayatListResponse.getResults().get(i).getId() != 0 ?
                                                villagePanchayatListResponse.getResults().get(i).getId() : 0));
                            }
                            VillagePanchayatAdapter vattamAdapter = new VillagePanchayatAdapter(villagePanchayatResultsItemArrayList, getActivity());
                            spinnerTownandVillagePanchayat.setAdapter(vattamAdapter);
                        } else {
                            Toast.makeText(getActivity(), villagePanchayatListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_village_panchayat), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<VillagePanchayatListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.no_it_village_panchayat), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getWardList(int divisionID, int partID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            wardList.clear();
            wardList.add(0, new WardListResultsItem(getString(R.string.select_ward), 0));
            final VattamWardList vattamWardListParam = new VattamWardList();
            vattamWardListParam.setDivisionID(divisionID);
            vattamWardListParam.setPartID(partID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getWardList(headerMap, vattamWardListParam).enqueue(new Callback<WardListResponse>() {
                @Override
                public void onResponse(Call<WardListResponse> call, Response<WardListResponse> response) {
                    hideProgress();
                    wardListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200
                            && wardListResponse != null) {
                        if (wardListResponse.getResults() != null) {
                            if (wardListResponse.getResults().size() > 0) {
                                spinnerWard.setVisibility(View.VISIBLE);
                                textLabelWard.setVisibility(View.VISIBLE);
                                viewBottomWard.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < wardListResponse.getResults().size(); i++) {
                                wardList.add(new WardListResultsItem(
                                        wardListResponse.getResults().get(i).getWardname() != null ?
                                                wardListResponse.getResults().get(i).getWardname() : "",
                                        wardListResponse.getResults().get(i).getId() != 0 ?
                                                wardListResponse.getResults().get(i).getId() : 0));
                            }
                            WardAdapter wardAdapter = new WardAdapter(wardList, getActivity());
                            spinnerWard.setAdapter(wardAdapter);
                        } else {
                            Toast.makeText(getActivity(), wardListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_ward), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<WardListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.no_it_ward), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    public void getBoothList(int divisionID, int partID, int wardID, int villageID, int unionType) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            boothList.clear();
            boothList.add(0, new BoothListResultsItem(getString(R.string.select_booth), 0));
            BoothListInputParam boothListInputParam = new BoothListInputParam();
            boothListInputParam.setDivisionID(divisionID);
            boothListInputParam.setPartID(partID);
            boothListInputParam.setWardID(wardID);
            boothListInputParam.setVillageID(villageID);
            boothListInputParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getBoothList(headerMap, boothListInputParam).enqueue(new Callback<BoothListResponse>() {
                @Override
                public void onResponse(Call<BoothListResponse> call, Response<BoothListResponse> response) {
                    hideProgress();
                    boothListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200
                            && boothListResponse != null) {
//                        if (boothListResponse.getResults().size() > 0) {
//                            spinnerBooth.setVisibility(View.VISIBLE);
//                            textLabelBooth.setVisibility(View.VISIBLE);
//                            viewBottom18.setVisibility(View.VISIBLE);
//                        }
                        if (boothListResponse.getResults() != null) {
                            for (int i = 0; i < boothListResponse.getResults().size(); i++) {
                                boothList.add(new BoothListResultsItem(
                                        boothListResponse.getResults().get(i).getBoothname() != null ?
                                                boothListResponse.getResults().get(i).getBoothname() : "",
                                        boothListResponse.getResults().get(i).getId() != 0 ?
                                                boothListResponse.getResults().get(i).getId() : 0));
                            }
                            BoothAdapter boothAdapter = new BoothAdapter(boothList, getActivity());
                            spinnerBooth.setAdapter(boothAdapter);
                        } else {
                            Toast.makeText(getActivity(), boothListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
//                        spinnerBooth.setVisibility(View.GONE);
//                        textLabelBooth.setVisibility(View.GONE);
//                        viewBottom18.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), getString(R.string.no_booth_list_found), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<BoothListResponse> call, Throwable t) {
                    hideProgress();
//                    spinnerBooth.setVisibility(View.GONE);
//                    textLabelBooth.setVisibility(View.GONE);
//                    viewBottom18.setVisibility(View.GONE);
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    private void uploadMedia() {
        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0 && selectedVideoAudioUri != null) {
//            both image and video
            mediaType = 1;
        } else if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0 && selectedVideoAudioUri == null) {
            //only image
            mediaType = 2;
        } else if (selectedImageAdapter == null && selectedVideoAudioUri != null) {
            //only video
            mediaType = 3;
        } else {
            //no media
            mediaType = 4;
        }

        if (mediaType == 1 || mediaType == 2) {
            showProgress(R.string.media_uploading);
            uploadImage();
        } else if (mediaType == 3) {
            showProgress(R.string.media_uploading);
            uploadVideoandAudio();
        } else {
            showProgress();
            addEvent();

        }
    }

    private void uploadImage() {
        for (int i = 0; i < selectedImageAdapter.getCount(); i++) {
            //        to send media name in s3 server
            destination = new File(getActivity().getExternalFilesDir(null),
                    selectedImageAdapter.getItem(i).getImageName());
            if (selectedImageAdapter.getItem(i).getSelectedImage() != null && getImageUri(getActivity(),
                    selectedImageAdapter.getItem(i).getSelectedImage()) != null) {
                createFile(getActivity(), getImageUri(getActivity(),
                        selectedImageAdapter.getItem(i).getSelectedImage()), destination);
                TransferObserver uploadObserver =
                        transferUtility.upload(getString(R.string.s3_bucket_events_path) +
                                selectedImageAdapter.getItem(i).getImageName(), destination);
                final int finalI = i;
                uploadObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (TransferState.COMPLETED == state) {
                            destination.delete();
                            if (finalI == selectedImageAdapter.getCount() - 1) {
                                if (mediaType == 1)
                                    uploadVideoandAudio();
                                else {
                                    addEvent();
                                }

                            }
                        } else if (TransferState.FAILED == state) {
                            destination.delete();
                            if (finalI == selectedImageAdapter.getCount() - 1) {
                                if (mediaType == 1)
                                    uploadVideoandAudio();
                                else {
                                    hideProgress();
                                    Toast.makeText(getActivity(),
                                            getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                        int percentDone = (int) percentDonef;


                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        ex.printStackTrace();
                        destination.delete();
                        if (finalI == selectedImageAdapter.getCount() - 1) {
                            if (mediaType == 1)
                                uploadVideoandAudio();
                            else {
                                hideProgress();
                                Toast.makeText(getActivity(),
                                        getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                            }


                        }
                    }

                });
            } else {
                if (mediaType == 1)
                    uploadVideoandAudio();
                else {
                    hideProgress();
                    Toast.makeText(getActivity(),
                            getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                }

            }
        }

    }


    private void uploadVideoandAudio() {
        //Audio and video file
        if (isAudioFromRecord) {
            //audio from record
            destination = new File(
                    Environment.getExternalStorageDirectory().getAbsolutePath().toString() +
                            "/Voice Recorder/RECORDING_"
                            + videoAudioFileNameInternal);
        } else {
            // audio from gallery
            destination = new File(getActivity().getExternalFilesDir(null), videoAudioFileNameInternal);
        }
        createFile(getActivity(), selectedVideoAudioUri, destination);
        TransferObserver uploadObserver =
                transferUtility.upload(getString(R.string.s3_bucket_events_path) +
                        videoAudioFileNameInternal, destination);
        final File finalDestination = destination;
        uploadObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    destination.delete();
                    addEvent();
                } else if (TransferState.FAILED == state) {
                    destination.delete();
                    hideProgress();
                    Toast.makeText(getActivity(),
                            getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;
            }

            @Override
            public void onError(int id, Exception ex) {
                ex.printStackTrace();
                destination.delete();
                hideProgress();
                Toast.makeText(getActivity(),
                        getString(R.string.media_upload_failed), Toast.LENGTH_SHORT).show();
            }

        });
    }


    private void addEvent() {
        AddEvents events = new AddEvents();
        events.setStartdate(eventStartDateFormat + " " + eventStartTimeFormat);
        events.setEndate(eventEndDateFormat + " " + eventEndTimeFormat);
        events.setStatus(1);
        events.setTitle(editTitle.getText().toString());
        events.setDescription(editTimeSub.getText().toString());
        events.setVenue(editvenue.getText().toString());
        events.setMediatype(2);
        events.setCreatedby(sharedPreferences.getInt(DmkConstants.USERID, 0));
        events.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0) {
            for (int i = 0; i < selectedImageAdapter.getCount(); i++) {
                selectedMediaListName.add(selectedImageAdapter.getItem(i).getImageName());
            }
        }
        if (selectedVideoAudioUri != null) {
            selectedMediaListName.add(videoAudioFileNameInternal);
            events.setVideothumb(videoThubmnail);
        } else {
            events.setVideothumb("0");
        }
        //        to send image name in dmk server
        events.setMediafiles(selectedMediaListName);


        switch (designationID) {
            case 1:
                //செயலாளர்
//                துணை செயலாளர்
//                மண்டல ஒருங்கிணைப்பா
                events.setDistrict(spinnerDistrict.isShown() ?
                        districtList.get(spinnerDistrict.getSelectedItemPosition()).getId() : 0);
                events.setPartydistrict(spinnerPartyDistrict.isShown() ?
                        partyDistrictResultsItems.get(spinnerPartyDistrict.getSelectedItemPosition()).getId() : 0);
                events.setConstituency(spinnerConstituency.isShown() ?
                        constituencyList.get(spinnerConstituency.getSelectedItemPosition()).getId() : 0);
                events.setDivisionId(spinnerDivision.isShown() ?
                        divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0);
                events.setWard(spinnerWard.isShown() ?
                        wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                events.setBoothid(spinnerBooth.isShown() ?
                        boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                switch (spinnerDivision.isShown() ?
                        divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0) {
                    case 1:
                        events.setPartid(spinnerPart.isShown() ?
                                partList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        break;
                    case 2:
                        events.setMunicipality(spinnerPart.isShown() ?
                                municipalityList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        break;
                    case 3:
                        events.setPanchayatunion(spinnerPart.isShown() ?
                                panchayatList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        if (unionType == 1) {
                            events.setTownship(spinnerTownandVillagePanchayat.isShown() ?
                                    townPanchayatList.get(spinnerTownandVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                        } else {
                            events.setVillagepanchayat(spinnerTownandVillagePanchayat.isShown() ?
                                    villagePanchayatResultsItemArrayList.get(spinnerTownandVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                        }
                        break;
                }
                break;
            case 2:
//
                //                மாவட்ட ஒருங்கிணைப்பாளர்  district
                events.setDistrict(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                events.setPartydistrict(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                events.setConstituency(spinnerConstituency.isShown() ?
                        constituencyList.get(spinnerConstituency.getSelectedItemPosition()).getId() : 0);
                events.setDivisionId(spinnerDivision.isShown() ?
                        divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0);
                events.setWard(spinnerWard.isShown() ?
                        wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                events.setBoothid(spinnerBooth.isShown() ?
                        boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                switch (spinnerDivision.isShown() ?
                        divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0) {
                    case 1:
                        events.setPartid(spinnerPart.isShown() ?
                                partList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        break;
                    case 2:
                        events.setMunicipality(spinnerPart.isShown() ?
                                municipalityList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        break;
                    case 3:
                        events.setPanchayatunion(spinnerPart.isShown() ?
                                panchayatList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        if (unionType == 1) {
                            events.setTownship(spinnerTownandVillagePanchayat.isShown() ?
                                    townPanchayatList.get(spinnerTownandVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                        } else {
                            events.setVillagepanchayat(spinnerTownandVillagePanchayat.isShown() ?
                                    villagePanchayatResultsItemArrayList.get(spinnerTownandVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                        }
                        break;
                }

                break;
            case 3:
//                தொகுதி ஒருங்கிணைப்பாளர்  constituency
                events.setDistrict(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                events.setConstituency(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                events.setPartydistrict(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                events.setDivisionId(spinnerDivision.isShown() ?
                        divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0);
                events.setWard(spinnerWard.isShown() ?
                        wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                events.setBoothid(spinnerBooth.isShown() ?
                        boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                switch (spinnerDivision.isShown() ?
                        divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0) {
                    case 1:
                        events.setPartid(spinnerPart.isShown() ?
                                partList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        break;
                    case 2:
                        events.setMunicipality(spinnerPart.isShown() ?
                                municipalityList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        break;
                    case 3:
                        events.setPanchayatunion(spinnerPart.isShown() ?
                                panchayatList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                        if (unionType == 1) {
                            events.setTownship(spinnerTownandVillagePanchayat.isShown() ?
                                    townPanchayatList.get(spinnerTownandVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                        } else {
                            events.setVillagepanchayat(spinnerTownandVillagePanchayat.isShown() ?
                                    villagePanchayatResultsItemArrayList.get(spinnerTownandVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                        }
                        break;
                }
                break;
            case 4:
            case 8:
            case 12:
                //Corporation
//                பகுதி ஒருங்கிணைப்பாளர்  part
                events.setDistrict(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                events.setConstituency(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                events.setPartydistrict(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                events.setDivisionId(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                events.setWard(spinnerWard.isShown() ?
                        wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                events.setBoothid(spinnerBooth.isShown() ?
                        boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                    case 1:
                        events.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                        break;
                    case 2:
                        events.setMunicipality(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                        break;
                    case 3:
                        events.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                        if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) == 1)
                            events.setTownship(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                        else
                            events.setVillagepanchayat(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));

                        break;
                }
                break;
            case 5:
            case 9:
            case 13:
//                வட்டம் ஒருங்கிணைப்பாளர்  vattam
//                Corporation
                events.setDistrict(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                events.setConstituency(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                events.setPartydistrict(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                events.setWard(sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0));
                events.setDivisionId(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));

                switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                    case 1:
                        events.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                        break;
                    case 2:
                        events.setMunicipality(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                        break;
                    case 3:
                        events.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                        if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) == 1)
                            events.setTownship(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                        else
                            events.setVillagepanchayat(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));
                        break;
                }

                events.setBoothid(spinnerBooth.isShown() ?
                        boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);


                break;
            case 7:
            case 11:
            case 15:
            case 19:
//                வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                //Corporation
                events.setDistrict(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                events.setConstituency(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                events.setPartydistrict(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                events.setDivisionId(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                events.setWard(sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0));

                switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                    case 1:
                        events.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                        break;
                    case 2:
                        events.setMunicipality(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                        break;
                    case 3:
                        events.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                        if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) == 1)
                            events.setTownship(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                        else
                            events.setVillagepanchayat(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));
                        break;
                }

                events.setBoothid(sharedPreferences.getInt(DmkConstants.USER_BOOTH_ID, 0));
                break;
            case 16:
                //PanchayatUnion
//                ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                events.setDistrict(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                events.setConstituency(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                events.setPartydistrict(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                events.setDivisionId(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                events.setWard(spinnerWard.isShown() ?
                        wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                events.setBoothid(spinnerBooth.isShown() ?
                        boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);

                switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                    case 1:
                        events.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                        break;
                    case 2:
                        events.setMunicipality(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                        break;
                    case 3:
                        events.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                        if (unionType == 1) {
                            events.setTownship(spinnerTownandVillagePanchayat.isShown() ?
                                    townPanchayatList.get(spinnerTownandVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                        } else {
                            events.setVillagepanchayat(spinnerTownandVillagePanchayat.isShown() ?
                                    villagePanchayatResultsItemArrayList.get(spinnerTownandVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                        }
                        break;
                }
                events.setBoothid(spinnerBooth.isShown() ?
                        boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);

                break;
            case 17:
//                ஊராட்சி ஒருங்கிணைப்பாளர் villagepanchayat
                //Panchayat Union
                events.setDistrict(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                events.setConstituency(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                events.setPartydistrict(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                events.setDivisionId(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                events.setWard(sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0));
                switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                    case 1:
                        events.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                        break;
                    case 2:
                        events.setMunicipality(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                        break;
                    case 3:
                        events.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                        if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) != 1)
                            events.setTownship(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                        else
                            events.setVillagepanchayat(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));
                        break;
                }

                events.setBoothid(spinnerBooth.isShown() ?
                        boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);

                break;
        }

        if (Util.isNetworkAvailable()) {
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.addEventList(headerMap, events).enqueue(new Callback<AddEventsResponse>() {
                @Override
                public void onResponse(Call<AddEventsResponse> call, Response<AddEventsResponse> response) {
                    addEventsResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && addEventsResponse != null) {
                        hideProgress();
                        if (addEventsResponse.getStatuscode() == 0) {
                            getActivity().onBackPressed();
                            Toast.makeText(getActivity(), getString(R.string.event_sucess), Toast.LENGTH_SHORT).show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, addEventsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, addEventsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(addEventsResponse.getSource(),
                                    addEventsResponse.getSourcedata()));
                            editor.commit();
                            showProgress();
                            addEvent();
                        }


                    } else {
                        Toast.makeText(getActivity(), getString(R.string.event_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddEventsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void profileNullCheck() {

        if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 0) {
            if (!editTitle.getText().toString().isEmpty() && editTitle.getText().toString() != null && editTitle.getText().toString().length() > 0 &&
                    editTitle.getText().toString().trim().length() <= DmkConstants.COMPOSE_TITLE_LIMIT &&
                    !editTimeSub.getText().toString().isEmpty() && editTimeSub.getText().toString() != null && editTimeSub.getText().toString().length() > 0 &&
                    !editStartDate.getText().toString().isEmpty() && editStartDate.getText().toString() != null && editStartDate.getText().toString().length() > 0 &&
                    !editEndDate.getText().toString().isEmpty() && editEndDate.getText().toString() != null && editEndDate.getText().toString().length() > 0
                    && spinnerConstituency.isShown() ? (spinnerConstituency.getSelectedItemPosition() != 0 ? true : false) : true &&
                    spinnerDivision.isShown() ? (spinnerDivision.getSelectedItemPosition() != 0 ? true : false) : true &&
                    spinnerPart.isShown() ? (spinnerPart.getSelectedItemPosition() != 0 ? true : false) : true &&
                    spinnerTownandVillagePanchayat.isShown() ? (spinnerTownandVillagePanchayat.getSelectedItemPosition() != 0 ? true : false) : true &&
                    spinnerBooth.isShown() ? (spinnerBooth.getSelectedItemPosition() != 0 ? true : false) : true &&
                    spinnerWard.isShown() ? (spinnerWard.getSelectedItemPosition() != 0 ? true : false) : true
                    && selectedImageAdapter.getCount() <= 3 ? true : false) {
                if (selectedVideoAudioUri != null) {

                    if (!getFileSize(selectedVideoAudioUri)) {
                        Toast.makeText(getContext(), getString(R.string.video_limit),
                                Toast.LENGTH_SHORT).show();
                    } else {
                        uploadMedia();
                    }
                } else {
                    uploadMedia();
                }

            } else {
                if (editTitle.getText().toString().isEmpty() || editTitle.getText().toString() == null || editTitle.getText().toString().length() < 0) {
                    editTitle.setError(getString(R.string.title_missing));
                }
                if (editTimeSub.getText().toString().isEmpty() || editTimeSub.getText().toString() == null || editTimeSub.getText().toString().length() < 0) {
                    editTimeSub.setError(getString(R.string.write_some_thing_missing));
                }
                if (editStartDate.getText().toString().isEmpty() || editStartDate.getText().toString() == null || editStartDate.getText().toString().length() < 0) {
                    editStartDate.setError(getString(R.string.Start_date_missing));
                }
                if (editEndDate.getText().toString().isEmpty() || editEndDate.getText().toString() == null || editEndDate.getText().toString().length() < 0) {
                    editEndDate.setError(getString(R.string.End_date_missing));
                }
                if (spinnerDivision.isShown()) {
                    if (spinnerDivision.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerDivision.getSelectedView()).setError(getString(R.string.select_division));
                    }
                }
                if (editTitle.getText().toString().trim().length() > DmkConstants.COMPOSE_TITLE_LIMIT) {
                    editTitle.setError(getString(R.string.title_limit));
                }
                if (spinnerConstituency.isShown()) {
                    if (spinnerConstituency.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerConstituency.getSelectedView()).setError(getString(R.string.select_constituency));
                    }
                }
                if (spinnerTownandVillagePanchayat.isShown()) {
                    if (spinnerTownandVillagePanchayat.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerTownandVillagePanchayat.getSelectedView()).setError(getString(R.string.select_village_panchayat));
                    }
                }
                if (spinnerWard.isShown()) {
                    if (spinnerWard.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerWard.getSelectedView()).setError(getString(R.string.select_ward));
                    }
                }
                if (spinnerPart.isShown()) {
                    if (spinnerPart.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerPart.getSelectedView()).setError(getString(R.string.part));
                    }
                }
                if (spinnerBooth.isShown()) {
                    if (spinnerBooth.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerBooth.getSelectedView()).setError(getString(R.string.select_booth));
                    }
                }
                if (selectedImageAdapter != null && selectedImageAdapter.getCount() > 3)
                    Toast.makeText(getActivity(), getString(R.string.image_limit),
                            Toast.LENGTH_SHORT).show();
            }

        } else {

            if (!editTitle.getText().toString().isEmpty() && editTitle.getText().toString() != null && editTitle.getText().toString().length() > 0 &&
                    editTitle.getText().toString().trim().length() <= DmkConstants.COMPOSE_TITLE_LIMIT &&
                    !editTimeSub.getText().toString().isEmpty() && editTimeSub.getText().toString() != null && editTimeSub.getText().toString().length() > 0 &&
                    !editStartDate.getText().toString().isEmpty() && editStartDate.getText().toString() != null && editStartDate.getText().toString().length() > 0 &&
                    !editEndDate.getText().toString().isEmpty() && editEndDate.getText().toString() != null && editEndDate.getText().toString().length() > 0
                    && spinnerConstituency.isShown() ? (spinnerConstituency.getSelectedItemPosition() != 0 ? true : false) : true &&
                    spinnerDivision.isShown() ? (spinnerDivision.getSelectedItemPosition() != 0 ? true : false) : true &&
                    spinnerPart.isShown() ? (spinnerPart.getSelectedItemPosition() != 0 ? true : false) : true &&
                    spinnerTownandVillagePanchayat.isShown() ? (spinnerTownandVillagePanchayat.getSelectedItemPosition() != 0 ? true : false) : true &&
                    spinnerBooth.isShown() ? (spinnerBooth.getSelectedItemPosition() != 0 ? true : false) : true &&
                    spinnerWard.isShown() ? (spinnerWard.getSelectedItemPosition() != 0 ? true : false) : true) {
                if (selectedVideoAudioUri != null) {
                    if (!getFileSize(selectedVideoAudioUri)) {
                        Toast.makeText(getContext(), getString(R.string.video_limit),
                                Toast.LENGTH_SHORT).show();
                    } else {
                        uploadMedia();
                    }
                } else {
                    uploadMedia();
                }
            } else {
                if (editTitle.getText().toString().isEmpty() || editTitle.getText().toString() == null || editTitle.getText().toString().length() < 0) {
                    editTitle.setError(getString(R.string.title_missing));
                }
                if (editTimeSub.getText().toString().isEmpty() || editTimeSub.getText().toString() == null || editTimeSub.getText().toString().length() < 0) {
                    editTimeSub.setError(getString(R.string.write_some_thing_missing));
                }
                if (editStartDate.getText().toString().isEmpty() || editStartDate.getText().toString() == null || editStartDate.getText().toString().length() < 0) {
                    editStartDate.setError(getString(R.string.Start_date_missing));
                }
                if (editEndDate.getText().toString().isEmpty() || editEndDate.getText().toString() == null || editEndDate.getText().toString().length() < 0) {
                    editEndDate.setError(getString(R.string.End_date_missing));
                }
                if (editTitle.getText().toString().trim().length() > DmkConstants.COMPOSE_TITLE_LIMIT) {
                    editTitle.setError(getString(R.string.title_limit));
                }
                if (spinnerDivision.isShown()) {
                    if (spinnerDivision.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerDivision.getSelectedView()).setError(getString(R.string.select_division));
                    }
                }
                if (spinnerConstituency.isShown()) {
                    if (spinnerConstituency.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerConstituency.getSelectedView()).setError(getString(R.string.select_constituency));
                    }
                }

                if (spinnerTownandVillagePanchayat.isShown()) {
                    if (spinnerTownandVillagePanchayat.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerTownandVillagePanchayat.getSelectedView()).setError(getString(R.string.select_village_panchayat));
                    }
                }
                if (spinnerWard.isShown()) {
                    if (spinnerWard.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerWard.getSelectedView()).setError(getString(R.string.select_ward));
                    }
                }
                if (spinnerPart.isShown()) {
                    if (spinnerPart.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerPart.getSelectedView()).setError(getString(R.string.part));
                    }
                }
                if (spinnerBooth.isShown()) {
                    if (spinnerBooth.getSelectedItemPosition() == 0) {
                        ((TextView) spinnerBooth.getSelectedView()).setError(getString(R.string.select_booth));
                    }
                }

            }

        }

    }

}
