package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.FeedbackCategoryAdpater;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.CompliantComposeFragment;
import com.dci.dmkitwings.fragment.EscalatedComplaintfragment;
import com.dci.dmkitwings.fragment.MycompliantsDetailsFragment;
import com.dci.dmkitwings.model.ComposeCompliantParams;
import com.dci.dmkitwings.model.FeedbackCategoryListResponse;
import com.dci.dmkitwings.model.FeedbackCategoryResultsItem;
import com.dci.dmkitwings.model.FeedbackCategorylistparams;
import com.dci.dmkitwings.model.FeedbackComposeParam;
import com.dci.dmkitwings.model.FeedbackPostResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompliantComposeActivity extends BaseActivity
{
    @BindView(R.id.frame_compliant_container)
    FrameLayout frameCompliantContainer;
    @BindView(R.id.cons_composecom)
    ConstraintLayout consFeedback;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Bundle intent;
    int from;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_compliantcompose, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        frameLayoutNotification.setVisibility(View.GONE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        intent=getIntent().getExtras();
        if (intent!=null)
        {
            from = intent.getInt("from");
            if (from==0)
            {
                push(new CompliantComposeFragment());
            }
            else if (from==1)
            {
                MycompliantsDetailsFragment mycompliantsDetailsFragment=new MycompliantsDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("status", intent.getString("status"));
                bundle.putString("feedback", intent.getString("feedback"));
                bundle.putString("categoryid",intent.getString("categoryid"));
                mycompliantsDetailsFragment.setArguments(bundle);
                push(mycompliantsDetailsFragment);
            }
            else {

                EscalatedComplaintfragment escalatedComplaintfragment=new EscalatedComplaintfragment();
                Bundle bundle = new Bundle();
                bundle.putInt("id", intent.getInt("id"));
                bundle.putString("status", intent.getString("status"));
                bundle.putString("feedback", intent.getString("feedback"));
                bundle.putString("SubjectID",intent.getString("SubjectID"));
                bundle.putInt("Categoryid",intent.getInt("Categoryid"));
                bundle.putString("Escalated",intent.getString("Escalated"));
                bundle.putString("Replied",intent.getString("Replied"));
                bundle.putString("FirstName",intent.getString("FirstName"));
                bundle.putString("LastName",intent.getString("LastName"));
                bundle.putString("Designation",intent.getString("Designation"));
                bundle.putString("avatar",intent.getString("avatar"));
                escalatedComplaintfragment.setArguments(bundle);
                push(escalatedComplaintfragment);


            }



        }







    }



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    public void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void popAllBackstack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();
        if (backCount > 0) {
            fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_compliant_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_compliant_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_compliant_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_compliant_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }
    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }

    public void pushWithAnimation(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        pushWithAnimation(fragment, null);
    }

    public void pushWithAnimation(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_compliant_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_compliant_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager
                        .beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_compliant_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_compliant_container, fragment, tag).commitAllowingStateLoss();
            }
        }
    }

}
