package com.dci.dmkitwings.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.HomeViewPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by vijayaganesh on 4/2/2018.
 */

public class RegistrationFragment extends BaseFragment {

    @BindView(R.id.text_title_login)
    TextView textTitleLogin;
    @BindView(R.id.cons_login_tool_bar_elements)
    ConstraintLayout consLoginToolBarElements;
    @BindView(R.id.tool_login)
    Toolbar toolLogin;
    @BindView(R.id.view_top)
    View viewTop;
    @BindView(R.id.tabs_login)
    TabLayout tabsLogin;
    @BindView(R.id.viewpager_login)
    ViewPager viewpagerLogin;
    @BindView(R.id.cons_reg_form)
    ConstraintLayout consRegForm;
    @BindView(R.id.cons_frag_reg)
    ConstraintLayout consFragReg;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        unbinder = ButterKnife.bind(this, view);
        HomeViewPagerAdapter homeViewPagerAdapter = new HomeViewPagerAdapter(getActivity().getSupportFragmentManager());
        homeViewPagerAdapter.addFrag(new PartyMemberRegFragment(), getResources().getString(R.string.party_member));
        homeViewPagerAdapter.addFrag(new CommonUserRegFragment(), getResources().getString(R.string.public_User));
        viewpagerLogin.setAdapter(homeViewPagerAdapter);
        tabsLogin.setupWithViewPager(viewpagerLogin, true);

        setupTabIcons();
        LinearLayout tabStrip = ((LinearLayout)tabsLogin.getChildAt(0));
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }

        viewpagerLogin.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });
        //Disabled pubilc user registration
//        LinearLayout tabStrip = ((LinearLayout)tabsLogin.getChildAt(1));
//        for(int i = 0; i < tabStrip.getChildCount(); i++) {
//            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    return true;
//                }
//            });
//        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setupTabIcons() {
        TextView textHome = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_text, null);
        textHome.setText(getResources().getString(R.string.party_member));
        textHome.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_pary_member, 0, 0, 0);
        tabsLogin.getTabAt(0).setCustomView(textHome);
        TextView textNews = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_text, null);
        textNews.setText(getResources().getString(R.string.public_User));
        textNews.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_public, 0, 0, 0);
        tabsLogin.getTabAt(1).setCustomView(textNews);

    }
}
