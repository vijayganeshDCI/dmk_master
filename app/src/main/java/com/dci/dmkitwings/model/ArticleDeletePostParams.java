package com.dci.dmkitwings.model;

public class ArticleDeletePostParams
{
    private int userid;
    private int articlesid;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getArticlesid() {
        return articlesid;
    }

    public void setArticlesid(int articlesid) {
        this.articlesid = articlesid;
    }
}
