package com.dci.dmkitwings.model;

import java.util.List;

public class TimeLineDetailsResults {
    private List<TimeLineDetailCommentsItem> comments;
    private String updated_at;

    public List<String> getMediapath() {
        return mediapath;
    }

    public void setMediapath(List<String> mediapath) {
        this.mediapath = mediapath;
    }

    private List<String> mediapath;
    private String created_at;
    private int id;
    private int userlikedstatus;
    private String title;
    private int mediatype;
    private TimeLineDetailUser user;
    private String content;
    private int status;

    public int getSharecount() {
        return Sharecount;
    }

    public void setSharecount(int sharecount) {
        Sharecount = sharecount;
    }

    private int Sharecount;

    public int getViewcount() {
        return Viewcount;
    }

    public void setViewcount(int viewcount) {
        Viewcount = viewcount;
    }

    private int Viewcount;

    public List<TimeLineDetailCommentsItem> getComments() {
        return comments;
    }

    public void setComments(List<TimeLineDetailCommentsItem> comments) {
        this.comments = comments;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getVideothumb() {
        return Videothumb;
    }

    public void setVideothumb(String videothumb) {
        Videothumb = videothumb;
    }

    private String Videothumb;

//	public String getMediapath() {
//		return mediapath;
//	}
//
//	public void setMediapath(String mediapath) {
//		this.mediapath = mediapath;
//	}

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserlikedstatus() {
        return userlikedstatus;
    }

    public void setUserlikedstatus(int userlikedstatus) {
        this.userlikedstatus = userlikedstatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMediatype() {
        return mediatype;
    }

    public void setMediatype(int mediatype) {
        this.mediatype = mediatype;
    }

    public TimeLineDetailUser getUser() {
        return user;
    }

    public void setUser(TimeLineDetailUser user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}