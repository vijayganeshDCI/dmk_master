package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.ElectionQuestionResultsItem;
import com.dci.dmkitwings.model.PollSaveParams;
import com.dci.dmkitwings.model.QuestionslistItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 4/16/2018.
 */

public class ElectionQuestionListAdapter extends BaseAdapter {


    public ElectionQuestionListAdapter(List<ElectionQuestionResultsItem> electionQuestionResultsItemList , Context context) {
        this.electionQuestionResultsItemList = electionQuestionResultsItemList;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    List<ElectionQuestionResultsItem> electionQuestionResultsItemList;
    Context context;
    LayoutInflater mInflater;
    PollSaveParams pollSaveParams;
    public HashMap<Integer,String> questions = new HashMap<>();
    public HashMap<Integer,ArrayList<String>> TEST = new HashMap<>();
    public ArrayList<String> ok=new ArrayList<>();
    public Map<Integer, List<String>> alternateMap = new HashMap<>();
    @Override
    public int getCount() {
        return electionQuestionResultsItemList.size();
    }

    @Override
    public ElectionQuestionResultsItem getItem(int position) {

        return electionQuestionResultsItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_election_question, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (electionQuestionResultsItemList.get(position).getOption1() != null && (!TextUtils.equals(electionQuestionResultsItemList.get(position).getOption1() ,"null")) && (!TextUtils.isEmpty(electionQuestionResultsItemList.get(position).getOption1()))){
            viewHolder.radioButtonOptionone.setVisibility(View.VISIBLE);
            viewHolder.radioButtonOptionone.setText(electionQuestionResultsItemList.get(position).getOption1());



        }
        else
        {
            viewHolder.radioButtonOptionone.setVisibility(View.GONE);
        }
        if (electionQuestionResultsItemList.get(position).getOption2() != null && (!TextUtils.equals(electionQuestionResultsItemList.get(position).getOption2() ,"null")) && (!TextUtils.isEmpty(electionQuestionResultsItemList.get(position).getOption2()))){
            viewHolder.radioButtonOptiontwo.setVisibility(View.VISIBLE);
            viewHolder.radioButtonOptiontwo.setText(electionQuestionResultsItemList.get(position).getOption2());


        }
        else
        {
            viewHolder.radioButtonOptiontwo.setVisibility(View.GONE);
        }
        if (electionQuestionResultsItemList.get(position).getOption3() != null && (!TextUtils.equals(electionQuestionResultsItemList.get(position).getOption3() ,"null")) && (!TextUtils.isEmpty(electionQuestionResultsItemList.get(position).getOption3()))){
            viewHolder.radioButtonOptionthree.setVisibility(View.VISIBLE);
            viewHolder.radioButtonOptionthree.setText(electionQuestionResultsItemList.get(position).getOption3());
            }
        else
        {
            viewHolder.radioButtonOptionthree.setVisibility(View.GONE);
        }

        if (electionQuestionResultsItemList.get(position).getOption4()!= null && (!TextUtils.equals(electionQuestionResultsItemList.get(position).getOption4() ,"null")) && (!TextUtils.isEmpty(electionQuestionResultsItemList.get(position).getOption4())))
        {
            viewHolder.radioButtonoptionFour.setVisibility(View.VISIBLE);
            viewHolder.radioButtonoptionFour.setText(electionQuestionResultsItemList.get(position).getOption4());
        }

        else
        {
            viewHolder.radioButtonoptionFour.setVisibility(View.GONE);
        }


        viewHolder.radioButtonOptionone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdatePollAnswers(electionQuestionResultsItemList.get(position).getId(),"Option1");


            }
        });
        viewHolder.radioButtonOptiontwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdatePollAnswers(electionQuestionResultsItemList.get(position).getId(),"Option2");


            }
        });
        viewHolder.radioButtonOptionthree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdatePollAnswers(electionQuestionResultsItemList.get(position).getId(),"Option3");


            }
        });
        viewHolder.radioButtonoptionFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdatePollAnswers(electionQuestionResultsItemList.get(position).getId(),"Option4");


            }
        });
        viewHolder.textQuestion.setText(electionQuestionResultsItemList.get(position).getQuestion());


        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.text_question)
        TextView textQuestion;
        @BindView(R.id.radio_button_optionone)
        RadioButton radioButtonOptionone;
        @BindView(R.id.radio_button_optiontwo)
        RadioButton radioButtonOptiontwo;
        @BindView(R.id.radio_button_optionthree)
        RadioButton radioButtonOptionthree;
        @BindView(R.id.radio_button_optionfour)
        RadioButton radioButtonoptionFour;
        @BindView(R.id.radio_group_answer)
        RadioGroup radioGroupAnswer;
//        @BindView(R.id.cons_item)
//        ConstraintLayout consItem;
        //        @BindView(R.id.card_polls)
//        CardView cardPolls;
        @BindView(R.id.cons_item_polls)
        ConstraintLayout consItemPolls;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void UpdatePollAnswers(int questionid,String answer)
    {


        if(questions.containsKey(questionid))
        {
            questions.remove(questionid);
            TEST.remove(questionid);
            questions.put(questionid,answer);

            if (ok.contains(answer))
            {
                ok.remove(answer);
                ok.add(answer);
                TEST.put(questionid,ok);
                alternateMap.put(questionid,ok);


            }
            else
            {
                ok.add(answer);
                TEST.put(questionid,ok);
                alternateMap.put(questionid,ok);
            }


        }
        else
        {
            questions.put(questionid,answer);
            ok.add(answer);
            TEST.put(questionid,ok);
            alternateMap.put(questionid,ok);
        }
    }

}
