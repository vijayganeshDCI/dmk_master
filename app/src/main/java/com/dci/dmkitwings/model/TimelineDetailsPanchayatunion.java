package com.dci.dmkitwings.model;

public class TimelineDetailsPanchayatunion
{
    private int id;
    private String VillagePanchayatName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVillagePanchayatName() {
        return VillagePanchayatName;
    }

    public void setVillagePanchayatName(String villagePanchayatName) {
        VillagePanchayatName = villagePanchayatName;
    }
}
