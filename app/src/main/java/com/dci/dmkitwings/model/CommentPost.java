package com.dci.dmkitwings.model;

public class CommentPost {

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getTimelineid() {
        return timelineid;
    }

    public void setTimelineid(int timelineid) {
        this.timelineid = timelineid;
    }

    public int getParentid() {
        return parentid;
    }

    public void setParentid(int parentid) {
        this.parentid = parentid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    private int userid;
    private int timelineid;
    private int parentid;
    private String comment;

}
