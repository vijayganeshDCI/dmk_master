package com.dci.dmkitwings.model;

public class TimelineDetailsWard
{
    private int id;
    private String wardname;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWardname() {
        return wardname;
    }

    public void setWardname(String wardname) {
        this.wardname = wardname;
    }
}
