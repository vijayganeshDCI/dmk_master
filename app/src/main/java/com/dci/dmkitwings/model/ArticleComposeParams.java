package com.dci.dmkitwings.model;

import java.util.List;

public class ArticleComposeParams {
    private int userid;
    private String title;
    private String content;
    private int mediatype;
    private int newstype;
    private String constituencyid;
    private String districtid;
    private List<String> MediaFiles;


    public List<String> getMediaFiles() {
        return MediaFiles;
    }

    public void setMediaFiles(List<String> mediaFiles) {
        MediaFiles = mediaFiles;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getMediatype() {
        return mediatype;
    }

    public void setMediatype(int mediatype) {
        this.mediatype = mediatype;
    }

    public int getNewstype() {
        return newstype;
    }

    public void setNewstype(int newstype) {
        this.newstype = newstype;
    }

    public String getConstituencyid() {
        return constituencyid;
    }

    public void setConstituencyid(String constituencyid) {
        this.constituencyid = constituencyid;
    }

    public String getDistrictid() {
        return districtid;
    }

    public void setDistrictid(String districtid) {
        this.districtid = districtid;
    }
}
