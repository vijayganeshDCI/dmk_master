package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.FeedBackInputParam;
import com.dci.dmkitwings.model.MyComplaintResultsItem;

import java.util.List;

/**
 * Created by vijayaganesh on 4/9/2018.
 */

public class MyCompliantsAdpater extends BaseAdapter {
    public MyCompliantsAdpater(List<MyComplaintResultsItem> compliantList, Context context) {
        this.compliantList = compliantList;
        this.context = context;

    }


    List<MyComplaintResultsItem> compliantList;
    Context context;

    @Override
    public int getCount() {
        return compliantList.size();
    }

    @Override
    public MyComplaintResultsItem getItem(int position) {
        return compliantList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_compliants, null);
        }

        TextView textFeedbackTitle = (TextView) convertView.findViewById(R.id.text_subtitle);
        TextView textFeedbackDescription = (TextView) convertView.findViewById(R.id.text_compl_Content);
        TextView text_status = (TextView) convertView.findViewById(R.id.text_status);
        TextView textFeedstatus = (TextView) convertView.findViewById(R.id.text_comstatus);
        View view = (View) convertView.findViewById(R.id.view_left);
        textFeedbackTitle.setText(compliantList.get(position).getSubjectID());
        textFeedbackDescription.setText(compliantList.get(position).getContent());
        //textFeedbackDate.setText(compliantList.get(position).getCreated_at());
        textFeedbackDescription.setVisibility(View.GONE);
        text_status.setText(context.getString(R.string.Status));
        text_status.setTextColor(Color.GRAY);
        if (compliantList.get(position).getStatus().equals("Resolved")) {
            textFeedstatus.setText(" " + context.getString(R.string.Resolved));
            textFeedstatus.setTextColor(context.getResources().getColor(R.color.accept_green));
            textFeedstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_resolved_32_green, 0);
        } else {
            textFeedstatus.setText(" " + context.getString(R.string.NotResolved));
            textFeedstatus.setTextColor(Color.RED);
            textFeedstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_not_resolved_32_red, 0);
        }

        return convertView;
    }


}
