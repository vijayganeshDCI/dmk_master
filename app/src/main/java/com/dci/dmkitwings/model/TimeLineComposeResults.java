package com.dci.dmkitwings.model;

public class TimeLineComposeResults {
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public int getLikescount() {
		return likescount;
	}

	public void setLikescount(int likescount) {
		this.likescount = likescount;
	}

	public String getPostedUserID() {
		return PostedUserID;
	}

	public void setPostedUserID(String postedUserID) {
		PostedUserID = postedUserID;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getPostAdminId() {
		return PostAdminId;
	}

	public void setPostAdminId(int postAdminId) {
		PostAdminId = postAdminId;
	}

	public String getMediaFilesPath() {
		return MediaFilesPath;
	}

	public void setMediaFilesPath(String mediaFilesPath) {
		MediaFilesPath = mediaFilesPath;
	}

	public String getMediatype() {
		return Mediatype;
	}

	public void setMediatype(String mediatype) {
		Mediatype = mediatype;
	}

	public int getCommentscount() {
		return Commentscount;
	}

	public void setCommentscount(int commentscount) {
		Commentscount = commentscount;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private int Status;
	private int DistrictID;
	private int likescount;
	private String PostedUserID;
	private String Title;
	private String created_at;
	private int PostAdminId;
	private String MediaFilesPath;
	private String Mediatype;
	private int Commentscount;
	private String updated_at;
	private String Content;
	private int id;
}
