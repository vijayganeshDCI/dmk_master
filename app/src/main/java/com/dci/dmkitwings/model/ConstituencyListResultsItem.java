package com.dci.dmkitwings.model;

public class ConstituencyListResultsItem {
	public ConstituencyListResultsItem(String constituencyName, int id) {
		ConstituencyName = constituencyName;
		this.id = id;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}



	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public String getConstituencyName() {
		return ConstituencyName;
	}

	public void setConstituencyName(String constituencyName) {
		ConstituencyName = constituencyName;
	}

	public String getConstituencyDescription() {
		return ConstituencyDescription;
	}

	public void setConstituencyDescription(String constituencyDescription) {
		ConstituencyDescription = constituencyDescription;
	}



	public int getPartyDistrictID() {
		return PartyDistrictID;
	}

	public void setPartyDistrictID(int partyDistrictID) {
		PartyDistrictID = partyDistrictID;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private int Status;

	public void setConstituencyCode(String constituencyCode) {
		ConstituencyCode = constituencyCode;
	}

	public void setConstituencyType(String constituencyType) {
		ConstituencyType = constituencyType;
	}

	private String ConstituencyCode;
	private String ConstituencyType;
	private String updated_at;
	private int DistrictID;
	private String ConstituencyName;
	private String ConstituencyDescription;
	private int PartyDistrictID;
	private String created_at;
	private int id;
}
