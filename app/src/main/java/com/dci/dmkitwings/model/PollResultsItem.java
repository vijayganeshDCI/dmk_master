package com.dci.dmkitwings.model;

import java.util.List;

public class PollResultsItem {
	private int Status;
	private String Description;
	private int CreatedBy;
	private int DistrictID;
	private int partydistrict;
	private List<QuestionslistItem> questionslist;
	private String created_at;
	private String EndDate;
	private String Name;
	private String StartDate;
	private int UpdatedBy;
	private String updated_at;
	private int id;
	private int ConstituencyID;
	private int Polltype;
	private List<resultfinderitem> resultfinder;

	public List<resultfinderitem> getResultfinder() {
		return resultfinder;
	}

	public void setResultfinder(List<resultfinderitem> resultfinder) {
		this.resultfinder = resultfinder;
	}
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public int getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(int createdBy) {
		CreatedBy = createdBy;
	}

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public int getPartydistrict() {
		return partydistrict;
	}

	public void setPartydistrict(int partydistrict) {
		this.partydistrict = partydistrict;
	}

	public List<QuestionslistItem> getQuestionslist() {
		return questionslist;
	}

	public void setQuestionslist(List<QuestionslistItem> questionslist) {
		this.questionslist = questionslist;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public int getUpdatedBy() {
		return UpdatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		UpdatedBy = updatedBy;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getConstituencyID() {
		return ConstituencyID;
	}

	public void setConstituencyID(int constituencyID) {
		ConstituencyID = constituencyID;
	}

	public int getPolltype() {
		return Polltype;
	}

	public void setPolltype(int polltype) {
		Polltype = polltype;
	}
}