package com.dci.dmkitwings.adapter;

import java.util.List;

public class PanchayatUnionListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<PanchayatUnionResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<PanchayatUnionResultsItem> results) {
		Results = results;
	}

	private String Status;
	private List<PanchayatUnionResultsItem> Results;

	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}