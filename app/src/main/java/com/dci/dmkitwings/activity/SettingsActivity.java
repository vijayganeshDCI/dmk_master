package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.fragment.HirerachyFragment;
import com.dci.dmkitwings.fragment.SettingsFragment;
import com.dci.dmkitwings.utils.LanguageHelper;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 4/13/2018.
 */

public class SettingsActivity extends BaseActivity {
    @BindView(R.id.frame_settings)
    FrameLayout frameSettings;
    @BindView(R.id.cons_activity)
    ConstraintLayout consActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_settings, null, false);
        ButterKnife.bind(this, contentView);
        mDrawerLayout.addView(contentView, 0);
        textTitle.setText(R.string.settings);
        frameLayoutNotification.setVisibility(View.GONE);
        push(new SettingsFragment());

        //setLocale("ta");
    }



    public void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void popAllBackstack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();
        if (backCount > 0) {
            fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_settings, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_settings, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_settings, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_settings, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }

    public void pushWithAnimation(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        pushWithAnimation(fragment, null);
    }

    public void pushWithAnimation(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_settings, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_settings, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager
                        .beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_settings, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                        .replace(R.id.frame_settings, fragment, tag).commitAllowingStateLoss();
            }
        }
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

}
