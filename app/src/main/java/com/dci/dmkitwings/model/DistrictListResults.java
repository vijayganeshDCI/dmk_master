package com.dci.dmkitwings.model;

import java.util.List;

public class DistrictListResults {
	private List<DistrictsItem> Districts;

	public List<DistrictsItem> getDistricts() {
		return Districts;
	}

	public void setDistricts(List<DistrictsItem> districts) {
		Districts = districts;
	}
}