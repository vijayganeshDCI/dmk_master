package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.MunicipalityListResultsItem;
import com.dci.dmkitwings.model.VattamResultsItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MunicipalityAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;

    public MunicipalityAdapter(List<MunicipalityListResultsItem> vattamResultsItemList, Context context) {
        this.vattamResultsItemList = vattamResultsItemList;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    List<MunicipalityListResultsItem> vattamResultsItemList;
    Context context;


    @Override
    public int getCount() {
        return vattamResultsItemList.size();
    }

    @Override
    public MunicipalityListResultsItem getItem(int position) {
        return vattamResultsItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VattamAdapter.ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_spinner_item, null);
            viewHolder = new VattamAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (VattamAdapter.ViewHolder) convertView.getTag();
        }
        viewHolder.textSpinnerItemOne.setText(vattamResultsItemList.get(position).getMunicipalityname());
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.text_spinner_item_one)
        TextView textSpinnerItemOne;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
