package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.budiyev.android.circularprogressbar.CircularProgressBar;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ElectionSurveyFilterActivity;
import com.dci.dmkitwings.adapter.ElectionListAdpater;
import com.dci.dmkitwings.adapter.ElectionOverAllListAdpater;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.ElectionListOverAllResponse;
import com.dci.dmkitwings.model.ElectionUserListParams;
import com.dci.dmkitwings.model.ElectionUserListResponse;
import com.dci.dmkitwings.model.ElectionUserlistItem;
import com.dci.dmkitwings.model.GetSurveyUserListParam;
import com.dci.dmkitwings.model.UserslistItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.dci.dmkitwings.view.CustomTextViewBold;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 3/16/2018.
 */

public class ElectionFragment extends BaseFragment {

    //    @BindView(R.id.card_election)
//    CardView cardView;
    @BindView(R.id.recycler_election)
    RecyclerView recyclerElection;
    Unbinder unbinder;
    @BindView(R.id.election_progress_bar)
    CircularProgressBar electionProgressBar;
    @BindView(R.id.text_completed)
    CustomTextViewBold textCompleted;
    @BindView(R.id.text_remaing)
    CustomTextViewBold textRemaing;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.cons_layout_elections)
    ConstraintLayout consLayoutElections;
    @BindView(R.id.co_home)
    CoordinatorLayout coHome;
    ElectionListAdpater electionListAdpater;
    ElectionOverAllListAdpater electionOverAllListAdpater;
    @BindView(R.id.image_no_elections)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_elections)
    CustomTextView textNoNetwork;
    @BindView(R.id.float_survey_filter)
    FloatingActionButton floatSurveyFilter;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static ArrayList<ElectionUserlistItem> electionuserListArray;
    ArrayList<UserslistItem> eleUserslistItems;
    ElectionUserListResponse electionUserListResponse;
    ElectionListOverAllResponse electionListOverAllResponse;
    int completed;
    int pending;
    int[] Surveryuser = new int[]{7, 11, 15, 19};
    private int districtID = 0;
    private int divisionID = 0;
    private int constituencyID = 0;
    private int partID = 0;
    private int villageID = 0;
    private int wardID = 0;
    private int boothID = 0;
    @BindView(R.id.electionsnestedscrrol)
    NestedScrollView nestedScrollView;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = false;
    boolean surveryuserboolean = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_election, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        electionuserListArray = new ArrayList<ElectionUserlistItem>();
        eleUserslistItems = new ArrayList<>();
        recyclerElection.setLayoutManager(new LinearLayoutManager(getActivity()));
        floatSurveyFilter.setVisibility(View.GONE);
        collapsingToolbar.setVisibility(View.VISIBLE);
        textCompleted.setText("0" + "-" + getString(R.string.completed));
        textRemaing.setText("0" + "-" + getString(R.string.remaining));
        electionProgressBar.setProgress(0);
        floatSurveyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ElectionSurveyFilterActivity.class);
                startActivity(intent);
            }
        });

        nestedScrollView.setSmoothScrollingEnabled(true);
        if (nestedScrollView != null) {
            nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (v.getChildAt(v.getChildCount() - 1) != null) {
                        if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) && scrollY > oldScrollY) {

                            if (!isLoading) {
                                if (!lastEnd) {
                                    if (electionuserListArray.size() >= 18) {
                                        currentPage = currentPage + 21;
                                        if (surveryuserboolean) {
                                            getElectionUserList();
                                        } else {
                                            getElectionSurveyVoterList();
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

            });

        }
        return view;
    }

    public void getElectionUserList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading = true;
            if (currentPage == 0) {
                electionuserListArray.clear();
            }

            final ElectionUserListParams electionUserListParams = new ElectionUserListParams();
            electionUserListParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            electionUserListParams.setLimit(20);
            electionUserListParams.setOffset(currentPage);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getElectionUserList(headerMap, electionUserListParams).enqueue(new Callback<ElectionUserListResponse>() {
                @Override
                public void onResponse(Call<ElectionUserListResponse> call, Response<ElectionUserListResponse> response) {
                    isLoading = false;
                    electionUserListResponse = response.body();
                    hideProgress();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (electionUserListResponse.getStatuscode() == 0) {

                            completed = electionUserListResponse.getResults().getCompleted();
                            pending = electionUserListResponse.getResults().getPending();
                            if (electionUserListResponse.getResults().getUserlist() != null &&
                                    electionUserListResponse.getResults().getUserlist().size() > 0) {
                                if (isAdded()) {
                                    imageNoNetwork.setVisibility(View.GONE);
                                    textNoNetwork.setVisibility(View.GONE);
                                    recyclerElection.setVisibility(View.VISIBLE);
                                }
                                for (int u = 0; u < electionUserListResponse.getResults().getUserlist().size(); u++) {
                                    if (electionUserListResponse.getResults() != null) {
                                        electionuserListArray.add(electionUserListResponse.getResults().getUserlist().get(u));
                                    }
                                }
                            } else {
                                if (isAdded()) {
                                    imageNoNetwork.setVisibility(View.VISIBLE);
                                    textNoNetwork.setVisibility(View.VISIBLE);
                                    imageNoNetwork.setImageResource(R.mipmap.icon_no_timeline);
                                    textNoNetwork.setText(R.string.no_survey);
                                    recyclerElection.setVisibility(View.GONE);
                                }
                            }
                            electionListAdpater = new ElectionListAdpater(electionuserListArray, getActivity(), true);
                            recyclerElection.setAdapter(electionListAdpater);

                            int totalnovotes = completed + pending;
                            float bmi = (float) completed / (float) (totalnovotes);
                            //double roundOff = Math.round(bmi * 100.0) / 100.0;
                            int pollre = (int) ((float) bmi * 100);
                            //double roundOff = Math.round(pollre * 100.0) / 100.0;
                            float bmi1 = (float) pending / (float) (totalnovotes);
                            //double roundOff1 = Math.round(bmi1 * 100.0) / 100.0;
                            int pollre1 = (int) ((float) bmi1 * 100);
                            float roundfinalPrice = (totalnovotes - completed) / totalnovotes;
                            //BigDecimal roundfinalPrice = new BigDecimal(bmi).setScale(1,BigDecimal.ROUND_HALF_UP);
                            textCompleted.setText(+completed + "-" + getString(R.string.completed));
                            textRemaing.setText(+pending + "-" + getString(R.string.remaining));
                            electionProgressBar.setProgress((int) bmi);
                            collapsingToolbar.setVisibility(View.VISIBLE);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, electionUserListResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, electionUserListResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(electionUserListResponse.getSource(),
                                    electionUserListResponse.getSourcedata()));
                            editor.commit();
                            getElectionUserList();
                        }
                    } else {
                        if (electionuserListArray.size() == 0) {
                            if (isAdded()) {
                                imageNoNetwork.setVisibility(View.VISIBLE);
                                textNoNetwork.setVisibility(View.VISIBLE);
                                imageNoNetwork.setImageResource(R.mipmap.icon_no_timeline);
                                //textNoNetwork.setText(R.string.no_network);
                                recyclerElection.setVisibility(View.GONE);
                            }
                        } else {
                            lastEnd = true;
                        }
                    }
                }

                @Override
                public void onFailure(Call<ElectionUserListResponse> call, Throwable t) {
                    hideProgress();
                    isLoading = false;
                    if (electionuserListArray.size() == 0) {
                        if (isAdded()) {
                            imageNoNetwork.setVisibility(View.VISIBLE);
                            textNoNetwork.setVisibility(View.VISIBLE);
                            imageNoNetwork.setImageResource(R.mipmap.icon_no_timeline);
                            textNoNetwork.setText(R.string.error);
                            recyclerElection.setVisibility(View.GONE);
                        }
                    }
                }
            });


        } else {
            if (isAdded()) {
                imageNoNetwork.setVisibility(View.VISIBLE);
                textNoNetwork.setVisibility(View.VISIBLE);
                imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
                textNoNetwork.setText(R.string.no_network);
                recyclerElection.setVisibility(View.GONE);
            }
        }

    }

    public void getElectionSurveyVoterList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            floatSurveyFilter.setVisibility(View.VISIBLE);
            isLoading = true;
            if (currentPage == 0) {
                eleUserslistItems.clear();
            }
            GetSurveyUserListParam getSurveyUserListParam = new GetSurveyUserListParam();
            getSurveyUserListParam.setLimit(20);
            getSurveyUserListParam.setOffset(currentPage);
            getSurveyUserListParam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));

            if (ElectionSurveyFilterActivity.getSurveyUserListParam != null) {
                //Filltered Value
                if (ElectionSurveyFilterActivity.getSurveyUserListParam.getDistrictID() != 0) {
                    getSurveyUserListParam = ElectionSurveyFilterActivity.getSurveyUserListParam;
                }

            } else {
                // Non Filter value
                switch (sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0)) {
                    case 1:
                        //செயலாளர்
//                துணை செயலாளர்
//                மண்டல ஒருங்கிணைப்பா
                        break;
                    case 2:
//
                        //                மாவட்ட ஒருங்கிணைப்பாளர்  district
                        getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                        getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                        break;
                    case 3:
//                தொகுதி ஒருங்கிணைப்பாளர்  constituency
                        getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                        getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                        getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                        break;
                    case 4:
                    case 8:
                    case 12:
                        //Corporation
//                பகுதி ஒருங்கிணைப்பாளர்  part
                        getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                        getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                        getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                        getSurveyUserListParam.setTypeid(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                        switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                            case 1:
                                getSurveyUserListParam.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                                break;
                            case 2:
                                getSurveyUserListParam.setMunicipalityid(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                                break;
                            case 3:
                                getSurveyUserListParam.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                                if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) == 1)
                                    getSurveyUserListParam.setTownshipid(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                                else
                                    getSurveyUserListParam.setVillageid(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));

                                break;
                        }
                        break;
                    case 5:
                    case 9:
                    case 13:
//                வட்டம் ஒருங்கிணைப்பாளர்  vattam
//                Corporation
                        getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                        getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                        getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                        getSurveyUserListParam.setWardid(sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0));
                        getSurveyUserListParam.setTypeid(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));

                        switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                            case 1:
                                getSurveyUserListParam.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                                break;
                            case 2:
                                getSurveyUserListParam.setMunicipalityid(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                                break;
                            case 3:
                                getSurveyUserListParam.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                                if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) == 1)
                                    getSurveyUserListParam.setTownshipid(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                                else
                                    getSurveyUserListParam.setVillageid(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));
                                break;
                        }
                        break;
                    case 7:
                    case 11:
                    case 15:
                    case 19:
//                வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                        //Corporation
                        getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                        getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                        getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                        getSurveyUserListParam.setTypeid(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                        getSurveyUserListParam.setWardid(sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0));

                        switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                            case 1:
                                getSurveyUserListParam.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                                break;
                            case 2:
                                getSurveyUserListParam.setMunicipalityid(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                                break;
                            case 3:
                                getSurveyUserListParam.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                                if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) == 1)
                                    getSurveyUserListParam.setTownshipid(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                                else
                                    getSurveyUserListParam.setVillageid(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));
                                break;
                        }

                        getSurveyUserListParam.setBoothID(sharedPreferences.getInt(DmkConstants.USER_BOOTH_ID, 0));
                        break;
                    case 16:
                        //PanchayatUnion
//                ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                        getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                        getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                        getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                        getSurveyUserListParam.setTypeid(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));

                        switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                            case 1:
                                getSurveyUserListParam.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                                break;
                            case 2:
                                getSurveyUserListParam.setMunicipalityid(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                                break;
                            case 3:
                                getSurveyUserListParam.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                                break;
                        }
                        break;
                    case 17:
//                ஊராட்சி ஒருங்கிணைப்பாளர் villagepanchayat
                        //Panchayat Union
                        getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                        getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                        getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                        getSurveyUserListParam.setTypeid(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                        getSurveyUserListParam.setWardid(sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0));
                        switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                            case 1:
                                getSurveyUserListParam.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                                break;
                            case 2:
                                getSurveyUserListParam.setMunicipalityid(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                                break;
                            case 3:
                                getSurveyUserListParam.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                                if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) != 1)
                                    getSurveyUserListParam.setTownshipid(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                                else
                                    getSurveyUserListParam.setVillageid(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));
                                break;
                        }
                        break;
                }


            }

            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));

            dmkAPI.getsurveysuperieruser(headerMap, getSurveyUserListParam).enqueue(new Callback<ElectionListOverAllResponse>() {
                @Override
                public void onResponse(Call<ElectionListOverAllResponse> call, Response<ElectionListOverAllResponse> response) {
                    hideProgress();
                    isLoading = false;
                    electionListOverAllResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (electionListOverAllResponse.getStatuscode() == 0) {
                            completed = electionListOverAllResponse.getResults().getResult().getOverallcompleted();
                            pending = electionListOverAllResponse.getResults().getResult().getOverallpending();
                            if (electionListOverAllResponse.getResults().getUserslist() != null &&
                                    electionListOverAllResponse.getResults().getUserslist().size() > 0) {
                                if (isAdded()) {
                                    imageNoNetwork.setVisibility(View.GONE);
                                    textNoNetwork.setVisibility(View.GONE);
                                    recyclerElection.setVisibility(View.VISIBLE);
                                }
                                for (int u = 0; u < electionListOverAllResponse.getResults().getUserslist().size(); u++) {
                                    if (electionListOverAllResponse.getResults() != null) {
                                        eleUserslistItems.add(electionListOverAllResponse.getResults().getUserslist().get(u));
                                    }
                                }
                            } else {
                                if (eleUserslistItems.size() == 0) {
                                    if (isAdded()) {
                                        imageNoNetwork.setVisibility(View.VISIBLE);
                                        textNoNetwork.setVisibility(View.VISIBLE);
                                        imageNoNetwork.setImageResource(R.mipmap.icon_no_timeline);
                                        textNoNetwork.setText(R.string.no_survey);
                                        recyclerElection.setVisibility(View.GONE);
                                    }
                                }
                            }
                            if (eleUserslistItems.size() == 0) {
                                if (isAdded()) {
                                    imageNoNetwork.setVisibility(View.VISIBLE);
                                    textNoNetwork.setVisibility(View.VISIBLE);
                                    imageNoNetwork.setImageResource(R.mipmap.icon_no_timeline);
                                    textNoNetwork.setText(R.string.no_survey);
                                    recyclerElection.setVisibility(View.GONE);
                                }
                            }
                            electionOverAllListAdpater = new ElectionOverAllListAdpater(eleUserslistItems, getActivity(), false);
                            recyclerElection.setAdapter(electionOverAllListAdpater);

                            int totalnovotes = completed + pending;
                            float bmi = (float) completed / (float) (totalnovotes);
                            double roundOff = Math.round(bmi * 100.0) / 100.0;
                            int pollre = (int) ((float) roundOff * 100);
                            float bmi1 = (float) pending / (float) (totalnovotes);
                            double roundOff1 = Math.round(bmi1 * 100.0) / 100.0;
                            int pollre1 = (int) ((float) roundOff1 * 100);

                            textCompleted.setText(+pollre + " % " + getString(R.string.completed));
                            textRemaing.setText(+pollre1 + " % " + getString(R.string.remaining));
                            electionProgressBar.setProgress(pollre);
                            collapsingToolbar.setVisibility(View.VISIBLE);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, electionListOverAllResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, electionListOverAllResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(electionListOverAllResponse.getSource(),
                                    electionListOverAllResponse.getSourcedata()));
                            editor.commit();
                            getElectionSurveyVoterList();
                        }
                    } else {
                        if (eleUserslistItems.size() == 0) {
                            if (isAdded()) {
                                imageNoNetwork.setVisibility(View.VISIBLE);
                                textNoNetwork.setVisibility(View.VISIBLE);
                                imageNoNetwork.setImageResource(R.mipmap.icon_no_timeline);
                                textNoNetwork.setText(R.string.no_survey);
                                recyclerElection.setVisibility(View.GONE);
                                textCompleted.setText(+0 + " % " + getString(R.string.completed));
                                textRemaing.setText(+0 + " % " + getString(R.string.remaining));
                                electionProgressBar.setProgress(0);
                                //collapsingToolbar.setVisibility(View.INVISIBLE);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ElectionListOverAllResponse> call, Throwable t) {
                    hideProgress();
                    isLoading = false;
                    if (eleUserslistItems.size() == 0) {
                        if (isAdded()) {
                            imageNoNetwork.setVisibility(View.VISIBLE);
                            textNoNetwork.setVisibility(View.VISIBLE);
                            imageNoNetwork.setImageResource(R.mipmap.icon_no_timeline);
                            textNoNetwork.setText(R.string.no_survey);
                            recyclerElection.setVisibility(View.GONE);
                            textCompleted.setText(+0 + " % " + getString(R.string.completed));
                            textRemaing.setText(+0 + " % " + getString(R.string.remaining));
                            electionProgressBar.setProgress(0);
                            //collapsingToolbar.setVisibility(View.INVISIBLE);

                        }
                    }
                }
            });


        } else {
            if (isAdded()) {
                imageNoNetwork.setVisibility(View.VISIBLE);
                textNoNetwork.setVisibility(View.VISIBLE);
                imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
                textNoNetwork.setText(R.string.no_network);
                recyclerElection.setVisibility(View.GONE);
                textCompleted.setText(+0 + " % " + getString(R.string.completed));
                textRemaing.setText(+0 + " % " + getString(R.string.remaining));
                electionProgressBar.setProgress(0);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        if (isVisible) {

            for (int i = 0; i < Surveryuser.length; i++) {
                if (sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == Surveryuser[i]) {

                    surveryuserboolean = true;
                    break;
                } else {
                    surveryuserboolean = false;
                }
            }
            if (surveryuserboolean) {
                getElectionUserList();
            } else {
                getElectionSurveyVoterList();
            }


        }

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {

            boolean surveryuserboolean = false;
            for (int i = 0; i < Surveryuser.length; i++) {
                if (sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == Surveryuser[i]) {

                    surveryuserboolean = true;
                    break;
                } else {
                    surveryuserboolean = false;
                }
            }
            if (surveryuserboolean) {
                getElectionUserList();
            } else {
                getElectionSurveyVoterList();
            }


        } else {
            ElectionSurveyFilterActivity.getSurveyUserListParam = null;
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
}
