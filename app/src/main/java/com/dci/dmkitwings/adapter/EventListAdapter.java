package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.EventComposeActivity;
import com.dci.dmkitwings.activity.EventDetailsActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.EventsFragment;
import com.dci.dmkitwings.model.AddEventsResponse;
import com.dci.dmkitwings.model.EventsResultsItem;
import com.dci.dmkitwings.model.ShareCount;
import com.dci.dmkitwings.model.ShareCountResponse;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Java_AES_Cipher;
import com.dci.dmkitwings.utils.Util;
import com.firebase.client.utilities.Base64;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.dmkitwings.app.DmkApplication.getProxy;

/**
 * Created by vijayaganesh on 4/2/2018.
 */

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.TimeLineViewHolder> {


    List<EventsResultsItem> eventList;
    Context context;
    HttpProxyCacheServer proxy;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    DmkAPI dmkAPI;
    BaseActivity baseActivity;
    EventsFragment eventsFragment;
    private Date currentDate;
    AddEventsResponse addEventsResponse;
    private SharedPreferences.Editor editor;
    private ShareCountResponse shareCountResponse;


    public EventListAdapter(List<EventsResultsItem> eventList, Context context, BaseActivity baseActivity,
                            EventsFragment eventsFragment) {
        this.eventList = eventList;
        this.context = context;
        this.eventsFragment = eventsFragment;
        this.baseActivity = baseActivity;

    }


    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event, parent, false);
        proxy = getProxy(context);
        DmkApplication.getContext().getComponent().inject(this);
        return new TimeLineViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TimeLineViewHolder holder, final int position) {
        editor = sharedPreferences.edit();
        universalImageLoader(holder.imageEvent, context.getString(R.string.s3_bucket_events_path),
                eventList.get(position).getMediafiles().size() > 0 ?
                        eventList.get(position).getMediafiles().get(0) : "", R.mipmap.icon_loading_100x100,
                R.mipmap.icon_no_image_100x100);


        holder.textEventname.setText(eventList.get(position).getName());
        holder.textEventDate.setText(
                context.getString(R.string.from) + " " + eventList.get(position).getStartDate() + " " +
                        context.getString(R.string.to) + " " + eventList.get(position).getEndDate());
        if (sharedPreferences.getInt(DmkConstants.USERID, 0) == eventList.get(position).getCreatedBy())
            holder.imageMenu.setVisibility(View.VISIBLE);
        else
            holder.imageMenu.setVisibility(View.GONE);

        holder.imageMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, holder.imageMenu);
                //inflating menu_icon from xml resource
                popup.inflate(R.menu.comment_item_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit:
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setMessage(R.string.confirm_to_edit);
                                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(context, EventComposeActivity.class);
                                        intent.putExtra("eventID", eventList.get(position).getId());
                                        intent.putExtra("districtID", eventList.get(position).getDistrictID());
                                        intent.putExtra("isFromEdit", true);
                                        context.startActivity(intent);
                                    }

                                });
                                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                                return true;
                            case R.id.delete:
                                //handle menu2 click
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(context);
                                alertDialog1.setMessage(R.string.confirm_to_delete);
                                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        deleteEvent(eventList.get(position).getId());
                                    }

                                });
                                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog1.show();
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
            }
        });

//        holder.imageShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String ps = "Dmk_encryption " + eventList.get(position).getId();
//                String tmp = Base64.encodeBytes(ps.getBytes());
//                tmp = Base64.encodeBytes(tmp.getBytes());
//                Intent i = new Intent(Intent.ACTION_SEND);
//                i.setType("text/plain");
//                i.putExtra(Intent.EXTRA_TEXT, eventList.get(position).getName()
//                        + "\n " + context.getString(R.string.from) + " " + eventList.get(position).getStartDate() + " " +
//                        context.getString(R.string.to) + " " + eventList.get(position).getEndDate()
//                        + "\n" + context.getString(R.string.for_more) +
//                        context.getString(R.string.deeplink_url) + "events/" + tmp);
//
//                context.startActivity(Intent.createChooser(i, "Choose share option"));
//            }
//        });

        holder.imageEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EventDetailsActivity.class);
                intent.putExtra("eventID", eventList.get(position).getId());
                context.startActivity(intent);
            }
        });

        holder.textEventDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EventDetailsActivity.class);
                intent.putExtra("eventID", eventList.get(position).getId());
                context.startActivity(intent);
            }
        });

        holder.textEventname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EventDetailsActivity.class);
                intent.putExtra("eventID", eventList.get(position).getId());
                context.startActivity(intent);
            }
        });

        if (eventList.get(position).getSharecount() > 99) {
            holder.textShare.setText("(99+)");
        } else if (eventList.get(position).getSharecount() == 0) {
            holder.textShare.setText("");
        } else {
            holder.textShare.setText("(" + eventList.get(position).getSharecount() + ")");
        }

        holder.textShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addShareCount(position, holder.textShare);
            }
        });


    }

    private void addShareCount(final int position, final TextView textShareCount) {
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            ShareCount shareCount = new ShareCount();
            shareCount.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            shareCount.setMainid(eventList.get(position).getId());
            shareCount.setType(2);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.addShareCount(headerMap, shareCount).enqueue(new Callback<ShareCountResponse>() {
                @Override
                public void onResponse(Call<ShareCountResponse> call, Response<ShareCountResponse> response) {
                    baseActivity.hideProgress();
                    shareCountResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (shareCountResponse.getStatuscode() == 0) {
                            eventList.get(position).setSharecount(eventList.get(position).getSharecount() + 1);
                            if (eventList.get(position).getSharecount() > 99) {
                                textShareCount.setText("(99+)");
                            } else if (eventList.get(position).getSharecount() == 0) {
                                textShareCount.setText("");
                            } else {
                                textShareCount.setText("(" + eventList.get(position).getSharecount() + ")");
                            }
                            String ps = "Dmk_encryption " + eventList.get(position).getId();
                            String tmp = Base64.encodeBytes(ps.getBytes());
                            tmp = Base64.encodeBytes(tmp.getBytes());
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_TEXT, eventList.get(position).getName()
                                    + "\n " + context.getString(R.string.from) + " " + eventList.get(position).getStartDate() + " " +
                                    context.getString(R.string.to) + " " + eventList.get(position).getEndDate()
                                    + "\n" + context.getString(R.string.for_more) +
                                    context.getString(R.string.deeplink_url) + "events/" + tmp);

                            context.startActivity(Intent.createChooser(i, "Choose share option"));
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, shareCountResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, shareCountResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(shareCountResponse.getSource(),
                                    shareCountResponse.getSourcedata()));
                            editor.commit();
                            addShareCount(position, textShareCount);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ShareCountResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    public Bitmap createThumbnailFromPath(String filePath, int type) {
        return ThumbnailUtils.createVideoThumbnail(filePath, type);
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }


    public class TimeLineViewHolder extends RecyclerView.ViewHolder {

        ImageView imageEvent, imageShare, imageMenu;
        TextView textEventname, textEventDate, textShare;

        public TimeLineViewHolder(View itemView) {
            super(itemView);
            imageEvent = (ImageView) itemView.findViewById(R.id.image_event);
            imageShare = (ImageView) itemView.findViewById(R.id.image_event_share);
            imageMenu = (ImageView) itemView.findViewById(R.id.image_menu);
            textEventname = (TextView) itemView.findViewById(R.id.text_event_name);
            textEventDate = (TextView) itemView.findViewById(R.id.text_event_date);
            textShare = (TextView) itemView.findViewById(R.id.text_share);


        }
    }


    private void deleteEvent(final int eventid) {
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            TimeLineDelete timeLineDelete = new TimeLineDelete();
            timeLineDelete.setEventid(eventid);
            timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.deleteEvents(headerMap, timeLineDelete).enqueue(new Callback<AddEventsResponse>() {
                @Override
                public void onResponse(Call<AddEventsResponse> call, Response<AddEventsResponse> response) {
                    baseActivity.hideProgress();
                    addEventsResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (addEventsResponse.getStatuscode() == 0) {
                            currentDate = Calendar.getInstance().getTime();
                            eventsFragment.getEventList(currentDate);
                            eventsFragment.getEventListInCalendar();
                            Toast.makeText(context, context.getString(R.string.event_del_sucess), Toast.LENGTH_SHORT).show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, addEventsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, addEventsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(addEventsResponse.getSource(),
                                    addEventsResponse.getSourcedata()));
                            editor.commit();
                            deleteEvent(eventid);
                        }
                    } else {
                        Toast.makeText(context, context.getString(R.string.event_del_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddEventsResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }

    public String getEncodedHeader(String sourceResponse, String sourceDataResponse) {

        byte[] sourceBase64 = android.util.Base64.decode(sourceResponse, android.util.Base64.NO_WRAP);
        byte[] sourceDataBase64 = android.util.Base64.decode(sourceDataResponse, android.util.Base64.NO_WRAP);
        String source = new String(sourceBase64, StandardCharsets.UTF_8);
        String sourceData = new String(sourceDataBase64, StandardCharsets.UTF_8);
        String data = source.substring(0, 12) + sourceData.substring(0, 8);
        String key = source.substring(12, 19) + sourceData.substring(8, 13);
        String iv = source.substring(19, 28) + sourceData.substring(13, 20);
        String encrpytData = Java_AES_Cipher.encrypt(key, iv, data);
        byte[] dataBase64Encode = encrpytData.getBytes(StandardCharsets.UTF_8);
        String base64 = android.util.Base64.encodeToString(dataBase64Encode, android.util.Base64.NO_WRAP);
        return base64;
    }

}
