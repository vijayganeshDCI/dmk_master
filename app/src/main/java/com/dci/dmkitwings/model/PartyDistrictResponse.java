package com.dci.dmkitwings.model;

import java.util.List;

public class PartyDistrictResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<PartyDistrictResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<PartyDistrictResultsItem> results) {
		Results = results;
	}

	private String Status;
	private List<PartyDistrictResultsItem> Results;
	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}