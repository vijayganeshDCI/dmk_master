package com.dci.dmkitwings.fragment;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.activity.UserProfileActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vijayaganesh on 4/2/2018.
 */

public class PartyMemberCardFragment extends BaseFragment {
    @BindView(R.id.view_top)
    View viewTop;
    @BindView(R.id.text_label_home)
    TextView textLabelHome;
    @BindView(R.id.guideline)
    Guideline guideline;
    @BindView(R.id.image_app_icon)
    ImageView imageAppIcon;
    @BindView(R.id.image_app_left)
    ImageView imageAppLeft;
    @BindView(R.id.image_app_right)
    ImageView imageAppRight;
    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    @BindView(R.id.text_label_member_name)
    TextView textLabelMemberName;
    @BindView(R.id.text_member_name)
    TextView textMemberName;
    @BindView(R.id.text_label_member_designation)
    TextView textLabelMemberDesignation;
    @BindView(R.id.text_member_designation)
    TextView textMemberDesignation;
    @BindView(R.id.text_label_member_age)
    TextView textLabelMemberAge;
    @BindView(R.id.text_member_Age)
    TextView textMemberAge;
    @BindView(R.id.text_label_member_district)
    TextView textLabelMemberDistrict;
    @BindView(R.id.text_member_district)
    TextView textMemberDistrict;
    @BindView(R.id.text_label_member_phone_number)
    TextView textLabelMemberPhoneNumber;
    @BindView(R.id.text_member_phone_number)
    TextView textMemberPhoneNumber;
    @BindView(R.id.view_bottom_left)
    View viewBottomLeft;
    @BindView(R.id.view_bottom_right)
    View viewBottomRight;
    @BindView(R.id.cons_member_details)
    ConstraintLayout consMemberDetails;
    @BindView(R.id.card_reg)
    CardView cardReg;
    @BindView(R.id.text_label_download)
    TextView textLabelDownload;
    @BindView(R.id.cons_reg_card)
    ConstraintLayout consRegCard;
    @BindView(R.id.download_icon)
    ImageView downloadicon;
    @BindView(R.id.image_Qr_Gene)
    ImageView imageQRcode;
    Animation animation;
    Bitmap bitmap;
    Bundle bundle;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @BindView(R.id.guideline1)
    Guideline guideline1;
    @BindView(R.id.text_label_member_id_water_mark)
    CustomTextView textLabelMemberIdWaterMark;
    @BindView(R.id.cons_member_water_mark)
    ConstraintLayout consMemberWaterMark;
    @BindView(R.id.text_label_member_id_number)
    CustomTextView textLabelMemberIdNumber;
    @BindView(R.id.text_member_id_number)
    CustomTextView textMemberIdNumber;
    private IntentIntegrator qrScan;
    private boolean profilePic=false;
    Bitmap imgProfilepic;
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_card, container, false);
        ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        animation = AnimationUtils.loadAnimation(getContext(), R.anim.download_animation);
        downloadicon.startAnimation(animation);
        qrScan = new IntentIntegrator(getActivity());
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            imgProfilepic=bundle.getParcelable("galleryUri");

        }

        if (getActivity() instanceof UserProfileActivity) {
            viewTop.setVisibility(View.GONE);
            textLabelHome.setVisibility(View.GONE);
            universalImageLoader(imageProPic, getString(R.string.s3_bucket_profile_path),
                    sharedPreferences.getString(DmkConstants.PROFILE_PICTURE, ""), R.mipmap.icon_pro_image_loading_256,
                    R.mipmap.icon_pro_image_loading_256);

        } else {
            viewTop.setVisibility(View.VISIBLE);
            textLabelHome.setVisibility(View.VISIBLE);
            imageProPic.setImageBitmap(imgProfilepic);

        }
        textLabelDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress();
                textLabelDownload.setVisibility(View.INVISIBLE);
                downloadicon.clearAnimation();
                downloadicon.setVisibility(View.INVISIBLE);
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        storeImage(getBitmapFromView(cardReg));
                    }
                }, 2500);
                downloadicon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showProgress();
                        textLabelDownload.setVisibility(View.INVISIBLE);
                        downloadicon.setVisibility(View.INVISIBLE);
                        downloadicon.clearAnimation();
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                storeImage(getBitmapFromView(cardReg));
                            }
                        }, 2500);
                    }
                });



            }
        });


        textMemberName.setText(sharedPreferences.getString(DmkConstants.FIRSTNAME, null));
        textMemberPhoneNumber.setText(sharedPreferences.getString(DmkConstants.PHONENUMBER, null));
        textMemberAge.setText(String.valueOf(sharedPreferences.getInt(DmkConstants.USERAGE, 0)));
        textMemberDesignation.setText(sharedPreferences.getString(DmkConstants.DESGINATION, null));
        textMemberDistrict.setText(sharedPreferences.getString(DmkConstants.USERDISTRICTNAME, null));
        textMemberIdNumber.setText(sharedPreferences.getString(DmkConstants.UNIQUEDMKID, null));
        textLabelMemberIdWaterMark.setText(sharedPreferences.getString(DmkConstants.UNIQUEDMKID, null));


//        VCard abhay=new VCard(sharedPreferences.getString(DmkConstants.FIRSTNAME,null)+sharedPreferences.getString(DmkConstants.LASTNAME,null))
//                .setAddress(sharedPreferences.getString(DmkConstants.USERDISTRICTNAME,null))
//                .setTitle(sharedPreferences.getString(DmkConstants.DESGINATION,null))
//                .setCompany(sharedPreferences.getString(DmkConstants.USERDISTRICTNAME,null))
//                .setPhoneNumber(sharedPreferences.getString(DmkConstants.PHONENUMBER,null))
//                .setWebsite("www.studytutorial.in");
//        Bitmap myBitmap= QRCode.from(abhay).bitmap();
        //imageQRcode.setImageBitmap(myBitmap);




        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {

            } else {
                //if qr contains data
                try {
                    //converting the data to json
                    JSONObject obj = new JSONObject(result.getContents());
                    //setting values to textviews


                } catch (JSONException e) {
                    e.printStackTrace();
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast


                }
            }
        } else {

            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();
//        null.unbind();
    }


    @OnClick({R.id.text_label_home})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_label_home:
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                if (getActivity().getIntent().getStringExtra("timeLineID") != null)
                    intent.putExtra("timeLineID", getActivity().getIntent().getStringExtra("timeLineID"));
                else if (getActivity().getIntent().getStringExtra("newsID") != null)
                    intent.putExtra("newsID", getActivity().getIntent().getStringExtra("newsID"));
                else if (getActivity().getIntent().getStringExtra("eventID") != null)
                    intent.putExtra("eventID", getActivity().getIntent().getStringExtra("eventID"));
                else if (getActivity().getIntent().getStringExtra("articleID") != null)
                    intent.putExtra("articleID", getActivity().getIntent().getStringExtra("articleID"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;


        }
    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();

        if (pictureFile == null) {
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            getActivity().sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri
                    .parse("file://" + pictureFile)));

            fos.close();
            hideProgress();
            //textLabelDownload.setVisibility(View.VISIBLE);
            //downloadicon.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), getString(R.string.imagedownload) + pictureFile, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            if (getActivity().getIntent().getStringExtra("timeLineID") != null)
                intent.putExtra("id", getActivity().getIntent().getStringExtra("timeLineID"));
            else if (getActivity().getIntent().getStringExtra("newsID") != null)
                intent.putExtra("newsID", getActivity().getIntent().getStringExtra("newsID"));
            else if (getActivity().getIntent().getStringExtra("eventID") != null)
                intent.putExtra("eventID", getActivity().getIntent().getStringExtra("eventID"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);


        } catch (FileNotFoundException e) {

        } catch (IOException e) {

        }
    }

    private File getOutputMediaFile() {
        //External Storage
        File mediaStorageDirExternal = new File(Environment.getExternalStorageDirectory()
                + "/Download");
        //Internal Storage
//        File mediaStorageDirInternal = new File(getContext().getFilesDir()
//                + "/Download");

//        if (!mediaStorageDir.exists()) {
//            if (!mediaStorageDir.mkdirs()) {
//                return null;
//            }
//        }
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "Regcard_" + timeStamp + ".png";
        mediaFile = new File(mediaStorageDirExternal.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

//    public static void saveFile(Context context, Bitmap b, String picName) {
//        FileOutputStream fos = null;
//        try {
//            fos = context.openFileOutput(picName, Context.MODE_PRIVATE);
//            b.compress(Bitmap.CompressFormat.PNG, 100, fos);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                fos.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }


}
