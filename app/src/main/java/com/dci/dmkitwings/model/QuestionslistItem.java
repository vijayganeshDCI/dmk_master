package com.dci.dmkitwings.model;

import java.util.List;

public class QuestionslistItem{
	private String Option4;
	private String Status;
	private String Option3;
	private String Option2;
	private String Option1;
	private String updated_at;
	private int PollID;
	private String created_at;
	private String Question;
	private int id;
	private String Option5;
	private String OptionOnePer;
	private String OptionTwoPer;
	private String OptionThreePer;
	private String OptionFourPer;

	public String getOptionOnePer() {
		return OptionOnePer;
	}

	public void setOptionOnePer(String optionOnePer) {
		OptionOnePer = optionOnePer;
	}

	public String getOptionTwoPer() {
		return OptionTwoPer;
	}

	public void setOptionTwoPer(String optionTwoPer) {
		OptionTwoPer = optionTwoPer;
	}

	public String getOptionThreePer() {
		return OptionThreePer;
	}

	public void setOptionThreePer(String optionThreePer) {
		OptionThreePer = optionThreePer;
	}

	public String getOptionFourPer() {
		return OptionFourPer;
	}

	public void setOptionFourPer(String optionFourPer) {
		OptionFourPer = optionFourPer;
	}

	public String getOption4() {
		return Option4;
	}

	public void setOption4(String option4) {
		Option4 = option4;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getOption3() {
		return Option3;
	}

	public void setOption3(String option3) {
		Option3 = option3;
	}

	public String getOption2() {
		return Option2;
	}

	public void setOption2(String option2) {
		Option2 = option2;
	}

	public String getOption1() {
		return Option1;
	}

	public void setOption1(String option1) {
		Option1 = option1;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getPollID() {
		return PollID;
	}

	public void setPollID(int pollID) {
		PollID = pollID;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getQuestion() {
		return Question;
	}

	public void setQuestion(String question) {
		Question = question;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOption5() {
		return Option5;
	}

	public void setOption5(String option5) {
		Option5 = option5;
	}
}
