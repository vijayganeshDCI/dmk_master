package com.dci.dmkitwings.model;

import java.util.ArrayList;

/**
 * Created by vijayaganesh on 1/4/2018.
 */

public class Registration {

    private int UserType;
    private String imeiid;
    private String DeviceToken;
    private String FirstName;
    private String LastName;
    private int Status;
    private String Gender;
    private String PhoneNumber;
    private String DOB;
    private int Age;
    private int DistrictID;
    private int ConstituencyID;
    private String typeid;
    private int partid;
    private int municipalityid;
    private int townshipid;
    // private int vattamid;
    private int WardID;
    private int Panchayatunion;
    private int Villagepanchayat;
    private int RoleID;
    private int uniontype;
    private ArrayList<Integer> UserPermission;
    private String Address;
    private String VoterID;
    private int BoothID;
    private String profilePhoto;
    private String Emailid;
    private int Wing;
    private String appversion;
    private int apptype;
    private String osversion;
    private String user_id;





    public int getUniontype() {
        return uniontype;
    }

    public void setUniontype(int uniontype) {
        this.uniontype = uniontype;
    }



    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getVoterID() {
        return VoterID;
    }

    public void setVoterID(String voterID) {
        VoterID = voterID;
    }



    public int getBoothID() {
        return BoothID;
    }

    public void setBoothID(int boothID) {
        BoothID = boothID;
    }


    public ArrayList<Integer> getUserPermission() {
        return UserPermission;
    }

    public void setUserPermission(ArrayList<Integer> userPermission) {
        UserPermission = userPermission;
    }

    public String getImage_pro_pic() {
        return profilePhoto;
    }

    public void setImage_pro_pic(String image_pro_pic) {
        this.profilePhoto = image_pro_pic;
    }


    public String getEmailid() {
        return Emailid;
    }

    public void setEmailid(String emailid) {
        Emailid = emailid;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public String getImeiid() {
        return imeiid;
    }

    public void setImeiid(String imeiid) {
        this.imeiid = imeiid;
    }

    public String getDevicetoken() {
        return DeviceToken;
    }

    public void setDevicetoken(String devicetoken) {
        this.DeviceToken = devicetoken;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public int getDistrictID() {
        return DistrictID;
    }

    public void setDistrictID(int districtID) {
        DistrictID = districtID;
    }

    public int getConstituencyID() {
        return ConstituencyID;
    }

    public void setConstituencyID(int constituencyID) {
        ConstituencyID = constituencyID;
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public int getPartid() {
        return partid;
    }

    public void setPartid(int partid) {
        this.partid = partid;
    }

    public int getMunicipalityid() {
        return municipalityid;
    }

    public void setMunicipalityid(int municipalityid) {
        this.municipalityid = municipalityid;
    }

    public int getTownshipid() {
        return townshipid;
    }

    public void setTownshipid(int townshipid) {
        this.townshipid = townshipid;
    }

//    public int getVattamid() {
//        return vattamid;
//    }
//
//    public void setVattamid(int vattamid) {
//        this.vattamid = vattamid;
//    }

    public int getWardID() {
        return WardID;
    }

    public void setWardID(int wardID) {
        WardID = wardID;
    }

    public int getPanchayatunion() {
        return Panchayatunion;
    }

    public void setPanchayatunion(int panchayatunion) {
        Panchayatunion = panchayatunion;
    }

    public int getVillagepanchayat() {
        return Villagepanchayat;
    }

    public void setVillagepanchayat(int villagepanchayat) {
        Villagepanchayat = villagepanchayat;
    }

    public int getRoleID() {
        return RoleID;
    }

    public void setRoleID(int roleID) {
        RoleID = roleID;
    }

    public int getWing() {
        return Wing;
    }

    public void setWing(int wing) {
        Wing = wing;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }



    public String getDeviceToken() {
        return DeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getAppversion() {
        return appversion;
    }

    public void setAppversion(String appversion) {
        this.appversion = appversion;
    }

    public int getApptype() {
        return apptype;
    }

    public void setApptype(int apptype) {
        this.apptype = apptype;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }




}
