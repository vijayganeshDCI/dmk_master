package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.TimeLineDetailPageActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.CommentFragment;
import com.dci.dmkitwings.model.CommentEditParams;
import com.dci.dmkitwings.model.CommentEditResponse;
import com.dci.dmkitwings.model.CommentList;
import com.dci.dmkitwings.model.CommentPost;
import com.dci.dmkitwings.model.CommentPostResponse;
import com.dci.dmkitwings.model.TimeLineDeleteResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Java_AES_Cipher;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomEditText;
import com.dci.dmkitwings.view.CustomTextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    Context context;
    CommentFragment commentFragment;
    Activity activity;
    BaseActivity baseActivity;
    TimeLineDetailPageActivity timeLineDetailPageActivity;
    List<CommentList> commentLists;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    CommentEditResponse commentEditResponse;
    TimeLineDeleteResponse timeLineDeleteResponse;
    public static final int TYPE_Footer = 1;
    public CommentListViewHolder commentListViewHolder;
    public FooterViewHolder footerViewHolder;
    private CommentPostResponse commentPostResponse;
    int timeLineID;
    //Type of data in recycler view
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private SharedPreferences.Editor editor;


    public CommentAdapter(Context context, List<CommentList> commentLists, CommentFragment commentFragment,
                          Activity activity, int timeLineID) {
        this.context = context;
        this.commentLists = commentLists;
        this.activity = activity;
        this.timeLineID = timeLineID;
        this.commentFragment = commentFragment;
        DmkApplication.getContext().getComponent().inject(this);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        editor = sharedPreferences.edit();
        if (viewType == TYPE_FOOTER) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_time_detail_footer, parent, false);
            return new FooterViewHolder(itemView);
        } else if (viewType == TYPE_ITEM) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_comment_list, parent, false);
            return new CommentListViewHolder(itemView);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");//Some error occurs then exception occurs
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (activity instanceof BaseActivity) {
            baseActivity = (BaseActivity) activity;
        } else {
            timeLineDetailPageActivity = (TimeLineDetailPageActivity) activity;
        }

        if (holder instanceof CommentListViewHolder) {
            commentListViewHolder = (CommentListViewHolder) holder;
            commentListViewHolder.textUserComment.setText(commentLists.get(position).getComment());
            commentListViewHolder.textUserDestination.setText(commentLists.get(position).getDate());


            if (commentLists.get(position).getUserID() == sharedPreferences.getInt(DmkConstants.USERID, 0)) {
                commentListViewHolder.textUserName.setText("You");
                commentListViewHolder.imageEdit.setVisibility(View.VISIBLE);
                commentListViewHolder.imageDelete.setVisibility(View.VISIBLE);
            } else {
                commentListViewHolder.imageEdit.setVisibility(View.GONE);
                commentListViewHolder.imageDelete.setVisibility(View.GONE);
                commentListViewHolder.textUserName.setText(commentLists.get(position).getName());
            }
            universalImageLoader(commentListViewHolder.imageProPic, context.getString(R.string.s3_bucket_profile_path),
                    commentLists.get(position).getProfilePicture(), R.mipmap.icon_pro_image_loading_256);

            commentListViewHolder.imageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setMessage(R.string.confirm_to_delete);
                    alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteComment(position);
                        }

                    });
                    alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            });

            commentListViewHolder.imageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setMessage(R.string.confirm_to_edit);
                    alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(context);
                            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View dialogView = mInflater.inflate(R.layout.alert_edit_comment, null);
                            alertDialog1.setView(dialogView);
                            final CustomEditText editText = (CustomEditText) dialogView.findViewById(R.id.edit_write_comment);
                            editText.setText(commentLists.get(position).getComment());
                            ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_comment_send);
                            alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (editText.getText().toString().isEmpty())
                                        editText.setError(context.getString(R.string.empty_comment));
                                    else
                                        updateComment(position, editText.getText().toString());
                                }

                            });
                            alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alertDialog1.show();
                        }

                    });
                    alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            });

            commentListViewHolder.imageProPic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (commentLists.get(position).getUserID() != 1 && commentLists.get(position).getUserID() != sharedPreferences.getInt(DmkConstants.USERID, 0)) {

                        BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(context).inflate(R.layout.profile_bubble, null);
                        PopupWindow popupWindow = BubblePopupHelper.create(context, bubbleLayout);
                        CircleImageView imageprofilepic = (CircleImageView) bubbleLayout.findViewById(R.id.image_pro_pic_bubble);
                        CustomTextView textpostedby = (CustomTextView) bubbleLayout.findViewById(R.id.text_posted_by_bubble);
                        CustomTextView textdesgination = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_designation_bubble);
                        CustomTextView textdistrict = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_district_bubble);
                        CustomTextView textconstituency = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_constituency_bubble);
                        CustomTextView textPartBoothWardVillage = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_common_bubble);
                        switch (commentLists.get(position).getLevel()) {
                            case 1:
//                            செயலாளர்
//                            துணை செயலாளர்
//                            மண்டல ஒருங்கிணைப்பாளர்
                                textpostedby.setText(commentLists.get(position).getName());
                                textdesgination.setText(commentLists.get(position).getDesigination());
                                textdistrict.setVisibility(View.GONE);
                                textconstituency.setVisibility(View.GONE);
                                textPartBoothWardVillage.setVisibility(View.GONE);
                                break;
                            case 2:
                                //மாவட்ட ஒருங்கிணைப்பாளர் district
                                textpostedby.setText(commentLists.get(position).getName());
                                textdesgination.setText(commentLists.get(position).getDesigination());
                                textdistrict.setText(commentLists.get(position).getDistrictName());
                                textconstituency.setVisibility(View.GONE);
                                textPartBoothWardVillage.setVisibility(View.GONE);
                                break;
                            case 3:
                                //தொகுதி ஒருங்கிணைப்பாளர் constituency
                                textpostedby.setText(commentLists.get(position).getName());
                                textdesgination.setText(commentLists.get(position).getDesigination());
                                textdistrict.setText(commentLists.get(position).getDistrictName());
                                textconstituency.setText(commentLists.get(position).getConstituencyName());
                                textPartBoothWardVillage.setVisibility(View.GONE);
                                break;
                            case 4:
                            case 8:
                            case 12:
                            case 16:
                                //பகுதி ஒருங்கிணைப்பாளர் part
                                //நகராட்சி ஒருங்கிணைப்பாளர் municipality
                                //பேரூராட்சி ஒருங்கிணைப்பாளர் township
                                //ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                                textpostedby.setText(commentLists.get(position).getName());
                                textdesgination.setText(commentLists.get(position).getDesigination());
                                textdistrict.setText(commentLists.get(position).getDistrictName());
                                textconstituency.setText(commentLists.get(position).getConstituencyName());
                                textPartBoothWardVillage.setText(commentLists.get(position).getPart());
                                break;
                            case 5:
                            case 6:
                            case 9:
                            case 10:
                            case 13:
                            case 14:
                            case 18:
                                //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                                //கிளை ஒருங்கிணைப்பாளர் ward
                                // வட்டம் ஒருங்கிணைப்பாளர்  vattam
                                // கிளை ஒருங்கிணைப்பாளர் ward
                                //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                                // கிளை ஒருங்கிணைப்பாளர் ward
                                //கிளை ஒருங்கிணைப்பாளர்  ward
                                textpostedby.setText(commentLists.get(position).getName());
                                textdesgination.setText(commentLists.get(position).getDesigination());
                                textdistrict.setText(commentLists.get(position).getDistrictName());
                                textconstituency.setText(commentLists.get(position).getConstituencyName());
                                textPartBoothWardVillage.setText(commentLists.get(position).getWard());
                                break;
                            case 7:
                            case 11:
                            case 15:
                            case 19:
                                //வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                                textpostedby.setText(commentLists.get(position).getName());
                                textdesgination.setText(commentLists.get(position).getDesigination());
                                textdistrict.setText(commentLists.get(position).getDistrictName());
                                textconstituency.setText(commentLists.get(position).getConstituencyName());
                                textPartBoothWardVillage.setText(commentLists.get(position).getBooth());
                                break;
                            case 17:
                                //ராட்சிஊ ஒருங்கிணைப்பாளர் villagepanchayat
                                textpostedby.setText(commentLists.get(position).getName());
                                textdesgination.setText(commentLists.get(position).getDesigination());
                                textdistrict.setText(commentLists.get(position).getDistrictName());
                                textconstituency.setText(commentLists.get(position).getConstituencyName());
                                textPartBoothWardVillage.setText(commentLists.get(position).getVillage());
                                break;


                        }


                        universalImageLoader(imageprofilepic, context.getString(R.string.s3_bucket_profile_path),
                                commentLists.get(position).getProfilePicture(), R.mipmap.icon_pro_image_loading_256);
                        final Random random = new Random();
                        int[] location = new int[2];
                        v.getLocationInWindow(location);
                        bubbleLayout.setArrowDirection(ArrowDirection.TOP);
                        popupWindow.showAtLocation(v, Gravity.NO_GRAVITY, v.getWidth() + location[0], v.getHeight() + location[1]);
                    }

                }
            });
        } else {
            footerViewHolder = (FooterViewHolder) holder;
            if (activity instanceof TimeLineDetailPageActivity) {
                footerViewHolder.imageProPic.setVisibility(View.VISIBLE);
                footerViewHolder.editComment.setVisibility(View.VISIBLE);
                footerViewHolder.imageSend.setVisibility(View.VISIBLE);
                footerViewHolder.imageSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (footerViewHolder.editComment.getText().toString().isEmpty())
                            footerViewHolder.editComment.setError(context.getString(R.string.empty_comment));
                        else
                            postComment();
                    }
                });
                universalImageLoader(footerViewHolder.imageProPic, context.getString(R.string.s3_bucket_profile_path),
                        sharedPreferences.getString(DmkConstants.PROFILE_PICTURE, ""), R.mipmap.icon_pro_image_loading_256);
            } else {
                footerViewHolder.imageProPic.setVisibility(View.GONE);
                footerViewHolder.editComment.setVisibility(View.GONE);
                footerViewHolder.imageSend.setVisibility(View.GONE);
                footerViewHolder.footerView.setVisibility(View.GONE);

            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        //Return item type according to requirement
        if (isPositionFooter(position))
            return TYPE_FOOTER;
        return TYPE_ITEM;
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(loadingImage)
                .showImageOnFail(loadingImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) + s3bucketName + image, imageView, options);
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == commentLists.size();
    }


    @Override
    public int getItemCount() {
        return (null != commentLists ? commentLists.size() + 1 : 0);
    }


    public class CommentListViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imageProPic;
        CustomTextView textUserName;
        public CustomTextView textUserComment;
        ConstraintLayout consComItem;
        ConstraintLayout consCommentList;
        ImageView imageDelete;
        ImageView imageEdit;
        CustomTextView textUserDestination;

        public CommentListViewHolder(View itemView) {
            super(itemView);
            imageProPic = (CircleImageView) itemView.findViewById(R.id.image_pro_pic);
            imageDelete = (ImageView) itemView.findViewById(R.id.image_delete);
            imageEdit = (ImageView) itemView.findViewById(R.id.image_edit);
            textUserName = (CustomTextView) itemView.findViewById(R.id.text_user_name);
            textUserComment = (CustomTextView) itemView.findViewById(R.id.text_user_comment);
            consComItem = (ConstraintLayout) itemView.findViewById(R.id.cons_com_item);
            consCommentList = (ConstraintLayout) itemView.findViewById(R.id.cons_comment_list);
            textUserDestination = (CustomTextView) itemView.findViewById(R.id.text_user_destination);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        ImageView imageProPic;
        ImageView imageSend;
        EditText editComment;
        View footerView;

        public FooterViewHolder(View itemView) {
            super(itemView);
            imageProPic = (ImageView) itemView.findViewById(R.id.image_pro_pic);
            imageSend = (ImageView) itemView.findViewById(R.id.image_comment_send);
            editComment = (EditText) itemView.findViewById(R.id.edit_write_comment);
            footerView = (View) itemView.findViewById(R.id.view_bottom_1);
            editComment.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Log.d("MONDAY", "comment");
                    editComment.setFocusableInTouchMode(true);
                    editComment.requestFocus();

                    return false;
                }
            });
        }
    }

    private void updateComment(final int position, final String comment) {
        CommentEditParams commentEditParams = new CommentEditParams();
        commentEditParams.setComment(comment);
        commentEditParams.setCommentid(commentLists.get(position).getCommentID());
        commentEditParams.setTimelineid(commentLists.get(position).getTimeLineID());
        commentEditParams.setParentid(0);
        commentEditParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            if (activity instanceof BaseActivity)
                baseActivity.showProgress();
            else
                timeLineDetailPageActivity.showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.updateComment(headerMap, commentEditParams).enqueue(new Callback<CommentEditResponse>() {
                @Override
                public void onResponse(Call<CommentEditResponse> call, Response<CommentEditResponse> response) {
                    commentEditResponse = response.body();
                    if (activity instanceof BaseActivity)
                        baseActivity.hideProgress();
                    else
                        timeLineDetailPageActivity.hideProgress();
                    if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                        if (commentEditResponse.getStatuscode() == 0) {
                            if (commentFragment instanceof CommentFragment)
                                commentFragment.getCommentList();
                            else
                                commentLists.get(position).setComment(commentEditResponse.getResults().getComment());

                            notifyDataSetChanged();
                            Toast.makeText(context, "Updated Success", Toast.LENGTH_SHORT).show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, commentEditResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, commentEditResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(commentEditResponse.getSource(),
                                    commentEditResponse.getSourcedata()));
                            editor.commit();
                            updateComment(position, comment);
                        }
                    } else {
                        Toast.makeText(context, "Updated Failed", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommentEditResponse> call, Throwable t) {
                    if (activity instanceof BaseActivity)
                        baseActivity.hideProgress();
                    else
                        timeLineDetailPageActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteComment(final int position) {
        CommentEditParams commentEditParams = new CommentEditParams();
        commentEditParams.setCommentid(commentLists.get(position).getCommentID());
        commentEditParams.setTimelineid(commentLists.get(position).getTimeLineID());
        commentEditParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));

        if (Util.isNetworkAvailable()) {
            if (activity instanceof BaseActivity)
                baseActivity.showProgress();
            else
                timeLineDetailPageActivity.showProgress();

            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));

            dmkAPI.deleteComment(headerMap, commentEditParams).enqueue(new Callback<TimeLineDeleteResponse>() {
                @Override
                public void onResponse(Call<TimeLineDeleteResponse> call, Response<TimeLineDeleteResponse> response) {
                    timeLineDeleteResponse = response.body();
                    if (activity instanceof BaseActivity)
                        baseActivity.hideProgress();
                    else
                        timeLineDetailPageActivity.hideProgress();

                    if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                        if (timeLineDeleteResponse.getStatuscode() == 0) {
                            if (commentFragment instanceof CommentFragment)
                                commentFragment.getCommentList();
                            else
                                commentLists.remove(position);

                            notifyDataSetChanged();
                            Toast.makeText(context, "Deleted Success", Toast.LENGTH_SHORT).show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, timeLineDeleteResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, timeLineDeleteResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(timeLineDeleteResponse.getSource(),
                                    timeLineDeleteResponse.getSourcedata()));
                            editor.commit();
                            deleteComment(position);
                        }
                    } else {
                        Toast.makeText(context, "Deleted Failed", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TimeLineDeleteResponse> call, Throwable t) {
                    if (activity instanceof BaseActivity)
                        baseActivity.hideProgress();
                    else
                        timeLineDetailPageActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void postComment() {
        CommentPost commentPost = new CommentPost();
        commentPost.setComment(footerViewHolder.editComment.getText().toString());
        commentPost.setParentid(0);
        commentPost.setTimelineid(timeLineID);
        commentPost.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            if (activity instanceof BaseActivity)
                baseActivity.showProgress();
            else
                timeLineDetailPageActivity.showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.postComment(headerMap, commentPost).enqueue(new Callback<CommentPostResponse>() {
                @Override
                public void onResponse(Call<CommentPostResponse> call, Response<CommentPostResponse> response) {
                    if (activity instanceof BaseActivity)
                        baseActivity.hideProgress();
                    else
                        timeLineDetailPageActivity.hideProgress();
                    commentPostResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (commentPostResponse.getStatuscode() == 0) {
                            commentLists.add(new CommentList(sharedPreferences.getString(DmkConstants.FIRSTNAME, "")
                                    + " " + sharedPreferences.getString(DmkConstants.LASTNAME, ""),
                                    sharedPreferences.getString(DmkConstants.PROFILE_PICTURE, ""),
                                    commentPostResponse.getResults().getComment(),
                                    commentPostResponse.getResults().getId(),
                                    Integer.parseInt(commentPostResponse.getResults().getTimeLineID()),
                                    commentPostResponse.getResults().getUpdated_at(),
                                    Integer.parseInt(commentPostResponse.getResults().getPostedUserID()),
                                    sharedPreferences.getString(DmkConstants.DESGINATION, ""), sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0), "", "", "", "",
                                    "", ""));
                            notifyDataSetChanged();
                            footerViewHolder.editComment.setText("");
                            footerViewHolder.editComment.setFocusableInTouchMode(true);
                            footerViewHolder.editComment.requestFocus();
                            Toast.makeText(context, "Post Success", Toast.LENGTH_SHORT).show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, commentPostResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, commentPostResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(commentPostResponse.getSource(),
                                    commentPostResponse.getSourcedata()));
                            editor.commit();
                            postComment();
                        }
                    } else {
                        Toast.makeText(context, "Post Failed", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                    if (activity instanceof BaseActivity)
                        baseActivity.hideProgress();
                    else
                        timeLineDetailPageActivity.hideProgress();
                    Toast.makeText(context, "Post Failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    public String getEncodedHeader(String sourceResponse, String sourceDataResponse) {

        byte[] sourceBase64 = Base64.decode(sourceResponse, Base64.NO_WRAP);
        byte[] sourceDataBase64 = Base64.decode(sourceDataResponse, Base64.NO_WRAP);
        String source = new String(sourceBase64, StandardCharsets.UTF_8);
        String sourceData = new String(sourceDataBase64, StandardCharsets.UTF_8);
        String data = source.substring(0, 12) + sourceData.substring(0, 8);
        String key = source.substring(12, 19) + sourceData.substring(8, 13);
        String iv = source.substring(19, 28) + sourceData.substring(13, 20);
        String encrpytData = Java_AES_Cipher.encrypt(key, iv, data);
        byte[] dataBase64Encode = encrpytData.getBytes(StandardCharsets.UTF_8);
        String base64 = Base64.encodeToString(dataBase64Encode, Base64.NO_WRAP);
        return base64;
    }

}
