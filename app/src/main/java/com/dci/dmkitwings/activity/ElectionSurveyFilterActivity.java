package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.BoothAdapter;
import com.dci.dmkitwings.adapter.ConstituencyAdapter;
import com.dci.dmkitwings.adapter.DistrictAdpater;
import com.dci.dmkitwings.adapter.DivisionAdapter;
import com.dci.dmkitwings.adapter.MunicipalityAdapter;
import com.dci.dmkitwings.adapter.PanchayatUnionListResponse;
import com.dci.dmkitwings.adapter.PanchayatUnionResultsItem;
import com.dci.dmkitwings.adapter.PanchyatUnionAdapter;
import com.dci.dmkitwings.adapter.PartAdapter;
import com.dci.dmkitwings.adapter.PartyDistrictAdapter;
import com.dci.dmkitwings.adapter.TownPanchayatAdapter;
import com.dci.dmkitwings.adapter.VillagePanchayatAdapter;
import com.dci.dmkitwings.adapter.WardAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.BoothListInputParam;
import com.dci.dmkitwings.model.BoothListResponse;
import com.dci.dmkitwings.model.BoothListResultsItem;
import com.dci.dmkitwings.model.ConsituencyList;
import com.dci.dmkitwings.model.ConsituencyListResponse;
import com.dci.dmkitwings.model.ConstituencyListResultsItem;
import com.dci.dmkitwings.model.DistrictList;
import com.dci.dmkitwings.model.DistrictListResponse;
import com.dci.dmkitwings.model.DistrictsItem;
import com.dci.dmkitwings.model.DivisionListResponse;
import com.dci.dmkitwings.model.DivisionListResultsItem;
import com.dci.dmkitwings.model.GetChatUserListResponse;
import com.dci.dmkitwings.model.GetChatUserListResultsItem;
import com.dci.dmkitwings.model.GetSurveyUserListParam;
import com.dci.dmkitwings.model.MunicipalityListResponse;
import com.dci.dmkitwings.model.MunicipalityListResultsItem;
import com.dci.dmkitwings.model.PartList;
import com.dci.dmkitwings.model.PartListResponse;
import com.dci.dmkitwings.model.PartListResultsItem;
import com.dci.dmkitwings.model.PartyDistrictInputParam;
import com.dci.dmkitwings.model.PartyDistrictResponse;
import com.dci.dmkitwings.model.PartyDistrictResultsItem;
import com.dci.dmkitwings.model.TownPanchayatListResponse;
import com.dci.dmkitwings.model.TownPanchayatResultsItem;
import com.dci.dmkitwings.model.VattamListResponse;
import com.dci.dmkitwings.model.VattamResultsItem;
import com.dci.dmkitwings.model.VattamWardList;
import com.dci.dmkitwings.model.VillagePanchayatListResponse;
import com.dci.dmkitwings.model.VillagePanchayatResultsItem;
import com.dci.dmkitwings.model.WardListResponse;
import com.dci.dmkitwings.model.WardListResultsItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomButton;
import com.dci.dmkitwings.view.CustomRadioButton;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ElectionSurveyFilterActivity extends BaseActivity {

    @BindView(R.id.text_label_post_to)
    CustomTextView textLabelPostTo;
    @BindView(R.id.radio_all)
    CustomRadioButton radioAll;
    @BindView(R.id.radio_selected_district)
    CustomRadioButton radioSelectedDistrict;
    @BindView(R.id.radio_group_type)
    RadioGroup radioGroupType;
    @BindView(R.id.view_bottom_3)
    View viewBottom3;
    @BindView(R.id.text_label_district)
    CustomTextView textLabelDistrict;
    @BindView(R.id.spinner_district)
    Spinner spinnerDistrict;
    @BindView(R.id.view_bottom_4)
    View viewBottom4;
    @BindView(R.id.text_label_party_district)
    CustomTextView textLabelPartyDistrict;
    @BindView(R.id.spinner_party_district)
    Spinner spinnerPartyDistrict;
    @BindView(R.id.view_bottom_17)
    View viewBottom17;
    @BindView(R.id.text_label_constituency)
    CustomTextView textLabelConstituency;
    @BindView(R.id.spinner_constituency)
    Spinner spinnerConstituency;
    @BindView(R.id.view_bottom_5)
    View viewBottom5;
    @BindView(R.id.text_label_division)
    CustomTextView textLabelDivision;
    @BindView(R.id.spinner_division)
    Spinner spinnerDivision;
    @BindView(R.id.view_bottom_division)
    View viewBottom9;
    @BindView(R.id.text_label_part)
    CustomTextView textLabelPart;
    @BindView(R.id.spinner_part)
    Spinner spinnerPart;
    @BindView(R.id.view_bottom_10)
    View viewBottom10;
    @BindView(R.id.text_label_village_panchayat)
    CustomTextView textLabelVillagePanchayat;
    @BindView(R.id.spinner_village_panchayat)
    Spinner spinnerVillagePanchayat;
    @BindView(R.id.view_bottom_6)
    View viewBottom6;
    @BindView(R.id.text_label_ward)
    CustomTextView textLabelWard;
    @BindView(R.id.spinner_ward)
    Spinner spinnerWard;
    @BindView(R.id.view_bottom_12)
    View viewBottom12;
    @BindView(R.id.text_label_Booth)
    CustomTextView textLabelBooth;
    @BindView(R.id.spinner_booth)
    Spinner spinnerBooth;
    @BindView(R.id.cons_chat_user_list)
    ConstraintLayout consChatUserList;
    Unbinder unbinder;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    DmkAPI dmkAPI;
    @BindView(R.id.text_label_union_type)
    CustomTextView textLabelUnionType;
    @BindView(R.id.spinner_union_type)
    Spinner spinnerUnionType;
    @BindView(R.id.view_bottom_union_type)
    View viewBottomUnionType;

    ConsituencyListResponse consituencyListResponse;
    DivisionListResponse divisionListResponse;
    DistrictListResponse districtListResponse;
    PartyDistrictResponse partyDistrictResponse;
    PartListResponse partListResponse;
    PanchayatUnionListResponse panchayatUnionListResponse;
    TownPanchayatListResponse townPanchayatListResponse;
    VattamListResponse vattamListResponse;
    WardListResponse wardListResponse;
    VillagePanchayatListResponse villagePanchayatListResponse;
    MunicipalityListResponse municipalityListResponse;
    ArrayList<ConstituencyListResultsItem> constituencyList;
    ArrayList<DivisionListResultsItem> divisionlist;
    ArrayList<DistrictsItem> districtList;
    ArrayList<PartListResultsItem> partList;
    ArrayList<PanchayatUnionResultsItem> panchayatList;
    ArrayList<TownPanchayatResultsItem> townPanchayatList;
    ArrayList<VattamResultsItem> vattamList;
    ArrayList<VillagePanchayatResultsItem> villagePanchayatResultsItemArrayList;
    ArrayList<String> partyRoleList;
    ArrayList<WardListResultsItem> wardList;
    ArrayList<MunicipalityListResultsItem> municipalityList;
    List<PartyDistrictResultsItem> partyDistrictResultsItems;
    @BindView(R.id.button_submit)
    CustomButton buttonSubmit;
    @BindView(R.id.cons_user_list_item)
    ConstraintLayout consUserListItem;
    @BindView(R.id.scroll_chat_user_filter)
    ScrollView scrollChatUserFilter;
    private int districtID = -1;
    private int divisionID = -1;
    private int partyDistrictId;
    private int constituencyID;
    private int partID;
    BoothListResponse boothListResponse;
    ArrayList<BoothListResultsItem> boothList;
    private int villageID;
    private int wardID;
    private int boothID;
    private String divisionName;
    GetChatUserListResponse getChatUserListResponse;
    ArrayList<GetChatUserListResultsItem> arrayListChatUser;
    HomeMessageActivity homeMessageActivity;
    public static GetSurveyUserListParam getSurveyUserListParam;
    private int unionType = 0;
    int designationID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_election_survey_user_filter, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        frameLayoutNotification.setVisibility(View.GONE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //toolbarHome.setVisibility(View.GONE);
        textTitle.setText(R.string.election);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //homeMessageActivity = (HomeMessageActivity) getApplicationContext();
        getSurveyUserListParam = new GetSurveyUserListParam();
        partyRoleList = new ArrayList<String>();
        constituencyList = new ArrayList<ConstituencyListResultsItem>();
        divisionlist = new ArrayList<DivisionListResultsItem>();
        districtList = new ArrayList<DistrictsItem>();
        partList = new ArrayList<PartListResultsItem>();
        wardList = new ArrayList<WardListResultsItem>();
        vattamList = new ArrayList<VattamResultsItem>();
        panchayatList = new ArrayList<PanchayatUnionResultsItem>();
        townPanchayatList = new ArrayList<TownPanchayatResultsItem>();
        municipalityList = new ArrayList<MunicipalityListResultsItem>();
        villagePanchayatResultsItemArrayList = new ArrayList<VillagePanchayatResultsItem>();
        partyDistrictResultsItems = new ArrayList<PartyDistrictResultsItem>();
        boothList = new ArrayList<BoothListResultsItem>();
        arrayListChatUser = new ArrayList<GetChatUserListResultsItem>();

        partyDistrictResultsItems.add(new PartyDistrictResultsItem(getString(R.string.select_party_district), 0));
        PartyDistrictAdapter partyDistrictAdapter = new PartyDistrictAdapter(partyDistrictResultsItems, getApplicationContext());
        spinnerPartyDistrict.setAdapter(partyDistrictAdapter);
        constituencyList.add(0, new ConstituencyListResultsItem(getString(R.string.select_constituency), 0));
        ConstituencyAdapter constituencyAdapter = new ConstituencyAdapter(constituencyList, getApplicationContext());
        spinnerConstituency.setAdapter(constituencyAdapter);
        divisionlist.add(0, new DivisionListResultsItem(getString(R.string.select_division), 0));
        DivisionAdapter divisionAdapter = new DivisionAdapter(divisionlist, getApplicationContext());
        spinnerDivision.setAdapter(divisionAdapter);

        partList.add(0, new PartListResultsItem(getString(R.string.select_part), 0));
        PartAdapter partAdapter = new PartAdapter(partList, getApplicationContext());
        spinnerPart.setAdapter(partAdapter);

//        villagePanchayatResultsItemArrayList.add(0, new VillagePanchayatResultsItem(0, getString(R.string.select_village_panchayat)));
//        VattamAdapter vattamAdapter = new VattamAdapter(vattamList, getApplicationContext());
//        spinnerVillagePanchayat.setAdapter(vattamAdapter);

        wardList.add(0, new WardListResultsItem(getString(R.string.select_ward), 0));
        WardAdapter wardAdapter = new WardAdapter(wardList, getApplicationContext());
        spinnerWard.setAdapter(wardAdapter);

        districtList.add(0, new DistrictsItem(getString(R.string.select_district), 0));
        DistrictAdpater districtAdpater = new DistrictAdpater(districtList, getApplicationContext());
        spinnerDistrict.setAdapter(districtAdpater);

        boothList.add(0, new BoothListResultsItem(getString(R.string.select_booth), 0));
        BoothAdapter boothAdapter = new BoothAdapter(boothList, getApplicationContext());
        spinnerBooth.setAdapter(boothAdapter);
        panchayatList.add(0, new PanchayatUnionResultsItem(getString(R.string.select_panchayat_union), 0));
        PanchyatUnionAdapter panchyatUnionAdapter = new PanchyatUnionAdapter(panchayatList, this);
        spinnerPart.setAdapter(panchyatUnionAdapter);

        final ArrayAdapter<String> adapterUnionType = new
                ArrayAdapter<String>(this, R.layout.custom_spinner_item,
                getResources().getStringArray(R.array.panchayat_union_list));
        spinnerUnionType.setAdapter(adapterUnionType);

        designationID = sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0);

        switch (designationID) {
            case 1:
                //                செயலாளர்
//                துணை செயலாளர்
//                மண்டல ஒருங்கிணைப்பா
                // radioGroupType.setVisibility(View.VISIBLE);
                //viewBottom4.setVisibility(View.GONE);
                spinnerDistrict.setVisibility(View.VISIBLE);
                textLabelDistrict.setVisibility(View.VISIBLE);
                viewBottom4.setVisibility(View.VISIBLE);

                spinnerPartyDistrict.setVisibility(View.VISIBLE);
                textLabelPartyDistrict.setVisibility(View.VISIBLE);
                viewBottom17.setVisibility(View.VISIBLE);

                spinnerConstituency.setVisibility(View.VISIBLE);
                textLabelConstituency.setVisibility(View.VISIBLE);
                viewBottom5.setVisibility(View.VISIBLE);

                spinnerDivision.setVisibility(View.VISIBLE);
                textLabelDivision.setVisibility(View.VISIBLE);
                viewBottom9.setVisibility(View.VISIBLE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.VISIBLE);
                textLabelPart.setVisibility(View.VISIBLE);
                viewBottom10.setVisibility(View.VISIBLE);

                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                viewBottom6.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.VISIBLE);
                textLabelWard.setVisibility(View.VISIBLE);
                viewBottom12.setVisibility(View.VISIBLE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);
                // viewBottomBooth.setVisibility(View.INVISIBLE);


                districtID = sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0);
                partyDistrictId = sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0);
                getDistrictList();
                //getConsituencyList(districtID);
                break;
            case 2:

                //                மாவட்ட ஒருங்கிணைப்பாளர்  district

                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.VISIBLE);
                textLabelConstituency.setVisibility(View.VISIBLE);
                viewBottom5.setVisibility(View.VISIBLE);

                spinnerDivision.setVisibility(View.VISIBLE);
                textLabelDivision.setVisibility(View.VISIBLE);
                viewBottom9.setVisibility(View.VISIBLE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.VISIBLE);
                textLabelPart.setVisibility(View.VISIBLE);
                viewBottom10.setVisibility(View.VISIBLE);

                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                viewBottom6.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottom12.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                districtID = sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0);
                partyDistrictId = sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0);
                getConsituencyList(partyDistrictId);
                break;
            case 3:
//                தொகுதி ஒருங்கிணைப்பாளர்  constituency
//                Hide till constituency
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottom5.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.VISIBLE);
                textLabelDivision.setVisibility(View.VISIBLE);
                viewBottom9.setVisibility(View.VISIBLE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.VISIBLE);
                textLabelPart.setVisibility(View.VISIBLE);
                viewBottom10.setVisibility(View.VISIBLE);

                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                viewBottom6.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottom12.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                constituencyID = sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0);
                districtID = sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0);
                getDivisionList();
                break;

            case 4:
//                Corporation
//                பகுதி ஒருங்கிணைப்பாளர்  part
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottom5.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottom9.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottom10.setVisibility(View.GONE);


                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                viewBottom6.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.VISIBLE);
                textLabelWard.setVisibility(View.VISIBLE);
                viewBottom12.setVisibility(View.VISIBLE);


                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);


                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                getWardList(divisionID, partID);
                break;
            case 8:
//                Municipality
//                நகராட்சி ஒருங்கிணைப்பாளர் municipality
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottom5.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottom9.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottom10.setVisibility(View.GONE);


                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                viewBottom6.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.VISIBLE);
                textLabelWard.setVisibility(View.VISIBLE);
                viewBottom12.setVisibility(View.VISIBLE);


                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                getWardList(divisionID, partID);
                break;

            case 12:
//                Town
//                பேரூராட்சி ஒருங்கிணைப்பாளர் township
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottom5.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottom9.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottom10.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.VISIBLE);
                textLabelWard.setVisibility(View.VISIBLE);
                viewBottom12.setVisibility(View.VISIBLE);

                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setText(getString(R.string.town_pachayat));

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);

                getWardList(divisionID, partID);


                break;
            case 16:
//                ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottom5.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottom9.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.VISIBLE);
                textLabelUnionType.setVisibility(View.VISIBLE);
                viewBottomUnionType.setVisibility(View.VISIBLE);

                spinnerPart.setVisibility(View.VISIBLE);
                textLabelPart.setVisibility(View.VISIBLE);
                viewBottom10.setVisibility(View.VISIBLE);

                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                viewBottom6.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottom12.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                constituencyID = sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0);
                districtID = sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0);
                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);

                break;

            case 5:
                //            Corporation
//                வட்டம் ஒருங்கிணைப்பாளர்  vattam
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottom5.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottom9.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottom10.setVisibility(View.GONE);

                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                viewBottom6.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottom12.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);


                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0);
                wardID = sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                villageID = sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0);


                getBoothList(divisionID, partID, wardID, villageID, unionType);

                break;
            case 9:
//                Municipality
//                வட்டம் ஒருங்கிணைப்பாளர்  vattam
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottom5.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottom9.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottom10.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                viewBottom6.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottom12.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0);
                wardID = sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                villageID = sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0);

                getBoothList(divisionID, partID, wardID, villageID, unionType);

                break;
            case 13:
//                Town
//                வட்டம் ஒருங்கிணைப்பாளர்  vattam
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottom5.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottom9.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottom10.setVisibility(View.GONE);

                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                viewBottom6.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottom12.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);

                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0);
                wardID = sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                villageID = sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0);

                getBoothList(divisionID, partID, wardID, villageID, unionType);

//                Panchayat union
                break;
            case 17:
//                ஊராட்சி ஒருங்கிணைப்பாளர் villagepanchayat
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottom5.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottom9.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottom10.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottom12.setVisibility(View.GONE);

                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setText(getString(R.string.village_panchayat));

                spinnerBooth.setVisibility(View.VISIBLE);
                textLabelBooth.setVisibility(View.VISIBLE);


                divisionID = sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0);
                partID = sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0);
                wardID = sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0);
                unionType = sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0);
                villageID = sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0);

                getBoothList(divisionID, partID, wardID, villageID, unionType);

                break;

//            Corporation
            case 7:
//                Municipality
            case 11:
//                Town
            case 15:
//                Panchayat union
            case 19:
//                வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                spinnerDistrict.setVisibility(View.GONE);
                textLabelDistrict.setVisibility(View.GONE);
                viewBottom4.setVisibility(View.GONE);

                spinnerPartyDistrict.setVisibility(View.GONE);
                textLabelPartyDistrict.setVisibility(View.GONE);
                viewBottom17.setVisibility(View.GONE);

                spinnerConstituency.setVisibility(View.GONE);
                textLabelConstituency.setVisibility(View.GONE);
                viewBottom5.setVisibility(View.GONE);

                spinnerDivision.setVisibility(View.GONE);
                textLabelDivision.setVisibility(View.GONE);
                viewBottom9.setVisibility(View.GONE);

                spinnerUnionType.setVisibility(View.GONE);
                textLabelUnionType.setVisibility(View.GONE);
                viewBottomUnionType.setVisibility(View.GONE);

                spinnerPart.setVisibility(View.GONE);
                textLabelPart.setVisibility(View.GONE);
                viewBottom10.setVisibility(View.GONE);

                spinnerVillagePanchayat.setVisibility(View.GONE);
                textLabelVillagePanchayat.setVisibility(View.GONE);
                viewBottom6.setVisibility(View.GONE);

                spinnerWard.setVisibility(View.GONE);
                textLabelWard.setVisibility(View.GONE);
                viewBottom12.setVisibility(View.GONE);

                spinnerBooth.setVisibility(View.GONE);
                textLabelBooth.setVisibility(View.GONE);

                break;
        }

        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                DistrictsItem districtsItem = districtList.get(position);
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                if (districtsItem.getId() != 0) {
                    districtID = districtsItem.getId();
                    getPartyDistrictlist(districtID);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerPartyDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                PartyDistrictResultsItem partyDistrictResultsItem = partyDistrictResultsItems.get(position);
                partyDistrictId = partyDistrictResultsItem.getId();
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                if (partyDistrictId != 0) {
                    getConsituencyList(partyDistrictId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerConstituency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                ConstituencyListResultsItem constituencyListResultsItem = constituencyList.get(position);
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                if (constituencyListResultsItem.getId() != 0) {
                    constituencyID = constituencyListResultsItem.getId();
                    getDivisionList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerDivision.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                DivisionListResultsItem divisionListResultsItem = divisionlist.get(position);
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                switch (divisionListResultsItem.getId()) {
                    case 0:
                        break;
                    case 1:
//                        Corporation
                        spinnerUnionType.setVisibility(View.GONE);
                        textLabelUnionType.setVisibility(View.GONE);
                        viewBottomUnionType.setVisibility(View.GONE);
                        unionType = 0;
                        divisionID = divisionListResultsItem.getId();
                        divisionName = divisionListResultsItem.getDivisionname();

                        if (constituencyID != 0 && districtID != 0 && divisionID != 0) {
                            getPartList(constituencyID, districtID, divisionID);
                        } else {

                            Toast.makeText(ElectionSurveyFilterActivity.this, getString(R.string.choose_district), Toast.LENGTH_SHORT).show();
                            spinnerDivision.setSelection(0);
                        }

                        break;
                    case 2:
//                        Municipality
                        divisionID = divisionListResultsItem.getId();
                        divisionName = divisionListResultsItem.getDivisionname();
                        unionType = 0;
                        spinnerUnionType.setVisibility(View.GONE);
                        textLabelUnionType.setVisibility(View.GONE);
                        viewBottomUnionType.setVisibility(View.GONE);
                        if (constituencyID != 0 && districtID != 0 && divisionID != 0) {
                            getMunicipalityList(constituencyID, districtID, divisionID);
                        } else {

                            Toast.makeText(ElectionSurveyFilterActivity.this, getString(R.string.choose_district), Toast.LENGTH_SHORT).show();
                            spinnerDivision.setSelection(0);
                        }

                        break;
                    case 3:
//                        PanchayatUnion
                        spinnerUnionType.setVisibility(View.VISIBLE);
                        textLabelUnionType.setVisibility(View.VISIBLE);
                        viewBottomUnionType.setVisibility(View.VISIBLE);
                        divisionID = divisionListResultsItem.getId();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerUnionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                int selectedTypePos = adapterView.getSelectedItemPosition();
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                if (selectedTypePos != 1) {
                    spinnerWard.setVisibility(View.GONE);
                    textLabelWard.setVisibility(View.GONE);
                    viewBottom12.setVisibility(View.GONE);
                }
                if (selectedTypePos != 0) {
                    if (constituencyID != 0 && districtID != 0 && divisionID != 0) {
                        //                1-  //TownPanchayat 2-//VillagePanchayat
                        unionType = selectedTypePos;
                        getPanchayatUnionList(constituencyID, districtID, divisionID, unionType);
                        ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    } else {
                        Toast.makeText(ElectionSurveyFilterActivity.this, getString(R.string.choose_district), Toast.LENGTH_SHORT).show();
                        spinnerDivision.setSelection(0);
                    }
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerPart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                switch (divisionID) {
                    case 1:
//                        Corporation
                        PartListResultsItem partListResultsItem = partList.get(position);
                        if (partListResultsItem.getId() != 0) {
                            partID = partListResultsItem.getId();
                            getWardList(divisionID, partID);
                            //((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        } else {
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom12.setVisibility(View.GONE);
                            //((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                        }
                        break;
                    case 2:
//                        Municipality
                        if (municipalityList.size() > 0) {
                            MunicipalityListResultsItem municipalityListResultsItem = municipalityList.get(position);
                            if (municipalityListResultsItem.getId() != 0) {
                                partID = municipalityListResultsItem.getId();
                                getWardList(divisionID, partID);
                                //((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                            } else {
                                spinnerWard.setVisibility(View.GONE);
                                textLabelWard.setVisibility(View.GONE);
                                viewBottom12.setVisibility(View.GONE);
                                //((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                            }
                        }
                        break;
                    case 3:
                        //PanchayatUnion
                        if (unionType == 1) {
                            //TownPanchayat

                            PanchayatUnionResultsItem panchayatUnionResultsItem = panchayatList.get(position);
                            if (panchayatUnionResultsItem.getId() != 0) {
                                partID = panchayatUnionResultsItem.getId();
                                getTownPanchayatList(constituencyID, districtID, partID, unionType);
                                //((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                            } else {
                                spinnerVillagePanchayat.setVisibility(View.GONE);
                                textLabelVillagePanchayat.setVisibility(View.GONE);
                                viewBottom6.setVisibility(View.GONE);
                                //((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                            }
                        } else {
                            //VillagePanchayat

                            PanchayatUnionResultsItem panchayatUnionResultsItem = panchayatList.get(position);
                            if (panchayatUnionResultsItem.getId() != 0) {
                                partID = panchayatUnionResultsItem.getId();
                                getVillagePanchayatList(constituencyID, partID, districtID, unionType);
                                // ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                            } else {
                                spinnerVillagePanchayat.setVisibility(View.GONE);
                                textLabelVillagePanchayat.setVisibility(View.GONE);
                                viewBottom6.setVisibility(View.GONE);
                                //((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_grey));
                            }
                        }


                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerVillagePanchayat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                switch (unionType) {
                    case 1:
                        //TownPanchayat
                        TownPanchayatResultsItem townPanchayatResultsItem = townPanchayatList.get(position);
                        if (townPanchayatResultsItem.getId() != 0) {
                            getWardList(divisionID, partID);


                        } else {
                            spinnerWard.setVisibility(View.GONE);
                            textLabelWard.setVisibility(View.GONE);
                            viewBottom12.setVisibility(View.GONE);

                        }
                        break;
                    case 2:
                        //Village panchayat
                        VillagePanchayatResultsItem villagePanchayatResultsItem = villagePanchayatResultsItemArrayList.get(position);
                        if (villagePanchayatResultsItem.getId() != 0) {
                            villageID = villagePanchayatResultsItem.getId();
                            getBoothList(divisionID, partID, wardID, villageID, unionType);
                        }

                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerWard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                WardListResultsItem wardListResultsItem = wardList.get(position);
                if (wardListResultsItem.getId() != 0) {
                    wardID = wardListResultsItem.getId();
                    getBoothList(divisionID, partID, wardID, villageID, unionType);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerBooth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getChildCount() > 0 && adapterView != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                BoothListResultsItem boothResultsItem = boothList.get(position);
                if (boothResultsItem.getId() != 0) {
                    boothID = boothResultsItem.getId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }


    public void getDistrictList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            districtList.clear();
            districtList.add(0, new DistrictsItem(getString(R.string.select_district), 0));
            DistrictList districtListparam = new DistrictList();
            districtListparam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getDistrictList(headerMap, districtListparam).enqueue(new Callback<DistrictListResponse>() {
                @Override
                public void onResponse(Call<DistrictListResponse> call, Response<DistrictListResponse> response) {
                    hideProgress();
                    districtListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (districtListResponse.getResults() != null) {
                            for (int i = 0; i < districtListResponse.getResults().getDistricts().size(); i++) {
                                districtList.add(new DistrictsItem(districtListResponse.getResults().getDistricts().get(i).getDistrictName(),
                                        districtListResponse.getResults().getDistricts().get(i).getId()));
                            }
                            DistrictAdpater adapterdivisionList = new DistrictAdpater(districtList, getApplicationContext());
                            spinnerDistrict.setAdapter(adapterdivisionList);
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.no_it_district), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), districtListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DistrictListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), getString(R.string.no_it_district), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    private void getPartyDistrictlist(int districtID) {
        PartyDistrictInputParam partyDistrictInputParam = new PartyDistrictInputParam();
        partyDistrictInputParam.setDistrictid(districtID);
        if (Util.isNetworkAvailable()) {
            showProgress();
            partyDistrictResultsItems.clear();
            partyDistrictResultsItems.add(new PartyDistrictResultsItem(getString(R.string.select_party_district), 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPartyDistrictList(headerMap,partyDistrictInputParam).enqueue(new Callback<PartyDistrictResponse>() {
                @Override
                public void onResponse(Call<PartyDistrictResponse> call, Response<PartyDistrictResponse> response) {
                    hideProgress();
                    partyDistrictResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (partyDistrictResponse.getResults() != null) {
                            for (int i = 0; i < partyDistrictResponse.getResults().size(); i++) {
                                partyDistrictResultsItems.add(new PartyDistrictResultsItem(
                                        partyDistrictResponse.getResults().get(i).getDistrictName(),
                                        partyDistrictResponse.getResults().get(i).getId()));
                            }
                            PartyDistrictAdapter partyDistrictAdapter = new PartyDistrictAdapter(
                                    partyDistrictResultsItems, getApplicationContext());
                            spinnerPartyDistrict.setAdapter(partyDistrictAdapter);
                        } else {
                            Toast.makeText(getApplicationContext(), partyDistrictResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_it_party_district), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<PartyDistrictResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getConsituencyList(int districtID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            constituencyList.clear();
            constituencyList.add(0, new ConstituencyListResultsItem(getString(R.string.select_constituency), 0));
            ConsituencyList consituencyList = new ConsituencyList();
            consituencyList.setDistrictID(districtID);
            consituencyList.setType(2);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getConstituencyList(headerMap, consituencyList).enqueue(new Callback<ConsituencyListResponse>() {
                @Override
                public void onResponse(Call<ConsituencyListResponse> call, Response<ConsituencyListResponse> response) {
                    hideProgress();
                    consituencyListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200
                            && consituencyListResponse != null) {
                        if (consituencyListResponse.getResults() != null) {
                            for (int i = 0; i < consituencyListResponse.getResults().size(); i++) {
                                constituencyList.add(new ConstituencyListResultsItem(
                                        consituencyListResponse.getResults().get(i).getConstituencyName() != null ?
                                                consituencyListResponse.getResults().get(i).getConstituencyName() : "",
                                        consituencyListResponse.getResults().get(i).getId() != 0 ?
                                                consituencyListResponse.getResults().get(i).getId() : 0));
                            }
                            ConstituencyAdapter adapterConstituencyList = new ConstituencyAdapter(constituencyList, getApplicationContext());
                            spinnerConstituency.setAdapter(adapterConstituencyList);
                        } else {
                            Toast.makeText(getApplicationContext(), consituencyListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_it_constituency), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ConsituencyListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), getString(R.string.no_it_constituency), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    public void getDivisionList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            divisionlist.clear();
            divisionlist.add(0, new DivisionListResultsItem(getString(R.string.select_division), 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getDivisionList(headerMap).enqueue(new Callback<DivisionListResponse>() {
                @Override
                public void onResponse(Call<DivisionListResponse> call, Response<DivisionListResponse> response) {
                    hideProgress();
                    divisionListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200
                            && divisionListResponse != null) {
                        if (divisionListResponse.getResults() != null) {
                            for (int i = 0; i < divisionListResponse.getResults().size(); i++) {
                                divisionlist.add(new DivisionListResultsItem(
                                        divisionListResponse.getResults().get(i).getDivisionname() != null ?
                                                divisionListResponse.getResults().get(i).getDivisionname() : "",
                                        divisionListResponse.getResults().get(i).getId() != 0 ?
                                                divisionListResponse.getResults().get(i).getId() : 0));
                            }
                            DivisionAdapter adapterdivisionList = new DivisionAdapter(divisionlist, getApplicationContext());
                            spinnerDivision.setAdapter(adapterdivisionList);
                        } else {
                            Toast.makeText(getApplicationContext(), districtListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_it_division), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DivisionListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), getString(R.string.no_it_division), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getPartList(int constituencyID, int districtID, int divisionID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            partList.clear();
            partList.add(0, new PartListResultsItem(getString(R.string.select_part), 0));
            PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setDivisionID(divisionID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPartList(headerMap, partListParam).enqueue(new Callback<PartListResponse>() {
                @Override
                public void onResponse(Call<PartListResponse> call, Response<PartListResponse> response) {
                    hideProgress();
                    partListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && partListResponse != null) {
                        if (partListResponse.getResults() != null) {
                            for (int i = 0; i < partListResponse.getResults().size(); i++) {
                                partList.add(new PartListResultsItem(
                                        partListResponse.getResults().get(i).getPartname() != null ?
                                                partListResponse.getResults().get(i).getPartname() : "",
                                        partListResponse.getResults().get(i).getId() != 0 ?
                                                partListResponse.getResults().get(i).getId() : 0));
                            }
                            PartAdapter partAdapter = new PartAdapter(partList, getApplicationContext());
                            spinnerPart.setAdapter(partAdapter);
                        } else {
                            Toast.makeText(getApplicationContext(), partListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_it_part), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PartListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), getString(R.string.no_it_part), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    public void getMunicipalityList(int constituencyID, int districtID, int divisionID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            municipalityList.clear();
            municipalityList.add(0, new MunicipalityListResultsItem(0, getString(R.string.select_town_municipality)));
            textLabelPart.setText(getString(R.string.municipallity));
            PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setDivisionID(divisionID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getMunicipalitytList(headerMap, partListParam).enqueue(new Callback<MunicipalityListResponse>() {
                @Override
                public void onResponse(Call<MunicipalityListResponse> call, Response<MunicipalityListResponse> response) {
                    hideProgress();
                    municipalityListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && municipalityListResponse != null) {
                        if (municipalityListResponse.getResults() != null) {
                            for (int i = 0; i < municipalityListResponse.getResults().size(); i++) {
                                municipalityList.add(new MunicipalityListResultsItem(
                                        municipalityListResponse.getResults().get(i).getId() != 0 ?
                                                municipalityListResponse.getResults().get(i).getId() : 0,
                                        municipalityListResponse.getResults().get(i).getMunicipalityname() != null ?
                                                municipalityListResponse.getResults().get(i).getMunicipalityname() : ""));
                            }
                            MunicipalityAdapter partAdapter = new MunicipalityAdapter(municipalityList, getApplicationContext());
                            spinnerPart.setAdapter(partAdapter);
                        } else {
                            Toast.makeText(getApplicationContext(), municipalityListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_it_municipality), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<MunicipalityListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), getString(R.string.no_it_municipality), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getPanchayatUnionList(int constituencyID, int districtID, int divisionID, int unionType) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            panchayatList.clear();
            panchayatList.add(0, new PanchayatUnionResultsItem(getString(R.string.select_panchayat_union), 0));
            textLabelPart.setText(R.string.pachayat_union);
            final PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setDivisionID(divisionID);
            partListParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getPanchayatUnionList(headerMap, partListParam).enqueue(new Callback<PanchayatUnionListResponse>() {
                @Override
                public void onResponse(Call<PanchayatUnionListResponse> call, Response<PanchayatUnionListResponse> response) {
                    hideProgress();
                    panchayatUnionListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && panchayatUnionListResponse != null) {
                        if (panchayatUnionListResponse.getResults() != null) {
                            for (int i = 0; i < panchayatUnionListResponse.getResults().size(); i++) {
                                panchayatList.add(new PanchayatUnionResultsItem(
                                        panchayatUnionListResponse.getResults().get(i).getVillagePanchayatName() != null
                                                ? panchayatUnionListResponse.getResults().get(i).getVillagePanchayatName() : "",
                                        panchayatUnionListResponse.getResults().get(i).getId() != 0 ?
                                                panchayatUnionListResponse.getResults().get(i).getId() : 0));
                            }
                            PanchyatUnionAdapter partAdapter = new PanchyatUnionAdapter(panchayatList, getApplicationContext());
                            spinnerPart.setAdapter(partAdapter);
                        } else {
                            Toast.makeText(getApplicationContext(), panchayatUnionListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_it_panchayat), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PanchayatUnionListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), getString(R.string.no_it_panchayat), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }


    }

    public void getTownPanchayatList(int constituencyID, int districtID, int partID, int unionType) {
        if (Util.isNetworkAvailable()) {
            showProgress();

            townPanchayatList.clear();
            townPanchayatList.add(0, new TownPanchayatResultsItem(getString(R.string.select_town_panchayat), 0));
            textLabelVillagePanchayat.setText(R.string.town_pachayat);
            final PartList partListParam = new PartList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setUnionID(partID);
            partListParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getTownPanchayatList(headerMap, partListParam).enqueue(new Callback<TownPanchayatListResponse>() {
                @Override
                public void onResponse(Call<TownPanchayatListResponse> call, Response<TownPanchayatListResponse> response) {
                    hideProgress();
                    townPanchayatListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && townPanchayatListResponse != null) {
                        if (townPanchayatListResponse.getResults() != null) {
                            if (townPanchayatListResponse.getResults().size() > 0) {

                                spinnerVillagePanchayat.setVisibility(View.VISIBLE);
                                textLabelVillagePanchayat.setVisibility(View.VISIBLE);
                                viewBottom6.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < townPanchayatListResponse.getResults().size(); i++) {
                                townPanchayatList.add(new TownPanchayatResultsItem(
                                        townPanchayatListResponse.getResults().get(i).getTownpanchayatname() != null ?
                                                townPanchayatListResponse.getResults().get(i).getTownpanchayatname() : "",
                                        townPanchayatListResponse.getResults().get(i).getId() != 0 ?
                                                townPanchayatListResponse.getResults().get(i).getId() : 0));
                            }
                            TownPanchayatAdapter partAdapter = new TownPanchayatAdapter(townPanchayatList, getApplicationContext());
                            spinnerVillagePanchayat.setAdapter(partAdapter);
                        } else {
                            Toast.makeText(getApplicationContext(), townPanchayatListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_it_town_panchayat), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TownPanchayatListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), getString(R.string.no_it_town_panchayat), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();

        }


    }


    public void getVillagePanchayatList(int constituencyID, int partID, int districtID, int unionType) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            villagePanchayatResultsItemArrayList.clear();
            villagePanchayatResultsItemArrayList.add(0, new VillagePanchayatResultsItem(getString(R.string.select_village_panchayat), 0));
            textLabelVillagePanchayat.setText(R.string.village_panchayat);
            final VattamWardList partListParam = new VattamWardList();
            partListParam.setConstituencyID(constituencyID);
            partListParam.setDistrictID(districtID);
            partListParam.setUnionID(partID);
            partListParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getVillagePanchayatList(headerMap, partListParam).enqueue(new Callback<VillagePanchayatListResponse>() {
                @Override
                public void onResponse(Call<VillagePanchayatListResponse> call, Response<VillagePanchayatListResponse> response) {
                    hideProgress();
                    villagePanchayatListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200
                            && villagePanchayatListResponse != null) {
                        if (villagePanchayatListResponse.getResults() != null) {
                            if (villagePanchayatListResponse.getResults().size() > 0) {
                                spinnerVillagePanchayat.setVisibility(View.VISIBLE);
                                textLabelVillagePanchayat.setVisibility(View.VISIBLE);
                                viewBottom6.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < villagePanchayatListResponse.getResults().size(); i++) {
                                villagePanchayatResultsItemArrayList.add(new VillagePanchayatResultsItem(
                                        villagePanchayatListResponse.getResults().get(i).getVillageName() != null ?
                                                villagePanchayatListResponse.getResults().get(i).getVillageName() : "",
                                        villagePanchayatListResponse.getResults().get(i).getId() != 0 ?
                                                villagePanchayatListResponse.getResults().get(i).getId() : 0));
                            }
                            VillagePanchayatAdapter vattamAdapter = new VillagePanchayatAdapter(villagePanchayatResultsItemArrayList, getApplicationContext());
                            spinnerVillagePanchayat.setAdapter(vattamAdapter);
                        } else {
                            Toast.makeText(getApplicationContext(), villagePanchayatListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_it_village_panchayat), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<VillagePanchayatListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), getString(R.string.no_it_village_panchayat), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public void getWardList(int divisionID, int partID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            wardList.clear();
            wardList.add(0, new WardListResultsItem(getString(R.string.select_ward), 0));
            final VattamWardList vattamWardListParam = new VattamWardList();
            vattamWardListParam.setDivisionID(divisionID);
            vattamWardListParam.setPartID(partID);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getWardList(headerMap, vattamWardListParam).enqueue(new Callback<WardListResponse>() {
                @Override
                public void onResponse(Call<WardListResponse> call, Response<WardListResponse> response) {
                    hideProgress();
                    wardListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200
                            && wardListResponse != null) {
                        if (wardListResponse.getResults() != null) {
                            if (wardListResponse.getResults().size() > 0) {
                                spinnerWard.setVisibility(View.VISIBLE);
                                textLabelWard.setVisibility(View.VISIBLE);
                                viewBottom12.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < wardListResponse.getResults().size(); i++) {
                                wardList.add(new WardListResultsItem(
                                        wardListResponse.getResults().get(i).getWardname() != null ?
                                                wardListResponse.getResults().get(i).getWardname() : "",
                                        wardListResponse.getResults().get(i).getId() != 0 ?
                                                wardListResponse.getResults().get(i).getId() : 0));
                            }
                            WardAdapter wardAdapter = new WardAdapter(wardList, getApplicationContext());
                            spinnerWard.setAdapter(wardAdapter);
                        } else {
                            Toast.makeText(getApplicationContext(), wardListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_it_ward), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<WardListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), getString(R.string.no_it_ward), Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    public void getBoothList(int divisionID, int partID, int wardID, int villageID, int unionType) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            boothList.clear();
            boothList.add(0, new BoothListResultsItem(getString(R.string.select_booth), 0));
            BoothListInputParam boothListInputParam = new BoothListInputParam();
            boothListInputParam.setDivisionID(divisionID);
            boothListInputParam.setPartID(partID);
            boothListInputParam.setWardID(wardID);
            boothListInputParam.setVillageID(villageID);
            boothListInputParam.setUnionType(unionType);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), getString(R.string.header_static_value));
            dmkAPI.getBoothList(headerMap, boothListInputParam).enqueue(new Callback<BoothListResponse>() {
                @Override
                public void onResponse(Call<BoothListResponse> call, Response<BoothListResponse> response) {
                    hideProgress();
                    boothListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200
                            && boothListResponse != null) {
//                        if (boothListResponse.getResults().size() > 0) {
//                            spinnerBooth.setVisibility(View.VISIBLE);
//                            textLabelBooth.setVisibility(View.VISIBLE);
//                            viewBottom18.setVisibility(View.VISIBLE);
//                        }
                        if (boothListResponse.getResults() != null) {
                            for (int i = 0; i < boothListResponse.getResults().size(); i++) {
                                boothList.add(new BoothListResultsItem(
                                        boothListResponse.getResults().get(i).getBoothname() != null ?
                                                boothListResponse.getResults().get(i).getBoothname() : "",
                                        boothListResponse.getResults().get(i).getId() != 0 ?
                                                boothListResponse.getResults().get(i).getId() : 0));
                            }
                            BoothAdapter boothAdapter = new BoothAdapter(boothList, getApplicationContext());
                            spinnerBooth.setAdapter(boothAdapter);
                        } else {
                            Toast.makeText(getApplicationContext(), boothListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
//                        spinnerBooth.setVisibility(View.GONE);
//                        textLabelBooth.setVisibility(View.GONE);
//                        viewBottom18.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), getString(R.string.no_booth_list_found), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<BoothListResponse> call, Throwable t) {
                    hideProgress();
//                    spinnerBooth.setVisibility(View.GONE);
//                    textLabelBooth.setVisibility(View.GONE);
//                    viewBottom18.setVisibility(View.GONE);
                }
            });


        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }


    private void getFilterUser() {
        if (Util.isNetworkAvailable()) {
            showProgress();

            getSurveyUserListParam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            switch (designationID) {
                case 1:
                    //செயலாளர்
//                துணை செயலாளர்
//                மண்டல ஒருங்கிணைப்பா
                    getSurveyUserListParam.setDistrictID(spinnerDistrict.isShown() ?
                            districtList.get(spinnerDistrict.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setPartydistrictid(spinnerPartyDistrict.isShown() ?
                            partyDistrictResultsItems.get(spinnerPartyDistrict.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setConstituencyID(spinnerConstituency.isShown() ?
                            constituencyList.get(spinnerConstituency.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setTypeid(spinnerDivision.isShown() ?
                            divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setWardid(spinnerWard.isShown() ?
                            wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setBoothID(spinnerBooth.isShown() ?
                            boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                    switch (spinnerDivision.isShown() ?
                            divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0) {
                        case 1:
                            getSurveyUserListParam.setPartid(spinnerPart.isShown() ?
                                    partList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                            break;
                        case 2:
                            getSurveyUserListParam.setMunicipalityid(spinnerPart.isShown() ?
                                    municipalityList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                            break;
                        case 3:
                            getSurveyUserListParam.setPanchayatunion(spinnerPart.isShown() ?
                                    panchayatList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                            if (unionType == 1) {
                                getSurveyUserListParam.setTownshipid(spinnerVillagePanchayat.isShown() ?
                                        townPanchayatList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                            } else {
                                getSurveyUserListParam.setVillageid(spinnerVillagePanchayat.isShown() ?
                                        villagePanchayatResultsItemArrayList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                            }
                            break;
                    }
                    break;
                case 2:
//
                    //                மாவட்ட ஒருங்கிணைப்பாளர்  district
                    getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                    getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                    getSurveyUserListParam.setConstituencyID(spinnerConstituency.isShown() ?
                            constituencyList.get(spinnerConstituency.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setTypeid(spinnerDivision.isShown() ?
                            divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setWardid(spinnerWard.isShown() ?
                            wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setBoothID(spinnerBooth.isShown() ?
                            boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                    switch (spinnerDivision.isShown() ?
                            divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0) {
                        case 1:
                            getSurveyUserListParam.setPartid(spinnerPart.isShown() ?
                                    partList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                            break;
                        case 2:
                            getSurveyUserListParam.setMunicipalityid(spinnerPart.isShown() ?
                                    municipalityList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                            break;
                        case 3:
                            getSurveyUserListParam.setPanchayatunion(spinnerPart.isShown() ?
                                    panchayatList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                            if (unionType == 1) {
                                getSurveyUserListParam.setTownshipid(spinnerVillagePanchayat.isShown() ?
                                        townPanchayatList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                            } else {
                                getSurveyUserListParam.setVillageid(spinnerVillagePanchayat.isShown() ?
                                        villagePanchayatResultsItemArrayList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                            }
                            break;
                    }

                    break;
                case 3:
//                தொகுதி ஒருங்கிணைப்பாளர்  constituency
                    getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                    getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                    getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                    getSurveyUserListParam.setTypeid(spinnerDivision.isShown() ?
                            divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setWardid(spinnerWard.isShown() ?
                            wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setBoothID(spinnerBooth.isShown() ?
                            boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                    switch (spinnerDivision.isShown() ?
                            divisionlist.get(spinnerDivision.getSelectedItemPosition()).getId() : 0) {
                        case 1:
                            getSurveyUserListParam.setPartid(spinnerPart.isShown() ?
                                    partList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                            break;
                        case 2:
                            getSurveyUserListParam.setMunicipalityid(spinnerPart.isShown() ?
                                    municipalityList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                            break;
                        case 3:
                            getSurveyUserListParam.setPanchayatunion(spinnerPart.isShown() ?
                                    panchayatList.get(spinnerPart.getSelectedItemPosition()).getId() : 0);
                            if (unionType == 1) {
                                getSurveyUserListParam.setTownshipid(spinnerVillagePanchayat.isShown() ?
                                        townPanchayatList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                            } else {
                                getSurveyUserListParam.setVillageid(spinnerVillagePanchayat.isShown() ?
                                        villagePanchayatResultsItemArrayList.get(spinnerVillagePanchayat
                                                .getSelectedItemPosition()).getId() : 0);
                            }
                            break;
                    }
                    break;
                case 4:
                case 8:
                case 12:
                    //Corporation
//                பகுதி ஒருங்கிணைப்பாளர்  part
                    getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                    getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                    getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                    getSurveyUserListParam.setTypeid(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                    getSurveyUserListParam.setWardid(spinnerWard.isShown() ?
                            wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setBoothID(spinnerBooth.isShown() ?
                            boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);
                    switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                        case 1:
                            getSurveyUserListParam.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                            break;
                        case 2:
                            getSurveyUserListParam.setMunicipalityid(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                            break;
                        case 3:
                            getSurveyUserListParam.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                            if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) == 1)
                                getSurveyUserListParam.setTownshipid(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                            else
                                getSurveyUserListParam.setVillageid(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));

                            break;
                    }
                    break;
                case 5:
                case 9:
                case 13:
//                வட்டம் ஒருங்கிணைப்பாளர்  vattam
//                Corporation
                    getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                    getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                    getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                    getSurveyUserListParam.setWardid(sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0));
                    getSurveyUserListParam.setTypeid(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));

                    switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                        case 1:
                            getSurveyUserListParam.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                            break;
                        case 2:
                            getSurveyUserListParam.setMunicipalityid(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                            break;
                        case 3:
                            getSurveyUserListParam.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                            if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) == 1)
                                getSurveyUserListParam.setTownshipid(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                            else
                                getSurveyUserListParam.setVillageid(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));
                            break;
                    }

                    getSurveyUserListParam.setBoothID(spinnerBooth.isShown() ?
                            boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);


                    break;
                case 7:
                case 11:
                case 15:
                case 19:
//                வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                    //Corporation
                    getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                    getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                    getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                    getSurveyUserListParam.setTypeid(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                    getSurveyUserListParam.setWardid(sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0));

                    switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                        case 1:
                            getSurveyUserListParam.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                            break;
                        case 2:
                            getSurveyUserListParam.setMunicipalityid(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                            break;
                        case 3:
                            getSurveyUserListParam.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                            if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) == 1)
                                getSurveyUserListParam.setTownshipid(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                            else
                                getSurveyUserListParam.setVillageid(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));
                            break;
                    }

                    getSurveyUserListParam.setBoothID(sharedPreferences.getInt(DmkConstants.USER_BOOTH_ID, 0));
                    break;
                case 16:
                    //PanchayatUnion
//                ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                    getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                    getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                    getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                    getSurveyUserListParam.setTypeid(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                    getSurveyUserListParam.setWardid(spinnerWard.isShown() ?
                            wardList.get(spinnerWard.getSelectedItemPosition()).getId() : 0);
                    getSurveyUserListParam.setBoothID(spinnerBooth.isShown() ?
                            boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);

                    switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                        case 1:
                            getSurveyUserListParam.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                            break;
                        case 2:
                            getSurveyUserListParam.setMunicipalityid(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                            break;
                        case 3:
                            getSurveyUserListParam.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                            if (unionType == 1) {
                                getSurveyUserListParam.setTownshipid(spinnerVillagePanchayat.isShown() ?
                                        townPanchayatList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                            } else {
                                getSurveyUserListParam.setVillageid(spinnerVillagePanchayat.isShown() ?
                                        villagePanchayatResultsItemArrayList.get(spinnerVillagePanchayat.getSelectedItemPosition()).getId() : 0);
                            }
                            break;
                    }
                    getSurveyUserListParam.setBoothID(spinnerBooth.isShown() ?
                            boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);

                    break;
                case 17:
//                ஊராட்சி ஒருங்கிணைப்பாளர் villagepanchayat
                    //Panchayat Union
                    getSurveyUserListParam.setDistrictID(sharedPreferences.getInt(DmkConstants.USER_DISTRICT_ID, 0));
                    getSurveyUserListParam.setConstituencyID(sharedPreferences.getInt(DmkConstants.USER_CONSTITUENCY_ID, 0));
                    getSurveyUserListParam.setPartydistrictid(sharedPreferences.getInt(DmkConstants.USER_PARTY_DISTRICT_ID, 0));
                    getSurveyUserListParam.setTypeid(sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0));
                    getSurveyUserListParam.setWardid(sharedPreferences.getInt(DmkConstants.USER_WARD_ID, 0));
                    switch (sharedPreferences.getInt(DmkConstants.USER_DIVISION_ID, 0)) {
                        case 1:
                            getSurveyUserListParam.setPartid(sharedPreferences.getInt(DmkConstants.USER_PART_ID, 0));
                            break;
                        case 2:
                            getSurveyUserListParam.setMunicipalityid(sharedPreferences.getInt(DmkConstants.USER_MUNICLIPALITY_ID, 0));
                            break;
                        case 3:
                            getSurveyUserListParam.setPanchayatunion(sharedPreferences.getInt(DmkConstants.USER_PANCHAYAT_UNION_ID, 0));
                            if (sharedPreferences.getInt(DmkConstants.USER_UNION_TYPE_ID, 0) != 1)
                                getSurveyUserListParam.setTownshipid(sharedPreferences.getInt(DmkConstants.USER_TOWNSHIP_ID, 0));
                            else
                                getSurveyUserListParam.setVillageid(sharedPreferences.getInt(DmkConstants.USER_VILLAGE_PANCHAYAT_ID, 0));
                            break;
                    }
                    getSurveyUserListParam.setBoothID(spinnerBooth.isShown() ?
                            boothList.get(spinnerBooth.getSelectedItemPosition()).getId() : 0);

                    break;
            }
        }
    }


    @OnClick(R.id.button_submit)
    public void onViewClicked() {
        getFilterUser();
        this.finish();

    }


}
