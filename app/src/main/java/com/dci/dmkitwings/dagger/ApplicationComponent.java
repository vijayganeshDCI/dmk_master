package com.dci.dmkitwings.dagger;


import android.content.SharedPreferences;

import com.dci.dmkitwings.activity.ArticleComposeActivity;
import com.dci.dmkitwings.activity.ArticleDetailActivity;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.ChatAct;
import com.dci.dmkitwings.activity.ChatActivity;
import com.dci.dmkitwings.activity.ComplaintActivity;
import com.dci.dmkitwings.activity.CompliantComposeActivity;
import com.dci.dmkitwings.activity.DigitalLibraryDetailsActivity;
import com.dci.dmkitwings.activity.ElectionPollActivity;
import com.dci.dmkitwings.activity.ElectionSurveyFilterActivity;
import com.dci.dmkitwings.activity.EventComposeActivity;
import com.dci.dmkitwings.activity.EventDetailsActivity;
import com.dci.dmkitwings.activity.FeedBackActivity;
import com.dci.dmkitwings.activity.HistoryActivity;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.activity.LoginActivity;
import com.dci.dmkitwings.activity.NewsComposeActivity;
import com.dci.dmkitwings.activity.NewsDetailaActivity;
import com.dci.dmkitwings.activity.NewsLinkActivity;
import com.dci.dmkitwings.activity.NotificationActivity;
import com.dci.dmkitwings.activity.PollListActivity;
import com.dci.dmkitwings.activity.SplashActivity;
import com.dci.dmkitwings.activity.TimeLineComposeActivity;
import com.dci.dmkitwings.activity.TimeLineDetailPageActivity;
import com.dci.dmkitwings.activity.UserProfileActivity;
import com.dci.dmkitwings.adapter.ArticlesAdpater;
import com.dci.dmkitwings.adapter.EventListAdapter;
import com.dci.dmkitwings.adapter.FeedAdpater;
import com.dci.dmkitwings.adapter.HirerachyAdapter;
import com.dci.dmkitwings.adapter.SubNewsAdpater;
import com.dci.dmkitwings.adapter.CommentAdapter;
import com.dci.dmkitwings.adapter.CommentListAdapter;
import com.dci.dmkitwings.adapter.TimeLineAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.ArticleFragment;
import com.dci.dmkitwings.fragment.CompliantComposeFragment;
import com.dci.dmkitwings.fragment.ContactListFragment;
import com.dci.dmkitwings.fragment.ElectionFragment;
import com.dci.dmkitwings.fragment.EscalatedComplaintfragment;
import com.dci.dmkitwings.fragment.EventComposeFragment;
import com.dci.dmkitwings.fragment.EventEditFragment;
import com.dci.dmkitwings.fragment.GetChatUserListFragment;
import com.dci.dmkitwings.fragment.HirerachyFragment;
import com.dci.dmkitwings.fragment.InboxFragment;
import com.dci.dmkitwings.fragment.Libraryfragment;
import com.dci.dmkitwings.fragment.MyPollsFragment;
import com.dci.dmkitwings.fragment.Mycompliantfragment;
import com.dci.dmkitwings.fragment.MycompliantsDetailsFragment;
import com.dci.dmkitwings.fragment.OtpFragment;
import com.dci.dmkitwings.fragment.PrivacyPolicyFragment;
import com.dci.dmkitwings.fragment.ReceivedComplaintfragment;
import com.dci.dmkitwings.fragment.SentFragment;
import com.dci.dmkitwings.fragment.SubNewsFragment;
import com.dci.dmkitwings.fragment.CommentFragment;
import com.dci.dmkitwings.fragment.EventsFragment;
import com.dci.dmkitwings.fragment.FeedbackComposeFragment;
import com.dci.dmkitwings.fragment.FeedbackDetailsFragment;
import com.dci.dmkitwings.fragment.FeedbackListFragment;
import com.dci.dmkitwings.fragment.LanguageFragment;
import com.dci.dmkitwings.fragment.NewsFragment;
import com.dci.dmkitwings.fragment.OtpVerificationFragment;
import com.dci.dmkitwings.fragment.PartyMemberCardFragment;
import com.dci.dmkitwings.fragment.PartyMemberRegFragment;
import com.dci.dmkitwings.fragment.PollsFragment;
import com.dci.dmkitwings.fragment.SettingsFragment;
import com.dci.dmkitwings.fragment.TimeLineFragment;
import com.dci.dmkitwings.fragment.UserProfileFragment;
import com.dci.dmkitwings.retrofit.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;


@Singleton
// modules for perform dependency injection on below classes
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    void inject(DmkApplication application);

    SharedPreferences sharedPreferences();

    Retrofit retrofit();

    void inject(BaseActivity baseActivity);

    void inject(SettingsFragment settingsFragment);

    void inject(LanguageFragment languageFragment);

    void inject(PartyMemberRegFragment partyMemberRegFragment);


    void inject(HomeActivity homeActivity);

    void inject(HistoryActivity historyActivity);

    void inject(SplashActivity splashActivity);

    void inject(LoginActivity loginActivity);

    void inject(OtpVerificationFragment otpVerificationFragment);

    void inject(TimeLineFragment timeLineFragment);

    void inject(TimeLineAdapter timeLineAdapter);

    void inject(CommentFragment commentFragment);

    void inject(CommentListAdapter commentListAdapter);

    void inject(UserProfileFragment userProfileFragment);

    void inject(UserProfileActivity userProfileActivity);

    void inject(PollsFragment pollsFragment);

    void inject(PartyMemberCardFragment partyMemberCardFragment);

    void inject(TimeLineDetailPageActivity timeLineDetailPageFragment);

    void inject(TimeLineComposeActivity timeLineComposeActivity);

    void inject(CommentAdapter commentAdapter);

    void inject(NewsFragment newsFragment);

    void inject(SubNewsFragment subNewsFragment);

    void inject(NewsDetailaActivity newsDetailaActivity);

    void inject(EventsFragment eventsFragment);

    void inject(EventComposeActivity eventComposeActivity);

    void inject(NewsComposeActivity newsComposeActivity);

    void inject(SubNewsAdpater subNewsAdpater);

    void inject(PollListActivity pollListActivity);

    //void inject(PollsListFragment pollsListFragment);
    void inject(NotificationActivity notificationActivity);

    void inject(FeedbackListFragment feedbackListFragment);

    void inject(FeedBackActivity feedBackActivity);

    void inject(FeedbackComposeFragment feedbackComposeFragment);

    void inject(FeedbackDetailsFragment feedbackDetailsFragment);

    void inject(EventListAdapter eventListAdapter);

    void inject(EventDetailsActivity eventDetailsActivity);

    void inject(HirerachyFragment hirerachyFragment);

    void inject(HirerachyAdapter hirerachyAdapter);

    void inject(EventComposeFragment eventComposeFragment);

    void inject(EventEditFragment eventEditFragment);

    void inject(NewsLinkActivity newsLinkActivity);

    void inject(ArticleFragment articleFragment);

    void inject(ArticlesAdpater articlesAdpater);

    void inject(ArticleDetailActivity articleDetailActivity);

    void inject(ArticleComposeActivity articleComposeActivity);

    void inject(MyPollsFragment myPollsFragment);

    void inject(FeedAdpater feedAdpater);

    void inject(ComplaintActivity complaintActivity);

    void inject(Mycompliantfragment mycompliantfragment);

    void inject(ReceivedComplaintfragment receivedComplaintfragment);

    void inject(CompliantComposeActivity compliantComposeActivity);

    void inject(CompliantComposeFragment compliantComposeFragment);

    void inject(MycompliantsDetailsFragment mycompliantsDetailsFragment);

    void inject(EscalatedComplaintfragment escalatedComplaintfragment);

    void inject(PrivacyPolicyFragment privacyPolicyFragment);

    void inject(Libraryfragment libraryfragment);

    void inject(DigitalLibraryDetailsActivity digitalLibraryDetailsActivity);


    void inject(ChatActivity chatActivity);

    void inject(ContactListFragment contactListFragment);

    void inject(ChatAct chatAct);

    void inject(GetChatUserListFragment getChatUserListFragment);

    void inject(InboxFragment inboxFragment);

    void inject(SentFragment sentFragment);

    void inject(ElectionFragment electionFragment);

    void inject(ElectionPollActivity electionPollActivity);
    void inject(ElectionSurveyFilterActivity electionSurveyFilterActivity);

    void inject(OtpFragment otpFragment);

    void inject(RetrofitModule retrofitModule);
}
