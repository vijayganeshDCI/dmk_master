package com.dci.dmkitwings.model;

public class Userdetail{
	private int id;
	private int usertype;
	private String name;
	private String LastName;
	private String username;
	private String password;
	private String Email;
	private String Password;
	private int RoleID;
	private int SuperiorUserID;
	private int SuperiorRoleId;
	private String PhoneNumber;
	private String UniquieID;
	private int Age;
	private String Gender;
	private int DistrictID;
	private int ConstituencyID;
	private int WardID;
	private String BoothID;
	private String VoterID;
	private String avatar;
	private String Signature;
	private String DeviceToken;
	private String DeviceChangeCount;
	private String IMEIID;
	private String PaymentStatus;
	private String OTP;
	private String OTPValidTime;
	private int Status;
	private String LastLogin;
	private String remember_token;
	private String created_at;
	private String updated_at;
	private int Panchayatunion;
	private int Villagepanchayat;
	private String DOB;
	private int Wing;
	private String FirstName;
	private int Typeid;
	private int partid;
	private int municipalityid;
	private int townshipid;
	private int vattamid;
	private int partydistrict;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUsertype() {
		return usertype;
	}

	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRoleID() {
		return RoleID;
	}

	public void setRoleID(int roleID) {
		RoleID = roleID;
	}

	public int getSuperiorUserID() {
		return SuperiorUserID;
	}

	public void setSuperiorUserID(int superiorUserID) {
		SuperiorUserID = superiorUserID;
	}

	public int getSuperiorRoleId() {
		return SuperiorRoleId;
	}

	public void setSuperiorRoleId(int superiorRoleId) {
		SuperiorRoleId = superiorRoleId;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getUniquieID() {
		return UniquieID;
	}

	public void setUniquieID(String uniquieID) {
		UniquieID = uniquieID;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public int getConstituencyID() {
		return ConstituencyID;
	}

	public void setConstituencyID(int constituencyID) {
		ConstituencyID = constituencyID;
	}

	public int getWardID() {
		return WardID;
	}

	public void setWardID(int wardID) {
		WardID = wardID;
	}

	public String getBoothID() {
		return BoothID;
	}

	public void setBoothID(String boothID) {
		BoothID = boothID;
	}

	public String getVoterID() {
		return VoterID;
	}

	public void setVoterID(String voterID) {
		VoterID = voterID;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getSignature() {
		return Signature;
	}

	public void setSignature(String signature) {
		Signature = signature;
	}

	public String getDeviceToken() {
		return DeviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		DeviceToken = deviceToken;
	}

	public String getDeviceChangeCount() {
		return DeviceChangeCount;
	}

	public void setDeviceChangeCount(String deviceChangeCount) {
		DeviceChangeCount = deviceChangeCount;
	}

	public String getIMEIID() {
		return IMEIID;
	}

	public void setIMEIID(String IMEIID) {
		this.IMEIID = IMEIID;
	}

	public String getPaymentStatus() {
		return PaymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		PaymentStatus = paymentStatus;
	}

	public String getOTP() {
		return OTP;
	}

	public void setOTP(String OTP) {
		this.OTP = OTP;
	}

	public String getOTPValidTime() {
		return OTPValidTime;
	}

	public void setOTPValidTime(String OTPValidTime) {
		this.OTPValidTime = OTPValidTime;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getLastLogin() {
		return LastLogin;
	}

	public void setLastLogin(String lastLogin) {
		LastLogin = lastLogin;
	}

	public String getRemember_token() {
		return remember_token;
	}

	public void setRemember_token(String remember_token) {
		this.remember_token = remember_token;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getPanchayatunion() {
		return Panchayatunion;
	}

	public void setPanchayatunion(int panchayatunion) {
		Panchayatunion = panchayatunion;
	}

	public int getVillagepanchayat() {
		return Villagepanchayat;
	}

	public void setVillagepanchayat(int villagepanchayat) {
		Villagepanchayat = villagepanchayat;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String DOB) {
		this.DOB = DOB;
	}

	public int getWing() {
		return Wing;
	}

	public void setWing(int wing) {
		Wing = wing;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public int getTypeid() {
		return Typeid;
	}

	public void setTypeid(int typeid) {
		Typeid = typeid;
	}

	public int getPartid() {
		return partid;
	}

	public void setPartid(int partid) {
		this.partid = partid;
	}

	public int getMunicipalityid() {
		return municipalityid;
	}

	public void setMunicipalityid(int municipalityid) {
		this.municipalityid = municipalityid;
	}

	public int getTownshipid() {
		return townshipid;
	}

	public void setTownshipid(int townshipid) {
		this.townshipid = townshipid;
	}

	public int getVattamid() {
		return vattamid;
	}

	public void setVattamid(int vattamid) {
		this.vattamid = vattamid;
	}

	public int getPartydistrict() {
		return partydistrict;
	}

	public void setPartydistrict(int partydistrict) {
		this.partydistrict = partydistrict;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}
}
