package com.dci.dmkitwings.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.MessageViewPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by vijayaganesh on 4/11/2018.
 */

public class HomeMessageFragment extends BaseFragment {

    @BindView(R.id.tabs_message)
    TabLayout tabsMessage;
    @BindView(R.id.viewpager_message)
    ViewPager viewpagerMessage;
    @BindView(R.id.cons_mes)
    ConstraintLayout consMes;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        MessageViewPagerAdapter messageViewPagerAdapter = new MessageViewPagerAdapter(getActivity().getSupportFragmentManager());
        //messageViewPagerAdapter.addFrag(new InboxFragment(), getResources().getString(R.string.Notification));
        messageViewPagerAdapter.addFrag(new MessageFragment(), getResources().getString(R.string.Message));
        viewpagerMessage.setAdapter(messageViewPagerAdapter);
        tabsMessage.setupWithViewPager(viewpagerMessage, true);
        setupTabIcons();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setupTabIcons() {
//        TextView textNotification = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_text, null);
//        textNotification.setText(getResources().getString(R.string.Notification));
//        textNotification.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_tab_notification, 0, 0, 0);
//        tabsMessage.getTabAt(0).setCustomView(textNotification);
        TextView textMessage = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tab_text, null);
        textMessage.setText(getResources().getString(R.string.Message));
        textMessage.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_tab_message, 0, 0, 0);
        tabsMessage.getTabAt(0).setCustomView(textMessage);
    }
}
