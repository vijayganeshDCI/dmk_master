package com.dci.dmkitwings.fragment;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.CompliantComposeActivity;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.view.CustomTextView;
import com.dci.dmkitwings.view.CustomTextViewBold;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MycompliantsDetailsFragment extends BaseFragment {
    Bundle bundle;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;
    Unbinder unbinder;
    CompliantComposeActivity compliantComposeActivity;
    @BindView(R.id.text_posted_by)
    CustomTextView textFeedPostedName;
    @BindView(R.id.text_post_date)
    CustomTextViewBold textFeedPostedDate;
    @BindView(R.id.text_label_feed_back_content)
    CustomTextView textlabelfeedbackcontent;
    @BindView(R.id.text_label_feed_back_title)
    CustomTextViewBold textLabelFeedBackTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedbackdetails, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        compliantComposeActivity = (CompliantComposeActivity) getActivity();
        compliantComposeActivity.textTitle.setText(R.string.My_Complaint);
        bundle = this.getArguments();
        if (bundle != null) {

            //textFeedPostedDesgination.setText(bundle.getString("designation"));
            textFeedPostedDate.setText(bundle.getString("date"));
            textLabelFeedBackTitle.setText(bundle.getString("categoryid"));
            textlabelfeedbackcontent.setText(bundle.getString("feedback"));
            if (bundle.getString("status").equals("Resolved")) {
                textFeedPostedName.setText(getString(R.string.Resolved));
                textFeedPostedName.setTextColor(getActivity().getResources().getColor(R.color.accept_green));
                textFeedPostedName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_resolved_32_green, 0);

            } else {
                textFeedPostedName.setText(getString(R.string.NotResolved));
                textFeedPostedName.setTextColor(Color.RED);
                textFeedPostedName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_not_resolved_32_red, 0);

            }

        }
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
