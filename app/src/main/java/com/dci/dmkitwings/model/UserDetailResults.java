package com.dci.dmkitwings.model;

public class UserDetailResults {
	private UserDetailWing wing;

	private int Wing;
	private String Email;
	private String OTPValidTime;
	private int SuperiorRoleId;
	private String OTP;
	private int Villagepanchayat;
	private String Gender;
	private String IMEIID;
	private int RoleID;
	private String password;
	private String DOB;
	private String VoterID;
	private int id;
	private int ConstituencyID;
	private String remember_token;
	private String PaymentStatus;
	private int Status;
	private int BoothID;
	private int partydistrict;
	private String DeviceChangeCount;
	private int WardID;
	private String WardName;
	private String LastLogin;
	private String name;
	private String LastName;
	//private int vattamid;

	private int UserType;
	private int Typeid;
	private int DistrictID;
	private String ConstituencyName;
	private UserDetailRoles roles;
	private String created_at;
	private String updated_at;
	private int Panchayatunion;
	private int townshipid;
	private int Age;
	private String Password;
	private int municipalityid;
	private int partid;
	private String FirstName;
	private int SuperiorUserID;
	private String avatar;
	private String DistrictName;
	private String UniquieID;
	private String DeviceToken;
	private String Signature;
	private String PhoneNumber;
	private String username;

	public int getUniontype() {
		return uniontype;
	}

	public void setUniontype(int uniontype) {
		this.uniontype = uniontype;
	}

	private int uniontype;
	private UserPermissionset UserPermissionset;

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	private String Address;


	public com.dci.dmkitwings.model.UserPermissionset getUserPermissionset() {
		return UserPermissionset;
	}

	public void setUserPermissionset(com.dci.dmkitwings.model.UserPermissionset userPermissionset) {
		UserPermissionset = userPermissionset;
	}

	public int getWing() {
		return Wing;
	}

	public void setWing(int wing) {
		Wing = wing;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getOTPValidTime() {
		return OTPValidTime;
	}

	public void setOTPValidTime(String OTPValidTime) {
		this.OTPValidTime = OTPValidTime;
	}

	public int getSuperiorRoleId() {
		return SuperiorRoleId;
	}

	public void setSuperiorRoleId(int superiorRoleId) {
		SuperiorRoleId = superiorRoleId;
	}

	public String getOTP() {
		return OTP;
	}

	public void setOTP(String OTP) {
		this.OTP = OTP;
	}

	public int getVillagepanchayat() {
		return Villagepanchayat;
	}

	public void setVillagepanchayat(int villagepanchayat) {
		Villagepanchayat = villagepanchayat;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getIMEIID() {
		return IMEIID;
	}

	public void setIMEIID(String IMEIID) {
		this.IMEIID = IMEIID;
	}

	public int getRoleID() {
		return RoleID;
	}

	public void setRoleID(int roleID) {
		RoleID = roleID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getMunicipalityid() {
		return municipalityid;
	}

	public void setMunicipalityid(int municipalityid) {
		this.municipalityid = municipalityid;
	}

	public int getPartid() {
		return partid;
	}

	public void setPartid(int partid) {
		this.partid = partid;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public int getSuperiorUserID() {
		return SuperiorUserID;
	}

	public void setSuperiorUserID(int superiorUserID) {
		SuperiorUserID = superiorUserID;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getDistrictName() {
		return DistrictName;
	}

	public void setDistrictName(String districtName) {
		DistrictName = districtName;
	}

	public String getUniquieID() {
		return UniquieID;
	}

	public void setUniquieID(String uniquieID) {
		UniquieID = uniquieID;
	}

	public String getDeviceToken() {
		return DeviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		DeviceToken = deviceToken;
	}

	public String getSignature() {
		return Signature;
	}

	public void setSignature(String signature) {
		Signature = signature;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String DOB) {
		this.DOB = DOB;
	}

	public String getVoterID() {
		return VoterID;
	}

	public void setVoterID(String voterID) {
		VoterID = voterID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getConstituencyID() {
		return ConstituencyID;
	}

	public void setConstituencyID(int constituencyID) {
		ConstituencyID = constituencyID;
	}

	public String getRemember_token() {
		return remember_token;
	}

	public void setRemember_token(String remember_token) {
		this.remember_token = remember_token;
	}

	public String getPaymentStatus() {
		return PaymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		PaymentStatus = paymentStatus;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public int getBoothID() {
		return BoothID;
	}

	public void setBoothID(int boothID) {
		BoothID = boothID;
	}

	public int getPartydistrict() {
		return partydistrict;
	}

	public void setPartydistrict(int partydistrict) {
		this.partydistrict = partydistrict;
	}

	public String getDeviceChangeCount() {
		return DeviceChangeCount;
	}

	public void setDeviceChangeCount(String deviceChangeCount) {
		DeviceChangeCount = deviceChangeCount;
	}

	public int getWardID() {
		return WardID;
	}

	public void setWardID(int wardID) {
		WardID = wardID;
	}

	public String getWardName() {
		return WardName;
	}

	public void setWardName(String wardName) {
		WardName = wardName;
	}

	public String getLastLogin() {
		return LastLogin;
	}

	public void setLastLogin(String lastLogin) {
		LastLogin = lastLogin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

//	public int getVattamid() {
//		return vattamid;
//	}
//
//	public void setVattamid(int vattamid) {
//		this.vattamid = vattamid;
//	}


	public UserDetailRoles getRoles() {
		return roles;
	}

	public void setRoles(UserDetailRoles roles) {
		this.roles = roles;
	}

	public UserDetailWing getWings()
	{
		return wing;
	}
	public void setWing(UserDetailWing wing1)
	{
		this.wing=wing1;
	}

	public int getUserType() {
		return UserType;
	}

	public void setUserType(int userType) {
		UserType = userType;
	}

	public int getTypeid() {
		return Typeid;
	}

	public void setTypeid(int typeid) {
		Typeid = typeid;
	}

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public String getConstituencyName() {
		return ConstituencyName;
	}

	public void setConstituencyName(String constituencyName) {
		ConstituencyName = constituencyName;
	}



	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public int getPanchayatunion() {
		return Panchayatunion;
	}

	public void setPanchayatunion(int panchayatunion) {
		Panchayatunion = panchayatunion;
	}

	public int getTownshipid() {
		return townshipid;
	}

	public void setTownshipid(int townshipid) {
		this.townshipid = townshipid;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}


}
