package com.dci.dmkitwings.model;

public class TimelineDetailsConstituency
{
    private int id;
    private String ConstituencyName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConstituencyName() {
        return ConstituencyName;
    }

    public void setConstituencyName(String constituencyName) {
        ConstituencyName = constituencyName;
    }
}
