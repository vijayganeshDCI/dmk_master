package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.DigitalLibraryDetailsActivity;
import com.dci.dmkitwings.activity.NewsDetailaActivity;
import com.dci.dmkitwings.activity.NewsLinkActivity;
import com.dci.dmkitwings.adapter.DigitalLibraryAdpater;
import com.dci.dmkitwings.adapter.SubNewsAdpater;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.BoothListResponse;
import com.dci.dmkitwings.model.DigitalLibraryListParams;
import com.dci.dmkitwings.model.LibraryResponse;
import com.dci.dmkitwings.model.LibraryResultsItem;
import com.dci.dmkitwings.model.NewsListResultsItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vijayaganesh on 3/16/2018.
 */

public class Libraryfragment extends BaseFragment {


    @BindView(R.id.list_library)
    ListView listLibrary;
    @BindView(R.id.swipe_library)
    SwipeRefreshLayout swipeDigitalLibrary;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Unbinder unbinder;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    @BindView(R.id.image_no_comments)
    ImageView imageNoTimeLine;
    @BindView(R.id.text_no_comm)
    CustomTextView textNoTimeLine;
    private int preLast;
    boolean lastEnd, onCreatebool = false;
    int totalcount = 0, pagecount = 0;
    DigitalLibraryAdpater digitalLibraryAdpater;
    private ArrayList<LibraryResultsItem> libraryResultsItems;
    LibraryResponse libraryResponse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_digital_library, container, false);
        ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        libraryResultsItems = new ArrayList<LibraryResultsItem>();
        digitalLibraryAdpater = new DigitalLibraryAdpater(libraryResultsItems, getContext());
        listLibrary.setAdapter(digitalLibraryAdpater);
        listLibrary.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), DigitalLibraryDetailsActivity.class);
                intent.putExtra("content", libraryResultsItems.get(position).getBookpdf());
                intent.putExtra("bookName", libraryResultsItems.get(position).getBookname());
                intent.putExtra("proPic", libraryResultsItems.get(position).getBookimage());
                startActivityForResult(intent, 2);

            }
        });

        swipeDigitalLibrary.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeDigitalLibrary.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            pagecount = 0;
                            onCreatebool = false;
                            lastEnd = false;
                            preLast = 0;
                            libraryResultsItems.clear();
                            showProgress();
                            getLibrarylist();
                            swipeDigitalLibrary.setRefreshing(false);

                        }
                    }
                }, 000);
            }
        });
        listLibrary.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (listLibrary.getLastVisiblePosition() - listLibrary.getHeaderViewsCount() -
                        listLibrary.getFooterViewsCount()) >= (digitalLibraryAdpater.getCount() - 1)) {

                    // Now your listview has hit the bottom
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                final int lastItem = firstVisibleItem + visibleItemCount;


                if (lastItem == totalItemCount) {
                    if (libraryResultsItems.size() >= 10) {
                        if (preLast != lastItem) {
                            try {
                                if (!lastEnd && onCreatebool) {
                                    pagecount = pagecount + 11;
                                    //LoadData(pagecount,"");
                                    showProgress();
                                    getLibrarylist();

                                    preLast = lastItem;
                                } else {
                                    //Log.d("Last", "fail");
                                }
                            } catch (Exception e) {
                            }

                        }
                    }
                }


            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            pagecount = 0;
            onCreatebool = false;
            lastEnd = false;
            preLast = 0;
            getLibrarylist();

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        pagecount = 0;
        onCreatebool = false;
        lastEnd = false;
        preLast = 0;
        if (isVisible) {
            getLibrarylist();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    public void getLibrarylist() {

        if (Util.isNetworkAvailable()) {
            if (pagecount == 0) {
                libraryResultsItems.clear();
            }
            final DigitalLibraryListParams digitalLibraryListParams = new DigitalLibraryListParams();
            digitalLibraryListParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            digitalLibraryListParams.setLimit(10);
            digitalLibraryListParams.setOffset(pagecount);
            // showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getLibrarylist(headerMap, digitalLibraryListParams).enqueue(new Callback<LibraryResponse>() {
                @Override
                public void onResponse(Call<LibraryResponse> call, Response<LibraryResponse> response) {
                    hideProgress();
                    libraryResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        hideProgress();
                        if (libraryResponse.getStatuscode() == 0) {
                            if (isAdded()) {
                                imageNoTimeLine.setVisibility(View.GONE);
                                textNoTimeLine.setVisibility(View.GONE);
                                listLibrary.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < libraryResponse.getResults().size(); i++) {
                                libraryResultsItems.add(libraryResponse.getResults().get(i));
                            }
                            digitalLibraryAdpater.notifyDataSetChanged();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, libraryResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, libraryResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(libraryResponse.getSource(),
                                    libraryResponse.getSourcedata()));
                            editor.commit();
                            getLibrarylist();
                        }
                    } else {
                        if (libraryResultsItems.size() == 0) {
                            imageNoTimeLine.setVisibility(View.VISIBLE);
                            imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                            textNoTimeLine.setText(R.string.no_news_line);
                            textNoTimeLine.setVisibility(View.INVISIBLE);
                            listLibrary.setVisibility(View.GONE);
                        } else {
                            lastEnd = true;
                        }
                    }
                }

                @Override
                public void onFailure(Call<LibraryResponse> call, Throwable t) {
                    hideProgress();
                    if (isAdded()) {
                        if (libraryResultsItems.size() == 0) {
                            imageNoTimeLine.setVisibility(View.VISIBLE);
                            textNoTimeLine.setVisibility(View.VISIBLE);
                            imageNoTimeLine.setImageResource(R.mipmap.icon_no_network);
                            textNoTimeLine.setText(R.string.no_network);
                            listLibrary.setVisibility(View.GONE);
                        }

                    }
                }
            });


        } else {
            hideProgress();
            if (isAdded()) {
                imageNoTimeLine.setVisibility(View.VISIBLE);
                textNoTimeLine.setVisibility(View.VISIBLE);
                imageNoTimeLine.setImageResource(R.mipmap.icon_no_network);
                textNoTimeLine.setText(R.string.no_network);
                listLibrary.setVisibility(View.GONE);
            }
        }
    }
}
