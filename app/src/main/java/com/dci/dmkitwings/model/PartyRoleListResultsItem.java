package com.dci.dmkitwings.model;

public class 	PartyRoleListResultsItem {

	private String rolecode;
	private String rolename;
	private String RoleDescription;
	private int id;





	public PartyRoleListResultsItem(int id, String rolename) {
		this.rolename = rolename;
		this.id = id;
	}

	public String getRolecode() {
		return rolecode;
	}

	public void setRolecode(String rolecode) {
		this.rolecode = rolecode;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public String getRoleDescription() {
		return RoleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		RoleDescription = roleDescription;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
