package com.dci.dmkitwings.model;

import java.util.List;

public class TimeLineUpdateParams {
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMediapath() {
        return mediapath;
    }

    public void setMediapath(String mediapath) {
        this.mediapath = mediapath;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getTimelineid() {
        return timelineid;
    }

    public void setTimelineid(int timelineid) {
        this.timelineid = timelineid;
    }

    public int getMediatype() {
        return mediatype;
    }

    public void setMediatype(int mediatype) {
        this.mediatype = mediatype;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getMediaFiles() {
        return MediaFiles;
    }

    public void setMediaFiles(List<String> mediaFiles) {
        MediaFiles = mediaFiles;
    }

    private List<String> MediaFiles;
    private String title;
    private String mediapath;
    private int userid;
    private int timelineid;
    private int mediatype;
    private String content;
    private String Videothumb;


    public String getVideothumb() {
        return Videothumb;
    }

    public void setVideothumb(String videothumb) {
        Videothumb = videothumb;
    }
}
