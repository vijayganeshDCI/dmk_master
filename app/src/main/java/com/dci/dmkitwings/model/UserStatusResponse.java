package com.dci.dmkitwings.model;

import java.util.List;

public class UserStatusResponse{
	public UserStatusResults getResults() {
		return Results;
	}

	public void setResults(UserStatusResults results) {
		Results = results;
	}

	public int getLoginstatus() {
		return Loginstatus;
	}

	public void setLoginstatus(int loginstatus) {
		Loginstatus = loginstatus;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}


	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}





	private String Status;
	private UserStatusResults Results;
	private int Loginstatus;
	private String Message;
}