package com.dci.dmkitwings.model;

public class CommentEditResults {
	public String getParentID() {
		return ParentID;
	}

	public void setParentID(String parentID) {
		ParentID = parentID;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getPostedUserID() {
		return PostedUserID;
	}

	public void setPostedUserID(String postedUserID) {
		PostedUserID = postedUserID;
	}

	public String getTimeLineID() {
		return TimeLineID;
	}

	public void setTimeLineID(String timeLineID) {
		TimeLineID = timeLineID;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String ParentID;
	private int Status;
	private String Comment;
	private String updated_at;
	private String PostedUserID;
	private String TimeLineID;
	private String created_at;
	private int id;
}
