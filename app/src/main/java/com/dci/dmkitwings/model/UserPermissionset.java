package com.dci.dmkitwings.model;

public class UserPermissionset
{
    private String Common;
    private String Article;
    private String Events;
    private String News;
    private String Election;

    public String getCommon() {
        return Common;
    }

    public void setCommon(String common) {
        Common = common;
    }

    public String getArticle() {
        return Article;
    }

    public void setArticle(String article) {
        Article = article;
    }

    public String getEvents() {
        return Events;
    }

    public void setEvents(String events) {
        Events = events;
    }

    public String getNews() {
        return News;
    }

    public void setNews(String news) {
        News = news;
    }

    public String getElection() {
        return Election;
    }

    public void setElection(String election) {
        Election = election;
    }
}
