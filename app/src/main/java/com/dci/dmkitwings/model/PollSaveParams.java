package com.dci.dmkitwings.model;

import java.util.ArrayList;
import java.util.HashMap;

public class PollSaveParams
{
    private int userid;
    private int pollid;
    HashMap<Integer, String> questions ;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getPollid() {
        return pollid;
    }

    public void setPollid(int pollid) {
        this.pollid = pollid;
    }

    public HashMap<Integer, String> getQuestions() {
        return questions;
    }

    public void setQuestions(HashMap<Integer, String> questions) {
        this.questions = questions;
    }
}
