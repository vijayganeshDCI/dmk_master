package com.dci.dmkitwings.model;

public class PolldetailsParams
{

    private int userid;
    private int pollid;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getPollid() {
        return pollid;
    }

    public void setPollid(int pollid) {
        this.pollid = pollid;
    }
}
