package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.NotificationListResultsItem;

import java.util.List;

/**
 * Created by vijayaganesh on 4/12/2018.
 */

public class NotificationAdapter extends BaseAdapter {

    Context context;
    List<NotificationListResultsItem> notificationLList;

    public NotificationAdapter(Context context, List<NotificationListResultsItem> notificationLList) {
        this.context = context;
        this.notificationLList = notificationLList;
    }

    @Override
    public int getCount() {
        return notificationLList.size();
    }

    @Override
    public NotificationListResultsItem getItem(int position) {
        return notificationLList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_notification, null);
        }


        ImageView imageNotifyIcon = (ImageView) convertView.findViewById(R.id.image_profile_pic);
        ConstraintLayout constraintLayout = (ConstraintLayout) convertView.findViewById(R.id.cons_notification);
        TextView textName = (TextView) convertView.findViewById(R.id.text_name);
        TextView textMes = (TextView) convertView.findViewById(R.id.text_mes);
        TextView textdate = (TextView) convertView.findViewById(R.id.text_date);

        textName.setText(notificationLList.get(position).getTitle());
        textdate.setText(notificationLList.get(position).getCreated_at());

        if (notificationLList.get(position).getType() == 1) {
//            events
            imageNotifyIcon.setImageResource(R.mipmap.icon_event_notification_list);
        } else if (notificationLList.get(position).getType() == 2) {
//            polls
            imageNotifyIcon.setImageResource(R.mipmap.icon_poll_notification_list);
        }

        if (notificationLList.get(position).getViewStatus() == 0) {
//            read
            constraintLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else if (notificationLList.get(position).getViewStatus() == 1) {
//un read
            constraintLayout.setBackgroundColor(context.getResources().getColor(R.color.light_grey3));
        }

        return convertView;
    }
}
