package com.dci.dmkitwings.model;

import java.util.List;

public class VattamListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<VattamResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<VattamResultsItem> results) {
		Results = results;
	}

	private String Status;
	private List<VattamResultsItem> Results;
}