package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.ElectionQuestionListAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.ElectionFragment;
import com.dci.dmkitwings.model.ElectionQuestionResponse;
import com.dci.dmkitwings.model.ElectionQuestionResultsItem;
import com.dci.dmkitwings.model.ElectionSurveryParam;
import com.dci.dmkitwings.model.ElectionUserListParams;
import com.dci.dmkitwings.model.ElectionUserListResponse;
import com.dci.dmkitwings.model.PollSaveParams;
import com.dci.dmkitwings.model.PollSaveResponse;
import com.dci.dmkitwings.model.QuestionslistItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.dci.dmkitwings.view.CustomTextViewBold;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.dmkitwings.app.DmkApplication.getProxy;

public class ElectionPollActivity extends BaseActivity {

    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    int userid;
    int surveyid;
    ElectionQuestionResponse electionQuestionResponse;
    @BindView(R.id.image_no_network)
    ImageView imageNoNetwork;
    @BindView(R.id.text_no_network)
    CustomTextView textNoNetwork;
    @BindView(R.id.election_list)
    ListView listPolls;
    @BindView(R.id.cons_polls)
    ConstraintLayout consPolls;
    @BindView(R.id.button_pollsubmit)
    Button buttonPollSubmit;
    @BindView(R.id.text_Poll_title)
    CustomTextViewBold textAddress;
    ElectionQuestionListAdapter electionQuestionListAdapter;
    List<ElectionQuestionResultsItem> electionQuestionResultsItemList;
    PollSaveResponse pollSaveResponse;
    String address;
    String username;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_election_poll, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        mDrawerLayout.addView(contentView, 0);
        frameLayoutNotification.setVisibility(View.GONE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toolbarHome.setVisibility(View.VISIBLE);
        textTitle.setText(getString(R.string.election));
        electionQuestionResultsItemList = new ArrayList<ElectionQuestionResultsItem>();
        //Navigation Control
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        electionQuestionListAdapter = new ElectionQuestionListAdapter(electionQuestionResultsItemList, getApplicationContext());
        listPolls.setAdapter(electionQuestionListAdapter);
        Intent bundle = getIntent();
        if (getIntent() != null) {
            userid = bundle.getIntExtra("userid", 0);
            surveyid = bundle.getIntExtra("surveyid", 0);
            address = bundle.getStringExtra("address");
            username = bundle.getStringExtra("username");
            position = bundle.getIntExtra("position", 0);

        }
        if (Util.isNetworkAvailable()) {
            getElectionQuestionList();
        }
        buttonPollSubmit.setVisibility(View.GONE);

    }

    public void getElectionQuestionList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            electionQuestionResultsItemList.clear();
            final ElectionUserListParams electionUserListParams = new ElectionUserListParams();
            electionUserListParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            electionUserListParams.setSurveyid(surveyid);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getElectionQuestionlist(headerMap, electionUserListParams).enqueue(new Callback<ElectionQuestionResponse>() {
                @Override
                public void onResponse(Call<ElectionQuestionResponse> call, Response<ElectionQuestionResponse> response) {
                    hideProgress();
                    electionQuestionResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (electionQuestionResponse.getStatuscode() == 0) {
                            for (int i = 0; i < electionQuestionResponse.getResults().size(); i++) {
                                electionQuestionResultsItemList.add(electionQuestionResponse.getResults().get(i));

                            }

                            electionQuestionListAdapter.notifyDataSetChanged();
                            textAddress.setText(username + " " + address);
                            buttonPollSubmit.setVisibility(View.VISIBLE);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, electionQuestionResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, electionQuestionResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(electionQuestionResponse.getSource(),
                                    electionQuestionResponse.getSourcedata()));
                            editor.commit();
                            getElectionQuestionList();
                        }
                    } else {
                        imageNoNetwork.setVisibility(View.VISIBLE);
                        textNoNetwork.setVisibility(View.VISIBLE);
                        imageNoNetwork.setImageResource(R.mipmap.icon_no_timeline);
                        textNoNetwork.setText(R.string.no_survey);
                        listPolls.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ElectionQuestionResponse> call, Throwable t) {
                    hideProgress();
                    imageNoNetwork.setVisibility(View.VISIBLE);
                    textNoNetwork.setVisibility(View.VISIBLE);
                    imageNoNetwork.setImageResource(R.mipmap.icon_no_timeline);
                    textNoNetwork.setText(R.string.error);
                    listPolls.setVisibility(View.GONE);
                }
            });
        } else {
            imageNoNetwork.setVisibility(View.VISIBLE);
            textNoNetwork.setVisibility(View.VISIBLE);
            imageNoNetwork.setImageResource(R.mipmap.icon_no_network);
            textNoNetwork.setText(R.string.no_network);
            listPolls.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.button_pollsubmit)
    public void onViewClicked(View view) {
        if (electionQuestionListAdapter.questions.size() == electionQuestionResultsItemList.size()) {
            saveSurveylist();
        } else {
            Toast.makeText(this, getString(R.string.ansswer_all_polls), Toast.LENGTH_SHORT).show();
        }


    }


    private void saveSurveylist() {
        showProgress();
        HashMap<Integer, String> questionsTypeTwo = new HashMap<>();
        final ElectionSurveryParam electionSurveryParam = new ElectionSurveryParam();
        electionSurveryParam.setUserid(userid);
        electionSurveryParam.setAgentid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        electionSurveryParam.setSurveyid(surveyid);
        electionSurveryParam.setQuestions(electionQuestionListAdapter.questions);
        electionSurveryParam.setBoothid(sharedPreferences.getInt(DmkConstants.USER_BOOTH_ID, 0));
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));

        dmkAPI.setSurveyAnswer(headerMap, electionSurveryParam).enqueue(new Callback<PollSaveResponse>() {
            @Override
            public void onResponse(Call<PollSaveResponse> call, Response<PollSaveResponse> response) {
                hideProgress();
                pollSaveResponse = response.body();
                if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                    if (pollSaveResponse.getStatuscode() == 0) {
                        if (pollSaveResponse.getStatus().contains("Success")) {
                            ElectionFragment.electionuserListArray.remove(position);
                            Toast.makeText(getApplicationContext(), pollSaveResponse.getResults()
                                    , Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        } else {

                            Toast.makeText(getApplicationContext(), pollSaveResponse.getResults()
                                    , Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }
                    } else {
                        editor.putString(DmkConstants.HEADER_SOURCE, pollSaveResponse.getSource());
                        editor.putString(DmkConstants.HEADER_SOURCE_DATA, pollSaveResponse.getSourcedata());
                        editor.putString(DmkConstants.HEADER, getEncodedHeader(pollSaveResponse.getSource(),
                                pollSaveResponse.getSourcedata()));
                        editor.commit();
                        saveSurveylist();
                    }


                } else {
                    hideProgress();
                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<PollSaveResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                onBackPressed();

            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }


}
