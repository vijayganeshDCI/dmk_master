package com.dci.dmkitwings.model;

public class BoothListInputParam {


    public int getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(int divisionID) {
        DivisionID = divisionID;
    }

    public int getPartID() {
        return PartID;
    }

    public void setPartID(int partID) {
        PartID = partID;
    }

    public int getWardID() {
        return WardID;
    }

    public void setWardID(int wardID) {
        WardID = wardID;
    }

    public int getVillageID() {
        return VillageID;
    }

    public void setVillageID(int villageID) {
        VillageID = villageID;
    }

    private int DivisionID;
    private int PartID;
    private int WardID;
    private int UnionType;
    private int VillageID;


    public int getUnionType() {
        return UnionType;
    }

    public void setUnionType(int unionType) {
        UnionType = unionType;
    }
}
