package com.dci.dmkitwings.model;

import java.util.List;

public class PartyRoleListResponse{
	private String Status;
	private List<PartyRoleListResultsItem> Results;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<PartyRoleListResultsItem> getResults() {
		return Results;
	}

	public void setResults(List<PartyRoleListResultsItem> results) {
		Results = results;
	}

	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}