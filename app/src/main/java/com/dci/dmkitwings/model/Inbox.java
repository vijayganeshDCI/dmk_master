package com.dci.dmkitwings.model;

/**
 * Created by vijayaganesh on 4/12/2018.
 */

public class Inbox {

    public Inbox(String name, String mes, int profilePic, int id, int time) {
        this.name = name;
        this.mes = mes;
        this.profilePic = profilePic;
        this.id = id;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public int getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(int profilePic) {
        this.profilePic = profilePic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    private String name;
    private String mes;
    private int profilePic;
    private int id;
    private int time;
}
