package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.ArticleDetailActivity;
import com.dci.dmkitwings.activity.EventDetailsActivity;
import com.dci.dmkitwings.activity.NewsDetailaActivity;
import com.dci.dmkitwings.activity.TimeLineDetailPageActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class ImageViewPageAdapter extends PagerAdapter {
    Context context;
    List<String> images;
    LayoutInflater layoutInflater;


    public ImageViewPageAdapter(Context context, List<String> images) {
        this.context = context;
        this.images = images;
        layoutInflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ConstraintLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.item_image_view_pager,
                container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.
                image_pager);
        if (context instanceof TimeLineDetailPageActivity) {
            universalImageLoader(imageView, context.getString(R.string.s3_bucket_time_line_path),
                    images.get(position), R.mipmap.icon_loading_image_400x100,
                    R.mipmap.icon_no_image_400x200);
        } else if (context instanceof NewsDetailaActivity) {
            universalImageLoader(imageView, context.getString(R.string.s3_bucket_news_path),
                    images.get(position), R.mipmap.icon_loading_image_400x100,
                    R.mipmap.icon_no_image_400x200);
        }else if (context instanceof ArticleDetailActivity){
            universalImageLoader(imageView, context.getString(R.string.s3_bucket_article_path),
                    images.get(position), R.mipmap.icon_loading_image_400x100,
                    R.mipmap.icon_no_image_400x200);
        }else if (context instanceof EventDetailsActivity){
            universalImageLoader(imageView, context.getString(R.string.s3_bucket_events_path),
                    images.get(position), R.mipmap.icon_loading_image_400x100,
                    R.mipmap.icon_no_image_400x200);
        }

        container.addView(itemView);
        //listening to image click
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ConstraintLayout) object);
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }
}
