package com.dci.dmkitwings.activity;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.ChatMessageParams;
import com.dci.dmkitwings.model.GetChatUserListResultsItem;
import com.dci.dmkitwings.model.TransparentMode;
import com.dci.dmkitwings.model.TransparentModeResponse;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomEditText;
import com.dci.dmkitwings.view.CustomTextView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.appindexing.Action;
import com.google.firebase.appindexing.FirebaseAppIndex;
import com.google.firebase.appindexing.FirebaseUserActions;
import com.google.firebase.appindexing.Indexable;
import com.google.firebase.appindexing.builders.Indexables;
import com.google.firebase.appindexing.builders.PersonBuilder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatAct extends BaseActivity {

    private static final int MY_REQUEST_CODE_CAMERA = 1;
    private static final int SELECT_IMAGE_FROM_CAMERA = 2;
    private static final int MY_REQUEST_CODE_GALLERY = 3;
    private static final int SELECT_IMAGE_FROM_GALLERY = 4;
    @Inject
    SharedPreferences sharedPreferences;
    @BindView(R.id.edit_write_comment)
    CustomEditText editWriteComment;
    @BindView(R.id.image_attach_file)
    ImageView imageAttachFile;
    @BindView(R.id.image_comment_send)
    ImageView imageCommentSend;
    @BindView(R.id.recycler_chat_view)
    RecyclerView messageRecyclerView;
    @BindView(R.id.cons_comment)
    ConstraintLayout consComment;
    @BindView(R.id.text_label_member_id_water_mark)
    CustomTextView textLabelMemberIdWaterMark;
    @BindView(R.id.cons_chat)
    ConstraintLayout consChat;
    @BindView(R.id.float_attach_image)
    FloatingActionButton floatAttachImage;
    @BindView(R.id.cord_time_compose)
    CoordinatorLayout cordTimeCompose;

    private LinearLayoutManager mLinearLayoutManager;
    private static final int REQUEST_IMAGE = 1;
    private static final String TAG = "ChatAct";
    private String senderUserName;
    private String senderUniqueID, senderRoleName;
    private String senderFcmToken, senderProPic;
    public static final int DEFAULT_MSG_LENGTH_LIMIT = 10;
    private static final String MESSAGE_SENT_EVENT = "message_sent";
    private static final String LOADING_IMAGE_URL = "https://www.google.com/images/spin-32.gif";
    private static final String MESSAGE_URL = "https://dmkitwings-66ae4.firebaseio.com/";
    public String MESSAGES_CHILD_SENDER, MESSAGES_CHILD_RECEIVER;
    public String RECENT_LIST_CHILD_SENDER, RECENT_LIST_CHILD_RECEIVER;
    public String ONE_TO_ONE_CHAT_MES_CHILD = "ONE_TO_ONE_CHAT_MESSAGES";
    public String ONE_TO_MANY_CHAT_MES_CHILD = "ONE_TO_MANY_CHAT_MESSAGES";
    public String RECENT_CHAT = "RECENT_CHAT_MESSAGES";
    private FirebaseRecyclerAdapter<ChatMessageParams, MessageViewHolder> mFirebaseAdapter;
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private String chatWith, memberID, memberFCMToken, chatwithProPic, designation;
    private int chatWithUserID, senderUserID;
    private String chatMessage;
    private String downloadChatImageUrl;
    private String imageUrl;
    private boolean recentList;
    private SharedPreferences senderfcmSharedPrefrences;
    private String selectedMember;
    private ArrayList<GetChatUserListResultsItem> selectedMemberList;
    private ArrayList<Integer> transparentModeMemberList;
    private Type type;
    private DatabaseReference messagesRef;
    private Intent chatTypeIntent;
    private static String dynamicGroupName;
    private boolean isFromContactList;
    private boolean isForGroundExist, isTransparentModeOn;
    private TransparentModeResponse transparentModeResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_dmk_chat, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        frameLayoutNotification.setVisibility(View.GONE);
        switchSendToLevelTwo.setVisibility(View.VISIBLE);
        switchSendToLevelTwo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isTransparentModeOn = isChecked;
                if (isChecked)
                    Toast.makeText(ChatAct.this, getString(R.string.transparent_mode_on), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(ChatAct.this, getString(R.string.transparent_mode_off), Toast.LENGTH_SHORT).show();
            }
        });
        transparentModeMemberList = new ArrayList<Integer>();
        //Navigation Control
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        imageBaseProPic.setVisibility(View.VISIBLE);
        senderUserName = sharedPreferences.getString(DmkConstants.FIRSTNAME, "") + "" +
                sharedPreferences.getString(DmkConstants.LASTNAME, "");
        senderUserID = sharedPreferences.getInt(DmkConstants.USERID, 0);
        senderUniqueID = sharedPreferences.getString(DmkConstants.UNIQUEDMKID, "");
        senderRoleName = sharedPreferences.getString(DmkConstants.DESGINATION, "");
        senderProPic = sharedPreferences.getString(DmkConstants.PROFILE_PICTURE, "");
        senderfcmSharedPrefrences = getSharedPreferences(DmkConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        senderFcmToken = senderfcmSharedPrefrences.getString(DmkConstants.FCMTOKEN, "");
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        type = new TypeToken<ArrayList<GetChatUserListResultsItem>>() {
        }.getType();
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        chatTypeIntent = getIntent();
        if (getIntent() != null) {
            if (chatTypeIntent.getStringExtra("oneTomany") == null) {
                //One to One chat
                chatWith = chatTypeIntent.getStringExtra("chatWith");
                memberID = chatTypeIntent.getStringExtra("memberID");
                designation = chatTypeIntent.getStringExtra("designation");
                chatwithProPic = chatTypeIntent.getStringExtra("proPic");
                memberFCMToken = chatTypeIntent.getStringExtra("fcmToken");
                chatWithUserID = chatTypeIntent.getIntExtra("chatWithUserID", 0);
                isFromContactList = chatTypeIntent.getBooleanExtra("isFromContactList", false);
                if (isFromContactList)
                    switchSendToLevelTwo.setVisibility(View.VISIBLE);
                else
                    switchSendToLevelTwo.setVisibility(View.GONE);
                textTitle.setText(chatWith);
                universalImageLoader(imageBaseProPic, getString(R.string.s3_bucket_profile_path),
                        chatwithProPic, R.mipmap.icon_pro_image_loading_256,
                        R.mipmap.icon_pro_image_loading_256);
                //My node
                MESSAGES_CHILD_SENDER = senderUserID + "to" + chatWithUserID;
                //Receiver node
                MESSAGES_CHILD_RECEIVER = chatWithUserID + "to" + senderUserID;
                //Inbox node
                RECENT_LIST_CHILD_SENDER = "" + senderUserID;
                //Sent node
                RECENT_LIST_CHILD_RECEIVER = "" + chatWithUserID;
                messagesRef = mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).
                        child(MESSAGES_CHILD_SENDER);
                mFirebaseDatabaseReference.child("IS_FORE_GROUND").push().setValue(RECENT_LIST_CHILD_SENDER);
            } else {
                // One to Many Chat
                selectedMemberList = new ArrayList<GetChatUserListResultsItem>();
                selectedMember = chatTypeIntent.getStringExtra("oneTomany");
                selectedMemberList = new Gson().fromJson(selectedMember, type);
                textTitle.setText(selectedMemberList.size() + " recipients");
                imageBaseProPic.setImageResource(R.mipmap.icon_group);
                dynamicGroupName = String.valueOf(System.currentTimeMillis());
                MESSAGES_CHILD_SENDER = senderUserID + dynamicGroupName;
                messagesRef = mFirebaseDatabaseReference.child(ONE_TO_MANY_CHAT_MES_CHILD).
                        child(MESSAGES_CHILD_SENDER);
            }

        }
        textLabelMemberIdWaterMark.setText(senderUniqueID);
        SnapshotParser<ChatMessageParams> parser = new SnapshotParser<ChatMessageParams>() {
            @Override
            public ChatMessageParams parseSnapshot(DataSnapshot dataSnapshot) {
                ChatMessageParams chatMessageParams = dataSnapshot.getValue(ChatMessageParams.class);
                if (chatMessageParams != null) {
                    chatMessageParams.setId(dataSnapshot.getKey());
                }
                return chatMessageParams;
            }
        };


        FirebaseRecyclerOptions<ChatMessageParams> options =
                new FirebaseRecyclerOptions.Builder<ChatMessageParams>()
                        .setQuery(messagesRef, parser)
                        .build();
        mFirebaseAdapter = new FirebaseRecyclerAdapter<ChatMessageParams, MessageViewHolder>(options) {

            @Override
            public MessageViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
                LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
                showProgress();
                return new MessageViewHolder(inflater.inflate(R.layout.item_message,
                        viewGroup, false));
            }

            @Override
            protected void onBindViewHolder(final MessageViewHolder viewHolder,
                                            final int position,
                                            ChatMessageParams chatMessageParams) {
                if (chatMessageParams.getText() != null) {
                    chatMessage = chatMessageParams.getText();
                } else if (chatMessageParams.getImageUrl() != null) {
                    imageUrl = chatMessageParams.getImageUrl();
                    if (imageUrl.startsWith("gs://")) {
                        StorageReference storageReference = FirebaseStorage.getInstance()
                                .getReferenceFromUrl(imageUrl);
                        storageReference.getDownloadUrl().addOnCompleteListener(
                                new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        if (task.isSuccessful()) {
                                            downloadChatImageUrl = task.getResult().toString();
                                        } else {
                                            Log.w(TAG, "Getting download url was not successful.",
                                                    task.getException());
                                        }
                                    }
                                });
                    } else {
                        downloadChatImageUrl = imageUrl;
                    }
                }
                if (chatMessageParams.getName().equalsIgnoreCase(senderUserName)) {
                    viewHolder.conChatItemLeft.setVisibility(View.GONE);
                    viewHolder.conChatItemRight.setVisibility(View.VISIBLE);
                    viewHolder.textMessengerRight.setText("You");
                    viewHolder.textMessengerRight.setVisibility(View.VISIBLE);
                    viewHolder.textSentMesssgeRight.setText(chatMessageParams.getSentTime());
                    viewHolder.textMessengerRight.setTextColor(getResources().getColor(R.color.red));
                    if (chatMessageParams.getText() != null) {
                        viewHolder.messageImageViewRight.setVisibility(View.GONE);
                        viewHolder.textMesssgeRight.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeRight.setText(chatMessage);
                    } else if (chatMessageParams.getImageUrl() != null) {
                        viewHolder.messageImageViewRight.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeRight.setVisibility(View.GONE);
                        Glide.with(viewHolder.messageImageViewRight.getContext())
                                .load(downloadChatImageUrl)
                                .into(viewHolder.messageImageViewRight);
                    }
                } else {
                    viewHolder.conChatItemLeft.setVisibility(View.VISIBLE);
                    viewHolder.conChatItemRight.setVisibility(View.GONE);
                    viewHolder.textMessengerLeft.setText(chatMessageParams.getName());
                    viewHolder.textMessengerLeft.setVisibility(View.VISIBLE);
                    viewHolder.textSentMesssgeLeft.setText(chatMessageParams.getSentTime());
                    viewHolder.textMessengerLeft.setTextColor(getResources().getColor(R.color.black));
                    if (chatMessageParams.getText() != null) {
                        viewHolder.messageImageViewLeft.setVisibility(View.GONE);
                        viewHolder.textMesssgeLeft.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeLeft.setText(chatMessage);
                    } else if (chatMessageParams.getImageUrl() != null) {
                        viewHolder.messageImageViewLeft.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeLeft.setVisibility(View.GONE);
                        Glide.with(viewHolder.messageImageViewLeft.getContext())
                                .load(downloadChatImageUrl)
                                .into(viewHolder.messageImageViewLeft);
                    }

                }
                if (chatMessageParams.getText() != null) {
                    // write this message to the on-device index
//                    FirebaseAppIndex.getInstance().update(getMessageIndexable(chatMessageParams));
                }

                // log a view action on it
                FirebaseUserActions.getInstance().end(getMessageViewAction(chatMessageParams));

                viewHolder.messageImageViewLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChatAct.this);
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert_detail_image_view, null);
                        dialogBuilder.setView(dialogView);
                        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_detail_view);
                        AlertDialog alertDialog = dialogBuilder.create();
                        Glide.with(imageView.getContext())
                                .load(mFirebaseAdapter.getItem(position).getImageUrl())
                                .into(imageView);
                        alertDialog.show();
                    }
                });
                viewHolder.messageImageViewRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChatAct.this);
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert_detail_image_view, null);
                        dialogBuilder.setView(dialogView);
                        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_detail_view);
                        AlertDialog alertDialog = dialogBuilder.create();
                        Glide.with(imageView.getContext())
                                .load(mFirebaseAdapter.getItem(position).getImageUrl())
                                .into(imageView);
                        alertDialog.show();
                    }
                });
                hideProgress();
            }
        };
        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the user is at the bottom of the list, scroll
                // to the bottom of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
                    messageRecyclerView.scrollToPosition(positionStart);
                }
            }
        });

        messageRecyclerView.setLayoutManager(mLinearLayoutManager);
        messageRecyclerView.setAdapter(mFirebaseAdapter);

        // Initialize Firebase Measurement.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        // Initialize Firebase Remote Config.
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        // Define Firebase Remote Config Settings.
        FirebaseRemoteConfigSettings firebaseRemoteConfigSettings =
                new FirebaseRemoteConfigSettings.Builder()
                        .setDeveloperModeEnabled(true)
                        .build();

        // Define default config values. Defaults are used when fetched config values are not
        // available. Eg: if an error occurred fetching values from the server.
        Map<String, Object> defaultConfigMap = new HashMap<>();
        defaultConfigMap.put("friendly_msg_length", 10L);

        // Apply config settings and default values.
        mFirebaseRemoteConfig.setConfigSettings(firebaseRemoteConfigSettings);
        mFirebaseRemoteConfig.setDefaults(defaultConfigMap);

        // Fetch remote config.
        fetchConfig();

        editWriteComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    imageCommentSend.setEnabled(true);
                } else {
                    imageCommentSend.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


    }

    @Override
    public void onPause() {
        mFirebaseAdapter.stopListening();
        mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot1) {
                        for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                            if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_SENDER)) {
                                dataSnapshot2.getRef().removeValue();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mFirebaseAdapter.startListening();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @OnClick({R.id.image_attach_file, R.id.image_comment_send, R.id.float_attach_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_attach_file:
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_IMAGE);
                break;
            case R.id.image_comment_send:
                if (editWriteComment.getText() != null && editWriteComment.getText().toString().length() > 0) {
                    if (chatTypeIntent.getStringExtra("oneTomany") == null) {
                        //One to One
                        //Chat list
                        ChatMessageParams chatMessageParams = new
                                ChatMessageParams(senderUserID, editWriteComment.getText().toString(), senderUserName,
                                senderProPic, null, getCurrentTimeandDate(), chatWith, chatWithUserID,
                                senderUniqueID, senderRoleName, senderFcmToken
                                , memberID, designation, memberFCMToken, chatwithProPic, 0);
                        //My node
                        mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).push().
                                setValue(chatMessageParams);
                        //Receiver node
                        mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).push().
                                setValue(chatMessageParams);
                        //Inbox list
                        final ChatMessageParams chatMessageParams3 = new
                                ChatMessageParams(senderUserID, "", senderUserName,
                                senderProPic,
                                null, getCurrentTimeandDate(), "", 0,
                                senderUniqueID, senderRoleName, senderFcmToken,
                                "", "", "", "", 0);

                        mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox")
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshot) {
                                        if (snapshot.hasChild(RECENT_LIST_CHILD_RECEIVER)) {
                                            //Old User
                                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                    child(RECENT_LIST_CHILD_RECEIVER)
                                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(final DataSnapshot snapshot) {
                                                            //Old User
                                                            for (final DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                                                if (chatMessageParams3.getUniqueID().
                                                                        equals(dataSnapshot.child("uniqueID").
                                                                                getValue())) {
                                                                    //Check chat with user is foreground or background
                                                                    mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                                                                            addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                @Override
                                                                                public void onDataChange(DataSnapshot dataSnapshot1) {
                                                                                    for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                                                                                        if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_RECEIVER)) {
                                                                                            isForGroundExist = true;
                                                                                        } else {
                                                                                            isForGroundExist = false;
                                                                                        }
                                                                                        break;
                                                                                    }
                                                                                    if (!isForGroundExist) {
                                                                                        dataSnapshot.getRef().child("readStatus").setValue(0);
                                                                                    }
                                                                                }

                                                                                @Override
                                                                                public void onCancelled(DatabaseError databaseError) {

                                                                                }
                                                                            });
                                                                    break;

                                                                }
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {

                                                        }
                                                    });
                                        } else {
                                            //                                        New
                                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                    child(RECENT_LIST_CHILD_RECEIVER).push().
                                                    setValue(chatMessageParams3);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });


                        //                    //Sent list--ignore
                        //                    ChatMessageParams chatMessageParams2 = new
                        //                            ChatMessageParams(0, "", "",
                        //                            "", null, getCurrentTimeandDate(), chatWith, chatWithUserID, "",
                        //                            "", ""
                        //                            , memberID, designation, memberFCMToken, chatwithProPic);
                        //                    mFirebaseDatabaseReference.child(RECENT_CHAT).child("Sent").
                        //                            child(RECENT_LIST_CHILD_SENDER).push().
                        //                            setValue(chatMessageParams2);
                    } else {
                        //                    One to Many
                        for (int i = 0; i < selectedMemberList.size(); i++) {
                            //Chat list
                            ChatMessageParams chatMessageParams = new
                                    ChatMessageParams(senderUserID, editWriteComment.getText().toString(), senderUserName,
                                    senderProPic, null, getCurrentTimeandDate(),
                                    selectedMemberList.get(i).getFirstName() + "" +
                                            selectedMemberList.get(i).getLastName()
                                    , selectedMemberList.get(i).getId(),
                                    senderUniqueID, senderRoleName, senderFcmToken
                                    , selectedMemberList.get(i).getUniquieID(), selectedMemberList.get(i).getRoles().getRolename(),
                                    selectedMemberList.get(i).getDeviceToken(), selectedMemberList.get(i).getAvatar(), 0);


                            //My node--update chat history in indiduval user chat history
                            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(
                                    senderUserID + "to" + selectedMemberList.get(i).getId()).push().
                                    setValue(chatMessageParams);
                            //Receiver node
                            MESSAGES_CHILD_RECEIVER = selectedMemberList.get(i).getId() + "to" + senderUserID;
                            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).push().
                                    setValue(chatMessageParams);

                            //Inbox list
                            final ChatMessageParams chatMessageParams3 = new
                                    ChatMessageParams(senderUserID, "", senderUserName,
                                    senderProPic,
                                    null, getCurrentTimeandDate(), "", 0,
                                    senderUniqueID, senderRoleName, senderFcmToken,
                                    "", "", "", "", 0);

                            final int finalI = i;
                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox")
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot snapshot) {
                                            if (snapshot.hasChild("" + selectedMemberList.get(finalI).getId())) {
                                                //Old User
                                                mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                        child("" + selectedMemberList.get(finalI).getId())
                                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot snapshot) {
                                                                //Old User
                                                                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                                                    if (chatMessageParams3.getUniqueID().
                                                                            equals(dataSnapshot.child("uniqueID").
                                                                                    getValue())) {
                                                                        dataSnapshot.getRef().child("readStatus").setValue(0);
                                                                    }
                                                                }
                                                            }

                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {

                                                            }
                                                        });
                                            } else {
                                                //                                        New
                                                mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                        child("" + selectedMemberList.get(finalI).getId()).push().
                                                        setValue(chatMessageParams3);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                            //                        //Sent list--ignore
                            //                        ChatMessageParams chatMessageParams2 = new
                            //                                ChatMessageParams(0, "", "",
                            //                                "", null, getCurrentTimeandDate(), chatWith, chatWithUserID, "",
                            //                                "", ""
                            //                                , memberID, designation, memberFCMToken, chatwithProPic);
                            //                        mFirebaseDatabaseReference.child(RECENT_CHAT).child("Sent").
                            //                                child(RECENT_LIST_CHILD_SENDER).push().
                            //                                setValue(chatMessageParams2);

                        }
                        //Chat list
                        ChatMessageParams chatMessageParams = new
                                ChatMessageParams(senderUserID, editWriteComment.getText().toString(), senderUserName,
                                senderProPic, null, getCurrentTimeandDate(),
                                "", 0, senderUniqueID, senderRoleName, senderFcmToken
                                , "", "", "", "", 0);
                        //My node
                        mFirebaseDatabaseReference.child(ONE_TO_MANY_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).push().
                                setValue(chatMessageParams);

                    }
                    editWriteComment.setText("");
                    mFirebaseAnalytics.logEvent(MESSAGE_SENT_EVENT, null);
                    if (isTransparentModeOn)
                        transparentMode();

                } else {
                    editWriteComment.setError(getString(R.string.empty_comment));
                }
                break;
            case R.id.float_attach_image:

//                getImageFiles();
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);
        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {
            getImageResult(data);
        } else if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {

        }
    }


    private void getImageResult(Intent data) {
        if (data != null) {
            final Uri uri = data.getData();
            Log.d(TAG, "Uri: " + uri.toString());
            if (chatTypeIntent.getStringExtra("oneTomany") == null) {
                //One to one
                ChatMessageParams tempMessage = new ChatMessageParams(senderUserID, null, senderUserName,
                        senderProPic, LOADING_IMAGE_URL, getCurrentTimeandDate(), chatWith, chatWithUserID,
                        senderUniqueID, senderRoleName, senderFcmToken, memberID, designation, memberFCMToken,
                        chatwithProPic, 0);
                mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).push()
                        .setValue(tempMessage, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError,
                                                   DatabaseReference databaseReference) {
                                if (databaseError == null) {
                                    String key = databaseReference.getKey();
                                    StorageReference storageReference =
                                            FirebaseStorage.getInstance()
                                                    .getReference()
                                                    .child(key)
                                                    .child(uri.getLastPathSegment());

                                    putImageInStorage1(storageReference, uri, key);
                                } else {
                                    Log.w(TAG, "Unable to write message to database.",
                                            databaseError.toException());
                                }
                            }
                        });
                mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).push()
                        .setValue(tempMessage, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError,
                                                   DatabaseReference databaseReference) {
                                if (databaseError == null) {
                                    String key = databaseReference.getKey();
                                    StorageReference storageReference =
                                            FirebaseStorage.getInstance()
                                                    .getReference()
                                                    .child(key)
                                                    .child(uri.getLastPathSegment());

                                    putImageInStorage2(storageReference, uri, key);
                                } else {
                                    Log.w(TAG, "Unable to write message to database.",
                                            databaseError.toException());
                                }
                            }
                        });
            } else {
//                One to Many
                for (int i = 0; i < selectedMemberList.size(); i++) {
                    //Chat list
                    ChatMessageParams tempMessage = new
                            ChatMessageParams(senderUserID, null, senderUserName,
                            senderProPic, LOADING_IMAGE_URL, getCurrentTimeandDate(),
                            selectedMemberList.get(i).getFirstName() + "" + selectedMemberList.get(i).getLastName()
                            , selectedMemberList.get(i).getId(),
                            senderUniqueID, senderRoleName, senderFcmToken
                            , selectedMemberList.get(i).getUniquieID(), selectedMemberList.get(i).getRoles().getRolename(),
                            selectedMemberList.get(i).getDeviceToken(), selectedMemberList.get(i).getAvatar(), 0);
                    //My node--update chat history in indiduval user chat history
                    final int finalI = i;
                    mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).
                            child("" + selectedMemberList.get(i).getId()).push()
                            .setValue(tempMessage, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError,
                                                       DatabaseReference databaseReference) {
                                    if (databaseError == null) {
                                        String key = databaseReference.getKey();
                                        StorageReference storageReference =
                                                FirebaseStorage.getInstance()
                                                        .getReference()
                                                        .child(key)
                                                        .child(uri.getLastPathSegment());
                                        putImageInStorageOnetoMany1(storageReference, uri, key, finalI);
                                    } else {
                                        Log.w(TAG, "Unable to write message to database.",
                                                databaseError.toException());
                                    }
                                }
                            });
                    //Receiver node
                    MESSAGES_CHILD_RECEIVER = selectedMemberList.get(i).getId() + "to" + senderUserID;
                    final int finalI1 = i;
                    mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).push()
                            .setValue(tempMessage, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError,
                                                       DatabaseReference databaseReference) {
                                    if (databaseError == null) {
                                        String key = databaseReference.getKey();
                                        StorageReference storageReference =
                                                FirebaseStorage.getInstance()
                                                        .getReference()
                                                        .child(key)
                                                        .child(uri.getLastPathSegment());

                                        putImageInStorageOnetoMany2(storageReference, uri, key, finalI1);
                                    } else {
                                        Log.w(TAG, "Unable to write message to database.",
                                                databaseError.toException());
                                    }
                                }
                            });

                }
                //Chat list
                //My node
                ChatMessageParams tempMessage = new
                        ChatMessageParams(senderUserID, null, senderUserName,
                        senderProPic, LOADING_IMAGE_URL, getCurrentTimeandDate(),
                        "", 0, senderUniqueID, senderRoleName, senderFcmToken
                        , "", "", "", "", 0);
                mFirebaseDatabaseReference.child(ONE_TO_MANY_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).push()
                        .setValue(tempMessage, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError,
                                                   DatabaseReference databaseReference) {
                                if (databaseError == null) {
                                    String key = databaseReference.getKey();
                                    StorageReference storageReference =
                                            FirebaseStorage.getInstance()
                                                    .getReference()
                                                    .child(key)
                                                    .child(uri.getLastPathSegment());

                                    putImageInStorage3(storageReference, uri, key);
                                } else {
                                    Log.w(TAG, "Unable to write message to database.",
                                            databaseError.toException());
                                }
                            }
                        });
            }


        }
    }


    private void putImageInStorage1(final StorageReference storageReference, Uri uri, final String key) {
        storageReference.putFile(uri).
                continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    ChatMessageParams chatMessageParams =
                            new ChatMessageParams(senderUserID, null, senderUserName, senderProPic,
                                    downloadUri.toString(),
                                    getCurrentTimeandDate(), chatWith, chatWithUserID, senderUniqueID,
                                    senderRoleName, senderFcmToken, memberID, designation, memberFCMToken,
                                    chatwithProPic, 0);
                    mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).child(key)
                            .setValue(chatMessageParams);
//                    //Sent items
//                    ChatMessageParams chatMessageParams2 = new
//                            ChatMessageParams(0, "", "",
//                            "", null, getCurrentTimeandDate(), chatWith, chatWithUserID, "",
//                            "", ""
//                            , memberID, designation, memberFCMToken, chatwithProPic);
//                    mFirebaseDatabaseReference.child(RECENT_CHAT).child("Sent").
//                            child(RECENT_LIST_CHILD_SENDER).push().
//                            setValue(chatMessageParams2);

                } else {
                    Log.w(TAG, "Image upload task was not successful.",
                            task.getException());
                }
            }
        });
    }

    private void putImageInStorage3(final StorageReference storageReference, Uri uri, final String key) {
        storageReference.putFile(uri).
                continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    ChatMessageParams chatMessageParams = new
                            ChatMessageParams(senderUserID, null, senderUserName,
                            senderProPic, downloadUri.toString(), getCurrentTimeandDate(),
                            "", 0, senderUniqueID, senderRoleName, senderFcmToken
                            , "", "", "", "", 0);
                    mFirebaseDatabaseReference.child(ONE_TO_MANY_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).child(key)
                            .setValue(chatMessageParams);
//                    //Sent items
//                    ChatMessageParams chatMessageParams2 = new
//                            ChatMessageParams(0, "", "",
//                            "", null, getCurrentTimeandDate(), chatWith, chatWithUserID, "",
//                            "", ""
//                            , memberID, designation, memberFCMToken, chatwithProPic);
//                    mFirebaseDatabaseReference.child(RECENT_CHAT).child("Sent").
//                            child(RECENT_LIST_CHILD_SENDER).push().
//                            setValue(chatMessageParams2);

                } else {
                    Log.w(TAG, "Image upload task was not successful.",
                            task.getException());
                }
            }
        });
    }

    private void putImageInStorageOnetoMany1(final StorageReference storageReference, Uri uri, final String key, final int i) {
        storageReference.putFile(uri).
                continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    ChatMessageParams chatMessageParams = new
                            ChatMessageParams(senderUserID, null, senderUserName,
                            senderProPic, downloadUri.toString(), getCurrentTimeandDate(),
                            selectedMemberList.get(i).getFirstName() + "" + selectedMemberList.get(i).getLastName()
                            , selectedMemberList.get(i).getId(),
                            senderUniqueID, senderRoleName, senderFcmToken
                            , selectedMemberList.get(i).getUniquieID(), selectedMemberList.get(i).getRoles().getRolename(),
                            selectedMemberList.get(i).getDeviceToken(), selectedMemberList.get(i).getAvatar(), 0);

                    //My node--update chat history in indiduval user chat history
                    mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(
                            senderUserID + "to" + selectedMemberList.get(i).getId()).child(key).setValue(chatMessageParams);

//                    //Sent items
//                    ChatMessageParams chatMessageParams2 = new
//                            ChatMessageParams(0, "", "",
//                            "", null, getCurrentTimeandDate(), chatWith, chatWithUserID, "",
//                            "", ""
//                            , memberID, designation, memberFCMToken, chatwithProPic);
//                    mFirebaseDatabaseReference.child(RECENT_CHAT).child("Sent").
//                            child(RECENT_LIST_CHILD_SENDER).push().
//                            setValue(chatMessageParams2);

                } else {
                    Log.w(TAG, "Image upload task was not successful.",
                            task.getException());
                }
            }
        });
    }

    private void putImageInStorage2(final StorageReference storageReference, Uri uri, final String key) {
        storageReference.putFile(uri).
                continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    ChatMessageParams chatMessageParams =
                            new ChatMessageParams(senderUserID, null, senderUserName, senderProPic,
                                    downloadUri.toString(), getCurrentTimeandDate(), chatWith, chatWithUserID,
                                    senderUniqueID, senderRoleName, senderFcmToken
                                    , memberID, designation, memberFCMToken, chatwithProPic, 0);

                    mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).child(key)
                            .setValue(chatMessageParams);
                    //Inbox
                    final ChatMessageParams chatMessageParams3 = new
                            ChatMessageParams(senderUserID, "", senderUserName,
                            senderProPic,
                            null, getCurrentTimeandDate(), "", 0,
                            senderUniqueID, senderRoleName, senderFcmToken,
                            "", "", "", "", 0);

                    mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox")
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot snapshot) {
                                    if (snapshot.hasChild(RECENT_LIST_CHILD_RECEIVER)) {
                                        //Old User
                                        mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                child(RECENT_LIST_CHILD_RECEIVER)
                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot snapshot) {
                                                        //Old User
                                                        for (final DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                                            if (chatMessageParams3.getUniqueID().
                                                                    equals(dataSnapshot.child("uniqueID").
                                                                            getValue())) {
                                                                //Check chat with user is foreground or background
                                                                mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                                                                        addListenerForSingleValueEvent(new ValueEventListener() {
                                                                            @Override
                                                                            public void onDataChange(DataSnapshot dataSnapshot1) {
                                                                                for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                                                                                    if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_RECEIVER)) {
                                                                                        isForGroundExist = true;
                                                                                    } else {
                                                                                        isForGroundExist = false;
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                if (!isForGroundExist) {
                                                                                    dataSnapshot.getRef().child("readStatus").setValue(0);
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onCancelled(DatabaseError databaseError) {

                                                                            }
                                                                        });
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                });
                                    } else {
//                                        New
                                        mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                child(RECENT_LIST_CHILD_RECEIVER).push().
                                                setValue(chatMessageParams3);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                } else {
                    Log.w(TAG, "Image upload task was not successful.",
                            task.getException());
                }
            }
        });
    }

    private void putImageInStorageOnetoMany2(final StorageReference storageReference, Uri uri, final String key, final int i) {
        storageReference.putFile(uri).
                continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    ChatMessageParams chatMessageParams = new
                            ChatMessageParams(senderUserID, null, senderUserName,
                            senderProPic, downloadUri.toString(), getCurrentTimeandDate(),
                            selectedMemberList.get(i).getFirstName() + "" + selectedMemberList.get(i).getLastName()
                            , selectedMemberList.get(i).getId(),
                            senderUniqueID, senderRoleName, senderFcmToken
                            , selectedMemberList.get(i).getUniquieID(), selectedMemberList.get(i).getRoles().getRolename(),
                            selectedMemberList.get(i).getDeviceToken(), selectedMemberList.get(i).getAvatar(), 0);
                    //Receiver node
                    MESSAGES_CHILD_RECEIVER = selectedMemberList.get(i).getId() + "to" + senderUserID;
                    mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).child(key)
                            .setValue(chatMessageParams);
                    //Inbox
                    final ChatMessageParams chatMessageParams3 = new
                            ChatMessageParams(senderUserID, "", senderUserName,
                            senderProPic,
                            null, getCurrentTimeandDate(), "", 0,
                            senderUniqueID, senderRoleName, senderFcmToken,
                            "", "", "", "", 0);

                    mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox")
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot snapshot) {
                                    if (snapshot.hasChild("" + selectedMemberList.get(i).getId())) {
                                        //Old User
                                        mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                child("" + selectedMemberList.get(i).getId())
                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot snapshot) {
                                                        //Old User
                                                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                                            if (chatMessageParams3.getUniqueID().
                                                                    equals(dataSnapshot.child("uniqueID").
                                                                            getValue())) {
                                                                dataSnapshot.getRef().child("readStatus").setValue(0);
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                });
                                    } else {
//                                        New
                                        mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                child("" + selectedMemberList.get(i).getId()).push().
                                                setValue(chatMessageParams3);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                } else {
                    Log.w(TAG, "Image upload task was not successful.",
                            task.getException());
                }
            }
        });
    }


    private void getImageFiles() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.media_dialog);
        // Set dialog title
        LinearLayout cameraLinear = (LinearLayout) dialog.findViewById(R.id.linear_camera);
        TextView textCamera = (TextView) dialog.findViewById(R.id.text_camera);
        textCamera.setText(R.string.camera);
        ImageView imageCamera = (ImageView) dialog.findViewById(R.id.image_camera);
        imageCamera.setImageResource(R.mipmap.icon_photo_camera);
        LinearLayout galleryLinear = (LinearLayout) dialog.findViewById(R.id.linear_gallery);
        TextView textGallery = (TextView) dialog.findViewById(R.id.text_gallery);
        textGallery.setText(R.string.gallery);
        ImageView imageGallery = (ImageView) dialog.findViewById(R.id.image_gallery);
        imageGallery.setImageResource(R.mipmap.icon_gallery);
        imageCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ChatAct.this, Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_REQUEST_CODE_CAMERA);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, SELECT_IMAGE_FROM_CAMERA);
                    }
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, SELECT_IMAGE_FROM_CAMERA);
                }
                dialog.dismiss();
            }
        });
        imageGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ChatAct.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE_GALLERY);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
//                        chatTypeIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);//
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        startActivityForResult(Intent.createChooser(intent, "Select File"),
                                SELECT_IMAGE_FROM_GALLERY);

                    }
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
//                    chatTypeIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    startActivityForResult(Intent.createChooser(intent, "Select File"),
                            SELECT_IMAGE_FROM_GALLERY);

                }
                dialog.dismiss();
            }

        });

        // set values for custom dialog components - text, image and button
        dialog.show();


    }


    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView textMesssgeLeft, textMesssgeRight;
        TextView textSentMesssgeLeft, textSentMesssgeRight;
        ImageView messageImageViewLeft, messageImageViewRight;
        TextView textMessengerLeft, textMessengerRight;
        ConstraintLayout conChatItemLeft, conChatItemRight;

        public MessageViewHolder(View v) {
            super(v);
            textMesssgeLeft = (TextView) itemView.findViewById(R.id.text_chat_mes_left);
            textMesssgeRight = (TextView) itemView.findViewById(R.id.text_chat_mes_right);
            textSentMesssgeRight = (TextView) itemView.findViewById(R.id.text_sent_time_right);
            textSentMesssgeLeft = (TextView) itemView.findViewById(R.id.text_sent_time_left);
            messageImageViewLeft = (ImageView) itemView.findViewById(R.id.image_chat_left);
            messageImageViewRight = (ImageView) itemView.findViewById(R.id.image_chat_right);
            textMessengerLeft = (TextView) itemView.findViewById(R.id.text_chat_left);
            textMessengerRight = (TextView) itemView.findViewById(R.id.text_chat_right);
            conChatItemLeft = (ConstraintLayout) itemView.findViewById(R.id.cons_chat_left);
            conChatItemRight = (ConstraintLayout) itemView.findViewById(R.id.cons_chat_right);
        }
    }

//    private Indexable getMessageIndexable(ChatMessageParams chatMessageParams) {
//        PersonBuilder sender = Indexables.personBuilder()
//                .setIsSelf(senderUserName.equals(chatMessageParams.getName()))
//                .setName(chatMessageParams.getName())
//                .setUrl(MESSAGE_URL.concat(chatMessageParams.getId() + "/sender"));
//
//        PersonBuilder recipient = Indexables.personBuilder()
//                .setName(chatWith)
//                .setUrl(MESSAGE_URL.concat(chatMessageParams.getId() + "/recipient"));
//
//        Indexable messageToIndex = Indexables.messageBuilder()
//                .setName(chatMessageParams.getText())
//                .setUrl(MESSAGE_URL.concat(chatMessageParams.getId()))
//                .setSender(sender)
//                .setRecipient(recipient)
//                .build();
//
//        return messageToIndex;
//    }

    private Action getMessageViewAction(ChatMessageParams chatMessageParams) {
        return new Action.Builder(Action.Builder.VIEW_ACTION)
                .setObject(chatMessageParams.getName(), MESSAGE_URL.concat(chatMessageParams.getId()))
                .setMetadata(new Action.Metadata.Builder().setUpload(false))
                .build();
    }

    // Fetch the config to determine the allowed length of messages.
    public void fetchConfig() {
        long cacheExpiration = 3600; // 1 hour in seconds
        // If developer mode is enabled reduce cacheExpiration to 0 so that each fetch goes to the
        // server. This should not be used in release builds.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Make the fetched config available via FirebaseRemoteConfig get<type> calls.
                        mFirebaseRemoteConfig.activateFetched();
//                        applyRetrievedLengthLimit();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // There has been an error fetching the config
                        Log.w(TAG, "Error fetching config", e);
//                        applyRetrievedLengthLimit();
                    }
                });
    }

//    private void applyRetrievedLengthLimit() {
//        Long friendly_msg_length = mFirebaseRemoteConfig.getLong("friendly_msg_length");
//        editWriteComment.setFilters(new InputFilter[]{new InputFilter.LengthFilter(friendly_msg_length.intValue())});
//        Log.d(TAG, "FML is: " + friendly_msg_length);
//    }

    private void transparentMode() {
        if (Util.isNetworkAvailable()) {
            final TransparentMode transparentMode = new TransparentMode();
            transparentMode.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            transparentMode.setRoleid(sharedPreferences.getInt(DmkConstants.ROLL_ID, 0));
            if (getIntent() != null) {
                if (chatTypeIntent.getStringExtra("oneTomany") == null) {
                    //One to One chat
                    transparentModeMemberList.add(chatWithUserID);
                } else {
                    //One to many
                    for (int i = 0; i < selectedMemberList.size(); i++) {
                        transparentModeMemberList.add(selectedMemberList.get(i).getId());
                    }
                }
            }
            transparentMode.setChildid(transparentModeMemberList);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.transparentMode(headerMap, transparentMode).enqueue(new Callback<TransparentModeResponse>() {
                @Override
                public void onResponse(Call<TransparentModeResponse> call, Response<TransparentModeResponse> response) {
                    transparentModeResponse = response.body();
                    if (response.code() == 200 && response.body() != null && response.isSuccessful()) {
                        if (transparentModeResponse.getStatuscode() == 0) {
                            Toast.makeText(ChatAct.this, transparentModeResponse.getResults(), Toast.LENGTH_SHORT).show();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, transparentModeResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, transparentModeResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(transparentModeResponse.getSource(),
                                    transparentModeResponse.getSourcedata()));
                            editor.commit();
                            transparentMode();
                        }
                    }

                }

                @Override
                public void onFailure(Call<TransparentModeResponse> call, Throwable t) {

                }
            });

        } else {
            Toast.makeText(ChatAct.this, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
        }
    }

}

