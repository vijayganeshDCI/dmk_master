package com.dci.dmkitwings.model;

import java.util.List;

public class EventsResultsItem {

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public int getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(int createdBy) {
		CreatedBy = createdBy;
	}

	public int getUpdatedBy() {
		return UpdatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		UpdatedBy = updatedBy;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}



	public String getTitleimage() {
		return titleimage;
	}

	public void setTitleimage(String titleimage) {
		this.titleimage = titleimage;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	public EventsResultsItem(int id, String name, String description,
							 List<String> mediafiles, String titleimage, String startDate,
							 String endDate,int createdBy,int districtID,int Sharecount) {
		this.id = id;
		Name = name;
		Description = description;
		this.mediafiles = mediafiles;
		this.titleimage = titleimage;
		StartDate = startDate;
		EndDate = endDate;
		this.CreatedBy=createdBy;
		this.DistrictID=districtID;
		this.Sharecount=Sharecount;
	}

	private int id;
	private int Status;
	private String Name;
	private String Description;
	private int CreatedBy;
	private int UpdatedBy;
	private String created_at;
	private String updated_at;
	private int DistrictID;

	public int getSharecount() {
		return Sharecount;
	}

	public void setSharecount(int sharecount) {
		Sharecount = sharecount;
	}

	private int Sharecount;

	public int getDistrictID() {
		return DistrictID;
	}

	public void setDistrictID(int districtID) {
		DistrictID = districtID;
	}

	public List<String> getMediafiles() {
		return mediafiles;
	}

	public void setMediafiles(List<String> mediafiles) {
		this.mediafiles = mediafiles;
	}

	private List<String> mediafiles;
	private String titleimage;
	private String StartDate;
	private String EndDate;


}
