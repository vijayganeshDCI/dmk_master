package com.dci.dmkitwings.model;

import java.util.ArrayList;

public class HirerachyResultsItem {

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public int getLevel() {
        return Level;
    }

    public void setLevel(int level) {
        Level = level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    private String Designation;

    public HirerachyResultsItem(String designation, String firstName,
                                int level, int id, String lastName, String avatar, int userType, boolean isClickable
            , boolean initialItem) {
        Designation = designation;
        FirstName = firstName;
        Level = level;
        this.id = id;
        LastName = lastName;
        this.avatar = avatar;
        UserType = userType;
        this.isClickable = isClickable;
        this.initialItem = initialItem;
    }

    private String FirstName;
    private int Level;
    private int id;
    private String LastName;
    private String avatar;
    private int UserType;

    public boolean isInitialItem() {
        return initialItem;
    }

    public void setInitialItem(boolean initialItem) {
        this.initialItem = initialItem;
    }

    private boolean initialItem;

    public boolean isClickable() {
        return isClickable;
    }

    public void setClickable(boolean clickable) {
        isClickable = clickable;
    }

    private boolean isClickable;

    public boolean isViewtopvisible() {
        return isViewtopvisible;
    }

    public void setViewtopvisible(boolean viewtopvisible) {
        isViewtopvisible = viewtopvisible;
    }

    private boolean isViewtopvisible;
    private ArrayList<HirerachyResultsItem> subMember = new ArrayList<>();

    public ArrayList<HirerachyResultsItem> getSubMember() {
        return subMember;
    }

    public void setSubMember(ArrayList<HirerachyResultsItem> subMember) {
        this.subMember = subMember;
    }


    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public GetChatUserListRoles getRoles() {
        return roles;
    }

    public void setRoles(GetChatUserListRoles roles) {
        this.roles = roles;
    }

    private String uniqueID;
    private String devicetoken;
    private String phonenumber;
    private GetChatUserListRoles roles;

}
