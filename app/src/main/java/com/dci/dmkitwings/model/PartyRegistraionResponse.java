package com.dci.dmkitwings.model;

public class PartyRegistraionResponse{
	private String Status;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public PartyRegistraionResults getResults() {
		return Results;
	}

	public void setResults(PartyRegistraionResults results) {
		Results = results;
	}

	private PartyRegistraionResults Results;

	private String Message;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}
