package com.dci.dmkitwings.utils;

/**
 * Created by vijayaganesh on 4/17/2018.
 */

public interface SmsListener {

    public void messageReceived(String sender);
}
