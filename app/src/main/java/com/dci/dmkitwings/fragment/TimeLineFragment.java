package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.activity.TimeLineComposeActivity;
import com.dci.dmkitwings.adapter.DistrictAdpater;
import com.dci.dmkitwings.adapter.TimeLineAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.DistrictList;
import com.dci.dmkitwings.model.DistrictListResponse;
import com.dci.dmkitwings.model.DistrictsItem;
import com.dci.dmkitwings.model.NotificationCount;
import com.dci.dmkitwings.model.NotificationCountResponse;
import com.dci.dmkitwings.model.TimeLineInputParam;
import com.dci.dmkitwings.model.TimeListParamsList;
import com.dci.dmkitwings.model.TimelineListResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Java_AES_Cipher;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.wooplr.spotlight.SpotlightConfig;
import com.wooplr.spotlight.SpotlightView;
import com.wooplr.spotlight.utils.SpotlightSequence;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.dmkitwings.activity.NotificationActivity.currentDateandTimeResponse;


/**
 * Created by vijayaganesh on 3/15/2018.
 */

public class TimeLineFragment extends BaseFragment {

    @BindView(R.id.cons_home)
    ConstraintLayout consHome;
    Unbinder unbinder;
    @BindView(R.id.co_home)
    CoordinatorLayout coHome;
    @BindView(R.id.recycler_home)
    RecyclerView recyclerHome;
    @BindView(R.id.float_mes_compose)
    FloatingActionButton floatMesCompose;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    View view;
    @BindView(R.id.swipe_time_line)
    SwipeRefreshLayout swipeTimeLine;
    @BindView(R.id.image_no_comments)
    ImageView imageNoTimeLine;
    @BindView(R.id.text_no_comm)
    CustomTextView textNoTimeLine;
    @BindView(R.id.spinner_district)
    Spinner spinnerDistrict;
    private TimeLineAdapter timeLineAdapter;
    private HomeActivity homeActivity;

    TimelineListResponse timelineListResponse;
    public ArrayList<TimeLineInputParam> timeLineList;
    BaseActivity baseActivity;
    LinearLayoutManager linearLayoutManager;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = false;
    private List<DistrictsItem> districtList;
    private DistrictListResponse districtListResponse;
    public static int districtID;
    public static boolean isDistrictSelected = false;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String currentDateandTime;
    NotificationCountResponse notificationCountResponse;
    private Boolean isStarted = false;
    private Boolean isVisible = false;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        isLoading = false;
        currentPage = 0;
        lastEnd = false;
        if (isVisible) {
            getTimeLineList();
        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            isLoading = false;
            currentPage = 0;
            getTimeLineList();
//            if (sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == 1) {
//                getDistrictList();
//            }
//            getNotificationCount();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
        isDistrictSelected=false;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_time_line, container, false);
        unbinder = ButterKnife.bind(this, view);
        DmkApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        homeActivity = (HomeActivity) getActivity();
        baseActivity = (BaseActivity) getActivity();
        timeLineList = new ArrayList<TimeLineInputParam>();
        districtList = new ArrayList<DistrictsItem>();

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        recyclerHome.setItemAnimator(new DefaultItemAnimator());
        recyclerHome.setLayoutManager(linearLayoutManager);
        timeLineAdapter = new TimeLineAdapter(timeLineList, getActivity(), baseActivity, TimeLineFragment.this);
        recyclerHome.setAdapter(timeLineAdapter);
//        recyclerHome.addOnItemTouchListener(
//                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        TimeLineInputParam timeLineInputParam = timeLineList.get(position);
//
//                    }
//                }));

        recyclerHome.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(View view) {
            }

            @Override
            public void onChildViewDetachedFromWindow(View view) {
                JZVideoPlayer videoPlayer = view.findViewById(R.id.videoplayer_time);
                if (videoPlayer != null) {
                    JZVideoPlayer currentJzvd = JZVideoPlayerManager.getCurrentJzvd();
                    if (currentJzvd != null && currentJzvd.currentScreen != JZVideoPlayer.SCREEN_WINDOW_FULLSCREEN) {
                        JZVideoPlayer.releaseAllVideos();
                    }
                }
//                YouTubePlayerView youTubePlayerView = view.findViewById(R.id.youtube_player_view);
//                if (youTubePlayerView != null)
//                    youTubePlayerView.getCu
//                    youTubePlayerView.release();
            }

        });

        swipeTimeLine.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeTimeLine.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible) {
                            isLoading = false;
                            currentPage = 0;
                            lastEnd = false;
                            timeLineList.clear();
                            timeLineAdapter.notifyDataSetChanged();
                            getTimeLineList();
                            swipeTimeLine.setRefreshing(false);
                        }
                    }
                }, 000);
            }
        });
        recyclerHome.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= 9) {
                        if (!lastEnd) {
                            currentPage = currentPage + 11;
                            getTimeLineList();
                        }
                    }
                }
            }

        });

        if (sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == 1)
            spinnerDistrict.setVisibility(View.VISIBLE);
        else
            spinnerDistrict.setVisibility(View.GONE);

        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                DistrictsItem districtsItem = districtList.get(position);
                districtID = districtsItem.getId();
                if (districtID != 0) {
                    isDistrictSelected = true;
                    getTimeLineList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




//        handleBackPress(view);

        SpotlightSequence.getInstance(getActivity(), config)
                .addSpotlight(((HomeActivity) getActivity()).imageViewNavToggle, getString(R.string.Menu),
                        getString(R.string.app_title_dmk), "menu1")
                .addSpotlight(((HomeActivity) getActivity()).frameLayoutNotification, getString(R.string.notification),
                        getString(R.string.app_title_dmk), "notify1")
                .addSpotlight(floatMesCompose, getString(R.string.time_line_com_show_case),
                        getString(R.string.app_title_dmk), "compose1")
                .startSequence();

        currentDateandTime = simpleDateFormat.format(Calendar.getInstance().getTime());


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.float_mes_compose)
    public void onViewClicked() {
        Intent intent = new Intent(getActivity(), TimeLineComposeActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onBackPressed() {
        return super.onBackPressed();
    }

    public void getTimeLineList() {

        if (Util.isNetworkAvailable()) {
            isLoading = true;
            final TimeListParamsList timeListParamsList = new TimeListParamsList();
            timeListParamsList.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            timeListParamsList.setOffset(currentPage);
            timeListParamsList.setLimit(10);
            if ((sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == 1)
                    && (districtID != 0)) {
                timeListParamsList.setDistrictid(districtID);
            }
            if (currentPage == 0) {
                timeLineList.clear();
            }
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            showProgress();
            dmkAPI.getTimelineList(headerMap, timeListParamsList).enqueue(new Callback<TimelineListResponse>() {
                @Override
                public void onResponse(Call<TimelineListResponse> call, Response<TimelineListResponse> response) {
                    hideProgress();
                    isLoading = false;
                    timelineListResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (timelineListResponse.getStatuscode() == 0) {
                            if (timelineListResponse.getResults().size() > 0) {
                                if (isAdded()) {
                                    imageNoTimeLine.setVisibility(View.GONE);
                                    textNoTimeLine.setVisibility(View.GONE);
                                    recyclerHome.setVisibility(View.VISIBLE);
                                }
                                for (int i = 0; i < timelineListResponse.getResults().size(); i++) {
                                    timeLineList.add(
                                            new TimeLineInputParam(
                                                    timelineListResponse.getResults().get(i).getTitle(),
                                                    timelineListResponse.getResults().get(i).getContent(),
                                                    timelineListResponse.getResults().get(i).getUser().getFirstName(),
                                                    timelineListResponse.getResults().get(i).getUser().getLastName(),
                                                    getDateFormat(timelineListResponse.getResults().get(i).getUpdated_at()),
                                                    timelineListResponse.getResults().get(i).getUser().getDesignation(),
                                                    timelineListResponse.getResults().get(i).getUser().getAvatar(),
                                                    timelineListResponse.getResults().get(i).getMediaFiles().size() > 0 ?
                                                            timelineListResponse.getResults().get(i).getMediaFiles().get(0) :
                                                            "",
                                                    timelineListResponse.getResults().get(i).getUserlikedstatus(),
                                                    timelineListResponse.getResults().get(i).getUser().getId(),
                                                    timelineListResponse.getResults().get(i).getId(),
                                                    timelineListResponse.getResults().get(i).getLikescount(),
                                                    timelineListResponse.getResults().get(i).getMediatype(),
                                                    timelineListResponse.getMediapath(),
                                                    timelineListResponse.getResults().get(i).getCommentscount(),
                                                    timelineListResponse.getResults().get(i).getVideothumb(),
                                                    timelineListResponse.getResults().get(i).getUser().getLevel(),
                                                    timelineListResponse.getResults().get(i).getUser().getDistrict(),
                                                    timelineListResponse.getResults().get(i).getUser().getConstituency(),
                                                    timelineListResponse.getResults().get(i).getUser().getWard(),
                                                    timelineListResponse.getResults().get(i).getUser().getPart(),
                                                    timelineListResponse.getResults().get(i).getUser().getBooth(),
                                                    timelineListResponse.getResults().get(i).getUser().getVillage(),
                                                    timelineListResponse.getResults().get(i).getSharecount()
                                            )
                                    );
                                }
                            } else {
                                if (timeLineList.size() == 0) {
                                    if (isAdded()) {
                                        imageNoTimeLine.setVisibility(View.VISIBLE);
                                        imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                                        textNoTimeLine.setVisibility(View.VISIBLE);
                                        textNoTimeLine.setText(R.string.no_time_line);
                                        recyclerHome.setVisibility(View.GONE);
                                    }
                                } else {
                                    lastEnd = true;
                                }
                            }

                            timeLineAdapter.notifyDataSetChanged();
                            hideProgress();
                            getNotificationCount();
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, timelineListResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, timelineListResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(timelineListResponse.getSource(),
                                    timelineListResponse.getSourcedata()));
                            editor.commit();
                            getTimeLineList();
                        }

                    } else {
                        if (timeLineList.size() == 0) {
                            if (isAdded()) {
                                imageNoTimeLine.setVisibility(View.VISIBLE);
                                imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                                textNoTimeLine.setVisibility(View.VISIBLE);
                                textNoTimeLine.setText(R.string.no_time_line);
                                recyclerHome.setVisibility(View.GONE);
                            }
                        } else {
                            lastEnd = true;
                        }
                    }
                }

                @Override
                public void onFailure(Call<TimelineListResponse> call, Throwable t) {
                    hideProgress();
                    isLoading = false;
                    if (timeLineList.size() == 0) {
                        if (isAdded()) {
                            imageNoTimeLine.setVisibility(View.VISIBLE);
                            imageNoTimeLine.setImageResource(R.mipmap.icon_no_timeline);
                            textNoTimeLine.setText(R.string.no_time_line);
                            textNoTimeLine.setVisibility(View.VISIBLE);
                            recyclerHome.setVisibility(View.GONE);
                        }
                    } else {
                        lastEnd = true;
                        //Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } else {

            if (isAdded()) {
                imageNoTimeLine.setVisibility(View.VISIBLE);
                textNoTimeLine.setVisibility(View.VISIBLE);
                imageNoTimeLine.setImageResource(R.mipmap.icon_no_network);
                textNoTimeLine.setText(R.string.no_network);
                recyclerHome.setVisibility(View.GONE);
            }
        }
    }

//    private void handleBackPress(View view) {
//        view.setFocusableInTouchMode(true);
//        view.requestFocus();
//        view.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    homeActivity.isBackPressed();
//                    return true;
//                }
//                return false;
//            }
//        });
//    }


    public void getDistrictList() {
        if (Util.isNetworkAvailable()) {
            districtList.clear();
            districtList.add(0, new DistrictsItem(getString(R.string.all_district), 0));
            DistrictList districtListparam = new DistrictList();
            districtListparam.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getDistrictList(headerMap, districtListparam).enqueue(new Callback<DistrictListResponse>() {
                @Override
                public void onResponse(Call<DistrictListResponse> call, Response<DistrictListResponse> response) {
                    districtListResponse = response.body();
                    hideProgress();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (districtListResponse.getResults() != null) {
                            for (int i = 0; i < districtListResponse.getResults().getDistricts().size(); i++) {
                                districtList.add(new DistrictsItem(districtListResponse.getResults().getDistricts().get(i).getDistrictName(),
                                        districtListResponse.getResults().getDistricts().get(i).getId()));
                            }
                            DistrictAdpater adapterdivisionList = new DistrictAdpater(districtList, getActivity());
                            spinnerDistrict.setAdapter(adapterdivisionList);
                            if (districtID != 0) {
                                for (int i = 0; i < districtList.size(); i++) {
                                    if (districtList.get(i).getId() == districtID) {
                                        spinnerDistrict.setSelection(i);
                                        break;
                                    }
                                }
                            } else {
                                spinnerDistrict.setSelection(0);
                            }
                        } else {
//                            Toast.makeText(getActivity(), districtListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.no_it_district), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<DistrictListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    private void getNotificationCount() {
        if (Util.isNetworkAvailable()) {
            NotificationCount notificationCount = new NotificationCount();
            notificationCount.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            if (currentDateandTimeResponse != null)
                notificationCount.setCurrenttime(currentDateandTimeResponse);
            else
                notificationCount.setCurrenttime(currentDateandTime);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getNotificationCount(headerMap, notificationCount).enqueue(new Callback<NotificationCountResponse>() {
                @Override
                public void onResponse(Call<NotificationCountResponse> call, Response<NotificationCountResponse> response) {
                    notificationCountResponse = response.body();
                    hideProgress();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null
                            && notificationCountResponse != null) {
                        homeActivity.textcart_badge.setVisibility(View.VISIBLE);
                        if (notificationCountResponse.getStatuscode() == 0) {

                            if (notificationCountResponse.getResults() > 9)
                                homeActivity.textcart_badge.setText("9+");
                            else
                                homeActivity.textcart_badge.setText("" + notificationCountResponse.getResults());

                            if (sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == 1 && !isDistrictSelected) {
                                getDistrictList();
                            }
                        } else {
//                            editor.putString(DmkConstants.HEADER_SOURCE, notificationCountResponse.getSource());
//                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, notificationCountResponse.getSourcedata());
//                            editor.putString(DmkConstants.HEADER, getEncodedHeader(notificationCountResponse.getSource(),
//                                    notificationCountResponse.getSourcedata()));
//                            editor.commit();
//                            getNotificationCount();
                        }
                    }
                }


                @Override
                public void onFailure(Call<NotificationCountResponse> call, Throwable t) {
                    hideProgress();
                    homeActivity.textcart_badge.setVisibility(View.GONE);
                    if (sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == 1 && !isDistrictSelected) {
                        getDistrictList();
                    }
                }
            });
        }
    }


}
