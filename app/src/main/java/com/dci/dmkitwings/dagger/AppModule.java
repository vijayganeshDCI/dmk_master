package com.dci.dmkitwings.dagger;


import android.content.Context;
import android.content.SharedPreferences;

import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.retrofit.DmkAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


//provides instance or objects for a class
@Module
public class AppModule {

    public static final String DMK_PREFS = "dmk";

    private final DmkApplication dmkApp;

    public AppModule(DmkApplication app) {
        this.dmkApp = app;
    }
    //provides dependencies (return application class objects)
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return dmkApp;
    }

    //provides dependencies (return sharedprference objects)
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return dmkApp.getSharedPreferences(DMK_PREFS, MODE_PRIVATE);
    }

    //provides dependencies (return API class objects)
    @Provides
    public DmkAPI provideDMKApiInterface(Retrofit retrofit) {
        return retrofit.create(DmkAPI.class);
    }


}
