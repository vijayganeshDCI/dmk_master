package com.dci.dmkitwings.model;

public class RefreshTokenResults {
	public String getSourcedata() {
		return sourcedata;
	}

	public void setSourcedata(String sourcedata) {
		this.sourcedata = sourcedata;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	private String sourcedata;
	private String source;
}
