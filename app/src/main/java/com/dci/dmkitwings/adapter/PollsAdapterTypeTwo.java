package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.PollSaveParams;
import com.dci.dmkitwings.model.QuestionslistItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 4/16/2018.
 */

public class PollsAdapterTypeTwo extends BaseAdapter {


    public PollsAdapterTypeTwo(List<QuestionslistItem> pollListResultsItemArrayList , Context context) {
        this.pollListResultsItemArrayList = pollListResultsItemArrayList;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    List<QuestionslistItem> pollListResultsItemArrayList;
    Context context;
    LayoutInflater mInflater;
    PollSaveParams pollSaveParams;
    public HashMap<Integer,String> questions = new HashMap<>();
    public HashMap<Integer,ArrayList<String>> TEST = new HashMap<>();
    public ArrayList<String> ok=new ArrayList<>();
    public Map<Integer, List<String>> alternateMap = new HashMap<>();
    @Override
    public int getCount() {
        return pollListResultsItemArrayList.size();
    }

    @Override
    public QuestionslistItem getItem(int position) {

        return pollListResultsItemArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_polls_typetwo, null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (pollListResultsItemArrayList.get(position).getOption1() != null && (!TextUtils.equals(pollListResultsItemArrayList.get(position).getOption1() ,"null")) && (!TextUtils.isEmpty(pollListResultsItemArrayList.get(position).getOption1()))){
            viewHolder.radioButtonOptionone.setVisibility(View.VISIBLE);
            viewHolder.radioButtonOptionone.setText(pollListResultsItemArrayList.get(position).getOption1());



        }
        else
        {
            viewHolder.radioButtonOptionone.setVisibility(View.GONE);
        }
        if (pollListResultsItemArrayList.get(position).getOption2()!= null && (!TextUtils.equals(pollListResultsItemArrayList.get(position).getOption2() ,"null")) && (!TextUtils.isEmpty(pollListResultsItemArrayList.get(position).getOption2()))){
            viewHolder.radioButtonOptiontwo.setVisibility(View.VISIBLE);
            viewHolder.radioButtonOptiontwo.setText(pollListResultsItemArrayList.get(position).getOption2());


        }
        else
        {
            viewHolder.radioButtonOptiontwo.setVisibility(View.GONE);
        }
        if (pollListResultsItemArrayList.get(position).getOption3() != null && (!TextUtils.equals(pollListResultsItemArrayList.get(position).getOption3() ,"null")) && (!TextUtils.isEmpty(pollListResultsItemArrayList.get(position).getOption3()))){
            viewHolder.radioButtonOptionthree.setVisibility(View.VISIBLE);
            viewHolder.radioButtonOptionthree.setText(pollListResultsItemArrayList.get(position).getOption3());
            }
        else
        {
            viewHolder.radioButtonOptionthree.setVisibility(View.GONE);
        }

        if (pollListResultsItemArrayList.get(position).getOption4() != null && (!TextUtils.equals(pollListResultsItemArrayList.get(position).getOption4() ,"null")) && (!TextUtils.isEmpty(pollListResultsItemArrayList.get(position).getOption4())))
        {
            viewHolder.radioButtonoptionFour.setVisibility(View.VISIBLE);
            viewHolder.radioButtonoptionFour.setText(pollListResultsItemArrayList.get(position).getOption4());
        }

        else
        {
            viewHolder.radioButtonoptionFour.setVisibility(View.GONE);
        }


        viewHolder.radioButtonOptionone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    UpdatePollAnswers(pollListResultsItemArrayList.get(position).getId(),"Option1");

                }
                else
                {
                    RemovePollAnwers(pollListResultsItemArrayList.get(position).getId(),"Option1");
                }
            }
        });
        viewHolder.radioButtonOptiontwo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    UpdatePollAnswers(pollListResultsItemArrayList.get(position).getId(),"Option2");

                }
                else
                {
                    RemovePollAnwers(pollListResultsItemArrayList.get(position).getId(),"Option2");
                }
            }
        });
        viewHolder.radioButtonOptionthree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    UpdatePollAnswers(pollListResultsItemArrayList.get(position).getId(),"Option3");

                }
                else
                {
                    RemovePollAnwers(pollListResultsItemArrayList.get(position).getId(),"Option3");
                }
            }
        });
        viewHolder.radioButtonoptionFour.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    UpdatePollAnswers(pollListResultsItemArrayList.get(position).getId(),"Option4");

                }
                else
                {
                    RemovePollAnwers(pollListResultsItemArrayList.get(position).getId(),"Option4");
                }
            }
        });

        viewHolder.textQuestion.setText(pollListResultsItemArrayList.get(position).getQuestion());


        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.text_question)
        TextView textQuestion;
        @BindView(R.id.radio_button_optionone)
        CheckBox radioButtonOptionone;
        @BindView(R.id.radio_button_optiontwo)
        CheckBox radioButtonOptiontwo;
        @BindView(R.id.radio_button_optionthree)
        CheckBox radioButtonOptionthree;
        @BindView(R.id.radio_button_optionfour)
        CheckBox radioButtonoptionFour;
//        @BindView(R.id.cons_item)
//        ConstraintLayout consItem;
        //        @BindView(R.id.card_polls)
//        CardView cardPolls;
        @BindView(R.id.cons_item_polls)
        ConstraintLayout consItemPolls;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
    public void RemovePollAnwers(int questionid,String answer)
    {
        if (TEST.containsKey(questionid))
        {
            if (ok.contains(answer))
            {
                ok.remove(answer);
                TEST.put(questionid,ok);
            }
        }
    }
    public void UpdatePollAnswers(int questionid,String answer)
    {


        if(questions.containsKey(questionid))
        {
            questions.remove(questionid);
            TEST.remove(questionid);
            questions.put(questionid,answer);

            if (ok.contains(answer))
            {
                ok.remove(answer);
                ok.add(answer);
                TEST.put(questionid,ok);
                alternateMap.put(questionid,ok);


            }
            else
            {
                ok.add(answer);
                TEST.put(questionid,ok);
                alternateMap.put(questionid,ok);
            }


        }
        else
        {
            questions.put(questionid,answer);
            ok.add(answer);
            TEST.put(questionid,ok);
            alternateMap.put(questionid,ok);
        }
    }

}
