package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.MemberList;
import com.dci.dmkitwings.view.CustomTextView;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MemberDownListAdapter extends BaseAdapter {

    public MemberDownListAdapter(List<MemberList> memberLists, Context context) {
        this.memberLists = memberLists;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    List<MemberList> memberLists;
    Context context;
    private final LayoutInflater mInflater;


    @Override
    public int getCount() {
        return memberLists.size();
    }

    @Override
    public MemberList getItem(int position) {
        return memberLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_member_list, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textLabelDesignation.setText(memberLists.get(position).getDesignation());
        viewHolder.imageProPic.setImageResource(memberLists.get(position).getProPicture());
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.image_pro_pic)
        CircularImageView imageProPic;
        @BindView(R.id.text_label_name)
        CustomTextView textLabelDesignation;
        @BindView(R.id.cons_member_list)
        ConstraintLayout consMemberList;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
