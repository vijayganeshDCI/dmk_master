package com.dci.dmkitwings.model;

public class TimeLineDelete {

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getTimelineid() {
        return timelineid;
    }

    public void setTimelineid(int timelineid) {
        this.timelineid = timelineid;
    }

    private int userid;
    private int timelineid;
    private int eventid;
    private int pageid;
    private int offset;
    private int limit;





    public int getEventid() {
        return eventid;
    }

    public void setEventid(int eventid) {
        this.eventid = eventid;
    }

    public int getPageid() {
        return pageid;
    }
    public void setPageid(int pageid) {
        this.pageid = pageid;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
