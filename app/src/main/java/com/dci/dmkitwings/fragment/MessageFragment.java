package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.activity.HomeMessageActivity;
import com.dci.dmkitwings.activity.LoginActivity;
import com.dci.dmkitwings.activity.MessageComposeActivity;
import com.dci.dmkitwings.adapter.MeassageBottomViewPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.aurelhubert.ahbottomnavigation.AHBottomNavigation.TitleState.ALWAYS_SHOW;

/**
 * Created by vijayaganesh on 4/11/2018.
 */

public class MessageFragment extends BaseFragment {

    @BindView(R.id.view_pager_message)
    AHBottomNavigationViewPager viewPagerMessage;
    @BindView(R.id.bottom_navigation_message)
    AHBottomNavigation bottomNavigationMessage;
    @BindView(R.id.cons_mes)
    ConstraintLayout consMes;
    Unbinder unbinder;
    @BindView(R.id.float_mes_compose)
    FloatingActionButton floatMesCompose;
    @BindView(R.id.cord_mes)
    CoordinatorLayout cordMes;
    private Handler handler = new Handler();
    private AHBottomNavigationAdapter navigationAdapter;
    MeassageBottomViewPagerAdapter meassageBottomViewPagerAdapter;
    private Fragment currentFragment;
    HomeMessageActivity homeActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_message, container, false);
        unbinder = ButterKnife.bind(this, view);
        homeActivity= (HomeMessageActivity) getActivity();
        bottomNavigationUIProperties();
        bottomNavigationMessage.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {

                if (currentFragment == null) {
                    currentFragment = meassageBottomViewPagerAdapter.getCurrentFragment();
                }
                viewPagerMessage.setCurrentItem(position, true);

                if (currentFragment == null) {
                    return true;
                }

                currentFragment = meassageBottomViewPagerAdapter.getCurrentFragment();

                return true;
            }
        });

        viewPagerMessage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                bottomNavigationMessage.setCurrentItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void bottomNavigationUIProperties() {

        showOrHideBottomNavigation(true);
        bottomNavigationMessage.setTitleState(ALWAYS_SHOW);
//        Active Color
        bottomNavigationMessage.setAccentColor(getResources().getColor(R.color.red));
//        In Active Color
        bottomNavigationMessage.setInactiveColor(getResources().getColor(R.color.hint_grey));
        navigationAdapter = new AHBottomNavigationAdapter(getActivity(), R.menu.bottom_message);
        navigationAdapter.setupWithBottomNavigation(bottomNavigationMessage);
        meassageBottomViewPagerAdapter = new MeassageBottomViewPagerAdapter(getFragmentManager());
        viewPagerMessage.setAdapter(meassageBottomViewPagerAdapter);
        viewPagerMessage.setCurrentItem(0);
        bottomNavigationMessage.setCurrentItem(0);
        viewPagerMessage.setPagingEnabled(true);


//        // Setting custom colors for icon_notification
//        AHNotification icon_notification = new AHNotification.Builder()
//                .setText("1")
//                .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.accept_green))
//                .setTextColor(ContextCompat.getColor(getActivity(), R.color.white))
//                .build();
//        bottomNavigationNews.setNotification(icon_notification, 1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    /**
     * Show or hide the bottom navigation with animation
     */
    public void showOrHideBottomNavigation(boolean show) {
        if (show) {
            bottomNavigationMessage.restoreBottomNavigation(true);
        } else {
            bottomNavigationMessage.hideBottomNavigation(true);
        }
    }

    @OnClick(R.id.float_mes_compose)
    public void onViewClicked() {
        Intent intent=new Intent(getActivity(), MessageComposeActivity.class);
        startActivity(intent);
    }
}
