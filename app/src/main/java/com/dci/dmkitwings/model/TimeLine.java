package com.dci.dmkitwings.model;

/**
 * Created by vijayaganesh on 4/2/2018.
 */

public class TimeLine {

    private int media;
    private int mediaType;
    private String title;
    private String description;
    private String like;
    private String comments;
    private String shares;
    private String posteDate;

    public TimeLine(int media, int mediaType, String title, String description, String like, String comments, String shares, String posteDate) {
        this.media = media;
        this.mediaType = mediaType;
        this.title = title;
        this.description = description;
        this.like = like;
        this.comments = comments;
        this.shares = shares;
        this.posteDate = posteDate;
    }

    public int getMedia() {
        return media;
    }

    public void setMedia(int media) {
        this.media = media;
    }

    public int getMediaType() {
        return mediaType;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getShares() {
        return shares;
    }

    public void setShares(String shares) {
        this.shares = shares;
    }

    public String getPosteDate() {
        return posteDate;
    }

    public void setPosteDate(String posteDate) {
        this.posteDate = posteDate;
    }

}
