package com.dci.dmkitwings.retrofit;


import com.dci.dmkitwings.adapter.PanchayatUnionListResponse;
import com.dci.dmkitwings.model.*;
import com.dci.dmkitwings.utils.BaseResponse;
import com.google.gson.JsonElement;

import java.util.Map;

import dagger.BindsOptionalOf;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface DmkAPI {

    @POST("api/districts")
    public Call<DistrictListResponse> getDistrictList(@HeaderMap Map<String, String> header, @Body DistrictList districtList);

    @POST("api/partydistrict")
    public Call<PartyDistrictResponse> getPartyDistrictList(@HeaderMap Map<String, String> header, @Body PartyDistrictInputParam partyDistrictInputParam);

    @POST("api/constituency")
    public Call<ConsituencyListResponse> getConstituencyList(@HeaderMap Map<String, String> header,
                                                             @Body ConsituencyList consituencyList);
    @POST("api/divisions")
    public Call<DivisionListResponse> getDivisionList(@HeaderMap Map<String, String> header);

    @POST("api/divisionspart")
    public Call<PartListResponse> getPartList(@HeaderMap Map<String, String> header, @Body PartList partList);

    @POST("api/divisionspart")
    public Call<MunicipalityListResponse> getMunicipalitytList(@HeaderMap Map<String, String> header, @Body PartList partList);

    @POST("api/panchayatUnion")
    public Call<PanchayatUnionListResponse> getPanchayatUnionList(@HeaderMap Map<String, String> header, @Body PartList partList);

    @POST("api/panchayat")
    public Call<TownPanchayatListResponse> getTownPanchayatList(@HeaderMap Map<String, String> header, @Body PartList partList);

    @POST("api/panchayat")
    public Call<VillagePanchayatListResponse> getVillagePanchayatList(@HeaderMap Map<String, String> header, @Body VattamWardList vattamWardList);

    @POST("api/wards")
    public Call<WardListResponse> getWardList(@HeaderMap Map<String, String> header, @Body VattamWardList vattamWardList);

    @POST("api/booths")
    public Call<BoothListResponse> getBoothList(@HeaderMap Map<String, String> header, @Body BoothListInputParam boothListInputParam);

    @POST("api/wings")
    public Call<ItWingsListResponse> getItWingsList(@HeaderMap Map<String, String> header);

    @POST("api/rolls")
    public Call<PartyRoleListResponse> getPartyRoleList(@HeaderMap Map<String, String> header, @Body PartyRoleList partyRoleList);

    @POST("api/vattams")
    public Call<VattamListResponse> getVattamList(@Body VattamWardList vattamWardList);


    @POST("api/signup")
    public Call<PartyRegistraionResponse> getRegistraion(@HeaderMap Map<String, String> header,
                                                         @Body Registration registrationList);


    @POST("timelinelists")
    public Call<TimelineListResponse> getTimelineList(@HeaderMap Map<String, String> header, @Body TimeListParamsList timeListParamsList);

    @POST("timelinelike")
    public Call<TimeLineLikeResponse> setTimelinelike(@HeaderMap Map<String, String> header, @Body TimeLineLikeParams timeLineLikeParams);

    @POST("commentslist")
    public Call<CommentListResponse> getCommentList(@HeaderMap Map<String, String> header, @Body CommentListInput commentListInput);

    @POST("postcomment")
    public Call<CommentPostResponse> postComment(@HeaderMap Map<String, String> header, @Body CommentPost commentPost);

    @POST("api/isuserexist")
    public Call<UserStatusResponse> getUserStatus(@HeaderMap Map<String, String> header, @Body UserStatusParam userStatusParam);


    @POST("createtimeline")
    public Call<TimeLineComposeResponse> createTimeLine(@HeaderMap Map<String, String> header, @Body TimeLineComposeParams timeLineComposeParams);

    @POST("updatetimeline")
    public Call<TimeLineComposeResponse> updateTimeLine(@HeaderMap Map<String, String> header, @Body TimeLineUpdateParams timeLineUpdateParams);

    @POST("deletetimeline")
    public Call<TimeLineDeleteResponse> deleteTimeLine(@HeaderMap Map<String, String> header, @Body TimeLineDelete timeLineDelete);


    @POST("timelineviews")
    public Call<TimeLineDetailsResponse> getTimeLineDetails(@HeaderMap Map<String, String> header, @Body TimeLineDelete timeLineDelete);

    @POST("editcomment")
    public Call<CommentEditResponse> updateComment(@HeaderMap Map<String, String> header, @Body CommentEditParams timeLineEditParams);

    @POST("deletecomment")
    public Call<TimeLineDeleteResponse> deleteComment(@HeaderMap Map<String, String> header, @Body CommentEditParams timeLineEditParams);

    @POST("api/userdetails")
    public Call<UserDetailsResponse> getUserDetails(@HeaderMap Map<String, String> header, @Body UserStatusParam userStatusParam);


    @POST("listpolls")
    public Call<PollListResponse> getPollList(@HeaderMap Map<String, String> header, @Body PollList pollList);

    @POST("pollsave")
    public Call<PollSaveResponse> setPolldata(@HeaderMap Map<String, String> header, @Body PollSaveParams pollSaveParams);

    @POST("mypolls")
    public Call<PollListResponse> getMyPollList(@Body PollList pollList);

    @POST("newslist")
    public Call<NewsListResponse> getNewsList(@HeaderMap Map<String, String> header, @Body NewsListParams newsListParams);

    @POST("newsview")
    public Call<NewsDetailsResponse> getNewsDetails(@HeaderMap Map<String, String> header, @Body NewsDetailParams newsDetailParams);

    @POST("eventslist")
    public Call<EventResponse> getEventList(@HeaderMap Map<String, String> header, @Body TimeLineDelete timeLineDelete);


    @POST("updateevents")
    public Call<AddEventsResponse> updateEventList(@HeaderMap Map<String, String> header, @Body AddEvents addEvents);


    @POST("constituencylist")
    public Call<NewsComposeConstituencyListResponse> getConstituencyListforNewsCompose(
            @HeaderMap Map<String, String> header, @Body NewComposeConstituenctPost newComposeConstituenctPost);

    @POST("createnews")
    public Call<NewsPostResponse> setNewNewsPost(
            @HeaderMap Map<String, String> header, @Body NewsPostParams newsPostParams);

    @POST("deltenews")
    public Call<NewsDeleteResponse> deletenews(@HeaderMap Map<String, String> header, @Body NewsDeletePostParams newsDeletePostParams);

    @POST("updatenews")
    public Call<NewsUpdateResponse> updatenews(@HeaderMap Map<String, String> header, @Body NewsUpdatePostParams newsPostParams);

    @POST("partydistrictList")
    public Call<NewsPartyDistrictResponse> getDistrictListforNewsCompose(
            @HeaderMap Map<String, String> header, @Body NewComposeConstituenctPost newComposeConstituenctPost);

    @POST("addfeedback")
    public Call<FeedbackPostResponse> setFeedback(@HeaderMap Map<String, String> header, @Body FeedbackComposeParam feedbackComposeParam);

    @POST("feedbacklist")
    public Call<FeedbackListResponse> getFeedbackList(@HeaderMap Map<String, String> header, @Body NewsListParams feedbackComposeParam);

    @POST("feedbackcategory")
    public Call<FeedbackCategoryListResponse> getFeedbackcategoryList(
            @HeaderMap Map<String, String> header, @Body FeedbackCategorylistparams feedbackCategorylistparams);

    @POST("heirarchylist")
    public Call<HirerachyResponse> getParentHirerachyList(
            @HeaderMap Map<String, String> header, @Body TimeLineDelete timeLineDelete);

    @POST("heirarchychildlist")
    public Call<HirerachyResponse> getParentChildList(
            @HeaderMap Map<String, String> header, @Body TimeLineDelete timeLineDelete);

    @POST("viewevent")
    public Call<ViewEventResponse> getEventDetails(
            @HeaderMap Map<String, String> header, @Body TimeLineDelete timeLineDelete);

    @POST("polldetails")
    public Call<PollDetailsResponse> getPolldetailsList(
            @HeaderMap Map<String, String> header, @Body PolldetailsParams polldetailsParams);

    @POST("polldetails")
    public Call<JsonElement> getPolldetailsListBystring(
            @HeaderMap Map<String, String> header, @Body PolldetailsParams polldetailsParams);

    @POST("deleteevent")
    public Call<AddEventsResponse> deleteEvents(
            @HeaderMap Map<String, String> header, @Body TimeLineDelete timeLineDelete);

    @POST("mypolls")
    public Call<PollListResponse> getMypolls(
            @HeaderMap Map<String, String> header, @Body PollList pollList);

        @POST("articleslist")
    public Call<ArticleListResponse> getArticleList(
            @HeaderMap Map<String, String> header, @Body ArticleListParams articleListParams);

    @POST("articlesview")
    public Call<ArticleDetailResponse> getArticleDetails(
            @HeaderMap Map<String, String> header, @Body ArticleDetailParams articleDetailParams);

    @POST("createarticles")
    public Call<NewsPostResponse> setArticlepost(
            @HeaderMap Map<String, String> header, @Body ArticleComposeParams articleComposeParams);

    @POST("deltearticles")
    public Call<NewsDeleteResponse> deletearticle(
            @HeaderMap Map<String, String> header, @Body ArticleDeletePostParams articleDeletePostParams);

    @POST("updatearticles")
    public Call<NewsUpdateResponse> updatearticle(
            @HeaderMap Map<String, String> header, @Body ArticleUpdatePostParams articleUpdatePostParams);

    @POST("complaintlist")
    public Call<MyComplaintsResponse> getCompliantList(
            @HeaderMap Map<String, String> header, @Body ComplaintListParams complaintListParams);

    @POST("addcomplaint")
    public Call<FeedbackPostResponse> composenewComplaint(
            @HeaderMap Map<String, String> header, @Body ComposeCompliantParams composeCompliantParams);

    @POST("updatecomplaint")
    public Call<FeedbackPostResponse> updateCompliant(
            @HeaderMap Map<String, String> header, @Body UpdateReceivedCompliantParams updateReceivedCompliantParams);

    @POST("staticpages")
    public Call<StaticPageResponse> getStaticPage(
            @HeaderMap Map<String, String> header, @Body TimeLineDelete timeLineDelete);


    @POST("usercount")
    public Call<NotificationCountResponse> getNotificationCount(@HeaderMap Map<String, String> header, @Body NotificationCount notificationCount);

    @POST("notificationlist")
    public Call<NotificationListResponse> getNotificationList(
            @HeaderMap Map<String, String> header, @Body NotificationCount notificationCount);

    @POST("library")
    public Call<LibraryResponse> getLibrarylist(
            @HeaderMap Map<String, String> header, @Body DigitalLibraryListParams digitalLibraryListParams);

    @POST("getuserslist")
    public Call<GetChatUserListResponse> getChatUserlist(
            @HeaderMap Map<String, String> header, @Body GetChatUserListParam getChatUserListParam);


    @POST("surveyuserlist")
    public Call<ElectionUserListResponse> getElectionUserList(
            @HeaderMap Map<String, String> header, @Body ElectionUserListParams electionUserListParams);

    @POST("surveyquestionlist")
    public Call<ElectionQuestionResponse> getElectionQuestionlist(
            @HeaderMap Map<String, String> header, @Body ElectionUserListParams electionUserListParams);


    @POST("surveyanswer")
    public Call<PollSaveResponse> setSurveyAnswer(
            @HeaderMap Map<String, String> header, @Body ElectionSurveryParam electionSurveryParam);

    @POST("surveysuperieruser")
    public Call<ElectionListOverAllResponse> getsurveysuperieruser(
            @HeaderMap Map<String, String> header, @Body GetSurveyUserListParam getSurveyUserListParam);


    @POST("addevents")
    public Call<AddEventsResponse> addEventList(
            @HeaderMap Map<String, String> header, @Body AddEvents addEvents);

    @POST("api/updateuserdetails")
    public Call<UserDetailsResponse> updateUserDetails(
            @HeaderMap Map<String, String> header, @Body UserProfileUpdate userProfileUpdate);


    @POST("messagenotification")
    public Call<TransparentModeResponse> transparentMode(
            @HeaderMap Map<String, String> header, @Body TransparentMode transparentMode);

    @POST("refreshauthtoken ")
    public Call<RefreshTokenResponse> getRefreshToken(@Body TimeLineDelete timeLineDelete);


    @POST("api/sharecount")
    public Call<ShareCountResponse> addShareCount(@HeaderMap Map<String, String> header, @Body ShareCount shareCount);


}

