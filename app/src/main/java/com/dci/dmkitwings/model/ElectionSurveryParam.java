package com.dci.dmkitwings.model;

import java.util.HashMap;

public class ElectionSurveryParam
{
    private int userid;
    private int agentid;
    private int surveyid;
    HashMap<Integer, String> questions ;
    private int boothid;

    public int getBoothid() {
        return boothid;
    }

    public void setBoothid(int boothid) {
        this.boothid = boothid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getAgentid() {
        return agentid;
    }

    public void setAgentid(int agentid) {
        this.agentid = agentid;
    }

    public int getSurveyid() {
        return surveyid;
    }

    public void setSurveyid(int surveyid) {
        this.surveyid = surveyid;
    }

    public HashMap<Integer, String> getQuestions() {
        return questions;
    }

    public void setQuestions(HashMap<Integer, String> questions) {
        this.questions = questions;
    }
}
