package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.CommentActivity;
import com.dci.dmkitwings.activity.TimeLineComposeActivity;
import com.dci.dmkitwings.activity.TimeLineDetailPageActivity;
import com.dci.dmkitwings.fragment.TimeLineFragment;
import com.dci.dmkitwings.model.ShareCount;
import com.dci.dmkitwings.model.ShareCountResponse;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.model.TimeLineDeleteResponse;
import com.dci.dmkitwings.model.TimeLineInputParam;
import com.dci.dmkitwings.model.TimeLineLikeParams;
import com.dci.dmkitwings.model.TimeLineLikeResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Java_AES_Cipher;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;
import com.google.android.exoplayer2.ExoPlayer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import cn.jzvd.JZVideoPlayerStandard;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.dmkitwings.app.DmkApplication.getContext;
import static com.dci.dmkitwings.app.DmkApplication.getProxy;

/**
 * Created by vijayaganesh on 4/2/2018.
 */

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.TimeLineViewHolder> {


    List<TimeLineInputParam> timeLineList;
    Context context;
    HttpProxyCacheServer proxy;
    TimeLineLikeParams timeLineLikeParams;
    //    TimeLineListResultsItem timeLine;
    TimeLineDeleteResponse timeLineDeleteResponse;
    TimeLineLikeResponse timeLineLikeResponse;
    BaseActivity baseActivity;
    TimeLineFragment timeLineFragment;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public String[] imageFormat = {"tif", "tiff", "gif", "jpeg", "jpg", "jif", "jfif"
            , "jp2", "jpx", "j2k", "j2c"
            , "fpx", "pcd", "png", "pdf"};
    public String[] audioFormat = {"3gp", "aa", "aac", "aax", "act", "aiff", "amr", "ape", "au", "awb"
            , "amr", "ape", "au", "awb", "dct", "dss", "dvf", "dvf", "flac", "gsm", "iklax", "ivs", "m4a", "m4b",
            "m4p", "mmf", "mp3", "mpc", "msv", "nsf", "ogg", "opus", "ra", "raw", "sln", "tta"
            , "vox", "wav", "wma", "webm", "8svx"};
    private final String expression = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";
    private ExoPlayer exoPlayer;
    private boolean playWhenReady = true;
    private ShareCountResponse shareCountResponse;

    public TimeLineAdapter(List<TimeLineInputParam> timeLineList, Context context, BaseActivity baseActivity,
                           TimeLineFragment timeLineFragment) {
        this.timeLineList = timeLineList;
        this.context = context;
        this.baseActivity = baseActivity;
        this.timeLineFragment = timeLineFragment;
        getContext().getComponent().inject(this);

    }


    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_time_line, parent, false);
        proxy = getProxy(context);
        editor = sharedPreferences.edit();
        return new TimeLineViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final TimeLineViewHolder holder, final int position) {
        holder.textTimeTitle.setText(timeLineList.get(position).getTitle());
        holder.textTimeDescription.setText(timeLineList.get(position).getContent());
        holder.textPostedBy.setText(timeLineList.get(position).getFirstName() + " " + timeLineList.get(position).getLastName());
        holder.textDesignation.setText(timeLineList.get(position).getDesignation());
        holder.textPostDate.setText(timeLineList.get(position).getDate());

        if (timeLineList.get(position).getLikeCount() > 0) {
            if (timeLineList.get(position).getLikeCount() > 99) {
                holder.textLike.setText("(99+)");
            } else {
                holder.textLike.setText("(" + timeLineList.get(position).getLikeCount() + ")");
            }
        } else {
            holder.textLike.setText("");
        }

        if (timeLineList.get(position).getCommetsCount() > 0) {
            if (timeLineList.get(position).getCommetsCount() > 99) {
                holder.textCom.setText("(99+)");
            } else {
                holder.textCom.setText("(" +
                        timeLineList.get(position).getCommetsCount() + ")");
            }
        } else {
            holder.textCom.setText("");
        }

//        Like status
//        0-Unlike 1-Like
        if (timeLineList.get(position).getLikeStatus() == 0) {

            holder.textLike.setTextColor(context.getResources().getColor(R.color.drak_grey_one));
            holder.textLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_like, 0, 0, 0);
//            ContextCompat.getDrawable(context, R.mipmap.icon_like).setColorFilter(new PorterDuffColorFilter(
//                    context.getResources().getColor(R.color.drak_grey_one), PorterDuff.Mode.SRC_IN));
        } else if (timeLineList.get(position).getLikeStatus() == 1) {
            holder.textLike.setTextColor(context.getResources().getColor(R.color.red));
            holder.textLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_like_red, 0, 0, 0);
//            ContextCompat.getDrawable(context, R.mipmap.icon_like).setColorFilter(new PorterDuffColorFilter(
//                    context.getResources().getColor(R.color.red), PorterDuff.Mode.SRC_IN));
        }

        if (timeLineList.get(position).getUserID() ==
                sharedPreferences.getInt(DmkConstants.USERID, 0)) {
            holder.imageDelete.setVisibility(View.GONE);
            holder.imageEdit.setVisibility(View.GONE);
            holder.imagemenu.setVisibility(View.VISIBLE);
        } else {
            holder.imageDelete.setVisibility(View.GONE);
            holder.imageEdit.setVisibility(View.GONE);
            holder.imagemenu.setVisibility(View.INVISIBLE);
        }
        holder.imagemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, holder.imagemenu);
                //inflating menu_icon from xml resource
                popup.inflate(R.menu.comment_item_menu);
                //adding click listener


                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit:
                                //handle menu1 click
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setMessage(R.string.confirm_to_edit);
                                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(context, TimeLineComposeActivity.class);
                                        intent.putExtra("timeLineID", timeLineList.get(position).getTimeLineID());
                                        intent.putExtra("timeLineTitle", timeLineList.get(position).getTitle());
                                        intent.putExtra("timeLineContent", timeLineList.get(position).getContent());
                                        intent.putExtra("fromTimeLine", true);
                                        context.startActivity(intent);
                                    }

                                });
                                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                                return true;
                            case R.id.delete:
                                //handle menu2 click
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(context);
                                alertDialog1.setMessage(R.string.confirm_to_delete);
                                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        deleteTimeLine(timeLineList.get(position).getTimeLineID());
                                    }

                                });
                                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog1.show();
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
            }

        });
        holder.imageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        universalImageLoader(holder.image_pro_pic, context.getString(R.string.s3_bucket_profile_path),
                timeLineList.get(position).getProfilePicture(), R.mipmap.icon_pro_image_loading_256,
                R.mipmap.icon_pro_image_loading_256);


        if (timeLineList.get(position).getMediaType() != 1) {
            if (isImageFile(timeLineList.get(position).getMedia())) {
                //Image Type
                holder.imageTime.setVisibility(View.VISIBLE);
                holder.videoPlayerStandard.setVisibility(View.INVISIBLE);
                holder.youtubePlayerView.setVisibility(View.INVISIBLE);
                universalImageLoader(holder.imageTime, context.getString(R.string.s3_bucket_time_line_path),
                        timeLineList.get(position).getMedia(), R.mipmap.icon_loading_image_400x100,
                        R.mipmap.icon_no_image_400x200);
            } else if (!timeLineList.get(position).getMedia().isEmpty()) {
                //Video and audio Type
                holder.imageTime.setVisibility(View.INVISIBLE);
                holder.videoPlayerStandard.setVisibility(View.VISIBLE);
                holder.youtubePlayerView.setVisibility(View.INVISIBLE);
//                initializeExoPlayer(holder.exoPlayerView,
//                        context.getString(R.string.s3_image_url) +
//                                context.getString(R.string.s3_bucket_time_line_path) +
//                                timeLineList.get(position).getMedia());
                holder.videoPlayerStandard.setUp(proxy.getProxyUrl(context.getString(R.string.s3_image_url_download) +
                                context.getString(R.string.s3_bucket_time_line_path) +
                                timeLineList.get(position).getMedia())
                        , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                holder.videoPlayerStandard.setSoundEffectsEnabled(true);

                if (isAudioFile(context.getString(R.string.s3_image_url_download) +
                        context.getString(R.string.s3_bucket_time_line_path) +
                        timeLineList.get(position).getMedia())) {
                    Glide.with(context)
                            .load(R.mipmap.icon_audio_thumbnail).into(holder.videoPlayerStandard.thumbImageView);
                } else {
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                    requestOptions.error(R.mipmap.icon_video_thumbnail);

                    Glide.with(getContext())
                            .load(context.getString(R.string.s3_image_url_download) +
                                    context.getString(R.string.s3_bucket_time_line_path) +
                                    timeLineList.get(position).getMedia())
                            .apply(requestOptions)
                            .thumbnail(Glide.with(getContext()).load(context.getString(R.string.s3_image_url_download) +
                                    context.getString(R.string.s3_bucket_time_line_path) +
                                    timeLineList.get(position).getMedia()))
                            .into(holder.videoPlayerStandard.thumbImageView);


                    //                ThumbnailExtract thumbnailExtract=new ThumbnailExtract(context.getString(R.string.s3_image_url) +
                    //                        context.getString(R.string.s3_bucket_time_line_path) +
                    //                        timeLineList.get(position).getMedia(),holder.videoPlayerStandard.thumbImageView,true);
                    //                thumbnailExtract.execute();

                    //                if (!timeLineList.get(position).getVideoThumb().equalsIgnoreCase("0")) {
                    //                   // byte [] encodeByte= Base64.decode(timeLineList.get(position).getVideoThumb(),Base64.DEFAULT);
                    //                    Bitmap bitmap = null;
                    ////                    try {
                    ////                         bitmap= retriveVideoFrameFromVideo(context.getString(R.string.s3_image_url) +
                    ////                                context.getString(R.string.s3_bucket_time_line_path) +
                    ////                                timeLineList.get(position).getMedia());
                    ////                    } catch (Throwable throwable) {
                    ////                        throwable.printStackTrace();
                    ////                    }
                    ////
                    ////                    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    ////                    //give YourVideoUrl below
                    ////                    retriever.setDataSource(context.getString(R.string.s3_image_url) +
                    ////                            context.getString(R.string.s3_bucket_time_line_path) +
                    ////                            timeLineList.get(position).getMedia(), new HashMap<String, String>());
                    ////// this gets frame at 2nd second
                    ////                    bitmap = retriever.getFrameAtTime(2000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
                    //
                    //                    ThumbnailExtract thumbnailExtract=new ThumbnailExtract(context.getString(R.string.s3_image_url) +
                    //                            context.getString(R.string.s3_bucket_time_line_path) +
                    //                            timeLineList.get(position).getMedia(),holder.videoPlayerStandard.thumbImageView,true);
                    //                    thumbnailExtract.execute();
                    //                    Glide.with(context).load(bitmap)
                    //                            .into(holder.videoPlayerStandard.thumbImageView);
                    //                } else {
                    //                    Glide.with(context).load(R.mipmap.icon_video_thumbnail)
                    //                            .into(holder.videoPlayerStandard.thumbImageView);
                    //                }

                    //                Glide.with(context).load(context.getString(R.string.s3_image_url) +
                    //                        context.getString(R.string.s3_bucket_time_line_path) +
                    //                        timeLineList.get(position).getMedia())
                    //                        .into(holder.videoPlayerStandard.thumbImageView);
                }

            } else {
                holder.imageTime.setVisibility(View.VISIBLE);
                holder.videoPlayerStandard.setVisibility(View.INVISIBLE);
                holder.youtubePlayerView.setVisibility(View.INVISIBLE);
                universalImageLoader(holder.imageTime, " ",
                        " ", R.mipmap.icon_loading_image_400x100,
                        R.mipmap.icon_no_image_400x200);
            }
        } else {
            holder.imageTime.setVisibility(View.INVISIBLE);
            holder.videoPlayerStandard.setVisibility(View.INVISIBLE);
            holder.youtubePlayerView.setVisibility(View.VISIBLE);
            playYouTubeVideo(timeLineList.get(position).getMedia(), holder.youtubePlayerView);

        }

        holder.textLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timeLineList.get(position).getLikeStatus() == 0)
//                    like
                    putLikeStatus(holder.textLike, 1, position, holder.textLike);
                else if (timeLineList.get(position).getLikeStatus() == 1)
//                    dislike
                    putLikeStatus(holder.textLike, 0, position, holder.textLike);


            }
        });

        if (timeLineList.get(position).getShareCount() > 99) {
            holder.textShar.setText("(99+)");
        } else if (timeLineList.get(position).getShareCount() == 0) {
            holder.textShar.setText("");
        } else {
            holder.textShar.setText("(" + timeLineList.get(position).getShareCount() + ")");
        }

        holder.textShar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addShareCount(position, holder.textShar);
            }
        });
        holder.textCom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, CommentActivity.class);
                intent.putExtra("timeLineID", timeLineList.get(position).getTimeLineID());
                context.startActivity(intent);
//                Fragment commentFragment = new CommentFragment();
//                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
//                fragmentManager.beginTransaction()
//                        .replace(R.id.frame_home_container, commentFragment).addToBackStack("commentFragment").commit();
//                Bundle bundle = new Bundle();
//                bundle.putInt("timeLineID", timeLineList.get(position).getId());
//                commentFragment.setArguments(bundle);
            }
        });

        holder.imageTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TimeLineDetailPageActivity.class);
                intent.putExtra("timeLineID", timeLineList.get(position).getTimeLineID());
                context.startActivity(intent);
                timeLineFragment.isLoading = false;
                timeLineFragment.currentPage = 0;
                timeLineFragment.lastEnd = false;
                timeLineFragment.timeLineList.clear();
            }
        });
        holder.textTimeTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TimeLineDetailPageActivity.class);
                intent.putExtra("timeLineID", timeLineList.get(position).getTimeLineID());
                context.startActivity(intent);
                timeLineFragment.isLoading = false;
                timeLineFragment.currentPage = 0;
                timeLineFragment.lastEnd = false;
                timeLineFragment.timeLineList.clear();
            }
        });
        holder.textTimeDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TimeLineDetailPageActivity.class);
                intent.putExtra("timeLineID", timeLineList.get(position).getTimeLineID());
                context.startActivity(intent);
                timeLineFragment.isLoading = false;
                timeLineFragment.currentPage = 0;
                timeLineFragment.lastEnd = false;
                timeLineFragment.timeLineList.clear();
            }
        });
        holder.image_pro_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timeLineList.get(position).getUserID() != 1 && timeLineList.get(position).getUserID() != sharedPreferences.getInt(DmkConstants.USERID, 0)) {

                    BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(context).inflate(R.layout.profile_bubble, null);
                    PopupWindow popupWindow = BubblePopupHelper.create(context, bubbleLayout);
                    CircleImageView imageprofilepic = (CircleImageView) bubbleLayout.findViewById(R.id.image_pro_pic_bubble);
                    CustomTextView textpostedby = (CustomTextView) bubbleLayout.findViewById(R.id.text_posted_by_bubble);
                    CustomTextView textdesgination = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_designation_bubble);
                    CustomTextView textdistrict = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_district_bubble);
                    CustomTextView textconstituency = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_constituency_bubble);
                    CustomTextView textPartBoothWardVillage = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_common_bubble);
                    switch (timeLineList.get(position).getLevel()) {
                        case 1:
//                            செயலாளர்
//                            துணை செயலாளர்
//                            மண்டல ஒருங்கிணைப்பாளர்
                            textpostedby.setText(timeLineList.get(position).getFirstName() + " " +
                                    timeLineList.get(position).getLastName());
                            textdesgination.setText(timeLineList.get(position).getDesignation());
                            textdistrict.setVisibility(View.GONE);
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 2:
                            //மாவட்ட ஒருங்கிணைப்பாளர் district
                            textpostedby.setText(timeLineList.get(position).getFirstName() + " " +
                                    timeLineList.get(position).getLastName());
                            textdesgination.setText(timeLineList.get(position).getDesignation());
                            textdistrict.setText(timeLineList.get(position).getDistrict());
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 3:
                            //தொகுதி ஒருங்கிணைப்பாளர் constituency
                            textpostedby.setText(timeLineList.get(position).getFirstName() + " " +
                                    timeLineList.get(position).getLastName());
                            textdesgination.setText(timeLineList.get(position).getDesignation());
                            textdistrict.setText(timeLineList.get(position).getDistrict());
                            textconstituency.setText(timeLineList.get(position).getConstituency());
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                            //பகுதி ஒருங்கிணைப்பாளர் part
                            //நகராட்சி ஒருங்கிணைப்பாளர் municipality
                            //பேரூராட்சி ஒருங்கிணைப்பாளர் township
                            //ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                            textpostedby.setText(timeLineList.get(position).getFirstName() + " " +
                                    timeLineList.get(position).getLastName());
                            textdesgination.setText(timeLineList.get(position).getDesignation());
                            textdistrict.setText(timeLineList.get(position).getDistrict());
                            textconstituency.setText(timeLineList.get(position).getConstituency());
                            textPartBoothWardVillage.setText(timeLineList.get(position).getPart());
                            break;
                        case 5:
                        case 6:
                        case 9:
                        case 10:
                        case 13:
                        case 14:
                        case 18:
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            //கிளை ஒருங்கிணைப்பாளர் ward
                            // வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //கிளை ஒருங்கிணைப்பாளர்  ward
                            textpostedby.setText(timeLineList.get(position).getFirstName() + " " +
                                    timeLineList.get(position).getLastName());
                            textdesgination.setText(timeLineList.get(position).getDesignation());
                            textdistrict.setText(timeLineList.get(position).getDistrict());
                            textconstituency.setText(timeLineList.get(position).getConstituency());
                            textPartBoothWardVillage.setText(timeLineList.get(position).getWard());
                            break;
                        case 7:
                        case 11:
                        case 15:
                        case 19:
                            //வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                            textpostedby.setText(timeLineList.get(position).getFirstName() + " " +
                                    timeLineList.get(position).getLastName());
                            textdesgination.setText(timeLineList.get(position).getDesignation());
                            textdistrict.setText(timeLineList.get(position).getDistrict());
                            textconstituency.setText(timeLineList.get(position).getConstituency());
                            textPartBoothWardVillage.setText(timeLineList.get(position).getBooth());
                            break;
                        case 17:
                            //ராட்சிஊ ஒருங்கிணைப்பாளர் villagepanchayat
                            textpostedby.setText(timeLineList.get(position).getFirstName() + " " +
                                    timeLineList.get(position).getLastName());
                            textdesgination.setText(timeLineList.get(position).getDesignation());
                            textdistrict.setText(timeLineList.get(position).getDistrict());
                            textconstituency.setText(timeLineList.get(position).getConstituency());
                            textPartBoothWardVillage.setText(timeLineList.get(position).getVillage());
                            break;


                    }


                    universalImageLoader(imageprofilepic, context.getString(R.string.s3_bucket_profile_path),
                            timeLineList.get(position).getProfilePicture(), R.mipmap.icon_pro_image_loading_256,
                            R.mipmap.icon_pro_image_loading_256);
                    final Random random = new Random();
                    int[] location = new int[2];
                    holder.image_pro_pic.getLocationInWindow(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP);
                    popupWindow.showAtLocation(v, Gravity.NO_GRAVITY, location[0], v.getHeight() + location[1]);
                }


            }
        });


    }


    //    public static boolean isAudioFile(String path) {
//        String mimeType = URLConnection.guessContentTypeFromName(path);
//        return mimeType != null && mimeType.endsWith(".mp3");
//    }
    public class ThumbnailExtract extends AsyncTask<String, long[], Bitmap> {

        private final String videoUrl;
        private final ImageView mThumbnail;
        private final boolean mIsVideo;
        private MediaMetadataRetriever mmr;

        Bitmap bitmap;

        public ThumbnailExtract(String videoLocalUrl, ImageView thumbnail, boolean isVideo) {
            this.videoUrl = videoLocalUrl;
            mThumbnail = thumbnail;
            Glide.with(context).load(R.mipmap.icon_video_thumbnail)
                    .into(mThumbnail);
            mIsVideo = isVideo;
            if (!isVideo) {
                mmr = new MediaMetadataRetriever();
            }
        }

        @Override
        protected Bitmap doInBackground(String... params) {


            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            //give YourVideoUrl below
            retriever.setDataSource(videoUrl, new HashMap<String, String>());
// this gets frame at 2nd second
            return bitmap = retriever.getFrameAtTime(4000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);


        }

        @Override
        protected void onPostExecute(Bitmap thumb) {
            if (thumb != null) {
                mThumbnail.setImageBitmap(thumb);

            }
        }


    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {
        Bitmap bitmap = null;

        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime(4000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public boolean isAudioFile(String fileNamePath) {
        for (int i = 0; i < audioFormat.length; i++) {
            if (getFileExt(fileNamePath).equalsIgnoreCase(audioFormat[i])) {
                return true;
            }
        }
        return false;
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }

    private Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void putLikeStatus(final TextView textLike, final int likeStatus, final int position, final TextView textLikeCount) {
        timeLineLikeParams = new TimeLineLikeParams();
        timeLineLikeParams.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        timeLineLikeParams.setStatus(likeStatus);
        timeLineLikeParams.setTimelineid(timeLineList.get(position).getTimeLineID());
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.setTimelinelike(headerMap, timeLineLikeParams).enqueue(new Callback<TimeLineLikeResponse>() {
                @Override
                public void onResponse(Call<TimeLineLikeResponse> call, Response<TimeLineLikeResponse> response) {
                    baseActivity.hideProgress();
                    timeLineLikeResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (timeLineLikeResponse.getStatuscode() == 0) {
                            if (likeStatus == 0) {
                                //                            dislike
                                textLike.setTextColor(context.getResources().getColor(R.color.drak_grey_one));
                                textLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_like, 0, 0, 0);
                                timeLineList.get(position).setLikeStatus(0);
                                timeLineList.get(position).setLikeCount(timeLineList.get(position).getLikeCount() - 1);
                                if (timeLineList.get(position).getLikeCount() > 99) {
                                    textLikeCount.setText("(99+)");
                                } else if (timeLineList.get(position).getLikeCount() == 0) {
                                    textLikeCount.setText("");
                                } else {
                                    textLikeCount.setText("(" + timeLineList.get(position).getLikeCount() + ")");
                                }
//                                notifyDataSetChanged();
                            } else if (likeStatus == 1) {
                                //                            like
                                textLike.setTextColor(context.getResources().getColor(R.color.red));
                                textLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_like_red, 0, 0, 0);
                                timeLineList.get(position).setLikeStatus(1);
                                timeLineList.get(position).setLikeCount(timeLineList.get(position).getLikeCount() + 1);
                                if (timeLineList.get(position).getLikeCount() > 99) {
                                    textLikeCount.setText("(99+)");
                                } else if (timeLineList.get(position).getLikeCount() == 0) {
                                    textLikeCount.setText("");
                                } else {
                                    textLikeCount.setText("(" + timeLineList.get(position).getLikeCount() + ")");
                                }
//                                notifyDataSetChanged();

                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, timeLineLikeResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, timeLineLikeResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(timeLineLikeResponse.getSource(),
                                    timeLineLikeResponse.getSourcedata()));
                            editor.commit();
                            putLikeStatus(textLike, likeStatus, position, textLikeCount);

                        }

                    } else {
                        Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TimeLineLikeResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void addShareCount(final int position, final TextView textShareCount) {
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            ShareCount shareCount = new ShareCount();
            shareCount.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            shareCount.setMainid(timeLineList.get(position).getTimeLineID());
            shareCount.setType(1);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.addShareCount(headerMap, shareCount).enqueue(new Callback<ShareCountResponse>() {
                @Override
                public void onResponse(Call<ShareCountResponse> call, Response<ShareCountResponse> response) {
                    baseActivity.hideProgress();
                    shareCountResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (shareCountResponse.getStatuscode() == 0) {
                            timeLineList.get(position).setShareCount(timeLineList.get(position).getShareCount() + 1);
                            if (timeLineList.get(position).getShareCount() > 99) {
                                textShareCount.setText("(99+)");
                            } else if (timeLineList.get(position).getShareCount() == 0) {
                                textShareCount.setText("");
                            } else {
                                textShareCount.setText("(" + timeLineList.get(position).getShareCount() + ")");
                            }
                            String ps = "Dmk_encryption " + timeLineList.get(position).getTimeLineID();
                            String tmp = com.firebase.client.utilities.Base64.encodeBytes(ps.getBytes());
                            tmp = com.firebase.client.utilities.Base64.encodeBytes(tmp.getBytes());
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_TEXT, timeLineList.get(position).getTitle() + "\n" + context.getString(R.string.for_more) +
                                    context.getString(R.string.deeplink_url) + "timeline/" + tmp);
                            context.startActivity(Intent.createChooser(i, "Choose share option"));
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, shareCountResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, shareCountResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(shareCountResponse.getSource(),
                                    shareCountResponse.getSourcedata()));
                            editor.commit();
                            addShareCount(position, textShareCount);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ShareCountResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    public String getEncodedHeader(String sourceResponse, String sourceDataResponse) {

        byte[] sourceBase64 = Base64.decode(sourceResponse, Base64.NO_WRAP);
        byte[] sourceDataBase64 = Base64.decode(sourceDataResponse, Base64.NO_WRAP);
        String source = new String(sourceBase64, StandardCharsets.UTF_8);
        String sourceData = new String(sourceDataBase64, StandardCharsets.UTF_8);
        String data = source.substring(0, 12) + sourceData.substring(0, 8);
        String key = source.substring(12, 19) + sourceData.substring(8, 13);
        String iv = source.substring(19, 28) + sourceData.substring(13, 20);
        String encrpytData = Java_AES_Cipher.encrypt(key, iv, data);
        byte[] dataBase64Encode = encrpytData.getBytes(StandardCharsets.UTF_8);
        String base64 = Base64.encodeToString(dataBase64Encode, Base64.NO_WRAP);
        return base64;
    }

    public Bitmap createThumbnailFromPath(String filePath, int type) {
        return ThumbnailUtils.createVideoThumbnail(filePath, type);
    }

    @Override
    public int getItemCount() {
        return timeLineList.size();
    }


    public class TimeLineViewHolder extends RecyclerView.ViewHolder {

        CardView cardTimeLine;
        View viewTimeLine;
        TextView textTimeTitle;
        TextView textTimeDescription;
        TextView textLike;
        TextView textCom;
        TextView textShar;
        TextView textPostDate;
        TextView textPostedBy;
        //        TextView textLikeCount;
//        TextView textCommentCount;
        TextView textDesignation;
        ImageView imageTime;
        ImageView imageDelete;
        ImageView imageEdit;
        ImageView imagemenu, image_pro_pic;
        ConstraintLayout conLikeShareComment;
        ConstraintLayout consItemTimeLine;
        ConstraintLayout consTimeLine;
        JZVideoPlayerStandard videoPlayerStandard;
        YouTubePlayerView youtubePlayerView;
//        PlayerView exoPlayerView;

        public TimeLineViewHolder(View itemView) {
            super(itemView);
            imageTime = (ImageView) itemView.findViewById(R.id.image_time);
            image_pro_pic = (ImageView) itemView.findViewById(R.id.image_pro_pic);
            imageDelete = (ImageView) itemView.findViewById(R.id.image_delete);
            imageEdit = (ImageView) itemView.findViewById(R.id.image_edit);
            textTimeTitle = (TextView) itemView.findViewById(R.id.text_time_title);
//            textLikeCount = (TextView) itemView.findViewById(R.id.text_like_count);
//            textCommentCount = (TextView) itemView.findViewById(R.id.text_comment_count);
            textTimeDescription = (TextView) itemView.findViewById(R.id.text_time_description);
            textDesignation = (TextView) itemView.findViewById(R.id.text_designation);
            textLike = (TextView) itemView.findViewById(R.id.text_like);
            textCom = (TextView) itemView.findViewById(R.id.text_com);
            textShar = (TextView) itemView.findViewById(R.id.text_share);
            textPostDate = (TextView) itemView.findViewById(R.id.text_post_date);
            textPostedBy = (TextView) itemView.findViewById(R.id.text_posted_by);
            viewTimeLine = (View) itemView.findViewById(R.id.view_time_line);
            consTimeLine = (ConstraintLayout) itemView.findViewById(R.id.cons_time_line);
            consItemTimeLine = (ConstraintLayout) itemView.findViewById(R.id.cons_item_time_line);
            conLikeShareComment = (ConstraintLayout) itemView.findViewById(R.id.cons_like_com_share);
            cardTimeLine = (CardView) itemView.findViewById(R.id.card_time_line);
            videoPlayerStandard = (JZVideoPlayerStandard) itemView.findViewById(R.id.videoplayer_time);
            videoPlayerStandard.setSystemTimeAndBattery();
            imagemenu = (ImageView) itemView.findViewById(R.id.image_menu);
            youtubePlayerView = (YouTubePlayerView) itemView.findViewById(R.id.youtube_player_view);
//            exoPlayerView = (PlayerView) itemView.findViewById(R.id.exo_video_view);


        }
    }

    private void deleteTimeLine(final int timeLineID) {
        TimeLineDelete timeLineDelete = new TimeLineDelete();
        timeLineDelete.setTimelineid(timeLineID);
        timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(context.getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.deleteTimeLine(headerMap, timeLineDelete).enqueue(new Callback<TimeLineDeleteResponse>() {
                @Override
                public void onResponse(Call<TimeLineDeleteResponse> call, Response<TimeLineDeleteResponse> response) {
                    baseActivity.hideProgress();
                    timeLineDeleteResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (timeLineDeleteResponse.getStatuscode() == 0) {
                            if (timeLineDeleteResponse.getStatus().contains("Success")) {
                                Toast.makeText(context, context.getString(R.string.timeline_delete_sucuess), Toast.LENGTH_SHORT).show();
                                timeLineFragment.getTimeLineList();
                            } else {
                                Toast.makeText(context, context.getString(R.string.timeline_delete_failed), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, timeLineDeleteResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, timeLineDeleteResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(timeLineDeleteResponse.getSource(),
                                    timeLineDeleteResponse.getSourcedata()));
                            editor.commit();
                            deleteTimeLine(timeLineID);
                        }
                    } else {
                        Toast.makeText(context, context.getString(R.string.timeline_delete_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TimeLineDeleteResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context, context.getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }


    public boolean isImageFile(String fileNamePath) {
        for (int i = 0; i < imageFormat.length; i++) {
            if (getFileExt(fileNamePath).equalsIgnoreCase(imageFormat[i])) {
                return true;
            }
        }
        return false;
    }

    private void playYouTubeVideo(final String videoID, YouTubePlayerView youTubePlayerView) {
//        final String videoID = getVideoId(videoUrl);
        baseActivity.getLifecycle().addObserver(youTubePlayerView);
        youTubePlayerView.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        if (videoID.length() > 0) {
                            initializedYouTubePlayer.loadVideo(videoID, 0);
                        }

                    }
                });
            }
        }, true);
    }

//    public String getVideoId(String videoUrl) {
//        if (videoUrl == null || videoUrl.trim().length() <= 0) {
//            return null;
//        }
//        Pattern pattern = Pattern.compile(expression);
//        Matcher matcher = pattern.matcher(videoUrl);
//        try {
//            if (matcher.find())
//                return matcher.group();
//        } catch (ArrayIndexOutOfBoundsException ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }

//    private void initializeExoPlayer(PlayerView playerView, String url) {
//        exoPlayer = ExoPlayerFactory.newSimpleInstance(
//                new DefaultRenderersFactory(context),
//                new DefaultTrackSelector(), new DefaultLoadControl());
//        playerView.setPlayer(exoPlayer);
////        exoPlayer.setPlayWhenReady(true);
////        exoPlayer.getPlaybackState();
////        exoPlayer.setPlayWhenReady(playWhenReady);
////        player.seekTo(currentWindow, playbackPosition);
//        Uri uri = Uri.parse(url);
//        MediaSource mediaSource = buildMediaSource(uri);
//        exoPlayer.prepare(mediaSource, true, false);
//    }

//    private MediaSource buildMediaSource(Uri uri) {
//        return new ExtractorMediaSource.Factory(
//                new DefaultHttpDataSourceFactory("exoplayer-codelab")).
//                createMediaSource(uri);
//    }
//
//    private void hideSystemUi(PlayerView playerView) {
//        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
//                | View.SYSTEM_UI_FLAG_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
//    }

//    private void releasePlayer() {
//        if (exoPlayer != null) {
////            playbackPosition = player.getCurrentPosition();
////            currentWindow = player.getCurrentWindowIndex();
////            playWhenReady = exoPlayer.getPlayWhenReady();
////            exoPlayer.release();
////            exoPlayer = null;
////            exoPlayer.setPlayWhenReady(false);
////            exoPlayer.getPlaybackState();
//        }
//    }

    //    @Override
//    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
//        super.onDetachedFromRecyclerView(recyclerView);
//        releasePlayer();
//
//    }
//
//    @Override
//    public void onViewDetachedFromWindow(@NonNull TimeLineViewHolder holder) {
//        super.onViewDetachedFromWindow(holder);
//        releasePlayer();
//    }
////
//    @Override
//    public void onViewRecycled(@NonNull TimeLineViewHolder holder) {
//        super.onViewRecycled(holder);
//        if (exoPlayer != null)
//            holder.exoPlayerView.getPlayer().release();
//    }


}
