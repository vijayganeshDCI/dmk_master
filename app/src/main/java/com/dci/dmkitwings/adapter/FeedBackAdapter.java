package com.dci.dmkitwings.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.BaseActivity;
import com.dci.dmkitwings.activity.FeedBackActivity;
import com.dci.dmkitwings.fragment.FeedbackDetailsFragment;
import com.dci.dmkitwings.fragment.TimeLineFragment;
import com.dci.dmkitwings.model.FeedBackInputParam;
import com.dci.dmkitwings.model.TimeLineDeleteResponse;
import com.dci.dmkitwings.model.TimeLineInputParam;
import com.dci.dmkitwings.model.TimeLineLikeParams;
import com.dci.dmkitwings.retrofit.DmkAPI;

import java.util.List;

import javax.inject.Inject;

import static com.dci.dmkitwings.app.DmkApplication.getProxy;

/**
 * Created by vijayaganesh on 4/2/2018.
 */

public class FeedBackAdapter extends RecyclerView.Adapter<FeedBackAdapter.TimeLineViewHolder> {


    List<FeedBackInputParam> feedbackList;
    Context context;
    HttpProxyCacheServer proxy;
    FeedBackActivity feedBackActivity;
    public FeedBackAdapter(List<FeedBackInputParam> feedbackList, Context context, FeedBackActivity feedBackActivity) {
        this.feedbackList = feedbackList;
        this.context = context;
        this.feedBackActivity=feedBackActivity;
        }


    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_feedback, parent, false);
        proxy = getProxy(context);
        return new TimeLineViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TimeLineViewHolder holder, final int position) {
        holder.textFeedbackTitle.setText(feedbackList.get(position).getCategoryid());
        holder.textFeedbackDescription.setText(feedbackList.get(position).getFeedback());
        holder.textFeedbackDate.setText(feedbackList.get(position).getCreated_at());
        if (feedbackList.get(position).getStatus().equals("Reviewed"))
        {
            holder.textFeedstatus.setText(feedbackList.get(position).getStatus());
            holder.textFeedstatus.setTextColor(Color.GREEN);
        }
        else
        {
            holder.textFeedstatus.setText(feedbackList.get(position).getStatus());
            holder.textFeedstatus.setTextColor(Color.RED);
        }

        holder.textFeedbackDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedbackDetailsFragment feedbackDetailsFragment=new FeedbackDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("status", feedbackList.get(position).getStatus());
                bundle.putString("feedback", feedbackList.get(position).getFeedback());
                bundle.putString("categoryid", feedbackList.get(position).getCategoryid());
                bundle.putString("date",  feedbackList.get(position).getCreated_at());
                feedbackDetailsFragment.setArguments(bundle);
                feedBackActivity.push(feedbackDetailsFragment,"FeedbackDetailsFragment");
            }
        });

        }




    @Override
    public int getItemCount() {
        return feedbackList.size();
    }


    public class TimeLineViewHolder extends RecyclerView.ViewHolder {

        TextView textFeedbackTitle;
        TextView textFeedbackDescription;
        TextView textFeedbackDate;
        TextView textFeedstatus;
        public TimeLineViewHolder(View itemView) {
            super(itemView);
            textFeedbackTitle = (TextView) itemView.findViewById(R.id.text_feedtitle);
            textFeedbackDescription = (TextView) itemView.findViewById(R.id.text_feedbackdescription);
            textFeedbackDate = (TextView) itemView.findViewById(R.id.text_feedbacktime);
            textFeedstatus=(TextView)itemView.findViewById(R.id.text_feedstatus);

        }
    }


}
