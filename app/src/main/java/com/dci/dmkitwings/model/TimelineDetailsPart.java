package com.dci.dmkitwings.model;

public class TimelineDetailsPart
{
    private int id;
    private String partname;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPartname() {
        return partname;
    }

    public void setPartname(String partname) {
        this.partname = partname;
    }
}
