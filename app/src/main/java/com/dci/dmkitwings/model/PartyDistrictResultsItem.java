package com.dci.dmkitwings.model;

public class PartyDistrictResultsItem {
	public int getParentID() {
		return ParentID;
	}

	public void setParentID(int parentID) {
		ParentID = parentID;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public String getDistrictName() {
		return DistrictName;
	}

	public void setDistrictName(String districtName) {
		DistrictName = districtName;
	}

	public String getDistrictTwitterID() {
		return DistrictTwitterID;
	}

	public void setDistrictTwitterID(String districtTwitterID) {
		DistrictTwitterID = districtTwitterID;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDistrictFBLink() {
		return DistrictFBLink;
	}

	public void setDistrictFBLink(String districtFBLink) {
		DistrictFBLink = districtFBLink;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getDistrictDescriprtion() {
		return DistrictDescriprtion;
	}

	public void setDistrictDescriprtion(String districtDescriprtion) {
		DistrictDescriprtion = districtDescriprtion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private int ParentID;
	private int Status;
	private String DistrictName;
	private String DistrictTwitterID;
	private String updated_at;
	private String DistrictFBLink;
	private String created_at;
	private String DistrictDescriprtion;

	public PartyDistrictResultsItem(String districtName, int id) {
		DistrictName = districtName;
		this.id = id;
	}

	private int id;
}
