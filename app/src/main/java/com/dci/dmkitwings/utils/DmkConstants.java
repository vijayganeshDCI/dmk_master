package com.dci.dmkitwings.utils;

/**
 * Created by vijayaganesh on 10/23/2017.
 */

public class DmkConstants {

    public static final String FCMTOKEN="FcmToken";
    public static final String FCMKEYSHAREDPERFRENCES="FCMKEYSHAREDPERFRENCES";
    public static final String DEVICEID="DeviceID";
    public static final String APPID="APPID";
    public static final String APPVERSION_CODE ="APPVERSION_CODE";
    public static final String APPVERSION ="APPVERSION";
    public static final String OS_VERSION="OS_VERSION";
    public static final String SELECTEDLANGUAGES="SELECTEDLANGUAGES";
    public static final String LOGINSTATUS="LOGINSTATUS";
    public static final String IMEIID="ImeiID";
    public static final String PHONENUMBER="PHONENUMBER";
    public static final String UNIQUEDMKID="UNIQUEDMKID";
    public static final String FIRSTNAME="FIRSTNAME";
    public static final String LASTNAME="LASTNAME";
    public static final String MOBILENUMBER="MOBILENUMBER";
    public static final String DESGINATION="DESGINATION";
    public static final String USERID = "USERID";
    public static final String USERDISTRICTNAME = "USERDISTRICTNAME";
    public static final String USERWINGNAME = "USERWINGNAME";
    public static final int PARTY_USERTYPE=1;
    public static final String INTENTTYPE="INTENTTYPE";
    public static final String USERAGE="USERAGE";
    public static final String USER_DISTRICT_ID="USER_DISTRICT_ID";
    public static final String USER_PARTY_DISTRICT_ID="USER_PARTY_DISTRICT_ID";
    public static final String USER_CONSTITUENCY_ID="USER_CONSTITUENCY_ID";
    public static final String USER_UNION_TYPE_ID="USER_UNION_TYPE_ID";
    public static final String USER_DIVISION_ID="USER_DIVISION_ID";
    public static final String USER_PART_ID="USER_PART_ID";
    public static final String USER_VATTAM_ID="USER_VATTAM_ID";
    public static final String USER_WARD_ID="USER_WARD_ID";
    public static final String USER_MUNICLIPALITY_ID="USER_MUNICLIPALITY_ID";
    public static final String USER_TOWNSHIP_ID="USER_TOWNSHIP_ID";
    public static final String USER_VILLAGE_PANCHAYAT_ID="USER_VILLAGE_PANCHAYAT_ID";
    public static final String USER_PANCHAYAT_UNION_ID="USER_PANCHAYAT_UNION_ID";
    public static final String USER_BOOTH_ID="USER_BOOTH_ID";
    public static final String SUPERIORID="SUPERIORID";
    public static final String DESIGNATIONID="DESIGNATIONID";
    public static final String USERTTYPE="USERTTYPE";
    public static final String PROFILE_PICTURE="PROFILE_PICTURE";
    public static final String USERPERMISSION_COMMON="Common";
    public static final String USERPERMISSION_ARTICLE="Article";
    public static final String USERPERMISSION_EVENTS="Events";
    public static final String USERPERMISSION_NEWS="News";
    public static final String USERPERMISSION_ELECTION="Election";
    public static final String USER_ADDRESS="Address";
    public static final String USER_VOTER_ID="VoterID";
    public static final String ROLL_ID="ROLL_ID";

    public static final String INSTANCE_ID_TOKEN_RETRIEVED = "iid_token_retrieved";
    public static final String FRIENDLY_MSG_LENGTH = "friendly_msg_length";

    public static final int COMPOSE_TITLE_LIMIT=200;
    public static final int FEEDBACK_COMPOSE_TITLE_LIMIT=50;
    public static final String HEADER="HEADER";
    public static final String HEADER_SOURCE="HEADER_SOURCE";
    public static final String HEADER_SOURCE_DATA="HEADER_SOURCE_DATA";




}
