package com.dci.dmkitwings.utils;

/**
 * Created by vijayaganesh on 4/19/2018.
 */

public interface LanguageListener {

    public void setLanguage(String language);

}
