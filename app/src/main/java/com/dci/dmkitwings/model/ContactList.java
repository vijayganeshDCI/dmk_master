package com.dci.dmkitwings.model;

/**
 * Created by vijayaganesh on 4/12/2018.
 */

public class ContactList {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ContactList(String name, int id) {
        this.name = name;
        this.designation = designation;
        this.id = id;
        this.proPic = proPic;
    }

    private String name;
    private String designation;
    private int id;

    public int getProPic() {
        return proPic;
    }

    public void setProPic(int proPic) {
        this.proPic = proPic;
    }

    private int proPic;


}
