package com.dci.dmkitwings.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.activity.HomeActivity;
import com.dci.dmkitwings.adapter.NewsBottomViewPagerAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.NewsListParams;
import com.dci.dmkitwings.model.NewsListResponse;
import com.dci.dmkitwings.model.UserDetailsResponse;
import com.dci.dmkitwings.model.UserStatusParam;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.Util;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aurelhubert.ahbottomnavigation.AHBottomNavigation.TitleState.ALWAYS_SHOW;

/**
 * Created by vijayaganesh on 3/15/2018.
 */

public class NewsFragment extends BaseFragment {


    @BindView(R.id.view_pager_news)
    AHBottomNavigationViewPager viewPagerNews;
    @BindView(R.id.bottom_navigation_news)
    AHBottomNavigation bottomNavigationNews;
    @BindView(R.id.cons_news)
    ConstraintLayout consNews;
    Unbinder unbinder;
     AHBottomNavigationAdapter navigationAdapter;
     NewsBottomViewPagerAdapter newsBottomViewPagerAdapter;
    private Fragment currentFragment;
    private Handler handler = new Handler();
    HomeActivity homeActivity;
    private Boolean isStarted=false;
    private Boolean isVisible=false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        unbinder = ButterKnife.bind(this, view);
        homeActivity = (HomeActivity) getActivity();
        bottomNavigationNews.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {

                if (currentFragment == null) {
                    currentFragment = newsBottomViewPagerAdapter.getCurrentFragment();
                }
                viewPagerNews.setCurrentItem(position, true);

                if (currentFragment == null) {
                    return true;
                }

                currentFragment = newsBottomViewPagerAdapter.getCurrentFragment();

                return true;
            }
        });

        viewPagerNews.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                bottomNavigationNews.setCurrentItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
//        handleBackPress(view);
        return view;
    }
//
//    private void handleBackPress(View view) {
//        view.setFocusableInTouchMode(true);
//        view.requestFocus();
//        view.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    if (viewPagerNews.getCurrentItem() != 1)
//                        viewPagerNews.setCurrentItem(1);
//                    else
//                        getActivity().onBackPressed();
//                    return true;
//                }
//                return false;
//            }
//        });
//    }


//    @Override
//    public void setMenuVisibility(boolean menuVisible) {
//        super.setMenuVisibility(menuVisible);
//        if (menuVisible) {
//
//            bottomNavigationUIProperties();
//        }
//    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        if (isVisible) {
            bottomNavigationUIProperties();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            bottomNavigationUIProperties();

        }

    }

    private void bottomNavigationUIProperties() {

        if (bottomNavigationNews!=null) {
            showOrHideBottomNavigation(true);
            bottomNavigationNews.setTitleState(ALWAYS_SHOW);
//        Active Color
            bottomNavigationNews.setAccentColor(getResources().getColor(R.color.red));
//        In Active Color
            bottomNavigationNews.setInactiveColor(getResources().getColor(R.color.hint_grey));
            navigationAdapter = new AHBottomNavigationAdapter(homeActivity, R.menu.bottom_news);
            navigationAdapter.setupWithBottomNavigation(bottomNavigationNews);

            newsBottomViewPagerAdapter = new NewsBottomViewPagerAdapter(getChildFragmentManager());
            viewPagerNews.setAdapter(newsBottomViewPagerAdapter);

            switch (homeActivity.bottomPosition)
            {
                case 0:
                    bottomNavigationNews.setCurrentItem(0);
                    viewPagerNews.setCurrentItem(0);
                    viewPagerNews.setPagingEnabled(true);
                    homeActivity.bottomPosition=1;
                    break;
                case 1:
                    bottomNavigationNews.setCurrentItem(1);
                    viewPagerNews.setCurrentItem(1);
                    viewPagerNews.setPagingEnabled(true);
                    homeActivity.bottomPosition=1;
                    break;
                case 2:
                    bottomNavigationNews.setCurrentItem(2);
                    viewPagerNews.setCurrentItem(2);
                    viewPagerNews.setPagingEnabled(true);
                    homeActivity.bottomPosition=1;
                    break;
                case 3:
                    bottomNavigationNews.setCurrentItem(3);
                    viewPagerNews.setCurrentItem(3);
                    viewPagerNews.setPagingEnabled(true);
                    homeActivity.bottomPosition=1;
                    break;
                case 4:
                    bottomNavigationNews.setCurrentItem(4);
                    viewPagerNews.setCurrentItem(4);
                    viewPagerNews.setPagingEnabled(true);
                    homeActivity.bottomPosition=1;
                    break;
            }


        }




//        // Setting custom colors for icon_notification
//        AHNotification icon_notification = new AHNotification.Builder()
//                .setText("1")
//                .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.accept_green))
//                .setTextColor(ContextCompat.getColor(getActivity(), R.color.white))
//                .build();
//        bottomNavigationNews.setNotification(icon_notification, 1);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    /**
     * Show or hide the bottom navigation with animation
     */
    public void showOrHideBottomNavigation(boolean show) {
        if (show) {
            bottomNavigationNews.restoreBottomNavigation(true);
        } else {
            bottomNavigationNews.hideBottomNavigation(true);
        }
    }


}
