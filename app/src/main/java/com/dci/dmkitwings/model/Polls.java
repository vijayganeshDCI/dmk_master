package com.dci.dmkitwings.model;

/**
 * Created by vijayaganesh on 4/16/2018.
 */

public class Polls {

    public Polls(String question, int questionType) {
        this.question = question;
        this.questionType = questionType;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    private String question;
    private int questionType;

}
