package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.danikula.videocache.HttpProxyCacheServer;
import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.CommentAdapter;
import com.dci.dmkitwings.adapter.ImageViewPageAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.CommentList;
import com.dci.dmkitwings.model.CommentPost;
import com.dci.dmkitwings.model.CommentPostResponse;
import com.dci.dmkitwings.model.ShareCount;
import com.dci.dmkitwings.model.ShareCountResponse;
import com.dci.dmkitwings.model.TimeLineDelete;
import com.dci.dmkitwings.model.TimeLineDeleteResponse;
import com.dci.dmkitwings.model.TimeLineDetailsResponse;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomEditText;
import com.dci.dmkitwings.view.CustomTextView;
import com.dci.dmkitwings.view.CustomTextViewBold;
import com.firebase.client.utilities.Base64;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerStandard;
import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dci.dmkitwings.app.DmkApplication.getContext;
import static com.dci.dmkitwings.app.DmkApplication.getProxy;

public class TimeLineDetailPageActivity extends BaseActivity {

    @BindView(R.id.image_time)
    ImageView imageTime;
    @BindView(R.id.videoplayer_time)
    JZVideoPlayerStandard videoplayerTime;
    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    @BindView(R.id.text_posted_by)
    CustomTextView textPostedBy;
    @BindView(R.id.text_designation)
    CustomTextView textDesignation;
    @BindView(R.id.text_post_date)
    CustomTextView textPostDate;
    @BindView(R.id.text_label_time_line_title)
    CustomTextViewBold textLabelTimeLineTitle;
    @BindView(R.id.text_label_time_line_content)
    CustomTextView textLabelTimeLineContent;
    @BindView(R.id.image_call)
    ImageView imageCall;
    Unbinder unbinder;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    DmkAPI dmkAPI;
    TimeLineDetailsResponse timeLineDetailsResponse;
    int timeLineID;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.list_comment_list)
    RecyclerView listCommentList;
    @BindView(R.id.cons_time_item)
    ConstraintLayout consTimeItem;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;
    CommentAdapter commentListAdapter;
    List<CommentList> commentList;
    CommentPostResponse commentPostResponse;
    EditText editComment;
    @BindView(R.id.view_bottom_1)
    View viewBottom1;
    @BindView(R.id.text_label_Comments)
    CustomTextView textLabelComments;
    @BindView(R.id.image_pro_pic_com)
    CircleImageView imageProPicCom;
    @BindView(R.id.edit_write_comment)
    CustomEditText editWriteComment;
    @BindView(R.id.image_comment_send)
    ImageView imageCommentSend;
    @BindView(R.id.image_menu)
    ImageView imageMenu;
    //    @BindView(R.id.flipper_layout)
//    FlipperLayout flipperLayout;
    @BindView(R.id.videoplayer_detail)
    JZVideoPlayerStandard videoplayerDetail;
    @BindView(R.id.image_view_pager)
    ViewPager imageViewPager;
    @BindView(R.id.view_pager_indicator)
    CircleIndicator viewPagerIndicator;
    @BindView(R.id.image_share)
    CustomTextView textShare;
    @BindView(R.id.youtube_player_view)
    YouTubePlayerView youtubePlayerView;
    @BindView(R.id.youtube_player_view_detail)
    YouTubePlayerView youtubePlayerViewDetail;
    @BindView(R.id.text_posted_view_count)
    CustomTextView textPostedViewCount;
    private TimeLineDeleteResponse timeLineDeleteResponse;
    private HttpProxyCacheServer proxy;
    ImageViewPageAdapter imageViewPageAdapter;
    private ShareCountResponse shareCountResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_time_line_detail);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_time_line_detail, null, false);
        ButterKnife.bind(this, contentView);
        mDrawerLayout.addView(contentView, 0);
        toolbarHome.setVisibility(View.GONE);
        frameLayoutNotification.setVisibility(View.GONE);
        DmkApplication.getContext().getComponent().inject(this);
//        textTitle.setText(R.string.comment);
        commentList = new ArrayList<CommentList>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setAutoMeasureEnabled(true);
        listCommentList.setItemAnimator(new DefaultItemAnimator());
        listCommentList.setLayoutManager(linearLayoutManager);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //Navigation Control
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().hide();

        Intent bundle = getIntent();
        if (getIntent() != null)
            timeLineID = bundle.getIntExtra("timeLineID", 0);
        videoplayerTime.setVisibility(View.GONE);
        videoplayerDetail.setVisibility(View.GONE);
        proxy = getProxy(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        getTimeLineDetail();
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        if (JZVideoPlayer.backPress()) {
//            return;
//        }
//        JZVideoPlayer.releaseAllVideos();
//    }

    private void getTimeLineDetail() {
        TimeLineDelete timeLineDelete = new TimeLineDelete();
        timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        timeLineDelete.setTimelineid(timeLineID);
        commentList.clear();
        if (Util.isNetworkAvailable()) {
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getTimeLineDetails(headerMap, timeLineDelete).enqueue(new Callback<TimeLineDetailsResponse>() {
                @Override
                public void onResponse(Call<TimeLineDetailsResponse> call, Response<TimeLineDetailsResponse> response) {
                    hideProgress();
                    timeLineDetailsResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {

                        if (timeLineDetailsResponse.getStatuscode() == 0) {
                            if (timeLineDetailsResponse.getResults().getUser().getId()
                                    == sharedPreferences.getInt(DmkConstants.USERID, 0)) {
                                textPostedBy.setText("You");
                                imageMenu.setVisibility(View.VISIBLE);
                            } else {
                                textPostedBy.setText(timeLineDetailsResponse.getResults().getUser().getFirstName() + " " +
                                        timeLineDetailsResponse.getResults().getUser().getLastName());
                                imageMenu.setVisibility(View.GONE);
                            }

                            textLabelTimeLineTitle.setText(timeLineDetailsResponse.getResults().getTitle());
                            textLabelTimeLineContent.setText(timeLineDetailsResponse.getResults().getContent());
//                        textDesignation.setText(timeLineDetailsResponse.getResults().getUser().getRoles().getRolename()
//                                + " " + getDateFormat(timeLineDetailsResponse.getResults().getCreated_at()));
                            textPostDate.setText(getDateFormat(timeLineDetailsResponse.getResults().getCreated_at()));
                            universalImageLoader(imageProPic, getString(R.string.s3_bucket_profile_path),
                                    timeLineDetailsResponse.getResults().getUser().getAvatar(),
                                    R.mipmap.icon_pro_image_loading_256,
                                    R.mipmap.icon_pro_image_loading_256);
                            if (timeLineDetailsResponse.getResults().getUser().getId() != 1 && sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == 1 && timeLineDetailsResponse.getResults().getUser().getId() != sharedPreferences.getInt(DmkConstants.USERID, 0))
                                imageCall.setVisibility(View.VISIBLE);
                            else imageCall.setVisibility(View.GONE);

                            if (timeLineDetailsResponse.getResults().getViewcount() != 0) {
                                textPostedViewCount.setVisibility(View.VISIBLE);
                                textPostedViewCount.setText("" + timeLineDetailsResponse.getResults().getViewcount());
                            } else {
                                textPostedViewCount.setVisibility(View.GONE);
                            }

                            if (timeLineDetailsResponse.getResults().getSharecount() > 99) {
                                textShare.setText("(99+)");
                            } else if (timeLineDetailsResponse.getResults().getSharecount() == 0) {
                                textShare.setText("");
                            } else {
                                textShare.setText("(" + timeLineDetailsResponse.getResults().getSharecount() + ")");
                            }

                            List<String> imageFiles = new ArrayList<String>();
                            List<String> videoFile = new ArrayList<String>();


                            for (int i = 0; i < timeLineDetailsResponse.getResults().getMediapath().size(); i++) {
                                if (isImageFile(timeLineDetailsResponse.getResults().getMediapath().get(i))) {
                                    imageFiles.add(timeLineDetailsResponse.getResults().getMediapath().get(i));
                                } else {
                                    videoFile.add(timeLineDetailsResponse.getResults().getMediapath().get(i));
                                }
                            }

                            if (imageFiles.size() > 0 && videoFile.size() > 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                youtubePlayerView.setVisibility(View.INVISIBLE);
                                if (timeLineDetailsResponse.getResults().getMediatype() != 1) {
//                                    normal video
                                    videoplayerDetail.setVisibility(View.VISIBLE);
                                    youtubePlayerViewDetail.setVisibility(View.GONE);

                                    videoplayerDetail.setUp(proxy.getProxyUrl(
                                            getString(R.string.s3_image_url_download) +
                                                    getString(R.string.s3_bucket_time_line_path) +
                                                    videoFile.get(0))
                                            , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                                    videoplayerDetail.setSoundEffectsEnabled(true);
                                    //thumbnail generation
                                    if (isAudioFile(getString(R.string.s3_image_url_download) +
                                            getString(R.string.s3_bucket_time_line_path) +
                                            videoFile.get(0))) {
                                        Glide.with(TimeLineDetailPageActivity.this)
                                                .load(R.mipmap.icon_audio_thumbnail).into(videoplayerDetail.thumbImageView);
                                    } else {
                                        RequestOptions requestOptions = new RequestOptions();
                                        requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                                        requestOptions.error(R.mipmap.icon_video_thumbnail);
                                        Glide.with(getContext())
                                                .load(getString(R.string.s3_image_url_download) +
                                                        getString(R.string.s3_bucket_time_line_path) +
                                                        videoFile.get(0))
                                                .apply(requestOptions)
                                                .thumbnail(Glide.with(getContext()).load(getString(R.string.s3_image_url_download) +
                                                        getString(R.string.s3_bucket_time_line_path) +
                                                        videoFile.get(0)))
                                                .into(videoplayerDetail.thumbImageView);


                                    }
                                } else {
//                                    if youtube video
                                    videoplayerDetail.setVisibility(View.GONE);
                                    youtubePlayerViewDetail.setVisibility(View.VISIBLE);
                                    playYouTubeVideo(timeLineDetailsResponse.getResults().getMediapath().get(0), youtubePlayerViewDetail);
                                }

                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            TimeLineDetailPageActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);


                            } else if (imageFiles.size() == 0 && videoFile.size() > 0) {
                                imageViewPager.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                youtubePlayerViewDetail.setVisibility(View.GONE);

                                if (timeLineDetailsResponse.getResults().getMediatype() != 1) {
                                    videoplayerTime.setVisibility(View.VISIBLE);
                                    youtubePlayerView.setVisibility(View.INVISIBLE);
                                    videoplayerTime.setUp(proxy.getProxyUrl(getString(R.string.s3_image_url_download) +
                                                    getString(R.string.s3_bucket_time_line_path) +
                                                    videoFile.get(0))
                                            , JZVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
                                    videoplayerTime.setSystemTimeAndBattery();
                                    //thumbnail generation
                                    if (isAudioFile(getString(R.string.s3_image_url_download) +
                                            getString(R.string.s3_bucket_time_line_path) +
                                            videoFile.get(0))) {
                                        Glide.with(TimeLineDetailPageActivity.this)
                                                .load(R.mipmap.icon_audio_thumbnail).into(videoplayerTime.thumbImageView);
                                    } else {
                                        RequestOptions requestOptions = new RequestOptions();
                                        requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                                        requestOptions.error(R.mipmap.icon_video_thumbnail);
                                        Glide.with(getContext())
                                                .load(getString(R.string.s3_image_url_download) +
                                                        getString(R.string.s3_bucket_time_line_path) +
                                                        videoFile.get(0))
                                                .apply(requestOptions)
                                                .thumbnail(Glide.with(getContext()).load(getString(R.string.s3_image_url_download) +
                                                        getString(R.string.s3_bucket_time_line_path) +
                                                        videoFile.get(0)))
                                                .into(videoplayerTime.thumbImageView);
                                        //                                if (!timeLineDetailsResponse.getResults().getVideothumb().
                                        //                                        equalsIgnoreCase("0")) {
                                        //                                    Glide.with(TimeLineDetailPageActivity.this).
                                        //                                            load(timeLineDetailsResponse.getResults().getVideothumb())
                                        //                                            .into(videoplayerTime.thumbImageView);
                                        //                                } else {
                                        //                                    Glide.with(TimeLineDetailPageActivity.this).
                                        //                                            load(R.mipmap.icon_video_thumbnail).
                                        //                                            into(videoplayerTime.thumbImageView);
                                        //                                }
                                    }
                                } else {
                                    videoplayerTime.setVisibility(View.INVISIBLE);
                                    youtubePlayerView.setVisibility(View.VISIBLE);
                                    playYouTubeVideo(timeLineDetailsResponse.getResults().getMediapath().get(0), youtubePlayerView);
                                }

                            } else if (imageFiles.size() > 0 && videoFile.size() == 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                youtubePlayerView.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                youtubePlayerViewDetail.setVisibility(View.GONE);
                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            TimeLineDetailPageActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);
                                //
                            } else if (imageFiles.size() == 0 && videoFile.size() == 0) {
                                imageViewPager.setVisibility(View.VISIBLE);
                                videoplayerTime.setVisibility(View.INVISIBLE);
                                youtubePlayerView.setVisibility(View.INVISIBLE);
                                videoplayerDetail.setVisibility(View.GONE);
                                youtubePlayerViewDetail.setVisibility(View.GONE);
                                imageFiles.add("");
                                for (int i = 0; i < imageFiles.size(); i++) {
                                    imageViewPageAdapter = new ImageViewPageAdapter(
                                            TimeLineDetailPageActivity.this, imageFiles);
                                }
                                imageViewPager.setAdapter(imageViewPageAdapter);
                                viewPagerIndicator.setViewPager(imageViewPager);
                            }

                            if (timeLineDetailsResponse.getResults().getComments().size() > 0) {
                                imageProPicCom.setVisibility(View.GONE);
                                editWriteComment.setVisibility(View.GONE);
                                imageCommentSend.setVisibility(View.GONE);
                                for (int i = 0; i < timeLineDetailsResponse.getResults().getComments().size(); i++) {
                                    commentList.add(new CommentList(
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getFirstname()
                                                    + " " + timeLineDetailsResponse.getResults().getComments().get(i).getUser().getLastname(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getImage(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getComment(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getId(),
                                            timeLineID, getDateFormat(timeLineDetailsResponse.getResults().getComments().get(i).getUpdated_at()),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getId(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getDesignation(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getLevel(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getDistrict(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getConstituency(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getWard(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getPart(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getBooth(),
                                            timeLineDetailsResponse.getResults().getComments().get(i).getUser().getVillage()));
                                }
                                Collections.sort(commentList, new Comparator<CommentList>() {
                                    public int compare(CommentList obj1, CommentList obj2) {
                                        // ## Ascending order
                                        //                                return Integer.valueOf(obj2.getId()).compareTo(obj1.getId()); // To compare integer values
                                        // ## Descending order
                                        return obj1.getDate().compareToIgnoreCase(obj2.getDate()); // To compare string values
                                        // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                    }
                                });
                                commentListAdapter = new CommentAdapter(TimeLineDetailPageActivity.this, commentList, null,
                                        TimeLineDetailPageActivity.this, timeLineID);
                                commentListAdapter.notifyDataSetChanged();
                                listCommentList.setAdapter(commentListAdapter);


                            } else {
                                imageProPicCom.setVisibility(View.VISIBLE);
                                universalImageLoader(imageProPicCom, getString(R.string.s3_bucket_profile_path),
                                        sharedPreferences.getString(DmkConstants.PROFILE_PICTURE, ""),
                                        R.mipmap.icon_pro_image_loading_256,
                                        R.mipmap.icon_pro_image_loading_256);
                                editWriteComment.setVisibility(View.VISIBLE);
                                imageCommentSend.setVisibility(View.VISIBLE);
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, timeLineDetailsResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, timeLineDetailsResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(timeLineDetailsResponse.getSource(),
                                    timeLineDetailsResponse.getSourcedata()));
                            editor.commit();
                            getTimeLineDetail();
                        }

                    } else {
                        Toast.makeText(TimeLineDetailPageActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TimeLineDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(TimeLineDetailPageActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(TimeLineDetailPageActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void postComment() {
        CommentPost commentPost = new CommentPost();
        commentPost.setComment(editWriteComment.getText().toString());
        commentPost.setParentid(0);
        commentPost.setTimelineid(timeLineID);
        commentPost.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.postComment(headerMap, commentPost).enqueue(new Callback<CommentPostResponse>() {
                @Override
                public void onResponse(Call<CommentPostResponse> call, Response<CommentPostResponse> response) {
                    hideProgress();
                    commentPostResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (commentPostResponse.getStatuscode() == 0) {
                            imageProPicCom.setVisibility(View.GONE);
                            editWriteComment.setVisibility(View.GONE);
                            imageCommentSend.setVisibility(View.GONE);
                            commentList.add(new CommentList(sharedPreferences.getString(DmkConstants.FIRSTNAME, "")
                                    + " " + sharedPreferences.getString(DmkConstants.LASTNAME, ""),
                                    sharedPreferences.getString(DmkConstants.PROFILE_PICTURE, ""),
                                    commentPostResponse.getResults().getComment(),
                                    commentPostResponse.getResults().getId(),
                                    Integer.parseInt(commentPostResponse.getResults().getTimeLineID()),
                                    commentPostResponse.getResults().getUpdated_at(),
                                    Integer.parseInt(commentPostResponse.getResults().getPostedUserID()),
                                    sharedPreferences.getString(DmkConstants.DESGINATION, ""), sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0),
                                    "", "", "", "", "", ""));
                            commentListAdapter = new CommentAdapter(TimeLineDetailPageActivity.this, commentList, null,
                                    TimeLineDetailPageActivity.this, timeLineID);
                            listCommentList.setAdapter(commentListAdapter);
                            commentListAdapter.notifyDataSetChanged();
                            editWriteComment.setText("");
                            listCommentList.smoothScrollToPosition(listCommentList.getAdapter().getItemCount() - 1);
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, commentPostResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, commentPostResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(commentPostResponse.getSource(),
                                    commentPostResponse.getSourcedata()));
                            editor.commit();
                            postComment();
                        }
                    } else {
                        Toast.makeText(TimeLineDetailPageActivity.this, "Post Failed", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(TimeLineDetailPageActivity.this, "Post Failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(TimeLineDetailPageActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    @OnClick({R.id.image_menu, R.id.image_comment_send, R.id.image_share, R.id.image_call, R.id.image_pro_pic})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_menu:
                PopupMenu popup = new PopupMenu(this, imageMenu);
                //inflating menu_icon from xml resource
                popup.inflate(R.menu.comment_item_menu);
                //adding click listener


                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit:
                                //handle menu1 click
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(TimeLineDetailPageActivity.this);
                                alertDialog.setMessage(R.string.confirm_to_edit);
                                alertDialog.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(TimeLineDetailPageActivity.this, TimeLineComposeActivity.class);
                                        intent.putExtra("timeLineID", timeLineID);
                                        intent.putExtra("timeLineTitle", textLabelTimeLineTitle.getText().toString());
                                        intent.putExtra("timeLineContent", textLabelTimeLineContent.getText().toString());
                                        intent.putExtra("fromTimeLine", true);
                                        startActivity(intent);
                                    }

                                });
                                alertDialog.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog.show();
                                return true;
                            case R.id.delete:
                                //handle menu2 click
                                AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(TimeLineDetailPageActivity.this);
                                alertDialog1.setMessage(R.string.confirm_to_delete);
                                alertDialog1.setPositiveButton(R.string.Alert_Ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        deleteTimeLine(timeLineID);
                                    }

                                });
                                alertDialog1.setNegativeButton(R.string.Alert_Cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog1.show();
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
                break;
            case R.id.image_comment_send:
                if (editWriteComment.getText().toString().isEmpty())
                    editWriteComment.setError(getString(R.string.empty_comment));
                else
                    postComment();
                break;
            case R.id.image_share:
                addShareCount();
                break;
            case R.id.image_call:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + "" + timeLineDetailsResponse.getResults().getUser().getPhoneNumber()));
                startActivity(intent);
                break;
            case R.id.image_pro_pic:
                BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.profile_bubble, null);
                PopupWindow popupWindow = BubblePopupHelper.create(this, bubbleLayout);
                CircleImageView imageprofilepic = (CircleImageView) bubbleLayout.findViewById(R.id.image_pro_pic_bubble);
                CustomTextView textpostedby = (CustomTextView) bubbleLayout.findViewById(R.id.text_posted_by_bubble);
                CustomTextView textdesgination = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_designation_bubble);
                CustomTextView textdistrict = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_district_bubble);
                CustomTextView textconstituency = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_constituency_bubble);
                CustomTextView textPartBoothWardVillage = (CustomTextView) bubbleLayout.findViewById(R.id.text_post_common_bubble);
                if (timeLineDetailsResponse.getResults().getUser().getId() != 1 && timeLineDetailsResponse.getResults().getUser().getId() != sharedPreferences.getInt(DmkConstants.USERID, 0)) {

                    switch (timeLineDetailsResponse.getResults().getUser().getRoles().getLevel()) {
                        case 1:

                            // செயலாளர்
//                            துணை செயலாளர்
//                            மண்டல ஒருங்கிணைப்பாளர்
                            textpostedby.setText(timeLineDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    timeLineDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(timeLineDetailsResponse.getResults().getUser().getRoles().getRolename());
                            textdistrict.setVisibility(View.GONE);
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 2:
                            //மாவட்ட ஒருங்கிணைப்பாளர் district
                            textpostedby.setText(timeLineDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    timeLineDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(timeLineDetailsResponse.getResults().getUser().getRoles().getRolename());
                            textdistrict.setText(timeLineDetailsResponse.getResults().getUser().getDistrictdetail().getDistrictName());
                            textconstituency.setVisibility(View.GONE);
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 3:
                            //தொகுதி ஒருங்கிணைப்பாளர் constituency
                            textpostedby.setText(timeLineDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    timeLineDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(timeLineDetailsResponse.getResults().getUser().getRoles().getRolename());
                            textdistrict.setText(timeLineDetailsResponse.getResults().getUser().getDistrictdetail().getDistrictName());
                            textconstituency.setText(timeLineDetailsResponse.getResults().getUser().getConstituencydetail().getConstituencyName());
                            textPartBoothWardVillage.setVisibility(View.GONE);
                            break;
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                            //பகுதி ஒருங்கிணைப்பாளர் part
                            //நகராட்சி ஒருங்கிணைப்பாளர் municipality
                            //பேரூராட்சி ஒருங்கிணைப்பாளர் township
                            //ஒன்றியம் ஒருங்கிணைப்பாளர் panchayatunion
                            textpostedby.setText(timeLineDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    timeLineDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(timeLineDetailsResponse.getResults().getUser().getRoles().getRolename());
                            textdistrict.setText(timeLineDetailsResponse.getResults().getUser().getDistrictdetail().getDistrictName());
                            textconstituency.setText(timeLineDetailsResponse.getResults().getUser().getConstituencydetail().getConstituencyName());
                            textPartBoothWardVillage.setText(timeLineDetailsResponse.getResults().getUser().getPartdetails().getPartname());
                            break;
                        case 5:
                        case 6:
                        case 9:
                        case 10:
                        case 13:
                        case 14:
                        case 18:
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            //கிளை ஒருங்கிணைப்பாளர் ward
                            // வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //வட்டம் ஒருங்கிணைப்பாளர்  vattam
                            // கிளை ஒருங்கிணைப்பாளர் ward
                            //கிளை ஒருங்கிணைப்பாளர்  ward
                            textpostedby.setText(timeLineDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    timeLineDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(timeLineDetailsResponse.getResults().getUser().getRoles().getRolename());
                            textdistrict.setText(timeLineDetailsResponse.getResults().getUser().getDistrictdetail().getDistrictName());
                            textconstituency.setText(timeLineDetailsResponse.getResults().getUser().getConstituencydetail().getConstituencyName());
                            textPartBoothWardVillage.setText(timeLineDetailsResponse.getResults().getUser().getWarddetail().getWardname());
                            break;
                        case 7:
                        case 11:
                        case 15:
                        case 19:
                            //வாக்குசாவடி ஒருங்கிணைப்பாளர் booth
                            textpostedby.setText(timeLineDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    timeLineDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(timeLineDetailsResponse.getResults().getUser().getRoles().getRolename());
                            textdistrict.setText(timeLineDetailsResponse.getResults().getUser().getDistrictdetail().getDistrictName());
                            textconstituency.setText(timeLineDetailsResponse.getResults().getUser().getConstituencydetail().getConstituencyName());
                            textPartBoothWardVillage.setText(timeLineDetailsResponse.getResults().getUser().getBooth().getBoothname());
                            break;
                        case 17:
                            //ராட்சிஊ ஒருங்கிணைப்பாளர் villagepanchayat
                            textpostedby.setText(timeLineDetailsResponse.getResults().getUser().getFirstName() + " " +
                                    timeLineDetailsResponse.getResults().getUser().getLastName());
                            textdesgination.setText(timeLineDetailsResponse.getResults().getUser().getRoles().getRolename());
                            textdistrict.setText(timeLineDetailsResponse.getResults().getUser().getDistrictdetail().getDistrictName());
                            textconstituency.setText(timeLineDetailsResponse.getResults().getUser().getConstituencydetail().getConstituencyName());
                            textPartBoothWardVillage.setText(timeLineDetailsResponse.getResults().getUser().getVillagepanchayatdetail().getVillageName());
                            break;
                    }


                    final Random random = new Random();
                    int[] location = new int[2];
                    view.getLocationInWindow(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.LEFT);
                    popupWindow.showAtLocation(view, Gravity.AXIS_PULL_BEFORE, view.getWidth() + 25, -185);
                    universalImageLoader(imageprofilepic, getString(R.string.s3_bucket_profile_path),
                            timeLineDetailsResponse.getResults().getUser().getAvatar(),
                            R.mipmap.icon_pro_image_loading_256,
                            R.mipmap.icon_pro_image_loading_256);

                    break;
                }
        }


    }


    @Override
    public void onPause() {
        super.onPause();
        JZVideoPlayer.releaseAllVideos();
    }

    private void deleteTimeLine(final int timeLineID) {
        TimeLineDelete timeLineDelete = new TimeLineDelete();
        timeLineDelete.setTimelineid(timeLineID);
        timeLineDelete.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
        if (Util.isNetworkAvailable()) {
            showProgress();
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.deleteTimeLine(headerMap, timeLineDelete).enqueue(new Callback<TimeLineDeleteResponse>() {
                @Override
                public void onResponse(Call<TimeLineDeleteResponse> call, Response<TimeLineDeleteResponse> response) {
                    hideProgress();
                    timeLineDeleteResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (timeLineDeleteResponse.getStatuscode() == 0) {
                            if (timeLineDeleteResponse.getStatus().contains("Success")) {
                                Toast.makeText(TimeLineDetailPageActivity.this, getString(R.string.timeline_delete_sucuess), Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            } else {
                                Toast.makeText(TimeLineDetailPageActivity.this, getString(R.string.timeline_delete_failed), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, timeLineDeleteResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, timeLineDeleteResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(timeLineDeleteResponse.getSource(),
                                    timeLineDeleteResponse.getSourcedata()));
                            editor.commit();
                            deleteTimeLine(timeLineID);
                        }
                    } else {
                        Toast.makeText(TimeLineDetailPageActivity.this, getString(R.string.timeline_delete_failed), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TimeLineDeleteResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(TimeLineDetailPageActivity.this, getText(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(TimeLineDetailPageActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }
    }

    private void addShareCount() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            ShareCount shareCount = new ShareCount();
            shareCount.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            shareCount.setMainid(timeLineDetailsResponse.getResults().getId());
            shareCount.setType(1);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.addShareCount(headerMap, shareCount).enqueue(new Callback<ShareCountResponse>() {
                @Override
                public void onResponse(Call<ShareCountResponse> call, Response<ShareCountResponse> response) {
                    hideProgress();
                    shareCountResponse = response.body();
                    if (response.isSuccessful() && response.body() != null && response.code() == 200) {
                        if (shareCountResponse.getStatuscode() == 0) {
                            timeLineDetailsResponse.getResults().setSharecount(
                                    timeLineDetailsResponse.getResults().getSharecount() + 1);
                            if (timeLineDetailsResponse.getResults().getSharecount() > 99) {
                                textShare.setText("(99+)");
                            } else if (timeLineDetailsResponse.getResults().getSharecount() == 0) {
                                textShare.setText("");
                            } else {
                                textShare.setText("(" + timeLineDetailsResponse.getResults().getSharecount() + ")");
                            }
                            String ps = "Dmk_encryption " + timeLineDetailsResponse.getResults().getId();
                            String tmp = Base64.encodeBytes(ps.getBytes());
                            tmp = Base64.encodeBytes(tmp.getBytes());
                            Intent i = new Intent(Intent.ACTION_SEND);
//                i.putExtra(Intent.EXTRA_TEXT, timeLineDetailsResponse.getResults().getTitle() + "\n" +getString(R.string.for_more) +
//                        "m.dmk.in/timeline?id=" +timeLineDetailsResponse.getResults().getId());
                            i.putExtra(Intent.EXTRA_TEXT, timeLineDetailsResponse.getResults().getTitle() + "\n" + getString(R.string.for_more) +
                                    getString(R.string.deeplink_url) + "timeline/" + tmp);
                            i.setType("text/plain");
                            startActivity(Intent.createChooser(i, "Choose share option"));
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, shareCountResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, shareCountResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(shareCountResponse.getSource(),
                                    shareCountResponse.getSourcedata()));
                            editor.commit();
                            addShareCount();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ShareCountResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(TimeLineDetailPageActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(TimeLineDetailPageActivity.this, getString(R.string.network_error_message), Toast.LENGTH_SHORT).show();
        }

    }
}
