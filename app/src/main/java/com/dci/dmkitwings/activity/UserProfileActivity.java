package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.OtpFragment;
import com.dci.dmkitwings.fragment.PartyMemberCardFragment;
import com.dci.dmkitwings.fragment.RegistrationFragment;
import com.dci.dmkitwings.fragment.UserProfileFragment;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vijayaganesh on 3/26/2018.
 */

public class UserProfileActivity extends BaseActivity {

    @BindView(R.id.frame_userprofile_container)
    FrameLayout frameLoginContainer;
    @BindView(R.id.cons_userprofile)
    ConstraintLayout consLogin;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Bundle bundle;
    String Bundlename;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_userprofile, null, false);
        ButterKnife.bind(this,contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        frameLayoutNotification.setVisibility(View.GONE);
        bundle = new Bundle();
        bundle = getIntent().getExtras();
        Bundlename = bundle.getString("name");
        if (Bundlename.equals("base"))
        {
            textTitle.setText(R.string.idenditycard);
            push(new PartyMemberCardFragment());
        }
        else
        {
            textTitle.setText(R.string.userprofile);
            push(new UserProfileFragment());
        }


    }

    public void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void popAllBackstack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();
        if (backCount > 0) {
            fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_userprofile_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_userprofile_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_userprofile_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_userprofile_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base,LanguageHelper.getLanguage(base)));
    }

}
