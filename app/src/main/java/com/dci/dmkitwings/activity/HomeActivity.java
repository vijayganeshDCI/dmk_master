package com.dci.dmkitwings.activity;

import android.content.BroadcastReceiver;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.HomeViewPagerAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.fragment.ElectionFragment;
import com.dci.dmkitwings.fragment.EventsFragment;
import com.dci.dmkitwings.fragment.NewsFragment;
import com.dci.dmkitwings.fragment.TimeLineFragment;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.jzvd.JZVideoPlayer;

/**
 * Created by vijayaganesh on 3/14/2018.
 */

public class HomeActivity extends BaseActivity {


    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @BindView(R.id.tabs_home)
    TabLayout tabsHome;
    @BindView(R.id.viewpager_home)
    ViewPager viewpagerHome;
    @BindView(R.id.cons_home)
    ConstraintLayout consHome;
    HomeViewPagerAdapter homeViewPagerAdapter;
    private int backButtonCount = 0;
    public int bottomPosition = 1;
    private NotificationReceiver notificationReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_home, null, false);
        ButterKnife.bind(this, contentView);
        toolbarHome.setBackgroundColor(getResources().getColor(R.color.black));
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        textTitle.setTextColor(getResources().getColor(R.color.yellow));
        if (getIntent().getStringExtra("timeLineID") != null) {
            Intent intent = new Intent(this, TimeLineDetailPageActivity.class);
            intent.putExtra("timeLineID", Integer.parseInt(getIntent().getStringExtra("timeLineID")));
            startActivity(intent);
        } else if (getIntent().getStringExtra("newsID") != null) {
            Intent intent = new Intent(this, NewsDetailaActivity.class);
            intent.putExtra("newsID", Integer.parseInt(getIntent().getStringExtra("newsID")));
            startActivity(intent);
        } else if (getIntent().getStringExtra("eventID") != null) {
            Intent intent = new Intent(this, EventDetailsActivity.class);
            intent.putExtra("eventID", Integer.parseInt(getIntent().getStringExtra("eventID")));
            startActivity(intent);
        } else if (getIntent().getStringExtra("articleID") != null) {
            Intent intent = new Intent(this, ArticleDetailActivity.class);
            intent.putExtra("articleID", Integer.parseInt(getIntent().getStringExtra("articleID")));
            startActivity(intent);
        }
//        else if (getIntent().getIntExtra("pollID", 0) != 0) {
//            viewpagerHome.setCurrentItem(1);
//            Bundle bundle = new Bundle();
//            NewsFragment newsFragment = new NewsFragment();
//            bundle.putInt("pollID", getIntent().getIntExtra("pollID", 0));
//            newsFragment.setArguments(bundle);
//        }
        frameLayoutNotification.setVisibility(View.VISIBLE);
        editor = sharedPreferences.edit();
        editor.putInt(DmkConstants.LOGINSTATUS, 1).commit();
        homeViewPagerAdapter = new HomeViewPagerAdapter(HomeActivity.this.getSupportFragmentManager());
        homeViewPagerAdapter.addFrag(new TimeLineFragment(), getResources().getString(R.string.home));
        homeViewPagerAdapter.addFrag(new NewsFragment(), getResources().getString(R.string.news));
        homeViewPagerAdapter.addFrag(new EventsFragment(), getResources().getString(R.string.events));
//        homeViewPagerAdapter.addFrag(new SocialMediaFragment(), getResources().getString(R.string.social_media));
        homeViewPagerAdapter.addFrag(new ElectionFragment(), getResources().getString(R.string.election));
        viewpagerHome.setAdapter(homeViewPagerAdapter);
        tabsHome.setupWithViewPager(viewpagerHome, false);
        setupTabIcons();

        tabsHome.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                homeViewPagerAdapter.getCount();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                homeViewPagerAdapter.getCount();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                homeViewPagerAdapter.getCount();
            }
        });

        //Broadcast receiver
        notificationReceiver = new NotificationReceiver();
        IntentFilter intentFilter = new IntentFilter("notificationListener");
        registerReceiver(notificationReceiver, intentFilter);

        frameLayoutNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textcart_badge.setVisibility(View.GONE);
                Intent intent = new Intent(HomeActivity.this, NotificationActivity.class);
                startActivity(intent);
            }
        });


    }

    private void setupTabIcons() {
        TextView textHome = (TextView) LayoutInflater.from(HomeActivity.this).inflate(R.layout.tab_text, null);
        textHome.setText(getResources().getString(R.string.home));
        textHome.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_tab_home, 0, 0, 0);
        tabsHome.getTabAt(0).setCustomView(textHome);
        TextView textNews = (TextView) LayoutInflater.from(HomeActivity.this).inflate(R.layout.tab_text, null);
        textNews.setText(getResources().getString(R.string.news));
        textNews.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_tab_news, 0, 0, 0);
        tabsHome.getTabAt(1).setCustomView(textNews);
        TextView textEvents = (TextView) LayoutInflater.from(HomeActivity.this).inflate(R.layout.tab_text, null);
        textEvents.setText(getResources().getString(R.string.events));
        textEvents.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_tab_events, 0, 0, 0);
        tabsHome.getTabAt(2).setCustomView(textEvents);
//        TextView textSocial = (TextView) LayoutInflater.from(HomeActivity.this).inflate(R.layout.tab_text, null);
//        textSocial.setText(getResources().getString(R.string.social_media));
//        textSocial.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_tab_social_media, 0, 0, 0);
//        tabsHome.getTabAt(3).setCustomView(textSocial);
        TextView textElection = (TextView) LayoutInflater.from(HomeActivity.this).inflate(R.layout.tab_text, null);
        textElection.setText(getResources().getString(R.string.election));
        textElection.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_tab_election, 0, 0, 0);
        tabsHome.getTabAt(3).setCustomView(textElection);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    @Override
    public void onBackPressed() {
        if (viewpagerHome.getCurrentItem() != 0) {
            viewpagerHome.setCurrentItem(0, true);
        } else if (JZVideoPlayer.backPress()) {
            return;
        } else {
            if (backButtonCount >= 1) {
                super.onBackPressed();
            } else {
                Toast.makeText(this, getString(R.string.back_pressed), Toast.LENGTH_SHORT).show();
                backButtonCount++;
            }
        }
        JZVideoPlayer.releaseAllVideos();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (resultCode) {
            case 0:
                bottomPosition = 0;
                break;
            case 1:
                bottomPosition = 1;
                break;
            case 2:
                bottomPosition = 2;
                break;
            case 3:
                bottomPosition = 3;
                break;
            case 4:
                bottomPosition = 4;
                break;

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        JZVideoPlayer.releaseAllVideos();
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("notify", false)) {
                int notificationCount;
                textcart_badge.setVisibility(View.VISIBLE);
                if (!textcart_badge.getText().toString().equalsIgnoreCase("9+"))
                    notificationCount = Integer.parseInt(textcart_badge.getText().toString()) + 1;
                else
                    notificationCount = 10;

                if (notificationCount > 9)
                    textcart_badge.setText("9+");
                else
                    textcart_badge.setText("" + notificationCount);

            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(notificationReceiver);
    }

}
