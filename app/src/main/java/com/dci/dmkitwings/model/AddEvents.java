package com.dci.dmkitwings.model;

import java.util.List;

public class AddEvents {

    public String getVideothumb() {
        return Videothumb;
    }

    public void setVideothumb(String videothumb) {
        Videothumb = videothumb;
    }

    private String Videothumb;

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEndate() {
        return endate;
    }

    public void setEndate(String endate) {
        this.endate = endate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitleimage() {
        return titleimage;
    }

    public void setTitleimage(String titleimage) {
        this.titleimage = titleimage;
    }



    public int getMediatype() {
        return mediatype;
    }

    public void setMediatype(int mediatype) {
        this.mediatype = mediatype;
    }

    public int getCreatedby() {
        return createdby;
    }

    public void setCreatedby(int createdby) {
        this.createdby = createdby;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getWard() {
        return ward;
    }

    public void setWard(int ward) {
        this.ward = ward;
    }

    public int getPanchayatunion() {
        return panchayatunion;
    }

    public void setPanchayatunion(int panchayatunion) {
        this.panchayatunion = panchayatunion;
    }

    public int getVillagepanchayat() {
        return villagepanchayat;
    }

    public void setVillagepanchayat(int villagepanchayat) {
        this.villagepanchayat = villagepanchayat;
    }

    public int getPartid() {
        return partid;
    }

    public void setPartid(int partid) {
        this.partid = partid;
    }

    public int getMunicipality() {
        return municipality;
    }

    public void setMunicipality(int municipality) {
        this.municipality = municipality;
    }

    public int getTownship() {
        return township;
    }

    public void setTownship(int township) {
        this.township = township;
    }

//    public int getVattam() {
//        return vattam;
//    }
//
//    public void setVattam(int vattam) {
//        this.vattam = vattam;
//    }

    public int getDistrict() {
        return district;
    }

    public void setDistrict(int district) {
        this.district = district;
    }

    public int getPartydistrict() {
        return partydistrict;
    }

    public void setPartydistrict(int partydistrict) {
        this.partydistrict = partydistrict;
    }

    public int getConstituency() {
        return constituency;
    }

    public void setConstituency(int constituency) {
        this.constituency = constituency;
    }

    public int getTypeid() {
        return typeid;
    }

    public void setDivisionId(int typeid) {
        this.typeid = typeid;
    }

    private String startdate;
    private String endate;
    private String title;
    private String description;
    private String titleimage;

    public List<String> getMediafiles() {
        return mediafiles;
    }

    public void setMediafiles(List<String> mediafiles) {
        this.mediafiles = mediafiles;
    }

    private List<String> mediafiles;
    private int mediatype;
    private int createdby;
    private int status;
    private int ward;
    private int panchayatunion;
    private int villagepanchayat;
    private int partid;
    private int municipality;
    private int township;
//    private int vattam;
    private int district;
    private int partydistrict;
    private int constituency;
    private int typeid;
    private String venue;

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    private int userid;

    public int getEventid() {
        return eventid;
    }

    public void setEventid(int eventid) {
        this.eventid = eventid;
    }

    private int eventid;

    public int getBoothid() {
        return BoothID;
    }

    public void setBoothid(int boothid) {
        this.BoothID = boothid;
    }

    private int BoothID;

}
