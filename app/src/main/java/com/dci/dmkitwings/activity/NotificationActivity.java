package com.dci.dmkitwings.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.adapter.NotificationAdapter;
import com.dci.dmkitwings.app.DmkApplication;
import com.dci.dmkitwings.model.NotificationCount;
import com.dci.dmkitwings.model.NotificationListResponse;
import com.dci.dmkitwings.model.NotificationListResultsItem;
import com.dci.dmkitwings.retrofit.DmkAPI;
import com.dci.dmkitwings.utils.DmkConstants;
import com.dci.dmkitwings.utils.LanguageHelper;
import com.dci.dmkitwings.utils.Util;
import com.dci.dmkitwings.view.CustomTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends BaseActivity {


    List<NotificationListResultsItem> notificationLSList;
    NotificationAdapter notificationAdapter;
    @BindView(R.id.swipe_notify)
    SwipeRefreshLayout swipeNotify;
    @BindView(R.id.cons_notify_list)
    ConstraintLayout consNotifyList;
    @Inject
    DmkAPI dmkAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String currentDateandTime;
    public static String currentDateandTimeResponse;
    @BindView(R.id.image_no_notify)
    ImageView imageNoNotify;
    @BindView(R.id.text_no_notification)
    CustomTextView textNoNotification;
    NotificationListResponse notificationListResponse;
    @BindView(R.id.list_notify)
    ListView listNotify;
    private int preLast;
    boolean lastEnd, onCreatebool = false;
    int totalcount = 0, pagecount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_notification_list, null, false);
        ButterKnife.bind(this, contentView);
        DmkApplication.getContext().getComponent().inject(this);
        mDrawerLayout.addView(contentView, 0);
        frameLayoutNotification.setVisibility(View.GONE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        textTitle.setText(R.string.notification);
        notificationLSList = new ArrayList<NotificationListResultsItem>();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        Show Activity Logo
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //Navigation Control
        getSupportActionBar().setHomeButtonEnabled(false);
//        ActionBar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        notificationAdapter = new NotificationAdapter(NotificationActivity.this, notificationLSList);
        listNotify.setAdapter(notificationAdapter);
        swipeNotify.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeNotify.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pagecount = 0;
                        onCreatebool = false;
                        lastEnd = false;
                        preLast = 0;
                        getNotificationList();
                        swipeNotify.setRefreshing(false);
                    }
                }, 000);
            }
        });


        listNotify.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (notificationLSList.get(position).getType() == 1) {
//                    events
                    Intent intent = new Intent(NotificationActivity.this, EventDetailsActivity.class);
                    intent.putExtra("eventID", notificationLSList.get(position).getId());
                    startActivity(intent);

                } else if (notificationLSList.get(position).getType() == 2) {
//                    polls
                    Intent intent = new Intent(NotificationActivity.this, PollListActivity.class);
                    intent.putExtra("pollid", notificationLSList.get(position).getId());
                    startActivity(intent);
                }
                notificationLSList.get(position).setViewStatus(0);
                notificationAdapter.notifyDataSetChanged();
            }
        });
        listNotify.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (listNotify.getLastVisiblePosition() - listNotify.getHeaderViewsCount() -
                        listNotify.getFooterViewsCount()) >= (notificationAdapter.getCount() - 1)) {

                    // Now your listview has hit the bottom
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                final int lastItem = firstVisibleItem + visibleItemCount;


                if (lastItem == totalItemCount) {
                    if (notificationLSList.size() >= 20) {
                        if (preLast != lastItem) {
                            try {
                                if (!lastEnd && onCreatebool) {
                                    pagecount = pagecount + 21;


                                    getNotificationList();

                                    preLast = lastItem;
                                } else {
                                    //Log.d("Last", "fail");
                                }
                            } catch (Exception e) {
                            }

                        }
                    }
                }


            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getNotificationList();
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    private void getNotificationList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            if (pagecount == 0) {
                notificationLSList.clear();
            }

            NotificationCount notificationCount = new NotificationCount();
            currentDateandTime = simpleDateFormat.format(Calendar.getInstance().getTime());
            if (currentDateandTimeResponse != null)
                notificationCount.setCurrenttime(currentDateandTimeResponse);
            else
                notificationCount.setCurrenttime(currentDateandTime);
            notificationCount.setUserid(sharedPreferences.getInt(DmkConstants.USERID, 0));
            notificationCount.setLimit(20);
            notificationCount.setOffset(pagecount);
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put(getString(R.string.header_key), sharedPreferences.getString(DmkConstants.HEADER, ""));
            dmkAPI.getNotificationList(headerMap,notificationCount).enqueue(new Callback<NotificationListResponse>() {
                @Override
                public void onResponse(Call<NotificationListResponse> call, Response<NotificationListResponse>
                        response) {
                    hideProgress();
                    notificationListResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200
                            && notificationListResponse != null) {
                        if (notificationListResponse.getStatuscode()==0) {
                            if (notificationListResponse.getResults().size() > 0) {
                                imageNoNotify.setVisibility(View.GONE);
                                textNoNotification.setVisibility(View.GONE);
                                listNotify.setVisibility(View.VISIBLE);
                                for (int i = 0; i < notificationListResponse.getResults().size(); i++) {
                                    notificationLSList.add(new NotificationListResultsItem(
                                            notificationListResponse.getResults().get(i).getCreated_at() != null ?
                                                    getDateFormat(notificationListResponse.getResults().get(i).getCreated_at()) : null,
                                            notificationListResponse.getResults().get(i).getId(),
                                            notificationListResponse.getResults().get(i).getTitle() != null ?
                                                    notificationListResponse.getResults().get(i).getTitle() : "",
                                            notificationListResponse.getResults().get(i).getType(),
                                            notificationListResponse.getResults().get(i).getViewStatus()
                                    ));
                                }
                                currentDateandTimeResponse = notificationListResponse.getUsertime();
                                notificationAdapter.notifyDataSetChanged();
                            } else {
                                if (notificationLSList.size() == 0) {
                                    imageNoNotify.setVisibility(View.VISIBLE);
                                    imageNoNotify.setImageResource(R.mipmap.icon_no_notification);
                                    textNoNotification.setVisibility(View.VISIBLE);
                                    textNoNotification.setText(getString(R.string.No_notification));
                                    listNotify.setVisibility(View.GONE);
                                } else

                                {
                                    lastEnd = true;
                                }
                            }
                        } else {
                            editor.putString(DmkConstants.HEADER_SOURCE, notificationListResponse.getSource());
                            editor.putString(DmkConstants.HEADER_SOURCE_DATA, notificationListResponse.getSourcedata());
                            editor.putString(DmkConstants.HEADER, getEncodedHeader(notificationListResponse.getSource(),
                                    notificationListResponse.getSourcedata()));
                            editor.commit();
                            getNotificationList();
                        }
                    } else {
                        if (notificationLSList.size() == 0) {
                            imageNoNotify.setVisibility(View.VISIBLE);
                            imageNoNotify.setImageResource(R.mipmap.icon_no_notification);
                            textNoNotification.setVisibility(View.VISIBLE);
                            textNoNotification.setText(getString(R.string.No_notification));
                            listNotify.setVisibility(View.GONE);
                        }
                    }


                }

                @Override
                public void onFailure(Call<NotificationListResponse> call, Throwable t) {
                    hideProgress();
                    if (notificationLSList.size() == 0) {
                        imageNoNotify.setVisibility(View.VISIBLE);
                        imageNoNotify.setImageResource(R.mipmap.icon_no_notification);
                        textNoNotification.setVisibility(View.VISIBLE);
                        textNoNotification.setText(getString(R.string.No_notification));
                        listNotify.setVisibility(View.GONE);
                    }
                }
            });


        } else {
            imageNoNotify.setVisibility(View.VISIBLE);
            imageNoNotify.setImageResource(R.mipmap.icon_no_network);
            textNoNotification.setVisibility(View.VISIBLE);
            textNoNotification.setText(getString(R.string.no_network));
            listNotify.setVisibility(View.GONE);
        }
    }


}
