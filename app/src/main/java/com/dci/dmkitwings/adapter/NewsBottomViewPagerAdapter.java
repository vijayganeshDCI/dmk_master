package com.dci.dmkitwings.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.dci.dmkitwings.fragment.ArticleFragment;
import com.dci.dmkitwings.fragment.SubNewsFragment;
import com.dci.dmkitwings.fragment.HirerachyFragment;
import com.dci.dmkitwings.fragment.Libraryfragment;
import com.dci.dmkitwings.fragment.MurasoliFragment;
import com.dci.dmkitwings.fragment.PollsFragment;

import java.util.ArrayList;

/**
 *
 */
public class NewsBottomViewPagerAdapter extends FragmentPagerAdapter {

	private ArrayList<Fragment> fragments = new ArrayList<>();
	private Fragment currentFragment;

	public NewsBottomViewPagerAdapter(FragmentManager fm) {
		super(fm);
		fragments.add(new Libraryfragment());
		fragments.add(new SubNewsFragment());
		fragments.add(new MurasoliFragment());
		fragments.add(new ArticleFragment());
		fragments.add(new PollsFragment());
	}

	@Override
	public Fragment getItem(int position) {
		return fragments.get(position);
	}

	@Override
	public int getCount() {
		return fragments.size();
	}

	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) {
		if (getCurrentFragment() != object) {
			currentFragment = ((Fragment) object);
		}
		super.setPrimaryItem(container, position, object);
	}

	/**
	 * Get the current fragment
	 */
	public Fragment getCurrentFragment() {
		return currentFragment;
	}
}