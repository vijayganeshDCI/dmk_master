package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.fragment.SentFragment;
import com.dci.dmkitwings.model.ChatMessageParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class ReecentChatListAdapter extends BaseAdapter {

    public ReecentChatListAdapter(Context context, List<ChatMessageParams> contactLists,Fragment fragment) {
        this.context = context;
        this.contactLists = contactLists;
        this.fragment=fragment;
    }

    Context context;
    List<ChatMessageParams> contactLists;
    Fragment fragment;

    @Override
    public int getCount() {
        return contactLists.size();
    }

    @Override
    public ChatMessageParams getItem(int position) {
        return contactLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_contact_list, null);
        }

        ImageView imageProPic = (ImageView) convertView.findViewById(R.id.image_profile_pic);
        ImageView imageSelected = (ImageView) convertView.findViewById(R.id.image_selected);
        imageSelected.setImageResource(R.mipmap.icon_un_read);
        ImageView imageCall = (ImageView) convertView.findViewById(R.id.image_call);
        TextView textName = (TextView) convertView.findViewById(R.id.text_name);
        TextView textDes = (TextView) convertView.findViewById(R.id.text_designation);

        if (fragment instanceof SentFragment) {
            //Sent list
            universalImageLoader(imageProPic, context.getString(R.string.s3_bucket_profile_path),
                    contactLists.get(position).getReceiverPhotoUrl(), R.mipmap.icon_pro_image_loading_256,
                    R.mipmap.icon_pro_image_loading_256);
            textName.setText(contactLists.get(position).getToName());
            textDes.setText(contactLists.get(position).getReceiverRoleName());
        }else {
            //Inbox list
            universalImageLoader(imageProPic, context.getString(R.string.s3_bucket_profile_path),
                    contactLists.get(position).getPhotoUrl(), R.mipmap.icon_pro_image_loading_256,
                    R.mipmap.icon_pro_image_loading_256);
            textName.setText(contactLists.get(position).getName());
            textDes.setText(contactLists.get(position).getRoleName());
            if (contactLists.get(position).getReadStatus()==0){
                //Un read
                imageSelected.setVisibility(View.VISIBLE);
            }else {
//                Read
                imageSelected.setVisibility(View.GONE);
            }
        }
        return convertView;
    }

    public void universalImageLoader(ImageView imageView, String s3bucketName, String image, int loadingImage,
                                     int emptyUriImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyUriImage)
                .showImageOnFail(emptyUriImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(context.getString(R.string.s3_image_url_download) +
                s3bucketName + image, imageView, options);
    }

}
