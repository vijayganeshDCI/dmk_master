package com.dci.dmkitwings.model;

public class CommentListUpdatedAt {
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public int getTimezone_type() {
		return timezone_type;
	}

	public void setTimezone_type(int timezone_type) {
		this.timezone_type = timezone_type;
	}

	private String date;
	private String timezone;
	private int timezone_type;
}
