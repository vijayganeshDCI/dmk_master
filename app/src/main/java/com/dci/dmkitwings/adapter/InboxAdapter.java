package com.dci.dmkitwings.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.dmkitwings.R;
import com.dci.dmkitwings.model.Inbox;

import java.util.List;

/**
 * Created by vijayaganesh on 4/12/2018.
 */

public class InboxAdapter extends BaseAdapter {

    Context context;
    List<Inbox> inboxList;

    public InboxAdapter(Context context, List<Inbox> inboxList) {
        this.context = context;
        this.inboxList = inboxList;
    }

    @Override
    public int getCount() {
        return inboxList.size();
    }

    @Override
    public Inbox getItem(int position) {
        return inboxList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_inbox, null);
        }


        ImageView imageProPic = (ImageView) convertView.findViewById(R.id.image_profile_pic);
        TextView textName = (TextView) convertView.findViewById(R.id.text_name);
        TextView textMes = (TextView) convertView.findViewById(R.id.text_mes);

        imageProPic.setImageResource(inboxList.get(position).getProfilePic());
        textName.setText(inboxList.get(position).getName());
        textMes.setText(inboxList.get(position).getMes());

        return convertView;
    }
}
